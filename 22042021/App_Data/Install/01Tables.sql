/****** Object:  Table [dbo].[Themes_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Themes_Mst](
	[tm_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tm_Name] [varchar](50) NOT NULL,
	[tm_Theme] [varchar](50) NOT NULL,
	[tm_Flag] [tinyint] NOT NULL,
	[tm_ThumbNailPath] [varchar](100) NULL,
	[tm_activeflg] [tinyint] NOT NULL,
 CONSTRAINT [PK_Themes_Mst] PRIMARY KEY CLUSTERED 
(
	[tm_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User_Hdr](
	[UH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UH_RHID] [numeric](18, 0) NOT NULL,
	[UH_Date] [datetime] NOT NULL,
	[UH_Name] [varchar](50) NOT NULL,
	[UH_DesgID] [numeric](18, 0) NULL,
	[UH_Address] [varchar](200) NULL,
	[UH_City] [varchar](50) NULL,
	[UH_State] [varchar](50) NULL,
	[UH_PinCode] [varchar](10) NULL,
	[UH_Mobile] [varchar](50) NULL,
	[UH_Email] [varchar](50) NULL,
	[UH_UserID] [varchar](50) NOT NULL,
	[UH_Password] [varchar](100) NOT NULL,
	[UH_TMID] [numeric](18, 0) NOT NULL,
	[UH_CurrentLogin] [datetime] NULL,
	[UH_LastLogin] [datetime] NULL,
	[UH_UserType] [tinyint] NOT NULL,
	[UH_Flag] [tinyint] NOT NULL,
	[UH_EWPwd] [varchar](100) NULL,
	[UH_EPinPwd] [varchar](100) NULL,
	[UH_EWCurrLogin] [datetime] NULL,
	[UH_EWLastLogin] [datetime] NULL,
	[UH_EpinCurrLogin] [datetime] NULL,
	[UH_EpinLastLogin] [datetime] NULL,
	[UH_chvExtra1] [varchar](200) NULL,
	[UH_chvExtra2] [varchar](200) NULL,
	[UH_decExtra3] [numeric](18, 2) NULL,
	[UH_decExtra4] [numeric](18, 2) NULL,
	[UH_dtmExtra5] [datetime] NULL,
	[UH_dtmExtra6] [datetime] NULL,
 CONSTRAINT [PK_User_Hdr] PRIMARY KEY CLUSTERED 
(
	[UH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProdDist_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProdDist_Mst](
	[PDM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PDM_Code] [varchar](50) NULL,
	[PDM_Name] [varchar](100) NULL,
	[PDM_Address] [varchar](150) NULL,
	[PDM_City] [varchar](50) NULL,
	[PDM_CntNo] [varchar](50) NULL,
	[PDM_Mobile] [varchar](50) NULL,
	[PDM_Email] [varchar](50) NULL,
	[PDM_State] [varchar](50) NULL,
	[PDM_CntPer] [varchar](50) NULL,
	[PDM_Location] [varchar](100) NULL,
	[PDM_LoginID] [varchar](50) NULL,
	[PDM_PWD] [varchar](50) NULL,
	[PDM_OpenBal] [numeric](18, 0) NOT NULL,
	[PDM_CrdDbtflag] [tinyint] NOT NULL,
	[PDM_DOJ] [datetime] NOT NULL,
	[PDM_RefFranName] [varchar](100) NULL,
	[PDM_Webflag] [tinyint] NOT NULL,
	[PDM_Webstatusid] [tinyint] NOT NULL,
	[PDM_Frflag] [numeric](18, 0) NOT NULL,
	[PDM_Introid] [numeric](18, 0) NOT NULL,
	[PDM_Date] [datetime] NULL,
	[PDM_decEXTRA1] [numeric](18, 2) NULL,
	[PDM_decEXTRA2] [numeric](18, 2) NULL,
	[PDM_chvEXTRA3] [varchar](100) NULL,
	[PDM_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_ProdDist_Mst] PRIMARY KEY CLUSTERED 
(
	[PDM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Paymode_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Paymode_mst](
	[PM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PM_Name] [varchar](50) NOT NULL,
	[PM_Desc] [varchar](50) NULL,
	[PM_MSMID] [numeric](20, 0) NOT NULL,
	[PM_Flag] [bit] NOT NULL,
	[PM_EpinFlg] [bit] NOT NULL,
	[PM_PaymentFlg] [bit] NOT NULL,
	[PM_ShoppeFlg] [bit] NOT NULL,
	[PM_ResaleFlg] [bit] NOT NULL,
	[PM_UpgradeFlg] [bit] NOT NULL,
	[PM_PCRFlg] [bit] NOT NULL,
	[PM_POFlg] [bit] NOT NULL,
	[PM_WalkInFlg] [bit] NULL,
	[PM_RegisterFlg] [bit] NULL,
	[PM_DefaultPaymentFlg] [bit] NOT NULL,
	[PM_SystemName] [varchar](100)  NULL,
	[PM_EwalletFlag] [bit] NULL,
 CONSTRAINT [PK_Paymode_mst] PRIMARY KEY CLUSTERED 
(
	[PM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[State_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[State_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_CountryID] [numeric](18, 0) NOT NULL,
	[SM_Name] [varchar](50) NOT NULL,
	[SM_Flag] [tinyint] NOT NULL,
	[SM_decEXTRA1] [numeric](18, 2) NULL,
	[SM_decEXTRA2] [numeric](18, 2) NULL,
	[SM_chvEXTRA3] [varchar](50) NULL,
	[SM_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_State_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyInfo_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfo_Mst](
	[CIM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CIM_Code] [varchar](50) NULL,
	[CIM_Name] [varchar](50) NOT NULL,
	[CIM_Address] [varchar](300) NOT NULL,
	[CIM_Country] [varchar](50) NOT NULL,
	[CIM_State] [varchar](50) NULL,
	[CIM_City] [varchar](50) NULL,
	[CIM_Pincode] [varchar](10) NULL,
	[CIM_VATNO] [varchar](50) NULL,
	[CIM_TINNO] [varchar](50) NULL,
	[CIM_ContactPerson] [varchar](200) NULL,
	[CIM_Designation] [varchar](100) NULL,
	[CIM_Phone1] [varchar](50) NULL,
	[CIM_Phone2] [varchar](50) NULL,
	[CIM_Email] [varchar](200) NULL,
	[CIM_URL] [varchar](150) NULL,
	[CIM_Fax] [varchar](100) NULL,
	[CIM_Currency] [varchar](15) NULL,
	[CIM_Logo] [varchar](100) NULL,
	[CIM_Timezone] [varchar](50) NOT NULL,
	[CIM_decExtra1] [numeric](18, 2) NULL,
	[CIM_decExtra2] [numeric](18, 2) NULL,
	[CIM_chvExtra3] [varchar](200) NULL,
	[CIM_chvExtra4] [varchar](200) NULL,
	[CIM_chvExtra5] [varchar](200) NULL,
	[CIM_VatCountry] [varchar](300) NULL,
	[CIM_VatState] [varchar](300) NULL,
	[CIM_DateFormat] [varchar](50) NULL,
	[CIM_LanguageID] [varchar](300) NULL,
	[CIM_ShowCName] [bit] NULL,
	[CIM_ShowCLogo] [bit] NULL,
	[CIM_CNameStyle] [varchar](200) NULL,
	[CIM_UserName] [varchar](100) NULL,
	[CIM_Password] [varchar](100) NULL,
	[CIM_Host] [varchar](100) NULL,
	[CIM_SSL] [tinyint] NULL,
	[CIM_Port] [numeric](18, 0) NULL,
	[CIM_EmailFlg] [tinyint] NULL,
	[CIM_BannerImg] [varchar](200) NULL,
	[CIM_EmailName] [varchar](100) NULL,
	[CIM_Salutation] [varchar](150) NULL,
	[CIM_Signature] [varchar](300) NULL,
	[CIM_IPAddressflg] [tinyint] NULL,
	[CIM_IPAddress] [varchar](150) NULL,
	[CIM_MemModNmFlg] [tinyint] NULL,
	[CIM_VersionFlg] [tinyint] NULL,
	[CIM_Version] [varchar](100) NULL,
	[CIM_FtrLogoFlg] [tinyint] NULL,
	[CIM_Favicon] [varchar](100) NULL,
	[CIM_AppTitle] [varchar](100) NULL,
	[CIM_DefaultTheme] [varchar](max) NULL,
	[CIM_ShowCBanner] [bit] NULL,
 CONSTRAINT [PK_CompanyInfo_Mst] PRIMARY KEY CLUSTERED 
(
	[CIM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country_mst](
	[CNM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CNM_Name] [varchar](50) NOT NULL,
	[CNM_ISDNCode] [varchar](25) NULL,
	[CNM_CurrencyName] [varchar](50) NOT NULL,
	[CNM_CurrencyCode] [varchar](10) NOT NULL,
	[CNM_Flag] [tinyint] NOT NULL,
	[CNM_chvExtra1] [varchar](100) NULL,
	[CNM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Country_mst] PRIMARY KEY CLUSTERED 
(
	[CNM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Order_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order_Dtls](
	[OD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OD_Code] [numeric](18, 0) NOT NULL,
	[OD_PVCode] [numeric](20, 0) NOT NULL,
	[OD_BVPts] [numeric](20, 2) NOT NULL,
	[OD_PVPts] [numeric](20, 6) NOT NULL,
	[OD_Units] [numeric](20, 2) NOT NULL,
	[OD_Rate] [numeric](20, 2) NOT NULL,
	[OD_VAT] [numeric](20, 2) NOT NULL,
	[OD_MRP] [numeric](20, 2) NOT NULL,
	[OD_Qty] [numeric](20, 0) NOT NULL,
	[OD_ShipAmt] [numeric](20, 2) NOT NULL,
	[OD_GrossAmt] [numeric](20, 2) NOT NULL,
	[OD_DiscAmt] [numeric](20, 2) NOT NULL,
	[OD_TotDiscAmt] [numeric](20, 2) NOT NULL,
	[OD_TotShipAmt] [numeric](20, 2) NOT NULL,
	[OD_NetAmt] [numeric](20, 2) NOT NULL,
	[OD_TotBVPts] [numeric](20, 2) NOT NULL,
	[OD_TotPVPts] [numeric](20, 6) NOT NULL,
	[OD_TotUnits] [numeric](20, 0) NOT NULL,
	[OD_decExtra1] [numeric](20, 2) NULL,
	[OD_decExtra2] [numeric](20, 2) NULL,
	[OD_decExtra3] [numeric](20, 2) NULL,
	[OD_decExtra4] [numeric](20, 2) NULL,
	[OD_chvExtra1] [varchar](500) NULL,
	[OD_chvExtra2] [varchar](500) NULL,
	[OD_chvExtra3] [varchar](500) NULL,
	[OD_chvExtra4] [varchar](500) NULL,
	[OD_dtmExtra1] [datetime] NULL,
	[OD_dtmExtra2] [datetime] NULL,
	[OD_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Order_Dtls] PRIMARY KEY CLUSTERED 
(
	[OD_Code] ASC,
	[OD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Order_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order_Hdr](
	[OH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OH_Code] [numeric](18, 0) NOT NULL,
	[OH_MemID] [numeric](20, 0) NULL,
	[OH_AMID] [numeric](20, 0) NULL,
	[OH_Date] [datetime] NOT NULL,
	[OH_DateTime] [datetime] NOT NULL,
	[OH_TotQty] [numeric](20, 0) NOT NULL,
	[OH_GrossAmt] [numeric](20, 2) NOT NULL,
	[OH_DiscAmt] [numeric](20, 2) NOT NULL,
	[OH_ShipAmt] [numeric](20, 2) NOT NULL,
	[OH_NetAmt] [numeric](20, 2) NOT NULL,
	[OH_TotBVPts] [numeric](20, 2) NOT NULL,
	[OH_TotPVPts] [numeric](20, 2) NOT NULL,
	[OH_TotUnits] [numeric](20, 2) NOT NULL,
	[OH_LR] [varchar](50) NULL,
	[OH_BillName] [varchar](100) NULL,
	[OH_BillAddress] [varchar](500) NULL,
	[OH_BillCountryID] [numeric](20, 0) NULL,
	[OH_BillCountry] [varchar](50) NULL,
	[OH_BillStateID] [numeric](20, 0) NULL,
	[OH_BillState] [varchar](50) NULL,
	[OH_BillCity] [varchar](50) NULL,
	[OH_BillPincode] [varchar](10) NULL,
	[OH_BillMobile] [varchar](20) NULL,
	[OH_BillContactNo] [varchar](20) NULL,
	[OH_BillEmail] [varchar](100) NULL,
	[OH_ShipName] [varchar](100) NULL,
	[OH_ShipAddress] [varchar](500) NULL,
	[OH_ShipCountryID] [numeric](20, 0) NULL,
	[OH_ShipCountry] [varchar](50) NULL,
	[OH_ShipStateID] [numeric](20, 0) NULL,
	[OH_ShipState] [varchar](50) NULL,
	[OH_ShipCity] [varchar](50) NULL,
	[OH_ShipPincode] [varchar](10) NULL,
	[OH_ShipMobile] [varchar](20) NULL,
	[OH_ShipContactNo] [varchar](20) NULL,
	[OH_ShipEmail] [varchar](100) NULL,
	[OH_Paymode] [numeric](20, 0) NOT NULL,
	[OH_ChqDDNo] [numeric](20, 0) NULL,
	[OH_ChqDDDate] [datetime] NULL,
	[OH_ChqDDBank] [varchar](100) NULL,
	[OH_ChqDDBranch] [varchar](50) NULL,
	[OH_PinSrNo] [numeric](18, 0) NULL,
	[OH_Remark] [varchar](500) NULL,
	[OH_TrnFlg] [tinyint] NOT NULL,
	[OH_StatusFlg] [tinyint] NOT NULL,
	[OH_StatusDate] [datetime] NULL,
	[OH_ConfirmFlg] [tinyint] NOT NULL,
	[OH_ConfirmDate] [datetime] NULL,
	[OH_DispatchMode] [numeric](20, 0) NULL,
	[OH_DispatchBy] [numeric](20, 0) NULL,
	[OH_DispatchFrID] [numeric](20, 0) NULL,
	[OH_OrderType] [tinyint] NOT NULL,
	[OH_FrID] [numeric](20, 0) NULL,
	[OH_AdminID] [numeric](20, 0) NULL,
	[OH_IPAddress] [varchar](50) NULL,
	[OH_Browser] [varchar](50) NULL,
	[OH_BrowserVersion] [varchar](50) NULL,
	[OH_PGCode] [varchar](50) NULL,
	[OH_CCTrnAmt] [numeric](20, 2) NULL,
	[OH_TrnTotalAmt] [numeric](20, 2) NULL,
	[OH_chvExtra1] [varchar](100) NULL,
	[OH_chvExtra2] [varchar](100) NULL,
	[OH_chvExtra3] [varchar](100) NULL,
	[OH_chvExtra4] [varchar](100) NULL,
	[OH_decExtra1] [numeric](20, 2) NULL,
	[OH_decExtra2] [numeric](20, 2) NULL,
	[OH_decExtra3] [numeric](20, 2) NULL,
	[OH_decExtra4] [numeric](20, 2) NULL,
	[OH_dtmExtra1] [datetime] NULL,
	[OH_dtmExtra2] [datetime] NULL,
	[OH_dtmExtra3] [datetime] NULL,
	[OH_dtmExtra4] [datetime] NULL,
	[OH_RejectedFrID] [numeric](20, 0) NULL,
	[OH_TotInQty] [numeric](18, 0) NOT NULL,
	[OH_PCRCode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Order_Hdr] PRIMARY KEY CLUSTERED 
(
	[OH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category_Mst](
	[CM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CM_Code] [varchar](15) NOT NULL,
	[CM_Date] [datetime] NOT NULL,
	[CM_DateTime] [datetime] NOT NULL,
	[CM_Name] [varchar](100) NOT NULL,
	[CM_Flag] [tinyint] NOT NULL,
	[CM_ParentID] [numeric](20, 0) NOT NULL,
	[CM_ImagePath] [varchar](100) NULL,
 CONSTRAINT [PK_Category_Mst] PRIMARY KEY CLUSTERED 
(
	[CM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PV_MST]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PV_MST](
	[PV_Code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PV_PDCode] [varchar](20) NULL,
	[PV_Date] [datetime] NULL,
	[PV_DateTime] [datetime] NULL,
	[PV_CategoryID] [numeric](18, 0) NOT NULL,
	[PV_BrandID] [numeric](20, 0) NULL,
	[PV_Name] [varchar](100) NULL,
	[PV_Desc] [varchar](8000) NULL,
	[PV_Image] [varchar](100) NULL,
	[PV_Thumbnail] [varchar](100) NULL,
	[PV_FilePath] [varchar](100) NULL,
	[PV_BVPts] [numeric](20, 2) NOT NULL,
	[PV_PVPts] [numeric](20, 2) NOT NULL,
	[PV_GrUnits] [numeric](20, 2) NOT NULL,
	[PV_SaleRate] [numeric](20, 2) NOT NULL,
	[PV_SaleVAT] [numeric](20, 2) NOT NULL,
	[PV_SalePrice] [numeric](20, 2) NOT NULL,
	[PV_Type] [numeric](20, 0) NOT NULL,
	[PV_PurRate] [numeric](20, 2) NOT NULL,
	[PV_PurVAT] [numeric](20, 2) NOT NULL,
	[PV_PurPrice] [numeric](20, 2) NOT NULL,
	[PV_UOM] [varchar](50) NULL,
	[PV_AdvanceAmt] [numeric](20, 2) NOT NULL,
	[PV_TOAmt] [numeric](20, 2) NOT NULL,
	[PV_ReserveAmt] [numeric](20, 2) NOT NULL,
	[PV_SpillAmt] [numeric](20, 2) NOT NULL,
	[PV_SpillNtwAmt] [numeric](20, 2) NOT NULL,
	[PV_DirectAmt] [numeric](20, 2) NOT NULL,
	[PV_RoyAmt] [numeric](20, 2) NOT NULL,
	[PV_FranchiseAmt] [numeric](20, 2) NOT NULL,
	[PV_SimAmt] [numeric](20, 2) NOT NULL,
	[PV_Award] [numeric](20, 2) NOT NULL,
	[PV_Passive] [numeric](20, 2) NOT NULL,
	[PV_ShipAmt] [numeric](20, 2) NOT NULL,
	[PV_DiscPercent] [numeric](20, 2) NOT NULL,
	[PV_DiscAmt] [numeric](20, 2) NOT NULL,
	[PV_LeadDays] [numeric](20, 0) NOT NULL,
	[PV_ReorderQty] [numeric](20, 2) NOT NULL,
	[PV_OpenStock] [numeric](20, 2) NOT NULL,
	[PV_MinStockQty] [numeric](20, 0) NULL,
	[PV_Manufacturer] [varchar](100) NULL,
	[PV_Model] [varchar](50) NULL,
	[PV_Weight] [numeric](20, 0) NULL,
	[PV_IsTodaySpecial] [tinyint] NOT NULL,
	[PV_IsDownloadable] [tinyint] NOT NULL,
	[PV_DownloadPath] [varchar](100) NULL,
	[PV_Rating] [numeric](20, 0) NOT NULL,
	[PV_Viewed] [numeric](20, 0) NOT NULL,
	[PV_IsHotDeal] [tinyint] NOT NULL,
	[PV_ShoppeFlg] [tinyint] NOT NULL,
	[PV_ResaleFlg] [tinyint] NOT NULL,
	[PV_JoiningFlg] [tinyint] NOT NULL,
	[PV_CapFlg] [tinyint] NOT NULL,
	[PV_StatusFlg] [tinyint] NOT NULL,
	[PV_PackageFlg] [tinyint] NOT NULL,
	[PV_WebFlag] [tinyint] NOT NULL,
	[PV_decExtra1] [numeric](20, 2) NULL,
	[PV_decExtra2] [numeric](20, 2) NULL,
	[PV_decExtra3] [numeric](20, 2) NULL,
	[PV_decExtra4] [numeric](20, 2) NULL,
	[PV_chvExtra1] [varchar](200) NULL,
	[PV_chvExtra2] [varchar](200) NULL,
	[PV_chvExtra3] [varchar](200) NULL,
	[PV_chvExtra4] [varchar](200) NULL,
	[PV_dtmExtra1] [datetime] NULL,
	[PV_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_PV_MST] PRIMARY KEY CLUSTERED 
(
	[PV_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemProfile_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemProfile_Dtls](
	[MPD_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPD_MemId] [numeric](18, 0) NOT NULL,
	[MPD_Name] [varchar](100) NOT NULL,
	[MPD_DOB] [datetime] NULL,
	[MPD_Title] [varchar](10) NULL,
	[MPD_Gender] [varchar](50) NULL,
	[MPD_MaritalStatus] [varchar](10) NULL,
	[MPD_Mother] [varchar](100) NULL,
	[MPD_Father] [varchar](100) NULL,
	[MPD_HoWo] [varchar](100) NULL,
	[MPD_PassportNo] [varchar](50) NULL,
	[MPD_IFSDCode] [varchar](20) NULL,
	[MPD_Qualification] [varchar](50) NULL,
	[MPD_Profession] [varchar](100) NULL,
	[MPD_Designation] [varchar](50) NULL,
	[MPD_Income] [varchar](50) NULL,
	[MPD_SalStatus] [varchar](50) NULL,
	[MPD_NoDepend] [numeric](18, 0) NULL,
	[MPD_TwoWheeler] [varchar](10) NULL,
	[MPD_FourWheeler] [varchar](10) NULL,
	[MPD_Age] [varchar](100) NULL,
	[MPD_Nationality] [varchar](50) NULL,
	[MPD_BloodGroup] [varchar](10) NULL,
	[MPD_ResStatus] [varchar](50) NULL,
	[MPD_PanCardNo] [varchar](50) NULL,
	[MPD_ApplyPan] [varchar](50) NULL,
	[MPD_ChqPayTo] [varchar](100) NULL,
	[MPD_Address] [varchar](400) NULL,
	[MPD_LandMark] [varchar](400) NULL,
	[MPD_City] [varchar](50) NULL,
	[MPD_District] [varchar](50) NULL,
	[MPD_State] [varchar](50) NULL,
	[MPD_Country] [varchar](50) NULL,
	[MPD_PinCode] [varchar](10) NULL,
	[MPD_Mobile] [varchar](20) NULL,
	[MPD_ResPhone] [varchar](20) NULL,
	[MPD_OffPhone] [varchar](20) NULL,
	[MPD_Email] [varchar](50) NULL,
	[MPD_Fax] [varchar](50) NULL,
	[MPD_ShpAddress] [varchar](400) NULL,
	[MPD_ShpLandMark] [varchar](400) NULL,
	[MPD_ShpCity] [varchar](50) NULL,
	[MPD_ShpDistrict] [varchar](50) NULL,
	[MPD_ShpState] [varchar](50) NULL,
	[MPD_ShpCountry] [varchar](50) NULL,
	[MPD_ShpPinCode] [varchar](10) NULL,
	[MPD_ShpContPerson] [varchar](20) NULL,
	[MPD_ShpContNo] [varchar](20) NULL,
	[MPD_Bank] [varchar](100) NULL,
	[MPD_Branch] [varchar](50) NULL,
	[MPD_BankAddress] [varchar](400) NULL,
	[MPD_AccType] [varchar](30) NULL,
	[MPD_AccNo] [varchar](16) NULL,
	[MPD_AccPwd] [varchar](16) NULL,
	[MPD_nomTitle] [varchar](50) NULL,
	[MPD_nomName] [varchar](100) NULL,
	[MPD_nomGender] [varchar](10) NULL,
	[MPD_nomRel] [varchar](50) NULL,
	[MPD_nomBdate] [datetime] NULL,
	[MPD_NomAddr] [varchar](50) NULL,
	[MPD_PinSrNo] [numeric](18, 0) NULL,
	[MPD_DDNo] [numeric](18, 0) NULL,
	[MPD_DDDate] [datetime] NULL,
	[MPD_DDBank] [varchar](100) NULL,
	[MPD_DDBranch] [varchar](100) NULL,
	[MPD_DDBankAddr] [varchar](100) NULL,
	[MPD_EpinPwd] [varchar](100) NULL,
	[MPD_EWSpon] [varchar](50) NULL,
	[MPD_EWAccNo] [varchar](100) NULL,
	[MPD_EWTransfer] [decimal](18, 0) NULL,
	[MPD_FirmType] [varchar](50) NULL,
	[MPD_FirmDetail] [varchar](500) NULL,
	[MPD_ReceiptNo] [varchar](50) NOT NULL,
	[MPD_EpinCurrentLogin] [datetime] NULL,
	[MPD_EpinLastLogin] [datetime] NULL,
	[MPD_Extra1] [varchar](100) NULL,
	[MPD_Extra2] [varchar](100) NULL,
	[MPD_Extra3] [varchar](100) NULL,
	[MPD_Extra4] [varchar](100) NULL,
	[MPD_Extra5] [varchar](100) NULL,
	[MPD_Extra6] [varchar](100) NULL,
	[MPD_Extra7] [varchar](100) NULL,
	[MPD_Extra8] [varchar](100) NULL,
	[MPD_Extra9] [varchar](100) NULL,
	[MPD_Extra10] [varchar](100) NULL,
	[MPD_Extra11] [varchar](100) NULL,
	[MPD_Extra12] [varchar](100) NULL,
	[MPD_Extra13] [varchar](100) NULL,
	[MPD_Extra14] [varchar](100) NULL,
	[MPD_Extra15] [varchar](100) NULL,
	[MPD_Extra16] [varchar](100) NULL,
	[MPD_Extra17] [varchar](100) NULL,
	[MPD_Extra18] [varchar](100) NULL,
	[MPD_Extra19] [varchar](100) NULL,
	[MPD_Extra20] [varchar](100) NULL,
	[MPD_Extra21] [varchar](100) NULL,
	[MPD_Extra22] [varchar](100) NULL,
	[MPD_Extra23] [varchar](100) NULL,
	[MPD_Extra24] [varchar](100) NULL,
	[MPD_Extra25] [varchar](100) NULL,
	[MPD_Extra26] [varchar](100) NULL,
	[MPD_Extra27] [varchar](100) NULL,
	[MPD_Extra28] [varchar](100) NULL,
	[MPD_Extra29] [varchar](100) NULL,
	[MPD_Extra30] [varchar](100) NULL,
	[MPD_ImagePath] [varchar](100) NULL,
	[MPD_PayReceiptMode] [numeric](18, 0) NULL,
	[ExternalAuthFlag] [varchar] (4)  NULL,
 CONSTRAINT [PK_MemProfile_Dtls] PRIMARY KEY CLUSTERED 
(
	[MPD_MemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_EpinSrno] UNIQUE NONCLUSTERED 
(
	[MPD_PinSrNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemProfileRequest_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemProfileRequest_Dtls](
	[MPD_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPD_MemId] [numeric](18, 0) NOT NULL,
	[MPD_Name] [varchar](100) NULL,
	[MPD_DOB] [datetime] NULL,
	[MPD_Title] [varchar](10) NOT NULL,
	[MPD_Gender] [varchar](50) NULL,
	[MPD_MaritalStatus] [varchar](10) NULL,
	[MPD_Mother] [varchar](100) NULL,
	[MPD_Father] [varchar](100) NULL,
	[MPD_HoWo] [varchar](100) NULL,
	[MPD_PassportNo] [varchar](50) NULL,
	[MPD_IFSDCode] [varchar](20) NULL,
	[MPD_Qualification] [varchar](50) NULL,
	[MPD_Profession] [varchar](100) NULL,
	[MPD_Designation] [varchar](50) NULL,
	[MPD_Income] [varchar](50) NULL,
	[MPD_SalStatus] [varchar](50) NULL,
	[MPD_NoDepend] [numeric](18, 0) NULL,
	[MPD_TwoWheeler] [varchar](10) NULL,
	[MPD_FourWheeler] [varchar](10) NULL,
	[MPD_Age] [varchar](100) NULL,
	[MPD_Nationality] [varchar](50) NULL,
	[MPD_BloodGroup] [varchar](10) NULL,
	[MPD_ResStatus] [varchar](50) NULL,
	[MPD_PanCardNo] [varchar](50) NULL,
	[MPD_ApplyPan] [varchar](50) NULL,
	[MPD_ChqPayTo] [varchar](100) NULL,
	[MPD_Address] [varchar](400) NULL,
	[MPD_LandMark] [varchar](400) NULL,
	[MPD_City] [varchar](50) NULL,
	[MPD_District] [varchar](50) NULL,
	[MPD_State] [varchar](50) NULL,
	[MPD_Country] [varchar](50) NULL,
	[MPD_PinCode] [varchar](10) NULL,
	[MPD_Mobile] [varchar](20) NULL,
	[MPD_ResPhone] [varchar](20) NULL,
	[MPD_OffPhone] [varchar](20) NULL,
	[MPD_Email] [varchar](50) NULL,
	[MPD_Fax] [varchar](50) NULL,
	[MPD_ShpAddress] [varchar](400) NULL,
	[MPD_ShpLandMark] [varchar](400) NULL,
	[MPD_ShpCity] [varchar](50) NULL,
	[MPD_ShpDistrict] [varchar](50) NULL,
	[MPD_ShpState] [varchar](50) NULL,
	[MPD_ShpCountry] [varchar](50) NULL,
	[MPD_ShpPinCode] [numeric](18, 0) NULL,
	[MPD_ShpContPerson] [varchar](20) NULL,
	[MPD_ShpContNo] [varchar](20) NULL,
	[MPD_Bank] [varchar](30) NULL,
	[MPD_Branch] [varchar](50) NULL,
	[MPD_BankAddress] [varchar](400) NULL,
	[MPD_AccType] [varchar](30) NULL,
	[MPD_AccNo] [varchar](16) NULL,
	[MPD_AccPwd] [varchar](16) NULL,
	[MPD_nomTitle] [varchar](50) NULL,
	[MPD_nomName] [varchar](100) NULL,
	[MPD_nomGender] [varchar](10) NULL,
	[MPD_nomRel] [varchar](50) NULL,
	[MPD_nomBdate] [datetime] NULL,
	[MPD_NomAddr] [varchar](50) NULL,
	[MPD_PinSrNo] [varchar](50) NULL,
	[MPD_DDNo] [numeric](18, 0) NULL,
	[MPD_DDDate] [datetime] NULL,
	[MPD_DDBank] [varchar](100) NULL,
	[MPD_DDBranch] [varchar](100) NULL,
	[MPD_DDBankAddr] [varchar](100) NULL,
	[MPD_EpinPwd] [varchar](50) NULL,
	[MPD_EWSpon] [varchar](50) NULL,
	[MPD_EWAccNo] [varchar](50) NULL,
	[MPD_EWTransfer] [decimal](18, 0) NULL,
	[MPD_FirmType] [varchar](50) NULL,
	[MPD_FirmDetail] [varchar](500) NULL,
	[MPD_ReceiptNo] [varchar](50) NOT NULL,
	[MPD_EpinCurrentLogin] [datetime] NULL,
	[MPD_EpinLastLogin] [datetime] NULL,
	[MPD_Extra1] [varchar](100) NULL,
	[MPD_Extra2] [varchar](100) NULL,
	[MPD_Extra3] [varchar](100) NULL,
	[MPD_Extra4] [varchar](100) NULL,
	[MPD_Extra5] [varchar](100) NULL,
	[MPD_Extra6] [varchar](100) NULL,
	[MPD_Extra7] [varchar](100) NULL,
	[MPD_Extra8] [varchar](100) NULL,
	[MPD_Extra9] [varchar](100) NULL,
	[MPD_Extra10] [varchar](100) NULL,
	[MPD_Extra11] [varchar](100) NULL,
	[MPD_Extra12] [varchar](100) NULL,
	[MPD_Extra13] [varchar](100) NULL,
	[MPD_Extra14] [varchar](100) NULL,
	[MPD_Extra15] [varchar](100) NULL,
	[MPD_Extra16] [varchar](100) NULL,
	[MPD_Extra17] [varchar](100) NULL,
	[MPD_Extra18] [varchar](100) NULL,
	[MPD_Extra19] [varchar](100) NULL,
	[MPD_Extra20] [varchar](100) NULL,
	[MPD_Extra21] [varchar](100) NULL,
	[MPD_Extra22] [varchar](100) NULL,
	[MPD_Extra23] [varchar](100) NULL,
	[MPD_Extra24] [varchar](100) NULL,
	[MPD_Extra25] [varchar](100) NULL,
	[MPD_Extra26] [varchar](100) NULL,
	[MPD_Extra27] [varchar](100) NULL,
	[MPD_Extra28] [varchar](100) NULL,
	[MPD_Extra29] [varchar](100) NULL,
	[MPD_Extra30] [varchar](100) NULL,
	[MPD_ReqDate] [datetime] NULL,
	[MPD_StatusFlg] [numeric](18, 0) NULL,
	[MPD_StatusDate] [datetime] NULL,
	[MPD_UHID] [numeric](18, 0) NULL,
	[MPD_ImagePath] [varchar](100) NULL,
	[MPD_PayReceiptMode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemProfileRequest_Dtls] PRIMARY KEY CLUSTERED 
(
	[MPD_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketCategory_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketCategory_Mst](
	[TCM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TCM_Date] [datetime] NOT NULL,
	[TCM_Datetime] [datetime] NOT NULL,
	[TCM_Name] [varchar](200) NOT NULL,
	[TCM_StatusFlg] [tinyint] NOT NULL,
	[TCM_AdminID] [numeric](18, 0) NULL,
	[TCM_IPAddress] [varchar](50) NULL,
	[TCM_Browser] [varchar](50) NULL,
	[TCM_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_TicketCategory_Mst] PRIMARY KEY CLUSTERED 
(
	[TCM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Settings_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings_Mst](
	[SM_ID] [numeric](18, 0) NOT NULL,
	[SM_SrNo] [numeric](18, 0) NULL,
	[SM_Name] [varchar](50) NULL,
	[SM_Desc] [varchar](50) NULL,
	[SM_Value] [numeric](18, 0) NULL,
	[SM_Flag] [numeric](18, 0) NULL,
	[SM_CustomizationFlag] [numeric](18, 0) NULL,
	[SM_decExtra1] [numeric](18, 2) NULL,
	[SM_decExtra2] [numeric](18, 2) NULL,
	[SM_decExtra3] [numeric](18, 2) NULL,
	[SM_decExtra4] [numeric](18, 2) NULL,
	[SM_chvExtra1] [varchar](100) NULL,
	[SM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Settings_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanType_Mst](
	[PM_ID] [numeric](18, 0) NOT NULL,
	[PM_PlanName] [varchar](100) NOT NULL,
	[PM_Power] [numeric](18, 0) NOT NULL,
	[PM_AutoTree] [numeric](18, 0) NOT NULL,
	[PM_StarFlg] [tinyint] NOT NULL,
	[PM_GrDistFlg] [tinyint] NOT NULL,
	[PM_Flag] [tinyint] NOT NULL,
	[PM_decExtra1] [numeric](18, 2) NULL,
	[PM_decExtra2] [numeric](18, 2) NULL,
	[PM_chvExtra3] [varchar](100) NULL,
	[PM_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_Table_1PlanType_Mst] PRIMARY KEY CLUSTERED 
(
	[PM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DateCode_Trn](
	[DCT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DCT_DtCode] [numeric](18, 0) NOT NULL,
	[DCT_StartDate] [datetime] NULL,
	[DCT_EndDate] [datetime] NULL,
	[DCT_Flag] [tinyint] NULL,
	[DCT_confflag] [tinyint] NULL,
	[DCT_Type] [numeric](18, 0) NOT NULL,
	[DCT_Cycle] [numeric](18, 0) NULL,
	[DCT_ShowFlag] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DateCode_Trn_1] PRIMARY KEY CLUSTERED 
(
	[DCT_DtCode] ASC,
	[DCT_Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayCycle_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayCycle_Mst](
	[PCM_srno] [numeric](18, 0) NOT NULL,
	[PCM_cycle] [numeric](18, 0) NOT NULL,
	[PCM_SH] [numeric](18, 0) NOT NULL,
	[PCM_SM] [numeric](18, 0) NOT NULL,
	[PCM_EH] [numeric](18, 0) NOT NULL,
	[PCM_EM] [numeric](18, 0) NOT NULL,
	[PCM_Extra1] [varchar](50) NOT NULL,
	[PCM_Extra2] [varchar](50) NOT NULL,
	[PCM_Extra3] [varchar](50) NOT NULL,
	[PCM_Extra4] [varchar](50) NOT NULL,
	[PCM_Extra5] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IDWiseGenDownline_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IDWiseGenDownline_Dtls](
	[IDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IDD_MEMID] [numeric](18, 0) NOT NULL,
	[IDD_DOWNID] [numeric](18, 0) NOT NULL,
	[IDD_LEVEL] [int] NOT NULL,
 CONSTRAINT [PK_IDWiseGenDownline_Dtls] PRIMARY KEY CLUSTERED 
(
	[IDD_MEMID] ASC,
	[IDD_DOWNID] ASC,
	[IDD_LEVEL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemJoining_Dtls](
	[MJD_MemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MJD_MemNo] [varchar](50) NULL,
	[MJD_DOJ] [smalldatetime] NOT NULL,
	[MJD_DTOJ] [datetime] NOT NULL,
	[MJD_JoinFr] [varchar](50) NULL,
	[MJD_PvCode] [numeric](18, 0) NOT NULL,
	[MJD_PvType] [numeric](18, 0) NOT NULL,
	[MJD_Paymode] [numeric](18, 0) NOT NULL,
	[MJD_PayType] [varchar](50) NULL,
	[MJD_Amount] [numeric](18, 2) NULL,
	[MJD_Multiplecnt] [numeric](18, 0) NOT NULL,
	[MJD_MasterID] [numeric](18, 0) NULL,
	[MJD_Introducer] [numeric](18, 0) NOT NULL,
	[MJD_AdjustedTo] [numeric](18, 0) NOT NULL,
	[MJD_LR] [varchar](50) NOT NULL,
	[MJD_RoyIntroducer] [numeric](18, 0) NULL,
	[MJD_RoyAdjustedTo] [numeric](18, 0) NULL,
	[MJD_RoyLR] [varchar](50) NULL,
	[MJD_CCTrnAmt] [numeric](18, 2) NULL,
	[MJD_TrnTotalAmt] [numeric](18, 2) NULL,
	[MJD_PGCode] [nchar](50) NULL,
	[MJD_BV] [numeric](18, 2) NULL,
	[MJD_PV] [numeric](18, 2) NULL,
	[MJD_pkgBV] [numeric](18, 2) NULL,
	[MJD_pkgPV] [numeric](18, 2) NULL,
	[CustomerId] [numeric](18, 0) NULL,
	[TraderId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemJoining_Dtls] PRIMARY KEY CLUSTERED 
(
	[MJD_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Unique_MJD_Memno] UNIQUE NONCLUSTERED 
(
	[MJD_MemNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ticket_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ticket_Trn](
	[TT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TT_Code] [numeric](18, 0) NOT NULL,
	[TT_MemID] [numeric](18, 0) NOT NULL,
	[TT_Date] [datetime] NOT NULL,
	[TT_Datetime] [datetime] NOT NULL,
	[TT_CategoryID] [numeric](18, 0) NULL,
	[TT_Subject] [varchar](500) NULL,
	[TT_Message] [varchar](8000) NULL,
	[TT_Priority] [tinyint] NOT NULL,
	[TT_StatusFlg] [tinyint] NOT NULL,
	[TT_ReadFlg] [tinyint] NOT NULL,
	[TT_DeleteFlg] [tinyint] NOT NULL,
	[TCM_IPAddress] [varchar](50) NULL,
	[TCM_Browser] [varchar](50) NULL,
	[TCM_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_Ticket_Trn] PRIMARY KEY CLUSTERED 
(
	[TT_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketReply_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketReply_Trn](
	[TRT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TRT_Code] [numeric](18, 0) NOT NULL,
	[TRT_AdminID] [numeric](18, 0) NOT NULL,
	[TRT_Date] [datetime] NOT NULL,
	[TRT_Datetime] [datetime] NOT NULL,
	[TRT_Message] [varchar](8000) NOT NULL,
	[TRT_StatusFlg] [tinyint] NOT NULL,
	[TCM_IPAddress] [varchar](50) NULL,
	[TCM_Browser] [varchar](50) NULL,
	[TCM_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_TicketReply_Trn] PRIMARY KEY CLUSTERED 
(
	[TRT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Repurchase_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Repurchase_Hdr](
	[RH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RH_MemID] [numeric](18, 0) NOT NULL,
	[RH_Code] [varchar](15) NULL,
	[RH_Date] [datetime] NOT NULL,
	[RH_Remark] [varchar](400) NULL,
	[RH_TotQty] [numeric](18, 0) NOT NULL,
	[RH_TotRcvdQty] [numeric](18, 0) NOT NULL,
	[RH_TotGrossAmt] [numeric](18, 2) NOT NULL,
	[RH_DiscPer] [numeric](18, 2) NOT NULL,
	[RH_TotDiscAmt] [numeric](18, 2) NOT NULL,
	[RH_TotNetAmt] [numeric](18, 2) NOT NULL,
	[RH_Status] [tinyint] NOT NULL,
	[RH_Paymode] [numeric](18, 0) NOT NULL,
	[RH_PayDate] [datetime] NULL,
	[RH_PayNo] [numeric](18, 0) NULL,
	[RH_PayBank] [varchar](50) NOT NULL,
	[RH_PayBranch] [varchar](50) NOT NULL,
	[RH_IFSC] [varchar](50) NULL,
	[RH_MICR] [numeric](18, 0) NOT NULL,
	[RH_PinSrNo] [numeric](18, 0) NOT NULL,
	[RH_EWSponID] [numeric](18, 0) NOT NULL,
	[RH_CRDate] [datetime] NOT NULL,
	[RH_decEXTRA1] [numeric](18, 2) NULL,
	[RH_decEXTRA2] [numeric](18, 2) NULL,
	[RH_chvEXTRA3] [varchar](100) NULL,
	[RH_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_Repurchase_Hdr] PRIMARY KEY CLUSTERED 
(
	[RH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Repurchase_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Repurchase_Dtls](
	[RD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RD_RHID] [numeric](18, 0) NOT NULL,
	[RD_PVCode] [numeric](18, 0) NULL,
	[RD_Qty] [numeric](18, 0) NOT NULL,
	[RD_GrossAmt] [numeric](18, 2) NOT NULL,
	[RD_DiscAmt] [numeric](18, 2) NOT NULL,
	[RD_NetAmt] [numeric](18, 2) NOT NULL,
	[RD_WebFlag] [tinyint] NOT NULL,
	[RD_RcvdQty] [numeric](18, 0) NOT NULL,
	[RD_decEXTRA1] [numeric](18, 2) NULL,
	[RD_decEXTRA2] [numeric](18, 2) NULL,
	[RD_chvEXTRA3] [varchar](100) NULL,
	[RD_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_Repurchase_Dtls] PRIMARY KEY CLUSTERED 
(
	[RD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempDownline]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempDownline](
	[TD_MemID] [numeric](18, 0) NOT NULL,
	[TD_DownID] [numeric](18, 0) NOT NULL,
	[TD_Side] [char](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempBVVoucher]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempBVVoucher](
	[TBVMemid] [numeric](18, 0) NOT NULL,
	[TBVLeftID] [varchar](15) NULL,
	[TBVRIGHTID] [varchar](15) NULL,
	[TBVlname] [varchar](100) NULL,
	[TBVRname] [varchar](100) NULL,
	[RepS] [numeric](18, 0) NOT NULL,
	[RepL] [numeric](18, 0) NOT NULL,
	[RepR] [numeric](18, 0) NOT NULL,
	[BVS] [numeric](18, 0) NOT NULL,
	[BVL] [numeric](18, 0) NOT NULL,
	[BVR] [numeric](18, 0) NOT NULL,
	[TOTBVS] [numeric](18, 0) NOT NULL,
	[TOTBVL] [numeric](18, 0) NOT NULL,
	[TOTBVR] [numeric](18, 0) NOT NULL,
	[PPCS] [numeric](18, 2) NOT NULL,
	[PPCL] [numeric](18, 2) NOT NULL,
	[PPCR] [numeric](18, 2) NOT NULL,
	[LDS] [numeric](18, 2) NOT NULL,
	[LDL] [numeric](18, 0) NOT NULL,
	[LDR] [numeric](18, 0) NOT NULL,
	[Extra1] [varchar](100) NULL,
	[Extra2] [varchar](100) NULL,
	[Extra3] [varchar](100) NULL,
	[Extra4] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempBinary_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempBinary_Trn](
	[TB_Memid] [numeric](18, 0) NOT NULL,
	[TB_Sets] [numeric](18, 0) NOT NULL,
	[TB_LeftChCnt] [numeric](18, 0) NOT NULL,
	[TB_RightChCnt] [numeric](18, 0) NOT NULL,
	[TB_LeftR] [numeric](18, 0) NOT NULL,
	[TB_RightR] [numeric](18, 0) NOT NULL,
	[TB_Amt] [numeric](18, 2) NOT NULL,
	[TB_Amt1] [numeric](18, 2) NULL,
	[TB_Amt2] [numeric](18, 2) NULL,
	[TB_Binset] [numeric](18, 0) NOT NULL,
	[TB_Hikeset] [numeric](18, 0) NOT NULL,
	[TB_Goldcoin] [numeric](18, 0) NOT NULL,
	[TB_Lapspair] [numeric](18, 0) NOT NULL,
	[TB_Extra1] [numeric](18, 2) NULL,
	[TB_Extra2] [numeric](18, 2) NULL,
	[TB_Extra3] [numeric](18, 0) NULL,
	[TB_Extra4] [numeric](18, 2) NULL,
	[TB_Extra5] [numeric](18, 2) NULL,
	[TB_Extra6] [numeric](18, 2) NULL,
	[TB_Extra7] [numeric](18, 2) NULL,
	[TB_Extra8] [numeric](18, 0) NULL,
	[TB_Extra9] [numeric](18, 2) NULL,
	[TB_Extra10] [numeric](18, 2) NULL,
 CONSTRAINT [PK_TempBinary_Trn] PRIMARY KEY NONCLUSTERED 
(
	[TB_Memid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SMSBlacklist_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMSBlacklist_Trn](
	[ST_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ST_MJDMemId] [numeric](18, 0) NOT NULL,
	[ST_Date] [datetime] NULL,
	[ST_Flag] [tinyint] NULL,
	[ST_Remarks] [varchar](50) NULL,
 CONSTRAINT [PK_SMSBlacklist_Trn] PRIMARY KEY CLUSTERED 
(
	[ST_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Royalty_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Royalty_Trn](
	[Roy_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Roy_Memid] [numeric](18, 0) NOT NULL,
	[Roy_Srno] [numeric](18, 0) NOT NULL,
	[Roy_Date] [datetime] NOT NULL,
	[Roy_Webflag] [tinyint] NOT NULL,
	[Roy_NoOfTimes] [varchar](100) NOT NULL,
	[Roy_decEXTRA1] [numeric](18, 2) NULL,
	[Roy_decEXTRA2] [numeric](18, 2) NULL,
	[Roy_chvEXTRA3] [varchar](300) NULL,
	[Roy_chvEXTRA4] [varchar](50) NULL,
	[Roy_Datetime] [datetime] NULL,
	[Roy_DwnMemId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Royalty_Trn] PRIMARY KEY CLUSTERED 
(
	[Roy_Memid] ASC,
	[Roy_Srno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RepurchaseSlab_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RepurchaseSlab_Trn](
	[RST_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RST_MemID] [numeric](18, 0) NOT NULL,
	[RST_SrNo] [numeric](18, 0) NOT NULL,
	[RST_Date] [datetime] NOT NULL,
	[RST_Flag] [tinyint] NOT NULL,
	[RD_decEXTRA1] [numeric](18, 2) NULL,
	[RD_decEXTRA2] [numeric](18, 2) NULL,
	[RD_chvEXTRA3] [varchar](100) NULL,
	[RD_chvEXTRA4] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Repurchase_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Repurchase_Trn](
	[RT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RT_MemID] [numeric](18, 0) NOT NULL,
	[RT_TransID] [varchar](50) NULL,
	[RT_Date] [datetime] NULL,
	[RT_Remark] [varchar](500) NULL,
	[RT_PvCode] [numeric](18, 0) NOT NULL,
	[RT_PvPts] [numeric](18, 6) NULL,
	[RT_PvAU] [numeric](18, 6) NULL,
	[RT_PvAP] [numeric](18, 0) NULL,
	[RT_Qty] [numeric](18, 0) NULL,
	[RT_Amount] [numeric](18, 2) NULL,
	[RT_PMID] [numeric](18, 0) NOT NULL,
	[RT_PinSrNo] [varchar](50) NULL,
	[RT_DDNo] [varchar](50) NULL,
	[RT_DDDate] [datetime] NULL,
	[RT_DDBank] [varchar](50) NULL,
	[RT_DDBranch] [varchar](50) NULL,
	[RT_AppnNo] [varchar](50) NULL,
	[RT_Name] [varchar](200) NULL,
	[RT_Address] [varchar](300) NULL,
	[RT_City] [varchar](50) NULL,
	[RT_State] [varchar](50) NULL,
	[RT_Pincode] [varchar](20) NULL,
	[RT_PhoneNo] [varchar](20) NULL,
	[RT_MobileNo] [varchar](20) NULL,
	[RT_AcceptFlg] [tinyint] NULL,
	[RT_AcceptDate] [datetime] NULL,
	[RT_RejectFlg] [tinyint] NULL,
	[RT_RejectDate] [datetime] NULL,
	[RT_ConfirmFlg] [tinyint] NULL,
	[RT_ConfirmDate] [datetime] NULL,
	[RT_DispatchFlg] [tinyint] NULL,
	[RT_DispatchMode] [varchar](100) NULL,
	[RT_DispatchDate] [datetime] NULL,
	[RT_LRNo] [varchar](100) NULL,
	[RT_DispatchRemarks] [varchar](500) NULL,
	[RT_chvExtra1] [varchar](200) NULL,
	[RT_chvExtra2] [varchar](200) NULL,
	[RT_chvExtra3] [varchar](200) NULL,
	[RT_chvExtra4] [varchar](200) NULL,
	[RT_dblExtra5] [numeric](18, 2) NULL,
	[RT_dblExtra6] [numeric](18, 2) NULL,
	[RT_dblExtra7] [numeric](18, 2) NULL,
	[RT_dblExtra8] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Repurchase_Trn] PRIMARY KEY CLUSTERED 
(
	[RT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductDel_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductDel_Trn](
	[PT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PT_MemID] [numeric](18, 0) NOT NULL,
	[PT_ProdCode] [numeric](18, 0) NOT NULL,
	[PT_DelDate] [datetime] NOT NULL,
	[PT_DMID] [numeric](18, 0) NULL,
	[PT_Remark] [varchar](400) NULL,
	[PT_WebFlag] [tinyint] NOT NULL,
	[PT_AcFlag] [tinyint] NOT NULL,
	[PT_ACStateCode] [numeric](18, 0) NOT NULL,
	[PT_Frid] [numeric](18, 0) NOT NULL,
	[PT_datecode] [numeric](18, 0) NOT NULL,
	[PT_decEXTRA1] [numeric](18, 2) NULL,
	[PT_decEXTRA2] [numeric](18, 2) NULL,
	[PT_chvEXTRA3] [varchar](100) NULL,
	[PT_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_ProductDel_Trn] PRIMARY KEY CLUSTERED 
(
	[PT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaidID_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaidID_Trn](
	[PI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PI_MemID] [numeric](18, 0) NOT NULL,
	[PI_date] [datetime] NOT NULL,
	[PI_Webflag] [tinyint] NULL,
	[PI_remark] [varchar](300) NULL,
	[PI_paymode] [varchar](100) NULL,
	[PI_balance] [numeric](18, 2) NULL,
	[PI_decExtra1] [numeric](18, 2) NULL,
	[PI_decExtra2] [numeric](18, 2) NULL,
	[PI_chvExtra3] [varchar](100) NULL,
	[PI_chvExtra4] [varchar](100) NULL,
	[PI_Datetime] [datetime] NULL,
	[PI_AdminID] [numeric](18, 0) NULL,
	[PI_IPAddress] [varchar](50) NULL,
	[PI_Browser] [varchar](50) NULL,
	[PI_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_PaidID_Trn] PRIMARY KEY CLUSTERED 
(
	[PI_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferTempQual_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferTempQual_Trn](
	[OT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OT_Memid] [numeric](18, 0) NOT NULL,
	[Ot_scid] [numeric](18, 0) NOT NULL,
	[OT_OHID] [numeric](18, 0) NOT NULL,
	[OT_TotLCnt] [numeric](18, 0) NOT NULL,
	[OT_TotRCnt] [numeric](18, 0) NULL,
	[OT_TotLBV] [numeric](18, 2) NULL,
	[OT_TotRBV] [numeric](18, 2) NULL,
	[OT_TotLPV] [numeric](18, 0) NULL,
	[OT_TotRPV] [numeric](18, 0) NULL,
	[OT_TotDirect] [numeric](18, 0) NULL,
	[OT_package] [numeric](18, 0) NULL,
	[OT_Chvextra1] [varchar](50) NULL,
	[OT_Chvextra2] [varchar](50) NULL,
	[OT_Chvextra3] [varchar](50) NULL,
	[OT_Chvextra4] [varchar](50) NULL,
	[OT_Chvextra5] [varchar](50) NULL,
	[OT_Chvextra6] [varchar](50) NULL,
	[OT_LeftDirect] [numeric](18, 0) NULL,
	[OT_RightDirect] [numeric](18, 0) NULL,
	[OT_LeftSale] [numeric](18, 2) NULL,
	[OT_RightSale] [numeric](18, 2) NULL,
 CONSTRAINT [PK_OfferTempQual_Trn] PRIMARY KEY CLUSTERED 
(
	[OT_Memid] ASC,
	[Ot_scid] ASC,
	[OT_OHID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferAchiever_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferAchiever_Trn](
	[OAT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OAT_MemID] [numeric](18, 0) NOT NULL,
	[OAT_serialno] [numeric](18, 0) NOT NULL,
	[OAT_AchvDt] [datetime] NOT NULL,
	[OAT_Status] [tinyint] NOT NULL,
	[OAT_Webflag] [tinyint] NULL,
	[OAT_Flag] [tinyint] NOT NULL,
	[OAT_ReqLcnt] [numeric](18, 0) NULL,
	[OAT_ReqRcnt] [numeric](18, 0) NULL,
	[OAT_ReqLBV] [numeric](18, 2) NULL,
	[OAT_ReqRBV] [numeric](18, 2) NULL,
	[OAT_ReqLPV] [numeric](18, 2) NULL,
	[OAT_ReqRPV] [numeric](18, 2) NULL,
	[OAT_Reqdirects] [numeric](18, 0) NULL,
	[OAT_decExtra1] [numeric](18, 2) NULL,
	[OAT_decExtra2] [numeric](18, 2) NULL,
	[OAT_chvExtra3] [varchar](100) NULL,
	[OAT_chvExtra4] [varchar](300) NULL,
	[OAT_dtmExtra5] [datetime] NULL,
	[OAT_DownMemID] [numeric](18, 0) NULL,
	[OAT_ReqLDirect] [numeric](18, 0) NULL,
	[OAT_ReqRDirect] [numeric](18, 0) NULL,
	[OAT_ReqLSale] [numeric](18, 2) NULL,
	[OAT_ReqRSale] [numeric](18, 2) NULL,
	[OAT_ShowFlag] [tinyint] NULL,
 CONSTRAINT [PK_OfferAchiever_Trn] PRIMARY KEY CLUSTERED 
(
	[OAT_MemID] ASC,
	[OAT_serialno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notification_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notification_Trn](
	[NT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NT_MemID] [numeric](18, 0) NOT NULL,
	[NT_DateTime] [datetime] NOT NULL,
	[NT_Notification] [varchar](500) NOT NULL,
	[NT_ReadDtm] [datetime] NULL,
	[NT_AdminID] [numeric](18, 0) NULL,
	[NT_IsGrowl] [tinyint] NOT NULL,
	[NT_ScheduleDate] [datetime] NULL,
	[NT_DeleteFlag] [tinyint] NOT NULL,
 CONSTRAINT [PK_Notification_Trn] PRIMARY KEY CLUSTERED 
(
	[NT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemSpill_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemSpill_Dtls](
	[MSD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MSD_Memid] [numeric](18, 0) NOT NULL,
	[MSD_SpillID] [numeric](18, 0) NOT NULL,
	[MSD_Date] [datetime] NOT NULL,
	[MSD_Flag] [tinyint] NOT NULL,
	[MSD_Type] [numeric](18, 0) NOT NULL,
	[MSD_PvCode] [numeric](18, 0) NOT NULL,
	[MSD_Amt] [numeric](18, 0) NOT NULL,
	[MSD_Datecode] [numeric](18, 0) NULL,
	[MSD_SpillNAmt] [numeric](18, 0) NULL,
	[MSD_SpillNDatecode] [numeric](18, 0) NULL,
	[MSD_CycleSpill] [numeric](18, 0) NULL,
	[MSD_CycleSpillNetwork] [numeric](18, 0) NULL,
	[MSD_JoiningCycle] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemSpill_Dtls] PRIMARY KEY CLUSTERED 
(
	[MSD_Memid] ASC,
	[MSD_SpillID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemLogin_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemLogin_Dtls](
	[MLD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLD_MemID] [numeric](18, 0) NOT NULL,
	[MLD_Login] [varchar](50) NULL,
	[MLD_pwd] [varchar](50) NULL,
	[MLD_Theme] [varchar](max) NULL,
	[MLD_CurrentLogin] [datetime] NULL,
	[MLD_LastLogin] [datetime] NULL,
	[MLD_HQuestion] [varchar](100) NULL,
	[MLD_HAnswer] [varchar](100) NULL,
	[MLD_decEXTRA1] [numeric](18, 2) NULL,
	[MLD_decEXTRA2] [numeric](18, 2) NULL,
	[MLD_chvEXTRA3] [varchar](50) NULL,
	[MLD_chvEXTRA4] [varchar](50) NULL,
	[MLD_CurrentLoginIP] [varchar](50) NULL,
	[MLD_LastLoginIP] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemDownline_Dtls_M2web]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemDownline_Dtls_M2web](
	[MDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MDD_MemID] [numeric](18, 0) NOT NULL,
	[MDD_PVCode] [numeric](18, 0) NOT NULL,
	[MDD_Level] [numeric](18, 0) NOT NULL,
	[MDD_PvType] [numeric](18, 0) NOT NULL,
	[MDD_Date] [datetime] NOT NULL,
	[MDD_LeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_RightCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfLeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfRightCnt] [numeric](18, 0) NOT NULL,
	[MDD_LeftBV] [numeric](18, 2) NULL,
	[MDD_RightBV] [numeric](18, 2) NULL,
	[MDD_ConfLeftBV] [numeric](18, 2) NULL,
	[MDD_ConfRightBV] [numeric](18, 2) NULL,
	[MDD_LeftPV] [numeric](18, 2) NULL,
	[MDD_RightPV] [numeric](18, 2) NULL,
	[MDD_ConfLeftPV] [numeric](18, 2) NULL,
	[MDD_ConfRightPV] [numeric](18, 2) NULL,
	[MDD_OwnBV] [numeric](18, 2) NULL,
	[MDD_ConfOwnBV] [numeric](18, 2) NULL,
	[MDD_OwnPV] [numeric](18, 2) NULL,
	[MDD_ConfOwnPV] [numeric](18, 2) NULL,
	[MDD_LeftID] [varchar](max) NULL,
	[MDD_RightID] [varchar](max) NULL,
	[MDD_decExtra1] [numeric](18, 2) NULL,
	[MDD_decExtra2] [numeric](18, 2) NULL,
	[MDD_decExtra3] [numeric](18, 2) NULL,
	[MDD_decExtra4] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftAP] [numeric](18, 2) NULL,
	[MDD_RejectRightAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectLeftAU] [numeric](18, 2) NULL,
	[MDD_RejectRightAU] [numeric](18, 2) NULL,
	[MDD_decExtra5] [numeric](18, 2) NULL,
	[MDD_decExtra6] [numeric](18, 2) NULL,
	[MDD_decExtra7] [numeric](18, 2) NULL,
	[MDD_decExtra8] [numeric](18, 2) NULL,
	[MDD_chvExtra9] [varchar](5000) NULL,
	[MDD_chvExtra10] [varchar](5000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemDownline_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemDownline_Dtls](
	[MDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MDD_MemID] [numeric](18, 0) NOT NULL,
	[MDD_PVCode] [numeric](18, 0) NOT NULL,
	[MDD_Level] [numeric](18, 0) NOT NULL,
	[MDD_PvType] [numeric](18, 0) NOT NULL,
	[MDD_Date] [datetime] NOT NULL,
	[MDD_LeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_RightCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfLeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfRightCnt] [numeric](18, 0) NOT NULL,
	[MDD_LeftBV] [numeric](18, 2) NULL,
	[MDD_RightBV] [numeric](18, 2) NULL,
	[MDD_ConfLeftBV] [numeric](18, 2) NULL,
	[MDD_ConfRightBV] [numeric](18, 2) NULL,
	[MDD_LeftPV] [numeric](18, 2) NULL,
	[MDD_RightPV] [numeric](18, 2) NULL,
	[MDD_ConfLeftPV] [numeric](18, 2) NULL,
	[MDD_ConfRightPV] [numeric](18, 2) NULL,
	[MDD_LeftSale] [numeric](18, 2) NULL,
	[MDD_RightSale] [numeric](18, 2) NULL,
	[MDD_ConfLeftSale] [numeric](18, 2) NULL,
	[MDD_ConfRightSale] [numeric](18, 2) NULL,
	[MDD_OwnBV] [numeric](18, 2) NULL,
	[MDD_ConfOwnBV] [numeric](18, 2) NULL,
	[MDD_OwnPV] [numeric](18, 2) NULL,
	[MDD_ConfOwnPV] [numeric](18, 2) NULL,
	[MDD_LeftID] [varchar](max) NULL,
	[MDD_RightID] [varchar](max) NULL,
	[MDD_decExtra1] [numeric](18, 2) NOT NULL,
	[MDD_decExtra2] [numeric](18, 2) NULL,
	[MDD_decExtra3] [numeric](18, 2) NULL,
	[MDD_decExtra4] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftAP] [numeric](18, 2) NULL,
	[MDD_RejectRightAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectLeftAU] [numeric](18, 2) NULL,
	[MDD_RejectRightAU] [numeric](18, 2) NULL,
	[MDD_decExtra5] [numeric](18, 2) NULL,
	[MDD_decExtra6] [numeric](18, 2) NULL,
	[MDD_decExtra7] [numeric](18, 2) NULL,
	[MDD_decExtra8] [numeric](18, 2) NULL,
	[MDD_chvExtra9] [varchar](5000) NULL,
	[MDD_chvExtra10] [varchar](5000) NULL,
	[MDD_OwnSale] numeric(18,2),
	[MDD_ConfOwnSale] numeric(18,2),
	--[MDD_confownsale] [numeric](18, 2) NULL,
 CONSTRAINT [PK_MemDownline_Dtls] PRIMARY KEY CLUSTERED 
(
	[MDD_MemID] ASC,
	[MDD_PVCode] ASC,
	[MDD_Level] ASC,
	[MDD_PvType] ASC,
	[MDD_Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemDirects_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemDirects_Dtls](
	[MDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MDD_DateCode] [numeric](18, 0) NULL,
	[MDD_Date] [datetime] NOT NULL,
	[MDD_MemID] [numeric](18, 0) NOT NULL,
	[MDD_DirectID] [numeric](18, 0) NOT NULL,
	[MDD_Amt] [numeric](18, 2) NOT NULL,
	[MDD_PvCode] [numeric](18, 0) NOT NULL,
	[MDD_DSrNo] [numeric](18, 0) NOT NULL,
	[MDD_Flag] [tinyint] NOT NULL,
	[MDD_decEXTRA1] [numeric](18, 2) NULL,
	[MDD_decEXTRA2] [numeric](18, 2) NULL,
	[MDD_chvEXTRA3] [varchar](50) NULL,
	[MDD_chvEXTRA4] [varchar](50) NULL,
	[MDD_Cycle] [numeric](18, 0) NULL,
	[MDD_JoiningCycle] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemDirects_Dtls] PRIMARY KEY CLUSTERED 
(
	[MDD_MemID] ASC,
	[MDD_DirectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemActDownline_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemActDownline_Dtls](
	[MDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MDD_MemID] [numeric](18, 0) NOT NULL,
	[MDD_PVCode] [numeric](18, 0) NOT NULL,
	[MDD_Level] [numeric](18, 0) NOT NULL,
	[MDD_PvType] [numeric](18, 0) NOT NULL,
	[MDD_Date] [datetime] NOT NULL,
	[MDD_LeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_RightCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfLeftCnt] [numeric](18, 0) NOT NULL,
	[MDD_ConfRightCnt] [numeric](18, 0) NOT NULL,
	[MDD_LeftBV] [numeric](18, 2) NULL,
	[MDD_RightBV] [numeric](18, 2) NULL,
	[MDD_ConfLeftBV] [numeric](18, 2) NULL,
	[MDD_ConfRightBV] [numeric](18, 2) NULL,
	[MDD_LeftPV] [numeric](18, 2) NULL,
	[MDD_RightPV] [numeric](18, 2) NULL,
	[MDD_ConfLeftPV] [numeric](18, 2) NULL,
	[MDD_ConfRightPV] [numeric](18, 2) NULL,
	[MDD_LeftSale] [numeric](18, 2) NULL,
	[MDD_RightSale] [numeric](18, 2) NULL,
	[MDD_ConfLeftSale] [numeric](18, 2) NULL,
	[MDD_ConfRightSale] [numeric](18, 2) NULL,
	[MDD_OwnBV] [numeric](18, 2) NULL,
	[MDD_ConfOwnBV] [numeric](18, 2) NULL,
	[MDD_OwnPV] [numeric](18, 2) NULL,
	[MDD_ConfOwnPV] [numeric](18, 2) NULL,
	[MDD_LeftID] [varchar](max) NULL,
	[MDD_RightID] [varchar](max) NULL,
	[MDD_decExtra1] [numeric](18, 2) NOT NULL,
	[MDD_decExtra2] [numeric](18, 2) NULL,
	[MDD_decExtra3] [numeric](18, 2) NULL,
	[MDD_decExtra4] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftAP] [numeric](18, 2) NULL,
	[MDD_RejectRightAP] [numeric](18, 2) NULL,
	[MDD_RejectLeftOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectRightOwnAU] [numeric](18, 2) NULL,
	[MDD_RejectLeftAU] [numeric](18, 2) NULL,
	[MDD_RejectRightAU] [numeric](18, 2) NULL,
	[MDD_decExtra5] [numeric](18, 2) NULL,
	[MDD_decExtra6] [numeric](18, 2) NULL,
	[MDD_decExtra7] [numeric](18, 2) NULL,
	[MDD_decExtra8] [numeric](18, 2) NULL,
	[MDD_chvExtra9] [varchar](5000) NULL,
	[MDD_chvExtra10] [varchar](5000) NULL,
	[MDD_OwnSale] numeric(18,2),
	[MDD_ConfOwnSale] numeric(18,2),
	--[MDD_confownsale] [numeric](18, 2) NULL,
 CONSTRAINT [PK_MemActDownline_Dtls] PRIMARY KEY CLUSTERED 
(
	[MDD_MemID] ASC,
	[MDD_PVCode] ASC,
	[MDD_Level] ASC,
	[MDD_PvType] ASC,
	[MDD_Date] ASC,
	[MDD_decExtra1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IDWiseDownline_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IDWiseDownline_Dtls](
	[IDD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IDD_MEMID] [numeric](18, 0) NOT NULL,
	[IDD_DOWNID] [numeric](18, 0) NOT NULL,
	[IDD_LEVEL] [int] NOT NULL,
	[IDD_LR] [tinyint] NOT NULL,
 CONSTRAINT [PK_IDWiseDownline_Dtls] PRIMARY KEY CLUSTERED 
(
	[IDD_MEMID] ASC,
	[IDD_DOWNID] ASC,
	[IDD_LEVEL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ForgotPwd_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ForgotPwd_Trn](
	[fp_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[fp_MemID] [numeric](18, 0) NULL,
	[fp_question] [varchar](100) NULL,
	[fp_answer] [varchar](200) NULL,
	[fp_decExtra1] [numeric](18, 2) NULL,
	[fp_chvExtra2] [varchar](100) NULL,
	[fp_chvExtra3] [varchar](100) NULL,
 CONSTRAINT [PK_ForgotPwd_Trn] PRIMARY KEY CLUSTERED 
(
	[fp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeedBack_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeedBack_Trn](
	[FT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FT_MemID] [numeric](18, 0) NULL,
	[FT_Name] [varchar](300) NOT NULL,
	[FT_City] [varchar](100) NOT NULL,
	[ft_Email] [varchar](100) NULL,
	[FT_CntNo] [varchar](50) NULL,
	[FT_Date] [datetime] NOT NULL,
	[FT_Subject] [varchar](100) NULL,
	[FT_Desc] [varchar](8000) NULL,
	[FT_WebFlag] [tinyint] NOT NULL,
	[FT_decextra1] [numeric](18, 2) NULL,
	[FT_chvextra2] [varchar](300) NULL,
	[FT_chvextra3] [varchar](300) NULL,
 CONSTRAINT [PK_FeedBack_Trn] PRIMARY KEY CLUSTERED 
(
	[FT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feder_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feder_Trn](
	[FT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FT_FID] [numeric](18, 0) NOT NULL,
	[FT_MemID] [numeric](18, 0) NULL,
	[FT_Srno] [numeric](18, 0) NULL,
	[FT_Status] [tinyint] NULL,
	[FT_Count] [numeric](18, 0) NULL,
	[FT_Pts] [numeric](18, 2) NULL,
	[FT_time1] [datetime] NULL,
	[FT_time2] [datetime] NULL,
	[FT_Priority] [numeric](18, 0) NULL,
	[FT_parentid] [numeric](18, 0) NULL,
	[FT_date] [datetime] NULL,
	[FT_decExtra1] [numeric](18, 2) NULL,
	[FT_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Feder_Trn] PRIMARY KEY CLUSTERED 
(
	[FT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyID_mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyID_mst_log](
	[CM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CM_Memid] [numeric](18, 0) NOT NULL,
	[CM_CurrSponFlg] [tinyint] NULL,
	[CM_AutoFlg] [tinyint] NULL,
	[CM_SMSFlg] [tinyint] NULL,
	[CM_Date] [datetime] NULL,
	[CM_IncFlg] [tinyint] NULL,
	[CM_chvExtra1] [varchar](100) NULL,
	[CM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_CompanyID_mst_log] PRIMARY KEY CLUSTERED 
(
	[CM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyID_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyID_Mst](
	[CM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CM_Memid] [numeric](18, 0) NOT NULL,
	[CM_CurrSponFlg] [tinyint] NULL,
	[CM_AutoFlg] [tinyint] NULL,
	[CM_SMSFlg] [tinyint] NULL,
	[CM_Date] [datetime] NULL,
	[CM_IncFlg] [tinyint] NULL,
	[CM_chvExtra1] [varchar](100) NULL,
	[CM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_CompanyID_Mst_1] PRIMARY KEY CLUSTERED 
(
	[CM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment_Hdr](
	[CH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CH_MemID] [numeric](18, 0) NOT NULL,
	[CH_Date] [datetime] NULL,
	[CH_Datetime] [datetime] NULL,
	[CH_Title] [varchar](200) NULL,
	[CH_Post] [varchar](1000) NULL,
	[CH_PhotoPath] [varchar](200) NULL,
	[CH_Flag] [tinyint] NULL,
	[CH_chvExtra1] [varchar](100) NULL,
	[CH_chvExtra2] [varchar](100) NULL,
	[CH_decExtra3] [numeric](18, 2) NULL,
	[CH_LastUpdatedThoughts] [datetime] NULL,
 CONSTRAINT [PK_Comment_Hdr] PRIMARY KEY CLUSTERED 
(
	[CH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment_Dtls](
	[CD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CD_CHID] [numeric](18, 0) NOT NULL,
	[CD_MemID] [numeric](18, 0) NOT NULL,
	[CD_Date] [datetime] NULL,
	[CD_Datetime] [datetime] NULL,
	[CD_Comment] [varchar](1000) NULL,
	[CD_Flag] [tinyint] NULL,
	[CD_chvExtra1] [varchar](600) NULL,
	[CD_chvExtra2] [varchar](600) NULL,
	[CD_decExtra3] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Comment_Dtls] PRIMARY KEY CLUSTERED 
(
	[CD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChequePrint_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChequePrint_Trn](
	[CPT_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CPT_Datecode] [numeric](18, 0) NULL,
	[CPT_Type] [numeric](18, 0) NULL,
	[CPT_CheqPayTo] [varchar](150) NULL,
	[CPT_MemID] [numeric](18, 0) NULL,
	[CPT_AmtInwords] [varchar](200) NULL,
	[CPT_AmtInFig] [numeric](18, 2) NULL,
	[CPT_CheqNo] [numeric](18, 0) NULL,
	[CPT_CheqDate] [datetime] NULL,
	[CPT_BankName] [varchar](150) NULL,
	[CPT_WebFlag] [tinyint] NULL,
	[CPT_Remark] [varchar](250) NULL,
	[CPT_SrNo] [numeric](18, 0) NULL,
	[CPT_ITID] [numeric](18, 0) NULL,
	[CPT_EndDtcode] [numeric](18, 0) NULL,
	[CPT_decExtra1] [numeric](18, 2) NULL,
	[CPT_decExtra2] [numeric](18, 2) NULL,
	[CPT_chvExtra3] [varchar](100) NULL,
	[CPT_chvExtra4] [varchar](100) NULL,
	[CustomerId] [varchar](100) NULL,
	[WithdrawlId] int
 CONSTRAINT [PK_ChequePrint_Trn] PRIMARY KEY CLUSTERED 
(
	[CPT_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AutoFrontLine_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoFrontLine_Trn](
	[AT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AT_Memid] [numeric](18, 0) NULL,
	[AT_Code] [varchar](20) NULL,
	[AT_Sponno] [varchar](20) NULL,
	[AT_Date] [datetime] NULL,
	[AT_ParentTrk] [varchar](100) NULL,
	[AT_DirectChTrk] [varchar](100) NULL,
	[AT_Lvl1cnt] [numeric](18, 0) NULL,
	[AT_Lvl2cnt] [numeric](18, 0) NULL,
	[AT_lvl3cnt] [numeric](18, 0) NULL,
	[AT_decExtra1] [numeric](18, 2) NULL,
	[AT_decExtra2] [numeric](18, 2) NULL,
	[AT_chvExtra3] [varchar](100) NULL,
	[AT_chvExtra4] [varchar](100) NULL,
	[AT_rentryflg] [tinyint] NULL,
	[AT_Freeflag] [tinyint] NULL,
 CONSTRAINT [PK_AutoFrontLine_Trn] PRIMARY KEY CLUSTERED 
(
	[AT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemFrInc_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemFrInc_trn](
	[MFI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MFI_Datecode] [numeric](18, 0) NOT NULL,
	[MFI_Memid] [numeric](18, 0) NOT NULL,
	[MFI_TotJoining] [numeric](18, 0) NOT NULL,
	[MFI_TotBV] [numeric](18, 2) NOT NULL,
	[MFI_JoiningAmt] [numeric](18, 2) NOT NULL,
	[MFI_BVAmt] [numeric](18, 2) NOT NULL,
	[MFI_Extra1] [numeric](18, 2) NULL,
	[MFI_Extra2] [numeric](18, 2) NULL,
	[MFI_Extra3] [numeric](18, 2) NULL,
	[MFI_Extra4] [numeric](18, 2) NULL,
 CONSTRAINT [PK_MemFrInc_Dtls] PRIMARY KEY CLUSTERED 
(
	[MFI_Datecode] ASC,
	[MFI_Memid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemFr_dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemFr_dtls](
	[MFR_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MFR_Datecode] [numeric](18, 0) NOT NULL,
	[MFR_Memid] [numeric](18, 0) NOT NULL,
	[MFR_Frid] [numeric](18, 0) NOT NULL,
	[MFR_Joining] [numeric](18, 0) NOT NULL,
	[MFR_Bv] [numeric](18, 2) NOT NULL,
	[MFR_JoiningAmt] [numeric](18, 2) NOT NULL,
	[MFR_BVAmt] [numeric](18, 2) NOT NULL,
	[MFR_Extra1] [numeric](18, 0) NULL,
	[MFR_Extra2] [numeric](18, 0) NULL,
	[MFR_Extra3] [varchar](150) NULL,
	[MFR_Extra4] [varchar](150) NULL,
 CONSTRAINT [PK_MemFr_dtls] PRIMARY KEY CLUSTERED 
(
	[MFR_Datecode] ASC,
	[MFR_Memid] ASC,
	[MFR_Frid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemRep_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemRep_Trn](
	[MRT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MRT_Desc] [varchar](500) NULL,
	[MRT_Field] [varchar](500) NULL,
	[MRT_decExtra1] [numeric](18, 2) NULL,
	[MRT_decExtra2] [numeric](18, 2) NULL,
	[MRT_chvExtra3] [varchar](500) NULL,
	[MRT_chvExtra4] [varchar](500) NULL,
	[MRT_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_MemRep_Trn] PRIMARY KEY CLUSTERED 
(
	[MRT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemDashBoard_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemDashBoard_Mst](
	[MDM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MDM_DBMID] [numeric](18, 0) NOT NULL,
	[MDM_MemID] [numeric](18, 0) NOT NULL,
	[MDM_Flag] [tinyint] NOT NULL,
	[MDM_chvExtra1] [varchar](200) NULL,
	[MDM_chvExtra2] [varchar](200) NULL,
	[MDM_decExtra1] [numeric](18, 2) NULL,
	[MDM_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_MemDashBoard_Mst] PRIMARY KEY CLUSTERED 
(
	[MDM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemCurrentStatus_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemCurrentStatus_Mst](
	[MCSM_ID] [numeric](18, 0) NOT NULL,
	[MCSM_Status] [varchar](50) NULL,
	[MCSM_ConfFlag] [tinyint] NULL,
 CONSTRAINT [PK_MemCurrentStatus_Mst] PRIMARY KEY CLUSTERED 
(
	[MCSM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemCurrentStatus_dtls_log_hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemCurrentStatus_dtls_log_hdr](
	[MLH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLH_Datetime] [datetime] NULL,
	[MLH_AmuID] [numeric](18, 0) NULL,
	[MLH_Remark] [varchar](500) NULL,
	[MLH_Date] [datetime] NULL,
 CONSTRAINT [PK_MemStatus_dtls_log_hdr] PRIMARY KEY CLUSTERED 
(
	[MLH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemCurrentStatus_dtls_log_dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemCurrentStatus_dtls_log_dtls](
	[MLD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLD_MLHID] [numeric](18, 0) NULL,
	[MLD_MemID] [numeric](18, 0) NULL,
	[MLD_StatusID] [numeric](18, 0) NULL,
	[MLD_PrevStatusID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemStatus_dtls_log_dtls] PRIMARY KEY CLUSTERED 
(
	[MLD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemCurrentPayReceipt_log_hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemCurrentPayReceipt_log_hdr](
	[MPH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPH_Datetime] [datetime] NULL,
	[MPH_AmuID] [numeric](18, 0) NULL,
	[MPH_Remark] [varchar](500) NULL,
	[MPH_Date] [datetime] NULL,
 CONSTRAINT [PK_MemCurrentPayReceipt_log_hdr] PRIMARY KEY CLUSTERED 
(
	[MPH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemCurrentPayReceipt_log_dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemCurrentPayReceipt_log_dtls](
	[MPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPD_MPHID] [numeric](18, 0) NULL,
	[MPD_MemID] [numeric](18, 0) NULL,
	[MPD_StatusID] [numeric](18, 0) NULL,
	[MPD_PrevStatusID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemCurrentPayReceipt_log_dtls] PRIMARY KEY CLUSTERED 
(
	[MPD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marquee_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Marquee_trn](
	[MT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MT_NewsHeading] [varchar](500) NULL,
	[MT_NewsDetail] [varchar](500) NULL,
	[MT_ShowFlag] [tinyint] NULL,
	[MT_Webflag] [tinyint] NULL,
	[MT_chvExtra1] [varchar](100) NULL,
	[MT_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Marquee_trn] PRIMARY KEY CLUSTERED 
(
	[MT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ManualSMS_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManualSMS_trn](
	[MST_ID] [numeric](18, 0) NOT NULL,
	[MST_Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ManualSMS_trn] PRIMARY KEY CLUSTERED 
(
	[MST_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Incundoconfirm_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incundoconfirm_Trn_Log](
	[ITL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ITL_DATECODE] [numeric](18, 0) NULL,
	[ITL_Trantype] [numeric](18, 0) NULL,
	[ITL_FrnDate] [datetime] NULL,
	[ITL_Todate] [datetime] NULL,
	[ITL_Date] [datetime] NULL,
	[ITL_Datetime] [datetime] NULL,
	[ITL_AmuId] [numeric](18, 0) NULL,
	[ITL_Ipaddress] [varchar](50) NULL,
	[ITL_Browser] [varchar](50) NULL,
	[ITL_Browserversion] [varchar](50) NULL,
	[ITL_chvExtra1] [varchar](50) NULL,
	[ITL_chvExtra2] [varchar](50) NULL,
	[ITL_decExtra3] [decimal](18, 0) NULL,
	[ITL_decExtra4] [decimal](18, 0) NULL,
	[ITL_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_Incundoconfirm_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[ITL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelCommissionAmt_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelCommissionAmt_Mst_log](
	[LML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LML_LMID] [numeric](18, 0) NOT NULL,
	[LML_Level] [numeric](18, 0) NULL,
	[LML_Amt] [numeric](18, 2) NULL,
	[LML_PvCode] [numeric](18, 0) NULL,
	[LML_LvlFlg] [tinyint] NULL,
	[LML_IncFlg] [tinyint] NULL,
	[LML_DATETIME] [datetime] NULL,
	[LML_logflg] [numeric](18, 0) NULL,
	[LML_AdminID] [numeric](18, 0) NULL,
	[LML_IPAdress] [varchar](100) NULL,
	[LML_Browser] [varchar](100) NULL,
	[LML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelCommissionAmt_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelCommissionAmt_Mst](
	[LM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LM_Level] [numeric](18, 0) NULL,
	[LM_Amt] [numeric](18, 2) NULL,
	[LM_PvCode] [numeric](18, 0) NULL,
	[LM_LvlFlg] [tinyint] NULL,
	[LM_IncFlg] [tinyint] NULL,
 CONSTRAINT [PK_LevelCommissionAmt_Mst] PRIMARY KEY CLUSTERED 
(
	[LM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelBinInc_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelBinInc_Trn](
	[LBI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LBI_DateCode] [numeric](18, 0) NOT NULL,
	[LBI_MemID] [numeric](18, 0) NOT NULL,
	[LBI_Type] [numeric](18, 0) NOT NULL,
	[LBI_Earn1] [numeric](18, 2) NOT NULL,
	[LBI_Earn2] [numeric](18, 2) NOT NULL,
	[LBI_Earn3] [numeric](18, 2) NOT NULL,
	[LBI_Earn4] [numeric](18, 2) NOT NULL,
	[LBI_Earn5] [numeric](18, 2) NOT NULL,
	[LBI_Earn6] [numeric](18, 2) NOT NULL,
	[LBI_Earn7] [numeric](18, 2) NOT NULL,
	[LBI_Earn8] [numeric](18, 2) NOT NULL,
	[LBI_Earn9] [numeric](18, 2) NOT NULL,
	[LBI_Earn10] [numeric](18, 2) NOT NULL,
	[LBI_Earn11] [numeric](18, 2) NOT NULL,
	[LBI_Earn12] [numeric](18, 2) NOT NULL,
	[LBI_Earn13] [numeric](18, 2) NOT NULL,
	[LBI_Earn14] [numeric](18, 2) NOT NULL,
	[LBI_Earn15] [numeric](18, 2) NOT NULL,
	[LBI_LeftChCnt] [numeric](18, 2) NOT NULL,
	[LBI_RightChCnt] [numeric](18, 2) NOT NULL,
	[LBI_LeftCF] [numeric](18, 2) NOT NULL,
	[LBI_RightCF] [numeric](18, 2) NOT NULL,
	[LBI_NewSetB10] [numeric](18, 0) NOT NULL,
	[LBI_NewSetA10] [numeric](18, 0) NOT NULL,
	[LBI_Sets1] [numeric](18, 0) NOT NULL,
	[LBI_Sets2] [numeric](18, 0) NOT NULL,
	[LBI_Sets3] [numeric](18, 0) NOT NULL,
	[LBI_Flag] [tinyint] NOT NULL,
	[LBI_LeftBF] [numeric](18, 2) NOT NULL,
	[LBI_RightBF] [numeric](18, 2) NOT NULL,
	[LBI_NewLeft] [numeric](18, 2) NOT NULL,
	[LBI_NewRight] [numeric](18, 2) NOT NULL,
	[LBI_silverBF] [numeric](18, 2) NOT NULL,
	[LBI_SilverNew] [numeric](18, 2) NOT NULL,
	[LBI_SilverCF] [numeric](18, 2) NOT NULL,
	[LBI_SimBAmt] [numeric](18, 2) NULL,
	[LBI_SimAAmt] [numeric](18, 2) NULL,
	[LBI_decExtra1] [numeric](18, 2) NOT NULL,
	[LBI_decExtra2] [numeric](18, 2) NOT NULL,
	[LBI_decExtra3] [numeric](18, 2) NOT NULL,
	[LBI_decExtra4] [numeric](18, 2) NOT NULL,
	[LBI_chvExtra5] [varchar](100) NOT NULL,
	[LBI_chvExtra6] [varchar](200) NOT NULL,
 CONSTRAINT [PK_LevelBinInc_Trn] PRIMARY KEY CLUSTERED 
(
	[LBI_DateCode] ASC,
	[LBI_MemID] ASC,
	[LBI_Type] ASC,
	[LBI_decExtra4] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelAmt_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelAmt_Mst_log](
	[LML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LML_LMID] [numeric](18, 0) NOT NULL,
	[LML_Level] [numeric](18, 0) NULL,
	[LML_Amt] [numeric](18, 2) NULL,
	[LML_PvCode] [numeric](18, 0) NULL,
	[LML_LvlFlg] [tinyint] NULL,
	[LML_IncFlg] [tinyint] NULL,
	[LML_DATETIME] [datetime] NULL,
	[LML_logflg] [numeric](18, 0) NULL,
	[LML_AdminID] [numeric](18, 0) NULL,
	[LML_IPAdress] [varchar](100) NULL,
	[LML_Browser] [varchar](100) NULL,
	[LML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelAmt_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelAmt_Mst](
	[LM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LM_Level] [numeric](18, 0) NULL,
	[LM_Amt] [numeric](18, 2) NULL,
	[LM_PvCode] [numeric](18, 0) NULL,
	[LM_LvlFlg] [tinyint] NULL,
	[LM_IncFlg] [tinyint] NULL,
 CONSTRAINT [PK_LevelAmt_Mst] PRIMARY KEY CLUSTERED 
(
	[LM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LangCurrency_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LangCurrency_Mst](
	[LC_ID] [numeric](18, 0) NOT NULL,
	[LC_LangName] [varchar](150) NOT NULL,
	[LC_CurName] [varchar](150) NOT NULL,
	[LC_CurSymbol] [varchar](50) NOT NULL,
	[LC_Culture] [varchar](150) NOT NULL,
	[LC_Flag] [tinyint] NULL,
	[LC_chvExtra1] [varchar](100) NULL,
	[LC_chvExtra2] [varchar](100) NULL,
	[LC_decExtra1] [numeric](18, 2) NULL,
	[LC_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_LangCurrency_Mst] PRIMARY KEY CLUSTERED 
(
	[LC_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JoiningPlan_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JoiningPlan_Mst_log](
	[JML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[JML_JMID] [numeric](18, 0) NOT NULL,
	[JML_Name] [varchar](100) NULL,
	[JML_Type] [numeric](18, 0) NULL,
	[JML_Weaker] [tinyint] NULL,
	[JML_Flag] [tinyint] NULL,
	[JML_decExtra1] [numeric](18, 0) NULL,
	[JML_chvExtra2] [varchar](100) NULL,
	[JML_chvExtra3] [varchar](100) NULL,
	[JML_MultiJoinLogic] [tinyint] NOT NULL,
	[JML_DATETIME] [datetime] NULL,
	[JML_logflg] [numeric](18, 0) NULL,
	[JML_AdminID] [numeric](18, 0) NULL,
	[JML_IPAdress] [varchar](100) NULL,
	[JML_Browser] [varchar](100) NULL,
	[JML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrowthAmt_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrowthAmt_Trn](
	[GAT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GAT_RootID] [numeric](18, 0) NULL,
	[GAT_Cycle] [numeric](18, 0) NULL,
	[GAT_PvCode] [numeric](18, 0) NULL,
	[GAT_Desc] [varchar](200) NULL,
	[GAT_Own] [numeric](18, 2) NULL,
	[GAT_Referral] [numeric](18, 2) NULL,
	[GAT_Indirect] [numeric](18, 2) NULL,
	[GAT_Percentage] [numeric](18, 2) NULL,
	[GAT_PercentFlg] [tinyint] NULL,
	[GAT_UpdateFlg] [tinyint] NULL,
	[GAT_amt1] [numeric](18, 2) NULL,
	[GAT_amt2] [numeric](18, 2) NULL,
	[GAT_chvExtra1] [varchar](100) NULL,
	[GAT_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_GrowthAmt_Trn] PRIMARY KEY CLUSTERED 
(
	[GAT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupClub_Hdr_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupClub_Hdr_Log](
	[GHL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GHL_Date] [datetime] NOT NULL,
	[GHL_GroupID] [numeric](18, 0) NOT NULL,
	[GHL_GroupName] [varchar](100) NOT NULL,
	[GHL_Flag] [tinyint] NOT NULL,
	[GHL_chvExtra1] [varchar](200) NULL,
	[GHL_chvExtra2] [varchar](200) NULL,
	[GHL_CPHID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_GroupClub_Hdr_Log] PRIMARY KEY CLUSTERED 
(
	[GHL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupClub_Dtls_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupClub_Dtls_Log](
	[GDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GDL_GroupID] [numeric](18, 0) NOT NULL,
	[GDL_Memid] [numeric](18, 0) NOT NULL,
	[GDL_Owner] [tinyint] NOT NULL,
	[GDL_Flag] [tinyint] NOT NULL,
	[GDL_chvExtra1] [varchar](200) NULL,
	[GDL_chvExtra2] [varchar](200) NULL,
	[GDL_decExtra3] [numeric](18, 2) NULL,
	[GDL_decExtra4] [numeric](18, 2) NULL,
	[GDL_CPHID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_GroupClub_Dtls_Log] PRIMARY KEY CLUSTERED 
(
	[GDL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group_Hdr_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group_Hdr_Log](
	[gl_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[gl_Date] [datetime] NOT NULL,
	[gl_GroupID] [numeric](18, 0) NOT NULL,
	[gl_GroupName] [numeric](18, 0) NOT NULL,
	[gl_Flag] [tinyint] NOT NULL,
	[gl_chvExtra1] [varchar](200) NULL,
	[gl_chvExtra2] [varchar](200) NULL,
	[gl_UHID] [numeric](18, 0) NOT NULL,
	[gl_logDate] [datetime] NOT NULL,
	[gl_chvIPAddress] [varchar](20) NULL,
 CONSTRAINT [PK_Group_Hdr_Log_1] PRIMARY KEY CLUSTERED 
(
	[gl_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group_Hdr](
	[gh_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[gh_Date] [datetime] NOT NULL,
	[gh_GroupID] [numeric](18, 0) NOT NULL,
	[gh_GroupName] [varchar](100) NOT NULL,
	[gh_Flag] [tinyint] NOT NULL,
	[gh_chvExtra1] [varchar](200) NULL,
	[gh_chvExtra2] [varchar](200) NULL,
 CONSTRAINT [PK_Group_Hdr_1] PRIMARY KEY CLUSTERED 
(
	[gh_GroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group_Dtls_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group_Dtls_Log](
	[gl_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[gl_GroupID] [numeric](18, 0) NOT NULL,
	[gl_Memid] [numeric](18, 0) NOT NULL,
	[gl_Owner] [tinyint] NOT NULL,
	[gl_Flag] [tinyint] NOT NULL,
	[gl_chvExtra1] [varchar](200) NULL,
	[gl_chvExtra2] [varchar](200) NULL,
	[gh_chvExtra3] [numeric](18, 0) NULL,
	[gh_chvExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Group_Dtls_Log] PRIMARY KEY CLUSTERED 
(
	[gl_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HintQue_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HintQue_Mst](
	[HQM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[HQM_Que] [varchar](100) NOT NULL,
 CONSTRAINT [PK_HintQue_Mst] PRIMARY KEY CLUSTERED 
(
	[HQM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrowthTarget_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrowthTarget_Mst](
	[GTM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GTM_RootID] [numeric](18, 0) NOT NULL,
	[GTM_Cycle] [numeric](18, 0) NOT NULL,
	[GTM_Name] [varchar](50) NULL,
	[GTM_YearID] [numeric](18, 0) NOT NULL,
	[GTM_Joinings] [numeric](18, 0) NOT NULL,
	[GTM_LessJoinings] [numeric](18, 0) NOT NULL,
	[GTM_Percent] [numeric](18, 2) NOT NULL,
	[GTM_Flag] [tinyint] NOT NULL,
	[GTM_decEXTRA1] [numeric](18, 2) NULL,
	[GTM_decEXTRA2] [numeric](18, 2) NULL,
	[GTM_chvEXTRA3] [varchar](50) NULL,
	[GTM_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_GrowthTarget_Mst] PRIMARY KEY CLUSTERED 
(
	[GTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Field_Hdr_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Field_Hdr_log](
	[FHL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FHL_FHID] [numeric](18, 0) NOT NULL,
	[FHL_ReportName] [varchar](150) NULL,
	[FHL_ExtraFieldCnt] [numeric](18, 0) NULL,
	[FHL_Flag] [tinyint] NULL,
	[FHL_DATETIME] [datetime] NULL,
	[FHL_logflg] [numeric](18, 0) NULL,
	[FHL_AdminID] [numeric](18, 0) NULL,
	[FHL_IPAdress] [varchar](100) NULL,
	[FHL_Browser] [varchar](100) NULL,
	[FHL_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Field_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Field_Hdr](
	[FH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FH_ReportName] [varchar](150) NULL,
	[FH_ExtraFieldCnt] [numeric](18, 0) NULL,
	[FH_Flag] [tinyint] NULL,
 CONSTRAINT [PK_Field_Hdr] PRIMARY KEY CLUSTERED 
(
	[FH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Field_Dtls_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Field_Dtls_log](
	[FML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FML_FMID] [numeric](18, 0) NOT NULL,
	[FML_FHID] [numeric](18, 0) NULL,
	[FML_TableName] [varchar](50) NOT NULL,
	[FML_FieldName] [varchar](500) NOT NULL,
	[FML_Alias] [varchar](500) NULL,
	[FML_FieldFlag] [tinyint] NULL,
	[FML_FilterFlag] [tinyint] NULL,
	[FML_GroupByFlag] [tinyint] NULL,
	[FML_chvExtra1] [varchar](500) NULL,
	[FML_decExtra2] [numeric](18, 2) NULL,
	[FML_DATETIME] [datetime] NULL,
	[FML_logflg] [numeric](18, 0) NULL,
	[FML_AdminID] [numeric](18, 0) NULL,
	[FML_IPAdress] [varchar](100) NULL,
	[FML_Browser] [varchar](100) NULL,
	[FML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GenoDesc_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenoDesc_Mst_log](
	[GML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GML_GMID] [numeric](18, 0) NOT NULL,
	[GML_Name] [varchar](50) NULL,
	[GML_Image] [varchar](200) NULL,
	[GML_Desc] [varchar](50) NULL,
	[GML_decExtra1] [numeric](18, 2) NULL,
	[GML_decExtra2] [numeric](18, 2) NULL,
	[GML_chvExtra3] [varchar](100) NULL,
	[GML_chvExtra4] [varchar](100) NULL,
	[GML_DefaultImage] [varchar](100) NULL,
	[GML_Flag] [numeric](18, 0) NULL,
	[GML_datetime] [datetime] NULL,
	[GML_logFlag] [numeric](18, 0) NULL,
	[GML_adminid] [numeric](18, 0) NULL,
	[GML_IPAdress] [varchar](100) NULL,
	[GML_Browser] [varchar](100) NULL,
	[GML_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_GenoDesc_Mst_log] PRIMARY KEY CLUSTERED 
(
	[GML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GenoDesc_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenoDesc_Mst](
	[GM_ID] [numeric](18, 0) NOT NULL,
	[GM_Name] [varchar](50) NULL,
	[GM_Image] [varchar](200) NULL,
	[GM_Desc] [nvarchar](MAX) NULL,
	[GM_decExtra1] [varbinary](MAX) NULL,
	[GM_decExtra2] [nvarchar](MAX) NULL,
	[GM_chvExtra3] [nvarchar](100) NULL,
	[GM_chvExtra4] [nvarchar](100) NULL,
	[GM_DefaultImage] [nvarchar](100) NULL,
	[GM_Flag] [numeric](18, 0) NULL,
 CONSTRAINT [PK_GenoDesc_Mst] PRIMARY KEY CLUSTERED 
(
	[GM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genealogy_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Genealogy_Mst_log](
	[GML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GML_GMID] [numeric](18, 0) NOT NULL,
	[GML_Sequence] [numeric](18, 0) NULL,
	[GMv_NAME] [varchar](200) NULL,
	[GML_FIELDNAME] [varchar](100) NULL,
	[GML_TABLE] [varchar](100) NULL,
	[GML_Flag] [tinyint] NULL,
	[GML_decEXTRA1] [numeric](18, 2) NULL,
	[GML_chvEXTRA2] [varchar](100) NULL,
	[GML_DATETIME] [datetime] NULL,
	[GML_logflg] [numeric](18, 0) NULL,
	[GML_AdminID] [numeric](18, 0) NULL,
	[GML_IPAdress] [varchar](100) NULL,
	[GML_Browser] [varchar](100) NULL,
	[GML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genealogy_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Genealogy_Mst](
	[GM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GM_Sequence] [numeric](18, 0) NULL,
	[GM_NAME] [varchar](200) NULL,
	[GM_FIELDNAME] [varchar](100) NULL,
	[GM_TABLE] [varchar](100) NULL,
	[GM_Flag] [tinyint] NULL,
	[GM_decEXTRA1] [numeric](18, 2) NULL,
	[GM_chvEXTRA2] [varchar](100) NULL,
 CONSTRAINT [PK_Genealogy_Mst] PRIMARY KEY CLUSTERED 
(
	[GM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GallerySetup_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GallerySetup_Mst](
	[GSM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GSM_VAHID] [numeric](18, 0) NULL,
	[GSM_Name] [varchar](100) NULL,
	[GSM_DisplayOption] [numeric](18, 0) NULL,
	[GSM_RecordCount] [numeric](18, 0) NULL,
 CONSTRAINT [PK_GallerySetup_Mst] PRIMARY KEY CLUSTERED 
(
	[GSM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FrMenu_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FrMenu_Mst](
	[MM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MM_SrNo] [numeric](18, 0) NOT NULL,
	[MM_Name] [varchar](50) NOT NULL,
	[MM_ParentId] [numeric](18, 0) NOT NULL,
	[MM_Url] [varchar](100) NULL,
	[MM_Level] [numeric](18, 0) NOT NULL,
	[MM_ShowFlag] [tinyint] NOT NULL,
	[MM_DropFlg] [tinyint] NULL,
	[MM_Desc] [varchar](100) NULL,
	[MM_MenuType] [numeric](18, 0) NULL,
	[MM_chvExtra1] [varchar](100) NULL,
	[MM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_FrMenu_Mst] PRIMARY KEY CLUSTERED 
(
	[MM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FRInc_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FRInc_Trn](
	[FI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FI_frid] [numeric](18, 0) NOT NULL,
	[FI_datecode] [numeric](18, 0) NOT NULL,
	[FI_joinamt] [numeric](18, 2) NOT NULL,
	[FI_bvamt] [numeric](18, 2) NOT NULL,
	[FI_Joinings] [numeric](18, 0) NOT NULL,
	[FI_BV] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_FRInc_Trn] PRIMARY KEY CLUSTERED 
(
	[FI_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FrEwallet_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FrEwallet_Trn](
	[FET_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FET_FrID] [numeric](18, 0) NOT NULL,
	[FET_Date] [datetime] NOT NULL,
	[FET_DateTime] [datetime] NOT NULL,
	[FET_Amt] [numeric](18, 2) NOT NULL,
	[FET_CrDrFlg] [tinyint] NOT NULL,
	[FET_ShowFlg] [numeric](18, 2) NOT NULL,
	[FET_Remark] [varchar](500) NULL,
	[FET_Type] [numeric](18, 0) NULL,
	[FET_Datecode] [numeric](18, 0) NULL,
	[FET_TrnFlg] [tinyint] NOT NULL,
	[FET_Webflag] [tinyint] NOT NULL,
	[FET_Remark2] [varchar](500) NULL,
	[FET_Amt2] [numeric](18, 2) NULL,
	[FET_AdminID] [numeric](18, 0) NULL,
	[FET_IPAddress] [varchar](50) NULL,
	[FET_Browser] [varchar](50) NULL,
	[FET_BrowserVersion] [varchar](50) NULL,
	[FET_chvExtra1] [varchar](200) NULL,
	[FET_chvExtra2] [varchar](200) NULL,
	[FET_decExtra1] [numeric](18, 2) NULL,
	[FET_decExtra2] [numeric](18, 2) NULL,
	[FET_dtmExtra1] [datetime] NULL,
 CONSTRAINT [PK_FrEwallet_Trn] PRIMARY KEY CLUSTERED 
(
	[FET_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FranchiseType_mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FranchiseType_mst_Log](
	[FTML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FTML_FTMID] [numeric](20, 0) NOT NULL,
	[FTML_Date] [datetime] NOT NULL,
	[FTML_DateTime] [datetime] NOT NULL,
	[FTML_Type] [varchar](100) NOT NULL,
	[FTML_Flag] [varchar](15) NOT NULL,
	[FTML_decExtra1] [numeric](20, 2) NULL,
	[FTML_chvExtra2] [varchar](100) NULL,
	[FTML_LogFlg] [tinyint] NOT NULL,
	[FTML_AdminID] [numeric](20, 0) NOT NULL,
	[FTML_IPAddress] [varchar](50) NULL,
	[FTML_Browser] [varchar](50) NULL,
	[FTML_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_FranchiseType_mst_Log] PRIMARY KEY CLUSTERED 
(
	[FTML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FranchiseType_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FranchiseType_mst](
	[FTM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FTM_Date] [datetime] NOT NULL,
	[FTM_DateTime] [datetime] NOT NULL,
	[FTM_Type] [varchar](100) NOT NULL,
	[FTM_Flag] [varchar](15) NOT NULL,
	[FTM_decExtra1] [numeric](20, 2) NULL,
	[FTM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_FranchiseType_mst] PRIMARY KEY CLUSTERED 
(
	[FTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FranchiseLevel_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FranchiseLevel_mst](
	[FLM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FLM_Date] [datetime] NOT NULL,
	[FLM_DateTime] [datetime] NOT NULL,
	[FLM_Level] [varchar](100) NOT NULL,
	[FLM_Flag] [tinyint] NOT NULL,
	[FLM_decExtra1] [numeric](18, 0) NULL,
	[FLM_chvExtra1] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Franchise_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Franchise_Mst_Log](
	[FML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FML_FMID] [numeric](20, 0) NOT NULL,
	[FML_Code] [varchar](50) NOT NULL,
	[FML_Date] [datetime] NOT NULL,
	[FML_DateTime] [datetime] NOT NULL,
	[FML_Level] [numeric](20, 0) NOT NULL,
	[FML_TypeID] [numeric](20, 0) NOT NULL,
	[FML_Name] [varchar](100) NOT NULL,
	[FML_Address] [varchar](500) NULL,
	[FML_CountryID] [numeric](20, 0) NULL,
	[FML_Country] [varchar](50) NULL,
	[FML_StateID] [numeric](20, 0) NULL,
	[FML_State] [varchar](50) NULL,
	[FML_City] [varchar](50) NULL,
	[FML_ContactNo] [varchar](50) NULL,
	[FML_Mobile] [varchar](50) NULL,
	[FML_Email] [varchar](50) NULL,
	[FML_ContactPerson] [varchar](100) NULL,
	[FML_LandMark] [varchar](100) NULL,
	[FML_PanNo] [varchar](50) NULL,
	[FML_Pincode] [varchar](50) NULL,
	[FML_Username] [varchar](50) NOT NULL,
	[FML_Password] [varchar](50) NOT NULL,
	[FML_ContractCharges] [numeric](20, 2) NULL,
	[FML_ContractPeriod] [numeric](20, 0) NULL,
	[FML_RefFrID] [numeric](20, 0) NULL,
	[FML_RefFrName] [varchar](100) NULL,
	[FML_MemID] [numeric](20, 0) NULL,
	[FML_Flag] [tinyint] NOT NULL,
	[FML_JoiningPercent] [numeric](20, 2) NOT NULL,
	[FML_JoiningAmt] [numeric](20, 2) NOT NULL,
	[FML_ResalePercent] [numeric](20, 2) NOT NULL,
	[FML_ResaleAmt] [numeric](20, 2) NOT NULL,
	[FML_ShoppePercent] [numeric](20, 2) NOT NULL,
	[FML_ShoppeAmt] [numeric](20, 2) NOT NULL,
	[FML_PinPwd] [varchar](50) NULL,
	[FML_EwPwd] [varchar](50) NULL,
	[FML_decExtra1] [numeric](20, 2) NULL,
	[FML_decExtra2] [numeric](20, 2) NULL,
	[FML_chvExtra1] [varchar](200) NULL,
	[FML_chvExtra2] [varchar](200) NULL,
	[FML_LogFlg] [tinyint] NOT NULL,
	[FML_AdminID] [numeric](20, 0) NOT NULL,
	[FML_IPAddress] [varchar](50) NULL,
	[FML_Browser] [varchar](50) NULL,
	[FML_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_Franchise_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[FML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Franchise_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Franchise_mst](
	[FM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FM_Code] [varchar](50) NOT NULL,
	[FM_Date] [datetime] NOT NULL,
	[FM_DateTime] [datetime] NOT NULL,
	[FM_Level] [numeric](20, 0) NOT NULL,
	[FM_TypeID] [numeric](20, 0) NOT NULL,
	[FM_Name] [varchar](100) NOT NULL,
	[FM_Address] [varchar](500) NULL,
	[FM_CountryID] [numeric](20, 0) NULL,
	[FM_Country] [varchar](50) NULL,
	[FM_StateID] [numeric](20, 0) NULL,
	[FM_State] [varchar](50) NULL,
	[FM_City] [varchar](50) NULL,
	[FM_ContactNo] [varchar](50) NULL,
	[FM_Mobile] [varchar](50) NULL,
	[FM_Email] [varchar](50) NULL,
	[FM_ContactPerson] [varchar](100) NULL,
	[FM_LandMark] [varchar](100) NULL,
	[FM_PanNo] [varchar](50) NULL,
	[FM_Pincode] [varchar](50) NULL,
	[FM_Username] [varchar](50) NOT NULL,
	[FM_Password] [varchar](50) NOT NULL,
	[FM_ContractCharges] [numeric](20, 2) NULL,
	[FM_ContractPeriod] [numeric](20, 0) NULL,
	[FM_RefFrID] [numeric](20, 0) NULL,
	[FM_RefFrName] [varchar](100) NULL,
	[FM_MemID] [numeric](20, 0) NULL,
	[FM_Flag] [tinyint] NOT NULL,
	[FM_JoiningPercent] [numeric](20, 2) NOT NULL,
	[FM_JoiningAmt] [numeric](20, 2) NOT NULL,
	[FM_ResalePercent] [numeric](20, 2) NOT NULL,
	[FM_ResaleAmt] [numeric](20, 2) NOT NULL,
	[FM_ShoppePercent] [numeric](20, 2) NOT NULL,
	[FM_ShoppeAmt] [numeric](20, 2) NOT NULL,
	[FM_PinPwd] [varchar](50) NULL,
	[FM_EwPwd] [varchar](50) NULL,
	[FM_decExtra1] [numeric](20, 2) NULL,
	[FM_decExtra2] [numeric](20, 2) NULL,
	[FM_chvExtra1] [varchar](200) NULL,
	[FM_chvExtra2] [varchar](200) NULL,
 CONSTRAINT [PK_Franchise_mst] PRIMARY KEY CLUSTERED 
(
	[FM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentAlbum_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentAlbum_Dtls](
	[PAD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PAD_Name] [varchar](100) NULL,
	[PAD_Desc] [varchar](5000) NULL,
	[PAD_Date] [datetime] NOT NULL,
	[PAD_Datetime] [datetime] NOT NULL,
	[PAD_ValidTillDate] [datetime] NULL,
	[PAD_Flag] [tinyint] NOT NULL,
	[PAD_ShowFlg] [tinyint] NOT NULL,
	[PAD_AdminID] [numeric](18, 0) NOT NULL,
	[PAD_decExtra1] [numeric](18, 2) NULL,
	[PAD_decExtra2] [numeric](18, 2) NULL,
	[PAD_chvExtra1] [varchar](200) NULL,
	[PAD_chvExtra2] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[District_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[District_Mst](
	[DM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DM_StateID] [numeric](18, 0) NOT NULL,
	[DM_Name] [varchar](100) NOT NULL,
	[DM_Flag] [tinyint] NOT NULL,
	[DM_decEXTRA1] [numeric](18, 2) NULL,
	[DM_decEXTRA2] [numeric](18, 2) NULL,
	[DM_chvEXTRA3] [varchar](50) NULL,
	[DM_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_District_Mst] PRIMARY KEY CLUSTERED 
(
	[DM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DispatchMode_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DispatchMode_mst](
	[DM_ID] [numeric](18, 0) NOT NULL,
	[DM_Name] [varchar](50) NOT NULL,
	[DM_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_DispatchMode_mst] PRIMARY KEY CLUSTERED 
(
	[DM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deduction_Dtls_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deduction_Dtls_log](
	[DDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DDL_DD_ID] [numeric](18, 0) NOT NULL,
	[DDL_DHMID] [numeric](18, 0) NULL,
	[DDL_PercFlag] [tinyint] NULL,
	[DDL_Value] [numeric](18, 2) NULL,
	[DDL_AdvanceFlag] [numeric](18, 0) NULL,
	[DDL_MaxAmtPerPayout] [numeric](18, 2) NULL,
	[DDL_CummulativeMaxAmt] [numeric](18, 2) NULL,
	[DDL_GrossOrNetFlag] [numeric](18, 0) NULL,
	[DDL_DedAfterCummulativeAmt] [numeric](18, 2) NULL,
	[DDL_AdvanceOnDedAfterCummulativeAmt] [numeric](18, 2) NULL,
	[DDL_Dtcode] [numeric](18, 0) NULL,
	[DDL_PlanType] [numeric](18, 0) NULL,
	[DDL_DDID] [numeric](18, 0) NULL,
	[DDL_Flag] [numeric](18, 0) NULL,
	[DDL_DATETIME] [datetime] NULL,
	[DDL_logflg] [numeric](18, 0) NULL,
	[DDL_AdminID] [numeric](18, 0) NULL,
	[DDL_IPAdress] [varchar](100) NULL,
	[DDL_Browser] [varchar](100) NULL,
	[DDL_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeedbackDtls_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeedbackDtls_Trn](
	[FT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FT_Memno] [varchar](15) NULL,
	[FT_Fname] [varchar](50) NULL,
	[FT_Lname] [varchar](50) NULL,
	[FT_City] [varchar](80) NULL,
	[FT_State] [varchar](100) NULL,
	[FT_Email] [varchar](100) NULL,
	[FT_Mobile] [varchar](20) NULL,
	[FT_Msg] [varchar](1000) NULL,
	[FT_AOS] [varchar](500) NULL,
	[FT_Address] [varchar](400) NULL,
	[FT_Pincode] [numeric](18, 0) NULL,
	[FT_CntPerson] [varchar](100) NULL,
	[FT_DOB] [datetime] NULL,
	[FT_Subject] [varchar](100) NULL,
	[FT_Date] [datetime] NULL,
	[FT_extra1] [varchar](200) NULL,
	[FT_extra2] [varchar](200) NULL,
	[FT_extra3] [varchar](200) NULL,
	[FT_extra4] [varchar](200) NULL,
	[FT_extra5] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deduction_Hdr_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deduction_Hdr_log](
	[DHL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DHL_DHID] [numeric](18, 0) NOT NULL,
	[DHL_DHMID] [numeric](18, 0) NULL,
	[DHL_Name] [varchar](400) NULL,
	[DHL_Desc] [varchar](400) NULL,
	[DHL_UpdateFlag] [numeric](18, 0) NULL,
	[DHL_Flag] [numeric](18, 0) NULL,
	[DHL_decExtra1] [numeric](18, 0) NULL,
	[DHL_decExtra2] [numeric](18, 0) NULL,
	[DHL_chvExtra3] [varchar](50) NULL,
	[DHL_chvExtra4] [varchar](50) NULL,
	[DHL_DATETIME] [datetime] NULL,
	[DHL_logflg] [numeric](18, 0) NULL,
	[DHL_AdminID] [numeric](18, 0) NULL,
	[DHL_IPAdress] [varchar](100) NULL,
	[DHL_Browser] [varchar](100) NULL,
	[DHL_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Favourites_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favourites_trn](
	[ft_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ft_MenuID] [numeric](18, 0) NOT NULL,
	[ft_MemID] [numeric](18, 0) NOT NULL,
	[ft_Date] [datetime] NOT NULL,
	[ft_Title] [varchar](200) NOT NULL,
	[ft_Link] [varchar](200) NOT NULL,
	[ft_SrNo] [numeric](18, 0) NOT NULL,
	[ft_Extra1] [varchar](200) NULL,
	[ft_Extra2] [varchar](200) NULL,
	[ft_Extra3] [varchar](200) NULL,
 CONSTRAINT [PK_Favourites_trn] PRIMARY KEY CLUSTERED 
(
	[ft_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FavouritePv_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritePv_trn](
	[FPM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FPM_Pvcode] [numeric](20, 0) NOT NULL,
	[FPM_FrID] [numeric](20, 0) NULL,
 CONSTRAINT [PK_FavouritePv_trn] PRIMARY KEY CLUSTERED 
(
	[FPM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FavouritelinksMember_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritelinksMember_Trn](
	[FT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FT_Date] [datetime] NOT NULL,
	[FT_Datetime] [datetime] NOT NULL,
	[FT_MemID] [numeric](18, 0) NOT NULL,
	[FT_MMID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_FavouritelinksMember_Trn] PRIMARY KEY CLUSTERED 
(
	[FT_MemID] ASC,
	[FT_MMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FavouritelinksFranchise_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritelinksFranchise_Trn](
	[FT_ID] [numeric](18, 0) NOT NULL,
	[FT_Date] [datetime] NOT NULL,
	[FT_Datetime] [datetime] NOT NULL,
	[FT_FrID] [numeric](18, 0) NOT NULL,
	[FT_MMID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_FavouritelinksFranchise_Trn] PRIMARY KEY CLUSTERED 
(
	[FT_FrID] ASC,
	[FT_MMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FavouritelinksAdmin_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritelinksAdmin_Trn](
	[FT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FT_Date] [datetime] NOT NULL,
	[FT_Datetime] [datetime] NOT NULL,
	[FT_UHID] [numeric](18, 0) NOT NULL,
	[FT_MMID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_FavouritelinksAdmin_Trn] PRIMARY KEY CLUSTERED 
(
	[FT_UHID] ASC,
	[FT_MMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Faq_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faq_Mst](
	[FAQ_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FAQ_Date] [datetime] NOT NULL,
	[FAQ_DateTime] [datetime] NOT NULL,
	[FAQ_Question] [varchar](100) NOT NULL,
	[FAQ_Answer] [varchar](1000) NOT NULL,
	[FAQ_Status] [tinyint] NOT NULL,
	[FAQ_chvExtra1] [varchar](200) NULL,
	[FAQ_chvExtra2] [varchar](200) NULL,
	[FAQ_decExtra1] [numeric](18, 2) NULL,
	[FAQ_decExtra2] [numeric](18, 2) NULL,
	[FAQ_SrNo] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Faq_Mst] PRIMARY KEY CLUSTERED 
(
	[FAQ_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EWPwd_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EWPwd_Mst](
	[EM_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EM_Pwd] [varchar](50) NOT NULL,
	[EM_MemId] [numeric](18, 0) NOT NULL,
	[EM_Webflag] [tinyint] NULL,
	[EM_Webstatus] [int] NULL,
	[EM_Extra1] [varchar](100) NULL,
	[EM_Extra2] [varchar](100) NULL,
	[EM_Extra3] [varchar](100) NULL,
	[EM_Extra4] [varchar](100) NULL,
 CONSTRAINT [PK_EWPwd_Mst] PRIMARY KEY CLUSTERED 
(
	[EM_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_EWPwd_Mst] UNIQUE NONCLUSTERED 
(
	[EM_MemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ewallet_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ewallet_Trn](
	[ET_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ET_Memid] [numeric](18, 0) NOT NULL,
	[ET_Date] [datetime] NOT NULL,
	[ET_Amt] [numeric](18, 2) NOT NULL,
	[ET_Flag] [tinyint] NOT NULL,
	[ET_Remark] [varchar](8000) NULL,
	[ET_Webflag] [tinyint] NOT NULL,
	[ET_chvExtra1] [varchar](100) NULL,
	[ET_chvExtra2] [varchar](100) NULL,
	[ET_chvExtra3] [varchar](100) NULL,
	[ET_chvExtra4] [varchar](100) NULL,
	[et_RemarkA] [varchar](8000) NULL,
	[ET_Amt1] [numeric](18, 2) NOT NULL,
	[ET_Webstatusid] [numeric](18, 2) NOT NULL,
	[ET_Flg] [numeric](18, 2) NOT NULL,
	[ET_Datecode] [numeric](18, 0) NOT NULL,
	[ET_Type] [numeric](18, 0) NOT NULL,
	[ET_Trnflag] [tinyint] NOT NULL,
	[ET_Datetime] [datetime] NULL,
	[ET_PPHCode] [numeric](18, 0) NULL,
	[ET_Balance] [numeric](18, 2) NULL,
	[ET_CustomerId] [int] NULL,
	[ET_PGCode] [varchar](100) NULL
 CONSTRAINT [PK_Ewallet_Trn] PRIMARY KEY CLUSTERED 
(
	[ET_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ewallet_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ewallet_Log](
	[EL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EL_Memid] [numeric](18, 0) NOT NULL,
	[EL_Date] [datetime] NOT NULL,
	[EL_Amt] [numeric](18, 2) NOT NULL,
	[EL_desc] [varchar](100) NULL,
	[EL_ipaddress] [varchar](100) NULL,
	[EL_chvExtra1] [varchar](100) NULL,
	[EL_chvExtra2] [varchar](100) NULL,
	[EL_chvExtra3] [varchar](100) NULL,
	[EL_chvExtra4] [varchar](100) NULL,
	[EL_CustomerId] [int] NULL,
 CONSTRAINT [PK_Ewallet_Log] PRIMARY KEY CLUSTERED 
(
	[EL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailSMS_mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailSMS_mst_log](
	[EML_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EML_EMID] [numeric](18, 0) NOT NULL,
	[EML_SrNo] [numeric](18, 0) NOT NULL,
	[EML_Description] [varchar](200) NOT NULL,
	[EML_SMSFlg] [tinyint] NOT NULL,
	[EML_EmailFlg] [tinyint] NOT NULL,
	[EML_MsgFlg] [tinyint] NOT NULL,
	[EML_SmsMsg] [varchar](max) NULL,
	[EML_EmailMsg] [varchar](max) NULL,
	[EML_MsgMsg] [varchar](max) NULL,
	[EML_Flag] [tinyint] NOT NULL,
	[EML_ChvExtra1] [varchar](50) NULL,
	[EML_ChvExtra2] [varchar](200) NULL,
	[EML_ChvExtra3] [varchar](200) NULL,
	[EML_DefSmsMsg] [varchar](max) NULL,
	[EML_DefEmailMsg] [varchar](max) NULL,
	[EML_Subject] [varchar](100) NULL,
	[EML_DefSubject] [varchar](100) NULL,
	[EML_Salutation] [varchar](50) NULL,
	[EML_DefSalutation] [varchar](50) NULL,
	[EML_Signature] [varchar](200) NULL,
	[EML_DefSignature] [varchar](200) NULL,
	[EML_datetime] [datetime] NULL,
	[EML_logFlag] [numeric](18, 0) NULL,
	[EML_adminid] [numeric](18, 0) NULL,
	[EML_IPAdress] [varchar](100) NULL,
	[EML_Browser] [varchar](100) NULL,
	[EML_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_EmailSMS_mst_log] PRIMARY KEY CLUSTERED 
(
	[EML_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailSMS_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailSMS_mst](
	[EM_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EM_SrNo] [numeric](18, 0) NOT NULL,
	[EM_Description] [varchar](200) NOT NULL,
	[EM_SMSFlg] [tinyint] NOT NULL,
	[EM_EmailFlg] [tinyint] NOT NULL,
	[EM_MsgFlg] [tinyint] NOT NULL,
	[EM_SmsMsg] [varchar](max) NULL,
	[EM_EmailMsg] [varchar](max) NULL,
	[EM_MsgMsg] [varchar](max) NULL,
	[EM_Flag] [tinyint] NOT NULL,
	[EM_ChvExtra1] [varchar](50) NULL,
	[EM_ChvExtra2] [varchar](200) NULL,
	[EM_ChvExtra3] [varchar](200) NULL,
	[EM_DefSmsMsg] [varchar](max) NULL,
	[EM_DefEmailMsg] [varchar](max) NULL,
	[EM_Subject] [varchar](100) NULL,
	[EM_DefSubject] [varchar](100) NULL,
	[EM_Salutation] [varchar](50) NULL,
	[EM_DefSalutation] [varchar](50) NULL,
	[EM_Signature] [varchar](200) NULL,
	[EM_DefSignature] [varchar](200) NULL,
	[EM_NotifyFlag] [tinyint] NOT NULL,
	[EM_DefMsgMsg] [varchar](max) NULL,
	[EM_IsGrowlNotify] [tinyint] NULL,
	[EM_Type] [varchar](10) NULL,
	[EM_ScheduledDate] [datetime] NULL,
 CONSTRAINT [PK_EmailSMS_mst] PRIMARY KEY CLUSTERED 
(
	[EM_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EarnDedHead_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EarnDedHead_Mst_log](
	[EDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EDL_EDID] [numeric](18, 0) NOT NULL,
	[EDL_SrNo] [numeric](18, 0) NOT NULL,
	[EDL_Type] [numeric](18, 0) NOT NULL,
	[EDL_HeadType] [tinyint] NOT NULL,
	[EDL_DMID] [numeric](18, 0) NULL,
	[EDL_PIMID] [numeric](18, 0) NULL,
	[EDL_FieldName] [varchar](50) NOT NULL,
	[EDL_Desc] [varchar](100) NOT NULL,
	[EDL_Procedure] [varchar](100) NOT NULL,
	[EDL_title] [varchar](100) NOT NULL,
	[EDL_chvExtra1] [varchar](100) NULL,
	[EDL_chvExtra2] [varchar](100) NULL,
	[EDL_decExtra3] [numeric](18, 2) NULL,
	[EDL_decExtra4] [numeric](18, 2) NULL,
	[EDL_RefID] [numeric](18, 0) NULL,
	[EDL_datetime] [datetime] NULL,
	[EDL_logflg] [numeric](18, 0) NULL,
	[EDL_adminid] [numeric](18, 0) NULL,
	[EDL_IPAdress] [varchar](100) NULL,
	[EDL_Browser] [varchar](100) NULL,
	[EDL_Browserversion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EarnDedHead_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EarnDedHead_Mst](
	[ED_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ED_SrNo] [numeric](18, 0) NOT NULL,
	[ED_Type] [numeric](18, 0) NOT NULL,
	[ED_HeadType] [tinyint] NOT NULL,
	[ED_DMID] [numeric](18, 0) NULL,
	[ED_PIMID] [numeric](18, 0) NULL,
	[ED_FieldName] [varchar](50) NOT NULL,
	[ED_Desc] [varchar](100) NOT NULL,
	[ED_Procedure] [varchar](100) NOT NULL,
	[ED_title] [varchar](100) NOT NULL,
	[ED_chvExtra1] [varchar](100) NULL,
	[ED_chvExtra2] [varchar](100) NULL,
	[ED_decExtra3] [numeric](18, 2) NULL,
	[ED_decExtra4] [numeric](18, 2) NULL,
	[ED_RefID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_EarnDedHead_Mst] PRIMARY KEY CLUSTERED 
(
	[ED_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_EarnDedHead_Mst_ED_RefID] UNIQUE NONCLUSTERED 
(
	[ED_RefID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_EarnDedHead_Mst_ED_SrNo] UNIQUE NONCLUSTERED 
(
	[ED_SrNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Designation_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Designation_Mst](
	[DM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DM_Name] [varchar](100) NULL,
	[DM_Flag] [tinyint] NULL,
	[DM_decExtra1] [numeric](18, 0) NULL,
	[DM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Designation_Mst] PRIMARY KEY CLUSTERED 
(
	[DM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Delivery_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Delivery_Mst_Log](
	[DML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DML_DMID] [numeric](20, 0) NULL,
	[DML_Code] [varchar](15) NOT NULL,
	[DML_Date] [datetime] NOT NULL,
	[DML_DateTime] [datetime] NOT NULL,
	[DML_Name] [varchar](100) NOT NULL,
	[DML_Address] [varchar](500) NOT NULL,
	[DML_CountryID] [numeric](20, 0) NULL,
	[DML_Country] [varchar](50) NULL,
	[DML_StateID] [numeric](20, 0) NULL,
	[DML_State] [varchar](50) NULL,
	[DML_City] [varchar](100) NULL,
	[DML_Pincode] [varchar](10) NULL,
	[DML_ContactPerson] [varchar](100) NULL,
	[DML_Mobile] [varchar](20) NULL,
	[DML_ContactNo] [varchar](20) NULL,
	[DML_Email] [varchar](100) NULL,
	[DML_PanNo] [varchar](50) NULL,
	[DML_Username] [varchar](15) NULL,
	[DML_Password] [varchar](800) NULL,
	[DML_Flag] [tinyint] NOT NULL,
	[DML_TypeFlg] [tinyint] NOT NULL,
	[DML_InWeightAmt] [numeric](20, 2) NULL,
	[DML_InDistanceAmt] [numeric](20, 2) NULL,
	[DML_InPeiceAmt] [numeric](20, 2) NULL,
	[DML_OutWeightAmt] [numeric](20, 2) NULL,
	[DML_OutDistanceAmt] [numeric](20, 2) NULL,
	[DML_OutPeiceAmt] [numeric](20, 2) NULL,
	[DML_chvExtra1] [varchar](200) NULL,
	[DML_chvExtra2] [varchar](200) NULL,
	[DML_chvExtra3] [varchar](200) NULL,
	[DML_chvExtra4] [varchar](200) NULL,
	[DML_decExtra1] [numeric](20, 2) NULL,
	[DML_decExtra2] [numeric](20, 2) NULL,
	[DML_decExtra3] [numeric](20, 2) NULL,
	[DML_decExtra4] [numeric](20, 2) NULL,
	[DML_LogFlg] [tinyint] NOT NULL,
	[DML_AdminID] [numeric](20, 0) NOT NULL,
	[DML_IPAddress] [varchar](50) NOT NULL,
	[DML_Browser] [varchar](50) NOT NULL,
	[DML_BrowserVersion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Delivery_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[DML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Delivery_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Delivery_Mst](
	[DM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DM_Code] [varchar](15) NOT NULL,
	[DM_Date] [datetime] NULL,
	[DM_DateTime] [datetime] NULL,
	[DM_Name] [varchar](100) NOT NULL,
	[DM_Address] [varchar](500) NOT NULL,
	[DM_CountryID] [numeric](20, 0) NULL,
	[DM_Country] [varchar](50) NULL,
	[DM_StateID] [numeric](20, 0) NULL,
	[DM_State] [varchar](50) NULL,
	[DM_City] [varchar](100) NULL,
	[DM_Pincode] [varchar](10) NULL,
	[DM_ContactPerson] [varchar](100) NULL,
	[DM_Mobile] [varchar](20) NULL,
	[DM_ContactNo] [varchar](20) NULL,
	[DM_Email] [varchar](100) NULL,
	[DM_PanNo] [varchar](50) NULL,
	[DM_Username] [varchar](15) NULL,
	[DM_Password] [varchar](800) NULL,
	[DM_Flag] [tinyint] NOT NULL,
	[DM_TypeFlg] [tinyint] NOT NULL,
	[DM_InWeightAmt] [numeric](20, 2) NULL,
	[DM_InDistanceAmt] [numeric](20, 2) NULL,
	[DM_InPeiceAmt] [numeric](20, 2) NULL,
	[DM_OutWeightAmt] [numeric](20, 2) NULL,
	[DM_OutDistanceAmt] [numeric](20, 2) NULL,
	[DM_OutPeiceAmt] [numeric](20, 2) NULL,
	[DM_chvExtra1] [varchar](200) NULL,
	[DM_chvExtra2] [varchar](200) NULL,
	[DM_chvExtra3] [varchar](200) NULL,
	[DM_chvExtra4] [varchar](200) NULL,
	[DM_decExtra1] [numeric](20, 2) NULL,
	[DM_decExtra2] [numeric](20, 2) NULL,
	[DM_decExtra3] [numeric](20, 2) NULL,
	[DM_decExtra4] [numeric](20, 2) NULL,
 CONSTRAINT [PK_Delivery_Mst] PRIMARY KEY CLUSTERED 
(
	[DM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeductionHead_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeductionHead_Mst](
	[DHM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DHM_Name] [varchar](100) NULL,
	[DHM_Desc] [varchar](200) NULL,
	[DHM_Flag] [tinyint] NULL,
 CONSTRAINT [PK_DeductionHead_Mst] PRIMARY KEY CLUSTERED 
(
	[DHM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DateCode]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DateCode](
	[DtCode] [numeric](18, 0) NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[Flag] [varchar](1) NOT NULL,
	[confflag] [tinyint] NOT NULL,
	[Type] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_DateCode] PRIMARY KEY NONCLUSTERED 
(
	[DtCode] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DashBoard_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DashBoard_Mst_log](
	[DBML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DBML_DBMID] [numeric](18, 0) NOT NULL,
	[DBML_PnlName] [varchar](50) NOT NULL,
	[DBML_PnlAlise] [varchar](100) NOT NULL,
	[DBML_Flag] [tinyint] NOT NULL,
	[DBML_ShtLngFlg] [tinyint] NOT NULL,
	[DBML_chvExtra1] [varchar](200) NULL,
	[DBML_chvExtra2] [varchar](200) NULL,
	[DBML_decExtra1] [numeric](18, 2) NULL,
	[DBML_decExtra2] [numeric](18, 2) NULL,
	[DBML_DATETIME] [datetime] NULL,
	[DBML_logflg] [numeric](18, 0) NULL,
	[DBML_AdminID] [numeric](18, 0) NULL,
	[DBML_IPAdress] [varchar](100) NULL,
	[DBML_Browser] [varchar](100) NULL,
	[DBML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DashBoard_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DashBoard_Mst](
	[DBM_ID] [numeric](18, 0) NOT NULL,
	[DBM_PnlName] [varchar](50) NOT NULL,
	[DBM_PnlAlise] [varchar](100) NOT NULL,
	[DBM_Flag] [tinyint] NOT NULL,
	[DBM_ShtLngFlg] [tinyint] NOT NULL,
	[DBM_chvExtra1] [varchar](200) NULL,
	[DBM_chvExtra2] [varchar](200) NULL,
	[DBM_decExtra1] [numeric](18, 2) NULL,
	[DBM_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_DashBoard_Mst] PRIMARY KEY CLUSTERED 
(
	[DBM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailySet_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailySet_Trn](
	[DT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DT_MemID] [numeric](18, 0) NOT NULL,
	[DT_Datecode] [numeric](18, 0) NOT NULL,
	[DT_Cycle] [numeric](18, 0) NOT NULL,
	[DT_TotalL] [numeric](18, 0) NOT NULL,
	[DT_TotalR] [numeric](18, 0) NOT NULL,
	[DT_Fixed] [numeric](18, 0) NOT NULL,
	[DT_Star] [numeric](18, 0) NOT NULL,
	[DT_Lapsed] [numeric](18, 0) NOT NULL,
	[DT_CFL] [numeric](18, 0) NOT NULL,
	[DT_CFR] [numeric](18, 0) NOT NULL,
	[DT_BFL] [numeric](18, 0) NOT NULL,
	[DT_BFR] [numeric](18, 0) NOT NULL,
	[DT_decEXTRA1] [numeric](18, 2) NULL,
	[DT_decEXTRA2] [numeric](18, 2) NULL,
	[DT_chvEXTRA3] [varchar](50) NULL,
	[DT_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_DailySet_Trn] PRIMARY KEY CLUSTERED 
(
	[DT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailyCycleOpt_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyCycleOpt_Mst](
	[DOM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOM_OptName] [numeric](18, 0) NULL,
	[DOM_Flag] [tinyint] NULL,
 CONSTRAINT [PK_DailyCycleOpt_Mst] PRIMARY KEY CLUSTERED 
(
	[DOM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AutoCode_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoCode_Mst](
	[ACM_TreeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ACM_SponCode] [varchar](50) NULL,
	[ACM_chvExtra1] [varchar](100) NULL,
 CONSTRAINT [PK_AutoCode_Mst] PRIMARY KEY CLUSTERED 
(
	[ACM_TreeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AutoMatrix_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoMatrix_Trn](
	[ATM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ATM_TreeID] [numeric](18, 0) NULL,
	[ATM_MemID] [numeric](18, 0) NULL,
	[ATM_Code] [varchar](20) NULL,
	[ATM_SponNo] [varchar](20) NULL,
	[ATM_Date] [datetime] NULL,
	[ATM_Lvl1Cnt] [numeric](18, 0) NULL,
	[ATM_Lvl2Cnt] [numeric](18, 0) NULL,
	[ATM_lvl3Cnt] [numeric](18, 0) NULL,
	[ATM_Rentry] [numeric](18, 0) NULL,
	[ATM_Freeflag] [tinyint] NULL,
	[ATM_decExtra1] [numeric](18, 2) NULL,
	[ATM_decExtra2] [numeric](18, 2) NULL,
	[ATM_chvExtra3] [varchar](100) NULL,
	[ATM_chvExtra4] [varchar](100) NULL,
	[ATM_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_AutoMatrixI_Trn] PRIMARY KEY CLUSTERED 
(
	[ATM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ARepField_MST]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ARepField_MST](
	[ARF_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ARF_SrNo] [numeric](18, 0) NULL,
	[ARF_NAME] [varchar](200) NULL,
	[ARF_FIELDNAME] [varchar](200) NULL,
	[ARF_TABLE] [varchar](200) NULL,
	[ARF_decEXTRA1] [numeric](18, 2) NULL,
	[ARF_decEXTRA2] [numeric](18, 2) NULL,
	[ARF_chvEXTRA3] [varchar](100) NULL,
	[ARF_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_ARepField_MST] PRIMARY KEY CLUSTERED 
(
	[ARF_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppMenu_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppMenu_Mst_log](
	[AML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AML_AMID] [numeric](18, 0) NOT NULL,
	[AML_SrNo] [int] NOT NULL,
	[AML_ParentID] [int] NOT NULL,
	[AML_MenuName] [varchar](50) NOT NULL,
	[AML_MenuTooltip] [varchar](50) NULL,
	[AML_PageName] [varchar](100) NULL,
	[AML_MenuUrl] [varchar](100) NOT NULL,
	[AML_QuickHelp] [varchar](8000) NULL,
	[AML_HelpURL] [varchar](500) NULL,
	[AML_MainModuleID] [int] NOT NULL,
	[AML_SubModuleID] [int] NOT NULL,
	[AML_Level] [int] NOT NULL,
	[AML_RootMMID] [numeric](18, 0) NOT NULL,
	[AML_MenuIconURL] [varchar](100) NULL,
	[AML_ListNo] [int] NULL,
	[AML_AuthenticationFlg] [tinyint] NOT NULL,
	[AML_ShowFlg] [tinyint] NOT NULL,
	[AML_Status] [tinyint] NOT NULL,
	[AML_numExtra1] [numeric](18, 0) NULL,
	[AML_numExtra2] [numeric](18, 0) NULL,
	[AML_chvExtra1] [varchar](100) NULL,
	[AML_chvExtra2] [varchar](100) NULL,
	[AML_Def_ShowFlg] [tinyint] NULL,
	[AML_DATETIME] [datetime] NULL,
	[AML_logflg] [numeric](18, 0) NULL,
	[AML_AdminID] [numeric](18, 0) NULL,
	[AML_IPAdress] [varchar](100) NULL,
	[AML_Browser] [varchar](100) NULL,
	[AML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AppMenu_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppMenu_Mst](
	[AM_ID] [numeric](18, 0) NOT NULL,
	[AM_SrNo] [int] NOT NULL,
	[AM_ParentID] [int] NOT NULL,
	[AM_MenuName] [varchar](50) NOT NULL,
	[AM_MenuTooltip] [varchar](50) NULL,
	[AM_PageName] [varchar](100) NULL,
	[AM_MenuUrl] [varchar](100) NOT NULL,
	[AM_QuickHelp] [varchar](8000) NULL,
	[AM_HelpURL] [varchar](500) NULL,
	[AM_MainModuleID] [int] NOT NULL,
	[AM_SubModuleID] [int] NOT NULL,
	[AM_Level] [int] NOT NULL,
	[AM_RootMMID] [numeric](18, 0) NOT NULL,
	[AM_MenuIconURL] [varchar](100) NULL,
	[AM_ListNo] [int] NULL,
	[AM_AuthenticationFlg] [tinyint] NOT NULL,
	[AM_ShowFlg] [tinyint] NOT NULL,
	[AM_Status] [tinyint] NOT NULL,
	[AM_numExtra1] [numeric](18, 0) NULL,
	[AM_numExtra2] [numeric](18, 0) NULL,
	[AM_chvExtra1] [varchar](100) NULL,
	[AM_chvExtra2] [varchar](100) NULL,
	[AM_Def_ShowFlg] [tinyint] NULL,
 CONSTRAINT [PK_AppMenu_Mst] PRIMARY KEY CLUSTERED 
(
	[AM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplicationError_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationError_Log](
	[ael_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ael_Datetime] [datetime] NOT NULL,
	[ael_GUID] [varchar](50) NOT NULL,
	[ael_Message] [varchar](1000) NOT NULL,
	[ael_Source] [varchar](500) NOT NULL,
	[ael_RequestURL] [varchar](500) NOT NULL,
	[ael_IPAddress] [varchar](50) NOT NULL,
	[ael_Browser] [varchar](50) NOT NULL,
	[ael_BrowserVersion] [varchar](50) NOT NULL,
	[ael_TargetSite] [varchar](500) NOT NULL,
	[ael_StackTrace] [varchar](8000) NOT NULL,
	[ael_InnerException] [varchar](500) NULL,
	[ael_AmUID] [numeric](18, 0) NOT NULL,
	[ael_chvExtra1] [varchar](100) NULL,
	[ael_chvExtra2] [varchar](100) NULL,
	[ael_chvExtra3] [varchar](100) NULL,
 CONSTRAINT [PK_ApplicationError_Log] PRIMARY KEY CLUSTERED 
(
	[ael_GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AMemStatus_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AMemStatus_Mst](
	[AMS_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AMS_Desc] [varchar](100) NULL,
	[AMS_chvExtra1] [varchar](100) NULL,
 CONSTRAINT [PK_AMemStatus_Mst] PRIMARY KEY CLUSTERED 
(
	[AMS_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Advance_trn_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Advance_trn_log](
	[ADTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ADTL_ADTID] [numeric](18, 0) NULL,
	[ADTL_Date] [datetime] NULL,
	[ADTL_Datetime] [datetime] NULL,
	[ADTL_AmuId] [numeric](18, 0) NULL,
	[ADTL_chvExtra1] [varchar](50) NULL,
	[ADTL_chvExtra2] [varchar](50) NULL,
	[ADTL_decExtra3] [decimal](18, 0) NULL,
	[ADTL_decExtra4] [decimal](18, 0) NULL,
	[ADTL_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_Advance_trn_log] PRIMARY KEY CLUSTERED 
(
	[ADTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AchieversListAward_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AchieversListAward_Dtls](
	[AAD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AAD_MemID] [numeric](18, 0) NOT NULL,
	[AAD_AwardID] [int] NOT NULL,
	[AAD_QualDate] [datetime] NOT NULL,
	[AAD_ShowFlg] [tinyint] NOT NULL,
	[AAD_TrnFlag] [tinyint] NOT NULL,
	[AAD_TrnDateTime] [datetime] NOT NULL,
	[AAD_Testimonial] [varchar](max) NULL,
	[AAD_chvExtra1] [varchar](200) NULL,
	[AAD_chvExtra2] [varchar](200) NULL,
	[AAD_decExtra1] [numeric](18, 2) NULL,
	[AAD_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_AchieversListAward_Dtls] PRIMARY KEY CLUSTERED 
(
	[AAD_MemID] ASC,
	[AAD_AwardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AchieversList_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AchieversList_Mst](
	[ALM_ID] [int] IDENTITY(1,1) NOT NULL,
	[ALM_EarnID] [numeric](18, 0) NULL,
	[ALM_AwardID] [numeric](18, 0) NULL,
	[ALM_HomeFlag] [tinyint] NOT NULL,
	[ALM_InnerFlag] [tinyint] NOT NULL,
 CONSTRAINT [PK_AchieversList_Mst] PRIMARY KEY CLUSTERED 
(
	[ALM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AchieversList_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AchieversList_Hdr](
	[ALH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ALH_Date] [datetime] NULL,
	[ALH_DateTime] [datetime] NULL,
	[ALH_MemID] [numeric](18, 0) NOT NULL,
	[ALH_HomeFlg] [tinyint] NOT NULL,
	[ALH_PhotoPath] [varchar](500) NULL,
	[ALH_Name] [varchar](200) NOT NULL,
	[ALH_State] [varchar](200) NULL,
	[ALH_City] [varchar](200) NULL,
	[ALH_Mobile] [varchar](200) NULL,
	[ALH_LastUpdateDate] [datetime] NULL,
	[ALH_LastUpdateDtm] [datetime] NULL,
	[ALH_AwardTestimonial] [varchar](max) NULL,
	[ALH_EarningTestimonial] [varchar](max) NULL,
	[ALH_chvExtra1] [varchar](200) NULL,
	[ALH_chvExtra2] [varchar](200) NULL,
	[ALH_decExtra1] [numeric](18, 2) NULL,
	[ALH_decExtra2] [numeric](18, 2) NULL,
	[ALH_AwardDelFlg] [tinyint] NOT NULL,
	[ALH_EarningDelFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_AchieversList_Hdr] PRIMARY KEY CLUSTERED 
(
	[ALH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BinaryReconciliation_dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BinaryReconciliation_dtls](
	[BR_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BR_Datecode] [numeric](18, 0) NOT NULL,
	[BR_Memid] [numeric](18, 0) NOT NULL,
	[BR_Memno] [varchar](100) NULL,
	[BR_Memname] [varchar](200) NULL,
	[BR_leftbf] [numeric](18, 2) NULL,
	[BR_rightbf] [numeric](18, 2) NULL,
	[BR_newleft] [numeric](18, 2) NULL,
	[BR_newright] [numeric](18, 2) NULL,
	[BR_leftcf] [numeric](18, 2) NULL,
	[BR_rightcf] [numeric](18, 2) NULL,
	[BR_leftdirectcnt] [numeric](18, 2) NULL,
	[BR_rightdirectcnt] [numeric](18, 2) NULL,
	[BR_leftspillcnt] [numeric](18, 2) NULL,
	[BR_rightspillcnt] [numeric](18, 2) NULL,
	[BR_leftspillnw] [numeric](18, 2) NULL,
	[BR_rightspillnw] [numeric](18, 2) NULL,
	[BR_Firstset] [numeric](18, 2) NULL,
	[BR_Secondset] [numeric](18, 2) NULL,
	[BR_Amtearned] [numeric](18, 2) NULL,
	[BR_Totcreditleft] [numeric](18, 2) NULL,
	[BR_Totcreditright] [numeric](18, 2) NULL,
	[BR_TotFree] [numeric](18, 2) NULL,
	[BR_Totred] [numeric](18, 2) NULL,
	[BR_Firstsetamt] [numeric](18, 2) NULL,
	[BR_Secsetamt] [numeric](18, 2) NULL,
 CONSTRAINT [PK_BinaryReconciliation_dtls] PRIMARY KEY CLUSTERED 
(
	[BR_ID] ASC,
	[BR_Datecode] ASC,
	[BR_Memid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank_Mst](
	[BM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BM_Name] [varchar](100) NOT NULL,
	[BM_Address] [varchar](150) NOT NULL,
	[BM_AcType] [varchar](15) NOT NULL,
	[BM_AcNo] [varchar](15) NOT NULL,
	[BM_ContactNo] [varchar](50) NULL,
	[BM_ContPer] [varchar](100) NULL,
	[BM_City] [varchar](50) NULL,
	[BM_State] [varchar](50) NOT NULL,
	[BM_WebFlag] [tinyint] NOT NULL,
	[BM_Status] [tinyint] NOT NULL,
	[BM_Extra1] [varchar](100) NULL,
	[BM_Extra2] [varchar](100) NULL,
	[BM_Extra3] [varchar](100) NULL,
	[BM_Extra4] [varchar](100) NULL,
 CONSTRAINT [PK_Bank_Mst] PRIMARY KEY CLUSTERED 
(
	[BM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardType_Mst](
	[ATM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ATM_Name] [varchar](100) NULL,
	[ATM_Flag] [tinyint] NULL,
 CONSTRAINT [PK_AwardType_Mst] PRIMARY KEY CLUSTERED 
(
	[ATM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardDtls_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardDtls_Mst](
	[ADM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ADM_Name] [varchar](200) NULL,
	[ADM_FieldName] [varchar](100) NULL,
	[ADM_Table] [varchar](100) NULL,
	[ADM_decExtra1] [numeric](18, 2) NULL,
	[ADM_decExtra2] [numeric](18, 2) NULL,
	[ADM_chvExtra3] [varchar](100) NULL,
	[ADM_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_AwardDtls_Mst] PRIMARY KEY CLUSTERED 
(
	[ADM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardCriteria_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardCriteria_Mst](
	[ACM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ACM_Name] [varchar](50) NOT NULL,
	[ACM_FieldName] [varchar](50) NOT NULL,
	[ACM_TableName] [varchar](50) NULL,
	[ACM_Desc] [varchar](200) NULL,
	[ACM_Flag] [tinyint] NOT NULL,
	[ACM_CondFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_AwardCriteria_Mst] PRIMARY KEY CLUSTERED 
(
	[ACM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category_Mst_Log](
	[CML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CML_CMID] [numeric](20, 0) NOT NULL,
	[CML_Code] [varchar](15) NOT NULL,
	[CML_Date] [datetime] NOT NULL,
	[CML_DateTime] [datetime] NOT NULL,
	[CML_Name] [varchar](100) NOT NULL,
	[CML_Flag] [tinyint] NOT NULL,
	[CML_ParentID] [numeric](20, 0) NOT NULL,
	[CML_ImagePath] [varchar](100) NULL,
	[CML_LogFlg] [tinyint] NOT NULL,
	[CML_AdminID] [numeric](20, 0) NOT NULL,
	[CML_IPAddress] [varchar](50) NOT NULL,
	[CML_Browser] [varchar](50) NOT NULL,
	[CML_BrowserVersion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[CML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Brand_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brand_Mst](
	[BM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BM_Code] [varchar](15) NULL,
	[BM_Date] [datetime] NOT NULL,
	[BM_DateTime] [datetime] NOT NULL,
	[BM_Name] [varchar](100) NULL,
	[BM_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_Brand_Mst] PRIMARY KEY CLUSTERED 
(
	[BM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AchieversListSetup_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AchieversListSetup_Mst](
	[ASM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ASM_PageType] [tinyint] NOT NULL,
	[ASM_RMID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_AchieversListSetup_Mst] PRIMARY KEY CLUSTERED 
(
	[ASM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AchieversListField_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AchieversListField_Mst](
	[AFM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AFM_SrNo] [int] NOT NULL,
	[AFM_TableName] [varchar](200) NULL,
	[AFM_FieldName] [varchar](200) NULL,
	[AFM_AliasName] [varchar](100) NULL,
	[AFM_HomeFlag] [tinyint] NOT NULL,
	[AFM_InnerFlag] [tinyint] NOT NULL,
	[AFM_AwardFlg] [tinyint] NOT NULL,
	[AFM_EarningFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_AchieversListField_Mst] PRIMARY KEY CLUSTERED 
(
	[AFM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Convention_Mst_log](
	[CML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CML_CMID] [numeric](18, 0) NOT NULL,
	[CML_SrNo] [numeric](18, 0) NOT NULL,
	[CML_Term] [varchar](25) NOT NULL,
	[CML_Name] [varchar](100) NOT NULL,
	[CML_Desc] [varchar](200) NOT NULL,
	[CML_CustomFlg] [tinyint] NULL,
	[CML_DATETIME] [datetime] NULL,
	[CML_logflg] [numeric](18, 0) NULL,
	[CML_AdminID] [numeric](18, 0) NULL,
	[CML_IPAdress] [varchar](100) NULL,
	[CML_Browser] [varchar](100) NULL,
	[CML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Convention_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Convention_Mst](
	[CM_ID] [numeric](18, 0) NOT NULL,
	[CM_SrNo] [numeric](18, 0) NOT NULL,
	[CM_Term] [varchar](25) NOT NULL,
	[CM_Name] [varchar](100) NOT NULL,
	[CM_Desc] [varchar](200) NOT NULL,
	[CM_CustomFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_Convention_Mst] PRIMARY KEY CLUSTERED 
(
	[CM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompanyInfo_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfo_Mst_log](
	[CIML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CIML_CIMID] [numeric](18, 0) NOT NULL,
	[CIML_Code] [varchar](50) NULL,
	[CIML_Name] [varchar](50) NOT NULL,
	[CIML_Address] [varchar](300) NOT NULL,
	[CIML_Country] [varchar](50) NOT NULL,
	[CIML_State] [varchar](50) NULL,
	[CIML_City] [varchar](50) NULL,
	[CIML_Pincode] [varchar](10) NULL,
	[CIML_VATNO] [varchar](50) NULL,
	[CIML_TINNO] [varchar](50) NULL,
	[CIML_ContactPerson] [varchar](200) NULL,
	[CIML_Designation] [varchar](100) NULL,
	[CIML_Phone1] [varchar](50) NULL,
	[CIML_Phone2] [varchar](50) NULL,
	[CIML_Email] [varchar](200) NULL,
	[CIML_URL] [varchar](150) NULL,
	[CIML_Fax] [varchar](100) NULL,
	[CIML_Currency] [varchar](15) NULL,
	[CIML_Logo] [varchar](100) NULL,
	[CIML_Timezone] [varchar](50) NOT NULL,
	[CIML_decExtra1] [numeric](18, 2) NULL,
	[CIML_decExtra2] [numeric](18, 2) NULL,
	[CIML_chvExtra3] [varchar](200) NULL,
	[CIML_chvExtra4] [varchar](200) NULL,
	[CIML_chvExtra5] [varchar](200) NULL,
	[CIML_VatCountry] [varchar](300) NULL,
	[CIML_VatState] [varchar](300) NULL,
	[CIML_DateFormat] [varchar](50) NULL,
	[CIML_LanguageID] [varchar](300) NULL,
	[CIML_ShowCName] [bit] NULL,
	[CIML_ShowCLogo] [bit] NULL,
	[CIML_CNameStyle] [varchar](200) NULL,
	[CIML_UserName] [varchar](100) NULL,
	[CIML_Password] [varchar](100) NULL,
	[CIML_Host] [varchar](100) NULL,
	[CIML_SSL] [tinyint] NULL,
	[CIML_Port] [numeric](18, 0) NULL,
	[CIML_EmailFlg] [tinyint] NULL,
	[CIML_BannerImg] [varchar](200) NULL,
	[CIML_EmailName] [varchar](100) NULL,
	[CIML_Salutation] [varchar](150) NULL,
	[CIML_Signature] [varchar](300) NULL,
	[CIML_IPAddressflg] [tinyint] NULL,
	[CIML_IPAddress] [varchar](150) NULL,
	[CIML_MemModNmFlg] [tinyint] NULL,
	[CIML_VersionFlg] [tinyint] NULL,
	[CIML_Version] [varchar](100) NULL,
	[CIML_FtrLogoFlg] [tinyint] NULL,
	[CIML_Favicon] [varchar](100) NULL,
	[CIML_AppTitle] [varchar](100) NULL,
	[CIML_datetime] [datetime] NULL,
	[CIML_logFlag] [numeric](18, 0) NULL,
	[CIML_adminid] [numeric](18, 0) NULL,
	[CIML_IPAdress] [varchar](100) NULL,
	[CIML_Browser] [varchar](100) NULL,
	[CIML_Browserversion] [varchar](100) NULL,
	[CIML_ShowCBanner] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[City_mst](
	[cm_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cm_StateID] [numeric](18, 0) NOT NULL,
	[cm_Name] [varchar](100) NOT NULL,
	[cm_Metro] [numeric](18, 0) NULL,
	[cm_Major] [numeric](18, 0) NULL,
	[cm_NearTo] [numeric](18, 0) NULL,
	[cm_Flag] [tinyint] NOT NULL,
	[cm_Link1] [varchar](50) NULL,
	[cm_Link2] [varchar](50) NULL,
	[cm_Link3] [varchar](50) NULL,
	[cm_Link4] [varchar](50) NULL,
	[cm_Link5] [varchar](50) NULL,
	[cm_decExtra1] [numeric](18, 2) NULL,
	[cm_chvExtra2] [varchar](100) NULL,
	[cm_chvExtra3] [varchar](100) NULL,
	[cm_Pincode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_City_mst] PRIMARY KEY CLUSTERED 
(
	[cm_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChqPrintBank_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChqPrintBank_Mst_log](
	[CPBML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CPBML_CPBMID] [numeric](18, 0) NOT NULL,
	[CPBML_Name] [varchar](200) NULL,
	[CPBML_Address] [varchar](500) NULL,
	[CPBML_AcType] [varchar](15) NULL,
	[CPBML_AcNo] [varchar](20) NULL,
	[CPBML_ContactNo] [varchar](50) NULL,
	[CPBML_ContPer] [varchar](100) NULL,
	[CPBML_City] [varchar](50) NULL,
	[CPBML_State] [varchar](50) NULL,
	[CPBML_WebFlag] [bit] NULL,
	[CPBML_Status] [bit] NULL,
	[CPBML_decExtra1] [numeric](18, 2) NULL,
	[CPBML_decExtra2] [numeric](18, 2) NULL,
	[CPBML_chvExtra3] [varchar](100) NULL,
	[CPBML_chvExtra4] [varchar](100) NULL,
	[CPBML_DATETIME] [datetime] NULL,
	[CPBML_logflg] [numeric](18, 0) NULL,
	[CPBML_AdminID] [numeric](18, 0) NULL,
	[CPBML_IPAdress] [varchar](100) NULL,
	[CPBML_Browser] [varchar](100) NULL,
	[CPBML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChqPrintBank_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChqPrintBank_Mst](
	[CPBM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CPBM_Name] [varchar](200) NULL,
	[CPBM_Address] [varchar](500) NULL,
	[CPBM_AcType] [varchar](15) NULL,
	[CPBM_AcNo] [varchar](20) NULL,
	[CPBM_ContactNo] [varchar](50) NULL,
	[CPBM_ContPer] [varchar](100) NULL,
	[CPBM_City] [varchar](50) NULL,
	[CPBM_State] [varchar](50) NULL,
	[CPBM_WebFlag] [bit] NULL,
	[CPBM_Status] [bit] NULL,
	[CPBM_decExtra1] [numeric](18, 2) NULL,
	[CPBM_decExtra2] [numeric](18, 2) NULL,
	[CPBM_chvExtra3] [varchar](100) NULL,
	[CPBM_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_ChqPrintBank_Mst] PRIMARY KEY CLUSTERED 
(
	[CPBM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChequePrint_TrnInv]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChequePrint_TrnInv](
	[cpt_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cpt_datecode] [numeric](18, 0) NOT NULL,
	[cpt_cheqPayTo] [varchar](150) NOT NULL,
	[cpt_memid] [numeric](18, 0) NOT NULL,
	[cpt_AmtInwords] [varchar](150) NOT NULL,
	[cpt_AmtInFig] [numeric](18, 2) NOT NULL,
	[cpt_CheqNo] [numeric](18, 0) NOT NULL,
	[cpt_CheqDate] [datetime] NOT NULL,
	[cpt_BankName] [varchar](150) NOT NULL,
	[cpt_WebFlag] [tinyint] NOT NULL,
	[cpt_Remark] [varchar](250) NULL,
	[cpt_type] [numeric](18, 0) NOT NULL,
	[cpt_enddtcode] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ChequePrint_TrnInv] PRIMARY KEY CLUSTERED 
(
	[cpt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DateCodeInv]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DateCodeInv](
	[DtCode] [numeric](18, 0) NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[Flag] [varchar](1) NOT NULL,
	[webflag] [tinyint] NOT NULL,
 CONSTRAINT [PK_DateCodeInv] PRIMARY KEY NONCLUSTERED 
(
	[DtCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RepurchaseCond_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RepurchaseCond_Mst](
	[RCM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RCM_Field] [varchar](20) NULL,
	[RCM_Operator] [varchar](20) NULL,
	[RCM_Value] [varchar](20) NULL,
	[RCM_SlabLevelFlg] [tinyint] NULL,
	[RCM_AndOrFlg] [tinyint] NULL,
	[RCM_decExtra1] [numeric](18, 2) NULL,
	[RCM_chvExtra2] [varchar](100) NULL,
	[RCM_chvExtra3] [varchar](100) NULL,
 CONSTRAINT [PK_RepurchaseCond_Mst] PRIMARY KEY CLUSTERED 
(
	[RCM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Repurchase_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Repurchase_Mst](
	[RM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RM_Designation] [varchar](100) NULL,
	[RM_Level] [numeric](18, 0) NULL,
	[RM_Min] [numeric](18, 2) NULL,
	[RM_Max] [numeric](18, 2) NULL,
	[RM_Per] [numeric](18, 2) NULL,
	[RM_Flag] [tinyint] NULL,
	[RM_TypeFlg] [tinyint] NULL,
	[RM_decExtra1] [numeric](18, 2) NULL,
	[RM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Repurchase_mst] PRIMARY KEY CLUSTERED 
(
	[RM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PV_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PV_Dtls](
	[PVD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PVD_HID] [numeric](18, 0) NOT NULL,
	[PVD_PvCode] [numeric](18, 0) NOT NULL,
	[PVD_BVPts] [numeric](18, 2) NOT NULL,
	[PVD_PVPts] [numeric](18, 2) NOT NULL,
	[PVD_GrUnits] [numeric](18, 2) NOT NULL,
	[PVD_Rate] [numeric](18, 2) NOT NULL,
	[PVD_VAT] [numeric](18, 2) NOT NULL,
	[PVD_Price] [numeric](18, 2) NOT NULL,
	[PVD_DiscPercent] [numeric](18, 2) NOT NULL,
	[PVD_DiscAmt] [numeric](18, 2) NOT NULL,
	[PVD_Qty] [numeric](18, 0) NOT NULL,
	[PVD_TotDiscAmt] [numeric](18, 2) NOT NULL,
	[PVD_SubTotal] [numeric](18, 2) NOT NULL,
	[PVD_TotBVPts] [numeric](18, 2) NOT NULL,
	[PVD_TotPVPts] [numeric](18, 2) NOT NULL,
	[PVD_TotGrUnits] [numeric](18, 2) NOT NULL,
	[PVD_chvExtra1] [varchar](200) NULL,
	[PVD_chvExtra2] [varchar](200) NULL,
	[PVD_decExtra1] [numeric](18, 2) NULL,
	[PVD_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_PV_Dtls] PRIMARY KEY CLUSTERED 
(
	[PVD_HID] ASC,
	[PVD_PvCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelwiseCnt_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelwiseCnt_Dtls](
	[Level_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Level_No] [numeric](18, 0) NULL,
	[Level_Name] [varchar](500) NULL,
	[Level_Cnt] [numeric](18, 0) NULL,
	[Level_TotalCnt] [numeric](18, 0) NULL,
 CONSTRAINT [PK_LevelwiseCnt_Dtls] PRIMARY KEY CLUSTERED 
(
	[Level_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pv_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pv_Mst_Log](
	[PVL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PVL_Code] [numeric](20, 0) NOT NULL,
	[PVL_PDCode] [varchar](20) NULL,
	[PVL_Date] [datetime] NULL,
	[PVL_DateTime] [datetime] NULL,
	[PVL_CategoryID] [numeric](20, 0) NULL,
	[PVL_BrandID] [numeric](20, 0) NULL,
	[PVL_Name] [varchar](100) NULL,
	[PVL_Desc] [varchar](8000) NULL,
	[PVL_Image] [varchar](100) NULL,
	[PVL_Thumbnail] [varchar](100) NULL,
	[PVL_FilePath] [varchar](100) NULL,
	[PVL_BVPts] [numeric](20, 2) NOT NULL,
	[PVL_PVPts] [numeric](20, 6) NOT NULL,
	[PVL_GrUnits] [numeric](20, 2) NOT NULL,
	[PVL_SaleRate] [numeric](20, 2) NOT NULL,
	[PVL_SaleVAT] [numeric](20, 2) NOT NULL,
	[PVL_SalePrice] [numeric](20, 2) NOT NULL,
	[PVL_Type] [numeric](20, 0) NOT NULL,
	[PVL_PurRate] [numeric](20, 2) NOT NULL,
	[PVL_PurVAT] [numeric](20, 2) NOT NULL,
	[PVL_PurPrice] [numeric](20, 2) NOT NULL,
	[PVL_UOM] [varchar](50) NULL,
	[PVL_AdvanceAmt] [numeric](20, 2) NOT NULL,
	[PVL_TOAmt] [numeric](20, 2) NOT NULL,
	[PVL_ReserveAmt] [numeric](20, 2) NOT NULL,
	[PVL_SpillAmt] [numeric](20, 2) NOT NULL,
	[PVL_SpillNtwAmt] [numeric](20, 2) NOT NULL,
	[PVL_DirectAmt] [numeric](20, 2) NOT NULL,
	[PVL_RoyAmt] [numeric](20, 2) NOT NULL,
	[PVL_FranchiseAmt] [numeric](20, 2) NOT NULL,
	[PVL_SimAmt] [numeric](20, 2) NOT NULL,
	[PVL_Award] [numeric](20, 2) NOT NULL,
	[PVL_Passive] [numeric](20, 2) NOT NULL,
	[PVL_ShipAmt] [numeric](20, 2) NOT NULL,
	[PVL_DiscPercent] [numeric](20, 2) NOT NULL,
	[PVL_DiscAmt] [numeric](20, 2) NOT NULL,
	[PVL_LeadDays] [numeric](20, 0) NOT NULL,
	[PVL_ReorderQty] [numeric](20, 2) NOT NULL,
	[PVL_OpenStock] [numeric](20, 2) NOT NULL,
	[PVL_MinStockQty] [numeric](20, 0) NULL,
	[PVL_Manufacturer] [varchar](100) NULL,
	[PVL_Model] [varchar](50) NULL,
	[PVL_Weight] [numeric](20, 0) NULL,
	[PVL_IsTodaySpecial] [tinyint] NOT NULL,
	[PVL_IsDownloadable] [tinyint] NOT NULL,
	[PVL_DownloadPath] [varchar](100) NULL,
	[PVL_Rating] [numeric](20, 0) NOT NULL,
	[PVL_Viewed] [numeric](20, 0) NOT NULL,
	[PVL_IsHotDeal] [tinyint] NOT NULL,
	[PVL_ShoppeFlg] [tinyint] NOT NULL,
	[PVL_ResaleFlg] [tinyint] NOT NULL,
	[PVL_JoiningFlg] [tinyint] NOT NULL,
	[PVL_CapFlg] [tinyint] NOT NULL,
	[PVL_StatusFlg] [tinyint] NOT NULL,
	[PVL_PackageFlg] [tinyint] NOT NULL,
	[PVL_WebFlag] [tinyint] NOT NULL,
	[PVL_decExtra1] [numeric](20, 2) NULL,
	[PVL_decExtra2] [numeric](20, 2) NULL,
	[PVL_decExtra3] [numeric](20, 2) NULL,
	[PVL_decExtra4] [numeric](20, 2) NULL,
	[PVL_chvExtra1] [varchar](200) NULL,
	[PVL_chvExtra2] [varchar](200) NULL,
	[PVL_chvExtra3] [varchar](200) NULL,
	[PVL_chvExtra4] [varchar](200) NULL,
	[PVL_dtmExtra1] [datetime] NULL,
	[PVL_dtmExtra2] [datetime] NULL,
	[PVL_LogFlg] [tinyint] NOT NULL,
	[PVL_AdminID] [numeric](20, 0) NOT NULL,
	[PVL_IPAddress] [varchar](50) NOT NULL,
	[PVL_Browser] [varchar](50) NOT NULL,
	[PVL_BrowserVersion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Pv_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[PVL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Renewal_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Renewal_Trn_Log](
	[RTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RTL_PTTID] [numeric](18, 0) NULL,
	[RTL_Date] [datetime] NULL,
	[RTL_Datetime] [datetime] NULL,
	[RTL_AmuId] [numeric](18, 0) NULL,
	[RTL_Ipaddress] [varchar](50) NULL,
	[RTL_Browser] [varchar](50) NULL,
	[RTL_BrowserVersion] [varchar](50) NULL,
	[RTL_chvExtra1] [varchar](50) NULL,
	[RTL_chvExtra2] [varchar](50) NULL,
	[RTL_decExtra3] [decimal](18, 0) NULL,
	[RTL_decExtra4] [decimal](18, 0) NULL,
	[RTL_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_Renewal_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[RTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Renewal_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Renewal_Trn](
	[RT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RT_Memid] [numeric](18, 0) NOT NULL,
	[RT_Date] [datetime] NOT NULL,
	[RT_QualDate] [datetime] NOT NULL,
	[RT_Amt] [numeric](18, 2) NOT NULL,
	[RT_SrNo] [numeric](18, 0) NOT NULL,
	[RT_Pvcode] [numeric](18, 0) NOT NULL,
	[RT_Type] [numeric](18, 0) NULL,
	[RT_datecode] [numeric](18, 0) NULL,
	[RT_Remark] [varchar](500) NULL,
	[RT_PMID] [numeric](18, 0) NULL,
	[RT_TypeFlg] [tinyint] NOT NULL,
	[RT_Flag] [tinyint] NOT NULL,
	[RT_PrevQualDate] [datetime] NULL,
	[RT_Extra1] [varchar](100) NULL,
	[RT_Extra2] [varchar](100) NULL,
	[RT_Extra3] [varchar](100) NULL,
	[RT_Extra4] [varchar](100) NULL,
	[RT_Extra5] [varchar](100) NULL,
 CONSTRAINT [PK_Renewal_trn] PRIMARY KEY CLUSTERED 
(
	[RT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RelatedlinksMember_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelatedlinksMember_Trn](
	[RT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RT_MMID] [numeric](18, 0) NOT NULL,
	[RT_RelatedMMID] [numeric](18, 0) NOT NULL,
	[RT_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_RelatedlinksMember_Trn] PRIMARY KEY CLUSTERED 
(
	[RT_MMID] ASC,
	[RT_RelatedMMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RelatedlinksFranchise_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelatedlinksFranchise_Trn](
	[RT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RT_MMID] [numeric](18, 0) NOT NULL,
	[RT_RelatedMMID] [numeric](18, 0) NOT NULL,
	[RT_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_RelatedlinksFranchise_Trn] PRIMARY KEY CLUSTERED 
(
	[RT_MMID] ASC,
	[RT_RelatedMMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RelatedlinksAdmin_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelatedlinksAdmin_Trn](
	[RT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RT_MMID] [numeric](18, 0) NOT NULL,
	[RT_RelatedMMID] [numeric](18, 0) NOT NULL,
	[RT_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_RelatedlinksAdmin_Trn] PRIMARY KEY CLUSTERED 
(
	[RT_MMID] ASC,
	[RT_RelatedMMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regt_DB_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Regt_DB_log](
	[RDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RDL_RDID] [numeric](18, 0) NOT NULL,
	[RDL_FieldName] [varchar](400) NOT NULL,
	[RDL_LabelName] [varchar](800) NULL,
	[RDL_ErrorMsg] [varchar](800) NULL,
	[RDL_Show] [tinyint] NOT NULL,
	[RDL_Compulsory] [tinyint] NOT NULL,
	[RDL_Enabled] [tinyint] NOT NULL,
	[RDL_Editable] [tinyint] NOT NULL,
	[RDL_EditDisplay] [tinyint] NOT NULL,
	[RDL_AltValue] [varchar](100) NULL,
	[RDL_MinLength] [numeric](18, 0) NOT NULL,
	[RDL_MaxLength] [numeric](18, 0) NOT NULL,
	[RDL_Tooltip] [tinyint] NOT NULL,
	[RDL_TooltipText] [varchar](200) NULL,
	[RDL_decEXTRA1] [numeric](18, 2) NULL,
	[RDL_decEXTRA2] [numeric](18, 2) NULL,
	[RDL_chvEXTRA3] [varchar](100) NULL,
	[RDL_chvEXTRA4] [varchar](100) NULL,
	[RDL_datetime] [datetime] NULL,
	[RDL_LogFlg] [numeric](18, 0) NULL,
	[RDL_Adminid] [numeric](18, 0) NULL,
	[RDL_IPAdress] [varchar](100) NULL,
	[RDL_Browser] [varchar](100) NULL,
	[RDL_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Regt_DB]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Regt_DB](
	[RD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RD_FieldName] [varchar](400) NOT NULL,
	[RD_LabelName] [varchar](800) NULL,
	[RD_ErrorMsg] [varchar](800) NULL,
	[RD_Show] [tinyint] NOT NULL,
	[RD_Compulsory] [tinyint] NOT NULL,
	[RD_Enabled] [tinyint] NOT NULL,
	[RD_Editable] [tinyint] NOT NULL,
	[RD_EditDisplay] [tinyint] NOT NULL,
	[RD_AltValue] [varchar](100) NULL,
	[RD_MinLength] [numeric](18, 0) NOT NULL,
	[RD_MaxLength] [numeric](18, 0) NOT NULL,
	[RD_Tooltip] [tinyint] NOT NULL,
	[RD_TooltipText] [varchar](200) NULL,
	[RD_decEXTRA1] [numeric](18, 2) NULL,
	[RD_decEXTRA2] [numeric](18, 2) NULL,
	[RD_chvEXTRA3] [varchar](100) NULL,
	[RD_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_Regt_DB] PRIMARY KEY CLUSTERED 
(
	[RD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegNo_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegNo_Mst_log](
	[RML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RML_RMID] [numeric](18, 0) NOT NULL,
	[RML_Type] [numeric](18, 0) NOT NULL,
	[RML_Noofchar] [numeric](18, 0) NOT NULL,
	[RML_Noofdigit] [numeric](18, 0) NOT NULL,
	[RML_SRFlg] [tinyint] NOT NULL,
	[RML_SPFlg] [tinyint] NOT NULL,
	[RML_CharFlg] [tinyint] NOT NULL,
	[RML_StartStr] [varchar](50) NULL,
	[RML_StartDigit] [numeric](18, 0) NOT NULL,
	[RML_LastDigit] [numeric](18, 0) NOT NULL,
	[RML_Letters] [numeric](18, 0) NOT NULL,
	[RML_Srnoflg] [numeric](18, 0) NOT NULL,
	[RML_ZonalFlg] [tinyint] NOT NULL,
	[RML_DateFlg] [tinyint] NOT NULL,
	[RML_decEXTRA1] [numeric](18, 2) NULL,
	[RML_decEXTRA2] [numeric](18, 2) NULL,
	[RML_chvEXTRA3] [varchar](100) NULL,
	[RML_chvEXTRA4] [varchar](100) NULL,
	[RML_DATETIME] [datetime] NULL,
	[RML_logflg] [numeric](18, 0) NULL,
	[RML_AdminID] [numeric](18, 0) NULL,
	[RML_IPAdress] [varchar](100) NULL,
	[RML_Browser] [varchar](100) NULL,
	[RML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegNo_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegNo_Mst](
	[RM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RM_Type] [numeric](18, 0) NOT NULL,
	[RM_Noofchar] [numeric](18, 0) NOT NULL,
	[RM_Noofdigit] [numeric](18, 0) NOT NULL,
	[RM_SRFlg] [tinyint] NOT NULL,
	[RM_SPFlg] [tinyint] NOT NULL,
	[RM_CharFlg] [tinyint] NOT NULL,
	[RM_StartStr] [varchar](50) NULL,
	[RM_StartDigit] [numeric](18, 0) NOT NULL,
	[RM_LastDigit] [numeric](18, 0) NOT NULL,
	[RM_Letters] [numeric](18, 0) NOT NULL,
	[RM_Srnoflg] [numeric](18, 0) NOT NULL,
	[RM_ZonalFlg] [tinyint] NOT NULL,
	[RM_DateFlg] [tinyint] NOT NULL,
	[RM_decEXTRA1] [numeric](18, 2) NULL,
	[RM_decEXTRA2] [numeric](18, 2) NULL,
	[RM_chvEXTRA3] [varchar](100) NULL,
	[RM_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_RegNo_Mst] PRIMARY KEY CLUSTERED 
(
	[RM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Referalincome_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Referalincome_Dtls](
	[ref_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ref_datecode] [numeric](18, 0) NULL,
	[ref_memid] [numeric](18, 0) NULL,
	[ref_directid] [numeric](18, 0) NULL,
	[ref_totalincome] [numeric](18, 2) NULL,
	[ref_totalearning] [numeric](18, 2) NULL,
	[ref_paidflag] [numeric](18, 0) NULL,
	[ref_paidflag2] [numeric](18, 0) NULL,
	[ref_incdatecode] [numeric](18, 0) NULL,
	[ref_per] [numeric](18, 2) NULL,
	[ref_type] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecordCount_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecordCount_Mst](
	[RM_ID] [numeric](18, 0) NOT NULL,
	[RM_RowPerPage] [numeric](18, 0) NOT NULL,
	[RM_Desc] [varchar](50) NULL,
	[RM_Flag] [tinyint] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Qualification_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Qualification_Mst](
	[QM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[QM_Name] [varchar](100) NULL,
	[QM_Flag] [tinyint] NULL,
	[QM_chvExtra1] [varchar](100) NULL,
	[QM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Qualification_Mst] PRIMARY KEY CLUSTERED 
(
	[QM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PvType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PvType_Mst](
	[PTM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PTM_Desc] [varchar](50) NULL,
 CONSTRAINT [PK_PvType_Mst_1] PRIMARY KEY CLUSTERED 
(
	[PTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMS_Text_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS_Text_Mst](
	[SM_ID] [numeric](18, 0) NOT NULL,
	[SM_SMS] [varchar](100) NOT NULL,
	[SM_MSTID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_SMS_Text_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMS_Status]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS_Status](
	[SS_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SS_Type] [varchar](10) NOT NULL,
	[SS_Msg] [varchar](500) NOT NULL,
	[SS_Flag] [numeric](18, 0) NOT NULL,
	[SS_Send] [tinyint] NULL,
	[SS_decEXTRA1] [numeric](18, 2) NULL,
	[SS_decEXTRA2] [numeric](18, 2) NULL,
	[SS_chvEXTRA3] [varchar](50) NULL,
	[SS_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_SMS_Status] PRIMARY KEY CLUSTERED 
(
	[SS_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Slab_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slab_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_DMID] [numeric](18, 0) NOT NULL,
	[SM_Min] [numeric](18, 0) NULL,
	[SM_Max] [numeric](18, 0) NULL,
	[SM_Value] [numeric](18, 2) NULL,
	[SM_PerFlg] [tinyint] NULL,
	[SM_Flag] [tinyint] NULL,
 CONSTRAINT [PK_Slab_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SimulationAmt_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SimulationAmt_Trn](
	[Sim_Datecode] [numeric](18, 0) NOT NULL,
	[Sim_AmtB] [numeric](18, 2) NOT NULL,
	[Sim_AmtA] [numeric](18, 2) NOT NULL,
	[sim_Type] [numeric](18, 0) NULL,
	[sim_Percentage] [numeric](18, 2) NULL,
	[sim_Formula] [tinyint] NULL,
	[sim_chvExtra1] [varchar](100) NULL,
 CONSTRAINT [PK_SimulationAmt] PRIMARY KEY CLUSTERED 
(
	[Sim_Datecode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Settings_Mst_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings_Mst_log](
	[SML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SML_SMID] [numeric](18, 0) NOT NULL,
	[SML_SrNo] [numeric](18, 0) NULL,
	[SML_Name] [varchar](50) NULL,
	[SML_Desc] [varchar](50) NULL,
	[SML_Value] [numeric](18, 0) NULL,
	[SML_Flag] [numeric](18, 0) NULL,
	[SML_CustomizationFlag] [numeric](18, 0) NULL,
	[SML_decExtra1] [numeric](18, 2) NULL,
	[SML_decExtra2] [numeric](18, 2) NULL,
	[SML_decExtra3] [numeric](18, 2) NULL,
	[SML_decExtra4] [numeric](18, 2) NULL,
	[SML_chvExtra1] [varchar](100) NULL,
	[SML_chvExtra2] [varchar](100) NULL,
	[SML_datetime] [datetime] NULL,
	[SML_logFlag] [numeric](18, 0) NULL,
	[SML_adminid] [numeric](18, 0) NULL,
	[SML_IPAdress] [varchar](100) NULL,
	[SML_Browser] [varchar](100) NULL,
	[SML_Browserversion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Set_Mst_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Set_Mst_log](
	[SML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SML_SMID] [numeric](18, 0) NOT NULL,
	[SML_SrNo] [numeric](18, 0) NOT NULL,
	[SML_Side1] [numeric](18, 2) NOT NULL,
	[SML_Side2] [numeric](18, 2) NOT NULL,
	[SML_Flag] [tinyint] NOT NULL,
	[SML_StarFlg] [tinyint] NOT NULL,
	[SML_Amt] [numeric](18, 2) NOT NULL,
	[SML_SetCnt] [numeric](18, 0) NOT NULL,
	[SML_PvType] [numeric](18, 0) NOT NULL,
	[SML_Type] [numeric](18, 0) NOT NULL,
	[SML_TailCnt] [numeric](18, 0) NOT NULL,
	[SML_TailFlg] [numeric](18, 0) NOT NULL,
	[SML_decEXTRA1] [numeric](18, 2) NULL,
	[SML_decEXTRA2] [numeric](18, 2) NULL,
	[SML_chvEXTRA3] [varchar](100) NULL,
	[SML_chvEXTRA4] [varchar](100) NULL,
	[SML_DATETIME] [datetime] NULL,
	[SML_logflg] [numeric](18, 0) NULL,
	[SML_AdminID] [numeric](18, 0) NULL,
	[SML_IPAdress] [varchar](100) NULL,
	[SML_Browser] [varchar](100) NULL,
	[SML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Set_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Set_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_SrNo] [numeric](18, 0) NOT NULL,
	[SM_Side1] [numeric](18, 2) NOT NULL,
	[SM_Side2] [numeric](18, 2) NOT NULL,
	[SM_Flag] [tinyint] NOT NULL,
	[SM_StarFlg] [tinyint] NOT NULL,
	[SM_Amt] [numeric](18, 2) NOT NULL,
	[SM_SetCnt] [numeric](18, 0) NOT NULL,
	[SM_PvType] [numeric](18, 0) NOT NULL,
	[SM_Type] [numeric](18, 0) NOT NULL,
	[SM_TailCnt] [numeric](18, 0) NOT NULL,
	[SM_TailFlg] [numeric](18, 0) NOT NULL,
	[SM_decEXTRA1] [numeric](18, 2) NULL,
	[SM_decEXTRA2] [numeric](18, 2) NULL,
	[SM_chvEXTRA3] [varchar](100) NULL,
	[SM_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_Set_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Scheme_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Scheme_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_Name] [varchar](50) NOT NULL,
	[SM_Desc] [varchar](200) NULL,
	[SM_Date] [datetime] NULL,
	[SM_SDate] [datetime] NULL,
	[SM_Edate] [datetime] NULL,
	[SM_SchemeType] [tinyint] NULL,
	[SM_CummFlg] [tinyint] NULL,
	[SM_AutoProcessFlg] [tinyint] NULL,
	[SM_AwardFlg] [tinyint] NULL,
	[SM_ClaimFlg] [tinyint] NULL,
	[SM_Flag] [tinyint] NULL,
 CONSTRAINT [PK_Scheme_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RunRoyQual_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunRoyQual_Dtls](
	[RY_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RY_Datecode] [numeric](18, 0) NULL,
	[RY_Memid] [numeric](18, 0) NULL,
	[RY_Type] [numeric](18, 0) NULL,
	[RY_Earn1] [numeric](18, 0) NULL,
	[RY_Earn2] [numeric](18, 0) NULL,
	[RY_Earn3] [numeric](18, 0) NULL,
	[RY_Earn4] [numeric](18, 0) NULL,
	[RY_Earn5] [numeric](18, 0) NULL,
	[RY_DecExtra1] [numeric](18, 0) NULL,
	[RY_DecExtra2] [numeric](18, 0) NULL,
	[RY_DecExtra3] [numeric](18, 2) NULL,
	[RY_DecExtra4] [numeric](18, 2) NULL,
	[RY_DecExtra5] [numeric](18, 2) NULL,
	[RY_TotEarn1] [numeric](18, 2) NULL,
	[RY_TotEarn2] [numeric](18, 2) NULL,
	[RY_TotEarn3] [numeric](18, 2) NULL,
	[RY_TotEarn4] [numeric](18, 2) NULL,
	[RY_TotEarn5] [numeric](18, 2) NULL,
 CONSTRAINT [PK_RunRoyQual_Dtls] PRIMARY KEY CLUSTERED 
(
	[RY_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPLevelCommissionAmt_Mst_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPLevelCommissionAmt_Mst_log](
	[RLML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RLML_RLMID] [numeric](18, 0) NOT NULL,
	[RLML_Level] [numeric](18, 0) NULL,
	[RLML_Amt] [numeric](18, 2) NULL,
	[RLML_PvCode] [numeric](18, 0) NULL,
	[RLML_LvlFlg] [tinyint] NULL,
	[RLML_IncFlg] [tinyint] NULL,
	[RLML_DATETIME] [datetime] NULL,
	[RLML_logflg] [numeric](18, 0) NULL,
	[RLML_AdminID] [numeric](18, 0) NULL,
	[RLML_IPAdress] [varchar](100) NULL,
	[RLML_Browser] [varchar](100) NULL,
	[RLML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RPLevelCommissionAmt_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPLevelCommissionAmt_Mst](
	[RLM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RLM_Level] [numeric](18, 0) NULL,
	[RLM_Amt] [numeric](18, 2) NULL,
	[RLM_PvCode] [numeric](18, 0) NULL,
	[RLM_LvlFlg] [tinyint] NULL,
	[RLM_IncFlg] [tinyint] NULL,
 CONSTRAINT [PK_RPLevelCommissionAmt_Mst] PRIMARY KEY CLUSTERED 
(
	[RLM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPLevelAmt_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPLevelAmt_Mst](
	[RLM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RLM_Level] [numeric](18, 0) NULL,
	[RLM_Amt] [numeric](18, 2) NULL,
	[RLM_PvCode] [numeric](18, 0) NULL,
	[RLM_LvlFlg] [tinyint] NULL,
	[RLM_IncFlg] [tinyint] NULL,
 CONSTRAINT [RPK_LevelAmt_Mst] PRIMARY KEY CLUSTERED 
(
	[RLM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoyMaxpv_Dtls](
	[ROY_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ROY_Type] [numeric](18, 0) NULL,
	[ROY_Datecode] [numeric](18, 0) NULL,
	[ROY_RMID] [numeric](18, 0) NULL,
	[ROY_PVCODE] [numeric](18, 0) NULL,
	[ROY_PVTYPE] [numeric](18, 0) NULL,
	[ROY_TOCNT] [numeric](18, 0) NULL,
	[ROY_TOAMT] [numeric](18, 6) NULL,
	[ROY_CFAmt] [numeric](18, 2) NULL,
	[ROY_BFAmt] [numeric](18, 2) NULL,
	[ROY_DecExtra1] [numeric](18, 0) NULL,
	[ROY_DecExtra2] [numeric](18, 0) NULL,
	[ROY_ChvExtra1] [varchar](500) NULL,
	[ROY_ChvExtra2] [varchar](500) NULL,
 CONSTRAINT [PK_RoyMaxpv_Dtls] PRIMARY KEY CLUSTERED 
(
	[ROY_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ROYCONDITION_MST_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ROYCONDITION_MST_log](
	[RCML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RCML_RCMSrNO] [numeric](18, 0) NOT NULL,
	[RCML_Name] [varchar](500) NULL,
	[RCML_Desc] [varchar](500) NULL,
	[RCML_Flag] [numeric](18, 0) NULL,
	[RCML_Value1] [numeric](18, 0) NULL,
	[RCML_ChvExtra1] [varchar](500) NULL,
	[RCML_ChvExtra2] [varchar](500) NULL,
	[RCML_DecExtra1] [numeric](18, 6) NULL,
	[RCML_DecExtra2] [numeric](18, 6) NULL,
	[RCML_DATETIME] [datetime] NULL,
	[RCML_logflg] [numeric](18, 0) NULL,
	[RCML_AdminID] [numeric](18, 0) NULL,
	[RCML_IPAdress] [varchar](100) NULL,
	[RCML_Browser] [varchar](100) NULL,
	[RCML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Royalty_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Royalty_Mst_log](
	[RML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RML_RMID] [numeric](18, 0) NULL,
	[RML_SrNo] [numeric](18, 0) NULL,
	[RML_Name] [varchar](200) NULL,
	[RML_Desc] [varchar](200) NULL,
	[RML_Days] [numeric](18, 0) NULL,
	[RML_Left] [numeric](18, 2) NULL,
	[RML_Right] [numeric](18, 2) NULL,
	[RML_BVFlg] [tinyint] NULL,
	[RML_GroupFlg] [tinyint] NULL,
	[RML_Direct] [numeric](18, 0) NULL,
	[RML_Spill] [numeric](18, 0) NULL,
	[RML_RoyPer] [numeric](18, 2) NULL,
	[RML_Graceperiod] [numeric](18, 0) NULL,
	[RML_GraceFlg] [tinyint] NULL,
	[RML_Level] [numeric](18, 0) NULL,
	[RML_LevelCnt] [numeric](18, 0) NULL,
	[RML_LevelFlg] [tinyint] NULL,
	[RML_RoyFlg] [tinyint] NULL,
	[RML_CummAmt] [numeric](18, 2) NULL,
	[RML_CummFlg] [tinyint] NULL,
	[RML_AltVal] [numeric](18, 0) NULL,
	[RML_AltDays] [numeric](18, 0) NULL,
	[RML_AltFlg] [tinyint] NULL,
	[RML_RoyTOAmt] [numeric](18, 2) NULL,
	[RML_RoyLimit] [numeric](18, 2) NULL,
	[RML_LimitFlg] [tinyint] NULL,
	[RML_ActiveFlg] [tinyint] NULL,
	[RML_ActiveL] [numeric](18, 0) NULL,
	[RML_ActiveR] [numeric](18, 0) NULL,
	[RML_Status] [tinyint] NULL,
	[RML_decExtra1] [numeric](18, 2) NULL,
	[RML_decExtra2] [numeric](18, 2) NULL,
	[RML_chvExtra3] [varchar](50) NULL,
	[RML_chvExtra4] [varchar](50) NULL,
	[RML_chvExtra5] [varchar](50) NULL,
	[RML_DATETIME] [datetime] NULL,
	[RML_logflg] [numeric](18, 0) NULL,
	[RML_AdminID] [numeric](18, 0) NULL,
	[RML_IPAdress] [varchar](100) NULL,
	[RML_Browser] [varchar](100) NULL,
	[RML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role_Hdr](
	[RH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RH_Name] [varchar](50) NULL,
	[RH_Flag] [tinyint] NULL,
	[RH_Date] [datetime] NULL,
	[RH_UserType] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Role_Hdr] PRIMARY KEY CLUSTERED 
(
	[RH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role_Dtls_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role_Dtls_log](
	[RDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RDL_RDID] [numeric](18, 0) NOT NULL,
	[RDL_RHID] [numeric](18, 0) NULL,
	[RDL_MMID] [numeric](18, 0) NULL,
	[RDL_Flag] [tinyint] NULL,
	[RDL_Def_Flag] [tinyint] NULL,
	[RDL_datetime] [datetime] NULL,
	[RDL_logFlag] [numeric](18, 0) NULL,
	[RDL_adminid] [numeric](18, 0) NULL,
	[RDL_IPAdress] [varchar](100) NULL,
	[RDL_Browser] [varchar](100) NULL,
	[RDL_Browserversion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[withdrawl_trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[withdrawl_trn](
	[wt_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[wt_memid] [numeric](18, 0) NOT NULL,
	[wt_Date] [datetime] NOT NULL,
	[wt_Datetime] [datetime] NOT NULL,
	[wt_Amt] [numeric](18, 2) NOT NULL,
	[wt_AdminAmt] [numeric](18, 2) NOT NULL,
	[wt_Flag] [tinyint] NOT NULL,
	[wt_Remark] [varchar](8000) NULL,
	[wt_Extra1] [varchar](100) NULL,
	[wt_Extra2] [varchar](100) NULL,
	[wt_Extra3] [varchar](100) NULL,
	[wt_Extra4] [datetime] NULL,
	[wt_datecode] [datetime] NULL,
	[wt_Type] [numeric](18, 0) NOT NULL,
	[wt_IPaddress] [varchar](200) NULL,
	[wt_browerversion] [varchar](200) NULL,
	[wt_browser] [varchar](200) NULL,
	[wt_paytype] [numeric](18, 0) NULL,
	[wt_CustomerId] [int] NULL,
 CONSTRAINT [PK_withdrawl_trn] PRIMARY KEY CLUSTERED 
(
	[wt_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[visiterCounter]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[visiterCounter](
	[ImgId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Path] [varchar](50) NULL,
	[Status] [tinyint] NULL,
	[extension] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserType_Mst](
	[UTM_ID] [numeric](18, 0) NOT NULL,
	[UTM_Name] [varchar](50) NOT NULL,
	[UTM_Desc] [varchar](100) NULL,
	[UTM_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_UserType_Mst] PRIMARY KEY CLUSTERED 
(
	[UTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[TempUpline]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempUpline](
	[TU_MemID] [numeric](18, 0) NOT NULL,
	[TU_MemNo] [varchar](20) NOT NULL,
	[TU_MemName] [varchar](50) NOT NULL,
	[TU_Memdoj] [datetime] NOT NULL,
	[TU_Side] [char](10) NOT NULL,
	[TU_Mempac] [varchar](200) NOT NULL,
	[TU_State] [varchar](30) NULL,
	[TU_City] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tempPhotoGallery]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tempPhotoGallery](
	[PGE_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PGE_Srno] [numeric](18, 0) NOT NULL,
	[PGE_Name] [varchar](50) NOT NULL,
	[PGE_Alise] [varchar](100) NOT NULL,
	[PGE_Flag] [tinyint] NOT NULL,
	[PGE_ShtLngFlg] [tinyint] NOT NULL,
	[PGE_chvExtra1] [varchar](200) NULL,
	[PGE_chvExtra2] [varchar](200) NULL,
	[PGE_decExtra1] [numeric](18, 2) NULL,
	[PGE_decExtra2] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempPCR_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempPCR_Hdr](
	[TPH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TPH_PGCode] [varchar](15) NOT NULL,
	[TPH_FromFrID] [numeric](20, 0) NOT NULL,
	[TPH_ToFrID] [numeric](20, 0) NULL,
	[TPH_Date] [datetime] NOT NULL,
	[TPH_DateTime] [datetime] NOT NULL,
	[TPH_TotQty] [numeric](20, 0) NOT NULL,
	[TPH_DiscAmt] [numeric](20, 2) NOT NULL,
	[TPH_ShipAmt] [numeric](20, 2) NOT NULL,
	[TPH_NetAmt] [numeric](20, 2) NOT NULL,
	[TPH_CCTrnAmt] [numeric](20, 2) NOT NULL,
	[TPH_TrnTotalAmt] [numeric](20, 2) NOT NULL,
	[TPH_TotBVPts] [numeric](20, 2) NOT NULL,
	[TPH_TotPVPts] [numeric](20, 6) NOT NULL,
	[TPH_TotGrUnits] [numeric](20, 6) NOT NULL,
	[TPH_Paymode] [numeric](20, 0) NOT NULL,
	[TPH_ChqDDNo] [numeric](20, 0) NULL,
	[TPH_ChqDDDate] [datetime] NULL,
	[TPH_ChqDDBank] [varchar](100) NULL,
	[TPH_ChqDDBranch] [varchar](50) NULL,
	[TPH_PinSrNo] [varchar](50) NULL,
	[TPH_Remark] [varchar](500) NULL,
	[TPH_StatusFlg] [tinyint] NOT NULL,
	[TPH_StatusDate] [datetime] NULL,
	[TPH_AdminID] [numeric](20, 0) NULL,
	[TPH_IPAddress] [varchar](50) NULL,
	[TPH_Browser] [varchar](50) NULL,
	[TPH_BrowserVersion] [varchar](50) NULL,
	[TPH_chvExtra1] [varchar](100) NULL,
	[TPH_chvExtra2] [varchar](100) NULL,
	[TPH_decExtra1] [numeric](20, 2) NULL,
	[TPH_decExtra2] [numeric](20, 2) NULL,
	[TPH_dtmExtra1] [datetime] NULL,
	[TPH_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_TempPCR_Hdr] PRIMARY KEY CLUSTERED 
(
	[TPH_PGCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempPCR_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempPCR_Dtls](
	[TPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TPD_PGCode] [varchar](15) NOT NULL,
	[TPD_PVCode] [numeric](20, 0) NOT NULL,
	[TPD_BVPts] [numeric](20, 2) NOT NULL,
	[TPD_PVPts] [numeric](20, 6) NOT NULL,
	[TPD_GrUnits] [numeric](20, 6) NOT NULL,
	[TPD_Rate] [numeric](20, 2) NOT NULL,
	[TPD_VAT] [numeric](20, 2) NOT NULL,
	[TPD_MRP] [numeric](20, 2) NOT NULL,
	[TPD_Qty] [numeric](20, 0) NOT NULL,
	[TPD_ShipAmt] [numeric](20, 2) NOT NULL,
	[TPD_DiscAmt] [numeric](20, 2) NOT NULL,
	[TPD_NetAmt] [numeric](20, 2) NOT NULL,
	[TPD_TotShipAmt] [numeric](20, 2) NOT NULL,
	[TPD_TotDiscAmt] [numeric](20, 2) NOT NULL,
	[TPD_TotBVPts] [numeric](20, 2) NOT NULL,
	[TPD_TotPVPts] [numeric](20, 6) NOT NULL,
	[TPD_TotGrUnits] [numeric](20, 0) NOT NULL,
	[TPD_decExtra1] [numeric](20, 2) NULL,
	[TPD_decExtra2] [numeric](20, 2) NULL,
	[TPD_decExtra3] [numeric](20, 2) NULL,
	[TPD_decExtra4] [numeric](20, 2) NULL,
	[TPD_chvExtra1] [varchar](500) NULL,
	[TPD_chvExtra2] [varchar](500) NULL,
	[TPD_chvExtra3] [varchar](500) NULL,
	[TPD_chvExtra4] [varchar](500) NULL,
	[TPD_dtmExtra1] [datetime] NULL,
	[TPD_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_TempPCR_Dtls] PRIMARY KEY CLUSTERED 
(
	[TPD_PGCode] ASC,
	[TPD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempMemProfile_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempMemProfile_Dtls](
	[MPD_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPD_MemId] [numeric](18, 0) NOT NULL,
	[MPD_Name] [varchar](100) NOT NULL,
	[MPD_DOB] [datetime] NULL,
	[MPD_Title] [varchar](10) NULL,
	[MPD_Gender] [varchar](50) NULL,
	[MPD_MaritalStatus] [varchar](10) NULL,
	[MPD_Mother] [varchar](100) NULL,
	[MPD_Father] [varchar](100) NULL,
	[MPD_HoWo] [varchar](100) NULL,
	[MPD_PassportNo] [varchar](50) NULL,
	[MPD_IFSDCode] [varchar](20) NULL,
	[MPD_Qualification] [varchar](50) NULL,
	[MPD_Profession] [varchar](100) NULL,
	[MPD_Designation] [varchar](50) NULL,
	[MPD_Income] [varchar](50) NULL,
	[MPD_SalStatus] [varchar](50) NULL,
	[MPD_NoDepend] [numeric](18, 0) NULL,
	[MPD_TwoWheeler] [varchar](10) NULL,
	[MPD_FourWheeler] [varchar](10) NULL,
	[MPD_Age] [varchar](100) NULL,
	[MPD_Nationality] [varchar](50) NULL,
	[MPD_BloodGroup] [varchar](10) NULL,
	[MPD_ResStatus] [varchar](50) NULL,
	[MPD_PanCardNo] [varchar](50) NULL,
	[MPD_ApplyPan] [varchar](50) NULL,
	[MPD_ChqPayTo] [varchar](100) NULL,
	[MPD_Address] [varchar](400) NULL,
	[MPD_LandMark] [varchar](400) NULL,
	[MPD_City] [varchar](50) NULL,
	[MPD_District] [varchar](50) NULL,
	[MPD_State] [varchar](50) NULL,
	[MPD_Country] [varchar](50) NULL,
	[MPD_PinCode] [varchar](10) NULL,
	[MPD_Mobile] [varchar](20) NULL,
	[MPD_ResPhone] [varchar](20) NULL,
	[MPD_OffPhone] [varchar](20) NULL,
	[MPD_Email] [varchar](50) NULL,
	[MPD_Fax] [varchar](50) NULL,
	[MPD_ShpAddress] [varchar](400) NULL,
	[MPD_ShpLandMark] [varchar](400) NULL,
	[MPD_ShpCity] [varchar](50) NULL,
	[MPD_ShpDistrict] [varchar](50) NULL,
	[MPD_ShpState] [varchar](50) NULL,
	[MPD_ShpCountry] [varchar](50) NULL,
	[MPD_ShpPinCode] [numeric](18, 0) NULL,
	[MPD_ShpContPerson] [varchar](20) NULL,
	[MPD_ShpContNo] [varchar](20) NULL,
	[MPD_Bank] [varchar](100) NULL,
	[MPD_Branch] [varchar](50) NULL,
	[MPD_BankAddress] [varchar](400) NULL,
	[MPD_AccType] [varchar](30) NULL,
	[MPD_AccNo] [varchar](16) NULL,
	[MPD_AccPwd] [varchar](16) NULL,
	[MPD_nomTitle] [varchar](50) NULL,
	[MPD_nomName] [varchar](100) NULL,
	[MPD_nomGender] [varchar](10) NULL,
	[MPD_nomRel] [varchar](50) NULL,
	[MPD_nomBdate] [datetime] NULL,
	[MPD_NomAddr] [varchar](50) NULL,
	[MPD_PinSrNo] [numeric](18, 0) NULL,
	[MPD_DDNo] [numeric](18, 0) NULL,
	[MPD_DDDate] [datetime] NULL,
	[MPD_DDBank] [varchar](100) NULL,
	[MPD_DDBranch] [varchar](100) NULL,
	[MPD_DDBankAddr] [varchar](100) NULL,
	[MPD_EpinPwd] [varchar](100) NULL,
	[MPD_EWSpon] [varchar](50) NULL,
	[MPD_EWAccNo] [varchar](100) NULL,
	[MPD_EWTransfer] [decimal](18, 0) NULL,
	[MPD_FirmType] [varchar](50) NULL,
	[MPD_FirmDetail] [varchar](500) NULL,
	[MPD_ReceiptNo] [varchar](50) NOT NULL,
	[MPD_EpinCurrentLogin] [datetime] NULL,
	[MPD_EpinLastLogin] [datetime] NULL,
	[MPD_Extra1] [varchar](100) NULL,
	[MPD_Extra2] [varchar](100) NULL,
	[MPD_Extra3] [varchar](100) NULL,
	[MPD_Extra4] [varchar](100) NULL,
	[MPD_Extra5] [varchar](100) NULL,
	[MPD_Extra6] [varchar](100) NULL,
	[MPD_Extra7] [varchar](100) NULL,
	[MPD_Extra8] [varchar](100) NULL,
	[MPD_Extra9] [varchar](100) NULL,
	[MPD_Extra10] [varchar](100) NULL,
	[MPD_Extra11] [varchar](100) NULL,
	[MPD_Extra12] [varchar](100) NULL,
	[MPD_Extra13] [varchar](100) NULL,
	[MPD_Extra14] [varchar](100) NULL,
	[MPD_Extra15] [varchar](100) NULL,
	[MPD_Extra16] [varchar](100) NULL,
	[MPD_Extra17] [varchar](100) NULL,
	[MPD_Extra18] [varchar](100) NULL,
	[MPD_Extra19] [varchar](100) NULL,
	[MPD_Extra20] [varchar](100) NULL,
	[MPD_Extra21] [varchar](100) NULL,
	[MPD_Extra22] [varchar](100) NULL,
	[MPD_Extra23] [varchar](100) NULL,
	[MPD_Extra24] [varchar](100) NULL,
	[MPD_Extra25] [varchar](100) NULL,
	[MPD_Extra26] [varchar](100) NULL,
	[MPD_Extra27] [varchar](100) NULL,
	[MPD_Extra28] [varchar](100) NULL,
	[MPD_Extra29] [varchar](100) NULL,
	[MPD_Extra30] [varchar](100) NULL,
	[MPD_ImagePath] [varchar](100) NULL,
 CONSTRAINT [PK_TempMemProfile_Dtls] PRIMARY KEY CLUSTERED 
(
	[MPD_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempAwardQualifier_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempAwardQualifier_Trn](
	[TAQ_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TAQ_MemID] [numeric](18, 0) NOT NULL,
	[TAQ_OMID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_TempAwardQualifier_Trn] PRIMARY KEY CLUSTERED 
(
	[TAQ_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TempAddress_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempAddress_Dtls](
	[TAD_CompanyID] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tax_Mst_Log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tax_Mst_Log](
	[TML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TML_TMID] [numeric](20, 0) NOT NULL,
	[TML_Date] [datetime] NOT NULL,
	[TML_DateTime] [datetime] NOT NULL,
	[TML_PvCode] [numeric](20, 0) NOT NULL,
	[TML_StateID] [numeric](20, 0) NOT NULL,
	[TML_VAT] [numeric](20, 2) NOT NULL,
	[TML_CST] [numeric](20, 2) NOT NULL,
	[TML_FormC] [numeric](20, 2) NOT NULL,
	[TML_Flag] [tinyint] NOT NULL,
	[TML_decExtra1] [numeric](20, 2) NULL,
	[TML_decExtra2] [numeric](20, 2) NULL,
	[TML_chvExtra3] [varchar](50) NULL,
	[TML_chvExtra4] [varchar](50) NULL,
	[TML_LogFlg] [tinyint] NOT NULL,
	[TML_AdminID] [numeric](20, 0) NOT NULL,
	[TML_IPAddress] [varchar](50) NULL,
	[TML_Browser] [varchar](50) NULL,
	[TML_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_Tax_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[TML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier_Mst_Log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier_Mst_Log](
	[SML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SML_SMID] [numeric](20, 0) NOT NULL,
	[SML_Code] [varchar](15) NOT NULL,
	[SML_Date] [datetime] NULL,
	[SML_DateTime] [datetime] NULL,
	[SML_Name] [varchar](100) NOT NULL,
	[SML_Address] [varchar](500) NULL,
	[SML_CountryID] [numeric](20, 0) NULL,
	[SML_Country] [varchar](50) NULL,
	[SML_StateID] [numeric](20, 0) NULL,
	[SML_State] [varchar](50) NULL,
	[SML_City] [varchar](100) NULL,
	[SML_PinCode] [varchar](10) NULL,
	[SML_ContactPerson] [varchar](100) NULL,
	[SML_Mobile] [varchar](20) NULL,
	[SML_ContactNo] [varchar](20) NULL,
	[SML_Email] [varchar](50) NULL,
	[SML_VATNo] [varchar](20) NULL,
	[SML_Flag] [tinyint] NOT NULL,
	[SML_decExtra1] [numeric](20, 2) NULL,
	[SML_decExtra2] [numeric](20, 2) NULL,
	[SML_decExtra3] [numeric](20, 2) NULL,
	[SML_decExtra4] [numeric](20, 2) NULL,
	[SML_chvExtra1] [varchar](200) NULL,
	[SML_chvExtra2] [varchar](200) NULL,
	[SML_chvExtra3] [varchar](200) NULL,
	[SML_chvExtra4] [varchar](200) NULL,
	[SML_LogFlg] [tinyint] NOT NULL,
	[SML_AdminID] [numeric](20, 0) NOT NULL,
	[SML_IPAddress] [varchar](50) NULL,
	[SML_Browser] [varchar](50) NULL,
	[SML_BrowserVersion] [varchar](50) NULL,
 CONSTRAINT [PK_Supplier_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[SML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_Code] [varchar](15) NOT NULL,
	[SM_Date] [datetime] NULL,
	[SM_DateTime] [datetime] NULL,
	[SM_Name] [varchar](100) NOT NULL,
	[SM_Address] [varchar](500) NULL,
	[SM_CountryID] [numeric](20, 0) NULL,
	[SM_Country] [varchar](50) NULL,
	[SM_StateID] [numeric](20, 0) NULL,
	[SM_State] [varchar](100) NULL,
	[SM_City] [varchar](100) NULL,
	[SM_PinCode] [varchar](10) NULL,
	[SM_ContactPerson] [varchar](100) NULL,
	[SM_Mobile] [varchar](20) NULL,
	[SM_ContactNo] [varchar](20) NULL,
	[SM_Email] [varchar](50) NULL,
	[SM_VATNo] [varchar](20) NULL,
	[SM_WebFlag] [tinyint] NOT NULL,
	[SM_decEXTRA1] [numeric](20, 2) NULL,
	[SM_decEXTRA2] [numeric](20, 2) NULL,
	[SM_decEXTRA3] [numeric](20, 2) NULL,
	[SM_decEXTRA4] [numeric](20, 2) NULL,
	[SM_chvEXTRA1] [varchar](200) NULL,
	[SM_chvEXTRA2] [varchar](200) NULL,
	[SM_chvEXTRA3] [varchar](200) NULL,
	[SM_chvEXTRA4] [varchar](200) NULL,
 CONSTRAINT [PK_Supplier_Mst] PRIMARY KEY CLUSTERED 
(
	[SM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempMemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempMemJoining_Dtls](
	[MJD_MemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MJD_MemNo] [varchar](50) NULL,
	[MJD_DOJ] [smalldatetime] NOT NULL,
	[MJD_DTOJ] [datetime] NOT NULL,
	[MJD_JoinFr] [varchar](50) NULL,
	[MJD_PvCode] [numeric](18, 0) NOT NULL,
	[MJD_PvType] [numeric](18, 0) NOT NULL,
	[MJD_Paymode] [varchar](50) NULL,
	[MJD_PayType] [varchar](50) NULL,
	[MJD_Amount] [numeric](18, 2) NULL,
	[MJD_Multiplecnt] [numeric](18, 0) NOT NULL,
	[MJD_MasterID] [numeric](18, 0) NULL,
	[MJD_Introducer] [numeric](18, 0) NOT NULL,
	[MJD_AdjustedTo] [numeric](18, 0) NOT NULL,
	[MJD_LR] [varchar](50) NOT NULL,
	[MJD_RoyIntroducer] [numeric](18, 0) NULL,
	[MJD_RoyAdjustedTo] [numeric](18, 0) NULL,
	[MJD_RoyLR] [varchar](50) NULL,
	[MJD_CCTrnAmt] [numeric](18, 2) NULL,
	[MJD_TrnTotalAmt] [numeric](18, 2) NULL,
	[MJD_PGCode] [varchar](50) NULL,
 CONSTRAINT [PK_TempMemJoining_Dtls] PRIMARY KEY CLUSTERED 
(
	[MJD_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempMember_Mst_Data]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempMember_Mst_Data](
	[TM_MemId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TM_MemNo] [varchar](15) NOT NULL,
	[TM_Name] [varchar](100) NOT NULL,
	[TM_DOJ] [datetime] NOT NULL,
	[TM_DOB] [datetime] NULL,
	[TM_MaritalStatus] [varchar](10) NULL,
	[TM_Gender] [varchar](10) NOT NULL,
	[TM_PanCardNo] [varchar](50) NULL,
	[TM_Mother] [varchar](100) NULL,
	[TM_Father] [varchar](100) NULL,
	[TM_HoWo] [varchar](100) NULL,
	[TM_PassportNo] [varchar](50) NULL,
	[TM_IFSDCode] [varchar](20) NULL,
	[TM_Qualification] [varchar](50) NULL,
	[TM_Profession] [varchar](100) NULL,
	[TM_Designation] [varchar](50) NULL,
	[TM_Income] [varchar](50) NULL,
	[TM_SalStatus] [varchar](50) NULL,
	[TM_NoDepend] [numeric](18, 0) NULL,
	[TM_TwoWheeler] [varchar](10) NULL,
	[TM_FourWheeler] [varchar](10) NULL,
	[TM_Age] [varchar](100) NULL,
	[TM_Address] [varchar](400) NOT NULL,
	[TM_LandMark] [varchar](400) NULL,
	[TM_City] [varchar](50) NULL,
	[TM_District] [varchar](50) NULL,
	[TM_State] [varchar](50) NULL,
	[TM_Country] [varchar](50) NULL,
	[TM_PinCode] [numeric](18, 0) NULL,
	[TM_Mobile] [varchar](20) NULL,
	[TM_ResPhone] [varchar](20) NULL,
	[TM_OffPhone] [varchar](20) NULL,
	[TM_Email] [varchar](50) NULL,
	[TM_Fax] [varchar](50) NULL,
	[TM_Nationality] [varchar](50) NULL,
	[TM_BloodGroup] [varchar](10) NULL,
	[TM_ResStatus] [varchar](50) NULL,
	[TM_ApplyPan] [varchar](50) NULL,
	[TM_ChqPayTo] [varchar](100) NULL,
	[TM_Bank] [varchar](30) NULL,
	[TM_Branch] [varchar](50) NULL,
	[TM_BankAddress] [varchar](400) NULL,
	[TM_AccType] [varchar](16) NULL,
	[TM_AccNo] [varchar](16) NULL,
	[TM_AccPwd] [varchar](16) NULL,
	[TM_nomTitle] [varchar](50) NULL,
	[TM_nomName] [varchar](100) NULL,
	[TM_nomGender] [varchar](10) NULL,
	[TM_nomRel] [varchar](50) NULL,
	[TM_nomBdate] [datetime] NULL,
	[TM_JoinFr] [varchar](50) NULL,
	[TM_Paymode] [varchar](50) NULL,
	[TM_SrNo] [varchar](50) NULL,
	[TM_DDNo] [numeric](18, 0) NULL,
	[TM_DDDate] [datetime] NULL,
	[TM_DDBank] [varchar](100) NULL,
	[TM_DDBranch] [varchar](100) NULL,
	[TM_DDAmt] [numeric](18, 0) NOT NULL,
	[TM_EWSpon] [numeric](18, 0) NULL,
	[TM_EWAcc] [numeric](18, 0) NULL,
	[TM_Introducer] [numeric](18, 0) NULL,
	[TM_AdjustedTo] [numeric](18, 0) NULL,
	[TM_LR] [varchar](50) NULL,
	[TM_PVCode] [numeric](18, 0) NOT NULL,
	[TM_PvType] [tinyint] NULL,
	[TM_FirmType] [varchar](50) NULL,
	[TM_FirmDetail] [varchar](500) NULL,
	[TM_Epinpwd] [varchar](15) NULL,
	[TM_UserID] [varchar](15) NOT NULL,
	[TM_PWD] [varchar](15) NOT NULL,
	[TM_EWPassword] [varchar](15) NULL,
	[TM_ReceiptNo] [varchar](50) NOT NULL,
	[TM_Introducer1] [numeric](18, 0) NULL,
	[TM_AdjustedTo1] [numeric](18, 0) NULL,
	[TM_LR1] [varchar](50) NULL,
	[TM_Question] [varchar](100) NULL,
	[TM_Answer] [varchar](100) NULL,
	[TM_PayType] [varchar](50) NULL,
	[TM_Theme] [varchar](100) NOT NULL,
	[TM_Extra1] [varchar](100) NULL,
	[TM_Extra2] [varchar](100) NULL,
	[TM_Extra3] [varchar](100) NULL,
	[TM_Extra4] [varchar](100) NULL,
	[TM_Extra5] [varchar](100) NULL,
	[TM_Extra6] [varchar](100) NULL,
	[TM_Extra7] [varchar](100) NULL,
	[TM_Extra8] [varchar](100) NULL,
	[TM_Extra9] [varchar](100) NULL,
	[TM_Extra10] [varchar](100) NULL,
	[TM_Extra11] [varchar](100) NULL,
	[TM_Extra12] [varchar](100) NULL,
	[TM_Extra13] [varchar](100) NULL,
	[TM_Extra14] [varchar](100) NULL,
	[TM_Extra15] [varchar](100) NULL,
	[TM_Extra16] [varchar](100) NULL,
	[TM_Extra17] [varchar](100) NULL,
	[TM_Extra18] [varchar](100) NULL,
	[TM_Extra19] [varchar](100) NULL,
	[TM_Extra20] [varchar](100) NULL,
	[TM_introducermemno] [varchar](50) NULL,
	[TM_adjustedtomemno] [varchar](50) NULL,
	[TM_Address1] [varchar](400) NULL,
	[TM_Landmark1] [varchar](400) NULL,
	[TM_City1] [varchar](50) NULL,
	[TM_District1] [varchar](50) NULL,
	[TM_State1] [varchar](50) NULL,
	[TM_Country1] [varchar](50) NULL,
	[TM_Pincode1] [numeric](18, 0) NULL,
	[TM_Extra21] [varchar](100) NULL,
	[TM_Extra22] [varchar](100) NULL,
	[TM_Extra23] [varchar](100) NULL,
	[TM_Extra24] [varchar](100) NULL,
	[TM_Extra25] [varchar](100) NULL,
	[TM_Extra26] [varchar](100) NULL,
	[TM_Extra27] [varchar](100) NULL,
	[TM_Extra28] [varchar](100) NULL,
	[TM_Extra29] [varchar](100) NULL,
	[TM_Extra30] [varchar](100) NULL,
	[TM_Multiplecnt] [numeric](18, 0) NULL,
	[TM_MasterID] [numeric](18, 0) NULL,
	[TM_CurrentLogin] [datetime] NULL,
	[TM_LastLogin] [datetime] NULL,
	[TM_EpinCurrentLogin] [datetime] NULL,
	[TM_EpinLastLogin] [datetime] NULL,
 CONSTRAINT [PK_TempMember_Mst_Data] PRIMARY KEY CLUSTERED 
(
	[TM_MemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_TempMember_Mst_Data] UNIQUE NONCLUSTERED 
(
	[TM_MemNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempIncentive_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempIncentive_Trn](
	[IT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IT_UMID] [numeric](18, 0) NOT NULL,
	[IT_Datecode] [numeric](18, 0) NOT NULL,
	[IT_MemID] [numeric](18, 0) NOT NULL,
	[IT_type] [numeric](18, 0) NOT NULL,
	[IT_TotAmt] [numeric](18, 2) NULL,
	[IT_TotDed] [numeric](18, 2) NULL,
	[IT_BFAmt] [numeric](18, 2) NULL,
	[IT_CFAmt] [numeric](18, 2) NULL,
	[IT_NetAmt] [numeric](18, 2) NULL,
	[IT_Earn1] [numeric](18, 2) NULL,
	[IT_Earn2] [numeric](18, 2) NULL,
	[IT_Earn3] [numeric](18, 2) NULL,
	[IT_Earn4] [numeric](18, 2) NULL,
	[IT_Earn5] [numeric](18, 2) NULL,
	[IT_Earn6] [numeric](18, 2) NULL,
	[IT_Earn7] [numeric](18, 2) NULL,
	[IT_Earn8] [numeric](18, 2) NULL,
	[IT_Earn9] [numeric](18, 2) NULL,
	[IT_Earn10] [numeric](18, 2) NULL,
	[IT_Earn11] [numeric](18, 2) NULL,
	[IT_Earn12] [numeric](18, 2) NULL,
	[IT_Earn13] [numeric](18, 2) NULL,
	[IT_Earn14] [numeric](18, 2) NULL,
	[IT_Earn15] [numeric](18, 2) NULL,
	[IT_Ded1] [numeric](18, 2) NULL,
	[IT_Ded2] [numeric](18, 2) NULL,
	[IT_Ded3] [numeric](18, 2) NULL,
	[IT_Ded4] [numeric](18, 2) NULL,
	[IT_Ded5] [numeric](18, 2) NULL,
	[IT_Ded6] [numeric](18, 2) NULL,
	[IT_Ded7] [numeric](18, 2) NULL,
	[IT_Ded8] [numeric](18, 2) NULL,
	[IT_Ded9] [numeric](18, 2) NULL,
	[IT_Ded10] [numeric](18, 2) NULL,
	[IT_Ded11] [numeric](18, 2) NULL,
	[IT_Ded12] [numeric](18, 2) NULL,
	[IT_Ded13] [numeric](18, 2) NULL,
	[IT_Ded14] [numeric](18, 2) NULL,
	[IT_Ded15] [numeric](18, 2) NULL,
	[IT_Flg] [tinyint] NULL,
	[IT_SDate] [datetime] NULL,
	[IT_EDate] [datetime] NULL,
	[IT_decExtra1] [numeric](18, 2) NULL,
	[IT_decExtra2] [numeric](18, 2) NULL,
	[IT_decExtra3] [numeric](18, 2) NULL,
	[IT_decExtra4] [numeric](18, 2) NULL,
	[IT_decExtra5] [numeric](18, 2) NULL,
	[IT_chvExtra6] [varchar](100) NULL,
	[IT_chvExtra7] [varchar](100) NULL,
	[IT_chvExtra8] [varchar](100) NULL,
	[IT_dtmExtra9] [datetime] NULL,
	[IT_inyExtra10] [tinyint] NULL,
 CONSTRAINT [PK_TempIncentive_Trn_1] PRIMARY KEY CLUSTERED 
(
	[IT_ID] ASC,
	[IT_UMID] ASC,
	[IT_Datecode] ASC,
	[IT_MemID] ASC,
	[IT_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tempfileddtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tempfileddtls](
	[FM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FM_FHID] [numeric](18, 0) NULL,
	[FM_TableName] [varchar](50) NOT NULL,
	[FM_FieldName] [varchar](50) NOT NULL,
	[FM_Alias] [varchar](50) NULL,
	[FM_FieldFlag] [tinyint] NULL,
	[FM_FilterFlag] [tinyint] NULL,
	[FM_GroupByFlag] [tinyint] NULL,
	[FM_chvExtra1] [varchar](100) NULL,
	[FM_decExtra2] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempField_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempField_Dtls](
	[FM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FM_FHID] [numeric](18, 0) NULL,
	[FM_TableName] [varchar](50) NOT NULL,
	[FM_FieldName] [varchar](50) NOT NULL,
	[FM_Alias] [varchar](50) NULL,
	[FM_FieldFlag] [tinyint] NULL,
	[FM_FilterFlag] [tinyint] NULL,
	[FM_GroupByFlag] [tinyint] NULL,
	[FM_chvExtra1] [varchar](100) NULL,
	[FM_decExtra2] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempEarnDedHead_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempEarnDedHead_Mst](
	[ED_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ED_SrNo] [numeric](18, 0) NULL,
	[ED_Type] [numeric](18, 0) NULL,
	[ED_HeadType] [tinyint] NULL,
	[ED_DMID] [numeric](18, 0) NULL,
	[ED_PIMID] [numeric](18, 0) NULL,
	[ED_FieldName] [varchar](50) NULL,
	[ED_Desc] [varchar](100) NULL,
	[ED_Procedure] [varchar](100) NULL,
	[ED_title] [varchar](100) NULL,
	[ED_chvExtra1] [varchar](100) NULL,
	[ED_chvExtra2] [varchar](100) NULL,
	[ED_decExtra3] [numeric](18, 2) NULL,
	[ED_decExtra4] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockInByFR_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockInByFR_Dtls](
	[SIFD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SIFD_Code] [numeric](18, 0) NOT NULL,
	[SIFD_PVCode] [numeric](20, 0) NOT NULL,
	[SIFD_BVPts] [numeric](20, 2) NULL,
	[SIFD_PVPts] [numeric](20, 6) NULL,
	[SIFD_Rate] [numeric](20, 2) NULL,
	[SIFD_VAT] [numeric](20, 2) NULL,
	[SIFD_ShipAmt] [numeric](20, 2) NULL,
	[SIFD_MRP] [numeric](20, 2) NULL,
	[SIFD_Qty] [numeric](20, 0) NOT NULL,
	[SIFD_GrossAmt] [numeric](20, 2) NULL,
	[SIFD_DiscAmt] [numeric](20, 2) NULL,
	[SIFD_TotShipAmt] [numeric](20, 2) NULL,
	[SIFD_NetAmt] [numeric](20, 2) NULL,
	[SIFD_TotBVPts] [numeric](20, 2) NULL,
	[SIFD_TotPVPts] [numeric](20, 6) NULL,
	[SIFD_decExtra1] [numeric](20, 2) NULL,
	[SIFD_decExtra2] [numeric](20, 2) NULL,
	[SIFD_decExtra3] [numeric](20, 2) NULL,
	[SIFD_decExtra4] [numeric](20, 2) NULL,
	[SIFD_chvExtra1] [varchar](500) NULL,
	[SIFD_chvExtra2] [varchar](500) NULL,
	[SIFD_chvExtra3] [varchar](500) NULL,
	[SIFD_chvExtra4] [varchar](500) NULL,
	[SIFD_dtmExtra1] [datetime] NULL,
	[SIFD_dtmExtra2] [datetime] NULL,
	[SIFD_TotTransitQty] [numeric](18, 0) NOT NULL,
	[SIFD_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockInByFR_Dtls] PRIMARY KEY CLUSTERED 
(
	[SIFD_Code] ASC,
	[SIFD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMSVariable_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMSVariable_Mst](
	[SVM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SVM_Date] [datetime] NOT NULL,
	[SVM_DateTime] [datetime] NOT NULL,
	[SVM_TemplateID] [numeric](18, 0) NULL,
	[SVM_Name] [varchar](200) NULL,
	[SVM_Status] [tinyint] NOT NULL,
	[SVM_NotifyFlag] [tinyint] NOT NULL,
 CONSTRAINT [PK_SMSVariable_Mst] PRIMARY KEY CLUSTERED 
(
	[SVM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMSUser_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMSUser_log](
	[SUL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SUL_SUID] [numeric](18, 0) NOT NULL,
	[SUL_SMSUser] [varchar](50) NULL,
	[SUL_SMSPwd] [varchar](50) NULL,
	[SUL_Flag] [tinyint] NOT NULL,
	[SUL_datetime] [datetime] NULL,
	[SUL_logFlag] [numeric](18, 0) NULL,
	[SUL_adminid] [numeric](18, 0) NULL,
	[SUL_IPAdress] [varchar](100) NULL,
	[SUL_Browser] [varchar](100) NULL,
	[SUL_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_SMSUser_log] PRIMARY KEY CLUSTERED 
(
	[SUL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMSUser]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMSUser](
	[SU_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SU_SMSUser] [varchar](50) NULL,
	[SU_SMSPwd] [varchar](50) NULL,
	[SU_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_SMSUser] PRIMARY KEY CLUSTERED 
(
	[SU_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockOut_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockOut_Dtls](
	[SOD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SOD_SOHCode] [numeric](18, 0) NOT NULL,
	[SOD_PVCode] [numeric](20, 0) NOT NULL,
	[SOD_Qty] [numeric](20, 0) NOT NULL,
	[SOD_decExtra1] [numeric](20, 2) NULL,
	[SOD_decExtra2] [numeric](20, 2) NULL,
	[SOD_chvExtra1] [varchar](500) NULL,
	[SOD_chvExtra2] [varchar](500) NULL,
	[SOD_dtmExtra1] [datetime] NULL,
	[SOD_dtmExtra2] [datetime] NULL,
	[SOD_TotTransitQty] [numeric](18, 0) NOT NULL,
	[SOD_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockOut_Dtls] PRIMARY KEY CLUSTERED 
(
	[SOD_SOHCode] ASC,
	[SOD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockReturnByFR_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockReturnByFR_Dtls](
	[SRFD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SRFD_SRHCode] [numeric](18, 0) NOT NULL,
	[SRFD_PVCode] [numeric](20, 0) NOT NULL,
	[SRFD_Rate] [numeric](20, 2) NULL,
	[SRFD_VAT] [numeric](20, 2) NULL,
	[SRFD_Price] [numeric](20, 2) NULL,
	[SRFD_Qty] [numeric](20, 0) NOT NULL,
	[SRFD_GrossAmt] [numeric](20, 2) NULL,
	[SRFD_DiscAmt] [numeric](20, 2) NULL,
	[SRFD_NetAmt] [numeric](20, 2) NULL,
	[SRFD_decExtra1] [numeric](20, 2) NULL,
	[SRFD_decExtra2] [numeric](20, 2) NULL,
	[SRFD_chvExtra1] [varchar](500) NULL,
	[SRFD_chvExtra2] [varchar](500) NULL,
	[SRFD_dtmExtra1] [datetime] NULL,
	[SRFD_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_StockReturnByFR_Dtls] PRIMARY KEY CLUSTERED 
(
	[SRFD_SRHCode] ASC,
	[SRFD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockInByHO_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockInByHO_Dtls](
	[SID_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SID_SIHCode] [numeric](18, 0) NOT NULL,
	[SID_PVCode] [numeric](20, 0) NOT NULL,
	[SID_PurRate] [numeric](20, 2) NOT NULL,
	[SID_PurVAT] [numeric](20, 2) NOT NULL,
	[SID_PurPrice] [numeric](20, 2) NOT NULL,
	[SID_DispatchQty] [numeric](20, 2) NOT NULL,
	[SID_Qty] [numeric](20, 0) NOT NULL,
	[SID_GrossAmt] [numeric](20, 2) NOT NULL,
	[SID_DiscAmt] [numeric](20, 2) NOT NULL,
	[SID_NetAmt] [numeric](20, 2) NOT NULL,
	[SID_decExtra1] [numeric](20, 2) NULL,
	[SID_decExtra2] [numeric](20, 2) NULL,
	[SID_chvExtra1] [varchar](500) NULL,
	[SID_chvExtra2] [varchar](500) NULL,
	[SID_dtmExtra1] [datetime] NULL,
	[SID_dtmExtra2] [datetime] NULL,
	[SID_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockInByHO_Dtls] PRIMARY KEY CLUSTERED 
(
	[SID_SIHCode] ASC,
	[SID_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Profession_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Profession_Mst](
	[PM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PM_Name] [varchar](50) NULL,
	[PM_Flag] [tinyint] NOT NULL,
	[PM_decEXTRA1] [numeric](18, 2) NULL,
	[PM_decEXTRA2] [numeric](18, 2) NULL,
	[PM_chvEXTRA3] [varchar](100) NULL,
	[PM_chvEXTRA4] [varchar](100) NULL,
 CONSTRAINT [PK_Profession_Mst] PRIMARY KEY CLUSTERED 
(
	[PM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Narration_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Narration_Mst](
	[NM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NM_Date] [datetime] NOT NULL,
	[NM_DateTime] [datetime] NOT NULL,
	[NM_Title] [varchar](50) NOT NULL,
	[NM_DESC] [varchar](300) NULL,
	[NM_TMID] [numeric](18, 0) NOT NULL,
	[NM_Flag] [tinyint] NOT NULL,
	[NM_UMID] [numeric](18, 0) NOT NULL,
	[NM_LastUpdateDtm] [datetime] NOT NULL,
 CONSTRAINT [PK_Narration_Mst] PRIMARY KEY CLUSTERED 
(
	[NM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MultipleJoin_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MultipleJoin_Mst](
	[MJ_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MJ_Name] [varchar](100) NULL,
	[MJ_no_of_joining] [numeric](18, 0) NULL,
	[MJ_desc] [varchar](50) NULL,
	[MM_chvExtra1] [varchar](100) NULL,
 CONSTRAINT [PK_MultipleJoin_Mst] PRIMARY KEY CLUSTERED 
(
	[MJ_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Motivation_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Motivation_Trn](
	[MT_ID] [numeric](18, 0) NOT NULL,
	[MT_desc] [varchar](500) NULL,
	[MT_Srno] [numeric](18, 0) NULL,
	[MT_Date] [datetime] NULL,
	[MT_Curent] [tinyint] NULL,
 CONSTRAINT [PK_Motivation_Trn] PRIMARY KEY CLUSTERED 
(
	[MT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message_mst](
	[MM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MM_MemID] [varchar](100) NULL,
	[MM_MDID] [numeric](18, 0) NULL,
	[MM_Date] [datetime] NULL,
	[MM_FromTo] [varchar](MAX) NULL,
	[MM_Subject] [varchar](2000) NULL,
	[MM_Message] [varchar](5000) NULL,
	[MM_City] [varchar](50) NULL,
	[MM_Flag] [bit] NULL,
	[MM_ReadFlg] [bit] NULL,
	[MM_DeleteFlg] [bit] NULL,
	[MM_decExtra1] [numeric](18, 2) NULL,
	[MM_chvExtra2] [varchar](200) NULL,
	[MM_chvExtra3] [varchar](200) NULL,
 CONSTRAINT [PK_Message_mst] PRIMARY KEY CLUSTERED 
(
	[MM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message_dtl]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message_dtl](
	[MD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MD_MemID] [varchar](30) NULL,
	[MD_Folder] [varchar](20) NULL,
	[MD_decExtra1] [numeric](18, 2) NULL,
	[MD_chvExtra2] [varchar](200) NULL,
	[MD_chvExtra3] [varchar](200) NULL,
 CONSTRAINT [PK_Message_dtl] PRIMARY KEY CLUSTERED 
(
	[MD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemStatus_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemStatus_Mst_log](
	[MSML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MSML_MSMID] [numeric](18, 0) NOT NULL,
	[MSML_Status] [varchar](50) NOT NULL,
	[MSML_ConfFlg] [tinyint] NOT NULL,
	[MSML_Flg] [tinyint] NOT NULL,
	[MSML_datetime] [datetime] NULL,
	[MSML_logFlag] [numeric](18, 0) NULL,
	[MSML_adminid] [numeric](18, 0) NULL,
	[MSML_IPAdress] [varchar](100) NULL,
	[MSML_Browser] [varchar](100) NULL,
	[MSML_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_MemStatus_Mst_log] PRIMARY KEY CLUSTERED 
(
	[MSML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemStatus_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemStatus_Mst](
	[MSM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MSM_Status] [varchar](50) NOT NULL,
	[MSM_ConfFlg] [tinyint] NOT NULL,
	[MSM_Flg] [tinyint] NOT NULL,
 CONSTRAINT [PK_MemStatus_Mst] PRIMARY KEY CLUSTERED 
(
	[MSM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemStatus_dtls_log_hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemStatus_dtls_log_hdr](
	[MLH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLH_Datetime] [datetime] NULL,
	[MLH_AmuID] [numeric](18, 0) NULL,
	[MLH_Remark] [varchar](500) NULL,
	[MLH_Date] [datetime] NULL,
 CONSTRAINT [PK_MemStatus_dtls_log_hdr1] PRIMARY KEY CLUSTERED 
(
	[MLH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemStatus_dtls_log_dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemStatus_dtls_log_dtls](
	[MLD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLD_MLHID] [numeric](18, 0) NULL,
	[MLD_MemID] [numeric](18, 0) NULL,
	[MLD_StatusID] [numeric](18, 0) NULL,
	[MLD_PrevStatusID] [numeric](18, 0) NULL,
	[MLD_ConfDate] [datetime] NULL,
	[MLD_PaymodeId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MemStatus_dtls_log_dtls_1] PRIMARY KEY CLUSTERED 
(
	[MLD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemMenu_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemMenu_Mst_log](
	[MML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MML_MMID] [numeric](18, 0) NOT NULL,
	[MML_SrNo] [int] NOT NULL,
	[MML_ParentID] [int] NOT NULL,
	[MML_MenuName] [varchar](50) NOT NULL,
	[MML_MenuTooltip] [varchar](50) NULL,
	[MML_PageName] [varchar](100) NULL,
	[MML_MenuUrl] [varchar](100) NOT NULL,
	[MML_QuickHelp] [varchar](8000) NULL,
	[MML_HelpURL] [varchar](500) NULL,
	[MML_MainModuleID] [int] NOT NULL,
	[MML_SubModuleID] [int] NOT NULL,
	[MML_Level] [int] NOT NULL,
	[MML_RootMMID] [numeric](18, 0) NOT NULL,
	[MML_MenuIconURL] [varchar](100) NULL,
	[MML_ListNo] [int] NULL,
	[MML_AuthenticationFlg] [tinyint] NOT NULL,
	[MML_ShowFlg] [tinyint] NOT NULL,
	[MML_Status] [tinyint] NOT NULL,
	[MML_numExtra1] [numeric](18, 0) NULL,
	[MML_numExtra2] [numeric](18, 0) NULL,
	[MML_chvExtra1] [varchar](100) NULL,
	[MML_chvExtra2] [varchar](100) NULL,
	[MML_Def_ShowFlg] [tinyint] NULL,
	[MML_DATETIME] [datetime] NULL,
	[MML_logflg] [numeric](18, 0) NULL,
	[MML_AdminID] [numeric](18, 0) NULL,
	[MML_IPAdress] [varchar](100) NULL,
	[MML_Browser] [varchar](100) NULL,
	[MML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemMenu_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemMenu_Mst](
	[MM_ID] [numeric](18, 0) NOT NULL,
	[MM_SrNo] [int] NOT NULL,
	[MM_ParentID] [int] NOT NULL,
	[MM_MenuName] [varchar](50) NOT NULL,
	[MM_MenuTooltip] [varchar](50) NULL,
	[MM_PageName] [varchar](100) NULL,
	[MM_MenuUrl] [varchar](100) NOT NULL,
	[MM_QuickHelp] [varchar](8000) NULL,
	[MM_HelpURL] [varchar](500) NULL,
	[MM_MainModuleID] [int] NOT NULL,
	[MM_SubModuleID] [int] NOT NULL,
	[MM_Level] [int] NOT NULL,
	[MM_RootMMID] [numeric](18, 0) NOT NULL,
	[MM_MenuIconURL] [varchar](100) NULL,
	[MM_ListNo] [int] NULL,
	[MM_AuthenticationFlg] [tinyint] NOT NULL,
	[MM_ShowFlg] [tinyint] NOT NULL,
	[MM_Status] [tinyint] NOT NULL,
	[MM_numExtra1] [numeric](18, 0) NULL,
	[MM_numExtra2] [numeric](18, 0) NULL,
	[MM_chvExtra1] [varchar](100) NULL,
	[MM_chvExtra2] [varchar](100) NULL,
	[MM_Def_ShowFlg] [tinyint] NULL,
 CONSTRAINT [PK_TempMemMenu_Mst] PRIMARY KEY CLUSTERED 
(
	[MM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NMNews_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NMNews_Trn_Log](
	[NTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NTL_NTID] [numeric](18, 0) NOT NULL,
	[NTL_NTCatID] [numeric](18, 0) NOT NULL,
	[NTL_NTCatName] [varchar](100) NULL,
	[NTL_NTNewsDate] [datetime] NULL,
	[NTL_NTSrNo] [numeric](18, 0) NOT NULL,
	[NTL_NTHeadline] [varchar](500) NOT NULL,
	[NTL_NTShortDesc] [varchar](1000) NULL,
	[NTL_NTLongDesc] [varchar](max) NULL,
	[NTL_NTPhotoPath] [varchar](200) NULL,
	[NTL_NTValidFrom] [datetime] NULL,
	[NTL_NTValidTo] [datetime] NULL,
	[NTL_NTSource] [varchar](100) NULL,
	[NTL_NTPostedBy] [varchar](100) NULL,
	[NTL_NTCity] [varchar](100) NULL,
	[NTL_NTBreakingFlag] [tinyint] NOT NULL,
	[NTL_NTFlag] [tinyint] NOT NULL,
	[NTL_chvExtra1] [varchar](100) NULL,
	[NTL_chvExtra2] [varchar](100) NULL,
	[NTL_decExtra3] [numeric](18, 0) NULL,
	[NTL_decExtra4] [numeric](18, 0) NULL,
	[NTL_AdminID] [numeric](18, 0) NOT NULL,
	[NTL_Date] [datetime] NULL,
	[NTL_DateTime] [datetime] NULL,
	[NTL_LogFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_NMNews_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[NTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NMNews_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NMNews_Trn](
	[NT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NT_CatID] [numeric](18, 0) NOT NULL,
	[NT_SrNo] [numeric](18, 0) NOT NULL,
	[NT_NewsDate] [datetime] NULL,
	[NT_Date] [datetime] NOT NULL,
	[NT_DateTime] [datetime] NOT NULL,
	[NT_Headline] [varchar](500) NOT NULL,
	[NT_ShortDesc] [varchar](1000) NULL,
	[NT_LongDesc] [varchar](max) NULL,
	[NT_PhotoPath] [varchar](50) NULL,
	[NT_ValidFrom] [datetime] NOT NULL,
	[NT_ValidTo] [datetime] NULL,
	[NT_Source] [varchar](500) NULL,
	[NT_PostedBy] [varchar](100) NULL,
	[NT_City] [varchar](100) NULL,
	[NT_BreakingFlag] [tinyint] NOT NULL,
	[NT_Flag] [tinyint] NOT NULL,
	[NT_chvExtra1] [varchar](100) NULL,
	[NT_chvExtra2] [varchar](100) NULL,
	[NT_decExtra3] [numeric](18, 0) NULL,
	[NT_decExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_NMNews_Trn] PRIMARY KEY CLUSTERED 
(
	[NT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NMCategory_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NMCategory_Mst](
	[NCM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NCM_Date] [datetime] NOT NULL,
	[NCM_DateTime] [datetime] NOT NULL,
	[NCM_Name] [varchar](100) NOT NULL,
	[NCM_Desc] [varchar](300) NULL,
	[NCM_Flag] [tinyint] NOT NULL,
	[NCM_chvExtra1] [varchar](100) NULL,
	[NCM_chvExtra2] [varchar](100) NULL,
	[NCM_decExtra3] [numeric](18, 0) NULL,
	[NCM_decExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_NMCategory_Mst] PRIMARY KEY CLUSTERED 
(
	[NCM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NMCategory_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NMCategory_Log](
	[NCL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NCL_NCMID] [numeric](18, 0) NOT NULL,
	[NCL_NCMName] [varchar](100) NOT NULL,
	[NCL_NCMDesc] [varchar](300) NULL,
	[NCL_NCMFlag] [tinyint] NOT NULL,
	[NCL_Date] [datetime] NOT NULL,
	[NCL_DateTime] [datetime] NOT NULL,
	[NCL_AdminID] [numeric](18, 0) NOT NULL,
	[NCL_LogFlag] [tinyint] NOT NULL,
	[NCL_chvExtra1] [varchar](100) NULL,
	[NCL_chvExtra2] [varchar](100) NULL,
	[NCL_decExtra3] [numeric](18, 0) NULL,
	[NCL_decExtra4] [nchar](10) NULL,
 CONSTRAINT [PK_NMCategory_Log] PRIMARY KEY CLUSTERED 
(
	[NCL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsCategory_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsCategory_Mst](
	[NCM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NCM_Date] [datetime] NOT NULL,
	[NCM_DateTime] [datetime] NOT NULL,
	[NCM_Name] [varchar](100) NOT NULL,
	[NCM_Desc] [varchar](300) NULL,
	[NCM_Flag] [tinyint] NOT NULL,
	[NCM_chvExtra1] [varchar](100) NULL,
	[NCM_chvExtra2] [varchar](100) NULL,
	[NCM_decExtra3] [numeric](18, 0) NULL,
	[NCM_decExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_NewsCategory_Mst] PRIMARY KEY CLUSTERED 
(
	[NCM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsCategory_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsCategory_Log](
	[NCL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NCL_NCMID] [numeric](18, 0) NOT NULL,
	[NCL_NCMName] [varchar](100) NOT NULL,
	[NCL_NCMDesc] [varchar](300) NULL,
	[NCL_NCMFlag] [tinyint] NOT NULL,
	[NCL_Date] [datetime] NOT NULL,
	[NCL_DateTime] [datetime] NOT NULL,
	[NCL_AdminID] [numeric](18, 0) NOT NULL,
	[NCL_LogFlag] [tinyint] NOT NULL,
	[NCL_chvExtra1] [varchar](100) NULL,
	[NCL_chvExtra2] [varchar](100) NULL,
	[NCL_decExtra3] [numeric](18, 0) NULL,
	[NCL_decExtra4] [nchar](10) NULL,
 CONSTRAINT [PK_NewsCategory_Log] PRIMARY KEY CLUSTERED 
(
	[NCL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News_Trn_Log](
	[NTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NTL_NTID] [numeric](18, 0) NOT NULL,
	[NTL_NTCatID] [numeric](18, 0) NOT NULL,
	[NTL_NTCatName] [varchar](100) NULL,
	[NTL_NTSrNo] [numeric](18, 0) NOT NULL,
	[NTL_NTHeadline] [varchar](500) NOT NULL,
	[NTL_NTShortDesc] [varchar](1000) NULL,
	[NTL_NTLongDesc] [varchar](8000) NULL,
	[NTL_NTPhotoPath] [varchar](200) NULL,
	[NTL_NTValidFrom] [datetime] NULL,
	[NTL_NTValidTo] [datetime] NULL,
	[NTL_NTSource] [varchar](100) NULL,
	[NTL_NTPostedBy] [varchar](100) NULL,
	[NTL_NTFeatureFlg] [tinyint] NULL,
	[NTL_NTFlag] [tinyint] NOT NULL,
	[NTL_chvExtra1] [varchar](100) NULL,
	[NTL_chvExtra2] [varchar](100) NULL,
	[NTL_decExtra3] [numeric](18, 0) NULL,
	[NTL_decExtra4] [numeric](18, 0) NULL,
	[NTL_AdminID] [numeric](18, 0) NOT NULL,
	[NTL_Date] [datetime] NULL,
	[NTL_DateTime] [datetime] NULL,
	[NTL_LogFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_News_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[NTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News_Trn](
	[NT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NT_CatID] [numeric](18, 0) NOT NULL,
	[NT_SrNo] [numeric](18, 0) NOT NULL,
	[NT_Date] [datetime] NOT NULL,
	[NT_DateTime] [datetime] NOT NULL,
	[NT_Headline] [varchar](500) NOT NULL,
	[NT_ShortDesc] [varchar](1000) NULL,
	[NT_LongDesc] [varchar](8000) NULL,
	[NT_PhotoPath] [varchar](200) NULL,
	[NT_ValidFrom] [datetime] NULL,
	[NT_ValidTo] [datetime] NULL,
	[NT_Source] [varchar](100) NULL,
	[NT_PostedBy] [varchar](100) NULL,
	[NT_FeatureFlg] [tinyint] NULL,
	[NT_Flag] [tinyint] NOT NULL,
	[NT_chvExtra1] [varchar](100) NULL,
	[NT_chvExtra2] [varchar](100) NULL,
	[NT_decExtra3] [numeric](18, 0) NULL,
	[NT_decExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_News_Trn] PRIMARY KEY CLUSTERED 
(
	[NT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offer_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offer_Mst_log](
	[OML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OML_omID] [numeric](18, 0) NOT NULL,
	[OML_ScID] [numeric](18, 0) NULL,
	[OML_Serialno] [numeric](18, 0) NULL,
	[OML_Name] [varchar](200) NULL,
	[OML_Desc] [varchar](200) NULL,
	[OML_Type] [tinyint] NULL,
	[OML_Days] [numeric](18, 0) NULL,
	[OML_Leftcnt] [numeric](18, 0) NULL,
	[OML_Rightcnt] [numeric](18, 0) NULL,
	[OML_Leftbv] [decimal](18, 2) NULL,
	[OML_Rightbv] [decimal](18, 2) NULL,
	[OML_Leftpv] [decimal](18, 2) NULL,
	[OML_Rightpv] [decimal](18, 2) NULL,
	[OML_Total] [numeric](18, 0) NULL,
	[OML_BVFlg] [tinyint] NULL,
	[OML_Groupflg] [tinyint] NULL,
	[OML_Pvcode] [varchar](200) NULL,
	[OML_Direct] [numeric](18, 0) NULL,
	[OML_Spill] [numeric](18, 0) NULL,
	[OML_RoyPer] [numeric](18, 2) NULL,
	[OML_DwnOMID] [numeric](18, 0) NULL,
	[OML_DwnCnt] [numeric](18, 0) NULL,
	[OML_Graceperiod] [numeric](18, 0) NULL,
	[OML_GraceFlg] [tinyint] NULL,
	[OML_Level] [numeric](18, 0) NULL,
	[OML_LevelCnt] [numeric](18, 0) NULL,
	[OML_LevelFlg] [tinyint] NULL,
	[OML_CummAmt] [numeric](18, 2) NULL,
	[OML_CummFlg] [tinyint] NULL,
	[OML_AltVal] [numeric](18, 0) NULL,
	[OML_AltDays] [numeric](18, 0) NULL,
	[OML_AltFlg] [tinyint] NULL,
	[OML_AwardFlg] [tinyint] NULL,
	[OML_Status] [tinyint] NULL,
	[OML_decExtra1] [numeric](18, 2) NULL,
	[OML_decExtra2] [numeric](18, 2) NULL,
	[OML_chvExtra3] [varchar](100) NULL,
	[OML_chvExtra4] [varchar](100) NULL,
	[OML_chvExtra5] [varchar](200) NULL,
	[OML_LeftDirect] [numeric](18, 0) NULL,
	[OML_RightDirect] [numeric](18, 0) NULL,
	[OML_LeftSale] [numeric](18, 2) NULL,
	[OML_RightSale] [numeric](18, 2) NULL,
	[OML_DATETIME] [datetime] NULL,
	[OML_logflg] [numeric](18, 0) NULL,
	[OML_AdminID] [numeric](18, 0) NULL,
	[OML_IPAdress] [varchar](100) NULL,
	[OML_Browser] [varchar](100) NULL,
	[OML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offer_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offer_Mst](
	[OM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OM_ScID] [numeric](18, 0) NULL,
	[OM_Serialno] [numeric](18, 0) NULL,
	[OM_Name] [varchar](200) NULL,
	[OM_Desc] [varchar](200) NULL,
	[OM_Type] [tinyint] NULL,
	[OM_Days] [numeric](18, 0) NULL,
	[OM_Leftcnt] [numeric](18, 0) NULL,
	[OM_Rightcnt] [numeric](18, 0) NULL,
	[OM_Leftbv] [decimal](18, 2) NULL,
	[OM_Rightbv] [decimal](18, 2) NULL,
	[OM_Leftpv] [decimal](18, 2) NULL,
	[OM_Rightpv] [decimal](18, 2) NULL,
	[OM_Total] [numeric](18, 0) NULL,
	[OM_BVFlg] [tinyint] NULL,
	[OM_Groupflg] [tinyint] NULL,
	[OM_Pvcode] [varchar](200) NULL,
	[OM_Direct] [numeric](18, 0) NULL,
	[OM_Spill] [numeric](18, 0) NULL,
	[OM_RoyPer] [numeric](18, 2) NULL,
	[OM_DwnOMID] [numeric](18, 0) NULL,
	[OM_DwnCnt] [numeric](18, 0) NULL,
	[OM_Graceperiod] [numeric](18, 0) NULL,
	[OM_GraceFlg] [tinyint] NULL,
	[OM_Level] [numeric](18, 0) NULL,
	[OM_LevelCnt] [numeric](18, 0) NULL,
	[OM_LevelFlg] [tinyint] NULL,
	[OM_CummAmt] [numeric](18, 2) NULL,
	[OM_CummFlg] [tinyint] NULL,
	[OM_AltVal] [numeric](18, 0) NULL,
	[OM_AltDays] [numeric](18, 0) NULL,
	[OM_AltFlg] [tinyint] NULL,
	[OM_AwardFlg] [tinyint] NULL,
	[OM_Status] [tinyint] NULL,
	[OM_decExtra1] [numeric](18, 2) NULL,
	[OM_decExtra2] [numeric](18, 2) NULL,
	[OM_chvExtra3] [varchar](100) NULL,
	[OM_chvExtra4] [varchar](100) NULL,
	[OM_chvExtra5] [varchar](200) NULL,
	[OM_LeftDirect] [numeric](18, 0) NULL,
	[OM_RightDirect] [numeric](18, 0) NULL,
	[OM_LeftSale] [numeric](18, 2) NULL,
	[OM_RightSale] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Offer_Mst] PRIMARY KEY CLUSTERED 
(
	[OM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Paymode_mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Paymode_mst_log](
	[PML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PML_PMID] [numeric](18, 0) NOT NULL,
	[PML_Name] [varchar](50) NOT NULL,
	[PML_Desc] [varchar](50) NULL,
	[PML_MSMID] [numeric](20, 0) NOT NULL,
	[PML_Flag] [tinyint] NOT NULL,
	[PML_EpinFlg] [tinyint] NOT NULL,
	[PML_PaymentFlg] [tinyint] NOT NULL,
	[PML_ShoppeFlg] [tinyint] NOT NULL,
	[PML_ResaleFlg] [tinyint] NOT NULL,
	[PML_UpgradeFlg] [tinyint] NOT NULL,
	[PML_PCRFlg] [tinyint] NOT NULL,
	[PML_POFlg] [tinyint] NOT NULL,
	[PML_WalkInFlg] [tinyint] NULL,
	[PML_RegisterFlg] [tinyint] NULL,
	[PML_DefaultPaymentFlg] [tinyint] NOT NULL,
	[PML_datetime] [datetime] NULL,
	[PML_logFlag] [numeric](18, 0) NULL,
	[PML_adminid] [numeric](18, 0) NULL,
	[PML_IPAdress] [varchar](100) NULL,
	[PML_Browser] [varchar](100) NULL,
	[PML_Browserversion] [varchar](100) NULL,
	[PM_EwalletFlag] [tinyint] NULL,
 CONSTRAINT [PK_Paymode_mst_log] PRIMARY KEY CLUSTERED 
(
	[PML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentType_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentType_mst](
	[PT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PT_Type] [varchar](50) NULL,
 CONSTRAINT [PK_Payment_Type] PRIMARY KEY CLUSTERED 
(
	[PT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentGateway_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGateway_Trn](
	[PGT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PGT_PGCode] [varchar](50) NOT NULL,
	[PGT_RefNo] [varchar](1000) NULL,
	[PGT_Date] [datetime] NOT NULL,
	[PGT_DateTime] [datetime] NOT NULL,
	[PGT_TrnID] [numeric](18, 0) NOT NULL,
	[PGT_PAID] [nchar](10) NULL,
	[PGT_BID] [varchar](50) NULL,
	[PGT_BANK] [varchar](100) NULL,
	[PGT_Remarks] [varchar](1000) NULL,
	[PGT_Amt] [varchar] (50) NULL,
    [PGT_PaymodeSystemName] [varchar] (50) NULL,
    [Status] [varchar] (50)  NULL
 CONSTRAINT [PK_PaymentGateway_Trn] PRIMARY KEY CLUSTERED 
(
	[PGT_PGCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentGateway_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGateway_Mst_log](
	[PGML_ID] [int] IDENTITY(1,1) NOT NULL,
	[PGML_PGMID] [numeric](18, 0) NOT NULL,
	[PGML_Code] [varchar](200) NOT NULL,
	[PGML_Name] [varchar](200) NOT NULL,
	[PGML_Flag] [tinyint] NOT NULL,
	[PGML_Parameter1] [varchar](200) NULL,
	[PGML_Parameter2] [varchar](200) NULL,
	[PGML_Parameter3] [varchar](200) NULL,
	[PGML_Parameter4] [varchar](200) NULL,
	[PGML_Parameter5] [varchar](200) NULL,
	[PGML_Parameter6] [varchar](200) NULL,
	[PGML_Parameter7] [varchar](200) NULL,
	[PGML_Parameter8] [varchar](200) NULL,
	[PGML_Parameter9] [varchar](200) NULL,
	[PGML_Parameter10] [varchar](200) NULL,
	[PGML_Parameter11] [varchar](200) NULL,
	[PGML_Parameter12] [varchar](200) NULL,
	[PGML_Parameter13] [varchar](200) NULL,
	[PGML_Parameter14] [varchar](200) NULL,
	[PGML_Parameter15] [varchar](200) NULL,
	[PGML_Parameter16] [varchar](200) NULL,
	[PGML_Parameter17] [varchar](200) NULL,
	[PGML_Parameter18] [varchar](200) NULL,
	[PGML_Parameter19] [varchar](200) NULL,
	[PGML_Parameter20] [varchar](200) NULL,
	[PGML_Parameter21] [varchar](200) NULL,
	[PGML_Parameter22] [varchar](200) NULL,
	[PGML_Parameter23] [varchar](200) NULL,
	[PGML_Parameter24] [varchar](200) NULL,
	[PGML_Parameter25] [varchar](200) NULL,
	[PGML_Parameter26] [varchar](200) NULL,
	[PGML_Parameter27] [varchar](200) NULL,
	[PGML_Parameter28] [varchar](200) NULL,
	[PGML_Parameter29] [varchar](200) NULL,
	[PGML_Parameter30] [varchar](200) NULL,
	[PGML_Parameter31] [varchar](200) NULL,
	[PGML_Parameter32] [varchar](200) NULL,
	[PGML_Parameter33] [varchar](200) NULL,
	[PGML_Parameter34] [varchar](200) NULL,
	[PGML_Parameter35] [varchar](200) NULL,
	[PGML_Parameter36] [varchar](200) NULL,
	[PGML_Parameter37] [varchar](200) NULL,
	[PGML_Parameter38] [varchar](200) NULL,
	[PGML_Parameter39] [varchar](200) NULL,
	[PGML_Parameter40] [varchar](200) NULL,
	[PGML_Parameter41] [varchar](200) NULL,
	[PGML_Parameter42] [varchar](200) NULL,
	[PGML_Parameter43] [varchar](200) NULL,
	[PGML_Parameter44] [varchar](200) NULL,
	[PGML_Parameter45] [varchar](200) NULL,
	[PGML_Parameter46] [varchar](200) NULL,
	[PGML_Parameter47] [varchar](200) NULL,
	[PGML_Parameter48] [varchar](200) NULL,
	[PGML_Parameter49] [varchar](200) NULL,
	[PGML_Parameter50] [varchar](200) NULL,
	[PGML_datetime] [datetime] NULL,
	[PGML_logFlag] [numeric](18, 0) NULL,
	[PGML_adminid] [numeric](18, 0) NULL,
	[PGML_IPAdress] [varchar](100) NULL,
	[PGML_Browser] [varchar](100) NULL,
	[PGML_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_PaymentGateway_Mst_log] PRIMARY KEY CLUSTERED 
(
	[PGML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentGateway_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGateway_Mst](
	[PGM_ID] [int] NOT NULL,
	[PGM_Code] [varchar](200) NOT NULL,
	[PGM_Name] [varchar](200) NOT NULL,
	[PGM_Flag] [tinyint] NOT NULL,
	[PGM_Parameter1] [varchar](200) NULL,
	[PGM_Parameter2] [varchar](200) NULL,
	[PGM_Parameter3] [varchar](200) NULL,
	[PGM_Parameter4] [varchar](200) NULL,
	[PGM_Parameter5] [varchar](200) NULL,
	[PGM_Parameter6] [varchar](200) NULL,
	[PGM_Parameter7] [varchar](200) NULL,
	[PGM_Parameter8] [varchar](200) NULL,
	[PGM_Parameter9] [varchar](200) NULL,
	[PGM_Parameter10] [varchar](200) NULL,
	[PGM_Parameter11] [varchar](200) NULL,
	[PGM_Parameter12] [varchar](200) NULL,
	[PGM_Parameter13] [varchar](200) NULL,
	[PGM_Parameter14] [varchar](200) NULL,
	[PGM_Parameter15] [varchar](200) NULL,
	[PGM_Parameter16] [varchar](200) NULL,
	[PGM_Parameter17] [varchar](200) NULL,
	[PGM_Parameter18] [varchar](200) NULL,
	[PGM_Parameter19] [varchar](200) NULL,
	[PGM_Parameter20] [varchar](200) NULL,
	[PGM_Parameter21] [varchar](200) NULL,
	[PGM_Parameter22] [varchar](200) NULL,
	[PGM_Parameter23] [varchar](200) NULL,
	[PGM_Parameter24] [varchar](200) NULL,
	[PGM_Parameter25] [varchar](200) NULL,
	[PGM_Parameter26] [varchar](200) NULL,
	[PGM_Parameter27] [varchar](200) NULL,
	[PGM_Parameter28] [varchar](200) NULL,
	[PGM_Parameter29] [varchar](200) NULL,
	[PGM_Parameter30] [varchar](200) NULL,
	[PGM_Parameter31] [varchar](200) NULL,
	[PGM_Parameter32] [varchar](200) NULL,
	[PGM_Parameter33] [varchar](200) NULL,
	[PGM_Parameter34] [varchar](200) NULL,
	[PGM_Parameter35] [varchar](200) NULL,
	[PGM_Parameter36] [varchar](200) NULL,
	[PGM_Parameter37] [varchar](200) NULL,
	[PGM_Parameter38] [varchar](200) NULL,
	[PGM_Parameter39] [varchar](200) NULL,
	[PGM_Parameter40] [varchar](200) NULL,
	[PGM_Parameter41] [varchar](200) NULL,
	[PGM_Parameter42] [varchar](200) NULL,
	[PGM_Parameter43] [varchar](200) NULL,
	[PGM_Parameter44] [varchar](200) NULL,
	[PGM_Parameter45] [varchar](200) NULL,
	[PGM_Parameter46] [varchar](200) NULL,
	[PGM_Parameter47] [varchar](200) NULL,
	[PGM_Parameter48] [varchar](200) NULL,
	[PGM_Parameter49] [varchar](200) NULL,
	[PGM_Parameter50] [varchar](200) NULL,
 CONSTRAINT [PK_PaymentGateway_Mst] PRIMARY KEY CLUSTERED 
(
	[PGM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayCycle_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayCycle_Mst_log](
	[PCML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PCML_srno] [numeric](18, 0) NOT NULL,
	[PCML_cycle] [numeric](18, 0) NOT NULL,
	[PCML_SH] [numeric](18, 0) NOT NULL,
	[PCML_SM] [numeric](18, 0) NOT NULL,
	[PCML_EH] [numeric](18, 0) NOT NULL,
	[PCML_EM] [numeric](18, 0) NOT NULL,
	[PCML_Extra1] [varchar](50) NOT NULL,
	[PCML_Extra2] [varchar](50) NOT NULL,
	[PCML_Extra3] [varchar](50) NOT NULL,
	[PCML_Extra4] [varchar](50) NOT NULL,
	[PCML_Extra5] [varchar](50) NOT NULL,
	[PCML_DATETIME] [datetime] NULL,
	[PCML_logflg] [numeric](18, 0) NULL,
	[PCML_AdminID] [numeric](18, 0) NULL,
	[PCML_IPAdress] [varchar](100) NULL,
	[PCML_Browser] [varchar](100) NULL,
	[PCML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PancardConf_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PancardConf_Dtls](
	[PCD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PCD_Memid] [numeric](18, 0) NOT NULL,
	[PCD_ConfDate] [datetime] NOT NULL,
	[PCD_AdminID] [numeric](18, 0) NOT NULL,
	[PCD_IPAddress] [varchar](50) NULL,
	[PCD_Browser] [varchar](50) NULL,
	[PCD_BrowserVersion] [varchar](50) NULL,
	[PCD_decExtra1] [numeric](18, 0) NULL,
	[PCD_decExtra2] [numeric](18, 0) NULL,
	[PCD_chvExtra3] [varchar](100) NULL,
	[PCD_chvExtra4] [varchar](100) NULL,
	[PCD_chvExtra5] [varchar](100) NULL,
 CONSTRAINT [PK_PancardConf_Dtls] PRIMARY KEY CLUSTERED 
(
	[PCD_Memid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PvType_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PvType_Dtls](
	[PTD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PTD_PTMID] [numeric](18, 0) NOT NULL,
	[PTD_Type] [numeric](18, 0) NOT NULL,
	[PTD_Flag] [tinyint] NOT NULL,
	[PTD_inyExtra1] [tinyint] NULL,
 CONSTRAINT [PK_PvType_Mst] PRIMARY KEY CLUSTERED 
(
	[PTD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemStatus_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MemStatus_Dtls](
	[MSD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MSD_Memid] [numeric](18, 0) NOT NULL,
	[MSD_ConfirmDate] [datetime] NULL,
	[MSD_DirectChCnt] [numeric](18, 0) NOT NULL,
	[MSD_ConfDirectChCnt] [numeric](18, 0) NOT NULL,
	[MSD_MSMID] [numeric](18, 0) NOT NULL,
	[MSD_ConfFlg] [tinyint] NOT NULL,
	[MSD_Cycle] [numeric](18, 0) NULL,
	[MSD_FirstDtCode] [numeric](18, 0) NULL,
	[MSD_IsSpillFlg] [tinyint] NOT NULL,
	[MSD_CurrStatusID] [tinyint] NOT NULL,
	[MSD_PvCode] [numeric](18, 0) NOT NULL,
	[MSD_PvType] [numeric](18, 0) NOT NULL,
	[MSD_IncFlg] [numeric](18, 0) NULL,
	[MSD_FConfDate] [datetime] NULL,
	[MSD_decExtra1] [numeric](18, 6) NULL,
	[MSD_decExtra2] [numeric](18, 6) NULL,
	[MSD_chvExtra3] [varchar](100) NULL,
	[MSD_chvExtra4] [varchar](100) NULL,
	[MSD_dtExtra5] [datetime] NULL,
	[MSD_dtExtra6] [datetime] NULL,
 CONSTRAINT [PK_MemStatus_Dtls] PRIMARY KEY CLUSTERED 
(
	[MSD_Memid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Paidid_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Paidid_Trn_Log](
	[PTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PTL_PTTID] [numeric](18, 0) NULL,
	[PTL_Date] [datetime] NULL,
	[PTL_Datetime] [datetime] NULL,
	[PTL_AmuId] [numeric](18, 0) NULL,
	[PTL_chvExtra1] [varchar](50) NULL,
	[PTL_chvExtra2] [varchar](50) NULL,
	[PTL_decExtra3] [decimal](18, 0) NULL,
	[PTL_decExtra4] [decimal](18, 0) NULL,
	[PTL_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_Paidid_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[PTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OnlineShoppingIDData_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnlineShoppingIDData_Trn](
	[OT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OT_MemID] [numeric](18, 0) NOT NULL,
	[OT_MemNo] [varchar](500) NULL,
	[OT_TransID] [varchar](500) NOT NULL,
	[OT_Date] [datetime] NULL,
	[OT_Remark] [varchar](500) NULL,
	[OT_Ourprice] [numeric](18, 2) NULL,
	[OT_PurVal] [numeric](18, 2) NULL,
	[OT_StoreID] [numeric](18, 2) NULL,
	[OT_PvName] [varchar](500) NULL,
	[OT_Amount] [numeric](18, 2) NULL,
	[OT_RejectFlg] [tinyint] NULL,
	[OT_RejectDate] [datetime] NULL,
	[OT_ConfirmFlg] [tinyint] NULL,
	[OT_ConfirmDate] [datetime] NULL,
	[OT_DisRejectFlg] [tinyint] NULL,
	[OT_DisRejectDate] [datetime] NULL,
	[OT_Decextra1] [numeric](18, 2) NULL,
	[OT_Decextra2] [numeric](18, 0) NULL,
	[OT_ChvExtra1] [varchar](500) NULL,
	[OT_ChvExtra2] [varchar](500) NULL,
 CONSTRAINT [PK_OnlineShoppingIDData_Trn] PRIMARY KEY CLUSTERED 
(
	[OT_ID] ASC,
	[OT_TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferType_Mst](
	[OTM_ID] [numeric](18, 0) NOT NULL,
	[OTM_Desc] [varchar](100) NULL,
	[OTM_offerflag] [tinyint] NULL,
	[OTM_Awardflag] [tinyint] NULL,
 CONSTRAINT [PK_OfferType_Mst] PRIMARY KEY CLUSTERED 
(
	[OTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferAchiever_Trn_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferAchiever_Trn_Log](
	[OTL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OTL_OATID] [numeric](18, 0) NOT NULL,
	[OTL_MemID] [numeric](18, 0) NOT NULL,
	[OTL_serialno] [numeric](18, 0) NOT NULL,
	[OTL_AchvDt] [datetime] NOT NULL,
	[OTL_Status] [tinyint] NOT NULL,
	[OTL_Webflag] [tinyint] NULL,
	[OTL_Flag] [tinyint] NOT NULL,
	[OTL_ReqLcnt] [numeric](18, 0) NULL,
	[OTL_ReqRcnt] [numeric](18, 0) NULL,
	[OTL_ReqLBV] [numeric](18, 2) NULL,
	[OTL_ReqRBV] [numeric](18, 2) NULL,
	[OTL_ReqLPV] [numeric](18, 2) NULL,
	[OTL_ReqRPV] [numeric](18, 2) NULL,
	[OTL_Reqdirects] [numeric](18, 0) NULL,
	[OTL_decExtra1] [numeric](18, 2) NULL,
	[OTL_decExtra2] [numeric](18, 2) NULL,
	[OTL_chvExtra3] [varchar](100) NULL,
	[OTL_chvExtra4] [varchar](300) NULL,
	[OTL_dtmExtra5] [datetime] NULL,
	[OTL_DownMemID] [numeric](18, 0) NULL,
	[OTL_ReqLDirect] [numeric](18, 0) NULL,
	[OTL_ReqRDirect] [numeric](18, 0) NULL,
	[OTL_ReqLSale] [numeric](18, 2) NULL,
	[OTL_ReqRSale] [numeric](18, 2) NULL,
	[OTL_LogFlag] [tinyint] NOT NULL,
	[OTL_TrnDatetime] [datetime] NOT NULL,
	[OTL_AmuID] [numeric](18, 0) NOT NULL,
	[OTL_Browser] [varchar](50) NULL,
	[OTL_BrowserVersion] [varchar](50) NULL,
	[OTL_IpAddress] [varchar](50) NULL,
 CONSTRAINT [PK_OfferAchiever_Trn_Log] PRIMARY KEY CLUSTERED 
(
	[OTL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[packagechangenew_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[packagechangenew_trn](
	[pct_id] [int] IDENTITY(1,1) NOT NULL,
	[pct_memid] [numeric](18, 0) NULL,
	[pct_oldpvcode] [numeric](18, 0) NULL,
	[pct_newpvcode] [numeric](18, 0) NULL,
	[pct_trndate] [datetime] NULL,
	[pct_datetime] [datetime] NULL,
	[pct_awardprocessflg] [numeric](18, 0) NULL,
	[pct_royaltyprocessflg] [numeric](18, 0) NULL,
	[pct_remark] [varchar](300) NULL,
	[pct_smsflg] [numeric](18, 0) NULL,
	[pct_emailflg] [numeric](18, 0) NULL,
	[pct_decextra1] [numeric](18, 2) NULL,
	[pct_decextra2] [numeric](18, 2) NULL,
	[pct_chvextra1] [varchar](100) NULL,
	[pct_chvextra2] [varchar](100) NULL,
	[pct_dtextra1] [datetime] NULL,
	[pct_dtextra2] [datetime] NULL,
	[pct_adminid] [numeric](18, 0) NULL,
	[pct_ipaddress] [varchar](50) NULL,
	[pct_browser] [varchar](100) NULL,
	[pct_browserversion] [varchar](100) NULL,
	[pct_processremark] [varchar](max) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PackageChange_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PackageChange_Trn](
	[PCT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PCT_Date] [datetime] NOT NULL,
	[PCT_Datetime] [datetime] NOT NULL,
	[PCT_MemID] [numeric](18, 0) NOT NULL,
	[PCT_NewType] [numeric](18, 0) NOT NULL,
	[PCT_PrevType] [numeric](18, 0) NOT NULL,
	[PCT_NewPvCode] [numeric](18, 0) NOT NULL,
	[PCT_PrevPvCode] [numeric](18, 0) NOT NULL,
	[PCT_Amount] [numeric](18, 2) NOT NULL,
	[PCT_Remark] [varchar](500) NULL,
	[PCT_AdminID] [numeric](18, 2) NOT NULL,
	[PCT_IPAddress] [varchar](50) NULL,
	[PCT_Browser] [varchar](50) NULL,
	[PCT_BrowserVersion] [varchar](50) NULL,
	[PCT_decExtra1] [numeric](18, 2) NULL,
	[PCT_decExtra2] [numeric](18, 2) NULL,
	[PCT_chvExtra3] [varchar](200) NULL,
	[PCT_chvExtra4] [varchar](200) NULL,
 CONSTRAINT [PK_PackageChange_Trn] PRIMARY KEY CLUSTERED 
(
	[PCT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderType_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderType_Mst_log](
	[OTML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OTML_OTMID] [numeric](18, 0) NOT NULL,
	[OTML_Type] [varchar](15) NOT NULL,
	[OTML_Flg] [numeric](20, 0) NOT NULL,
	[OTML_decExtra1] [numeric](20, 2) NULL,
	[OTML_decExtra2] [numeric](20, 2) NULL,
	[OTML_chvExtra1] [varchar](500) NULL,
	[OTML_chvExtra2] [varchar](500) NULL,
	[OTML_dtmExtra1] [datetime] NULL,
	[OTML_dtmExtra2] [datetime] NULL,
	[OTML_datetime] [datetime] NULL,
	[OTML_logFlag] [numeric](18, 0) NULL,
	[OTML_adminid] [numeric](18, 0) NULL,
	[OTML_IPAdress] [varchar](100) NULL,
	[OTML_Browser] [varchar](100) NULL,
	[OTML_Browserversion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderType_Mst](
	[OTM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OTM_Type] [varchar](15) NOT NULL,
	[OTM_Flg] [numeric](20, 0) NOT NULL,
	[OTM_decExtra1] [numeric](20, 2) NULL,
	[OTM_decExtra2] [numeric](20, 2) NULL,
	[OTM_chvExtra1] [varchar](500) NULL,
	[OTM_chvExtra2] [varchar](500) NULL,
	[OTM_dtmExtra1] [datetime] NULL,
	[OTM_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_OrderType_Mst] PRIMARY KEY CLUSTERED 
(
	[OTM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Popup_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Popup_Mst](
	[PM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PM_Title] [varchar](50) NOT NULL,
	[PM_Desc] [varchar](max) NULL,
	[PM_Status] [tinyint] NOT NULL,
	[PM_ValidFrom] [datetime] NULL,
	[PM_ValidTo] [datetime] NULL,
	[PM_Date] [datetime] NULL,
	[PM_decExtra1] [numeric](18, 0) NULL,
	[PM_decExtra2] [numeric](18, 2) NULL,
	[PM_chvExtra3] [varchar](50) NULL,
	[PM_dtmExtra4] [datetime] NULL,
 CONSTRAINT [PK_Popup_Mst] PRIMARY KEY CLUSTERED 
(
	[PM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlanType_Mst_log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanType_Mst_log](
	[PML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PML_PMID] [numeric](18, 0) NOT NULL,
	[PML_PlanName] [varchar](100) NOT NULL,
	[PML_Power] [numeric](18, 0) NOT NULL,
	[PML_AutoTree] [numeric](18, 0) NULL,
	[PML_StarFlg] [tinyint] NOT NULL,
	[PML_GrDistFlg] [tinyint] NOT NULL,
	[PML_Flag] [tinyint] NOT NULL,
	[PML_decExtra1] [numeric](18, 2) NULL,
	[PML_decExtra2] [numeric](18, 2) NULL,
	[PML_chvExtra3] [varchar](100) NULL,
	[PML_chvExtra4] [varchar](100) NULL,
	[PML_DATETIME] [datetime] NULL,
	[PML_logflg] [numeric](18, 0) NULL,
	[PML_adminid] [numeric](18, 0) NULL,
	[PML_IPAdress] [varchar](100) NULL,
	[PML_Browser] [varchar](100) NULL,
	[PML_Browserversion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AchieversListEarning_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AchieversListEarning_Dtls](
	[AED_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AED_MemID] [numeric](18, 0) NOT NULL,
	[AED_PlanType] [numeric](18, 0) NOT NULL,
	[AED_EarnID] [int] NULL,
	[AED_Earnings] [numeric](18, 2) NOT NULL,
	[AED_ShowFlg] [tinyint] NOT NULL,
	[AED_TrnFlag] [tinyint] NOT NULL,
	[AED_TrnDateTime] [datetime] NOT NULL,
	[AED_Testimonial] [varchar](max) NULL,
	[AED_chvExtra1] [varchar](200) NULL,
	[AED_chvExtra2] [varchar](200) NULL,
	[AED_decExtra1] [numeric](18, 2) NULL,
	[AED_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_AchieversListEarning_Dtls_1] PRIMARY KEY CLUSTERED 
(
	[AED_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseOrder_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Hdr](
	[PH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PH_POCode] [numeric](18, 0) NOT NULL,
	[PH_SupplierID] [numeric](18, 0) NOT NULL,
	[PH_Date] [datetime] NOT NULL,
	[PH_DateTime] [datetime] NOT NULL,
	[PH_TotQty] [numeric](20, 2) NOT NULL,
	[PH_TotAmt] [numeric](20, 2) NOT NULL,
	[PH_Octroi] [numeric](20, 2) NOT NULL,
	[PH_Freight] [numeric](20, 2) NOT NULL,
	[PH_Other] [numeric](20, 2) NOT NULL,
	[PH_DiscAmt] [numeric](20, 2) NOT NULL,
	[PH_PercentFlg] [tinyint] NULL,
	[PH_Percent] [numeric](20, 2) NULL,
	[PH_NetAmt] [numeric](20, 2) NOT NULL,
	[PH_ShipName] [varchar](100) NULL,
	[PH_ShipAddress] [varchar](500) NULL,
	[PH_ShipCountryID] [numeric](20, 0) NULL,
	[PH_ShipCountry] [varchar](50) NULL,
	[PH_ShipStateID] [numeric](20, 0) NULL,
	[PH_ShipState] [varchar](50) NULL,
	[PH_ShipCity] [varchar](50) NULL,
	[PH_ShipPincode] [numeric](20, 0) NULL,
	[PH_ShipMobile] [numeric](20, 0) NULL,
	[PH_ShipContactNo] [varchar](20) NULL,
	[PH_ShipEmail] [varchar](100) NULL,
	[PH_Paymode] [numeric](18, 0) NOT NULL,
	[PH_ChqDDNo] [numeric](20, 0) NULL,
	[PH_ChqDDDate] [datetime] NULL,
	[PH_ChqDDBank] [varchar](100) NULL,
	[PH_ChqDDBranch] [varchar](50) NULL,
	[PH_PayslipImg] [varchar](100) NULL,
	[PH_PinSrNo] [numeric](20, 0) NULL,
	[PH_StatusFlag] [numeric](20, 0) NOT NULL,
	[PH_DeliveryDate] [datetime] NULL,
	[PH_StatusDate] [datetime] NULL,
	[PH_StatusDtm] [datetime] NULL,
	[PH_decExtra1] [numeric](20, 2) NULL,
	[PH_decExtra2] [numeric](20, 2) NULL,
	[PH_decExtra3] [numeric](20, 2) NULL,
	[PH_decExtra4] [numeric](20, 2) NULL,
	[PH_chvExtra1] [varchar](200) NULL,
	[PH_chvExtra2] [varchar](200) NULL,
	[PH_chvExtra3] [varchar](200) NULL,
	[PH_chvExtra4] [varchar](200) NULL,
	[PH_dtmExtra1] [datetime] NULL,
	[PH_dtmExtra2] [datetime] NULL,
	[PH_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PurchaseOrder_Hdr] PRIMARY KEY CLUSTERED 
(
	[PH_POCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseOrder_Dtls_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Dtls_Log](
	[PDL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PDL_POCode] [numeric](18, 0) NOT NULL,
	[PDL_PVCode] [numeric](20, 0) NOT NULL,
	[PDL_PurRate] [numeric](20, 2) NOT NULL,
	[PDL_PurVAT] [numeric](20, 2) NOT NULL,
	[PDL_PurPrice] [numeric](20, 2) NOT NULL,
	[PDL_Qty] [numeric](20, 0) NOT NULL,
	[PDL_GrossAmt] [numeric](20, 2) NOT NULL,
	[PDL_DiscAmt] [numeric](20, 2) NOT NULL,
	[PDL_NetAmt] [numeric](20, 2) NOT NULL,
	[PDL_decExtra1] [numeric](20, 2) NULL,
	[PDL_decExtra2] [numeric](20, 2) NULL,
	[PDL_chvExtra1] [varchar](500) NULL,
	[PDL_chvExtra2] [varchar](500) NULL,
	[PDL_dtmExtra1] [datetime] NULL,
	[PDL_dtmExtra2] [datetime] NULL,
	[PDL_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PurchaseOrder_Dtls_Log] PRIMARY KEY CLUSTERED 
(
	[PDL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Process_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Process_Mst](
	[PR_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PR_Process] [varchar](100) NOT NULL,
	[PR_Flag] [tinyint] NOT NULL,
	[PR_chvExtra1] [varchar](100) NULL,
	[PR_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_Process_Mst] PRIMARY KEY CLUSTERED 
(
	[PR_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payoutstatus_mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payoutstatus_mst](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[prcstatus] [bit] NULL,
 CONSTRAINT [PK_payoutstatus_mst] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payoutstatus]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payoutstatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[prcstatus] [int] NULL,
 CONSTRAINT [PK_payoutstatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PinCode_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PinCode_Mst](
	[PM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PM_CityID] [numeric](18, 0) NOT NULL,
	[PM_Name] [varchar](50) NOT NULL,
	[PM_PinCode] [numeric](18, 0) NOT NULL,
	[PM_Flag] [tinyint] NOT NULL,
	[PM_decExtra1] [numeric](18, 2) NULL,
	[PM_chvExtra2] [varchar](100) NULL,
	[PM_chvExtra3] [varchar](100) NULL,
 CONSTRAINT [PK_Pincode_mst] PRIMARY KEY CLUSTERED 
(
	[PM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PCR_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PCR_Dtls](
	[PD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PD_Code] [numeric](18, 0) NOT NULL,
	[PD_PVCode] [numeric](20, 0) NOT NULL,
	[PD_BVPts] [numeric](20, 2) NOT NULL,
	[PD_PVPts] [numeric](20, 6) NOT NULL,
	[PD_GrUnits] [numeric](20, 6) NOT NULL,
	[PD_Rate] [numeric](20, 2) NOT NULL,
	[PD_VAT] [numeric](20, 2) NOT NULL,
	[PD_MRP] [numeric](20, 2) NOT NULL,
	[PD_Qty] [numeric](20, 0) NOT NULL,
	[PD_ShipAmt] [numeric](20, 2) NOT NULL,
	[PD_DiscAmt] [numeric](20, 2) NOT NULL,
	[PD_NetAmt] [numeric](20, 2) NOT NULL,
	[PD_TotShipAmt] [numeric](20, 2) NOT NULL,
	[PD_TotDiscAmt] [numeric](20, 2) NOT NULL,
	[PD_TotBVPts] [numeric](20, 2) NOT NULL,
	[PD_TotPVPts] [numeric](20, 6) NOT NULL,
	[PD_TotGrUnits] [numeric](20, 0) NOT NULL,
	[PD_decExtra1] [numeric](20, 2) NULL,
	[PD_decExtra2] [numeric](20, 2) NULL,
	[PD_decExtra3] [numeric](20, 2) NULL,
	[PD_decExtra4] [numeric](20, 2) NULL,
	[PD_chvExtra1] [varchar](500) NULL,
	[PD_chvExtra2] [varchar](500) NULL,
	[PD_chvExtra3] [varchar](500) NULL,
	[PD_chvExtra4] [varchar](500) NULL,
	[PD_dtmExtra1] [datetime] NULL,
	[PD_dtmExtra2] [datetime] NULL,
	[PD_TotTransitQty] [numeric](18, 0) NOT NULL,
	[PD_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PCR_Dtls] PRIMARY KEY CLUSTERED 
(
	[PD_Code] ASC,
	[PD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaySMS_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaySMS_Trn](
	[PT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PT_Datecode] [numeric](18, 0) NULL,
	[PT_Type] [numeric](18, 0) NULL,
	[PT_MemID] [numeric](18, 0) NULL,
	[PT_Mobile] [varchar](50) NULL,
	[PT_decExtra1] [numeric](18, 0) NULL,
	[PT_decExtra2] [numeric](18, 0) NULL,
	[PT_chvExtra3] [varchar](500) NULL,
	[PT_chvExtra4] [varchar](500) NULL,
	[PT_chvExtra5] [varchar](500) NULL,
 CONSTRAINT [PK_PaySMS_Trn] PRIMARY KEY CLUSTERED 
(
	[PT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayProcessPO_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayProcessPO_Dtls](
	[PPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PPD_Code] [numeric](18, 0) NOT NULL,
	[PPD_Type] [numeric](18, 0) NOT NULL,
	[PPD_Datecode] [numeric](18, 2) NOT NULL,
	[PPD_decExtra1] [numeric](18, 2) NULL,
	[PPD_decExtra2] [numeric](18, 2) NULL,
	[PPD_chvExtra1] [varchar](50) NULL,
	[PPD_chvExtra2] [varchar](50) NULL,
 CONSTRAINT [PK_PayProcessPO_Dtls] PRIMARY KEY CLUSTERED 
(
	[PPD_Code] ASC,
	[PPD_Type] ASC,
	[PPD_Datecode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayProcess_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayProcess_Hdr](
	[PPH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PPH_Code] [numeric](18, 0) NOT NULL,
	[PPH_Date] [datetime] NOT NULL,
	[PPH_DateTime] [datetime] NULL,
	[PPH_Flag] [tinyint] NOT NULL,
	[PPH_Remark] [varchar](1000) NULL,
	[PPH_UHID] [numeric](18, 0) NULL,
	[PPH_IPAddress] [varchar](20) NULL,
	[PPH_Browser] [varchar](20) NULL,
	[PPH_BrowserVersion] [varchar](20) NULL,
	[PPH_chvExtra1] [varchar](200) NULL,
	[PPH_chvExtra2] [varchar](200) NULL,
	[PPH_decExtra1] [numeric](18, 0) NULL,
	[PPH_decExtra2] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PayProcess_Hdr] PRIMARY KEY CLUSTERED 
(
	[PPH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayProcess_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayProcess_Dtls](
	[PPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PPD_Code] [numeric](18, 0) NOT NULL,
	[PPD_MemID] [numeric](18, 0) NOT NULL,
	[PPD_AmtinFig] [numeric](18, 2) NOT NULL,
	[PPD_AmtinWords] [varchar](500) NOT NULL,
	[PPD_PaymodeID] [numeric](18, 0) NOT NULL,
	[PPD_Flag] [tinyint] NOT NULL,
	[PPD_decExtra1] [numeric](18, 2) NULL,
	[PPD_decExtra2] [numeric](18, 2) NULL,
	[PPD_chvExtra1] [varchar](500) NULL,
	[PPD_chvExtra2] [varchar](500) NULL,
 CONSTRAINT [PK_PayProcess_Dtls] PRIMARY KEY CLUSTERED 
(
	[PPD_Code] ASC,
	[PPD_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhotoGalleryEffect_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhotoGalleryEffect_Mst](
	[PGE_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PGE_Srno] [numeric](18, 0) NOT NULL,
	[PGE_Name] [varchar](50) NOT NULL,
	[PGE_Alise] [varchar](100) NOT NULL,
	[PGE_Flag] [tinyint] NOT NULL,
	[PGE_ShtLngFlg] [tinyint] NOT NULL,
	[PGE_chvExtra1] [varchar](200) NULL,
	[PGE_chvExtra2] [varchar](200) NULL,
	[PGE_decExtra1] [numeric](18, 2) NULL,
	[PGE_decExtra2] [numeric](18, 2) NULL,
 CONSTRAINT [PK_PhotoGalleryEffect_Mst] PRIMARY KEY CLUSTERED 
(
	[PGE_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PDCommission_Trn](
	[PCT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PCT_PDID] [numeric](18, 0) NOT NULL,
	[PCT_Datecode] [numeric](18, 0) NOT NULL,
	[PCT_JoinAmt] [numeric](18, 0) NOT NULL,
	[PCT_BVAmt] [numeric](18, 0) NOT NULL,
	[PCT_TotAmt] [numeric](18, 2) NOT NULL,
	[PCT_TDSAmt] [numeric](18, 2) NOT NULL,
	[PCT_Processing] [numeric](18, 2) NOT NULL,
	[PCT_Charity] [numeric](18, 2) NOT NULL,
	[PCT_TotDed] [numeric](18, 2) NOT NULL,
	[PCT_CFamt] [numeric](18, 2) NOT NULL,
	[pct_BFAmt] [numeric](18, 2) NOT NULL,
	[PCT_NetAmt] [numeric](18, 2) NOT NULL,
	[PCT_Webflag] [tinyint] NOT NULL,
	[PCT_WebstatusID] [numeric](18, 0) NOT NULL,
	[PCT_Extra1] [numeric](18, 2) NOT NULL,
	[PCT_Extra2] [numeric](18, 2) NOT NULL,
	[PCT_Extra3] [numeric](18, 2) NOT NULL,
	[PCT_Extra4] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_PDCommission_Trn] PRIMARY KEY CLUSTERED 
(
	[PCT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UOM_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UOM_Mst](
	[UM_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UM_Desc] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UOM_Mst] PRIMARY KEY CLUSTERED 
(
	[UM_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionStatus_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionStatus_Mst](
	[TSM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TSM_Type] [nvarchar](50) NOT NULL,
	[TSM_Flag] [tinyint] NOT NULL,
	[TSM_decExtra1] [numeric](18, 0) NULL,
	[TSM_chvExtra1] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction_Mst](
	[TM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TM_Name] [varchar](50) NOT NULL,
	[TM_NarrationFlg] [tinyint] NOT NULL,
	[TM_NarrationTrn] [numeric](18, 0) NULL,
	[TM_InventoryFlg] [tinyint] NOT NULL,
	[TM_GatewayFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_Transaction_Mst] PRIMARY KEY CLUSTERED 
(
	[TM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction_log](
	[TL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TL_TMID] [numeric](18, 0) NULL,
	[TL_Tile] [varchar](500) NULL,
	[TL_Description] [varchar](8000) NULL,
	[TL_AdminID] [numeric](18, 0) NULL,
	[TL_DateTime] [datetime] NULL,
	[TL_IPAddress] [varchar](100) NULL,
	[TL_Browser] [varchar](100) NULL,
	[TL_BrowserVersion] [varchar](100) NULL,
	[TL_decextra1] [numeric](18, 2) NULL,
	[TL_decextra2] [numeric](18, 2) NULL,
	[TL_chvextra1] [varchar](200) NULL,
	[TL_chvextra2] [varchar](200) NULL,
	[TL_dateExtra1] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TotalTeam_MST_log]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TotalTeam_MST_log](
	[TML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TML_tmID] [numeric](18, 0) NOT NULL,
	[TML_Name] [varchar](200) NOT NULL,
	[TML_FieldName] [varchar](100) NOT NULL,
	[TML_Table] [varchar](200) NOT NULL,
	[TML_decEXTRA1] [numeric](18, 2) NULL,
	[TML_decEXTRA2] [numeric](18, 2) NULL,
	[TML_chvEXTRA3] [varchar](100) NULL,
	[TML_chvEXTRA4] [varchar](100) NULL,
	[TML_ShowFlag] [tinyint] NULL,
	[TML_DATETIME] [datetime] NULL,
	[TML_logflg] [numeric](18, 0) NULL,
	[TML_AdminID] [numeric](18, 0) NULL,
	[TML_IPAdress] [varchar](100) NULL,
	[TML_Browser] [varchar](100) NULL,
	[TML_BrowserVersion] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TotalTeam_MST]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TotalTeam_MST](
	[TM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TM_Name] [varchar](200) NOT NULL,
	[TM_FieldName] [varchar](100) NOT NULL,
	[TM_Table] [varchar](200) NOT NULL,
	[TM_decEXTRA1] [numeric](18, 2) NULL,
	[TM_decEXTRA2] [numeric](18, 2) NULL,
	[TM_chvEXTRA3] [varchar](100) NULL,
	[TM_chvEXTRA4] [varchar](100) NULL,
	[TM_ShowFlag] [tinyint] NULL,
 CONSTRAINT [PK_TotalTeam_MST] PRIMARY KEY CLUSTERED 
(
	[TM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TmpFindGenology]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TmpFindGenology](
	[TG_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TG_level] [numeric](18, 0) NOT NULL,
	[TG_MemID] [numeric](18, 0) NOT NULL,
	[TG_MemNo] [varchar](15) NOT NULL,
	[TG_MemName] [varchar](150) NOT NULL,
	[TG_Position] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Tmp_Genology1] PRIMARY KEY CLUSTERED 
(
	[TG_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Timezone_mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Timezone_mst](
	[TM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TM_Timezone] [varchar](100) NOT NULL,
	[TM_Hours] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_Timezone_mst] PRIMARY KEY CLUSTERED 
(
	[TM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketStatus_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketStatus_Mst](
	[TSM_ID] [tinyint] NOT NULL,
	[TSM_Name] [varchar](50) NOT NULL,
	[TSM_StatusFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_TicketStatus_Mst] PRIMARY KEY CLUSTERED 
(
	[TSM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketPriority_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketPriority_Mst](
	[TPM_ID] [tinyint] NOT NULL,
	[TPM_Name] [varchar](50) NOT NULL,
	[TPM_StatusFlg] [tinyint] NOT NULL,
 CONSTRAINT [PK_TicketPriority_Mst] PRIMARY KEY CLUSTERED 
(
	[TPM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockReturnByHO_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockReturnByHO_Dtls](
	[SRD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SRD_SRHCode] [numeric](18, 0) NOT NULL,
	[SRD_PVCode] [numeric](20, 0) NOT NULL,
	[SRD_PurRate] [numeric](20, 2) NOT NULL,
	[SRD_PurVAT] [numeric](20, 2) NOT NULL,
	[SRD_PurPrice] [numeric](20, 2) NOT NULL,
	[SRD_Qty] [numeric](20, 0) NOT NULL,
	[SRD_GrossAmt] [numeric](20, 2) NOT NULL,
	[SRD_DiscAmt] [numeric](20, 2) NOT NULL,
	[SRD_NetAmt] [numeric](20, 2) NOT NULL,
	[SRD_AssignQty] [numeric](20, 0) NOT NULL,
	[SRD_decExtra1] [numeric](20, 2) NULL,
	[SRD_decExtra2] [numeric](20, 2) NULL,
	[SRD_chvExtra1] [varchar](500) NULL,
	[SRD_chvExtra2] [varchar](500) NULL,
	[SRD_dtmExtra1] [datetime] NULL,
	[SRD_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_StockReturnByHO_Dtls] PRIMARY KEY CLUSTERED 
(
	[SRD_SRHCode] ASC,
	[SRD_PVCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockReturnByFR_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockReturnByFR_Hdr](
	[SRFH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SRFH_Code] [numeric](18, 0) NOT NULL,
	[SRFH_StkInCode] [varchar](15) NULL,
	[SRFH_Date] [datetime] NOT NULL,
	[SRFH_DateTime] [datetime] NOT NULL,
	[SRFH_ByFrID] [numeric](18, 0) NULL,
	[SRFH_ToFrID] [numeric](18, 0) NULL,
	[SRFH_TrnFlag] [tinyint] NOT NULL,
	[SRFH_TotQty] [numeric](20, 0) NOT NULL,
	[SRFH_Remarks] [varchar](500) NULL,
	[SRFH_DelMode] [tinyint] NULL,
	[SRFH_DelBy] [numeric](20, 2) NULL,
	[SRFH_DelRefNo] [varchar](50) NULL,
	[SRFH_DelRefInfo] [varchar](50) NULL,
	[SRFH_DelDate] [datetime] NULL,
	[SRFH_DelRemarks] [varchar](500) NULL,
	[SRFH_IPAddress] [varchar](50) NOT NULL,
	[SRFH_Browser] [varchar](50) NOT NULL,
	[SRFH_BrowserVersion] [varchar](50) NOT NULL,
	[SRFH_StatusFlg] [tinyint] NOT NULL,
	[SRFH_StatusDate] [datetime] NULL,
	[SRFH_AcptAdminID] [numeric](20, 0) NULL,
	[SRFH_AcptIPAddress] [varchar](50) NULL,
	[SRFH_AcptBrowser] [varchar](50) NULL,
	[SRFH_AcptBrowserVersion] [varchar](50) NULL,
	[SRFH_decExtra1] [numeric](20, 2) NULL,
	[SRFH_decExtra2] [numeric](20, 2) NULL,
	[SRFH_chvExtra1] [varchar](200) NULL,
	[SRFH_chvExtra2] [varchar](200) NULL,
	[SRFH_dtmExtra1] [datetime] NULL,
	[SRFH_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_StockReturnByFR_Hdr] PRIMARY KEY CLUSTERED 
(
	[SRFH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PCR_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PCR_Hdr](
	[PH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PH_Code] [numeric](18, 0) NOT NULL,
	[PH_FromFrID] [numeric](18, 0) NOT NULL,
	[PH_ToFrID] [numeric](18, 0) NULL,
	[PH_Date] [datetime] NOT NULL,
	[PH_DateTime] [datetime] NOT NULL,
	[PH_TotQty] [numeric](20, 0) NOT NULL,
	[PH_DiscAmt] [numeric](20, 2) NOT NULL,
	[PH_ShipAmt] [numeric](20, 2) NOT NULL,
	[PH_NetAmt] [numeric](20, 2) NOT NULL,
	[PH_CCTrnAmt] [numeric](20, 2) NOT NULL,
	[PH_TrnTotalAmt] [numeric](20, 2) NOT NULL,
	[PH_TotBVPts] [numeric](20, 2) NOT NULL,
	[PH_TotPVPts] [numeric](20, 6) NOT NULL,
	[PH_TotGrUnits] [numeric](20, 6) NOT NULL,
	[PH_Paymode] [numeric](18, 0) NOT NULL,
	[PH_ChqDDNo] [numeric](20, 0) NULL,
	[PH_ChqDDDate] [datetime] NULL,
	[PH_ChqDDBank] [varchar](100) NULL,
	[PH_ChqDDBranch] [varchar](50) NULL,
	[PH_PinSrNo] [varchar](50) NULL,
	[PH_PGCode] [varchar](15) NULL,
	[PH_Remark] [varchar](500) NULL,
	[PH_StatusFlg] [tinyint] NOT NULL,
	[PH_StatusDate] [datetime] NULL,
	[PH_AdminID] [numeric](20, 0) NULL,
	[PH_IPAddress] [varchar](50) NULL,
	[PH_Browser] [varchar](50) NULL,
	[PH_BrowserVersion] [varchar](50) NULL,
	[PH_chvExtra1] [varchar](100) NULL,
	[PH_chvExtra2] [varchar](100) NULL,
	[PH_decExtra1] [numeric](20, 2) NULL,
	[PH_decExtra2] [numeric](20, 2) NULL,
	[PH_dtmExtra1] [datetime] NULL,
	[PH_dtmExtra2] [datetime] NULL,
	[PH_RejectedFrID] [numeric](20, 0) NULL,
	[PH_TotTransitQty] [numeric](18, 0) NOT NULL,
	[PH_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PCR_Hdr] PRIMARY KEY CLUSTERED 
(
	[PH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Process_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Process_Hdr](
	[PH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PH_GUID] [varchar](50) NOT NULL,
	[PH_DateCode] [numeric](18, 0) NOT NULL,
	[PH_Type] [numeric](18, 0) NOT NULL,
	[PH_POSDate] [datetime] NOT NULL,
	[PH_POEDate] [datetime] NOT NULL,
	[PH_PRID] [numeric](18, 0) NOT NULL,
	[PH_TrnDate] [datetime] NULL,
	[PH_SDateTime] [datetime] NULL,
	[PH_EDateTime] [datetime] NULL,
	[PH_StatusFlg] [tinyint] NOT NULL,
	[PH_Activeflg] [tinyint] NOT NULL,
	[PH_CalcFlg] [tinyint] NOT NULL,
	[PH_SimFlg] [tinyint] NOT NULL,
	[PH_SimPercent] [numeric](18, 2) NOT NULL,
	[PH_DednFlg] [tinyint] NOT NULL,
	[PH_ErrFlg] [tinyint] NOT NULL,
	[PH_ErrMsg] [varchar](200) NULL,
	[PH_decExtra1] [numeric](18, 2) NULL,
	[PH_decExtra2] [numeric](18, 2) NULL,
	[PH_decExtra3] [numeric](18, 2) NULL,
	[PH_chvExtra4] [varchar](200) NULL,
	[PH_chvExtra5] [varchar](200) NULL,
	[PH_chvExtra6] [varchar](200) NULL,
 CONSTRAINT [PK_Process_Hdr] PRIMARY KEY CLUSTERED 
(
	[PH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Process_Hdr] UNIQUE NONCLUSTERED 
(
	[PH_GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offer_Hdr](
	[OH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OH_SrNo] [numeric](18, 0) NOT NULL,
	[OH_SMID] [numeric](18, 0) NOT NULL,
	[OH_Name] [varchar](50) NOT NULL,
	[OH_Desc] [varchar](500) NULL,
	[OH_PvCode] [numeric](18, 0) NULL,
	[OH_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_Offer_Hdr] PRIMARY KEY CLUSTERED 
(
	[OH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferTempForceful_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferTempForceful_Trn](
	[OFT_MemID] [numeric](18, 0) NOT NULL,
	[OFT_MemNo] [varchar](50) NOT NULL,
	[OFT_Name] [varchar](100) NOT NULL,
	[OFT_DOJ] [datetime] NOT NULL,
	[OFT_OHID] [numeric](18, 0) NOT NULL,
	[OFT_OHName] [varchar](100) NOT NULL,
	[OFT_Status] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OfferClaim_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OfferClaim_Trn](
	[OCT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OCT_MemID] [numeric](18, 0) NOT NULL,
	[OCT_OHID] [numeric](18, 0) NOT NULL,
	[OCT_RequestDt] [datetime] NULL,
	[OCT_Status] [tinyint] NULL,
	[OCT_StatusDt] [datetime] NULL,
	[OCT_Webflag] [tinyint] NULL,
	[OCT_Flag] [tinyint] NULL,
	[OCT_decExtra1] [numeric](18, 2) NULL,
	[OCT_decExtra2] [numeric](18, 2) NULL,
	[OCT_chvExtra3] [varchar](100) NULL,
	[OCT_chvExtra4] [varchar](100) NULL,
	[OCT_dtmExtra5] [datetime] NULL,
 CONSTRAINT [PK_OfferClaim_Trn] PRIMARY KEY CLUSTERED 
(
	[OCT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MemGracePeriod_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemGracePeriod_Trn](
	[MGT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MGT_MemID] [numeric](18, 0) NOT NULL,
	[MGT_OHID] [numeric](18, 0) NOT NULL,
	[MGT_Date] [datetime] NOT NULL,
	[MGT_GracePeriod] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MemGracePeriod_Trn] PRIMARY KEY CLUSTERED 
(
	[MGT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DwnLnOfferAchiever_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DwnLnOfferAchiever_Trn](
	[DOA_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOA_MemID] [numeric](18, 0) NOT NULL,
	[DOA_PLcnt] [numeric](18, 0) NOT NULL,
	[DOA_PRCnt] [numeric](18, 0) NOT NULL,
	[DOA_OHID] [numeric](18, 0) NOT NULL,
	[DOA_Date] [datetime] NOT NULL,
	[DOA_decExtra1] [numeric](18, 2) NULL,
	[DOA_decExtra2] [numeric](18, 2) NULL,
	[DOA_chvExtra3] [varchar](100) NULL,
	[DOA_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_DwnLnOfferAchiever_Trn] PRIMARY KEY CLUSTERED 
(
	[DOA_MemID] ASC,
	[DOA_OHID] ASC,
	[DOA_Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardDispatch_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardDispatch_trn](
	[ADT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ADT_OHID] [numeric](18, 0) NOT NULL,
	[ADT_MJDMemID] [numeric](18, 0) NOT NULL,
	[ADT_Date] [datetime] NOT NULL,
	[ADT_DMID] [numeric](18, 0) NOT NULL,
	[ADT_Remarks] [varchar](50) NULL,
	[ADT_Extra1] [varchar](50) NULL,
	[ADT_Extra2] [varchar](50) NULL,
	[ADT_Extra3] [varchar](50) NULL,
	[ADT_Extra4] [varchar](50) NULL,
	[ADT_Extra5] [varchar](50) NULL,
 CONSTRAINT [PK_AwardDispatch_trn] PRIMARY KEY CLUSTERED 
(
	[ADT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockInByHO_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockInByHO_Hdr](
	[SIH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SIH_Code] [numeric](18, 0) NOT NULL,
	[SIH_PO] [numeric](18, 0) NULL,
	[SIH_Date] [datetime] NOT NULL,
	[SIH_DateTime] [datetime] NOT NULL,
	[SIH_SMID] [numeric](18, 0) NOT NULL,
	[SIH_TotalQty] [numeric](20, 0) NOT NULL,
	[SIH_TotalAmt] [numeric](20, 2) NOT NULL,
	[SIH_Remarks] [varchar](500) NULL,
	[SIH_DeliveryMode] [tinyint] NOT NULL,
	[SIH_DeliveredBy] [varchar](200) NULL,
	[SIH_DelRefNo] [varchar](50) NULL,
	[SIH_DelRefInfo] [varchar](50) NULL,
	[SIH_DelRemarks] [varchar](500) NULL,
	[SIH_AdminID] [numeric](20, 0) NOT NULL,
	[SIH_IPAddress] [varchar](50) NOT NULL,
	[SIH_Browser] [varchar](50) NOT NULL,
	[SIH_BrowserVersion] [varchar](50) NOT NULL,
	[SIH_decExtra1] [numeric](20, 2) NULL,
	[SIH_decExtra2] [numeric](20, 2) NULL,
	[SIH_chvExtra1] [varchar](500) NULL,
	[SIH_chvExtra2] [varchar](500) NULL,
	[SIH_dtmExtra1] [datetime] NULL,
	[SIH_dtmExtra2] [datetime] NULL,
	[SIH_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockInByHO_Hdr] PRIMARY KEY CLUSTERED 
(
	[SIH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockReturnByHO_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockReturnByHO_Hdr](
	[SRH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SRH_Code] [numeric](18, 0) NOT NULL,
	[SRH_StkInCode] [varchar](15) NULL,
	[SRH_Date] [datetime] NOT NULL,
	[SRH_DateTime] [datetime] NOT NULL,
	[SRH_SupplierID] [numeric](18, 0) NOT NULL,
	[SRH_Remarks] [varchar](100) NULL,
	[SRH_DeliveryMode] [tinyint] NOT NULL,
	[SRH_DeliveryBy] [varchar](20) NULL,
	[SRH_DelRefNo] [varchar](50) NULL,
	[SRH_DelRefInfo] [varchar](50) NULL,
	[SRH_DelDate] [datetime] NULL,
	[SRH_DelRemarks] [varchar](100) NULL,
	[SRH_AdminID] [numeric](20, 0) NOT NULL,
	[SRH_IPAddress] [varchar](50) NOT NULL,
	[SRH_Browser] [varchar](50) NOT NULL,
	[SRH_BrowserVersion] [varchar](50) NOT NULL,
	[SRH_decExtra1] [numeric](20, 2) NULL,
	[SRH_decExtra2] [numeric](20, 2) NULL,
	[SRH_chvExtra1] [varchar](500) NULL,
	[SRH_chvExtra2] [varchar](500) NULL,
	[SRH_dtmExtra1] [datetime] NULL,
	[SRH_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_StockReturnByHO_Hdr] PRIMARY KEY CLUSTERED 
(
	[SRH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TempMemLogin_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempMemLogin_Dtls](
	[MLD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MLD_MemID] [numeric](18, 0) NULL,
	[MLD_Login] [varchar](50) NULL,
	[MLD_pwd] [varchar](50) NULL,
	[MLD_Theme] [varchar](50) NULL,
	[MLD_CurrentLogin] [datetime] NULL,
	[MLD_LastLogin] [datetime] NULL,
	[MLD_HQuestion] [varchar](100) NULL,
	[MLD_HAnswer] [varchar](100) NULL,
	[MLD_decEXTRA1] [numeric](18, 2) NULL,
	[MLD_decEXTRA2] [numeric](18, 2) NULL,
	[MLD_chvEXTRA3] [varchar](50) NULL,
	[MLD_chvEXTRA4] [varchar](50) NULL,
 CONSTRAINT [PK_TempMemLogin_Dtls] PRIMARY KEY CLUSTERED 
(
	[MLD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role_Dtls](
	[RD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RD_RHID] [numeric](18, 0) NULL,
	[RD_MMID] [numeric](18, 0) NULL,
	[RD_Flag] [tinyint] NULL,
	[RD_Def_Flag] [tinyint] NULL,
 CONSTRAINT [PK_Role_Dtls] PRIMARY KEY CLUSTERED 
(
	[RD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BinInc_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BinInc_Trn](
	[BI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BI_DateCode] [numeric](18, 0) NOT NULL,
	[BI_MemID] [numeric](18, 0) NOT NULL,
	[BI_Type] [numeric](18, 0) NOT NULL,
	[BI_Earn1] [numeric](18, 2) NOT NULL,
	[BI_Earn2] [numeric](18, 2) NOT NULL,
	[BI_Earn3] [numeric](18, 2) NOT NULL,
	[BI_Earn4] [numeric](18, 2) NOT NULL,
	[BI_Earn5] [numeric](18, 2) NOT NULL,
	[BI_Earn6] [numeric](18, 2) NOT NULL,
	[BI_Earn7] [numeric](18, 2) NOT NULL,
	[BI_Earn8] [numeric](18, 2) NOT NULL,
	[BI_Earn9] [numeric](18, 2) NOT NULL,
	[BI_Earn10] [numeric](18, 2) NOT NULL,
	[BI_Earn11] [numeric](18, 2) NOT NULL,
	[BI_Earn12] [numeric](18, 2) NOT NULL,
	[BI_Earn13] [numeric](18, 2) NOT NULL,
	[BI_Earn14] [numeric](18, 2) NOT NULL,
	[BI_Earn15] [numeric](18, 2) NOT NULL,
	[BI_LeftChCnt] [numeric](18, 2) NOT NULL,
	[BI_RightChCnt] [numeric](18, 2) NOT NULL,
	[BI_LeftCF] [numeric](18, 2) NOT NULL,
	[BI_RightCF] [numeric](18, 2) NOT NULL,
	[BI_NewSetB10] [numeric](18, 0) NOT NULL,
	[BI_NewSetA10] [numeric](18, 0) NOT NULL,
	[BI_Sets1] [numeric](18, 0) NOT NULL,
	[BI_Sets2] [numeric](18, 0) NOT NULL,
	[BI_Sets3] [numeric](18, 0) NOT NULL,
	[BI_Flag] [tinyint] NOT NULL,
	[BI_LeftBF] [numeric](18, 2) NOT NULL,
	[BI_RightBF] [numeric](18, 2) NOT NULL,
	[BI_NewLeft] [numeric](18, 2) NOT NULL,
	[BI_NewRight] [numeric](18, 2) NOT NULL,
	[BI_silverBF] [numeric](18, 2) NOT NULL,
	[BI_SilverNew] [numeric](18, 2) NOT NULL,
	[BI_SilverCF] [numeric](18, 2) NOT NULL,
	[BI_SimBAmt] [numeric](18, 2) NULL,
	[BI_SimAAmt] [numeric](18, 2) NULL,
	[BI_decExtra1] [numeric](18, 2) NOT NULL,
	[BI_decExtra2] [numeric](18, 2) NOT NULL,
	[BI_decExtra3] [numeric](18, 2) NOT NULL,
	[BI_decExtra4] [numeric](18, 2) NOT NULL,
	[BI_chvExtra5] [varchar](MAX) NOT NULL,
	[BI_chvExtra6] [varchar](MAX) NOT NULL,
 CONSTRAINT [PK_BinInc_Trn] PRIMARY KEY CLUSTERED 
(
	[BI_DateCode] ASC,
	[BI_MemID] ASC,
	[BI_Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RPLevelInc_trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPLevelInc_trn](
	[RLI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RLI_MemID] [numeric](18, 0) NULL,
	[RLI_Type] [numeric](18, 0) NULL,
	[RLI_Datecode] [numeric](18, 0) NULL,
	[RLI_Level] [numeric](18, 0) NULL,
	[RLI_AmtPerID] [numeric](18, 2) NULL,
	[RLI_Count] [numeric](18, 0) NULL,
	[RLI_Amount] [numeric](18, 2) NULL,
	[RLI_PvCode] [numeric](18, 0) NULL,
	[RLI_decExtra1] [numeric](18, 2) NULL,
	[RLI_decExtra2] [numeric](18, 2) NULL,
	[RLI_decExtra3] [numeric](18, 2) NULL,
	[RLI_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_RPLevelInc_trn] PRIMARY KEY CLUSTERED 
(
	[RLI_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoyInc_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoyInc_Trn](
	[RI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RI_DateCode] [numeric](18, 0) NOT NULL,
	[RI_Type] [numeric](18, 0) NOT NULL,
	[RI_MemID] [numeric](18, 0) NOT NULL,
	[RI_Earn0] [numeric](18, 2) NOT NULL,
	[RI_Earn1] [numeric](18, 2) NOT NULL,
	[RI_Earn2] [numeric](18, 2) NOT NULL,
	[RI_Earn3] [numeric](18, 2) NOT NULL,
	[RI_Earn4] [numeric](18, 2) NOT NULL,
	[RI_Earn5] [numeric](18, 2) NOT NULL,
	[RI_Earn6] [numeric](18, 2) NOT NULL,
	[RI_CummEarningFlg] NUMERIC(18,0),
	[RI_PayoutLimitFlg] NUMERIC(18,0),
	[RI_Earningamt] NUMERIC(18,2),
	[RI_Differenceamt] NUMERIC(18,2),
	[RI_Cappingamt] NUMERIC(18,2)
 CONSTRAINT [PK_RoyInc_Trn] PRIMARY KEY CLUSTERED 
(
	[RI_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelInc_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelInc_trn](
	[LI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LI_MemID] [numeric](18, 0) NULL,
	[LI_Type] [numeric](18, 0) NULL,
	[LI_Datecode] [numeric](18, 0) NULL,
	[LI_Level] [numeric](18, 0) NULL,
	[LI_AmtPerID] [numeric](18, 2) NULL,
	[LI_Count] [numeric](18, 0) NULL,
	[LI_Amount] [numeric](18, 2) NULL,
	[LI_PvCode] [numeric](18, 0) NULL,
	[LI_decExtra1] [numeric](18, 2) NULL,
	[LI_decExtra2] [numeric](18, 2) NULL,
	[LI_decExtra3] [numeric](18, 2) NULL,
	[LI_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_LevelInc_trn] PRIMARY KEY CLUSTERED 
(
	[LI_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LevelCommissionInc_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LevelCommissionInc_trn](
	[LI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[LI_MemID] [numeric](18, 0) NULL,
	[LI_Type] [numeric](18, 0) NULL,
	[LI_Datecode] [numeric](18, 0) NULL,
	[LI_Level] [numeric](18, 0) NULL,
	[LI_AmtPerID] [numeric](18, 2) NULL,
	[LI_Count] [numeric](18, 2) NULL,
	[LI_Amount] [numeric](18, 2) NULL,
	[LI_PvCode] [numeric](18, 0) NULL,
	[LI_decExtra1] [numeric](18, 2) NULL,
	[LI_decExtra2] [numeric](18, 2) NULL,
	[LI_decExtra3] [numeric](18, 2) NULL,
	[LI_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_LevelCommissionInc_trn] PRIMARY KEY CLUSTERED 
(
	[LI_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Incentive_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incentive_Trn](
	[IT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IT_Datecode] [numeric](18, 0) NOT NULL,
	[IT_MemID] [numeric](18, 0) NOT NULL,
	[IT_type] [numeric](18, 0) NOT NULL,
	[IT_TotAmt] [numeric](18, 2) NULL,
	[IT_TotDed] [numeric](18, 2) NULL,
	[IT_BFAmt] [numeric](18, 2) NULL,
	[IT_CFAmt] [numeric](18, 2) NULL,
	[IT_NetAmt] [numeric](18, 2) NULL,
	[IT_Earn1] [numeric](18, 2) NULL,
	[IT_Earn2] [numeric](18, 2) NULL,
	[IT_Earn3] [numeric](18, 2) NULL,
	[IT_Earn4] [numeric](18, 2) NULL,
	[IT_Earn5] [numeric](18, 2) NULL,
	[IT_Earn6] [numeric](18, 2) NULL,
	[IT_Earn7] [numeric](18, 2) NULL,
	[IT_Earn8] [numeric](18, 2) NULL,
	[IT_Earn9] [numeric](18, 2) NULL,
	[IT_Earn10] [numeric](18, 2) NULL,
	[IT_Earn11] [numeric](18, 2) NULL,
	[IT_Earn12] [numeric](18, 2) NULL,
	[IT_Earn13] [numeric](18, 2) NULL,
	[IT_Earn14] [numeric](18, 2) NULL,
	[IT_Earn15] [numeric](18, 2) NULL,
	[IT_Ded1] [numeric](18, 2) NULL,
	[IT_Ded2] [numeric](18, 2) NULL,
	[IT_Ded3] [numeric](18, 2) NULL,
	[IT_Ded4] [numeric](18, 2) NULL,
	[IT_Ded5] [numeric](18, 2) NULL,
	[IT_Ded6] [numeric](18, 2) NULL,
	[IT_Ded7] [numeric](18, 2) NULL,
	[IT_Ded8] [numeric](18, 2) NULL,
	[IT_Ded9] [numeric](18, 2) NULL,
	[IT_Ded10] [numeric](18, 2) NULL,
	[IT_Ded11] [numeric](18, 2) NULL,
	[IT_Ded12] [numeric](18, 2) NULL,
	[IT_Ded13] [numeric](18, 2) NULL,
	[IT_Ded14] [numeric](18, 2) NULL,
	[IT_Ded15] [numeric](18, 2) NULL,
	[IT_Flg] [tinyint] NULL,
	[IT_SDate] [datetime] NULL,
	[IT_EDate] [datetime] NULL,
	[IT_decExtra1] [numeric](18, 2) NULL,
	[IT_decExtra2] [numeric](18, 2) NULL,
	[IT_decExtra3] [numeric](18, 2) NULL,
	[IT_decExtra4] [numeric](18, 2) NULL,
	[IT_decExtra5] [numeric](18, 2) NULL,
	[IT_chvExtra6] [varchar](100) NULL,
	[IT_chvExtra7] [varchar](100) NULL,
	[IT_chvExtra8] [varchar](100) NULL,
	[IT_dtmExtra9] [datetime] NULL,
	[IT_inyExtra10] [tinyint] NULL,
 CONSTRAINT [PK_Incentive_Trn] PRIMARY KEY CLUSTERED 
(
	[IT_Datecode] ASC,
	[IT_MemID] ASC,
	[IT_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrowthQual_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrowthQual_Trn](
	[GQT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GQT_MemID] [numeric](18, 0) NOT NULL,
	[GQT_Cycle] [numeric](18, 0) NULL,
	[GQT_Points] [numeric](18, 0) NOT NULL,
	[GQT_Type] [numeric](18, 0) NOT NULL,
	[GQT_DtCode] [numeric](18, 0) NULL,
	[GQT_ConfFlg] [tinyint] NOT NULL,
	[GQT_RemainingPts] [numeric](18, 0) NOT NULL,
	[GQT_decExtra1] [numeric](18, 2) NULL,
	[GQT_decExtra2] [numeric](18, 2) NULL,
	[GQT_chvExtra3] [varchar](100) NULL,
	[GQT_dtmExtra4] [datetime] NULL,
 CONSTRAINT [PK_GrowthQual_Trn] PRIMARY KEY CLUSTERED 
(
	[GQT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GRInc_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GRInc_Trn](
	[GI_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GI_memid] [numeric](18, 0) NULL,
	[GI_sponid] [numeric](18, 0) NULL,
	[GI_Type] [numeric](18, 0) NULL,
	[GI_datecode] [numeric](18, 0) NULL,
	[GI_ActualAmt] [numeric](18, 2) NULL,
	[GI_DefineAmt] [numeric](18, 2) NULL,
	[GI_srno] [numeric](18, 0) NULL,
	[GI_Cycle] [numeric](18, 0) NULL,
	[GI_Per] [numeric](18, 2) NULL,
	[GI_level] [numeric](18, 0) NULL,
	[GI_decExtra1] [numeric](18, 2) NULL,
	[GI_decExtra2] [numeric](18, 2) NULL,
	[GI_chvExtra3] [varchar](100) NULL,
	[GI_chvExtra4] [varchar](100) NULL,
	[GI_chvExtra5] [varchar](100) NULL,
 CONSTRAINT [PK_GRInc_Trn] PRIMARY KEY CLUSTERED 
(
	[GI_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DGInc_trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DGInc_trn](
	[dt_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[dt_memid] [numeric](18, 0) NULL,
	[dt_Type] [numeric](18, 0) NULL,
	[dt_datecode] [numeric](18, 0) NULL,
	[dt_ActualAmt] [numeric](18, 2) NULL,
	[dt_DefinedAmt] [numeric](18, 2) NULL,
	[dt_MemCycle] [numeric](18, 0) NULL,
	[dt_Cycle] [numeric](18, 0) NULL,
	[dt_Per] [numeric](18, 2) NULL,
	[dt_decExtra1] [numeric](18, 2) NULL,
	[dt_decExtra2] [numeric](18, 2) NULL,
	[dt_chvExtra3] [varchar](100) NULL,
	[dt_chvExtra4] [varchar](100) NULL,
	[dt_inyExtra5] [tinyint] NULL,
 CONSTRAINT [PK_DGInc_trn] PRIMARY KEY CLUSTERED 
(
	[dt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BinInc_TrnDummy]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BinInc_TrnDummy](
	[BI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BI_DateCode] [numeric](18, 0) NOT NULL,
	[BI_MemID] [numeric](18, 0) NOT NULL,
	[BI_Type] [numeric](18, 0) NOT NULL,
	[BI_Earn1] [numeric](18, 2) NOT NULL,
	[BI_Earn2] [numeric](18, 2) NOT NULL,
	[BI_Earn3] [numeric](18, 2) NOT NULL,
	[BI_Earn4] [numeric](18, 2) NOT NULL,
	[BI_Earn5] [numeric](18, 2) NOT NULL,
	[BI_Earn6] [numeric](18, 2) NOT NULL,
	[BI_Earn7] [numeric](18, 2) NOT NULL,
	[BI_Earn8] [numeric](18, 2) NOT NULL,
	[BI_Earn9] [numeric](18, 2) NOT NULL,
	[BI_Earn10] [numeric](18, 2) NOT NULL,
	[BI_Earn11] [numeric](18, 2) NOT NULL,
	[BI_Earn12] [numeric](18, 2) NOT NULL,
	[BI_Earn13] [numeric](18, 2) NOT NULL,
	[BI_Earn14] [numeric](18, 2) NOT NULL,
	[BI_Earn15] [numeric](18, 2) NOT NULL,
	[BI_LeftChCnt] [numeric](18, 2) NOT NULL,
	[BI_RightChCnt] [numeric](18, 2) NOT NULL,
	[BI_LeftCF] [numeric](18, 2) NOT NULL,
	[BI_RightCF] [numeric](18, 2) NOT NULL,
	[BI_NewSetB10] [numeric](18, 0) NOT NULL,
	[BI_NewSetA10] [numeric](18, 0) NOT NULL,
	[BI_Sets1] [numeric](18, 0) NOT NULL,
	[BI_Sets2] [numeric](18, 0) NOT NULL,
	[BI_Sets3] [numeric](18, 0) NOT NULL,
	[BI_Flag] [tinyint] NOT NULL,
	[BI_LeftBF] [numeric](18, 2) NOT NULL,
	[BI_RightBF] [numeric](18, 2) NOT NULL,
	[BI_NewLeft] [numeric](18, 2) NOT NULL,
	[BI_NewRight] [numeric](18, 2) NOT NULL,
	[BI_silverBF] [numeric](18, 2) NOT NULL,
	[BI_SilverNew] [numeric](18, 2) NOT NULL,
	[BI_SilverCF] [numeric](18, 2) NOT NULL,
	[BI_SimBAmt] [numeric](18, 2) NULL,
	[BI_SimAAmt] [numeric](18, 2) NULL,
	[BI_decExtra1] [numeric](18, 2) NOT NULL,
	[BI_decExtra2] [numeric](18, 2) NOT NULL,
	[BI_decExtra3] [numeric](18, 2) NOT NULL,
	[BI_decExtra4] [numeric](18, 2) NOT NULL,
	[BI_chvExtra5] [varchar](100) NOT NULL,
	[BI_chvExtra6] [varchar](200) NOT NULL,
 CONSTRAINT [PK_BinInc_TrnDummy] PRIMARY KEY CLUSTERED 
(
	[BI_DateCode] ASC,
	[BI_MemID] ASC,
	[BI_Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Advance_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Advance_Trn](
	[ADT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ADT_MemID] [numeric](18, 0) NOT NULL,
	[ADT_Amount] [numeric](18, 2) NOT NULL,
	[ADT_Value] [numeric](18, 2) NOT NULL,
	[ADT_EMIFlg] [tinyint] NOT NULL,
	[ADT_LateFees] [numeric](18, 2) NOT NULL,
	[ADT_Date] [datetime] NOT NULL,
	[ADT_TrnStatus] [tinyint] NOT NULL,
	[ADT_RecoverFlg] [tinyint] NOT NULL,
	[ADT_Narration] [numeric](18, 0) NULL,
	[ADT_Type] [numeric](18, 0) NULL,
	[ADT_AdtID] [numeric](18, 0) NULL,
	[ADT_Datecode] [numeric](18, 0) NULL,
	[ADT_EditFlg] [tinyint] NULL,
	[ADT_Remarks] [varchar](200) NULL,
	[ADT_decExtra1] [numeric](18, 2) NULL,
	[ADT_decExtra2] [numeric](18, 2) NULL,
	[ADT_chvExtra3] [varchar](200) NULL,
	[ADT_chvExtra4] [varchar](200) NULL,
	[ADT_DeductionFlg] [numeric](18, 0) NULL,
	[ADT_DebitBalAmt] [numeric](18, 2) NULL,
	[ADT_Amuid] [numeric](18, 0) NULL,
	[ADT_IPAddress] [varchar](100) NULL,
	[ADT_Browser] [varchar](100) NULL,
	[ADT_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_Advance_Trn] PRIMARY KEY CLUSTERED 
(
	[ADT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountLedger_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountLedger_Trn](
	[ALT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ALT_MemID] [numeric](18, 0) NOT NULL,
	[ALT_Date] [datetime] NOT NULL,
	[ALT_Type] [numeric](18, 0) NULL,
	[ALT_DateCode] [numeric](18, 0) NULL,
	[ALT_InAmt] [numeric](18, 2) NOT NULL,
	[ALT_OutAmt] [numeric](18, 2) NOT NULL,
	[ALT_CrDrFlg] [tinyint] NOT NULL,
	[ALT_PayFlag] [tinyint] NULL,
	[ALT_IncFlag] [tinyint] NULL,
	[ALT_IncDate] [datetime] NULL,
	[ALT_ProcessFlg] [tinyint] NOT NULL,
	[ALT_ProcessDate] [datetime] NULL,
	[ALT_Paymode] [numeric](18, 0) NULL,
	[ALT_Remark] [varchar](500) NULL,
	[ALT_ShowFlag] [tinyint] NOT NULL,
	[ALT_chvExtra1] [varchar](100) NULL,
	[ALT_chvExtra2] [varchar](100) NULL,
	[ALT_chvExtra3] [varchar](100) NULL,
	[ALT_chvExtra4] [varchar](100) NULL,
	[ALT_chvExtra5] [varchar](100) NULL,
	[ALT_EndDateCode] [numeric](18, 0) NULL,
	[ALT_TrnCode] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChangeDispatchBy_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChangeDispatchBy_Trn](
	[CDT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CDT_OHCode] [numeric](18, 0) NOT NULL,
	[CDT_FromFrID] [numeric](18, 0) NULL,
	[CDT_ToFrID] [numeric](18, 0) NULL,
	[CDT_Date] [datetime] NOT NULL,
	[CDT_DateTime] [datetime] NOT NULL,
	[CDT_TrnFlg] [tinyint] NOT NULL,
	[CDT_FrID] [numeric](20, 0) NULL,
	[CDT_AdminID] [numeric](20, 0) NULL,
	[CDT_IPAddress] [varchar](50) NULL,
	[CDT_Browser] [varchar](50) NULL,
	[CDT_BrowserVersion] [varchar](50) NULL,
	[CDT_decExtra1] [numeric](20, 2) NULL,
	[CDT_chvExtra1] [varchar](200) NULL,
 CONSTRAINT [PK_ChangeDispatchBy_Trn] PRIMARY KEY CLUSTERED 
(
	[CDT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AutomatrixTree_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutomatrixTree_Mst](
	[AM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AM_TreeID] [numeric](18, 0) NOT NULL,
	[AM_Level] [numeric](18, 0) NULL,
	[AM_Joinings] [numeric](18, 0) NULL,
	[AM_AmtperID] [numeric](18, 2) NULL,
	[AM_Amt] [numeric](18, 2) NULL,
	[AM_DedAmt] [numeric](18, 2) NULL,
	[AM_Reentry] [numeric](18, 0) NULL,
	[AM_Direct] [numeric](18, 0) NULL,
	[AM_decExtra1] [numeric](18, 2) NULL,
	[AM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_AutomatrixTree_Mst_1] PRIMARY KEY CLUSTERED 
(
	[AM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailyCycle_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyCycle_Mst](
	[DCM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DCM_DOMID] [numeric](18, 0) NULL,
	[DCM_Cycle] [numeric](18, 0) NULL,
	[DCM_Name] [varchar](100) NULL,
	[DCM_Stime] [datetime] NULL,
	[DCM_Etime] [datetime] NULL,
	[DCM_Flag] [tinyint] NULL,
	[DCM_decExtra1] [numeric](18, 2) NULL,
	[DCM_chvExtra2] [varchar](100) NULL,
 CONSTRAINT [PK_DailyCycle_Mst] PRIMARY KEY CLUSTERED 
(
	[DCM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Courier_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Courier_Mst_Log](
	[CML_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CML_CMID] [numeric](18, 0) NOT NULL,
	[CML_Code] [varchar](15) NOT NULL,
	[CML_Date] [datetime] NOT NULL,
	[CML_DateTime] [datetime] NOT NULL,
	[CML_Name] [varchar](100) NOT NULL,
	[CML_Address] [varchar](500) NULL,
	[CML_CountryID] [numeric](18, 0) NULL,
	[CML_Country] [varchar](50) NULL,
	[CML_StateID] [numeric](18, 0) NULL,
	[CML_State] [varchar](50) NULL,
	[CML_City] [varchar](100) NULL,
	[CML_ContactPerson] [varchar](100) NULL,
	[CML_Mobile] [numeric](18, 0) NULL,
	[CML_ContactNo] [varchar](20) NULL,
	[CML_Username] [varchar](15) NOT NULL,
	[CML_Password] [varchar](15) NOT NULL,
	[CML_Flag] [tinyint] NOT NULL,
	[CML_chvExtra1] [varchar](100) NULL,
	[CML_chvExtra2] [varchar](100) NULL,
	[CML_chvExtra3] [varchar](100) NULL,
	[CML_chvExtra4] [varchar](100) NULL,
	[CML_decExtra1] [numeric](18, 2) NULL,
	[CML_decExtra2] [numeric](18, 2) NULL,
	[CML_decExtra3] [numeric](18, 2) NULL,
	[CML_decExtra4] [numeric](18, 2) NULL,
	[CML_LogFlg] [tinyint] NOT NULL,
	[CML_AdminID] [numeric](18, 0) NOT NULL,
	[CML_IPAddress] [varchar](50) NOT NULL,
	[CML_Browser] [varchar](50) NOT NULL,
	[CML_BrowserVersion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Courier_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[CML_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deduction_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deduction_Hdr](
	[DH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DH_DHMID] [numeric](18, 0) NULL,
	[DH_Name] [varchar](400) NULL,
	[DH_Desc] [varchar](400) NULL,
	[DH_UpdateFlag] [numeric](18, 0) NULL,
	[DH_Flag] [numeric](18, 0) NULL,
	[DH_decExtra1] [numeric](18, 0) NULL,
	[DH_decExtra2] [numeric](18, 0) NULL,
	[DH_chvExtra3] [varchar](50) NULL,
	[DH_chvExtra4] [varchar](50) NULL,
 CONSTRAINT [PK_Deduction_Hdr] PRIMARY KEY CLUSTERED 
(
	[DH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deduction_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deduction_Dtls](
	[DD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DD_DHMID] [numeric](18, 0) NOT NULL,
	[DD_PercFlag] [tinyint] NULL,
	[DD_Value] [numeric](18, 2) NULL,
	[DD_AdvanceFlag] [numeric](18, 0) NULL,
	[DD_MaxAmtPerPayout] [numeric](18, 2) NULL,
	[DD_CummulativeMaxAmt] [numeric](18, 2) NULL,
	[DD_GrossOrNetFlag] [numeric](18, 0) NULL,
	[DD_DedAfterCummulativeAmt] [numeric](18, 2) NULL,
	[DD_AdvanceOnDedAfterCummulativeAmt] [numeric](18, 2) NULL,
	[DD_Dtcode] [numeric](18, 0) NULL,
	[DD_PlanType] [numeric](18, 0) NOT NULL,
	[DD_DDID] [numeric](18, 0) NULL,
	[DD_Flag] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Deduction_Dtls] PRIMARY KEY CLUSTERED 
(
	[DD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChequePrint_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChequePrint_Hdr](
	[CPH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CPH_Code] [varchar](50) NOT NULL,
	[CPH_Date] [datetime] NOT NULL,
	[CPH_CPBMID] [numeric](18, 0)  NULL,
	[CPH_Flag] [tinyint] NOT NULL,
	[CPH_ChequeDate] [datetime] NULL,
	[CPH_DateTime] [datetime] NULL,
	[CPH_Remark] [varchar](500) NULL,
	[CPH_UHID] [numeric](18, 0) NULL,
	[CPH_IPAddress] [varchar](50) NULL,
	[CPH_Browser] [varchar](50) NULL,
	[CPH_BrowserVersion] [varchar](50) NULL,
	[CPH_PPHCode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ChequePrint_Hdr] PRIMARY KEY CLUSTERED 
(
	[CPH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_ChequePrint_Hdr] UNIQUE NONCLUSTERED 
(
	[CPH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChequePrint_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChequePrint_Dtls](
	[CPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CPD_CPHID] [numeric](18, 0) NOT NULL,
	[CPD_ChqPayTo] [varchar](200) NOT NULL,
	[CPD_MemID] [numeric](18, 0) NOT NULL,
	[CPD_AmtinFig] [numeric](18, 2) NOT NULL,
	[CPD_AmtinWords] [varchar](500) NOT NULL,
	[CPD_ChqNo] [numeric](18, 0) NOT NULL,
	[CPD_ChqDate] [datetime] NOT NULL,
	[CPD_Datecode] [numeric](18, 0) NULL,
	[CPD_Type] [numeric](18, 0) NULL,
	[CPD_ITID] [numeric](18, 0) NULL,
	[CPD_EndDateCode] [numeric](18, 0) NULL,
	[CPD_Flag] [tinyint] NOT NULL,
	[CPD_decExtra1] [numeric](18, 2) NULL,
	[CPD_decExtra2] [numeric](18, 2) NULL,
	[CPD_chvExtra3] [varchar](50) NULL,
	[CPD_chvExtra4] [varchar](50) NULL,
	[CPD_PrintCount] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ChequePrint_Dtls] PRIMARY KEY CLUSTERED 
(
	[CPD_CPHID] ASC,
	[CPD_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Field_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Field_Dtls](
	[FM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FM_FHID] [numeric](18, 0) NULL,
	[FM_TableName] [varchar](50) NOT NULL,
	[FM_FieldName] [varchar](500) NOT NULL,
	[FM_Alias] [varchar](500) NULL,
	[FM_FieldFlag] [tinyint] NULL,
	[FM_FilterFlag] [tinyint] NULL,
	[FM_GroupByFlag] [tinyint] NULL,
	[FM_chvExtra1] [varchar](500) NULL,
	[FM_decExtra2] [numeric](18, 2) NULL,
	[Default_FM_FieldFlag] [tinyint] NULL,
 CONSTRAINT [PK_Field_Mst] PRIMARY KEY CLUSTERED 
(
	[FM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group_Dtls](
	[GD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GD_GroupID] [numeric](18, 0) NOT NULL,
	[GD_Memid] [numeric](18, 0) NOT NULL,
	[GD_Owner] [tinyint] NOT NULL,
	[GD_Flag] [tinyint] NOT NULL,
	[GD_chvExtra1] [varchar](200) NULL,
	[GD_chvExtra2] [varchar](200) NULL,
	[GD_decExtra3] [numeric](18, 2) NULL,
	[GD_decExtra4] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Group_Dtls] PRIMARY KEY CLUSTERED 
(
	[GD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JoiningPlan_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JoiningPlan_Mst](
	[JM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[JM_Name] [varchar](100) NULL,
	[JM_Type] [numeric](18, 0) NULL,
	[JM_Weaker] [tinyint] NULL,
	[JM_Flag] [tinyint] NULL,
	[JM_decExtra1] [numeric](18, 0) NULL,
	[JM_chvExtra2] [varchar](100) NULL,
	[JM_chvExtra3] [varchar](100) NULL,
	[JM_MultiJoinLogic] [tinyint] NOT NULL,
 CONSTRAINT [PK_JoiningPlan_Mst] PRIMARY KEY CLUSTERED 
(
	[JM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IncomeType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IncomeType_Mst](
	[PIM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PIM_IncomeName] [varchar](50) NOT NULL,
	[PIM_Desc] [varchar](100) NULL,
	[PIM_PMID] [numeric](18, 0) NOT NULL,
	[PIM_BonusFlg] [tinyint] NOT NULL,
	[PIM_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_IncomeType_Mst] PRIMARY KEY CLUSTERED 
(
	[PIM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MakePayment_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MakePayment_Hdr](
	[MPH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPH_PPHCode] [numeric](18, 0) NOT NULL,
	[MPH_Date] [datetime] NOT NULL,
	[MPH_Datetime] [datetime] NOT NULL,
	[MPH_Amuid] [numeric](18, 0) NULL,
	[MPH_IPAddress] [varchar](100) NULL,
	[MPH_Browser] [varchar](100) NULL,
	[MPH_Browserversion] [varchar](100) NULL,
 CONSTRAINT [PK_MakePayment_Hdr] PRIMARY KEY CLUSTERED 
(
	[MPH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MakePayment_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MakePayment_Dtls](
	[MPD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MPD_MPHID] [numeric](18, 0) NOT NULL,
	[MPD_PPDID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MakePayment_Dtls] PRIMARY KEY CLUSTERED 
(
	[MPD_MPHID] ASC,
	[MPD_PPDID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deduction_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deduction_Log](
	[DL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DL_Datecode] [numeric](18, 0) NULL,
	[DL_Type] [numeric](18, 0) NULL,
	[DL_DMID] [numeric](18, 0) NULL,
	[DL_Desc] [varchar](100) NULL,
	[DL_Perflg] [tinyint] NULL,
	[DL_Amt] [numeric](18, 0) NULL,
	[DL_CummFlg] [tinyint] NULL,
	[DL_CummEarn] [numeric](18, 2) NULL,
	[DL_Maxflg] [tinyint] NULL,
	[DL_MaxDed] [numeric](18, 2) NULL,
	[DL_ApplyPayout] [numeric](18, 0) NULL,
	[DL_SlabFlg] [tinyint] NULL,
	[DL_RenewalFlg] [tinyint] NULL,
	[DL_UndoFlg] [tinyint] NULL,
	[DL_decExtra1] [numeric](18, 2) NULL,
	[DL_decExtra2] [numeric](18, 2) NULL,
	[DL_decExtra3] [numeric](18, 2) NULL,
	[DL_decExtra4] [numeric](18, 2) NULL,
	[DL_decExtra5] [numeric](18, 2) NULL,
	[DL_chvExtra6] [varchar](100) NULL,
	[DL_chvExtra7] [varchar](100) NULL,
	[DL_chvExtra8] [varchar](100) NULL,
	[DL_dtmExtra9] [datetime] NULL,
	[DL_inyExtra10] [tinyint] NULL,
 CONSTRAINT [PK_Deduction_Log] PRIMARY KEY CLUSTERED 
(
	[DL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdditionalInc_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdditionalInc_Trn](
	[AIT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AIT_PIMID] [numeric](18, 0) NOT NULL,
	[AIT_MemID] [numeric](18, 0) NOT NULL,
	[AIT_amount] [numeric](18, 2) NOT NULL,
	[AIT_Date] [datetime] NULL,
	[AIT_Narration] [numeric](18, 0) NULL,
	[AIT_Type] [numeric](18, 0) NULL,
	[AIT_Datecode] [numeric](18, 0) NULL,
	[AIT_PayFlg] [tinyint] NULL,
	[AIT_Remarks] [varchar](200) NULL,
	[AIT_decExtra1] [numeric](18, 2) NULL,
	[AIT_decExtra2] [numeric](18, 2) NULL,
	[AIT_chvExtra3] [varchar](100) NULL,
	[AIT_chvExtra4] [varchar](100) NULL,
 CONSTRAINT [PK_AdditionalInc_Trn] PRIMARY KEY CLUSTERED 
(
	[AIT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PurchaseOrder_Hdr_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Hdr_Log](
	[PHL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PHL_POCode] [numeric](18, 0) NOT NULL,
	[PHL_SupplierID] [numeric](20, 0) NOT NULL,
	[PHL_Date] [datetime] NOT NULL,
	[PHL_DateTime] [datetime] NOT NULL,
	[PHL_TotQty] [numeric](20, 0) NOT NULL,
	[PHL_TotAmt] [numeric](20, 2) NOT NULL,
	[PHL_Octroi] [numeric](20, 2) NOT NULL,
	[PHL_Freight] [numeric](20, 2) NOT NULL,
	[PHL_Other] [numeric](20, 2) NOT NULL,
	[PHL_DiscAmt] [numeric](20, 2) NOT NULL,
	[PHL_PercentFlg] [tinyint] NULL,
	[PHL_Percent] [numeric](20, 2) NULL,
	[PHL_NetAmt] [numeric](20, 2) NOT NULL,
	[PHL_ShipName] [varchar](100) NULL,
	[PHL_ShipAddress] [varchar](500) NULL,
	[PHL_ShipCountryID] [numeric](20, 0) NULL,
	[PHL_ShipCountry] [varchar](50) NULL,
	[PHL_ShipStateID] [numeric](20, 0) NULL,
	[PHL_ShipState] [varchar](50) NULL,
	[PHL_ShipCity] [varchar](50) NULL,
	[PHL_ShipPincode] [numeric](20, 0) NULL,
	[PHL_ShipMobile] [numeric](20, 0) NULL,
	[PHL_ShipContactNo] [varchar](20) NULL,
	[PHL_ShipEmail] [varchar](100) NULL,
	[PHL_Paymode] [numeric](20, 0) NOT NULL,
	[PHL_ChqDDNo] [numeric](20, 0) NULL,
	[PHL_ChqDDDate] [datetime] NULL,
	[PHL_ChqDDBank] [varchar](100) NULL,
	[PHL_ChqDDBranch] [varchar](50) NULL,
	[PHL_PayslipImg] [varchar](100) NULL,
	[PHL_PinSrNo] [numeric](20, 0) NULL,
	[PHL_StatusFlag] [numeric](20, 0) NOT NULL,
	[PHL_StatusDate] [datetime] NULL,
	[PHL_StatusDtm] [datetime] NULL,
	[PHL_DeliveryDate] [datetime] NULL,
	[PHL_decExtra1] [numeric](20, 2) NULL,
	[PHL_decExtra2] [numeric](20, 2) NULL,
	[PHL_decExtra3] [numeric](20, 2) NULL,
	[PHL_decExtra4] [numeric](20, 2) NULL,
	[PHL_chvExtra1] [varchar](200) NULL,
	[PHL_chvExtra2] [varchar](200) NULL,
	[PHL_chvExtra3] [varchar](200) NULL,
	[PHL_chvExtra4] [varchar](200) NULL,
	[PHL_dtmExtra1] [datetime] NULL,
	[PHL_dtmExtra2] [datetime] NULL,
	[PHL_LogFlg] [tinyint] NOT NULL,
	[PHL_AdminID] [numeric](18, 0) NOT NULL,
	[PHL_IPAddress] [varchar](50) NOT NULL,
	[PHL_Browser] [varchar](50) NOT NULL,
	[PHL_BrowserVersion] [varchar](50) NOT NULL,
	[PHL_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PurchaseOrder_Hdr_Log] PRIMARY KEY CLUSTERED 
(
	[PHL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Dtls](
	[UD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UD_UHID] [numeric](18, 0) NULL,
	[UD_MMID] [numeric](18, 0) NULL,
	[UD_Flag] [tinyint] NULL,
 CONSTRAINT [PK_User_Dtls] PRIMARY KEY CLUSTERED 
(
	[UD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tax_Mst]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tax_Mst](
	[TM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TM_Date] [datetime] NOT NULL,
	[TM_DateTime] [datetime] NOT NULL,
	[TM_PvCode] [numeric](18, 0) NOT NULL,
	[TM_StateID] [numeric](18, 0) NOT NULL,
	[TM_VAT] [numeric](20, 2) NOT NULL,
	[TM_CST] [numeric](20, 2) NOT NULL,
	[TM_FormC] [numeric](20, 2) NOT NULL,
	[TM_Flag] [tinyint] NOT NULL,
	[TM_decExtra1] [numeric](20, 2) NULL,
	[TM_decExtra2] [numeric](20, 2) NULL,
	[TM_chvExtra3] [varchar](50) NULL,
	[TM_chvExtra4] [varchar](50) NULL,
 CONSTRAINT [PK_Tax_Mst] PRIMARY KEY CLUSTERED 
(
	[TM_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockOut_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockOut_Hdr](
	[SOH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SOH_Code] [numeric](18, 0) NOT NULL,
	[SOH_PCRCode] [numeric](18, 0) NOT NULL,
	[SOH_Date] [datetime] NOT NULL,
	[SOH_DateTime] [datetime] NOT NULL,
	[SOH_ByFrID] [numeric](18, 0) NULL,
	[SOH_ToFrID] [numeric](18, 0) NOT NULL,
	[SOH_TrnFlag] [tinyint] NOT NULL,
	[SOH_TotQty] [numeric](20, 0) NOT NULL,
	[SOH_Remarks] [varchar](500) NULL,
	[SOH_DelMode] [tinyint] NOT NULL,
	[SOH_DelBy] [varchar](15) NULL,
	[SOH_DelRefNo] [varchar](50) NULL,
	[SOH_DelRefInfo] [varchar](50) NULL,
	[SOH_DelRemarks] [varchar](500) NULL,
	[SOH_DelDate] [datetime] NULL,
	[SOH_AdminID] [numeric](20, 0) NULL,
	[SOH_IPAddress] [varchar](50) NOT NULL,
	[SOH_Browser] [varchar](50) NOT NULL,
	[SOH_BrowserVersion] [varchar](50) NOT NULL,
	[SOH_decExtra1] [numeric](20, 2) NULL,
	[SOH_decExtra2] [numeric](20, 2) NULL,
	[SOH_chvExtra1] [varchar](500) NULL,
	[SOH_chvExtra2] [varchar](500) NULL,
	[SOH_dtmExtra1] [datetime] NULL,
	[SOH_dtmExtra2] [datetime] NULL,
	[SOH_TotTransitQty] [numeric](18, 0) NOT NULL,
	[SOH_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockOut_Hdr] PRIMARY KEY CLUSTERED 
(
	[SOH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StockInventory_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StockInventory_Dtls](
	[SID_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SID_Date] [datetime] NULL,
	[SID_DateTime] [datetime] NULL,
	[SID_TransitDate] [datetime] NULL,
	[SID_TransitDateTime] [datetime] NULL,
	[SID_PVCode] [numeric](18, 0) NOT NULL,
	[SID_FrID] [numeric](18, 0) NULL,
	[SID_SupplierID] [numeric](18, 0) NULL,
	[SID_MemID] [numeric](18, 0) NULL,
	[SID_InQty] [numeric](18, 0) NOT NULL,
	[SID_OutQty] [numeric](18, 0) NOT NULL,
	[SID_TransitQty] [numeric](18, 0) NOT NULL,
	[SID_StatusFlg] [tinyint] NOT NULL,
	[SID_TrnID] [numeric](18, 0) NOT NULL,
	[SID_TrnRefCode] [numeric](18, 0) NOT NULL,
	[SID_Remark] [varchar](200) NULL,
	[SID_decExtra1] [numeric](18, 0) NULL,
	[SID_decExtra2] [numeric](18, 0) NULL,
	[SID_decExtra3] [numeric](18, 0) NULL,
	[SID_chvExtra1] [varchar](100) NULL,
	[SID_chvExtra2] [varchar](100) NULL,
	[SID_chvExtra3] [varchar](100) NULL,
	[SID_dtmExtra1] [datetime] NULL,
	[SID_dtmExtra2] [datetime] NULL,
 CONSTRAINT [PK_StockInventory_Dtls] PRIMARY KEY CLUSTERED 
(
	[SID_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offer_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offer_Dtls](
	[OD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OD_OHID] [numeric](18, 0) NOT NULL,
	[OD_ACMID] [numeric](18, 0) NOT NULL,
	[OD_Value] [numeric](18, 0) NOT NULL,
	[OD_Days] [numeric](18, 0) NOT NULL,
	[OD_AndOrFlg] [tinyint] NOT NULL,
	[OD_Flag] [tinyint] NOT NULL,
 CONSTRAINT [PK_Offer_Dtls] PRIMARY KEY CLUSTERED 
(
	[OD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Narration_Mst_Log]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Narration_Mst_Log](
	[NL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NL_NMID] [numeric](18, 0) NOT NULL,
	[NL_Date] [datetime] NOT NULL,
	[NL_Datetime] [datetime] NOT NULL,
	[NL_Title] [varchar](50) NOT NULL,
	[NL_Desc] [varchar](300) NULL,
	[NL_TrnFlag] [tinyint] NOT NULL,
	[NL_Flag] [tinyint] NOT NULL,
	[NL_LogFlg] [tinyint] NOT NULL,
	[NL_UMID] [numeric](18, 0) NOT NULL,
	[NL_IPAddress] [varchar](50) NOT NULL,
	[NL_Browser] [varchar](50) NOT NULL,
	[NL_BrowserVersion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Narration_Mst_Log] PRIMARY KEY CLUSTERED 
(
	[NL_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MaxPV_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaxPV_Trn](
	[MT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MT_Datecode] [numeric](18, 0) NOT NULL,
	[MT_Type] [numeric](18, 0) NOT NULL,
	[MT_PvCode] [numeric](18, 0) NOT NULL,
	[MT_PvType] [numeric](18, 0) NOT NULL,
	[MT_PvCnt] [numeric](18, 6) NOT NULL,
	[MT_Newcnt] [numeric](18, 6) NOT NULL,
	[MT_TOamt] [numeric](18, 2) NOT NULL,
	[MT_ResAmt] [numeric](18, 2) NOT NULL,
	[MT_decExtra1] [numeric](18, 2) NULL,
	[MT_decExtra2] [numeric](18, 2) NULL,
	[MT_decExtra3] [numeric](18, 2) NULL,
	[MT_decExtra4] [numeric](18, 2) NULL,
 CONSTRAINT [PK_MaxPV_Trn] PRIMARY KEY CLUSTERED 
(
	[MT_Datecode] ASC,
	[MT_Type] ASC,
	[MT_PvCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaxID_Trn]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaxID_Trn](
	[MIDT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MIDT_DateCode] [numeric](18, 0) NOT NULL,
	[MIDT_Type] [numeric](18, 0) NOT NULL,
	[MIDT_MaxID] [numeric](18, 0) NOT NULL,
	[MIDT_MaxPvs] [numeric](18, 2) NOT NULL,
	[MIDT_TOAmt] [numeric](18, 2) NOT NULL,
	[MIDT_FreshCnt] [numeric](18, 2) NOT NULL,
	[MIDT_CF] [numeric](18, 2) NOT NULL,
	[MIDT_SimB] [numeric](18, 2) NOT NULL,
	[MIDT_SimA] [numeric](18, 2) NOT NULL,
	[MIDT_SimPer] [numeric](18, 2) NOT NULL,
	[MIDT_VarRate] [numeric](18, 2) NOT NULL,
	[MIDT_DRate] [numeric](18, 2) NOT NULL,
	[MIDT_Cycle] [numeric](18, 0) NOT NULL,
	[MIDT_GrPer] [numeric](18, 2) NOT NULL,
	[MIDT_Target] [numeric](18, 2) NOT NULL,
	[MIDT_Actual] [numeric](18, 2) NOT NULL,
	[MIDT_TOBV] [numeric](18, 2) NOT NULL,
	[MIDT_CFunit] [numeric](18, 2) NOT NULL,
	[MIDT_BFUnit] [numeric](18, 2) NOT NULL,
	[MIDT_Flag] [tinyint] NOT NULL,
	[MIDT_Root] [numeric](18, 6) NOT NULL,
 CONSTRAINT [PK_MaxID_Trn] PRIMARY KEY CLUSTERED 
(
	[MIDT_DateCode] ASC,
	[MIDT_Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseOrder_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Dtls](
	[PD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PD_POCode] [numeric](18, 0) NOT NULL,
	[PD_PVCode] [numeric](18, 0) NOT NULL,
	[PD_PurRate] [numeric](20, 2) NOT NULL,
	[PD_PurVAT] [numeric](20, 2) NOT NULL,
	[PD_PurPrice] [numeric](20, 2) NOT NULL,
	[PD_Qty] [numeric](20, 0) NOT NULL,
	[PD_GrossAmt] [numeric](20, 2) NOT NULL,
	[PD_DiscAmt] [numeric](20, 2) NOT NULL,
	[PD_NetAmt] [numeric](20, 2) NOT NULL,
	[PD_decExtra1] [numeric](20, 2) NULL,
	[PD_decExtra2] [numeric](20, 2) NULL,
	[PD_chvExtra1] [varchar](500) NULL,
	[PD_chvExtra2] [varchar](500) NULL,
	[PD_dtmExtra1] [datetime] NULL,
	[PD_dtmExtra2] [datetime] NULL,
	[PD_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PurchaseOrder_Dtls] PRIMARY KEY CLUSTERED 
(
	[PD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Process_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
CREATE TABLE [dbo].[Process_Dtls](
	[PD_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PD_UHID] [numeric](18, 0) NOT NULL,
	[PD_PHID] [numeric](18, 0) NOT NULL,
	[PD_ViewFlg] [tinyint] NOT NULL,
	[PD_SDate] [datetime] NULL,
	[PD_EDate] [datetime] NULL,
	[PD_ProcessFlg] [tinyint] NOT NULL,
	[PD_SimFlg] [tinyint] NOT NULL,
	[PD_decExtra1] [numeric](18, 2) NULL,
	[PD_decExtra2] [numeric](18, 2) NULL,
	[PD_chvExtra3] [varchar](200) NULL,
	[PD_chvExtra4] [varchar](200) NULL,
 CONSTRAINT [PK_Process_Dtls] PRIMARY KEY CLUSTERED 
(
	[PD_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Upgrade_Trn]    Script Date: 09/16/2016 10:42:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Upgrade_Trn](
	[UT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UT_MemID] [numeric](18, 0) NOT NULL,
	[UT_OHCode] [varchar](15) NULL,
	[UT_NewPvCode] [numeric](18, 0) NOT NULL,
	[UT_PrevPvCode] [numeric](18, 0) NOT NULL,
	[UT_NewType] [numeric](18, 0) NOT NULL,
	[UT_PrevType] [numeric](18, 0) NOT NULL,
	[UT_Date] [datetime] NOT NULL,
	[UT_Amount] [numeric](18, 2) NOT NULL,
	[UT_SponsorID] [numeric](18, 0) NOT NULL,
	[UT_Datecode] [numeric](18, 0) NOT NULL,
	[UT_IncAmt] [numeric](18, 2) NOT NULL,
	[UT_Flag] [tinyint] NOT NULL,
	[UT_Remark] [varchar](300) NULL,
	[UT_AdminID] [numeric](18, 2) NOT NULL,
	[UT_IPAddress] [varchar](50) NULL,
	[UT_Browser] [varchar](50) NULL,
	[UT_BrowserVersion] [varchar](50) NULL,
	[UT_decEXTRA1] [numeric](18, 2) NULL,
	[UT_decEXTRA2] [numeric](18, 2) NULL,
	[UT_chvEXTRA3] [varchar](200) NULL,
	[UT_chvEXTRA4] [varchar](200) NULL,
	[UT_ConfirmDate] [datetime] NULL,
	[UT_ReciptImg] [VARBINARY](max) NUll,
	[UT_UpgradeBy] [nvarchar] (max) NUll,
 CONSTRAINT [PK_Upgrade_Trn] PRIMARY KEY CLUSTERED 
(
	[UT_MemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockInByFR_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
CREATE TABLE [dbo].[StockInByFR_Hdr](
	[SIFH_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SIFH_Code] [numeric](18, 0) NOT NULL,
	[SIFH_SOHCode] [numeric](18, 0) NOT NULL,
	[SIFH_Date] [datetime] NOT NULL,
	[SIFH_DateTime] [datetime] NOT NULL,
	[SIFH_ByFrID] [numeric](18, 0) NOT NULL,
	[SIFH_FromFrID] [numeric](18, 0) NULL,
	[SIFH_TrnFlag] [tinyint] NOT NULL,
	[SIFH_TotQty] [numeric](20, 0) NOT NULL,
	[SIFH_TotAmt] [numeric](20, 2) NULL,
	[SIFH_Remarks] [varchar](500) NULL,
	[SIFH_DelMode] [tinyint] NULL,
	[SIFH_DelBy] [varchar](15) NULL,
	[SIFH_DelRefNo] [varchar](50) NULL,
	[SIFH_DelRefInfo] [varchar](50) NULL,
	[SIFH_DelRemarks] [varchar](500) NULL,
	[SIFH_AdminID] [numeric](18, 0) NULL,
	[SIFH_IPAddress] [varchar](50) NULL,
	[SIFH_Browser] [varchar](50) NULL,
	[SIFH_BrowserVersion] [varchar](50) NULL,
	[SIFH_decExtra1] [numeric](20, 2) NULL,
	[SIFH_decExtra2] [numeric](20, 2) NULL,
	[SIFH_chvExtra1] [varchar](500) NULL,
	[SIFH_chvExtra2] [varchar](500) NULL,
	[SIFH_dtmExtra1] [datetime] NULL,
	[SIFH_dtmExtra2] [datetime] NULL,
	[SIFH_TotTransitQty] [numeric](18, 0) NOT NULL,
	[SIFH_TotInQty] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockInByFR_Hdr] PRIMARY KEY CLUSTERED 
(
	[SIFH_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_AchieversList_Hdr_ALH_AwardDelFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversList_Hdr] ADD  CONSTRAINT [DF_AchieversList_Hdr_ALH_AwardDelFlg]  DEFAULT ((0)) FOR [ALH_AwardDelFlg]
GO
/****** Object:  Default [DF_AchieversList_Hdr_ALH_ALH_EarningDelFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversList_Hdr] ADD  CONSTRAINT [DF_AchieversList_Hdr_ALH_ALH_EarningDelFlg]  DEFAULT ((0)) FOR [ALH_EarningDelFlg]
GO
/****** Object:  Default [DF_AchieversList_Mst_ALM_InnerFlag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversList_Mst] ADD  CONSTRAINT [DF_AchieversList_Mst_ALM_InnerFlag]  DEFAULT ((0)) FOR [ALM_InnerFlag]
GO
/****** Object:  Default [DF_AchieversListAward_Dtls_AAD_ShowFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListAward_Dtls] ADD  CONSTRAINT [DF_AchieversListAward_Dtls_AAD_ShowFlg]  DEFAULT ((0)) FOR [AAD_ShowFlg]
GO
/****** Object:  Default [DF_AchieversListEarning_Dtls_AED_ShowFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListEarning_Dtls] ADD  CONSTRAINT [DF_AchieversListEarning_Dtls_AED_ShowFlg]  DEFAULT ((0)) FOR [AED_ShowFlg]
GO
/****** Object:  Default [DF_AchieversListField_Mst_AFM_InnerFlag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListField_Mst] ADD  CONSTRAINT [DF_AchieversListField_Mst_AFM_InnerFlag]  DEFAULT ((0)) FOR [AFM_InnerFlag]
GO
/****** Object:  Default [DF_AchieversListField_Mst_AFM_AwardFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListField_Mst] ADD  CONSTRAINT [DF_AchieversListField_Mst_AFM_AwardFlg]  DEFAULT ((0)) FOR [AFM_AwardFlg]
GO
/****** Object:  Default [DF_AchieversListField_Mst_AFM_EarningFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListField_Mst] ADD  CONSTRAINT [DF_AchieversListField_Mst_AFM_EarningFlg]  DEFAULT ((0)) FOR [AFM_EarningFlg]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_amount]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_amount]  DEFAULT ((0)) FOR [ADT_Amount]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_Value]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_Value]  DEFAULT ((0)) FOR [ADT_Value]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_EMIFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_EMIFlg]  DEFAULT ((0)) FOR [ADT_EMIFlg]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_LateFees]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_LateFees]  DEFAULT ((0)) FOR [ADT_LateFees]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_TrnStatus]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_TrnStatus]  DEFAULT ((0)) FOR [ADT_TrnStatus]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_RecoverFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_RecoverFlg]  DEFAULT ((0)) FOR [ADT_RecoverFlg]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_EditFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_EditFlg]  DEFAULT ((0)) FOR [ADT_EditFlg]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_DeductionFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_DeductionFlg]  DEFAULT ((0)) FOR [ADT_DeductionFlg]
GO
/****** Object:  Default [DF_Advance_Trn_ADT_DebitBalAmt]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn] ADD  CONSTRAINT [DF_Advance_Trn_ADT_DebitBalAmt]  DEFAULT ((0)) FOR [ADT_DebitBalAmt]
GO
/****** Object:  Default [DF_AwardCriteria_Mst_ACM_CondFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AwardCriteria_Mst] ADD  CONSTRAINT [DF_AwardCriteria_Mst_ACM_CondFlg]  DEFAULT ((0)) FOR [ACM_CondFlg]
GO
/****** Object:  Default [DF_Bank_Mst_BM_WebFlag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_WebFlag]  DEFAULT ((0)) FOR [BM_WebFlag]
GO
/****** Object:  Default [DF_Bank_Mst_BM_WebFlag1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_WebFlag1]  DEFAULT ((0)) FOR [BM_Status]
GO
/****** Object:  Default [DF_Bank_Mst_BM_Status1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_Status1]  DEFAULT ((0)) FOR [BM_Extra1]
GO
/****** Object:  Default [DF_Bank_Mst_BM_Extra11]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_Extra11]  DEFAULT ((0)) FOR [BM_Extra2]
GO
/****** Object:  Default [DF_Bank_Mst_BM_Extra12]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_Extra12]  DEFAULT ((0)) FOR [BM_Extra3]
GO
/****** Object:  Default [DF_Bank_Mst_BM_Extra13]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Bank_Mst] ADD  CONSTRAINT [DF_Bank_Mst_BM_Extra13]  DEFAULT ((0)) FOR [BM_Extra4]
GO
/****** Object:  Default [DF_Convention_Mst_CM_CustomFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Convention_Mst] ADD  CONSTRAINT [DF_Convention_Mst_CM_CustomFlg]  DEFAULT ((1)) FOR [CM_CustomFlg]
GO
/****** Object:  Default [DF_DispatchMode_mst_DM_Flag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DispatchMode_mst] ADD  CONSTRAINT [DF_DispatchMode_mst_DM_Flag]  DEFAULT ((0)) FOR [DM_Flag]
GO
/****** Object:  Default [DF_EmailSMS_mst_EM_NotifyFlag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[EmailSMS_mst] ADD  CONSTRAINT [DF_EmailSMS_mst_EM_NotifyFlag]  DEFAULT ((0)) FOR [EM_NotifyFlag]
GO
/****** Object:  Default [DF_EmailSMS_mst_EM_IsGrowlNotify]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[EmailSMS_mst] ADD  CONSTRAINT [DF_EmailSMS_mst_EM_IsGrowlNotify]  DEFAULT ((0)) FOR [EM_IsGrowlNotify]
GO
/****** Object:  Default [DF_Ewallet_trn_ET_datecode]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Ewallet_Trn] ADD  CONSTRAINT [DF_Ewallet_trn_ET_datecode]  DEFAULT ((0)) FOR [ET_Datecode]
GO
/****** Object:  Default [DF_Ewallet_trn_ET_Type]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Ewallet_Trn] ADD  CONSTRAINT [DF_Ewallet_trn_ET_Type]  DEFAULT ((0)) FOR [ET_Type]
GO
/****** Object:  Default [DF_Ewallet_trn_ET_Trnflag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Ewallet_Trn] ADD  CONSTRAINT [DF_Ewallet_trn_ET_Trnflag]  DEFAULT ((0)) FOR [ET_Trnflag]
GO
/****** Object:  Default [DF_FrEwallet_Trn_FET_Type]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FrEwallet_Trn] ADD  CONSTRAINT [DF_FrEwallet_Trn_FET_Type]  DEFAULT ((0)) FOR [FET_Type]
GO
/****** Object:  Default [DF_FrEwallet_Trn_FET_datecode]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FrEwallet_Trn] ADD  CONSTRAINT [DF_FrEwallet_Trn_FET_datecode]  DEFAULT ((0)) FOR [FET_Datecode]
GO
/****** Object:  Default [DF_FrEwallet_Trn_FET_Trnflag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FrEwallet_Trn] ADD  CONSTRAINT [DF_FrEwallet_Trn_FET_Trnflag]  DEFAULT ((0)) FOR [FET_TrnFlg]
GO
/****** Object:  Default [DF_FRInc_Trn_FI_Joinings]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FRInc_Trn] ADD  CONSTRAINT [DF_FRInc_Trn_FI_Joinings]  DEFAULT ((0)) FOR [FI_Joinings]
GO
/****** Object:  Default [DF_FRInc_Trn_FI_BV]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FRInc_Trn] ADD  CONSTRAINT [DF_FRInc_Trn_FI_BV]  DEFAULT ((0)) FOR [FI_BV]
GO
/****** Object:  Default [DF_GrowthQual_Trn_GQT_Points]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn] ADD  CONSTRAINT [DF_GrowthQual_Trn_GQT_Points]  DEFAULT ((0)) FOR [GQT_Points]
GO
/****** Object:  Default [DF_GrowthQual_Trn_GQT_ConfFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn] ADD  CONSTRAINT [DF_GrowthQual_Trn_GQT_ConfFlg]  DEFAULT ((0)) FOR [GQT_ConfFlg]
GO
/****** Object:  Default [DF_GrowthQual_Trn_GQT_RemainingPts]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn] ADD  CONSTRAINT [DF_GrowthQual_Trn_GQT_RemainingPts]  DEFAULT ((0)) FOR [GQT_RemainingPts]
GO
/****** Object:  Default [DF__binary_dt__MDD_ow__740F363E1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemActDownline_Dtls] ADD  CONSTRAINT [DF__binary_dt__MDD_ow__740F363E1]  DEFAULT ((0)) FOR [MDD_OwnBV]
GO
/****** Object:  Default [DF__binary_dt__MDD_Co__75035A771]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemActDownline_Dtls] ADD  CONSTRAINT [DF__binary_dt__MDD_Co__75035A771]  DEFAULT ((0)) FOR [MDD_ConfOwnBV]
GO
/****** Object:  Default [DF_MemActDownline_Dtls_MDD_ownbv1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemActDownline_Dtls] ADD  CONSTRAINT [DF_MemActDownline_Dtls_MDD_ownbv1]  DEFAULT ((0)) FOR [MDD_OwnPV]
GO
/****** Object:  Default [DF_MemActDownline_Dtls_MDD_Confownbv1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemActDownline_Dtls] ADD  CONSTRAINT [DF_MemActDownline_Dtls_MDD_Confownbv1]  DEFAULT ((0)) FOR [MDD_ConfOwnPV]
GO
/****** Object:  Default [DF_MemDirects_Dtls_MDD_Datecode]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDirects_Dtls] ADD  CONSTRAINT [DF_MemDirects_Dtls_MDD_Datecode]  DEFAULT ((0)) FOR [MDD_DateCode]
GO
/****** Object:  Default [DF_MemDirects_Dtls_MDD_Flag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDirects_Dtls] ADD  CONSTRAINT [DF_MemDirects_Dtls_MDD_Flag]  DEFAULT ((0)) FOR [MDD_Flag]
GO
/****** Object:  Default [DF__binary_dt__MDD_ow__740F363E]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls] ADD  CONSTRAINT [DF__binary_dt__MDD_ow__740F363E]  DEFAULT ((0)) FOR [MDD_OwnBV]
GO
/****** Object:  Default [DF__binary_dt__MDD_Co__75035A77]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls] ADD  CONSTRAINT [DF__binary_dt__MDD_Co__75035A77]  DEFAULT ((0)) FOR [MDD_ConfOwnBV]
GO
/****** Object:  Default [DF_MemDownline_Dtls_MDD_ownbv1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls] ADD  CONSTRAINT [DF_MemDownline_Dtls_MDD_ownbv1]  DEFAULT ((0)) FOR [MDD_OwnPV]
GO
/****** Object:  Default [DF_MemDownline_Dtls_MDD_Confownbv1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls] ADD  CONSTRAINT [DF_MemDownline_Dtls_MDD_Confownbv1]  DEFAULT ((0)) FOR [MDD_ConfOwnPV]
GO
/****** Object:  Default [DF_TempMember_Mst_MPD_NoDepend]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfile_Dtls] ADD  CONSTRAINT [DF_TempMember_Mst_MPD_NoDepend]  DEFAULT ((0)) FOR [MPD_NoDepend]
GO
/****** Object:  Default [DF__tempmembe__MPD_Jo__1F83A428]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfile_Dtls] ADD  CONSTRAINT [DF__tempmembe__MPD_Jo__1F83A428]  DEFAULT ((0)) FOR [MPD_NomAddr]
GO
/****** Object:  Default [DF_TempMember_Mst_MPD_DDNo]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfile_Dtls] ADD  CONSTRAINT [DF_TempMember_Mst_MPD_DDNo]  DEFAULT ((0)) FOR [MPD_DDNo]
GO
/****** Object:  Default [DF_TempMember_Mst_MPD_ReceiptNo]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfile_Dtls] ADD  CONSTRAINT [DF_TempMember_Mst_MPD_ReceiptNo]  DEFAULT ((0)) FOR [MPD_ReceiptNo]
GO
/****** Object:  Default [DF_MemSpill_Dtls_MSD_Flag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemSpill_Dtls] ADD  CONSTRAINT [DF_MemSpill_Dtls_MSD_Flag]  DEFAULT ((0)) FOR [MSD_Flag]
GO
/****** Object:  Default [DF_MemSpill_Dtls_MSD_PvCode]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemSpill_Dtls] ADD  CONSTRAINT [DF_MemSpill_Dtls_MSD_PvCode]  DEFAULT ((0)) FOR [MSD_PvCode]
GO
/****** Object:  Default [DF_MemStatus_Dtls_MSD_IncFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemStatus_Dtls] ADD  CONSTRAINT [DF_MemStatus_Dtls_MSD_IncFlg]  DEFAULT ((0)) FOR [MSD_IncFlg]
GO
/****** Object:  Default [DF_PancardConf_Dtls_UT_AdminID]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PancardConf_Dtls] ADD  CONSTRAINT [DF_PancardConf_Dtls_UT_AdminID]  DEFAULT ((0)) FOR [PCD_AdminID]
GO
/****** Object:  Default [DF_Paymode_mst_PM_DefaultPaymentFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Paymode_mst] ADD  CONSTRAINT [DF_Paymode_mst_PM_DefaultPaymentFlg]  DEFAULT ((0)) FOR [PM_DefaultPaymentFlg]
GO
/****** Object:  Default [DF_Paymode_mst_log_PM_DefaultPaymentFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Paymode_mst_log] ADD  CONSTRAINT [DF_Paymode_mst_log_PM_DefaultPaymentFlg]  DEFAULT ((0)) FOR [PML_DefaultPaymentFlg]
GO
/****** Object:  Default [DF__PCR_Dtls__PD_Tot__355EA205]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Dtls] ADD  CONSTRAINT [DF__PCR_Dtls__PD_Tot__355EA205]  DEFAULT ((0)) FOR [PD_TotTransitQty]
GO
/****** Object:  Default [DF__PCR_Dtls__PD_Tot__3652C63E]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Dtls] ADD  CONSTRAINT [DF__PCR_Dtls__PD_Tot__3652C63E]  DEFAULT ((0)) FOR [PD_TotInQty]
GO
/****** Object:  Default [DF__PCR_Hdr__PH_TotT__3282355A]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Hdr] ADD  CONSTRAINT [DF__PCR_Hdr__PH_TotT__3282355A]  DEFAULT ((0)) FOR [PH_TotTransitQty]
GO
/****** Object:  Default [DF__PCR_Hdr__PH_TotI__33765993]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Hdr] ADD  CONSTRAINT [DF__PCR_Hdr__PH_TotI__33765993]  DEFAULT ((0)) FOR [PH_TotInQty]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_Webflag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_Webflag]  DEFAULT ((0)) FOR [PCT_Webflag]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_WebstatusID]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_WebstatusID]  DEFAULT ((0)) FOR [PCT_WebstatusID]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_Extra1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_Extra1]  DEFAULT ((0)) FOR [PCT_Extra1]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_Extra2]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_Extra2]  DEFAULT ((0)) FOR [PCT_Extra2]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_Extra3]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_Extra3]  DEFAULT ((0)) FOR [PCT_Extra3]
GO
/****** Object:  Default [DF_PDCommission_Trn_PCT_Extra4]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PDCommission_Trn] ADD  CONSTRAINT [DF_PDCommission_Trn_PCT_Extra4]  DEFAULT ((0)) FOR [PCT_Extra4]
GO
/****** Object:  Default [DF_PlanType_Mst_PM_Power]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PlanType_Mst] ADD  CONSTRAINT [DF_PlanType_Mst_PM_Power]  DEFAULT ((0)) FOR [PM_Power]
GO
/****** Object:  Default [DF_PlanType_Mst_PM_AutoTree]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PlanType_Mst] ADD  CONSTRAINT [DF_PlanType_Mst_PM_AutoTree]  DEFAULT ((0)) FOR [PM_AutoTree]
GO
/****** Object:  Default [DF_PlanType_Mst_PM_StarFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PlanType_Mst] ADD  CONSTRAINT [DF_PlanType_Mst_PM_StarFlg]  DEFAULT ((0)) FOR [PM_StarFlg]
GO
/****** Object:  Default [DF_PlanType_Mst_PM_GrDistFlg]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PlanType_Mst] ADD  CONSTRAINT [DF_PlanType_Mst_PM_GrDistFlg]  DEFAULT ((0)) FOR [PM_GrDistFlg]
GO
/****** Object:  Default [DF_PlanType_Mst_PM_Flag]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PlanType_Mst] ADD  CONSTRAINT [DF_PlanType_Mst_PM_Flag]  DEFAULT ((0)) FOR [PM_Flag]
GO
/****** Object:  Default [DF__PurchaseO__PD_Di__5ECACBEC]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Dtls] ADD  CONSTRAINT [DF__PurchaseO__PD_Di__5ECACBEC]  DEFAULT ((0)) FOR [PD_TotInQty]
GO
/****** Object:  Default [DF__PurchaseO__PDL_D__5FBEF025]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Dtls_Log] ADD  CONSTRAINT [DF__PurchaseO__PDL_D__5FBEF025]  DEFAULT ((0)) FOR [PDL_TotInQty]
GO
/****** Object:  Default [DF__PurchaseO__PH_Di__5CE2837A]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Hdr] ADD  CONSTRAINT [DF__PurchaseO__PH_Di__5CE2837A]  DEFAULT ((0)) FOR [PH_TotInQty]
GO
/****** Object:  Default [DF__PurchaseO__PHL_D__5DD6A7B3]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Hdr_Log] ADD  CONSTRAINT [DF__PurchaseO__PHL_D__5DD6A7B3]  DEFAULT ((0)) FOR [PHL_TotInQty]
GO
/****** Object:  Default [DF_SMSUser_SU_Flag]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[SMSUser] ADD  CONSTRAINT [DF_SMSUser_SU_Flag]  DEFAULT ((0)) FOR [SU_Flag]
GO
/****** Object:  Default [DF_SMSVariable_Mst_SVM_NotifyFlag]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[SMSVariable_Mst] ADD  CONSTRAINT [DF_SMSVariable_Mst_SVM_NotifyFlag]  DEFAULT ((0)) FOR [SVM_NotifyFlag]
GO
/****** Object:  Default [DF__StockInBy__SIFD___48717679]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Dtls] ADD  CONSTRAINT [DF__StockInBy__SIFD___48717679]  DEFAULT ((0)) FOR [SIFD_TotTransitQty]
GO
/****** Object:  Default [DF__StockInBy__SIFD___49659AB2]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Dtls] ADD  CONSTRAINT [DF__StockInBy__SIFD___49659AB2]  DEFAULT ((0)) FOR [SIFD_TotInQty]
GO
/****** Object:  Default [DF__StockInBy__SIFH___46892E07]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Hdr] ADD  CONSTRAINT [DF__StockInBy__SIFH___46892E07]  DEFAULT ((0)) FOR [SIFH_TotTransitQty]
GO
/****** Object:  Default [DF__StockInBy__SIFH___477D5240]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Hdr] ADD  CONSTRAINT [DF__StockInBy__SIFH___477D5240]  DEFAULT ((0)) FOR [SIFH_TotInQty]
GO
/****** Object:  Default [DF__StockInBy__SID_T__4A8EC915]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByHO_Dtls] ADD  CONSTRAINT [DF__StockInBy__SID_T__4A8EC915]  DEFAULT ((0)) FOR [SID_TotInQty]
GO
/****** Object:  Default [DF__StockInBy__SIH_T__499AA4DC]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByHO_Hdr] ADD  CONSTRAINT [DF__StockInBy__SIH_T__499AA4DC]  DEFAULT ((0)) FOR [SIH_TotInQty]
GO
/****** Object:  Default [DF__StockOut___SOD_T__44A0E595]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Dtls] ADD  CONSTRAINT [DF__StockOut___SOD_T__44A0E595]  DEFAULT ((0)) FOR [SOD_TotTransitQty]
GO
/****** Object:  Default [DF__StockOut___SOD_T__459509CE]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Dtls] ADD  CONSTRAINT [DF__StockOut___SOD_T__459509CE]  DEFAULT ((0)) FOR [SOD_TotInQty]
GO
/****** Object:  Default [DF__StockOut___SOH_T__42B89D23]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Hdr] ADD  CONSTRAINT [DF__StockOut___SOH_T__42B89D23]  DEFAULT ((0)) FOR [SOH_TotTransitQty]
GO
/****** Object:  Default [DF__StockOut___SOH_T__43ACC15C]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Hdr] ADD  CONSTRAINT [DF__StockOut___SOH_T__43ACC15C]  DEFAULT ((0)) FOR [SOH_TotInQty]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_NoDepend]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_NoDepend]  DEFAULT ((0)) FOR [TM_NoDepend]
GO
/****** Object:  Default [DF__tempmembe__TM_Jo__1F83A428]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF__tempmembe__TM_Jo__1F83A428]  DEFAULT ((0)) FOR [TM_JoinFr]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_DDNo]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_DDNo]  DEFAULT ((0)) FOR [TM_DDNo]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_DDAmt]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_DDAmt]  DEFAULT ((0)) FOR [TM_DDAmt]
GO
/****** Object:  Default [DF__tempmembe__TM_PV__5F7E2DAC]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF__tempmembe__TM_PV__5F7E2DAC]  DEFAULT ((0)) FOR [TM_PVCode]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_PvType]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_PvType]  DEFAULT ((0)) FOR [TM_PvType]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_ReceiptNo]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_ReceiptNo]  DEFAULT ((0)) FOR [TM_ReceiptNo]
GO
/****** Object:  Default [DF_TempMember_Mst_Data_TM_Theme]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMember_Mst_Data] ADD  CONSTRAINT [DF_TempMember_Mst_Data_TM_Theme]  DEFAULT ('Default') FOR [TM_Theme]
GO
/****** Object:  Default [DF_TempMemProfile_Dtls_MPD_NoDepend]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMemProfile_Dtls] ADD  CONSTRAINT [DF_TempMemProfile_Dtls_MPD_NoDepend]  DEFAULT ((0)) FOR [MPD_NoDepend]
GO
/****** Object:  Default [DF_TempMemProfile_Dtls_MPD_ReceiptNo]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMemProfile_Dtls] ADD  CONSTRAINT [DF_TempMemProfile_Dtls_MPD_ReceiptNo]  DEFAULT ((0)) FOR [MPD_ReceiptNo]
GO
/****** Object:  Default [DF_Themes_Mst_tm_activeflg]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[Themes_Mst] ADD  CONSTRAINT [DF_Themes_Mst_tm_activeflg]  DEFAULT ((0)) FOR [tm_activeflg]
GO
/****** Object:  ForeignKey [FK_AccountLedger_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AccountLedger_Trn]  WITH CHECK ADD  CONSTRAINT [FK_AccountLedger_Trn_DateCode_Trn] FOREIGN KEY([ALT_DateCode], [ALT_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[AccountLedger_Trn] CHECK CONSTRAINT [FK_AccountLedger_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_AccountLedger_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AccountLedger_Trn]  WITH CHECK ADD  CONSTRAINT [FK_AccountLedger_Trn_MemJoining_Dtls] FOREIGN KEY([ALT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[AccountLedger_Trn] CHECK CONSTRAINT [FK_AccountLedger_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_AchieversListEarning_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListEarning_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_AchieversListEarning_Dtls_MemJoining_Dtls] FOREIGN KEY([AED_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[AchieversListEarning_Dtls] CHECK CONSTRAINT [FK_AchieversListEarning_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_AchieversListEarning_Dtls_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AchieversListEarning_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_AchieversListEarning_Dtls_PlanType_Mst] FOREIGN KEY([AED_PlanType])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[AchieversListEarning_Dtls] CHECK CONSTRAINT [FK_AchieversListEarning_Dtls_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_AdditionalInc_Trn_IncomeType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AdditionalInc_Trn]  WITH CHECK ADD  CONSTRAINT [FK_AdditionalInc_Trn_IncomeType_Mst] FOREIGN KEY([AIT_PIMID])
REFERENCES [dbo].[IncomeType_Mst] ([PIM_ID])
GO
ALTER TABLE [dbo].[AdditionalInc_Trn] CHECK CONSTRAINT [FK_AdditionalInc_Trn_IncomeType_Mst]
GO
/****** Object:  ForeignKey [FK_Advance_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn]  WITH CHECK ADD  CONSTRAINT [FK_Advance_Trn_DateCode_Trn] FOREIGN KEY([ADT_Datecode], [ADT_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[Advance_Trn] CHECK CONSTRAINT [FK_Advance_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_Advance_Trn_MemJoining_Dtls1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Advance_Trn_MemJoining_Dtls1] FOREIGN KEY([ADT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Advance_Trn] CHECK CONSTRAINT [FK_Advance_Trn_MemJoining_Dtls1]
GO
/****** Object:  ForeignKey [FK_Advance_trn_log_Advance_trn_log]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Advance_trn_log]  WITH CHECK ADD  CONSTRAINT [FK_Advance_trn_log_Advance_trn_log] FOREIGN KEY([ADTL_ID])
REFERENCES [dbo].[Advance_trn_log] ([ADTL_ID])
GO
ALTER TABLE [dbo].[Advance_trn_log] CHECK CONSTRAINT [FK_Advance_trn_log_Advance_trn_log]
GO
/****** Object:  ForeignKey [FK_AutoFrontLine_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AutoFrontLine_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoFrontLine_Trn_MemJoining_Dtls] FOREIGN KEY([AT_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[AutoFrontLine_Trn] CHECK CONSTRAINT [FK_AutoFrontLine_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_AutoMatrix_Trn_AutoCode_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AutoMatrix_Trn]  WITH CHECK ADD  CONSTRAINT [FK_AutoMatrix_Trn_AutoCode_Mst] FOREIGN KEY([ATM_TreeID])
REFERENCES [dbo].[AutoCode_Mst] ([ACM_TreeID])
GO
ALTER TABLE [dbo].[AutoMatrix_Trn] CHECK CONSTRAINT [FK_AutoMatrix_Trn_AutoCode_Mst]
GO
/****** Object:  ForeignKey [FK_AutoMatrix_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AutoMatrix_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoMatrix_Trn_MemJoining_Dtls] FOREIGN KEY([ATM_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[AutoMatrix_Trn] CHECK CONSTRAINT [FK_AutoMatrix_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_AutomatrixTree_Mst_AutoCode_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AutomatrixTree_Mst]  WITH CHECK ADD  CONSTRAINT [FK_AutomatrixTree_Mst_AutoCode_Mst] FOREIGN KEY([AM_TreeID])
REFERENCES [dbo].[AutoCode_Mst] ([ACM_TreeID])
GO
ALTER TABLE [dbo].[AutomatrixTree_Mst] CHECK CONSTRAINT [FK_AutomatrixTree_Mst_AutoCode_Mst]
GO
/****** Object:  ForeignKey [FK_AwardDispatch_trn_DispatchMode_mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AwardDispatch_trn]  WITH CHECK ADD  CONSTRAINT [FK_AwardDispatch_trn_DispatchMode_mst] FOREIGN KEY([ADT_DMID])
REFERENCES [dbo].[DispatchMode_mst] ([DM_ID])
GO
ALTER TABLE [dbo].[AwardDispatch_trn] CHECK CONSTRAINT [FK_AwardDispatch_trn_DispatchMode_mst]
GO
/****** Object:  ForeignKey [FK_AwardDispatch_trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AwardDispatch_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_AwardDispatch_trn_MemJoining_Dtls] FOREIGN KEY([ADT_MJDMemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[AwardDispatch_trn] CHECK CONSTRAINT [FK_AwardDispatch_trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_AwardDispatch_trn_Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AwardDispatch_trn]  WITH CHECK ADD  CONSTRAINT [FK_AwardDispatch_trn_Offer_Hdr] FOREIGN KEY([ADT_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[AwardDispatch_trn] CHECK CONSTRAINT [FK_AwardDispatch_trn_Offer_Hdr]
GO
/****** Object:  ForeignKey [FK_AwardType_Mst_AwardType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[AwardType_Mst]  WITH CHECK ADD  CONSTRAINT [FK_AwardType_Mst_AwardType_Mst] FOREIGN KEY([ATM_ID])
REFERENCES [dbo].[AwardType_Mst] ([ATM_ID])
GO
ALTER TABLE [dbo].[AwardType_Mst] CHECK CONSTRAINT [FK_AwardType_Mst_AwardType_Mst]
GO
/****** Object:  ForeignKey [FK_BinInc_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[BinInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_BinInc_Trn_DateCode_Trn] FOREIGN KEY([BI_DateCode], [BI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[BinInc_Trn] CHECK CONSTRAINT [FK_BinInc_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_BinInc_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[BinInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_BinInc_Trn_MemJoining_Dtls] FOREIGN KEY([BI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[BinInc_Trn] CHECK CONSTRAINT [FK_BinInc_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_BinInc_TrnDummy_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[BinInc_TrnDummy]  WITH NOCHECK ADD  CONSTRAINT [FK_BinInc_TrnDummy_DateCode_Trn] FOREIGN KEY([BI_DateCode], [BI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[BinInc_TrnDummy] CHECK CONSTRAINT [FK_BinInc_TrnDummy_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_BinInc_TrnDummy_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[BinInc_TrnDummy]  WITH NOCHECK ADD  CONSTRAINT [FK_BinInc_TrnDummy_MemJoining_Dtls] FOREIGN KEY([BI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[BinInc_TrnDummy] CHECK CONSTRAINT [FK_BinInc_TrnDummy_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_ChangeDispatchBy_Trn_Franchise_mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ChangeDispatchBy_Trn]  WITH CHECK ADD  CONSTRAINT [FK_ChangeDispatchBy_Trn_Franchise_mst] FOREIGN KEY([CDT_FromFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[ChangeDispatchBy_Trn] CHECK CONSTRAINT [FK_ChangeDispatchBy_Trn_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_ChangeDispatchBy_Trn_Franchise_mst1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ChangeDispatchBy_Trn]  WITH CHECK ADD  CONSTRAINT [FK_ChangeDispatchBy_Trn_Franchise_mst1] FOREIGN KEY([CDT_ToFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[ChangeDispatchBy_Trn] CHECK CONSTRAINT [FK_ChangeDispatchBy_Trn_Franchise_mst1]
GO
/****** Object:  ForeignKey [FK_ChequePrint_Dtls_ChequePrint_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ChequePrint_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_ChequePrint_Dtls_ChequePrint_Hdr] FOREIGN KEY([CPD_CPHID])
REFERENCES [dbo].[ChequePrint_Hdr] ([CPH_ID])
GO
ALTER TABLE [dbo].[ChequePrint_Dtls] CHECK CONSTRAINT [FK_ChequePrint_Dtls_ChequePrint_Hdr]
GO
/****** Object:  ForeignKey [FK_ChequePrint_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ChequePrint_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequePrint_Dtls_MemJoining_Dtls] FOREIGN KEY([CPD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[ChequePrint_Dtls] CHECK CONSTRAINT [FK_ChequePrint_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_ChequePrint_Hdr_ChqPrintBank_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ChequePrint_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_ChequePrint_Hdr_ChqPrintBank_Mst] FOREIGN KEY([CPH_CPBMID])
REFERENCES [dbo].[ChqPrintBank_Mst] ([CPBM_ID])
GO
ALTER TABLE [dbo].[ChequePrint_Hdr] CHECK CONSTRAINT [FK_ChequePrint_Hdr_ChqPrintBank_Mst]
GO
/****** Object:  ForeignKey [FK_ChequePrint_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
--ALTER TABLE [dbo].[ChequePrint_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequePrint_Trn_MemJoining_Dtls] FOREIGN KEY([CPT_MemID])
--REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
--GO
--ALTER TABLE [dbo].[ChequePrint_Trn] CHECK CONSTRAINT [FK_ChequePrint_Trn_MemJoining_Dtls]
--GO
/****** Object:  ForeignKey [FK_Comment_Dtls_Comment_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Comment_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Dtls_Comment_Hdr] FOREIGN KEY([CD_CHID])
REFERENCES [dbo].[Comment_Hdr] ([CH_ID])
GO
ALTER TABLE [dbo].[Comment_Dtls] CHECK CONSTRAINT [FK_Comment_Dtls_Comment_Hdr]
GO
/****** Object:  ForeignKey [FK_Comment_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Comment_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_Comment_Dtls_MemJoining_Dtls] FOREIGN KEY([CD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Comment_Dtls] CHECK CONSTRAINT [FK_Comment_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Comment_Hdr_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Comment_Hdr]  WITH NOCHECK ADD  CONSTRAINT [FK_Comment_Hdr_MemJoining_Dtls] FOREIGN KEY([CH_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Comment_Hdr] CHECK CONSTRAINT [FK_Comment_Hdr_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_CompanyID_Mst_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[CompanyID_Mst]  WITH NOCHECK ADD  CONSTRAINT [FK_CompanyID_Mst_MemJoining_Dtls] FOREIGN KEY([CM_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[CompanyID_Mst] CHECK CONSTRAINT [FK_CompanyID_Mst_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_CompanyID_mst_log_CompanyID_mst_log]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[CompanyID_mst_log]  WITH NOCHECK ADD  CONSTRAINT [FK_CompanyID_mst_log_CompanyID_mst_log] FOREIGN KEY([CM_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[CompanyID_mst_log] CHECK CONSTRAINT [FK_CompanyID_mst_log_CompanyID_mst_log]
GO
/****** Object:  ForeignKey [FK_Courier_Mst_Log_Courier_Mst]    Script Date: 09/16/2016 10:42:03 ******/
--ALTER TABLE [dbo].[Courier_Mst_Log]  WITH CHECK ADD  CONSTRAINT [FK_Courier_Mst_Log_Courier_Mst] FOREIGN KEY([CML_CMID])
--REFERENCES [dbo].[Courier_Mst] ([CM_ID])
--GO
--ALTER TABLE [dbo].[Courier_Mst_Log] CHECK CONSTRAINT [FK_Courier_Mst_Log_Courier_Mst]
--GO
/****** Object:  ForeignKey [FK_DailyCycle_Mst_DailyCycleOpt_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DailyCycle_Mst]  WITH CHECK ADD  CONSTRAINT [FK_DailyCycle_Mst_DailyCycleOpt_Mst] FOREIGN KEY([DCM_DOMID])
REFERENCES [dbo].[DailyCycleOpt_Mst] ([DOM_ID])
GO
ALTER TABLE [dbo].[DailyCycle_Mst] CHECK CONSTRAINT [FK_DailyCycle_Mst_DailyCycleOpt_Mst]
GO
/****** Object:  ForeignKey [FK_DailySet_Trn_DailySet_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DailySet_Trn]  WITH CHECK ADD  CONSTRAINT [FK_DailySet_Trn_DailySet_Trn] FOREIGN KEY([DT_ID])
REFERENCES [dbo].[DailySet_Trn] ([DT_ID])
GO
ALTER TABLE [dbo].[DailySet_Trn] CHECK CONSTRAINT [FK_DailySet_Trn_DailySet_Trn]
GO
/****** Object:  ForeignKey [FK_DateCode_Trn_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DateCode_Trn]  WITH CHECK ADD  CONSTRAINT [FK_DateCode_Trn_PlanType_Mst] FOREIGN KEY([DCT_Type])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[DateCode_Trn] CHECK CONSTRAINT [FK_DateCode_Trn_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_Deduction_Dtls_DeductionHead_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Deduction_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Deduction_Dtls_DeductionHead_Mst] FOREIGN KEY([DD_DHMID])
REFERENCES [dbo].[DeductionHead_Mst] ([DHM_ID])
GO
ALTER TABLE [dbo].[Deduction_Dtls] CHECK CONSTRAINT [FK_Deduction_Dtls_DeductionHead_Mst]
GO
/****** Object:  ForeignKey [FK_Deduction_Hdr_DeductionHead_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Deduction_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_Deduction_Hdr_DeductionHead_Mst] FOREIGN KEY([DH_DHMID])
REFERENCES [dbo].[DeductionHead_Mst] ([DHM_ID])
GO
ALTER TABLE [dbo].[Deduction_Hdr] CHECK CONSTRAINT [FK_Deduction_Hdr_DeductionHead_Mst]
GO
/****** Object:  ForeignKey [FK_Deduction_Log_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Deduction_Log]  WITH NOCHECK ADD  CONSTRAINT [FK_Deduction_Log_DateCode_Trn] FOREIGN KEY([DL_Datecode], [DL_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[Deduction_Log] CHECK CONSTRAINT [FK_Deduction_Log_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_DGInc_trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DGInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_DGInc_trn_DateCode_Trn] FOREIGN KEY([dt_datecode], [dt_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[DGInc_trn] CHECK CONSTRAINT [FK_DGInc_trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_DGInc_trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DGInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_DGInc_trn_MemJoining_Dtls] FOREIGN KEY([dt_memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[DGInc_trn] CHECK CONSTRAINT [FK_DGInc_trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_DwnLnOfferAchiever_Trn_DwnLnOfferAchiever_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DwnLnOfferAchiever_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_DwnLnOfferAchiever_Trn_DwnLnOfferAchiever_Trn] FOREIGN KEY([DOA_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[DwnLnOfferAchiever_Trn] CHECK CONSTRAINT [FK_DwnLnOfferAchiever_Trn_DwnLnOfferAchiever_Trn]
GO
/****** Object:  ForeignKey [FK_DwnLnOfferAchiever_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[DwnLnOfferAchiever_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_DwnLnOfferAchiever_Trn_MemJoining_Dtls] FOREIGN KEY([DOA_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[DwnLnOfferAchiever_Trn] CHECK CONSTRAINT [FK_DwnLnOfferAchiever_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Feder_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Feder_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Feder_Trn_MemJoining_Dtls] FOREIGN KEY([FT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Feder_Trn] CHECK CONSTRAINT [FK_Feder_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_FeedBack_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[FeedBack_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_FeedBack_Trn_MemJoining_Dtls] FOREIGN KEY([FT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[FeedBack_Trn] CHECK CONSTRAINT [FK_FeedBack_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Field_Dtls_Field_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Field_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Field_Dtls_Field_Hdr] FOREIGN KEY([FM_FHID])
REFERENCES [dbo].[Field_Hdr] ([FH_ID])
GO
ALTER TABLE [dbo].[Field_Dtls] CHECK CONSTRAINT [FK_Field_Dtls_Field_Hdr]
GO
/****** Object:  ForeignKey [FK_Field_Hdr_Field_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Field_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_Field_Hdr_Field_Hdr] FOREIGN KEY([FH_ID])
REFERENCES [dbo].[Field_Hdr] ([FH_ID])
GO
ALTER TABLE [dbo].[Field_Hdr] CHECK CONSTRAINT [FK_Field_Hdr_Field_Hdr]
GO
/****** Object:  ForeignKey [FK_ForgotPwd_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ForgotPwd_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_ForgotPwd_Trn_MemJoining_Dtls] FOREIGN KEY([fp_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[ForgotPwd_Trn] CHECK CONSTRAINT [FK_ForgotPwd_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_GRInc_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GRInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_GRInc_Trn_DateCode_Trn] FOREIGN KEY([GI_datecode], [GI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[GRInc_Trn] CHECK CONSTRAINT [FK_GRInc_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_GRInc_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GRInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_GRInc_Trn_MemJoining_Dtls] FOREIGN KEY([GI_sponid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[GRInc_Trn] CHECK CONSTRAINT [FK_GRInc_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_GRInc_Trn_MemJoining_Dtls2]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GRInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_GRInc_Trn_MemJoining_Dtls2] FOREIGN KEY([GI_memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[GRInc_Trn] CHECK CONSTRAINT [FK_GRInc_Trn_MemJoining_Dtls2]
GO
/****** Object:  ForeignKey [FK_Group_Dtls_Group_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Group_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Group_Dtls_Group_Hdr] FOREIGN KEY([GD_GroupID])
REFERENCES [dbo].[Group_Hdr] ([gh_GroupID])
GO
ALTER TABLE [dbo].[Group_Dtls] CHECK CONSTRAINT [FK_Group_Dtls_Group_Hdr]
GO
/****** Object:  ForeignKey [FK_GrowthQual_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_GrowthQual_Trn_DateCode_Trn] FOREIGN KEY([GQT_DtCode], [GQT_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[GrowthQual_Trn] CHECK CONSTRAINT [FK_GrowthQual_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_GrowthQual_Trn_GrowthTarget_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn]  WITH CHECK ADD  CONSTRAINT [FK_GrowthQual_Trn_GrowthTarget_Mst] FOREIGN KEY([GQT_Cycle])
REFERENCES [dbo].[GrowthTarget_Mst] ([GTM_ID])
GO
ALTER TABLE [dbo].[GrowthQual_Trn] CHECK CONSTRAINT [FK_GrowthQual_Trn_GrowthTarget_Mst]
GO
/****** Object:  ForeignKey [FK_GrowthQual_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[GrowthQual_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_GrowthQual_Trn_MemJoining_Dtls] FOREIGN KEY([GQT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[GrowthQual_Trn] CHECK CONSTRAINT [FK_GrowthQual_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_IDWiseDownline_Dtls_IDD_DOWNID_MemJoining_Dtls_MJD_MemID]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[IDWiseDownline_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_IDWiseDownline_Dtls_IDD_DOWNID_MemJoining_Dtls_MJD_MemID] FOREIGN KEY([IDD_DOWNID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[IDWiseDownline_Dtls] CHECK CONSTRAINT [FK_IDWiseDownline_Dtls_IDD_DOWNID_MemJoining_Dtls_MJD_MemID]
GO
/****** Object:  ForeignKey [FK_IDWiseDownline_Dtls_IDD_MemID_MemJoining_Dtls_MJD_MemID]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[IDWiseDownline_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_IDWiseDownline_Dtls_IDD_MemID_MemJoining_Dtls_MJD_MemID] FOREIGN KEY([IDD_MEMID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[IDWiseDownline_Dtls] CHECK CONSTRAINT [FK_IDWiseDownline_Dtls_IDD_MemID_MemJoining_Dtls_MJD_MemID]
GO
/****** Object:  ForeignKey [FK_Incentive_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Incentive_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Incentive_Trn_DateCode_Trn] FOREIGN KEY([IT_Datecode], [IT_type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[Incentive_Trn] CHECK CONSTRAINT [FK_Incentive_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_Incentive_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Incentive_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Incentive_Trn_MemJoining_Dtls] FOREIGN KEY([IT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Incentive_Trn] CHECK CONSTRAINT [FK_Incentive_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_IncomeType_Mst_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[IncomeType_Mst]  WITH NOCHECK ADD  CONSTRAINT [FK_IncomeType_Mst_PlanType_Mst] FOREIGN KEY([PIM_PMID])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[IncomeType_Mst] CHECK CONSTRAINT [FK_IncomeType_Mst_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_JoiningPlan_Mst_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[JoiningPlan_Mst]  WITH CHECK ADD  CONSTRAINT [FK_JoiningPlan_Mst_PlanType_Mst] FOREIGN KEY([JM_ID])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[JoiningPlan_Mst] CHECK CONSTRAINT [FK_JoiningPlan_Mst_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_LevelCommissionInc_trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[LevelCommissionInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_LevelCommissionInc_trn_DateCode_Trn] FOREIGN KEY([LI_Datecode], [LI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[LevelCommissionInc_trn] CHECK CONSTRAINT [FK_LevelCommissionInc_trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_LevelCommissionInc_trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[LevelCommissionInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_LevelCommissionInc_trn_MemJoining_Dtls] FOREIGN KEY([LI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[LevelCommissionInc_trn] CHECK CONSTRAINT [FK_LevelCommissionInc_trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_LevelInc_trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[LevelInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_LevelInc_trn_DateCode_Trn] FOREIGN KEY([LI_Datecode], [LI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[LevelInc_trn] CHECK CONSTRAINT [FK_LevelInc_trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_LevelInc_trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[LevelInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_LevelInc_trn_MemJoining_Dtls] FOREIGN KEY([LI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[LevelInc_trn] CHECK CONSTRAINT [FK_LevelInc_trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MakePayment_Dtls_MakePayment_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MakePayment_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_MakePayment_Dtls_MakePayment_Hdr] FOREIGN KEY([MPD_MPHID])
REFERENCES [dbo].[MakePayment_Hdr] ([MPH_ID])
GO
ALTER TABLE [dbo].[MakePayment_Dtls] CHECK CONSTRAINT [FK_MakePayment_Dtls_MakePayment_Hdr]
GO
/****** Object:  ForeignKey [FK_MakePayment_Hdr_PayProcess_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MakePayment_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_MakePayment_Hdr_PayProcess_Hdr] FOREIGN KEY([MPH_PPHCode])
REFERENCES [dbo].[PayProcess_Hdr] ([PPH_Code])
GO
ALTER TABLE [dbo].[MakePayment_Hdr] CHECK CONSTRAINT [FK_MakePayment_Hdr_PayProcess_Hdr]
GO
/****** Object:  ForeignKey [FK_MaxID_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MaxID_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_MaxID_Trn_DateCode_Trn] FOREIGN KEY([MIDT_DateCode], [MIDT_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[MaxID_Trn] CHECK CONSTRAINT [FK_MaxID_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_MaxPV_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MaxPV_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_MaxPV_Trn_DateCode_Trn] FOREIGN KEY([MT_Datecode], [MT_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[MaxPV_Trn] CHECK CONSTRAINT [FK_MaxPV_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_MaxPV_Trn_PvType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MaxPV_Trn]  WITH CHECK ADD  CONSTRAINT [FK_MaxPV_Trn_PvType_Mst] FOREIGN KEY([MT_PvType])
REFERENCES [dbo].[PvType_Dtls] ([PTD_ID])
GO
ALTER TABLE [dbo].[MaxPV_Trn] CHECK CONSTRAINT [FK_MaxPV_Trn_PvType_Mst]
GO
/****** Object:  ForeignKey [FK_MemActDownline_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemActDownline_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemActDownline_Dtls_MemJoining_Dtls] FOREIGN KEY([MDD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemActDownline_Dtls] CHECK CONSTRAINT [FK_MemActDownline_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemDirects_Dtls_MemDirects_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDirects_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemDirects_Dtls_MemDirects_Dtls] FOREIGN KEY([MDD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemDirects_Dtls] CHECK CONSTRAINT [FK_MemDirects_Dtls_MemDirects_Dtls]
GO
/****** Object:  ForeignKey [FK_MemDirects_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDirects_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemDirects_Dtls_MemJoining_Dtls] FOREIGN KEY([MDD_DirectID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemDirects_Dtls] CHECK CONSTRAINT [FK_MemDirects_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemDownline_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemDownline_Dtls_MemJoining_Dtls] FOREIGN KEY([MDD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemDownline_Dtls] CHECK CONSTRAINT [FK_MemDownline_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemDownline_Dtls_M2web_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemDownline_Dtls_M2web]  WITH CHECK ADD  CONSTRAINT [FK_MemDownline_Dtls_M2web_MemJoining_Dtls] FOREIGN KEY([MDD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemDownline_Dtls_M2web] CHECK CONSTRAINT [FK_MemDownline_Dtls_M2web_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemGracePeriod_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemGracePeriod_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_MemGracePeriod_Trn_MemJoining_Dtls] FOREIGN KEY([MGT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemGracePeriod_Trn] CHECK CONSTRAINT [FK_MemGracePeriod_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemGracePeriod_Trn_Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemGracePeriod_Trn]  WITH CHECK ADD  CONSTRAINT [FK_MemGracePeriod_Trn_Offer_Hdr] FOREIGN KEY([MGT_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[MemGracePeriod_Trn] CHECK CONSTRAINT [FK_MemGracePeriod_Trn_Offer_Hdr]
GO
/****** Object:  ForeignKey [FK_MemLogin_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemLogin_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemLogin_Dtls_MemJoining_Dtls] FOREIGN KEY([MLD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemLogin_Dtls] CHECK CONSTRAINT [FK_MemLogin_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemProfile_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfile_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemProfile_Dtls_MemJoining_Dtls] FOREIGN KEY([MPD_MemId])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemProfile_Dtls] CHECK CONSTRAINT [FK_MemProfile_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemProfileRequest_Dtls_MemProfile_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemProfileRequest_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemProfileRequest_Dtls_MemProfile_Dtls] FOREIGN KEY([MPD_MemId])
REFERENCES [dbo].[MemProfile_Dtls] ([MPD_MemId])
GO
ALTER TABLE [dbo].[MemProfileRequest_Dtls] CHECK CONSTRAINT [FK_MemProfileRequest_Dtls_MemProfile_Dtls]
GO
/****** Object:  ForeignKey [FK_MemSpill_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemSpill_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemSpill_Dtls_MemJoining_Dtls] FOREIGN KEY([MSD_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemSpill_Dtls] CHECK CONSTRAINT [FK_MemSpill_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemSpill_Dtls_MemJoining_Dtls1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemSpill_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_MemSpill_Dtls_MemJoining_Dtls1] FOREIGN KEY([MSD_SpillID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemSpill_Dtls] CHECK CONSTRAINT [FK_MemSpill_Dtls_MemJoining_Dtls1]
GO
/****** Object:  ForeignKey [FK_MemStatus_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemStatus_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_MemStatus_Dtls_MemJoining_Dtls] FOREIGN KEY([MSD_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[MemStatus_Dtls] CHECK CONSTRAINT [FK_MemStatus_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_MemStatus_Dtls_MemStatus_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemStatus_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_MemStatus_Dtls_MemStatus_Mst] FOREIGN KEY([MSD_MSMID])
REFERENCES [dbo].[MemStatus_Mst] ([MSM_ID])
GO
ALTER TABLE [dbo].[MemStatus_Dtls] CHECK CONSTRAINT [FK_MemStatus_Dtls_MemStatus_Mst]
GO
/****** Object:  ForeignKey [FK_MemStatus_Dtls_PvType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[MemStatus_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_MemStatus_Dtls_PvType_Mst] FOREIGN KEY([MSD_PvType])
REFERENCES [dbo].[PvType_Dtls] ([PTD_ID])
GO
ALTER TABLE [dbo].[MemStatus_Dtls] CHECK CONSTRAINT [FK_MemStatus_Dtls_PvType_Mst]
GO
/****** Object:  ForeignKey [FK_Narration_Mst_Log_User_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Narration_Mst_Log]  WITH NOCHECK ADD  CONSTRAINT [FK_Narration_Mst_Log_User_Hdr] FOREIGN KEY([NL_UMID])
REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
ALTER TABLE [dbo].[Narration_Mst_Log] CHECK CONSTRAINT [FK_Narration_Mst_Log_User_Hdr]
GO
/****** Object:  ForeignKey [FK_Notification_Trn_Notification_Trn]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Notification_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Notification_Trn_Notification_Trn] FOREIGN KEY([NT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Notification_Trn] CHECK CONSTRAINT [FK_Notification_Trn_Notification_Trn]
GO
/****** Object:  ForeignKey [FK_Offer_Dtls_AwardCriteria_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Offer_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Offer_Dtls_AwardCriteria_Mst] FOREIGN KEY([OD_ACMID])
REFERENCES [dbo].[AwardCriteria_Mst] ([ACM_ID])
GO
ALTER TABLE [dbo].[Offer_Dtls] CHECK CONSTRAINT [FK_Offer_Dtls_AwardCriteria_Mst]
GO
/****** Object:  ForeignKey [FK_Offer_Dtls_Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Offer_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_Offer_Dtls_Offer_Hdr] FOREIGN KEY([OD_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[Offer_Dtls] CHECK CONSTRAINT [FK_Offer_Dtls_Offer_Hdr]
GO
/****** Object:  ForeignKey [FK_Offer_Hdr_Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Offer_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_Offer_Hdr_Offer_Hdr] FOREIGN KEY([OH_ID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[Offer_Hdr] CHECK CONSTRAINT [FK_Offer_Hdr_Offer_Hdr]
GO
/****** Object:  ForeignKey [FK_Offer_Hdr_Scheme_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Offer_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_Offer_Hdr_Scheme_Mst] FOREIGN KEY([OH_SMID])
REFERENCES [dbo].[Scheme_Mst] ([SM_ID])
GO
ALTER TABLE [dbo].[Offer_Hdr] CHECK CONSTRAINT [FK_Offer_Hdr_Scheme_Mst]
GO
/****** Object:  ForeignKey [FK_OfferAchiever_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferAchiever_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_OfferAchiever_Trn_MemJoining_Dtls] FOREIGN KEY([OAT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[OfferAchiever_Trn] CHECK CONSTRAINT [FK_OfferAchiever_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_OfferClaim_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferClaim_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_OfferClaim_Trn_MemJoining_Dtls] FOREIGN KEY([OCT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[OfferClaim_Trn] CHECK CONSTRAINT [FK_OfferClaim_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_OfferClaim_Trn_Offer_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferClaim_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_OfferClaim_Trn_Offer_Mst] FOREIGN KEY([OCT_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[OfferClaim_Trn] CHECK CONSTRAINT [FK_OfferClaim_Trn_Offer_Mst]
GO
/****** Object:  ForeignKey [FK_OfferForceful_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferTempForceful_Trn]  WITH CHECK ADD  CONSTRAINT [FK_OfferForceful_Trn_MemJoining_Dtls] FOREIGN KEY([OFT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[OfferTempForceful_Trn] CHECK CONSTRAINT [FK_OfferForceful_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_OfferForceful_Trn_Offer_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferTempForceful_Trn]  WITH CHECK ADD  CONSTRAINT [FK_OfferForceful_Trn_Offer_Hdr] FOREIGN KEY([OFT_OHID])
REFERENCES [dbo].[Offer_Hdr] ([OH_ID])
GO
ALTER TABLE [dbo].[OfferTempForceful_Trn] CHECK CONSTRAINT [FK_OfferForceful_Trn_Offer_Hdr]
GO
/****** Object:  ForeignKey [FK_OfferTempQual_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[OfferTempQual_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_OfferTempQual_Trn_MemJoining_Dtls] FOREIGN KEY([OT_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[OfferTempQual_Trn] CHECK CONSTRAINT [FK_OfferTempQual_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_PaidID_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PaidID_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_PaidID_Trn_MemJoining_Dtls] FOREIGN KEY([PI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[PaidID_Trn] CHECK CONSTRAINT [FK_PaidID_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_PayProcess_Dtls_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PayProcess_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_PayProcess_Dtls_MemJoining_Dtls] FOREIGN KEY([PPD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[PayProcess_Dtls] CHECK CONSTRAINT [FK_PayProcess_Dtls_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_PayProcess_Dtls_PayProcess_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PayProcess_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_PayProcess_Dtls_PayProcess_Hdr] FOREIGN KEY([PPD_Code])
REFERENCES [dbo].[PayProcess_Hdr] ([PPH_Code])
GO
ALTER TABLE [dbo].[PayProcess_Dtls] CHECK CONSTRAINT [FK_PayProcess_Dtls_PayProcess_Hdr]
GO
/****** Object:  ForeignKey [FK_PCR_Hdr_Franchise_mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_PCR_Hdr_Franchise_mst] FOREIGN KEY([PH_FromFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[PCR_Hdr] CHECK CONSTRAINT [FK_PCR_Hdr_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_PCR_Hdr_Franchise_mst1]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_PCR_Hdr_Franchise_mst1] FOREIGN KEY([PH_ToFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[PCR_Hdr] CHECK CONSTRAINT [FK_PCR_Hdr_Franchise_mst1]
GO
/****** Object:  ForeignKey [FK_PCR_Hdr_Paymode_mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PCR_Hdr]  WITH NOCHECK ADD  CONSTRAINT [FK_PCR_Hdr_Paymode_mst] FOREIGN KEY([PH_Paymode])
REFERENCES [dbo].[Paymode_mst] ([PM_ID])
GO
ALTER TABLE [dbo].[PCR_Hdr] CHECK CONSTRAINT [FK_PCR_Hdr_Paymode_mst]
GO
/****** Object:  ForeignKey [FK_Process_Dtls_Process_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Process_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Process_Dtls_Process_Hdr] FOREIGN KEY([PD_PHID])
REFERENCES [dbo].[Process_Hdr] ([PH_ID])
GO
ALTER TABLE [dbo].[Process_Dtls] CHECK CONSTRAINT [FK_Process_Dtls_Process_Hdr]
GO
/****** Object:  ForeignKey [FK_Process_Dtls_User_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Process_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_Process_Dtls_User_Hdr] FOREIGN KEY([PD_UHID])
REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
ALTER TABLE [dbo].[Process_Dtls] CHECK CONSTRAINT [FK_Process_Dtls_User_Hdr]
GO
/****** Object:  ForeignKey [FK_Process_Hdr_Process_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Process_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_Process_Hdr_Process_Mst] FOREIGN KEY([PH_PRID])
REFERENCES [dbo].[Process_Mst] ([PR_ID])
GO
ALTER TABLE [dbo].[Process_Hdr] CHECK CONSTRAINT [FK_Process_Hdr_Process_Mst]
GO
/****** Object:  ForeignKey [FK_ProductDel_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[ProductDel_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductDel_Trn_MemJoining_Dtls] FOREIGN KEY([PT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[ProductDel_Trn] CHECK CONSTRAINT [FK_ProductDel_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_PurchaseOrder_Dtls_PurchaseOrder_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrder_Dtls_PurchaseOrder_Hdr] FOREIGN KEY([PD_POCode])
REFERENCES [dbo].[PurchaseOrder_Hdr] ([PH_POCode])
GO
ALTER TABLE [dbo].[PurchaseOrder_Dtls] CHECK CONSTRAINT [FK_PurchaseOrder_Dtls_PurchaseOrder_Hdr]
GO
/****** Object:  ForeignKey [FK_PurchaseOrder_Dtls_PV_MST]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_PurchaseOrder_Dtls_PV_MST] FOREIGN KEY([PD_PVCode])
REFERENCES [dbo].[PV_MST] ([PV_Code])
GO
ALTER TABLE [dbo].[PurchaseOrder_Dtls] CHECK CONSTRAINT [FK_PurchaseOrder_Dtls_PV_MST]
GO
/****** Object:  ForeignKey [FK_PurchaseOrder_Hdr_Log_PurchaseOrder_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Hdr_Log]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrder_Hdr_Log_PurchaseOrder_Hdr] FOREIGN KEY([PHL_POCode])
REFERENCES [dbo].[PurchaseOrder_Hdr] ([PH_POCode])
GO
ALTER TABLE [dbo].[PurchaseOrder_Hdr_Log] CHECK CONSTRAINT [FK_PurchaseOrder_Hdr_Log_PurchaseOrder_Hdr]
GO
/****** Object:  ForeignKey [FK_PurchaseOrder_Hdr_Log_User_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PurchaseOrder_Hdr_Log]  WITH NOCHECK ADD  CONSTRAINT [FK_PurchaseOrder_Hdr_Log_User_Hdr] FOREIGN KEY([PHL_AdminID])
REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
ALTER TABLE [dbo].[PurchaseOrder_Hdr_Log] CHECK CONSTRAINT [FK_PurchaseOrder_Hdr_Log_User_Hdr]
GO
/****** Object:  ForeignKey [FK_PV_MST_Category_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PV_MST]  WITH CHECK ADD  CONSTRAINT [FK_PV_MST_Category_Mst] FOREIGN KEY([PV_CategoryID])
REFERENCES [dbo].[Category_Mst] ([CM_ID])
GO
ALTER TABLE [dbo].[PV_MST] CHECK CONSTRAINT [FK_PV_MST_Category_Mst]
GO
/****** Object:  ForeignKey [FK_PvType_Dtls_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PvType_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_PvType_Dtls_PlanType_Mst] FOREIGN KEY([PTD_Type])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[PvType_Dtls] CHECK CONSTRAINT [FK_PvType_Dtls_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_PvType_Dtls_PvType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PvType_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_PvType_Dtls_PvType_Mst] FOREIGN KEY([PTD_PTMID])
REFERENCES [dbo].[PvType_Mst] ([PTM_ID])
GO
ALTER TABLE [dbo].[PvType_Dtls] CHECK CONSTRAINT [FK_PvType_Dtls_PvType_Mst]
GO
/****** Object:  ForeignKey [FK_PvType_Mst_PlanType_Mst]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[PvType_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_PvType_Mst_PlanType_Mst] FOREIGN KEY([PTD_Type])
REFERENCES [dbo].[PlanType_Mst] ([PM_ID])
GO
ALTER TABLE [dbo].[PvType_Dtls] CHECK CONSTRAINT [FK_PvType_Mst_PlanType_Mst]
GO
/****** Object:  ForeignKey [FK_Repurchase_Dtls_Repurchase_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Repurchase_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_Repurchase_Dtls_Repurchase_Hdr] FOREIGN KEY([RD_RHID])
REFERENCES [dbo].[Repurchase_Hdr] ([RH_ID])
GO
ALTER TABLE [dbo].[Repurchase_Dtls] CHECK CONSTRAINT [FK_Repurchase_Dtls_Repurchase_Hdr]
GO
/****** Object:  ForeignKey [FK_Repurchase_Hdr_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Repurchase_Hdr]  WITH NOCHECK ADD  CONSTRAINT [FK_Repurchase_Hdr_MemJoining_Dtls] FOREIGN KEY([RH_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Repurchase_Hdr] CHECK CONSTRAINT [FK_Repurchase_Hdr_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Repurchase_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Repurchase_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Repurchase_Trn_MemJoining_Dtls] FOREIGN KEY([RT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Repurchase_Trn] CHECK CONSTRAINT [FK_Repurchase_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_RepurchaseSlab_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[RepurchaseSlab_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_RepurchaseSlab_Trn_MemJoining_Dtls] FOREIGN KEY([RST_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[RepurchaseSlab_Trn] CHECK CONSTRAINT [FK_RepurchaseSlab_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Role_Dtls_Role_Hdr]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Role_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_Role_Dtls_Role_Hdr] FOREIGN KEY([RD_RHID])
REFERENCES [dbo].[Role_Hdr] ([RH_ID])
GO
ALTER TABLE [dbo].[Role_Dtls] CHECK CONSTRAINT [FK_Role_Dtls_Role_Hdr]
GO
/****** Object:  ForeignKey [FK_Royalty_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:03 ******/
ALTER TABLE [dbo].[Royalty_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Royalty_Trn_MemJoining_Dtls] FOREIGN KEY([Roy_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Royalty_Trn] CHECK CONSTRAINT [FK_Royalty_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_RoyInc_Trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[RoyInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_RoyInc_Trn_DateCode_Trn] FOREIGN KEY([RI_DateCode], [RI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[RoyInc_Trn] CHECK CONSTRAINT [FK_RoyInc_Trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_RoyInc_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[RoyInc_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_RoyInc_Trn_MemJoining_Dtls] FOREIGN KEY([RI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[RoyInc_Trn] CHECK CONSTRAINT [FK_RoyInc_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_RPLevelInc_trn_DateCode_Trn]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[RPLevelInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_RPLevelInc_trn_DateCode_Trn] FOREIGN KEY([RLI_Datecode], [RLI_Type])
REFERENCES [dbo].[DateCode_Trn] ([DCT_DtCode], [DCT_Type])
GO
ALTER TABLE [dbo].[RPLevelInc_trn] CHECK CONSTRAINT [FK_RPLevelInc_trn_DateCode_Trn]
GO
/****** Object:  ForeignKey [FK_RPLevelInc_trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[RPLevelInc_trn]  WITH NOCHECK ADD  CONSTRAINT [FK_RPLevelInc_trn_MemJoining_Dtls] FOREIGN KEY([RLI_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[RPLevelInc_trn] CHECK CONSTRAINT [FK_RPLevelInc_trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_SimulationAmt_SimulationAmt]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[SimulationAmt_Trn]  WITH CHECK ADD  CONSTRAINT [FK_SimulationAmt_SimulationAmt] FOREIGN KEY([Sim_Datecode])
REFERENCES [dbo].[SimulationAmt_Trn] ([Sim_Datecode])
GO
ALTER TABLE [dbo].[SimulationAmt_Trn] CHECK CONSTRAINT [FK_SimulationAmt_SimulationAmt]
GO
/****** Object:  ForeignKey [FK_SMS_Text_Mst_SMS_Text_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[SMS_Text_Mst]  WITH CHECK ADD  CONSTRAINT [FK_SMS_Text_Mst_SMS_Text_Mst] FOREIGN KEY([SM_ID])
REFERENCES [dbo].[SMS_Text_Mst] ([SM_ID])
GO
ALTER TABLE [dbo].[SMS_Text_Mst] CHECK CONSTRAINT [FK_SMS_Text_Mst_SMS_Text_Mst]
GO
/****** Object:  ForeignKey [FK_SMSBlacklist_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[SMSBlacklist_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_SMSBlacklist_Trn_MemJoining_Dtls] FOREIGN KEY([ST_MJDMemId])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[SMSBlacklist_Trn] CHECK CONSTRAINT [FK_SMSBlacklist_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_StockInByFR_Hdr_Franchise_mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockInByFR_Hdr_Franchise_mst] FOREIGN KEY([SIFH_ByFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockInByFR_Hdr] CHECK CONSTRAINT [FK_StockInByFR_Hdr_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_StockInByFR_Hdr_Franchise_mst1]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockInByFR_Hdr_Franchise_mst1] FOREIGN KEY([SIFH_FromFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockInByFR_Hdr] CHECK CONSTRAINT [FK_StockInByFR_Hdr_Franchise_mst1]
GO
/****** Object:  ForeignKey [FK_StockInByFR_Hdr_StockOut_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByFR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockInByFR_Hdr_StockOut_Hdr] FOREIGN KEY([SIFH_SOHCode])
REFERENCES [dbo].[StockOut_Hdr] ([SOH_Code])
GO
ALTER TABLE [dbo].[StockInByFR_Hdr] CHECK CONSTRAINT [FK_StockInByFR_Hdr_StockOut_Hdr]
GO
/****** Object:  ForeignKey [FK_StockInByHO_Hdr_PurchaseOrder_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByHO_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockInByHO_Hdr_PurchaseOrder_Hdr] FOREIGN KEY([SIH_PO])
REFERENCES [dbo].[PurchaseOrder_Hdr] ([PH_POCode])
GO
ALTER TABLE [dbo].[StockInByHO_Hdr] CHECK CONSTRAINT [FK_StockInByHO_Hdr_PurchaseOrder_Hdr]
GO
/****** Object:  ForeignKey [FK_StockInByHO_Hdr_Supplier_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInByHO_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockInByHO_Hdr_Supplier_Mst] FOREIGN KEY([SIH_SMID])
REFERENCES [dbo].[Supplier_Mst] ([SM_ID])
GO
ALTER TABLE [dbo].[StockInByHO_Hdr] CHECK CONSTRAINT [FK_StockInByHO_Hdr_Supplier_Mst]
GO
/****** Object:  ForeignKey [FK_StockInventory_Dtls_Franchise_mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInventory_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_StockInventory_Dtls_Franchise_mst] FOREIGN KEY([SID_FrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockInventory_Dtls] CHECK CONSTRAINT [FK_StockInventory_Dtls_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_StockInventory_Dtls_PV_MST]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockInventory_Dtls]  WITH CHECK ADD  CONSTRAINT [FK_StockInventory_Dtls_PV_MST] FOREIGN KEY([SID_PVCode])
REFERENCES [dbo].[PV_MST] ([PV_Code])
GO
ALTER TABLE [dbo].[StockInventory_Dtls] CHECK CONSTRAINT [FK_StockInventory_Dtls_PV_MST]
GO
/****** Object:  ForeignKey [FK_StockOut_Hdr_Franchise_mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockOut_Hdr_Franchise_mst] FOREIGN KEY([SOH_ByFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockOut_Hdr] CHECK CONSTRAINT [FK_StockOut_Hdr_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_StockOut_Hdr_Franchise_mst1]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockOut_Hdr_Franchise_mst1] FOREIGN KEY([SOH_ToFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockOut_Hdr] CHECK CONSTRAINT [FK_StockOut_Hdr_Franchise_mst1]
GO
/****** Object:  ForeignKey [FK_StockOut_Hdr_PCR_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockOut_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockOut_Hdr_PCR_Hdr] FOREIGN KEY([SOH_PCRCode])
REFERENCES [dbo].[PCR_Hdr] ([PH_Code])
GO
ALTER TABLE [dbo].[StockOut_Hdr] CHECK CONSTRAINT [FK_StockOut_Hdr_PCR_Hdr]
GO
/****** Object:  ForeignKey [FK_StockReturnByFR_Hdr_Franchise_mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockReturnByFR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockReturnByFR_Hdr_Franchise_mst] FOREIGN KEY([SRFH_ByFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockReturnByFR_Hdr] CHECK CONSTRAINT [FK_StockReturnByFR_Hdr_Franchise_mst]
GO
/****** Object:  ForeignKey [FK_StockReturnByFR_Hdr_Franchise_mst1]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockReturnByFR_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockReturnByFR_Hdr_Franchise_mst1] FOREIGN KEY([SRFH_ToFrID])
REFERENCES [dbo].[Franchise_mst] ([FM_ID])
GO
ALTER TABLE [dbo].[StockReturnByFR_Hdr] CHECK CONSTRAINT [FK_StockReturnByFR_Hdr_Franchise_mst1]
GO
/****** Object:  ForeignKey [FK_StockReturnByHO_Hdr_Supplier_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[StockReturnByHO_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_StockReturnByHO_Hdr_Supplier_Mst] FOREIGN KEY([SRH_SupplierID])
REFERENCES [dbo].[Supplier_Mst] ([SM_ID])
GO
ALTER TABLE [dbo].[StockReturnByHO_Hdr] CHECK CONSTRAINT [FK_StockReturnByHO_Hdr_Supplier_Mst]
GO
/****** Object:  ForeignKey [FK_Tax_Mst_PV_MST]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[Tax_Mst]  WITH NOCHECK ADD  CONSTRAINT [FK_Tax_Mst_PV_MST] FOREIGN KEY([TM_PvCode])
REFERENCES [dbo].[PV_MST] ([PV_Code])
GO
ALTER TABLE [dbo].[Tax_Mst] CHECK CONSTRAINT [FK_Tax_Mst_PV_MST]
GO
/****** Object:  ForeignKey [FK_Tax_Mst_State_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[Tax_Mst]  WITH CHECK ADD  CONSTRAINT [FK_Tax_Mst_State_Mst] FOREIGN KEY([TM_StateID])
REFERENCES [dbo].[State_Mst] ([SM_ID])
GO
ALTER TABLE [dbo].[Tax_Mst] CHECK CONSTRAINT [FK_Tax_Mst_State_Mst]
GO
/****** Object:  ForeignKey [FK_TempBinary_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempBinary_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_TempBinary_Trn_MemJoining_Dtls] FOREIGN KEY([TB_Memid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[TempBinary_Trn] CHECK CONSTRAINT [FK_TempBinary_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_TempBVVoucher_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempBVVoucher]  WITH NOCHECK ADD  CONSTRAINT [FK_TempBVVoucher_MemJoining_Dtls] FOREIGN KEY([TBVMemid])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[TempBVVoucher] CHECK CONSTRAINT [FK_TempBVVoucher_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_TempDownline_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempDownline]  WITH CHECK ADD  CONSTRAINT [FK_TempDownline_MemJoining_Dtls] FOREIGN KEY([TD_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[TempDownline] CHECK CONSTRAINT [FK_TempDownline_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_TempMemLogin_Dtls_TempMemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TempMemLogin_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_TempMemLogin_Dtls_TempMemJoining_Dtls] FOREIGN KEY([MLD_MemID])
REFERENCES [dbo].[TempMemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[TempMemLogin_Dtls] CHECK CONSTRAINT [FK_TempMemLogin_Dtls_TempMemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Ticket_Trn_MemJoining_Dtls]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[Ticket_Trn]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Trn_MemJoining_Dtls] FOREIGN KEY([TT_MemID])
REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
ALTER TABLE [dbo].[Ticket_Trn] CHECK CONSTRAINT [FK_Ticket_Trn_MemJoining_Dtls]
GO
/****** Object:  ForeignKey [FK_Ticket_Trn_TicketCategory_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[Ticket_Trn]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Trn_TicketCategory_Mst] FOREIGN KEY([TT_CategoryID])
REFERENCES [dbo].[TicketCategory_Mst] ([TCM_ID])
GO
ALTER TABLE [dbo].[Ticket_Trn] CHECK CONSTRAINT [FK_Ticket_Trn_TicketCategory_Mst]
GO
/****** Object:  ForeignKey [FK_TicketReply_Trn_Ticket_Trn]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[TicketReply_Trn]  WITH CHECK ADD  CONSTRAINT [FK_TicketReply_Trn_Ticket_Trn] FOREIGN KEY([TRT_Code])
REFERENCES [dbo].[Ticket_Trn] ([TT_Code])
GO
ALTER TABLE [dbo].[TicketReply_Trn] CHECK CONSTRAINT [FK_TicketReply_Trn_Ticket_Trn]
GO
/****** Object:  ForeignKey [FK_User_Dtls_User_Hdr]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[User_Dtls]  WITH NOCHECK ADD  CONSTRAINT [FK_User_Dtls_User_Hdr] FOREIGN KEY([UD_UHID])
REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
ALTER TABLE [dbo].[User_Dtls] CHECK CONSTRAINT [FK_User_Dtls_User_Hdr]
GO
/****** Object:  ForeignKey [FK_User_Hdr_Themes_Mst]    Script Date: 09/16/2016 10:42:04 ******/
ALTER TABLE [dbo].[User_Hdr]  WITH CHECK ADD  CONSTRAINT [FK_User_Hdr_Themes_Mst] FOREIGN KEY([UH_TMID])
REFERENCES [dbo].[Themes_Mst] ([tm_ID])
GO
ALTER TABLE [dbo].[User_Hdr] CHECK CONSTRAINT [FK_User_Hdr_Themes_Mst]
GO
/****** Object:  Table [dbo].[TempEwallet]    Script Date: 09/14/2016 18:51:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempEwallet](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[memid] [int] NULL,
	[flag] [int] NULL,
	[amount] [numeric](18, 2) NULL,
 CONSTRAINT [PK_TempEwallet] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stage_Mst]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stage_Mst](
	[SM_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SM_SrNo] [numeric](18, 0) NULL,
	[SM_Name] [varchar](50) NULL,
	[SM_Left] [numeric](18, 0) NULL,
	[SM_Right] [numeric](18, 0) NULL,
	[SM_Income] [numeric](18, 2) NULL,
	[SM_decExtra1] [numeric](18, 2) NULL,
	[SM_Royper] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Autocalc_Trn](
	[AU_ID] [int] IDENTITY(1,1) NOT NULL,
	[AU_DtCode] [numeric](18, 0) NULL,
	[AU_Date] [datetime] NULL,
	[AU_DateTime] [datetime] NULL,
	[AU_Flag] [tinyint] NULL,
	[AU_confflag] [tinyint] NULL,
	[AU_Type] [numeric](18, 0) NULL,
	[AU_ShowFlag] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Autocalc_Trn] PRIMARY KEY CLUSTERED 
([AU_ID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AutoCalcMakePayment_Trn](
	[AuMP_Id] [int] IDENTITY(1,1) NOT NULL,
	[AuMP_DateCode] [numeric](18, 0) NULL,
	[AuMP_Date] [datetime] NULL,
	[AuMP_Datetime] [datetime] NULL,
	[AuMP_DCTID] [numeric](18, 0) NULL,
	[AuMP_Type] [numeric](18, 0) NULL,
	[AuMP_PPDCode] [numeric](18, 0) NULL,
	[AuM_PPDFlag] [tinyint] NULL,
	[AuMP_PaymodeId] [tinyint] NULL,
 CONSTRAINT [PK_AutoCalcMakePayment_Trn] PRIMARY KEY CLUSTERED 
(
	[AuMP_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BankConf_Trn](
	[BCT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BCT_Memid] [numeric](18, 0) NULL,
	[BCT_Type] [numeric](18, 0) NULL,
	[BCT_Datecode] [numeric](18, 0) NULL,
	[BCT_Amt] [numeric](18, 2) NULL,
	[BCT_Status] [numeric](18, 0) NULL,
	[BCT_TCode] [varchar](100) NULL,
	[BCT_Remark] [varchar](5000) NULL,
	[BCT_Date] [datetime] NULL,
	[BCT_DateTime] [datetime] NULL,
	[BCT_Admin] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BankConf_Trn] PRIMARY KEY CLUSTERED 
(
	[BCT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempUpgrade_Trn](
	[UT_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UT_PGCode] [varchar](15) NULL,
	[UT_MemID] [numeric](18, 0) NOT NULL,
	[UT_NewPvCode] [numeric](18, 0) NOT NULL,
	[UT_PrevPvCode] [numeric](18, 0) NOT NULL,
	[UT_NewType] [numeric](18, 0) NOT NULL,
	[UT_PrevType] [numeric](18, 0) NOT NULL,
	[UT_Date] [datetime] NOT NULL,
	[UT_Amount] [numeric](18, 2) NOT NULL,
	[UT_SponsorID] [numeric](18, 0) NOT NULL,
	[UT_Datecode] [numeric](18, 0) NOT NULL,
	[UT_IncAmt] [numeric](18, 2) NOT NULL,
	[UT_Flag] [tinyint] NOT NULL,
	[UT_Remark] [varchar](300) NULL,
	[UT_AdminID] [numeric](18, 2) NOT NULL,
	[UT_IPAddress] [varchar](50) NULL,
	[UT_Browser] [varchar](50) NULL,
	[UT_BrowserVersion] [varchar](50) NULL,
	[UT_decEXTRA1] [numeric](18, 2) NULL,
	[UT_decEXTRA2] [numeric](18, 2) NULL,
	[UT_chvEXTRA3] [varchar](200) NULL,
	[UT_chvEXTRA4] [varchar](200) NULL,
	[UT_ConfirmDate] [datetime] NULL,
	[UT_PvPts] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayUErrorCode_Mst](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PUCode] [varchar](15) NOT NULL,
	[PUName] [varchar](1000) NULL,
	[DecExtra1] [varchar](50) NULL,
	[DecExtra2] [varchar](50) NULL,
	[ChvExtra1] [varchar](50) NULL,
	[ChvExtra2] [varchar](50) NULL,
	[DateTime1] [datetime] NULL,
	[DateTime2] [datetime] NULL,
 CONSTRAINT [PK_PayUErrorCode_Mst] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlushScript_Mst]
    (
      [FSM_ID] [numeric](18, 0) IDENTITY(1, 1)
                                NOT NULL ,
      [FSM_SrNo] [numeric](18, 0) NOT NULL ,
      [FSM_Name] [varchar](300) NULL ,
      [FSM_Desc] [varchar](MAX) NULL ,
      [FSM_Flag] [numeric](18, 0) NULL
    )
ON  [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FlushScript_Trn]
    (
      [FST_ID] [numeric](18, 0) IDENTITY(1, 1)
                                NOT NULL ,
      [FST_SrNo] [numeric](18, 0) NOT NULL ,
      [FST_Remark] [varchar](MAX) NULL ,
      [FST_Flag] [numeric](18, 0) NULL ,
      [FST_CustomerId] [numeric](18, 0) NULL ,
      [FST_IPAddress] [varchar](300) NULL ,
      [FST_Browser] [varchar](300) NULL ,
      [FST_BrowserVersion] [varchar](300) NULL
    )
ON  [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentTermSetting]
    (
      [PTS_All] [varchar](50) NULL ,
      [PTS_MemberSide] [varchar](50) NULL ,
      [PTS_AdminSide] [varchar](50) NULL ,
      [PTS_AdminMsgFlag] [varchar](50) NULL,
	  [PTS_AdminMsg] [varchar](4000) NULL
    )
ON  [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TurnOver_Trn](
	[TO_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TO_Datecode] [numeric](18, 0) NOT NULL,
	[TO_Type] [numeric](18, 0) NOT NULL,
	[TO_IncType] [numeric](18, 0) NOT NULL,
	--[TO_TotBV] [numeric](18, 2) NULL,
	[TO_TotBusiness] [numeric](18, 2) NULL,	
	[TO_ToPer] [numeric](18, 2) NULL,
	[TO_Amt] [numeric](18, 2) NULL,	
	[TO_TotPoint] [numeric](18, 2) NULL,
	[TO_PointVal] [numeric](18, 2) NULL,
	[TO_decExtra1] [numeric](18, 0) NULL,
	[TO_decExtra2] [numeric](18, 0) NULL,
	[TO_decExtra3] [numeric](18, 2) NULL,
	[TO_decExtra4] [numeric](18, 3) NULL,
 CONSTRAINT [PK_TurnOver_Trn] PRIMARY KEY CLUSTERED 
(
	[TO_Datecode] ASC,
	[TO_Type] ASC,
	[TO_IncType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoCalSMSEmail_Log]
(
[AL_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[AL_Date] [datetime] NULL,
[AL_Datetime] [datetime] NULL,
[ML_Url] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AL_chvExtra1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AL_chvExtra2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AL_chvExtra3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AL_chvExtra4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AutoCalSMSEmail_Log] ADD CONSTRAINT [PK_AutoCalSMSEmail_Log] PRIMARY KEY CLUSTERED  ([AL_ID]) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlabQual_Trn]    Script Date: 2/6/2018 4:55:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlabQual_Trn](
	[SL_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SL_MemID] [numeric](18, 0) NULL,
	[SL_SrNo] [numeric](18, 0) NULL,
	[SL_Per] [numeric](18, 2) NULL,
	[SL_Datecode] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RepurchaseInc_Trn]    Script Date: 2/6/2018 4:57:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RepurchaseInc_Trn](
	[RI_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RI_Memid] [numeric](18, 0) NOT NULL,
	[RI_RefMemid] [numeric](18, 0) NOT NULL,
	[RI_Datecode] [numeric](18, 0) NOT NULL,
	[RI_Type] [numeric](18, 0) NOT NULL,
	[RI_Amount] [numeric](18, 2) NOT NULL,
	[RI_Per] [numeric](18, 2) NOT NULL,
	[RI_OwnPer] [numeric](18, 2) NOT NULL,
	[RI_Extra1] [numeric](18, 2) NULL,
	[RI_Extra2] [numeric](18, 2) NULL,
	[RI_Extra3] [numeric](18, 2) NULL,
	[RI_Extra4] [numeric](18, 2) NULL,
	[RI_Ownbv] [numeric](18, 2) NULL,
	[RI_Downbv] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SameLevelQual_Trn]    Script Date: 2/6/2018 4:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SameLevelQual_Trn](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SlqMemId] [int] NOT NULL,
	[SlqFromMemid] [int] NULL,
	[SlqLevel] [int] NOT NULL,
	[Slqpercentage] [numeric](18, 2) NOT NULL,
	[SqlDateCode] [numeric](18, 2) NOT NULL,
	[SlqAmount] [numeric](18, 2) NOT NULL,
	[SqlExtr1] [numeric](18, 2) NULL,
	[SqlExtr2] [numeric](18, 2) NULL,
	[SqlExtr3] [numeric](18, 2) NULL,
	[SqlExtrStr1] [varbinary](50) NULL,
	[SqlExtrStr2] [varbinary](50) NULL,
	[SqlExtrStr3] [varbinary](50) NULL,
 CONSTRAINT [PK_SameLevelQual_Trn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoyCondition_MST]    Script Date: 2/14/2018 11:13:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoyCondition_MST](
	[RCM_ID] [int] IDENTITY(1,1) NOT NULL,
	[RCM_QualOn] [varchar](10) NULL,
	[RCM_Bus_ConsidOn] [varchar](10) NULL,
	[RCM_Roy_Type] [varchar](10) NULL,
	[RCM_IsTotal] [numeric](18, 0) NULL,
	[RCM_Active_Roy_Criteria] [varchar](10) NULL,
	[RMC_Cappingflag] [varchar](10) NULL,
	[RCM_Qual_EligibleFor] [varchar](10) NULL,
	[RCM_Roy_ResAmt] [numeric](18, 0) NULL,
	[RCM_Roy_Inc_Start] [varchar](10) NULL,
	[RCM_PerOn_ComTO] [numeric](18, 0) NULL,
	[RCM_TO_BasedOn] [varchar](10) NULL,
	[RMC_IsPayotLmt] [varchar](10) NULL,
	[RMC_Capping] [varchar](10) NULL,
	[RMC_CreatedOnUTC] [datetime] NULL,
      CONSTRAINT [PK_RoyCondition_MST] PRIMARY KEY CLUSTERED
        ( [RCM_ID] ASC )
        WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
               IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
               ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
ON  [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FundBonusCommissionInc_trn]    Script Date: 03/14/2018 12:37:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FundBonusCommissionInc_trn](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FBC_MemId] [numeric](18, 0) NULL,
	[FBC_DateCode] [numeric](18, 0) NULL,
	[FBC_FundId] [int] NOT NULL,
	[FBC_Income] [numeric](18, 0) NULL,
	[FBC_decExtra1] [numeric](18, 0) NULL,
	[FBC_decExtra2] [numeric](18, 0) NULL,
	[FBC_decExtra3] [numeric](18, 0) NULL,
	[FBC_decExtra4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FundBonusCommissionInc_trn] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FundBonusCommissionInc_trn]  WITH CHECK ADD  CONSTRAINT [FK_FundBonusCommissionInc_trn_FundBonusCommissionInc_trn] FOREIGN KEY([Id])
REFERENCES [dbo].[FundBonusCommissionInc_trn] ([Id])
GO
ALTER TABLE [dbo].[FundBonusCommissionInc_trn] CHECK CONSTRAINT [FK_FundBonusCommissionInc_trn_FundBonusCommissionInc_trn]
GO
CREATE TABLE [dbo].[RankQual_Trn]
(
[RQ_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[RQ_MemID] [numeric] (18, 0) NOT NULL,
[RQ_SrNo] [numeric] (18, 0) NOT NULL,
[RQ_Date] [datetime] NULL,
[RQ_DateTime] [datetime] NULL,
[RQ_DownID] [numeric] (18, 0) NULL,
[RQ_Datecode] [numeric] (18, 0) NULL,
[RQ_TYpeID] [numeric] (18, 0) NULL,
[RQ_decExtra1] [numeric] (18, 2) NULL,
[RQ_decExtra2] [numeric] (18, 0) NULL,
[RQ_chvExtra3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RQ_chvExtra4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RankQual_Trn] ADD CONSTRAINT [PK_RankQual_Trn] PRIMARY KEY CLUSTERED  ([RQ_MemID], [RQ_SrNo]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
CREATE TABLE [dbo].[TempMemDownline_Dtls]
(
[MDD_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[MDD_MemID] [numeric] (18, 0) NOT NULL,
[MDD_PVCode] [numeric] (18, 0) NOT NULL,
[MDD_Level] [numeric] (18, 0) NOT NULL,
[MDD_PvType] [numeric] (18, 0) NOT NULL,
[MDD_Date] [datetime] NOT NULL,
[MDD_LeftCnt] [numeric] (18, 0) NOT NULL,
[MDD_RightCnt] [numeric] (18, 0) NOT NULL,
[MDD_ConfLeftCnt] [numeric] (18, 0) NOT NULL,
[MDD_ConfRightCnt] [numeric] (18, 0) NOT NULL,
[MDD_LeftBV] [numeric] (18, 2) NULL,
[MDD_RightBV] [numeric] (18, 2) NULL,
[MDD_ConfLeftBV] [numeric] (18, 2) NULL,
[MDD_ConfRightBV] [numeric] (18, 2) NULL,
[MDD_LeftPV] [numeric] (18, 2) NULL,
[MDD_RightPV] [numeric] (18, 2) NULL,
[MDD_ConfLeftPV] [numeric] (18, 2) NULL,
[MDD_ConfRightPV] [numeric] (18, 2) NULL,
[MDD_OwnBV] [numeric] (18, 2) NULL,
[MDD_ConfOwnBV] [numeric] (18, 2) NULL,
[MDD_OwnPV] [numeric] (18, 2) NULL,
[MDD_ConfOwnPV] [numeric] (18, 2) NULL,
[MDD_LeftID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_RightID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_decExtra1] [numeric] (18, 2) NULL,
[MDD_decExtra2] [numeric] (18, 2) NULL,
[MDD_decExtra3] [numeric] (18, 2) NULL,
[MDD_decExtra4] [numeric] (18, 2) NULL,
[MDD_RejectLeftOwnAP] [numeric] (18, 2) NULL,
[MDD_RejectRightOwnAP] [numeric] (18, 2) NULL,
[MDD_RejectLeftAP] [numeric] (18, 2) NULL,
[MDD_RejectRightAP] [numeric] (18, 2) NULL,
[MDD_RejectLeftOwnAU] [numeric] (18, 2) NULL,
[MDD_RejectRightOwnAU] [numeric] (18, 2) NULL,
[MDD_RejectLeftAU] [numeric] (18, 2) NULL,
[MDD_RejectRightAU] [numeric] (18, 2) NULL,
[MDD_decExtra5] [numeric] (18, 2) NOT NULL,
[MDD_decExtra6] [numeric] (18, 2) NOT NULL,
[MDD_decExtra7] [numeric] (18, 2) NULL,
[MDD_decExtra8] [numeric] (18, 2) NULL,
[MDD_chvExtra9] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_chvExtra10] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempMemDownline_Dtls] ADD CONSTRAINT [PK_TempMemDownline_Dtls] PRIMARY KEY CLUSTERED  ([MDD_MemID], [MDD_PVCode], [MDD_Level], [MDD_PvType], [MDD_Date], [MDD_decExtra5], [MDD_decExtra6]) ON [PRIMARY]
GO
CREATE TABLE [dbo].[IncomeWiseBinInc_Trn]
(
[IncBI_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[IncBI_DateCode] [numeric] (18, 0) NOT NULL,
[IncBI_MemID] [numeric] (18, 0) NOT NULL,
[IncBI_Type] [numeric] (18, 0) NOT NULL,
[IncBI_Income2NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income2NewRight] [numeric] (18, 2) NULL,
[IncBI_Income2LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income2RightBF] [numeric] (18, 2) NULL,
[IncBI_Income2LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income2RightCF] [numeric] (18, 2) NULL,
[IncBI_Income2DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income2LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income2LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income2LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income2CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income2FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income2TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income2ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income3NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income3NewRight] [numeric] (18, 2) NULL,
[IncBI_Income3LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income3RightBF] [numeric] (18, 2) NULL,
[IncBI_Income3LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income3RightCF] [numeric] (18, 2) NULL,
[IncBI_Income3DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income3LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income3LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income3LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income3CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income3FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income3TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income3ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income4NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income4NewRight] [numeric] (18, 2) NULL,
[IncBI_Income4LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income4RightBF] [numeric] (18, 2) NULL,
[IncBI_Income4LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income4RightCF] [numeric] (18, 2) NULL,
[IncBI_Income4DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income4LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income4LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income4LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income4CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income4FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income4TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income4ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income5NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income5NewRight] [numeric] (18, 2) NULL,
[IncBI_Income5LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income5RightBF] [numeric] (18, 2) NULL,
[IncBI_Income5LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income5RightCF] [numeric] (18, 2) NULL,
[IncBI_Income5DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income5LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income5LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income5LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income5CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income5FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income5TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income5ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income6NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income6NewRight] [numeric] (18, 2) NULL,
[IncBI_Income6LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income6RightBF] [numeric] (18, 2) NULL,
[IncBI_Income6LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income6RightCF] [numeric] (18, 2) NULL,
[IncBI_Income6DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income6LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income6LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income6LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income6CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income6FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income6TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income6ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income7NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income7NewRight] [numeric] (18, 2) NULL,
[IncBI_Income7LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income7RightBF] [numeric] (18, 2) NULL,
[IncBI_Income7LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income7RightCF] [numeric] (18, 2) NULL,
[IncBI_Income7DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income7LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income7LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income7LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income7CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income7FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income7TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income7ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income8NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income8NewRight] [numeric] (18, 2) NULL,
[IncBI_Income8LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income8RightBF] [numeric] (18, 2) NULL,
[IncBI_Income8LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income8RightCF] [numeric] (18, 2) NULL,
[IncBI_Income8DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income8LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income8LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income8LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income8CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income8FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income8TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income8ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income9NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income9NewRight] [numeric] (18, 2) NULL,
[IncBI_Income9LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income9RightBF] [numeric] (18, 2) NULL,
[IncBI_Income9LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income9RightCF] [numeric] (18, 2) NULL,
[IncBI_Income9DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income9LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income9LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income9LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income9CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income9FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income9TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income9ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income10NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income10NewRight] [numeric] (18, 2) NULL,
[IncBI_Income10LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income10RightBF] [numeric] (18, 2) NULL,
[IncBI_Income10LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income10RightCF] [numeric] (18, 2) NULL,
[IncBI_Income10DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income10LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income10LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income10LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income10CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income10FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income10TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income10ActualPairs] [numeric] (18, 2) NULL,
[IncBI_Income11NewLeft] [numeric] (18, 2) NULL,
[IncBI_Income11NewRight] [numeric] (18, 2) NULL,
[IncBI_Income11LeftBF] [numeric] (18, 2) NULL,
[IncBI_Income11RightBF] [numeric] (18, 2) NULL,
[IncBI_Income11LeftCF] [numeric] (18, 2) NULL,
[IncBI_Income11RightCF] [numeric] (18, 2) NULL,
[IncBI_Income11DiffAmt] [numeric] (18, 2) NULL,
[IncBI_Income11LapsePairs] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncBI_Income11LapsePairCnt] [numeric] (18, 2) NULL,
[IncBI_Income11LapsedAmt] [numeric] (18, 2) NULL,
[IncBI_Income11CappFlag] [numeric] (18, 2) NULL,
[IncBI_Income11FirstPair] [numeric] (18, 2) NULL,
[IncBI_Income11TotalPaidPairs] [numeric] (18, 2) NULL,
[IncBI_Income11ActualPairs] [numeric] (18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[IncomeWiseBinInc_Trn] ADD CONSTRAINT [PK_IncomeWiseBinInc_Trn] PRIMARY KEY CLUSTERED  ([IncBI_DateCode], [IncBI_MemID], [IncBI_Type]) ON [PRIMARY]
GO
CREATE TABLE [dbo].[IncomeWiseMemberFirstDtCode]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[MemId] [numeric] (18, 2) NULL,
[firstdtcode] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncomeType] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IncomeWiseMemberFirstDtCode] ADD CONSTRAINT [PK_IncomeWiseMemberFirstDtCode] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE TABLE [dbo].[IncomeWiseMemDownline_Dtls]
(
[IMDD_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[IMDD_MemID] [numeric] (18, 2) NOT NULL,
[IMDD_PVCode] [numeric] (18, 2) NOT NULL,
[IMDD_Level] [numeric] (18, 2) NOT NULL,
[IMDD_PvType] [numeric] (18, 2) NOT NULL,
[IMDD_Date] [datetime] NOT NULL,
[IMDD_DateCode] [int] NOT NULL,
[IMDD_Cycle] [int] NOT NULL,
[IMDD_LBinary2] [numeric] (18, 2) NULL,
[IMDD_RBinary2] [numeric] (18, 2) NULL,
[IMDD_LBinary3] [numeric] (18, 2) NULL,
[IMDD_RBinary3] [numeric] (18, 2) NULL,
[IMDD_LBinary4] [numeric] (18, 2) NULL,
[IMDD_RBinary4] [numeric] (18, 2) NULL,
[IMDD_LBinary5] [numeric] (18, 2) NULL,
[IMDD_RBinary5] [numeric] (18, 2) NULL,
[IMDD_LBinary6] [numeric] (18, 2) NULL,
[IMDD_RBinary6] [numeric] (18, 2) NULL,
[IMDD_LBinary7] [numeric] (18, 2) NULL,
[IMDD_RBinary7] [numeric] (18, 2) NULL,
[IMDD_LBinary8] [numeric] (18, 2) NULL,
[IMDD_RBinary8] [numeric] (18, 2) NULL,
[IMDD_LBinary9] [numeric] (18, 2) NULL,
[IMDD_RBinary9] [numeric] (18, 2) NULL,
[IMDD_LBinary10] [numeric] (18, 2) NULL,
[IMDD_RBinary10] [numeric] (18, 2) NULL,
[IMDD_LBinary11] [numeric] (18, 2) NULL,
[IMDD_RBinary11] [numeric] (18, 2) NULL,
[IMDD_decExtra1] [numeric] (18, 2) NULL,
[IMDD_decExtra2] [numeric] (18, 2) NULL,
[IMDD_decExtra3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMDD_decExtra4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IncomeWiseMemDownline_Dtls] ADD CONSTRAINT [PK_IncomeWiseMemDownline_Dtls] PRIMARY KEY CLUSTERED  ([IMDD_MemID], [IMDD_PVCode], [IMDD_Level], [IMDD_PvType], [IMDD_Date], [IMDD_DateCode], [IMDD_Cycle]) ON [PRIMARY]
GO
CREATE TABLE [dbo].[FirstBinaryMemDownline_Dtls]
(
[MDD_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[MDD_MemID] [numeric] (18, 0) NOT NULL,
[MDD_PVCode] [numeric] (18, 0) NOT NULL,
[MDD_Level] [numeric] (18, 0) NOT NULL,
[MDD_PvType] [numeric] (18, 0) NOT NULL,
[MDD_Date] [datetime] NOT NULL,
[MDD_LeftCnt] [numeric] (18, 0) NOT NULL,
[MDD_RightCnt] [numeric] (18, 0) NOT NULL,
[MDD_ConfLeftCnt] [numeric] (18, 0) NOT NULL,
[MDD_ConfRightCnt] [numeric] (18, 0) NOT NULL,
[MDD_LeftBV] [numeric] (18, 2) NULL,
[MDD_RightBV] [numeric] (18, 2) NULL,
[MDD_ConfLeftBV] [numeric] (18, 2) NULL,
[MDD_ConfRightBV] [numeric] (18, 2) NULL,
[MDD_LeftPV] [numeric] (18, 2) NULL,
[MDD_RightPV] [numeric] (18, 2) NULL,
[MDD_ConfLeftPV] [numeric] (18, 2) NULL,
[MDD_ConfRightPV] [numeric] (18, 2) NULL,
[MDD_LeftSale] [numeric] (18, 2) NULL,
[MDD_RightSale] [numeric] (18, 2) NULL,
[MDD_ConfLeftSale] [numeric] (18, 2) NULL,
[MDD_ConfRightSale] [numeric] (18, 2) NULL,
[MDD_OwnBV] [numeric] (18, 2) NULL CONSTRAINT [DF__binary_dt__MDD_ow__740F373E] DEFAULT ((0)),
[MDD_ConfOwnBV] [numeric] (18, 2) NULL CONSTRAINT [DF__binary_dt__MDD_Co__75037A77] DEFAULT ((0)),
[MDD_OwnPV] [numeric] (18, 2) NULL CONSTRAINT [DF_FirstBinaryMemDownline_Dtls_MDD_ownbv1] DEFAULT ((0)),
[MDD_ConfOwnPV] [numeric] (18, 2) NULL CONSTRAINT [DF_FirstBinaryMemDownline_Dtls_MDD_Confownbv1] DEFAULT ((0)),
[MDD_LeftID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_RightID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_decExtra1] [numeric] (18, 2) NOT NULL,
[MDD_decExtra2] [numeric] (18, 2) NULL,
[MDD_decExtra3] [numeric] (18, 2) NULL,
[MDD_decExtra4] [numeric] (18, 2) NULL,
[MDD_RejectLeftOwnAP] [numeric] (18, 2) NULL,
[MDD_RejectRightOwnAP] [numeric] (18, 2) NULL,
[MDD_RejectLeftAP] [numeric] (18, 2) NULL,
[MDD_RejectRightAP] [numeric] (18, 2) NULL,
[MDD_RejectLeftOwnAU] [numeric] (18, 2) NULL,
[MDD_RejectRightOwnAU] [numeric] (18, 2) NULL,
[MDD_RejectLeftAU] [numeric] (18, 2) NULL,
[MDD_RejectRightAU] [numeric] (18, 2) NULL,
[MDD_decExtra5] [numeric] (18, 2) NULL,
[MDD_decExtra6] [numeric] (18, 2) NULL,
[MDD_decExtra7] [numeric] (18, 2) NULL,
[MDD_decExtra8] [numeric] (18, 2) NULL,
[MDD_chvExtra9] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_chvExtra10] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MDD_OwnSale] [numeric] (18, 2) NULL,
[MDD_ConfOwnSale] [numeric] (18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirstBinaryMemDownline_Dtls] ADD CONSTRAINT [PK_FirstBinaryMemDownline_Dtls] PRIMARY KEY CLUSTERED  ([MDD_MemID], [MDD_PVCode], [MDD_Level], [MDD_PvType], [MDD_Date]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FirstBinaryMemDownline_Dtls] WITH NOCHECK ADD CONSTRAINT [FK_FirstBinaryMemDownline_Dtls_MemJoining_Dtls] FOREIGN KEY ([MDD_MemID]) REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
CREATE TABLE [dbo].[BinSet_Dtls]
(
[BI_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[BI_MemID] [numeric] (18, 0) NULL,
[BI_BinaryIncome1] [numeric] (18, 0) NULL,
[BI_BinaryIncome2] [numeric] (18, 0) NULL,
[BI_BinaryIncome3] [numeric] (18, 0) NULL,
[BI_BinaryIncome4] [numeric] (18, 0) NULL,
[BI_BinaryIncome5] [numeric] (18, 0) NULL,
[BI_BinaryIncome6] [numeric] (18, 0) NULL,
[BI_BinaryIncome7] [numeric] (18, 0) NULL,
[BI_BinaryIncome8] [numeric] (18, 0) NULL,
[BI_BinaryIncome9] [numeric] (18, 0) NULL,
[BI_BinaryIncome10] [numeric] (18, 0) NULL,
[BI_BinaryIncome11] [numeric] (18, 0) NULL,
[BI_Decextra1] [numeric] (18, 0) NULL,
[BI_Decextra2] [numeric] (18, 0) NULL,
[BI_Decextra3] [numeric] (18, 0) NULL,
[BI_Decextra4] [numeric] (18, 0) NULL
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[FundIncomeMemList]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[memid] [numeric] (18, 0) NULL,
[fundid] [int] NULL,
[income] [numeric] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FundIncomeMemList] ADD CONSTRAINT [PK_FundIncomeMemList] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE TYPE [dbo].[bulkImportDataTable] AS TABLE(
	[RowId] [int] NULL,
	[MJD_MemNo] [varchar](50) NULL,
	[MJD_Introducer] [varchar](50) NULL,
	[MJD_AdjustedTo] [varchar](50) NULL,
	[MJD_LR] [varchar](50) NULL,
	[MJD_PvCode] [numeric](18, 0) NULL,
	[MJD_PvType] [numeric](18, 0) NULL,
	[MJD_bv] [numeric](18, 2) NULL,
	[MJD_Pv] [numeric](18, 2) NULL,
	[MJD_pkgbv] [numeric](18, 2) NULL,
	[MJD_pkgPv] [numeric](18, 2) NULL,
	[MJD_Paymode] [numeric](18, 0) NULL,
	[MJD_PayType] [varchar](50) NULL,
	[MJD_Multiplecnt] [numeric](18, 0) NULL,
	[MJD_MasterID] [numeric](18, 0) NULL,
	[MJD_DTOJ] [varchar](50) NULL,
	[MJD_DOJ] [varchar](50) NULL,
	[MJD_RoyIntroducer] [numeric](18, 0) NULL,
	[MJD_RoyAdjustedTo] [numeric](18, 0) NULL,
	[MJD_CCTrnAmt] [numeric](18, 2) NULL,
	[MJD_TrnTotalAmt] [numeric](18, 2) NULL,
	[MPD_DDBank] [varchar](100) NULL,
	[MPD_DDBranch] [varchar](100) NULL,
	[MPD_DDNo] [numeric](18, 0) NULL,
	[MPD_DDDate] [varchar](50) NULL,
	[MJD_Amount] [numeric](18, 2) NULL,
	[MPD_Title] [varchar](10) NULL,
	[MPD_Name] [varchar](200) NULL,
	[MPD_Mobile] [varchar](20) NULL,
	[MPD_Email] [varchar](50) NULL,
	[MPD_PanCardNo] [varchar](50) NULL,
	[MPD_ApplyPan] [varchar](50) NULL,
	[MPD_ChqPayTo] [varchar](100) NULL,
	[MPD_Bank] [varchar](30) NULL,
	[MPD_Branch] [varchar](50) NULL,
	[MPD_BankAddress] [varchar](400) NULL,
	[MPD_AccType] [varchar](16) NULL,
	[MPD_AccNo] [varchar](16) NULL,
	[MPD_AccPwd] [varchar](16) NULL,
	[MPD_IFSDCode] [varchar](20) NULL,
	[MPD_FirmType] [varchar](100) NULL,
	[MPD_FirmDetail] [varchar](100) NULL,
	[MPD_NoDepend] [numeric](18, 0) NULL,
	[MPD_ShpPinCode] [varchar](100) NULL,
	[MPD_PinSrNo] [varchar](50) NULL,
	[MPD_ReceiptNo] [varchar](50) NULL,
	[MLD_Login] [varchar](50) NULL,
	[MLD_pwd] [varchar](50) NULL,
	[MPD_Gender] [varchar](50) NULL,
	[MPD_IPAddress] [varchar](50) NULL,
	[MPD_Browser] [varchar](50) NULL,
	[MPD_BrowserVersion] [varchar](50) NULL,
	[OH_FRID] [numeric](18, 0) NULL,
	[OH_DispatchFrID] [numeric](18, 0) NULL,
	[OH_DispatchBy] [numeric](18, 0) NULL,
	[MPD_Extra17] [varchar](100) NULL,
	[MPD_Extra18] [varchar](100) NULL,
	[MPD_PayReceiptMode] [numeric](18, 0) NULL,
	[MPD_EWTransfer] [decimal](18, 0) NULL,
	[CurrDate] [datetime] NULL,
	[CurrDateTime] [datetime] NULL,
	[MPD_FirstName] [varchar](100) NULL,
	[MPD_MiddleName] [varchar](100) NULL,
	[MPD_LastName] [varchar](100) NULL,
	[MPD_nomName] [varchar](100) NULL,
	[MPD_nomRel] [varchar](100) NULL,
	[MPD_nomBdate] [datetime] NULL,
	[MPD_DOB] [varchar](50) NULL,
	[MPD_Address] [varchar](100) NULL,
	[MPD_City] [varchar](100) NULL,
	[MPD_Country] [varchar](100) NULL,
	[MPD_State] [varchar](100) NULL,
	[MPD_District] [varchar](100) NULL,
	[MPD_PinCode] [varchar](100) NULL,
	[MLD_HQuestion] [varchar](100) NULL,
	[MLD_HAnswer] [varchar](100) NULL,
	[MPD_resphone] [varchar](100) NULL,
	[MPD_EWAccNo] [varchar](100) NULL,
	[MPD_Extra21] [varchar](50) NULL,
	[customerId] [int] NULL,
	[ddlProduct] [varchar](50) NULL
)
GO
/****** Object:  Table [dbo].[CreditNoteWithdrawl_trn]    Script Date: 07/31/2018 16:21:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditNoteWithdrawl_trn](
	[wt_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[wt_memid] [numeric](18, 0) NOT NULL,
	[wt_Date] [datetime] NOT NULL,
	[wt_Datetime] [datetime] NOT NULL,
	[wt_Amt] [numeric](18, 2) NOT NULL,
	[wt_AdminAmt] [numeric](18, 2) NOT NULL,
	[wt_Flag] [tinyint] NOT NULL,
	[wt_Remark] [varchar](8000) NULL,
	[wt_Extra1] [varchar](100) NULL,
	[wt_Extra2] [varchar](100) NULL,
	[wt_Extra3] [varchar](100) NULL,
	[wt_Extra4] [datetime] NULL,
	[wt_datecode] [datetime] NULL,
	[wt_Type] [numeric](18, 0) NOT NULL,
	[wt_IPaddress] [varchar](200) NULL,
	[wt_browerversion] [varchar](200) NULL,
	[wt_browser] [varchar](200) NULL,
	[wt_paytype] [numeric](18, 0) NULL,
	[wt_CustomerId] [int] NULL,
 CONSTRAINT [PK_CreditNoteWithdrawl_trn] PRIMARY KEY CLUSTERED 
(
	[wt_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CreditNoteEwallet_Trn]    Script Date: 07/31/2018 16:21:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditNoteEwallet_Trn](
	[ET_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ET_Memid] [numeric](18, 0) NOT NULL,
	[ET_Date] [datetime] NOT NULL,
	[ET_Amt] [numeric](18, 2) NOT NULL,
	[ET_Flag] [tinyint] NOT NULL,
	[ET_Remark] [varchar](8000) NULL,
	[ET_Webflag] [tinyint] NOT NULL,
	[ET_chvExtra1] [varchar](100) NULL,
	[ET_chvExtra2] [varchar](100) NULL,
	[ET_chvExtra3] [varchar](100) NULL,
	[ET_chvExtra4] [varchar](100) NULL,
	[et_RemarkA] [varchar](8000) NULL,
	[ET_Amt1] [numeric](18, 2) NOT NULL,
	[ET_Webstatusid] [numeric](18, 2) NOT NULL,
	[ET_Flg] [numeric](18, 2) NOT NULL,
	[ET_Datecode] [numeric](18, 0) NOT NULL,
	[ET_Type] [numeric](18, 0) NOT NULL,
	[ET_Trnflag] [tinyint] NOT NULL,
	[ET_Datetime] [datetime] NULL,
	[ET_PPHCode] [numeric](18, 0) NULL,
	[ET_Balance] [numeric](18, 2) NULL,
	[ET_CustomerId] [int] NULL,
	[ET_PGCode] [varchar](100) NULL,
 CONSTRAINT [PK_CreditNoteEwallet_Trn] PRIMARY KEY CLUSTERED 
(
	[ET_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_CreditNoteEwallet_Trn_ET_datecode]    Script Date: 07/31/2018 16:21:45 ******/
ALTER TABLE [dbo].[CreditNoteEwallet_Trn] ADD  CONSTRAINT [DF_CreditNoteEwallet_Trn_ET_datecode]  DEFAULT ((0)) FOR [ET_Datecode]
GO
/****** Object:  Default [DF_CreditNoteEwallet_Trn_ET_Type]    Script Date: 07/31/2018 16:21:45 ******/
ALTER TABLE [dbo].[CreditNoteEwallet_Trn] ADD  CONSTRAINT [DF_CreditNoteEwallet_Trn_ET_Type]  DEFAULT ((0)) FOR [ET_Type]
GO
/****** Object:  Default [DF_CreditNoteEwallet_Trn_ET_Trnflag]    Script Date: 07/31/2018 16:21:45 ******/
ALTER TABLE [dbo].[CreditNoteEwallet_Trn] ADD  CONSTRAINT [DF_CreditNoteEwallet_Trn_ET_Trnflag]  DEFAULT ((0)) FOR [ET_Trnflag]
GO
/****** Object:  Trigger [dbo].[trg_CreditNoteEwalletBalance]    Script Date: 07/31/2018 14:57:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_CreditNoteEwalletBalance] ON [dbo].[CreditNoteEwallet_Trn]
    FOR INSERT
AS
    DECLARE @dblMJD_MemID NUMERIC(18, 0),
        @ET_ID NUMERIC(18, 0),
        @ET_Type NUMERIC(18, 0),
        @Balance NUMERIC(18, 2),
        @TotCRamt NUMERIC(18, 2),
        @TotDRamt NUMERIC(18, 2)  
    BEGIN  
        SELECT  @ET_ID = ET_ID, @dblMJD_MemID = ET_CustomerId, @ET_Type = ET_Type
        FROM    Inserted  
  
        SELECT  @Balance = 0  
  
        SELECT  @TotCRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    CreditNoteEwallet_Trn
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 0  
  
        SELECT  @TotDRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    CreditNoteEwallet_Trn
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 1  
  
        SELECT  @Balance = @TotCRamt - @TotDRamt  
  
        UPDATE  CreditNoteEwallet_Trn
        SET     ET_Balance = @Balance
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_Type = @ET_Type
                AND ET_ID = @ET_ID   
    END
GO