DECLARE @PvType INT,
        @PvCode INT,
        @PvName VARCHAR(100),
        @PvCost NUMERIC(18, 0),
        @PvTypeName VARCHAR(100),
        @PaymodeID INT,
        @PayReceiptMode INT,
        @PayReceiptName VARCHAR(100),
        @PayName VARCHAR(50),
        @PayType VARCHAR(10),
        @DOJ VARCHAR(50),
        @DTOJ VARCHAR(50),
        @DOB VARCHAR(50),
        @MemberID VARCHAR(50),
        @MemName VARCHAR(100),
        @errMsg VARCHAR(MAX);

SELECT @errMsg = '',
       @PayType = 0,
       @PvCost = 0,
       @PayReceiptMode = 0;

-- ***** SET THE ID & NAME OF THE 1ST MEMBER ID *****
SELECT @MemberID = '10000001',
       @MemName = 'Company Id';

-- ***** SET THE PAYMODE OF THE 1ST MEMBER ID *****
SET @PaymodeID = 7;

-- ***** SET THE PACKAGE OF THE 1ST MEMBER ID *****
SET @PvCode = 1;

-- ***** SET DATE OF JOINING OF THE 1ST MEMBER ID *****
SET @DOJ = '01/01/2010';
-- SET @DOJ = CONVERT(VARCHAR(10), GETDATE(), 103)

-- ***** SET BIRTH-DATE OF THE 1ST MEMBER ID *****
SET @DOB = @DOJ;
-- SET @DOB = '01/01/2014'

SET @DTOJ = @DOJ;

SELECT @PayName = PM_Name,
       @PayType = PM_Desc
FROM Paymode_mst (NOLOCK)
WHERE PM_ID = @PaymodeID;

SELECT @PayReceiptMode = PM_ID,
       @PayReceiptName = PM_Name
FROM Paymode_mst (NOLOCK)
WHERE PM_DefaultPaymentFlg = 1;

SELECT @PvName = PV_Name,
       @PvCost = PV_SalePrice,
       @PvType = PV_Type
FROM PV_MST (NOLOCK)
WHERE PV_Code = @PvCode;

SELECT @PvTypeName = PTM_Desc
FROM PvType_Mst (NOLOCK)
WHERE PTM_ID = @PvType;

BEGIN TRY
    IF @MemberID = ''
        RAISERROR('Member ID NOT set.', 16, 1);

    IF @MemName = ''
        RAISERROR('Member Name NOT set.', 16, 1);

    IF @PayReceiptMode = 0
        RAISERROR('Default Pay Receipt Mode NOT set.', 16, 1);

    --IF @PvCost = 0 
    --    RAISERROR ('Package NOT created.', 16, 1);



    PRINT 'Member ID ----------------------> ' + ISNULL(@MemberID, '');
    PRINT 'Member Name --------------------> ' + ISNULL(@MemName, '');
    PRINT 'Date of Birth ------------------> ' + CAST(@DOB AS VARCHAR);
    PRINT 'Date of Joining ----------------> ' + CAST(@DOJ AS VARCHAR);
    PRINT 'DOJ with Time ------------------> ' + CAST(@DTOJ AS VARCHAR);
    PRINT 'Paymode ID ---------------------> ' + CAST(@PaymodeID AS VARCHAR) + ' (' + @PayName + ' - ' + @PayType + ')';
    PRINT 'Default Payment Receipt Mode ---> ' + CAST(ISNULL(@PayReceiptMode, 0) AS VARCHAR) + ' (' + @PayReceiptName
          + ')';
    PRINT 'Package Code -------------------> ' + CAST(@PvCode AS VARCHAR) + ' (' + @PvName + ')';
    PRINT 'Package Type -------------------> ' + CAST(@PvType AS VARCHAR) + ' (' + @PvTypeName + ')';
    PRINT 'Package Cost -------------------> ' + CAST(@PvCost AS VARCHAR) + '/-';

    BEGIN TRANSACTION;

    IF @PvCode > 0
       AND @PvType > 0
       --AND @PvCost > 0
       AND @PaymodeID > 0
       AND @PayType <> ''
       AND @DOJ <> ''
       AND @DTOJ <> ''
       AND @DOB <> ''
       AND @MemberID <> ''
       AND @MemName <> ''
    BEGIN
        SET IDENTITY_INSERT [MemJoining_Dtls] ON;
        INSERT INTO [MemJoining_Dtls]
        (
            [MJD_MemID],
            [MJD_MemNo],
            [MJD_DOJ],
            [MJD_DTOJ],
            [MJD_JoinFr],
            [MJD_PvCode],
            [MJD_PvType],
            [MJD_Paymode],
            [MJD_PayType],
            [MJD_Amount],
            [MJD_Multiplecnt],
            [MJD_MasterID],
            [MJD_Introducer],
            [MJD_AdjustedTo],
            [MJD_LR],
            [MJD_RoyIntroducer],
            [MJD_RoyAdjustedTo],
            [MJD_RoyLR]
        )
        VALUES
        (1, @MemberID, CONVERT(DATETIME, @DOJ, 103), CONVERT(DATETIME, @DTOJ, 103), NULL, @PvCode, @PvType, @PaymodeID,
         @PayType, @PvCost, 1, 1, 0, 0, 'L', NULL, NULL, NULL);
        SET IDENTITY_INSERT [MemJoining_Dtls] OFF;

        -- Add 1 rows to [MemLogin_Dtls]
        SET IDENTITY_INSERT [MemLogin_Dtls] ON;
        INSERT INTO [MemLogin_Dtls]
        (
            [MLD_ID],
            [MLD_MemID],
            [MLD_Login],
            [MLD_pwd],
            [MLD_Theme],
            [MLD_CurrentLogin],
            [MLD_LastLogin],
            [MLD_HQuestion],
            [MLD_HAnswer],
            [MLD_decEXTRA1],
            [MLD_decEXTRA2],
            [MLD_chvEXTRA3],
            [MLD_chvEXTRA4]
        )
        VALUES
        (1, 1, @MemberID, 'vx2FvkWijsZ83gomku12tg==', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
        SET IDENTITY_INSERT [MemLogin_Dtls] OFF;

        -- Add 1 rows to [MemProfile_Dtls]
        SET IDENTITY_INSERT [MemProfile_Dtls] ON;
        INSERT INTO [MemProfile_Dtls]
        (
            [MPD_Id],
            [MPD_MemId],
            [MPD_Name],
            [MPD_DOB],
            [MPD_Title],
            [MPD_Gender],
            [MPD_MaritalStatus],
            [MPD_Mother],
            [MPD_Father],
            [MPD_HoWo],
            [MPD_PassportNo],
            [MPD_IFSDCode],
            [MPD_Qualification],
            [MPD_Profession],
            [MPD_Designation],
            [MPD_Income],
            [MPD_SalStatus],
            [MPD_NoDepend],
            [MPD_TwoWheeler],
            [MPD_FourWheeler],
            [MPD_Age],
            [MPD_Nationality],
            [MPD_BloodGroup],
            [MPD_ResStatus],
            [MPD_PanCardNo],
            [MPD_ApplyPan],
            [MPD_ChqPayTo],
            [MPD_Address],
            [MPD_LandMark],
            [MPD_City],
            [MPD_District],
            [MPD_State],
            [MPD_Country],
            [MPD_PinCode],
            [MPD_Mobile],
            [MPD_ResPhone],
            [MPD_OffPhone],
            [MPD_Email],
            [MPD_Fax],
            [MPD_ShpAddress],
            [MPD_ShpLandMark],
            [MPD_ShpCity],
            [MPD_ShpDistrict],
            [MPD_ShpState],
            [MPD_ShpCountry],
            [MPD_ShpPinCode],
            [MPD_ShpContPerson],
            [MPD_ShpContNo],
            [MPD_Bank],
            [MPD_Branch],
            [MPD_BankAddress],
            [MPD_AccType],
            [MPD_AccNo],
            [MPD_AccPwd],
            [MPD_nomTitle],
            [MPD_nomName],
            [MPD_nomGender],
            [MPD_nomRel],
            [MPD_nomBdate],
            [MPD_NomAddr],
            [MPD_PinSrNo],
            [MPD_DDNo],
            [MPD_DDDate],
            [MPD_DDBank],
            [MPD_DDBranch],
            [MPD_DDBankAddr],
            [MPD_EpinPwd],
            [MPD_EWSpon],
            [MPD_EWAccNo],
            [MPD_EWTransfer],
            [MPD_FirmType],
            [MPD_FirmDetail],
            [MPD_ReceiptNo],
            [MPD_EpinCurrentLogin],
            [MPD_EpinLastLogin],
            [MPD_Extra1],
            [MPD_Extra2],
            [MPD_Extra3],
            [MPD_Extra4],
            [MPD_Extra5],
            [MPD_Extra6],
            [MPD_Extra7],
            [MPD_Extra8],
            [MPD_Extra9],
            [MPD_Extra10],
            [MPD_Extra11],
            [MPD_Extra12],
            [MPD_Extra13],
            [MPD_Extra14],
            [MPD_Extra15],
            [MPD_Extra16],
            [MPD_Extra17],
            [MPD_Extra18],
            [MPD_Extra19],
            [MPD_Extra20],
            [MPD_Extra21],
            [MPD_Extra22],
            [MPD_Extra23],
            [MPD_Extra24],
            [MPD_Extra25],
            [MPD_Extra26],
            [MPD_Extra27],
            [MPD_Extra28],
            [MPD_Extra29],
            [MPD_Extra30],
            [MPD_ImagePath],
            [MPD_PayReceiptMode]
        )
        VALUES
        (1, 1, @MemName, CONVERT(DATETIME, @DOB, 103), 'M/S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
         NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '', 'True', NULL, NULL, NULL, NULL, NULL, NULL,
         'India', NULL, NULL, NULL, NULL, 'admin@yourstore.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
         NULL, NULL, NULL, NULL, NULL, 0, 'Prop', 'Owner-Name', '10000001', NULL, NULL, NULL, NULL,
         'What is your past-time?', 'MLM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
         NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @PayReceiptMode);
        SET IDENTITY_INSERT [MemProfile_Dtls] OFF;

        --SET IDENTITY_INSERT [PancardConf_Dtls] ON		  
        --INSERT  INTO PancardConf_Dtls
        --       ( PCD_ID ,
        --         PCD_Memid ,
        --         PCD_ConfDate ,
        --         PCD_AdminID ,
        --         PCD_IPAddress ,
        --         PCD_Browser ,
        --         PCD_BrowserVersion ,
        --         PCD_decExtra1 ,
        --         PCD_decExtra2 ,
        --         PCD_chvExtra3 ,
        --         PCD_chvExtra4 ,
        --         PCD_chvExtra5
        --       )
        --VALUES  ( 1 ,
        --          1 , -- PCD_Memid - numeric
        --         CONVERT(DATETIME, @DOJ, 103) , -- PCD_ConfDate - datetime
        --          0 , -- PCD_AdminID - numeric
        --         '192.168.1.78' , -- PCD_IPAddress - varchar(50)
        --         'Chrome' , -- PCD_Browser - varchar(50)
        --         '57.0' , -- PCD_BrowserVersion - varchar(50)
        --         NULL , -- PCD_decExtra1 - numeric
        --         NULL , -- PCD_decExtra2 - numeric
        --         '' , -- PCD_chvExtra3 - varchar(100)
        --         '' , -- PCD_chvExtra4 - varchar(100)
        --         ''  -- PCD_chvExtra5 - varchar(100)
        --       )
        --SET IDENTITY_INSERT [PancardConf_Dtls] OFF

        SELECT 'CONGRATULATION!!!  FIRST MEMBER INITIALIZATION SUCCESSFUL';
        --ROLLBACK TRANSACTION
        COMMIT TRANSACTION;
        EXEC TempExec;
    END;
    ELSE
        ROLLBACK TRANSACTION;

END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;

    DECLARE @errormessage AS VARCHAR(MAX);
    DECLARE @errorseverity AS INT;
    DECLARE @errorState AS INT;

    SELECT @errormessage = ERROR_MESSAGE(),
           @errorseverity = ERROR_SEVERITY(),
           @errorState = ERROR_STATE();

    --RAISERROR (@errormessage, @errorseverity, @errorState) ;
    PRINT '';
    PRINT '=======================================================================================================================================';
    PRINT 'ERROR OCCURRED ==> ' + @errormessage;
    PRINT '=======================================================================================================================================';
    SET @errMsg = @errormessage;

END CATCH;

IF @errMsg = ''
BEGIN
    SELECT *
    FROM MemJoining_Dtls (NOLOCK);
    SELECT *
    FROM MemLogin_Dtls (NOLOCK);
    SELECT *
    FROM MemProfile_Dtls (NOLOCK);
END;