CREATE PROCEDURE [dbo].[prc_GetUpgradeRecords]    
    @UT_MemID int ,    
    @UT_NewPvCode int     
AS     
    BEGIN      
        SELECT  UT_decEXTRA2 AS 'checkno' ,    
                UT_chvEXTRA3 AS 'Bankname'  ,  
                  UT_chvEXTRA4 AS 'ChequeDate'    
        FROM    Upgrade_Trn (nolock)    
        WHERE   UT_NewPvCode = @UT_NewPvCode AND UT_MemID =@UT_MemID  
    END     
    RETURN @@ERROR 
Go
CREATE  PROCEDURE Prc_CheckBusinessStatus  
    @Sdate AS DATETIME = NULL ,  
    @Edate AS DATETIME = NULL  
AS   
    BEGIN       
        IF EXISTS ( SELECT TOP 1  
                            [id]  
                    FROM    [order] WITH ( NOLOCK )  
                    WHERE   PaidDateUtc >= @Sdate  
                            AND PaidDateUtc <= @Edate  
                            AND BizProcessflg = 0  
                            AND PaidDateUtc IS NOT NULL )   
            BEGIN                              
                SELECT  0 AS [Status]       
                    
            END      
        ELSE   
            BEGIN                              
                SELECT  1 AS [Status]       
                    
            END                  
    END 
Go 
CREATE PROCEDURE [prc_RepurchaseCount_New] -- prc_RepurchaseCount_New  1                                                               
    @AdminFlg NUMERIC(18, 0)      
AS       
    BEGIN                                                                        
          --RAISERROR ( 'Invalid mode of Payment.', 16, 1 )                                                             
                                                               
        DECLARE @dblMJD_MemID NUMERIC(18, 0) ,      
            @dtmMJD_DOJ DATETIME ,      
            @OrderId NUMERIC(18, 0) ,      
            @MaxCnt NUMERIC(18, 0) ,      
            @SCnt NUMERIC(18, 0) ,      
            @ConfirmFlg NUMERIC(18, 0) ,      
            @OrderType NUMERIC(18, 0) ,      
            @UpgDiffPVPts NUMERIC(18, 2) ,      
            @UpgDiffBVPts NUMERIC(18, 2)                                                             
            --@UT_OHCode VARCHAR(50)                                                         
                                                              
        SET NOCOUNT ON                                                           
      -- VARIABLE DECLARATION                                                              
                                                              
        DECLARE @intPV_PTMTypeID INT ,      
            @TreeType NUMERIC(18, 2)                                                                    
                                                               
        SELECT  @intPV_PTMTypeID = 5 ,      
                @TreeType = 0   -- 1 : binary , 2 : gen 3 : binary + generation                                                                      
                                                                
        SELECT  @TreeType = SM_Value      
        FROM    Settings_Mst (NOLOCK)      
        WHERE   sm_id = 73                                                                
                                                               
                                                           
        UPDATE  [Order]      
        SET     BizProcessFlg = 1      
        WHERE   ISNULL(BizProcessFlg, 0) = 0      
                AND PaymentMethodSystemName = 'Payments.CreditNoteWallet'                         
                                    
        UPDATE  [Order]      
        SET     BizProcessFlg = 1      
        WHERE   ISNULL(BizProcessFlg, 0) = 0      
                AND Id IN (      
                SELECT  id      
                FROM    [Order] (NOLOCK)      
                WHERE   OrderType = 0      
                        AND PaymentMethodSystemName = 'Payments.EPin'      
                        AND PaymentStatusId = 10      
                        AND CustomerId IN (      
                        SELECT  id      
                        FROM    Customer (NOLOCK)      
                        WHERE   MemId IN ( SELECT   MJD_MemID      
                                           FROM     MemJoining_Dtls (NOLOCK)      
                                           WHERE    MJD_PayType = 'FE' ) ) )                  
                           
                                          
                                                                  
        UPDATE  [Order]      
        SET     BizProcessFlg = 1      
        FROM    Customer (NOLOCK)      
        WHERE   CustomerId = customer.Id      
                AND ISNULL(MemId, 0) = 0      
                AND ISNULL(BizProcessFlg, 0) = 0                            
                                   
        --UPDATE  [Order]                        
        --SET     BizProcessFlg = 1                        
        --FROM    Customer (NOLOCK)                        
        --WHERE   OrderType NOT IN ( 1, 2, 3 )                              
                                                                 
                                                               
        DECLARE @noRows AS INT ,      
            @iCnt AS INT                                     
                                                               
        IF OBJECT_ID('tempdb..#TempReCur', 'U') IS NOT NULL       
            BEGIN                                                                      
                                   
                DROP TABLE #TempReCur                                                                 
                                           
                       
            END                   
                                                                
        CREATE TABLE #TempReCur      
            (      
              RowID INT IDENTITY(1, 1)      
                        PRIMARY KEY ,      
              PVCode NUMERIC(18, 0) ,      
              MRP NUMERIC(18, 2) ,      
              PVPts NUMERIC(18, 2) ,      
              BVPts NUMERIC(18, 2) ,      
              Qty NUMERIC(18, 0) ,      
              TotalSaleAmt NUMERIC(18, 2) ,      
              TotalPVPts NUMERIC(18, 2) ,      
              TotalBVPts NUMERIC(18, 2) ,      
              Memid NUMERIC(18, 0) ,      
              PVTypeID NUMERIC(18, 0) ,      
              OrderDate DATETIME      
            )                                                                                                  
                     
                                                               
        IF OBJECT_ID('tempdb..#TempOrderProcess', 'U') IS NOT NULL       
            BEGIN                                                               
                DROP TABLE #TempOrderProcess                                                            
            END                                                
                                                                   
        CREATE TABLE #TempOrderProcess      
            (      
              RowID INT IDENTITY(1, 1)      
                        PRIMARY KEY ,      
              OrderID NUMERIC(18, 0) ,      
              OrderDt DATETIME ,      
              Memid NUMERIC(18, 0) ,      
              OrderType NUMERIC(18, 0) ,      
              UpgDiffPVPts NUMERIC(18, 2) ,      
              UpgDiffBVPts NUMERIC(18, 2)      
            )                                                         
                                                                       
        INSERT  INTO #TempOrderProcess      
                ( OrderID ,      
                  OrderDt ,      
                  Memid ,      
                  OrderType ,      
                  UpgDiffPVPts ,      
                  UpgDiffBVPts                                                   
                )      
                SELECT  [Order].id ,      
                        PaidDateUtc ,      
                        MemId ,      
                        OrderType ,      
                        CONVERT(NUMERIC(18, 2), ISNULL(NewBiz.PV_PVPts, 0))      
                        - CONVERT(NUMERIC(18, 2), ISNULL(OldBiz.PV_PVPts, 0)) ,      
                        CONVERT(NUMERIC(18, 2), ISNULL(NewBiz.PV_BVPts, 0))      
                        - CONVERT(NUMERIC(18, 2), ISNULL(OldBiz.PV_BVPts, 0))      
                FROM    [Order] (NOLOCK)      
                        INNER JOIN Customer (NOLOCK) ON CustomerId = Customer.Id      
                        INNER JOIN MemJoining_Dtls (NOLOCK) ON MJD_MemID = MemId      
                        LEFT OUTER JOIN Upgrade_Trn WITH ( NOLOCK ) ON Upgrade_Trn.UT_MemID = MemId      
                                                              AND Upgrade_Trn.UT_ID = ( SELECT      
                                                              MAX(u.UT_ID)      
                                                              FROM      
                                                              Upgrade_Trn u      
                                                              WITH ( NOLOCK )      
                                                              WHERE      
                                                              u.UT_MemID = Upgrade_Trn.UT_MemID      
                                                              )      
                        LEFT OUTER JOIN PV_MST AS NewBiz ( NOLOCK ) ON ISNULL(UT_NewPvCode,      
                                                              MJD_PvCode) = NewBiz.pv_code      
                        LEFT OUTER JOIN PV_MST AS OldBiz ( NOLOCK ) ON UT_PrevPvCode = OldBiz.pv_code      
                WHERE   ISNULL(BizProcessFlg, 0) = 0      
                        AND PaymentStatusId = 30      
                        AND PaymentMethodSystemName <> 'Payments.CreditNoteWallet'      
                        AND PaidDateUtc IS NOT NULL                        
                        --AND OrderType IN ( 1, 2, 3 )                                      
                                              
                                                       
        SET @SCnt = 1                                                        
        SELECT  @MaxCnt = COUNT(RowID)      
        FROM    #TempOrderProcess                                                        
                                                               
        WHILE @SCnt <= @MaxCnt       
            BEGIN                                                    
                BEGIN TRY                                         
                    BEGIN TRANSACTION                                                
                                                         
                    TRUNCATE  TABLE #TempReCur                                                           
                                                                        
                    SELECT  @dblMJD_MemID = Memid ,      
                            @dtmMJD_DOJ = OrderDt ,      
                            @OrderId = OrderID ,      
                            @OrderType = OrderType ,      
                            @UpgDiffPVPts = UpgDiffPVPts ,      
                            @UpgDiffBVPts = UpgDiffBVPts      
                    FROM    #TempOrderProcess (NOLOCK)      
                    WHERE   RowID = @SCnt                                                        
                                             
                                         
                                             
                    DECLARE @UpgCountFlag TINYINT ,      
                        @UpgCount INT             
                                                
                    SET @UpgCount = 0            
                    SET @UpgCountFlag = 0             
                    IF @OrderType = 2       
                        BEGIN            
                                -- Chk for id confirm on upgrade -- if yes inc count tp upline            
                                --@dtmMJD_DOJ            
                            SELECT  @UpgCount = COUNT(UT_ID)      
                            FROM    Upgrade_Trn (NOLOCK)      
                            WHERE   UT_MemID = @dblMJD_MemID      
                                 --   AND UT_ConfirmDate < @dtmMJD_DOJ             
                                          
                                          
                            DECLARE @ConfStatus NUMERIC(18, 0)      
                                          
                            IF EXISTS ( SELECT  msd_memid      
                                        FROM    MemStatus_Dtls (NOLOCK)      
                                        WHERE   MSD_ConfFlg = 1      
                                                AND msd_memid = @dblMJD_MemID )       
                                BEGIN          
                                    SET @ConfStatus = 1      
                                             
                                END      
                            ELSE       
                                BEGIN      
                                    SET @ConfStatus = 0       
                                END       
                                        
         
                                          
                            IF @UpgCount > 0      
                                AND @ConfStatus = 0       
                                BEGIN      
                                    -- Increase Count      
                                    SET @UpgCountFlag = 1           
                                           
                                    UPDATE  MemStatus_Dtls      
                                    SET     MSD_MSMID = 5 ,      
                                            MSD_ConfirmDate = @dtmMJD_DOJ ,      
                                            MSD_ConfFlg = 1      
                                    WHERE   msd_memid = @dblMJD_MemID             
                                                   
                                END      
             ELSE       
                                BEGIN      
                                    SET @UpgCountFlag = 0          
                                END      
                                                                  
                                  
                                            
                                            
                        END            
                    ELSE       
                        BEGIN            
                            SET @UpgCountFlag = 0             
                            SET @UpgCount = 0            
                        END            
                                            
                                             
                                                                 
                    SET @ConfirmFlg = 1                                                        
                
                    INSERT  INTO #TempReCur      
                            SELECT  ISNULL(PV_MST.PV_Code, 0) ,      
                                    ISNULL(OrderItem.PriceInclTax, 0) ,      
                                    ( CASE WHEN @OrderType = 2      
                                           THEN @UpgDiffPVPts      
                                           ELSE ISNULL(OrderItem.PV, 0)      
                                      END ) ,      
                                    ( CASE WHEN @OrderType = 2      
                                           THEN @UpgDiffBVPts      
                                           ELSE ISNULL(OrderItem.BV, 0)      
                                      END ) ,      
                                    ISNULL(OrderItem.Quantity, 0) ,      
                                    ISNULL(OrderItem.PriceInclTax, 0) ,      
                                    ( CASE WHEN @OrderType = 2      
                                           THEN @UpgDiffPVPts      
                                           ELSE ISNULL(OrderItem.PV, 0)      
                                      END ) ,      
                                    ( CASE WHEN @OrderType = 2      
                                           THEN @UpgDiffBVPts      
                                           ELSE ISNULL(OrderItem.BV, 0)      
                                      END ) ,      
                                    @dblMJD_MemID ,      
                                    @intPV_PTMTypeID ,      
                                    @dtmMJD_DOJ      
                            FROM    OrderItem (NOLOCK)      
                                    INNER JOIN PV_MST (NOLOCK) ON PV_MST.PV_decExtra4 = OrderItem.ProductId      
                            WHERE   OrderItem.OrderId = @OrderId      
                                    AND IsPackage = 1                                                    
                                                          
                                                               
                                                                      
                    INSERT  INTO #TempReCur      
                            SELECT  ISNULL(PV_MST.PV_Code, 0) ,      
                                    ISNULL(OrderItem.PriceInclTax, 0) ,      
                                    ISNULL(OrderItem.PV, 0) ,      
                                    ISNULL(OrderItem.BV, 0) ,      
                                    ISNULL(OrderItem.Quantity, 0) ,      
                                    ISNULL(OrderItem.PriceInclTax, 0) ,      
                                    ISNULL(OrderItem.PV, 0) ,      
                                    ISNULL(OrderItem.BV, 0) ,      
                                    @dblMJD_MemID ,      
                                    @intPV_PTMTypeID ,      
                                    @dtmMJD_DOJ      
                            FROM    OrderItem (NOLOCK)      
                                    INNER JOIN PV_MST (NOLOCK) ON PV_MST.PV_decExtra4 = OrderItem.ProductId      
                            WHERE   OrderItem.OrderId = @OrderId      
                                    AND IsPackage = 0      
                                    AND ProductPackageId = 0                                                    
                                                            
  /* Depending upon the Position Newly Joined Members Detail will be stored in there Adjusted To Downline.                                                                    
                                                              
                IF there is an already a Record in MemDownline_Dtls THEN UPDATE it, ELSE INSERT INTO it.  */                                                           
                                                              
       ------ For Own ----------------------------------------------------                                                            
                                         
                    IF @AdminFlg = 1 -- Request By Member/Admin                                                 
                        BEGIN                                                          
                            IF @ConfirmFlg = 1       
                                BEGIN                                                                    
                                    UPDATE  MemDownline_Dtls      
                                    SET     MDD_ConfOwnBV = MDD_ConfOwnBV      
                                            + TotalBVPts ,      
                                            mdd_confownPV = mdd_confownPV      
                                            + TotalPVPts ,      
                                            MDD_ConfOwnSale = MDD_ConfOwnSale      
                                            + TotalSaleAmt ,      
                                            MDD_OwnSale = MDD_OwnSale      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalSaleAmt      
                                                END ) ,      
                                            MDD_OwnBV = MDD_OwnBV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalBVPts      
                                                END ) ,      
                                            MDD_OwnPV = MDD_OwnPV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalPVPts      
                                                END )      
                                    FROM    #TempReCur  (NOLOCK)      
                                    WHERE   MDD_MemID = Memid ---  @dblMJD_MemID                                                    
                                            AND MDD_Date = OrderDate      
                                            AND MDD_PvType = PVTypeID      
                         AND MDD_Level = 0      
                                            AND MDD_PvCode = PVCode                                                           
                                END                                                                          
                            INSERT  INTO MemDownline_Dtls      
                                    SELECT  Memid ,      
                                            PVCode ,      
                                            0 ,      
                                            PVTypeID ,      
                                            OrderDate ,      
                         0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                           0 ,      
                                            0 ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalBVPts      
                                              END ) ,      
                                            CASE WHEN 1 = 0 THEN 0      
                                                 ELSE TotalBVPts      
                                            END ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalPVPts      
                                              END ) ,      
                                            CASE WHEN 1 = 0 THEN 0      
                                                 ELSE TotalPVPts      
                                            END ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalSaleAmt      
                                              END ) ,      
                                            CASE WHEN 1 = 0 THEN 0      
                                                 ELSE TotalSaleAmt      
                                            END      
                                    FROM    #TempReCur (NOLOCK)      
                                    WHERE   Memid NOT IN (      
                                            SELECT  mdd_memid      
              FROM    MemDownline_Dtls  (NOLOCK)      
                                            WHERE   MDD_PvCode = #TempReCur.PVCode      
                                                    AND MDD_memid = Memid      
                                                    AND MDD_Date = OrderDate      
                                                    AND MDD_PvType = PVTypeID      
                                                    AND MDD_Level = 0 )                                                                      
                                                              
                                                                
                        END                                                                    
                                                                           
                                                                                       
    ---------------- Own End ------------------------------------------                                                                    
                                                              
                    IF @AdminFlg = 1 --Request By Member/Admin                                                                    
                        BEGIN                                       
                            IF @ConfirmFlg = 1       
                                BEGIN                                                          
                                    UPDATE  MemDownline_Dtls      
                         SET     MDD_ConfLeftBV = MDD_ConfLeftBV      
                                            + TotalBVPts ,      
                                            MDD_ConfLeftPV = MDD_ConfLeftPV      
                                            + TotalPVPts ,      
                                            MDD_ConfLeftSale = MDD_ConfLeftSale      
                                            + TotalSaleAmt ,      
                                            MDD_ConfLeftCnt = MDD_ConfLeftCnt      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 1      
                                                     WHEN @OrderType = 2      
                                                          AND @UpgCountFlag = 1      
                                                     THEN 1      
                                                     ELSE 0      
                                                END ) ,      
                                            MDD_LeftBV = MDD_LeftBV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalBVPts      
                                                END ) ,      
                                            MDD_LeftPV = MDD_LeftPV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalPVPts      
                                                END ) ,      
                                            MDD_LeftSale = MDD_LeftSale      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalSaleAmt      
                                                END )      
                                    FROM    IDWiseDownline_Dtls  (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND MDD_MemID = IDD_MEMID      
                                            AND MDD_Date = OrderDate      
  AND MDD_PvType = PVTypeID      
                                            AND MDD_PvCode = PVCode      
                                            AND MDD_Level = IDD_LEVEL      
                                            AND idd_lr = 1 --'L'                                                           
                                                                                        
                                    UPDATE  MemDownline_Dtls      
                                    SET     MDD_ConfRightBV = MDD_ConfRightBV      
                                            + TotalBVPts ,      
                                            MDD_ConfRightPV = MDD_ConfRightPV      
                                            + TotalPVPts ,      
                                            MDD_ConfRightSale = MDD_ConfRightSale      
                           + TotalSaleAmt ,      
                                            MDD_ConfRightCnt = MDD_ConfRightCnt      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 1      
                                                     WHEN @OrderType = 2      
                                                          AND @UpgCountFlag = 1      
                                                     THEN 1      
                                                     ELSE 0      
                                                END ) ,      
                                            MDD_RightBV = MDD_RightBV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
       ELSE TotalBVPts      
                                                END ) ,      
                                            MDD_RightPV = MDD_RightPV      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalPVPts      
                                                END ) ,      
                                            MDD_RightSale = MDD_RightSale      
                                            + ( CASE WHEN @OrderType = 0      
                                                     THEN 0      
                                                     ELSE TotalSaleAmt      
                                                END )      
                                    FROM    IDWiseDownline_Dtls  (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND MDD_MemID = IDD_MEMID      
                                            AND MDD_Date = OrderDate      
                                            AND MDD_PvType = PVTypeID      
                                            AND MDD_PvCode = PVCode      
                                            AND MDD_Level = IDD_LEVEL      
                                            AND idd_lr = 2 --'R'                                                            
                                END                                                            
                                                                
                            INSERT  INTO MemDownline_Dtls      
                                    SELECT  IDD_MEMID ,      
                                            PVCode ,      
                                            IDD_LEVEL ,      
                                            PVTypeID ,      
                                            OrderDate ,      
                                            0 ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 1      
                                                   WHEN @OrderType = 2      
                                              AND @UpgCountFlag = 1      
                                                   THEN 1      
                                                   ELSE 0      
                                              END ) , -- Conf Left                      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalBVPts      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalBVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                            ELSE TotalPVPts      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalPVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalSaleAmt      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalSaleAmt      
                                            END ,      
                                            0 ,      
                                 0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0      
                                    FROM    IDWiseDownline_Dtls  (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND idd_lr = 1 --'L'                                                                
                                            AND IDD_MEMID NOT IN (      
                                            SELECT  mdd_memid      
                                            FROM    MemDownline_Dtls  (NOLOCK)      
                                            WHERE   MDD_PvCode = #TempReCur.PVCode      
     AND MDD_memid = IDD_MEMID      
                                                    AND MDD_Date = OrderDate      
                                                    AND MDD_PvType = PVTypeID      
                                                    AND MDD_Level = IDD_LEVEL )                                                
                                                                                                
                            INSERT  INTO MemDownline_Dtls      
                                    SELECT  IDD_MEMID ,      
                                            PVCode ,      
                                            IDD_LEVEL ,      
                                            PVTypeID ,      
                                            OrderDate ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 1      
   WHEN @OrderType = 2      
                                                        AND @UpgCountFlag = 1      
                                                   THEN 1      
                                                   ELSE 0      
                                              END ) ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalBVPts      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalBVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalPVPts      
                                              END ) ,      
                              0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalPVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalSaleAmt      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalSaleAmt      
                                            END ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
       0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0      
                                    FROM    IDWiseDownline_Dtls  (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND idd_lr = 2 --'R'                                                                
                                            AND IDD_MEMID NOT IN (      
                                            SELECT  mdd_memid      
                                            FROM    MemDownline_Dtls  (NOLOCK)      
                                            WHERE   MDD_PvCode = #TempReCur.PVCode      
                                                    AND MDD_memid = IDD_MEMID      
                                                    AND MDD_Date = OrderDate      
AND MDD_PvType = PVTypeID      
                                                    AND MDD_Level = IDD_LEVEL )                                                          
                                                              
                        END                                                         
                    IF @TreeType = 3  --------  For Binary + Generation Plan ------------                                                            
                        BEGIN                                                              
                            IF @ConfirmFlg = 1       
                                BEGIN                                                           
                    ----------------  New Code Added by PVD For Own PV,BV,Sale Maintainance ------------------                                                              
                                    UPDATE  MemActDownline_Dtls      
                                    SET     MDD_ConfOwnBV = MDD_ConfOwnBV      
                                            + TotalBVPts ,      
                                            MDD_ConfOwnPV = MDD_ConfOwnPV      
      + TotalPVPts ,      
                                            MDD_ConfOwnSale = MDD_ConfOwnSale      
                                            + TotalSaleAmt      
                                    FROM    #TempReCur  (NOLOCK)      
                                    WHERE   MDD_MemID = Memid      
                                            AND MDD_Date = OrderDate      
                                            AND MDD_PvType = PVTypeID      
                                            AND MDD_PvCode = PVCode      
                                            AND MDD_Level = 0                                                              
                                                                                        
                  ---------- End of Code Added by PVD For Own PV,BV,Sale Maintainance ----------------                                                           
                           
                                    UPDATE  MemActDownline_Dtls      
                                    SET     MDD_ConfLeftBV = MDD_ConfLeftBV      
                                            + TotalBVPts ,      
                                            MDD_ConfLeftPV = MDD_ConfLeftPV      
                                            + TotalPVPts ,      
                                            MDD_ConfLeftSale = MDD_ConfLeftSale      
                                            + TotalSaleAmt ,      
                                            MDD_ConfLeftCnt = MDD_ConfLeftCnt      
                                            + CASE WHEN @OrderType = 0 THEN 1      
                                                   WHEN @OrderType = 2      
             AND @UpgCountFlag = 1      
                                                   THEN 1      
                                                   ELSE 0      
                                              END      
                                    FROM    IDWiseGenDownline_Dtls  (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND MDD_MemID = IDD_MEMID      
                                            AND MDD_Date = OrderDate      
                                            AND MDD_PvType = PVTypeID      
                                            AND MDD_PvCode = PVCode      
                                            AND MDD_Level = IDD_LEVEL                                                          
                                END                                                              
                                                                            
              ----------------  New Code Added by PVD For Own PV,BV,Sale Maintainance ------------------                                                                
                                      
                            INSERT  INTO MemActDownline_Dtls      
                                    SELECT  Memid ,      
                                            PVCode ,      
                                            0 ,      
                                            PVTypeID ,      
                                            OrderDate ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalBVPts      
                                              END ) ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalBVPts      
                                            END ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalPVPts      
                                              END ) ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalPVPts      
                                            END ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                              0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalSaleAmt      
                                              END ) ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalSaleAmt      
                                            END      
                                    FROM    #TempReCur  (NOLOCK)      
                                    WHERE   Memid NOT IN (      
                                            SELECT  mdd_memid      
                                            FROM    MemActDownline_Dtls  (NOLOCK)      
                                            WHERE   MDD_PvCode = #TempReCur.PVCode      
                                                    AND MDD_memid = Memid      
                                                    AND MDD_Date = OrderDate      
                               AND MDD_PvType = PVTypeID      
                                                    AND MDD_Level = 0 )                                                            
                                                              
   ---------- End of Code Added by PVD For Own PV,BV,Sale Maintainance ----------------                                                           
                                                              
                            INSERT  INTO MemActDownline_Dtls      
                                    SELECT  IDD_MEMID ,      
                                            PVCode ,      
                                            IDD_LEVEL ,      
                                            PVTypeID ,      
                                            OrderDate ,      
                                            0 ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 1      
                                                   WHEN @OrderType = 2      
                                                        AND @UpgCountFlag = 1      
                                                   THEN 1      
                                                   ELSE 0      
                                              END ) ,      
                                            0 ,      
                              ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalBVPts      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalBVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalPVPts      
                                              END ) ,      
                                            0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalPVPts      
                                            END ,      
                                            0 ,      
                                            ( CASE WHEN @OrderType = 0 THEN 0      
                                                   ELSE TotalSaleAmt      
                                              END ) ,      
                                       0 ,      
                                            CASE WHEN @ConfirmFlg = 0 THEN 0      
                                                 ELSE TotalSaleAmt      
                                            END ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                                            0 ,      
                    0 ,      
                                            NULL ,      
                                            NULL ,      
                                            0 ,      
                                            0      
                                    FROM    IDWiseGenDownline_Dtls   (NOLOCK) ,      
                                            #TempReCur  (NOLOCK)      
                                    WHERE   IDD_DOWNID = Memid      
                                            AND IDD_MEMID NOT IN (      
                                            SELECT  mdd_memid      
                                            FROM    MemActDownline_Dtls  (NOLOCK)      
                                            WHERE   MDD_PvCode = #TempReCur.PVCode      
                                                    AND MDD_memid = IDD_MEMID      
                                                    AND MDD_Date = OrderDate      
                                                    AND MDD_PvType = PVTypeID      
                                                    AND MDD_Level = IDD_LEVEL )                               
                        END                                                           
                                                                       
                /* Royalty Qualification procedure call */                                                                              
                                                                  
                    IF EXISTS ( SELECT  SM_ID      
                                FROM    Settings_Mst  (NOLOCK)      
                                WHERE   SM_ID = 23      
                                        AND SM_Flag = 1 /*AND SM_Value = 1*/ )       
                        BEGIN                                                                                                             
                            EXEC roychkstructure @dtmMJD_DOJ, 0, @dblMJD_MemID,      
                                4                                
                            IF EXISTS ( SELECT  Roy_ID      
                                        FROM    Royalty_Trn WITH ( NOLOCK )      
                                        WHERE   Roy_DwnMemId = @dblMJD_MemID      
                                                AND Roy_Datetime = @dtmMJD_DOJ )       
                                BEGIN                            
                                    UPDATE  [Order]      
                                    SET     RoyltyStatusFlag = 1      
              WHERE   id = @OrderId                             
                                END                                                                                           
                        END                                
                                                    
           /* Award Qualification procedure call */                                          
                                                    
                    IF EXISTS ( SELECT  SM_ID      
                                FROM    Settings_Mst WITH ( NOLOCK )      
                                WHERE   SM_ID = 26      
                                        AND SM_Flag = 1 )       
                        BEGIN                                  
                            DECLARE @strUpline VARCHAR(MAX)                                                                 
                    
                            SELECT  @strUpline = COALESCE(@strUpline + ',', '')      
                                    + CAST(IDD_MEMID AS VARCHAR)      
                            FROM    IDWiseDownline_Dtls WITH ( NOLOCK )      
                            WHERE   IDD_DOWNID = @dblMJD_MemID                                                                                            
                                        
                            IF @strUpline <> ''       
                                EXEC prc_AwardQualifierBulk @strUpline,      
                                    @dtmMJD_DOJ, @dblMJD_MemID, 1              
                            IF EXISTS ( SELECT  OAT_ID      
                                        FROM    OfferAchiever_Trn WITH ( NOLOCK )      
                                        WHERE   OAT_DownMemID = @dblMJD_MemID      
                                                AND OAT_AchvDt = @dtmMJD_DOJ )       
                                BEGIN                            
                                    UPDATE  [Order]      
                                    SET     AwardStatusFlag = 1      
                                    WHERE   id = @OrderId                             
                                END                                                                                    
                        END                              
                                                                              
                    UPDATE  [Order]      
                    SET     BizProcessFlg = 1      
                    WHERE   id = @OrderId                                                      
                                                                     
                    SET @SCnt = @SCnt + 1                               
                                                               
                                                               
                    COMMIT TRANSACTION;                                                                                          
                END TRY                                                                                    
                BEGIN CATCH                                                                                
              IF @@TRANCOUNT > 0       
                        ROLLBACK TRANSACTION;                                                                                          
                    DECLARE @ErrorMessage NVARCHAR(4000);                                                       
                    DECLARE @ErrorSeverity INT;                                                                                          
                    DECLARE @ErrorState INT;                                                        
                    SELECT  @ErrorMessage = ERROR_MESSAGE() ,      
                            @ErrorSeverity = ERROR_SEVERITY() ,      
                            @ErrorState = ERROR_STATE();                                                                                          
  RAISERROR (@ErrorMessage, -- Message text.                                     
            @ErrorSeverity, -- Severity.                                                                                          
            @ErrorState-- State.                                                                                          
               );                                                                                          
                END CATCH                                           
                                                           
                                                                     
            END                                                                        
    END
GO
ALTER  PROCEDURE [prc_ins_TempOrder_Hdr_PayU]   --Hello
    @TOH_ID NUMERIC(18, 0) OUTPUT ,    
    @TOH_PGCode VARCHAR(50) OUTPUT ,    
    @TOH_Code NUMERIC(20, 0) = NULL ,    
    @TOH_MemID NUMERIC(20, 0) = NULL ,    
    @TOH_AMID NUMERIC(20, 0) = NULL ,    
    @TOH_Date DATETIME ,    
    @TOH_DateTime DATETIME ,    
    @TOH_TotQty NUMERIC(20, 0) ,    
    @TOH_GrossAmt NUMERIC(20, 2) ,    
    @TOH_DiscAmt NUMERIC(20, 2) ,    
    @TOH_ShipAmt NUMERIC(20, 2) ,    
    @TOH_NetAmt NUMERIC(20, 2) ,    
    @TOH_TotBVPts NUMERIC(20, 2) ,    
    @TOH_TotPVPts NUMERIC(20, 2) ,    
    @TOH_TotUnits NUMERIC(20, 2) ,    
    @TOH_LR VARCHAR(50) = NULL ,    
    @TOH_BillName VARCHAR(100) = NULL ,    
    @TOH_BillAddress VARCHAR(500) = NULL ,    
    @TOH_BillCountryID NUMERIC(20, 0) = NULL ,    
    @TOH_BillCountry VARCHAR(50) = NULL ,    
    @TOH_BillStateID NUMERIC(20, 0) = NULL ,    
    @TOH_BillState VARCHAR(50) = NULL ,    
    @TOH_BillCity VARCHAR(50) = NULL ,    
    @TOH_BillPincode VARCHAR(20) = NULL ,    
    @TOH_BillMobile VARCHAR(20) = NULL ,    
    @TOH_BillContactNo VARCHAR(20) = NULL ,    
    @TOH_BillEmail VARCHAR(100) = NULL ,    
    @TOH_ShipName VARCHAR(100) = NULL ,    
    @TOH_ShipAddress VARCHAR(500) = NULL ,    
    @TOH_ShipCountryID NUMERIC(20, 0) = NULL ,    
    @TOH_ShipCountry VARCHAR(50) = NULL ,    
    @TOH_ShipStateID NUMERIC(20, 0) = NULL ,    
    @TOH_ShipState VARCHAR(50) = NULL ,    
    @TOH_ShipCity VARCHAR(50) = NULL ,    
    @TOH_ShipPincode VARCHAR(20) = NULL ,    
    @TOH_ShipMobile VARCHAR(20) = NULL ,    
    @TOH_ShipContactNo VARCHAR(20) = NULL ,    
    @TOH_ShipEmail VARCHAR(100) = NULL ,    
    @TOH_Paymode NUMERIC(20, 0) ,    
    @TOH_ChqDDNo NUMERIC(20, 0) = NULL ,    
    @TOH_ChqDDDate DATETIME = NULL ,    
    @TOH_ChqDDBank VARCHAR(100) = NULL ,    
    @TOH_ChqDDBranch VARCHAR(50) = NULL ,    
    @TOH_PinSrNo NUMERIC(18, 0) = NULL ,    
    @TOH_Remark VARCHAR(500) = NULL ,    
    @TOH_TrnFlg TINYINT ,    
    @TOH_StatusFlg TINYINT ,    
    @TOH_StatusDate DATETIME = NULL ,    
    @TOH_ConfirmFlg TINYINT ,    
    @TOH_ConfirmDate DATETIME = NULL ,    
    @TOH_DispatchMode NUMERIC(20, 0) = NULL ,    
    @TOH_DispatchBy NUMERIC(20, 0) = NULL ,    
    @TOH_DispatchFrID NUMERIC(20, 0) = NULL ,    
    @TOH_OrderType TINYINT ,    
    @TOH_FrID NUMERIC(20, 0) = NULL ,    
    @TOH_AdminID NUMERIC(20, 0) = NULL ,    
    @TOH_IPAddress VARCHAR(50) = NULL ,    
    @TOH_Browser VARCHAR(50) = NULL ,    
    @TOH_BrowserVersion VARCHAR(50) = NULL ,    
    --@TOH_PGCode VARCHAR(50) = NULL ,    
    @TOH_CCTrnAmt NUMERIC(20, 2) = NULL ,    
    @TOH_TrnTotalAmt NUMERIC(20, 2) = NULL ,    
    @TOH_chvExtra1 VARCHAR(100) = NULL ,    
    @TOH_chvExtra2 VARCHAR(100) = NULL ,    
    @TOH_chvExtra3 VARCHAR(100) = NULL ,    
    @TOH_chvExtra4 VARCHAR(100) = NULL ,    
    @TOH_decExtra1 NUMERIC(20, 2) = NULL ,    
    @TOH_decExtra2 NUMERIC(20, 2) = NULL ,    
    @TOH_decExtra3 NUMERIC(20, 2) = NULL ,    
    @TOH_decExtra4 NUMERIC(20, 2) = NULL ,    
    @TOH_dtmExtra1 DATETIME = NULL ,    
    @TOH_dtmExtra2 DATETIME = NULL ,    
    @TOH_dtmExtra3 DATETIME = NULL ,    
    @TOH_dtmExtra4 DATETIME = NULL ,    
    @TOH_RejectedFrID NUMERIC(20, 0) = NULL    
AS     
    DECLARE @PGT_PGCode AS VARCHAR(15)    
                                                     
    EXEC prc_GeneratePGCode @PGT_PGCode OUTPUT, @TOH_DateTime              
      
    INSERT  INTO PaymentGateway_Trn    
            ( PGT_PGCode ,    
              PGT_RefNo ,    
              PGT_Date ,    
              PGT_DateTime ,    
              PGT_TrnID ,    
              PGT_PAID ,    
              PGT_BID ,    
              PGT_BANK ,    
              PGT_Remarks          
            )    
    VALUES  ( @PGT_PGCode ,    
              NULL ,    
              @TOH_Date ,    
              @TOH_DateTime ,    
              21 ,    
              NULL ,    
              NULL ,    
              NULL ,    
            NULL                  
            )          
     
              
                
    SET @TOH_ID = @@IDENTITY         
        
        
    SET @TOH_PGCode = @PGT_PGCode                       
                
    RETURN @@ERROR  
GO
ALTER  PROCEDURE [dbo].[prc_ins_TempOrder_HdrResaleForCC]  --Hello
    @TOH_ID NUMERIC(18, 0) OUTPUT ,  
    @TOH_PGCode VARCHAR(50) OUTPUT ,  
    @TOH_MemID NUMERIC(20, 0) = NULL ,  
    @TOH_AMID NUMERIC(20, 0) = NULL ,  
    @TOH_TotQty NUMERIC(20, 0) ,  
    @TOH_GrossAmt NUMERIC(20, 2) ,  
    @TOH_DiscAmt NUMERIC(20, 2) ,  
    @TOH_ShipAmt NUMERIC(20, 2) ,  
    @TOH_NetAmt NUMERIC(20, 2) ,  
    @TOH_TotBVPts NUMERIC(20, 2) ,  
    @TOH_TotPVPts NUMERIC(20, 2) ,  
    @TOH_TotUnits NUMERIC(20, 2) ,  
    @TOH_LR VARCHAR(50) = NULL ,  
    @TOH_ShipName VARCHAR(100) = NULL ,  
    @TOH_ShipAddress VARCHAR(500) = NULL ,  
    @TOH_ShipCountryID NUMERIC(20, 0) = NULL ,  
    @TOH_ShipCountry VARCHAR(50) = NULL ,  
    @TOH_ShipStateID NUMERIC(20, 0) = NULL ,  
    @TOH_ShipState VARCHAR(50) = NULL ,  
    @TOH_ShipCity VARCHAR(50) = NULL ,  
    @TOH_ShipPincode VARCHAR(10) = NULL ,  
    @TOH_ShipMobile VARCHAR(20) = NULL ,  
    @TOH_ShipContactNo VARCHAR(20) = NULL ,  
    @TOH_ShipEmail VARCHAR(100) = NULL ,  
    @TOH_ChqDDNo NUMERIC(20, 0) = NULL ,  
    @TOH_ChqDDDate DATETIME = NULL ,  
    @TOH_ChqDDBank VARCHAR(100) = NULL ,  
    @TOH_ChqDDBranch VARCHAR(50) = NULL ,  
    @TOH_PinSrNo NUMERIC(20, 0) = NULL ,  
    @TOH_Remark VARCHAR(500) = NULL ,  
    @TOH_TrnFlg TINYINT ,  
    @TOH_DispatchMode NUMERIC(20, 0) = NULL ,  
    @TOH_DispatchBy NUMERIC(20, 0) = NULL ,  
    @TOH_DispatchFrID NUMERIC(20, 0) = NULL ,  
    @TOH_OrderType TINYINT ,  
    @TOH_FrID NUMERIC(20, 0) = NULL ,  
    @TOH_AdminID NUMERIC(20, 0) = NULL ,  
    @TOH_IPAddress VARCHAR(50) = NULL ,  
    @TOH_Browser VARCHAR(50) = NULL ,  
    @TOH_BrowserVersion VARCHAR(50) = NULL ,  
    @Payment_Desc VARCHAR(100) ,  
    @TOD_PVCode NVARCHAR(MAX) ,  
    @TOD_Qty NVARCHAR(MAX) ,  
    @TOH_CCTrnAmt NUMERIC(20, 2) ,  
    @TOH_TrnTotalAmt NUMERIC(20, 0) ,  
    @StateSaleVATs NVARCHAR(MAX) ,  
    @CurrDate DATETIME ,  
    @CurrDateTime DATETIME  
AS   
    BEGIN                                                              
        DECLARE @Flag INT ,  
            @i INT ,  
            @PVCode NUMERIC(18, 0) ,  
            @Qty AS NUMERIC(20, 0) ,  
            @ResaleTrnID INT ,  
            @TOH_StatusFlg INT ,  
            @TOH_StatusDate DATETIME                                     
                                  
        DECLARE @SumNetAmt NUMERIC(20, 2) ,  
            @SumTotBVPts NUMERIC(20, 2) ,  
            @SumTotPVPts NUMERIC(20, 2) ,  
            @SumTotUnits NUMERIC(20, 2) ,  
            @StateSaleVAT NUMERIC(18, 2) ,  
            @TOH_BillName VARCHAR(100) ,  
            @TOH_BillAddress VARCHAR(500) ,  
            @TOH_BillCountryID NUMERIC(20, 0) ,  
            @TOH_BillCountry VARCHAR(50) ,  
            @TOH_BillStateID NUMERIC(20, 0) ,  
            @TOH_BillState VARCHAR(50) ,  
            @TOH_BillCity VARCHAR(50) ,  
            @TOH_BillPincode VARCHAR(10) ,  
            @TOH_BillMobile VARCHAR(20) ,  
            @TOH_BillContactNo VARCHAR(20) ,  
            @TOH_BillEmail VARCHAR(100)                                            
                                                        
                                                                       
                            
        SET @SumNetAmt = 0                                            
        SET @SumTotBVPts = 0                                            
        SET @SumTotPVPts = 0                                            
        SET @SumTotUnits = 0                                 
        SET @TOH_TotQty = 0                                         
                                          
        DECLARE @TOH_Paymode AS INT                                                            
        SELECT  @TOH_Paymode = PM_ID  
        FROM    Paymode_mst  WITH (NOLOCK)
        WHERE   PM_desc = @Payment_Desc                                            
                                                  
        BEGIN TRY                                        
            BEGIN TRANSACTION                                          
                                                        
        /* ---- CODE TO GENERATE PAYMENTGATEWAY REF. CODE IN DDMMYYNNNN format. e.g. 0707101001, 0707101002, 0807101001, 0807101002 */                                        
            EXEC prc_GeneratePGCode @TOH_PGCode OUTPUT, @CurrDateTime                                      
                                                
            IF @TOH_PGCode IS NULL  
                OR LTRIM(RTRIM(@TOH_PGCode)) = ''   
                BEGIN                                        
                    RAISERROR ('Payment Gateway Reference ID not generated.', 16,      
                    1)                                        
                END                                        
            ELSE   
                BEGIN              
                            
                    IF @TOH_MemID IS NULL   
                        BEGIN            
                            SET @TOH_BillName = @TOH_ShipName            
                            SET @TOH_BillAddress = @TOH_ShipAddress            
                            SET @TOH_BillCountryID = @TOH_ShipCountryID             
                            SET @TOH_BillCountry = @TOH_ShipCountry            
                            SET @TOH_BillStateID = @TOH_ShipStateID             
                            SET @TOH_BillState = @TOH_ShipState            
                            SET @TOH_BillCity = @TOH_ShipCity            
                            SET @TOH_BillPincode = @TOH_ShipPincode            
                            SET @TOH_BillMobile = @TOH_ShipMobile            
                            SET @TOH_BillContactNo = @TOH_ShipContactNo            
                            SET @TOH_BillEmail = @TOH_ShipEmail            
                        END             
                    ELSE   
                        IF @TOH_MemID IS NOT NULL   
                            BEGIN             
                                SELECT  @TOH_BillName = MPD_Name ,  
                                        @TOH_BillAddress = MPD_Address ,  
                                        @TOH_BillCity = MPD_City ,  
                                        @TOH_BillState = MPD_State ,  
                                        @TOH_BillPincode = MPD_PinCode ,  
                                        @TOH_BillContactNo = MPD_ResPhone ,  
                                        @TOH_BillMobile = MPD_Mobile  
                                FROM    MemProfile_Dtls  WITH (NOLOCK)
                                WHERE   MPD_MemId = @TOH_MemID                 
                            END              
                                
                                
                    IF @TOH_DispatchMode = 1   
                        BEGIN                 
                            SET @TOH_ShipName = NULL            
                            SET @TOH_ShipAddress = NULL            
                            SET @TOH_ShipCountryID = NULL            
                            SET @TOH_ShipCountry = NULL            
                            SET @TOH_ShipStateID = NULL            
                            SET @TOH_ShipState = NULL            
                            SET @TOH_ShipCity = NULL            
                            SET @TOH_ShipPincode = NULL            
                            SET @TOH_ShipMobile = NULL            
                            SET @TOH_ShipContactNo = NULL            
                            SET @TOH_ShipEmail = NULL                   
                        END               
                                                      
                                                      
                    IF @TOH_AdminID IS NOT NULL   
                        BEGIN                                                      
                            SET @ResaleTrnID = 13                                              
                        END                                                      
                    ELSE   
                 IF @TOH_FrID IS NOT NULL   
                            BEGIN                                                
                                SET @ResaleTrnID = 14                          
                            END                 
                        ELSE   
                            IF ( ( @TOH_FrID IS  NULL )  
                                 AND ( @TOH_AdminID IS  NULL )  
                               )   
                                BEGIN                                                
                                    SET @ResaleTrnID = 23                                       
                                END                                                          
                                                              
                    INSERT  INTO PaymentGateway_Trn  
                            ( PGT_RefNo ,  
                              PGT_PGCode ,  
                              PGT_Date ,  
                              PGT_DateTime ,  
                              PGT_TrnID ,  
                              PGT_PAID ,  
                              PGT_BID ,  
                              PGT_BANK ,  
                              PGT_Remarks  
                            )  
                    VALUES  ( NULL ,  
                              @TOH_PGCode ,  
                              @CurrDate ,  
                              @CurrDateTime ,                                       
                                               
              /* PGT_TrnID - numeric(18, 0) */  
                              @ResaleTrnID ,                                        
              /* PGT_PAID - nchar(10) */  
                              NULL ,                                        
              /* PGT_BID - varchar(50) */  
                              NULL ,                                        
              /* PGT_BANK - varchar(100) */  
                              NULL ,                                        
              /* PGT_Remarks - varchar(1000) */  
                              NULL  
                            )                                        
                END                                        
                                                      
                                         
                                                     
            IF @TOH_DispatchMode = 1   
                BEGIN                                             
                    SET @TOH_StatusFlg = 3                                            
                    SET @TOH_StatusDate = @CurrDateTime                                  
                END                                      
            ELSE   
                BEGIN                                    
                    SET @TOH_StatusFlg = 1                                            
                    SET @TOH_StatusDate = NULL                                    
                END                              
                             
                                                  
                                                                     
                                              
            SET @TOH_ID = @@IDENTITY                                            
                                                   
            DECLARE @RowCnt AS INT                                                                          
            SELECT  @RowCnt = COUNT(items)  
            FROM    Split(@TOD_PVCode, ',')                                                         
                                                                    
            SET @i = 1                                                           
                                                                    
            WHILE @i <= ( @RowCnt )   
                BEGIN                                   
     /*------------ For Product Code--------------------*/                                                                  
                    SELECT  @PVCode = items  
                    FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY ( SELECT  
                                                              0  
                                                              ) ) AS ROW_NUMBER ,  
                                        items  
                              FROM      Split(@TOD_PVCode, ',')  
                            ) SearchResults  
                    WHERE   ROW_NUMBER = @i                                                         
                                                                                               
                                                             
                    SELECT  @Qty = items  
                    FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY ( SELECT  
                                                              0  
                                                              ) ) AS ROW_NUMBER ,  
                                        items  
                              FROM      Split(@TOD_Qty, ',')  
                            ) SearchResults  
                    WHERE   ROW_NUMBER = @i                             
                                                                      
                                                                            
          
                    SELECT  @StateSaleVAT = items  
                    FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY ( SELECT  
                                                              0  
                                                              ) ) AS ROW_NUMBER ,  
                                        items  
                              FROM      Split(@StateSaleVATs, ',')  
                            ) SearchResults  
                    WHERE   ROW_NUMBER = @i                                                                                
                                                                        
                                                    
                    DECLARE @TOD_BVPts NUMERIC(20, 2) ,  
                        @TOD_PVPts NUMERIC(20, 2) ,  
                        @TOD_Units NUMERIC(20, 2) ,  
                        @TOD_Rate NUMERIC(20, 2) ,  
                        @TOD_VAT NUMERIC(20, 2) ,  
                        @TOD_MRP NUMERIC(20, 2) ,  
                        @TOD_ShipAmt NUMERIC(20, 2) ,  
                        @TOD_DiscAmt NUMERIC(20, 2) ,  
                        @TOD_TotDiscAmt NUMERIC(20, 2) ,  
                        @TOD_TotShipAmt NUMERIC(20, 2) ,  
                        @TOD_NetAmt NUMERIC(20, 2) ,  
                        @TOD_TotBVPts NUMERIC(20, 2) ,  
                        @TOD_TotPVPts NUMERIC(20, 2) ,  
                        @TOD_TotUnits NUMERIC(20, 2)                                             
                                                            
                                                             
                                                                      
                                                                      
                    SELECT  @TOD_BVPts = pv_bvpts ,  
                            @TOD_PVPts = PV_PVPts ,  
                            @TOD_Units = PV_GrUnits ,  
                            @TOD_Rate = ( PV_SalePrice * 100 )  
                            / ( @StateSaleVAT + 100 ) ,  
                            @TOD_VAT = @StateSaleVAT ,  
                            @TOD_MRP = PV_SalePrice ,  
                            @TOD_ShipAmt = PV_ShipAmt ,  
                            @TOD_DiscAmt = PV_DiscAmt  
                    FROM    PV_MST  WITH (NOLOCK)
                    WHERE   PV_Code = @PVCode                               
                       
                    IF @TOH_DispatchMode = 1   
                        BEGIN                             
                            SET @TOD_ShipAmt = 0                          
                        END                                              
                                                                                                                      
                                                                  
                    SET @TOD_TotDiscAmt = @TOD_DiscAmt * @Qty                                         
                    SET @TOD_TotShipAmt = @TOD_ShipAmt * @Qty                                      
                    SET @TOD_NetAmt = ( ( @TOD_MRP * @Qty ) + @TOD_TotShipAmt )  
                        - @TOD_TotDiscAmt                                            
                                                                    
                    SET @TOD_TotBVPts = @TOD_BVPts * @Qty                                            
                    SET @TOD_TotPVPts = @TOD_PVPts * @Qty                                            
                    SET @TOD_TotUnits = @TOD_Units * @Qty                                            
                                                                
                                           
                    SET @SumNetAmt = @SumNetAmt + @TOD_NetAmt                                            
                    SET @SumTotBVPts = @SumTotBVPts + @TOD_TotBVPts                                            
                    SET @SumTotPVPts = @SumTotPVPts + @TOD_TotPVPts                                             
                    SET @SumTotUnits = @SumTotUnits + @TOD_TotUnits                                             
                                                                
                                                                    
                                  
                                                              
                    SET @TOH_TotQty = @TOH_TotQty + @Qty                                                                                     
                                                 
                    SET @i = @i + 1                                                                     
                END                                                         
                                                                                 
                            
                                      
                                                      
                                             
                                                                
            IF @@TRANCOUNT > 0   
                COMMIT TRANSACTION;                                                        
                                                               
        END TRY                                                                                
                                                                                        
        BEGIN CATCH                                                  
            ROLLBACK TRANSACTION;                                                                       
            DECLARE @ErrorMessage NVARCHAR(4000);                                       
            DECLARE @ErrorSeverity INT;                                                                                          
            DECLARE @ErrorState INT;                                                                     
                                                                                          
            SELECT  @ErrorMessage = ERROR_MESSAGE() ,  
                    @ErrorSeverity = ERROR_SEVERITY() ,  
                    @ErrorState = ERROR_STATE();                                                                                          
                                             
                                                                                             
            RAISERROR (@ErrorMessage, -- Message text.                                             
            @ErrorSeverity, -- Severity.                                                                                          
            @ErrorState-- State.                                                                                          
               );                                                                                          
        END CATCH                                          
    END
GO
DROP PROC prc_AddToCart
DROP PROCEDURE prc_ins_ChangeDispatchBy_TrnForHO
DROP PROCEDURE Prc_ExecOrderPlaceBym2webMEmberConfirmTransaction
DROP PROCEDURE prc_IsOrderPresent
DROP PROCEDURE prc_IsOrderPresentForUnconfirm
DROP PROCEDURE prc_OrdernoSearch
DROP PROCEDURE prc_OrdersForLeaderPCR
DROP PROCEDURE prc_GeneratePCRForCC
DROP PROCEDURE prc_GeneratePCR
DROP PROCEDURE prc_PvWisePCRQtyToOrderForLeader
DROP View vw_TotalvatAmount
DROP PROCEDURE prc_TotalVatOrderWise
DROP PROCEDURE prc_GetOrderDetails
DROP PROCEDURE prc_GetPlacedOrderDetailsForAdminSMS
DROP PROCEDURE prc_ins_Order_Dtls
DROP PROCEDURE prc_ins_UpgradeComplete
DROP PROCEDURE prc_AddToCartNew
DROP PROCEDURE prc_CheckoutCartEwallet
DROP PROCEDURE prc_CheckoutCartEwalletNew
DROP PROCEDURE prc_GenerateOrderCode
DROP PROCEDURE prc_GetCartDetails
DROP PROCEDURE prc_GenerateNewOrderCode
DROP PROCEDURE Prc_UnconfirmedOrderSummary
DROP PROCEDURE prc_GetMemberOrderSummaryRpt
DROP PROCEDURE prc_GetOrderSummaryRpt
DROP PROCEDURE prc_GetOrderSummaryRptCount
DROP PROCEDURE prc_ins_StockReturnByFR_Hdr
DROP PROCEDURE Prc_UnconfirmedOrderSummaryCount
DROP VIEW vw_GetOrderDtls
DROP VIEW vwInventoryOrderedQty
DROP PROCEDURE prc_RemoveFromCart
DROP PROCEDURE prc_getRegisrationOrderOH_Code
DROP PROCEDURE Prc_ExecOrderPlaceBym2webMEmber
DROP PROCEDURE prc_UpdateTaxAndShippingChargesForSelectedState
DROP PROCEDURE prc_RepurchaseCount
DROP PROCEDURE prc_RejectOrderbyHO
DROP PROCEDURE Prc_SelSMSMemDtls
DROP PROCEDURE prc_CheckoutCart
DROP PROCEDURE prc_MemOrderSearch
DROP PROCEDURE prc_IsPOExists
DROP PROCEDURE prc_ins_Order_Dtls_PayU
DROP PROCEDURE prc_OrderHdrOhCode
DROP PROCEDURE prc_CheckOrderExists
DROP PROCEDURE prc_CheckIfOrderExists
DROP TABLE dbo.Order_Dtls
DROP TABLE dbo.Order_Hdr
DROP VIEW vw_MakeTransfer
GO
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES( N'OrderSetting.InvoiceNo.Increment.Logic', N'0', 0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'commonsettings.taskfailureemailidNotification', N'',0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'Customer.Fields.Setting.StateId', N'0',  0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'dashboardsettings.ZipCodeDetails', N'False',  0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'dashboardsettings.ZipCodeURL', N'',  0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'dashboardsettings.ZipCodeKey', N'0',  0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'customsettings.TwofactorAuthentication', N'0',  0)
INSERT INTO dbo.Setting ( Name, Value, StoreId )VALUES  ( N'customsettings.AppliedToKeyword', N'',  0)
GO
CREATE PROCEDURE prc_GetCustomerInfo
    @memNumber VARCHAR(MAX)
AS 
    BEGIN
        SELECT  MJD_MemID ,
                MJD_Introducer ,
                MPD_Name ,
                MJD_MemNo AS sponNo ,
                MPD_Name AS sponname ,
                MJD_AdjustedTo AS AdjustedToNo ,
                MPD_Name AS AdjustedName ,
                MPD_Email ,
                MJD_LR
        FROM    MemJoining_Dtls WITH ( NOLOCK )
                INNER JOIN MemProfile_Dtls WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = MemProfile_Dtls.MPD_MemId
        WHERE   MemJoining_Dtls.MJD_MemNo = @memNumber
    END     
GO
CREATE PROCEDURE prc_GetAdjustedInfo
    @AdjustedToNo VARCHAR(MAX)
AS 
    BEGIN
        SELECT  MJD_MemNo AS AdjustedToNo ,
                MPD_Name AS AdjustedName
        FROM    MemJoining_Dtls WITH ( NOLOCK )
                INNER JOIN MemProfile_Dtls WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = MemProfile_Dtls.MPD_MemId
        WHERE   MemJoining_Dtls.MJD_MemID = @AdjustedToNo
    END     
GO
CREATE PROCEDURE prc_GetSponserinfo
    @MJD_Introducer VARCHAR(MAX)
AS 
    BEGIN
        SELECT  MJD_MemNo AS sponNo ,
                MPD_Name AS sponname
        FROM    MemJoining_Dtls WITH ( NOLOCK )
                INNER JOIN MemProfile_Dtls WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = MemProfile_Dtls.MPD_MemId
        WHERE   MemJoining_Dtls.MJD_MemID = @MJD_Introducer
    END   
GO
Create PROCEDURE prc_GetStatusWiseAward
AS 
    BEGIN
        SELECT  Id ,
                CustomerId ,
                ( SELECT    ISNULL(MemId, 0) AS MemId
                  FROM      Customer (NOLOCK)
                  WHERE     Id = CustomerId
                ) AS MemId ,
                ( SELECT    ISNULL(Email, NULL) AS Email
                  FROM      Customer (NOLOCK)
                  WHERE     Id = CustomerId
                ) AS Email ,
                ( SELECT TOP 1
                            ISNULL(value, NULL) AS PhoneNo
                  FROM      GenericAttribute (NOLOCK)
                  WHERE     [Key] = 'Phone'
                            AND EntityID = CustomerId
                  ORDER BY  id DESC
                ) AS PhoneNo
        FROM    [Order] (NOLOCK)
        WHERE   AwardStatusFlag = 1
    END
Go
Create PROCEDURE prc_GetStatusWiseRoyalty
AS 
    BEGIN
        SELECT  Id ,
                CustomerId ,
                ( SELECT    ISNULL(MemId, 0) AS MemId
                  FROM      Customer (NOLOCK)
                  WHERE     Id = CustomerId
                ) AS MemId ,
                ( SELECT    ISNULL(Email, NULL) AS Email
                  FROM      Customer (NOLOCK)
                  WHERE     Id = CustomerId
                ) AS Email ,
                ( SELECT TOP 1
                            ISNULL(value, NULL) AS PhoneNo
                  FROM      GenericAttribute (NOLOCK)
                  WHERE     [Key] = 'Phone'
                            AND EntityID = CustomerId
                  ORDER BY  id DESC
                ) AS PhoneNo
        FROM    [Order] (NOLOCK)
        WHERE   RoyltyStatusFlag = 1
    END
Go
CREATE PROCEDURE prc_GetAwardFromOfferMst
     @DwnMeID VARCHAR(100)
    AS 
        BEGIN
            SELECT  OM_Name ,
                    OAT_MemID
            FROM    Offer_Mst (NOLOCK)
                    INNER JOIN ( SELECT OAT_MemID ,
                                        MAX(OAT_serialno) AS SrNo
                                 FROM   OfferAchiever_Trn WITH ( NOLOCK )
                                 WHERE  OAT_decExtra2 = 1
                                        AND OAT_DownMemID = @DwnMeID
                                 GROUP BY OAT_MemID
                               ) AS CTE ON OM_Serialno = SrNo
            WHERE   OM_AwardFlg = 1  
END
GO
CREATE PROCEDURE prc_GetCustomerTradersForAutocomplete
    @term VARCHAR(MAX)
AS 
    BEGIN
       SELECT Id,EntityId,FirstName,LastName,MemId,Memno,Username
FROM
(
SELECT Top 10  Id, EntityId,FirstName,LastName,MemId,Memno,Username FROM  
(SELECT [EntityId], [key], [value] FROM GenericAttribute WITH (NOLOCK)) AS t PIVOT (MAX([value]) 
 FOR [key] IN ([FirstName],[LastName])) AS p INNER JOIN Customer c WITH (NOLOCK) ON c.Id=p.EntityId
  WHERE Id <> 4 AND (Memno LIKE @term OR FirstName LIKE '%'+@term+'%' OR LastName LIKE '%'+@term+'%') 
  UNION
  SELECT  Id, EntityId,Company,NULL,MemId,Memno,Username FROM  
(SELECT [EntityId], [key], [value] FROM GenericAttribute WITH (NOLOCK)) AS t PIVOT (MAX([value]) 
 FOR [key] IN ([Company])) AS p INNER JOIN Customer c WITH (NOLOCK) ON c.Id=p.EntityId
  WHERE Id <> 4 AND (Memno LIKE @term OR Company LIKE '%'+@term+'%')

 )  as d
 END     
GO
CREATE PROCEDURE [dbo].[prc_RepurchaseBiz_Process]
AS 
    BEGIN

        DECLARE @Sm_BinaryFlag INT 
        SET @Sm_BinaryFlag = 0 
  
        SELECT  @Sm_BinaryFlag = ISNULL(SM_decExtra1, 0)
        FROM    Settings_Mst (NOLOCK)
        WHERE   SM_ID = 19
    
        IF @Sm_BinaryFlag = 2 
            BEGIN
            
            -- For Daily Binary
             --   EXEC prc_RepurchaseCount_DailyBinary 1
                EXEC prc_RepurchaseCount_New 1 
            END
        ELSE 
            BEGIN
            
            -- For Other Plan
                EXEC prc_RepurchaseCount_New 1
            END 
            


    END
GO
CREATE VIEW [vw_CustomerWalletbalancwe]      
 AS       
             SELECT  ET_CustomerId ,   ISNULL( (credit), 0) AS credit , ISNULL( (debit), 0)AS debit  ,     
                   ISNULL( (credit), 0) - (ISNULL( (debit), 0)-ISNULL( (WT_Amt), 0))  AS  balance  FROM      
                   ( SELECT  ET_CustomerId  , ISNULL(SUM(credit), 0) AS credit , ISNULL(SUM(debit), 0)AS debit  ,     
                   ISNULL(SUM(credit), 0) - (ISNULL(SUM(debit), 0) )  AS  balance      
                   FROM(      
        SELECT ET_CustomerId  , ISNULL(SUM(ET_Amt), 0) AS credit ,0  AS debit  FROM Ewallet_trn       
         WHERE   ET_Flag = 0       
         GROUP BY ET_CustomerId        
                   UNION ALL      
         SELECT ET_CustomerId   ,0  AS credit, ISNULL(SUM(ET_Amt), 0) AS debit  FROM Ewallet_trn       
       WHERE   ET_Flag = 1       
         GROUP BY ET_CustomerId      
             
         ) AS t       
                      GROUP BY ET_CustomerId )temp LEFT OUTER JOIN   ( SELECT  wt_CustomerId,ISNULL(SUM(WT_Amt), 0)   AS WT_Amt    
                    FROM    withdrawl_trn (NOLOCK)      
                    WHERE   wt_Flag = 0      
                            GROUP BY wt_CustomerId    
                  )withdrawl     ON temp.ET_CustomerId = withdrawl.wt_CustomerId
GO
   CREATE PROCEDURE [dbo].[prc_sel_Ewallet_Trn_chkbalanceadminCustomerId_NEW]     
    @ET_CustomerId NUMERIC(18, 0) ,    
    @WT_id NUMERIC(18, 0)    
AS     
    SELECT   (SELECT Balance FROM vw_CustomerWalletbalancwe WITH (NOLOCK)      
              WHERE ET_CustomerID= @ET_CustomerId ) AS balance,    
                
            ( SELECT    ISNULL(COUNT(wt_ID), 0)    
              FROM      withdrawl_trn (NOLOCK)    
              WHERE     WT_customerid = @ET_CustomerId    
                        AND wt_id = @WT_id    
                        AND wt_flag = 0    
            ) AS ID
GO