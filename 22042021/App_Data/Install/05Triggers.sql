Create TRIGGER [dbo].[trg_MemberRegistration] ON [dbo].[MemJoining_Dtls]
    FOR INSERT
AS 
BEGIN
	DECLARE @dblMJD_MemID NUMERIC(18, 0),
        @chvMJD_MemNo VARCHAR(50),
        @intPM_ID NUMERIC(18, 0),
        @dtmMJD_DOJ DATETIME

    BEGIN
        DECLARE @Sm_BinaryFlag INT 
        SET @Sm_BinaryFlag = 0 
  
        SELECT  @dblMJD_MemID = MJD_MemID, @chvMJD_MemNo = MJD_MemNo,
                @dtmMJD_DOJ = MJD_DOJ, @intPM_ID = MJD_Paymode
        FROM    Inserted
    
        SELECT  @Sm_BinaryFlag = ISNULL(SM_decExtra1, 0)
        FROM    Settings_Mst (NOLOCK)
        WHERE   SM_ID = 19
    
        IF @Sm_BinaryFlag = 2 
            BEGIN
                EXEC prc_MemberDailyRegistration @dblMJD_MemID, @chvMJD_MemNo,
                    @intPM_ID, @dtmMJD_DOJ 
            END
        ELSE 
            BEGIN
                EXEC prc_MemberRegistration @dblMJD_MemID, @chvMJD_MemNo,
                    @intPM_ID, @dtmMJD_DOJ 
            END 
    END

END
/****** Object:  Trigger [dbo].[trgVoucherIns]    Script Date: 09/16/2016 11:47:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trgVoucherIns] ON [dbo].[MemProfile_Dtls]--Hello
    FOR INSERT
AS
    BEGIN
        DECLARE @memid INT ,
            @tm_SrNo VARCHAR(100) ,
            @tm_Paymode VARCHAR(100) ,
            @PUT_DATE DATETIME ,
            @PUT_DateTime DATETIME ,
            @IPAddr VARCHAR(100) ,
            @Browser VARCHAR(100) ,
            @BrowserVersion VARCHAR(100) ,
            @PUT_SRNO NUMERIC(18, 0) ,
            @PUT_FLAG TINYINT ,
            @PUT_ID AS NUMERIC(18, 0) ,
            @Customerid AS NUMERIC(18, 0) ,
            @Traderid AS NUMERIC(18, 0) ,
            @MemNo AS VARCHAR(100) ;
            
        SELECT  @memid = MPD_MemId ,
                @tm_SrNo = MPD_PinSrNo ,
                @PUT_SRNO = MPD_PinSrNo
        FROM    Inserted;

        SELECT  @tm_Paymode = MJD_Paymode ,
                @PUT_DATE = MJD_DOJ ,
                @PUT_DateTime = MJD_DTOJ ,
                @Customerid = Customerid ,
                @Traderid = Traderid ,
                @MemNo = MJD_MemNo
        FROM    MemJoining_Dtls (NOLOCK)
        WHERE   MJD_MemID = @memid;


        IF ( @tm_Paymode = 1
             OR @tm_Paymode = 8
             OR @tm_Paymode = 9
           ) 
            BEGIN
                SET @PUT_FLAG = 0;

                IF @PUT_SRNO > 0 
                    BEGIN
            
                        IF ISNULL(@Traderid, 0) = 0 
                            BEGIN
                                UPDATE  PinDetails
                                SET     PinDetailsStatus = '2' ,
                                        PinUsedCustomerId = ISNULL(@Customerid,
                                                              0) ,
                                        PinUsedDate = @PUT_DateTime ,
                                        PinUsedTransactionFlag = '25' ,
                                        PinUsedRemark = 'Used for registration of ID ' +CONVERT(VARCHAR,@MemNo) 
                                WHERE   SerialNo = @PUT_SRNO;
                            END;
                        ELSE 
                            BEGIN
                                UPDATE  PinDetails
                                SET     PinDetailsStatus = '2' ,
                                        PinUsedTraderId = ISNULL(@Traderid, 0) ,
                                        PinUsedDate = @PUT_DateTime ,
                                        PinUsedTransactionFlag = '25' ,
                                        PinUsedRemark = 'Used for registration of ID ' +CONVERT(VARCHAR,@MemNo) 
                                WHERE   SerialNo = @PUT_SRNO;
                            END

                    END
            END


        DECLARE @Oh_disamt NUMERIC(18, 2) ,
            @Oh_shipamt NUMERIC(18, 2) ,
            @Oh_netamt NUMERIC(18, 2) ,
            @Oh_Totbvs NUMERIC(18, 2) ,
            @Oh_Totpvs NUMERIC(18, 2) ,
            @Oh_Totunits NUMERIC(18, 2) ,
            @Oh_billpincode VARCHAR(10) ,
            @Oh_billmobile VARCHAR(20) ,
            @Oh_billcontactno VARCHAR(20) ,
            @Oh_paymode NUMERIC(18, 2) ,
            @Oh_chqno NUMERIC(18, 2) ,
            @Oh_pinsrno NUMERIC(18, 2) ,
            @Od_pvcode NUMERIC(18, 2) ,
            @Od_units NUMERIC(18, 2) ,
            @Od_rate NUMERIC(18, 2) ,
            @Od_vat NUMERIC(18, 2) ,
            @Od_mrp NUMERIC(18, 2) ,
            @billname VARCHAR(100) ,
            @billaddr VARCHAR(500) ,
            @billcountry VARCHAR(100) ,
            @billstate VARCHAR(100) ,
            @billcity VARCHAR(100) ,
            @billemail VARCHAR(500) ,
            @OH_ChqDDDate DATETIME ,
            @Oh_chqddbank VARCHAR(500) ,
            @Oh_chqddbranch VARCHAR(500) ,
            @Oh_remark VARCHAR(500) ,
            @OH_DispatchFrID VARCHAR(100) ,
            @OH_DispatchBy VARCHAR(100) ,
            @OH_FrID VARCHAR(100) ,
            @OH_ConfirmFlg NUMERIC(18, 0) ,
            @OH_ConfirmDate VARCHAR(50) ,
            @OH_ReceiptId VARCHAR(50);

        SELECT  @Oh_disamt = PV_DiscAmt ,
                @Oh_shipamt = PV_ShipAmt ,
                @Oh_Totbvs = PV_BVPts ,
                @Oh_Totpvs = PV_PVPts ,
                @Od_units = PV_GrUnits ,
                @Od_rate = PV_SaleRate ,
                @Od_vat = PV_SaleVAT ,
                @Od_mrp = PV_SalePrice
        FROM    PV_MST (NOLOCK)
        WHERE   PV_Code = ( SELECT  MJD_PvCode
                            FROM    MemJoining_Dtls (NOLOCK)
                            WHERE   MJD_MemID = @memid
                          );

        SELECT  @Oh_Totunits = 0 ,
                @Oh_netamt = ( ( @Od_mrp - @Oh_disamt ) + @Oh_shipamt ) ,
                @Oh_remark = 'For Registartion Of Member';

        SELECT  @billname = MPD_Name ,
                @billaddr = MPD_Address ,
                @billcountry = MPD_Country ,
                @billstate = MPD_State ,
                @billcity = MPD_City ,
                @Oh_billpincode = MPD_PinCode ,
                @Oh_billmobile = MPD_Mobile ,
                @Oh_billcontactno = MPD_Mobile ,
                @billemail = MPD_Email ,
                @Oh_pinsrno = MPD_PinSrNo
        FROM    MemProfile_Dtls (NOLOCK)
        WHERE   MPD_MemId = @memid;

        SELECT  @Oh_paymode = MJD_Paymode ,
                @Od_pvcode = MJD_PvCode ,
                @OH_DispatchFrID = ISNULL([MJD_JoinFr], '0')
        FROM    MemJoining_Dtls (NOLOCK)
        WHERE   MJD_MemID = @memid;

    -- these paymodes are confirmed orders otherwise else          
    --1 E-Pin          
    --5 E-Wallet          
    --6 Credit Card          
    --10 Pay Card          
    --11 PayPal          
    --12 Bank Transfer  

        IF ( @Oh_paymode = 1
             OR @Oh_paymode = 5
             OR @Oh_paymode = 6
             OR @Oh_paymode = 10
             OR @Oh_paymode = 11
             OR @Oh_paymode = 12
           ) 
            SELECT  @OH_ConfirmFlg = 1 ,
                    @OH_ConfirmDate = @PUT_DateTime;
        ELSE 
            SELECT  @OH_ConfirmFlg = 0 ,
                    @OH_ConfirmDate = NULL;


        IF ( @Oh_paymode = 4
             OR @Oh_paymode = 2
             OR @Oh_paymode = 3
           ) 
            BEGIN
                SELECT  @Oh_chqno = MPD_DDNo ,
                        @OH_ChqDDDate = MPD_DDDate ,
                        @Oh_chqddbank = MPD_DDBank ,
                        @Oh_chqddbranch = MPD_DDBranch ,
                        @OH_ReceiptId = MPD_Extra21
                FROM    MemProfile_Dtls (NOLOCK)
                WHERE   MPD_MemId = @memid;
            END;

        IF @OH_DispatchFrID = 0 
            SELECT  @OH_DispatchFrID = NULL ,
                    @OH_DispatchBy = 1 ,
                    @OH_FrID = NULL;
        ELSE 
            SELECT  @OH_DispatchFrID = @OH_DispatchFrID ,
                    @OH_DispatchBy = 2 ,
                    @OH_FrID = @OH_DispatchFrID;

        DECLARE @Code VARCHAR(15);
        --EXECUTE prc_GenerateOrderCode @Code OUTPUT, 1;

        DECLARE @stateExists AS TINYINT;
        --EXECUTE prc_UpdateTaxAndShippingChargesForSelectedState_QuickRegt 1,
        --    @Code, @billstate, @stateExists OUTPUT;
    END

/****** Object:  Trigger [dbo].[trg_EwalletBalance]    Script Date: 09/16/2016 11:48:10 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--Create TRIGGER [dbo].[trg_EwalletBalance] ON [dbo].[Ewallet_Trn]
--    FOR INSERT
--AS
--    DECLARE @dblMJD_MemID NUMERIC(18, 0),
--        @ET_ID NUMERIC(18, 0),
--        @ET_Type NUMERIC(18, 0),
--        @Balance NUMERIC(18, 2),
--        @TotCRamt NUMERIC(18, 2),
--        @TotDRamt NUMERIC(18, 2)  
--    BEGIN  
--        SELECT  @ET_ID = ET_ID, @dblMJD_MemID = ET_Memid, @ET_Type = ET_Type
--        FROM    Inserted  

--        SELECT  @Balance = 0  

--        SELECT  @TotCRamt = ISNULL(SUM(ET_Amt), 0)
--        FROM    Ewallet_Trn
--        WHERE   ET_Memid = @dblMJD_MemID
--                AND ET_ID <= @ET_ID
--                AND ET_Flag = 0  

--        SELECT  @TotDRamt = ISNULL(SUM(ET_Amt), 0)
--        FROM    Ewallet_Trn
--        WHERE   ET_Memid = @dblMJD_MemID
--                AND ET_ID <= @ET_ID
--                AND ET_Flag = 1  

--        SELECT  @Balance = @TotCRamt - @TotDRamt  

--        UPDATE  Ewallet_Trn
--        SET     ET_Balance = @Balance
--        WHERE   ET_Memid = @dblMJD_MemID
--                AND ET_Type = @ET_Type
--                AND ET_ID = @ET_ID   
--    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_EwalletBalance] ON [dbo].[Ewallet_Trn]
    FOR INSERT
AS
    DECLARE @dblMJD_MemID NUMERIC(18, 0),
        @ET_ID NUMERIC(18, 0),
        @ET_Type NUMERIC(18, 0),
        @Balance NUMERIC(18, 2),
        @TotCRamt NUMERIC(18, 2),
        @TotDRamt NUMERIC(18, 2)  
    BEGIN  
        SELECT  @ET_ID = ET_ID, @dblMJD_MemID = ET_CustomerId, @ET_Type = ET_Type
        FROM    Inserted  
  
        SELECT  @Balance = 0  
  
        SELECT  @TotCRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    Ewallet_Trn (NOLOCK)
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 0  
  
        SELECT  @TotDRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    Ewallet_Trn (NOLOCK)
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 1  
  
        SELECT  @Balance = @TotCRamt - @TotDRamt  
  
        UPDATE  Ewallet_Trn
        SET     ET_Balance = @Balance
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_Type = @ET_Type
                AND ET_ID = @ET_ID   
    END
