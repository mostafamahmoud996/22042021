CREATE TABLE [dbo].[BulkImportProcess]
(
    [Id] [INT] NOT NULL IDENTITY(1, 1),
    [MemberNo] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [JoiningDateTime] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [SponsorID] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AdjustedToID] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [position] [NVARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Package] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Paymode] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [EpinType] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Gender] [NVARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [FirstName] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [LastName] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DateOfBirth] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Email] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [UserName] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [PanCardNo] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Company] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ST1] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ST2] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ZIP] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [City] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Country] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [state] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Phone] [NVARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [OrgPassword] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Password] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CustomerPassword] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CustomerPasswordSalt] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ReceiptNo] [NVARCHAR](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [BIDocDtlsId] [INT] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
GO
ALTER TABLE [dbo].[BulkImportProcess]
ADD CONSTRAINT [PK_BulkImportProcess]
    PRIMARY KEY CLUSTERED ([Id]) ON [PRIMARY];
GO
CREATE PROC JoiningPlan_MstDetails
AS
BEGIN
    SELECT JM_ID,
           JM_Name,
           JM_Type,
           JM_Weaker,
           JM_Flag,
           JM_decExtra1,
           JM_MultiJoinLogic
    FROM JoiningPlan_Mst (NOLOCK);
END;
GO
CREATE TABLE ImportMemJoiningDtls
(
    [MJD_MemID] [NUMERIC](18, 0) NULL,
    [MJD_MemNo] [VARCHAR](50) NULL,
    [MJD_DOJ] [SMALLDATETIME] NULL,
    [MJD_DTOJ] [DATETIME] NULL,
    [MJD_Introducer] [NUMERIC](18, 0) NULL,
    [MJD_AdjustedTo] [NUMERIC](18, 0) NULL,
    [MJD_LR] [VARCHAR](50) NULL
);
GO
CREATE TABLE ImportDOJDetails
(
    RowID INT IDENTITY(1, 1) PRIMARY KEY,
    [MemNo] [VARCHAR](50) NULL,
    [DTOJ] [DATETIME] NULL,
    [Introducer] [VARCHAR](50) NULL,
    [AdjustedTo] [VARCHAR](50) NULL,
    [position] [VARCHAR](50) NULL,
    [Package] [VARCHAR](500) NULL
);
GO
CREATE PROCEDURE [Prc_Import_Val_Duplicate_Member]
    @PlanType VARCHAR(50),
    @ErrorMsg NVARCHAR(MAX) OUT
AS
BEGIN
    SET NOCOUNT ON;
    SET @ErrorMsg = '';

    --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '') + 'Duplicate Member ID '  
    --        + MemberNo + ','  
    --FROM    BulkImportProcess (NOLOCK)  
    --GROUP BY MemberNo  
    --HAVING  COUNT(Id) > 1;        

    SELECT @ErrorMsg = STUFF(
                       (
                           SELECT ' ' + 'Duplicate Member ID ' + MemberNo + ','
                           FROM BulkImportProcess (NOLOCK)
                           GROUP BY MemberNo
                           HAVING COUNT(Id) > 1
                           FOR XML PATH('')
                       ),
                       1,
                       1,
                       ''
                            );


    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));

    --IF (ISNULL(@ErrorMsg, '') != '')        
    --BEGIN        
    --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1);        
    --END;        


    --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
    --        + 'Duplicate Member ID in database ' + MemberNo + ','  
    --FROM    BulkImportProcess (NOLOCK)  
    --        INNER JOIN MemJoining_Dtls (NOLOCK) ON MemberNo = MJD_MemNo  
    --WHERE   MJD_MemID > 1;        


    SELECT @ErrorMsg = STUFF(
                       (
                           SELECT ' ' + 'Duplicate Member ID in database ' + MemberNo + ','
                           FROM BulkImportProcess (NOLOCK)
                               INNER JOIN MemJoining_Dtls (NOLOCK)
                                   ON MemberNo = MJD_MemNo
                           WHERE MJD_MemID > 1
                           FOR XML PATH('')
                       ),
                       1,
                       1,
                       ''
                            );

    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));


    IF (ISNULL(RIGHT(@ErrorMsg, 1), '') = ',')
    BEGIN
        SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1);

    END;


END;
GO
CREATE PROCEDURE Prc_Import_Val_Duplicate_UserName
    @PlanType VARCHAR(50),
    @ErrorMsg NVARCHAR(MAX) OUT
AS
BEGIN
    SET NOCOUNT ON;
    SET @ErrorMsg = '';

    --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '') + ' Duplicate UserName  '  
    --        + UserName + ','  
    --FROM    BulkImportProcess (NOLOCK)  
    --GROUP BY UserName  
    --HAVING  COUNT(Id) > 1;          


    SELECT @ErrorMsg = STUFF(
                       (
                           SELECT ' ' + ' Duplicate UserName  ' + UserName + ','
                           FROM BulkImportProcess (NOLOCK)
                           GROUP BY UserName
                           HAVING COUNT(Id) > 1
                           FOR XML PATH('')
                       ),
                       1,
                       1,
                       ''
                            );



    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));




    --IF (ISNULL(@ErrorMsg, '') != '')          
    --BEGIN          

    --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1);          
    --END;          


    --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
    --        + ' Duplicate UserName in database '  
    --        + BulkImportProcess.Username + ','  
    --FROM    BulkImportProcess (NOLOCK)  
    --        INNER JOIN Customer (NOLOCK) ON Customer.Username = BulkImportProcess.UserName  
    --WHERE   MemId > 1;          


    SELECT @ErrorMsg = STUFF(
                       (
                           SELECT ' ' + ' Duplicate UserName in database ' + BulkImportProcess.UserName + ','
                           FROM BulkImportProcess (NOLOCK)
                               INNER JOIN Customer (NOLOCK)
                                   ON Customer.UserName = BulkImportProcess.UserName
                           WHERE MemId > 1
                           FOR XML PATH('')
                       ),
                       1,
                       1,
                       ''
                            );



    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));


    IF (ISNULL(RIGHT(@ErrorMsg, 1), '') = ',')
    BEGIN
        SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1);

    END;
END;
GO
CREATE PROCEDURE Prc_Import_Val_SponsorID_Exist -- Prc_Import_Val_SponsorID_Exist 'hjas',''     
    @PlanType VARCHAR(50),
    @ErrorMsg NVARCHAR(MAX) OUT
AS
BEGIN
    SET NOCOUNT ON;
    SET @ErrorMsg = '';

    IF OBJECT_ID('tempdb..#TempValSponsorID', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempValSponsorID;
    END;

    CREATE TABLE #TempValSponsorID
    (
        [MemNo] [VARCHAR](50) NULL,
        [Introducer] [VARCHAR](50) NULL,
    );

    -- Check Sponsor != memno      

    --SELECT @ErrorMsg = COALESCE(@ErrorMsg, '') + 'Member ID  and Sponsor ID are same for Member ID ' + MemNo + ','    
    --FROM ImportDOJDetails (NOLOCK)    
    --WHERE MemNo = Introducer;    

    SELECT @ErrorMsg = STUFF(
                       (
                           SELECT ' ' + 'Member ID  and Sponsor ID are same for Member ID ' + MemNo + ','
                           FROM ImportDOJDetails (NOLOCK)
                           WHERE MemNo = Introducer
                           FOR XML PATH('')
                       ),
                       1,
                       1,
                       ''
                            );



    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));




    -- First check sponsor present in Excel or Not                    

    INSERT INTO #TempValSponsorID
    SELECT T1.MemNo,
           T1.Introducer
    FROM ImportDOJDetails T1 (NOLOCK)
        LEFT OUTER JOIN ImportDOJDetails T2 (NOLOCK)
            ON T1.Introducer = T2.MemNo
    WHERE T1.Introducer <> '0'
          AND T2.MemNo IS NULL;


    --SELECT @ErrorMsg    
    --    = COALESCE(@ErrorMsg, '') + 'Specified Sponsor ID ' + Introducer + ' of Member ID ' + MemNo    
    --      + ' is not a Member.' + ','    
    --FROM #TempValSponsorID    
    --WHERE Introducer NOT IN (    
    --                            SELECT MJD_MemNo FROM ImportMemJoiningDtls (NOLOCK)    
    --                        );    



    SELECT @ErrorMsg
        = STUFF(
          (
              SELECT ' ' + 'Specified Sponsor ID ' + Introducer + ' of Member ID ' + MemNo + ' is not a Member.' + ','
              FROM #TempValSponsorID
              WHERE Introducer NOT IN (
                                          SELECT MJD_MemNo FROM ImportMemJoiningDtls (NOLOCK)
                                      )
              FOR XML PATH('')
          ),
          1,
          1,
          ''
               );



    SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));




    IF OBJECT_ID('tempdb..#TempValSponsorID', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempValSponsorID;
    END;

    IF (ISNULL(RIGHT(@ErrorMsg, 1), '') = ',')
    BEGIN
        SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1);

    END;


END;
GO
CREATE VIEW vw_Import_Val_PositionChk
AS
SELECT TM_adjustedtomemnobdt,
       tm_lrbdt
FROM
(
    SELECT AdjustedTo AS TM_adjustedtomemnobdt,
           position AS tm_lrbdt
    FROM ImportDOJDetails (NOLOCK)
    WHERE AdjustedTo != '0'
    UNION ALL
    SELECT adj.MJD_MemNo AS TM_adjustedtomemnobdt,
           mem.MJD_LR AS tm_lrbdt
    FROM MemJoining_Dtls (NOLOCK) AS mem
        INNER JOIN MemJoining_Dtls (NOLOCK) AS adj
            ON mem.MJD_AdjustedTo = adj.MJD_MemID
    WHERE mem.MJD_AdjustedTo != 0
) AdjPosition
GROUP BY TM_adjustedtomemnobdt,
         tm_lrbdt
HAVING COUNT(*) > 1;
GO
CREATE PROCEDURE Prc_Import_Val_wrtAdjustedToID -- Prc_Import_Val_wrtAdjustedToID 'sdf',   ''  
    @PlanType VARCHAR(50),
    @ErrorMsg NVARCHAR(MAX) OUT
AS
BEGIN
    SET NOCOUNT ON;
    SET @ErrorMsg = '';

    IF @PlanType <> 'Generation'
    BEGIN
        -- ********* Chk AdjustedTo ID exist                

        -- Check AdjID  != memno        

        --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
        --        + 'Member ID  and AdjustedTo ID are same for Member ID '  
        --        + MemNo + ','  
        --FROM    ImportDOJDetails (NOLOCK)  
        --WHERE   MemNo = AdjustedTo;    



        SELECT @ErrorMsg = STUFF(
                           (
                               SELECT ' ' + 'Member ID  and AdjustedTo ID are same for Member ID ' + MemNo + ','
                               FROM ImportDOJDetails (NOLOCK)
                               WHERE MemNo = AdjustedTo
                               FOR XML PATH('')
                           ),
                           1,
                           1,
                           ''
                                );

        SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));

        IF OBJECT_ID('tempdb..#TempValAdustedID', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempValAdustedID;
        END;

        CREATE TABLE #TempValAdustedID
        (
            [MemNo] [VARCHAR](50) NULL,
            [Introducer] [VARCHAR](50) NULL,
        );

        -- First check AdjustedTo ID present in Excel or Not                          

        INSERT INTO #TempValAdustedID
        SELECT T1.MemNo,
               T1.AdjustedTo
        FROM ImportDOJDetails T1 (NOLOCK)
            LEFT OUTER JOIN ImportDOJDetails T2 (NOLOCK)
                ON T1.AdjustedTo = T2.MemNo
        WHERE T1.AdjustedTo <> '0'
              AND T2.MemNo IS NULL;

        --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
        --        + 'Specified Adjusted To ID not exist in excel and database '  
        --        + T1.Introducer + ' of Member ID ' + T1.MemNo  
        --        + ' is not a Member.' + ','  
        --FROM    #TempValAdustedID T1 ( NOLOCK )  
        --WHERE   Introducer NOT IN (  
        --        SELECT  MJD_MemNo  
        --        FROM    ImportMemJoiningDtls (NOLOCK) );    



        SELECT @ErrorMsg
            = STUFF(
              (
                  SELECT ' ' + 'Specified Adjusted To ID not exist in excel and database ' + T1.Introducer
                         + ' of Member ID ' + T1.MemNo + ' is not a Member.' + ','
                  FROM #TempValAdustedID T1 (NOLOCK)
                  WHERE Introducer NOT IN (
                                              SELECT MJD_MemNo FROM ImportMemJoiningDtls (NOLOCK)
                                          )
                  FOR XML PATH('')
              ),
              1,
              1,
              ''
                   );

        SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));



        --IF (ISNULL(@ErrorMsg, '') != '')          
        --BEGIN          

        --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1) + '</Br>';          
        --END;          


        --  ********* Chk AdjustedTo ID exist  before Member                      
        --SELECT @ErrorMsg              
        --    = COALESCE(@ErrorMsg, '') + 'Specified Adjusted To ID ' + T1.AdjustedTo + ' of Member ID ' + T1.MemNo              
        --      + '  is not exists before in the excel sheet. Adjusted To ID of Member ID must come first in excel sheet.'              
        --      + ','              
        --FROM ImportDOJDetails T1 (NOLOCK)              
        --    LEFT OUTER JOIN ImportDOJDetails T2 (NOLOCK)              
        --        ON T1.AdjustedTo = T2.MemNo              
        --WHERE T1.AdjustedTo <> '0'              
        --      AND T2.RowID >= T1.RowID;              



        --IF (ISNULL(@ErrorMsg, '') != '')              
        --BEGIN              

        --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1) + '</Br>';              
        --END;              

        -- *********  Chk AdjustedTo ID placements                   
        -- Chk Adj full filled in DB            
        -- Chk Adj 1 Filled in DB w.r.t Position            
        -- Chk Adj in excel            


        -- Adj Used multiple time in excel            

        --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
        --        + 'Adjusted To ID ' + AdjustedTo + ' of Member ID '  
        --        + MemNo + ' is pointing to more than two Member ID'  
        --        + ','  
        --FROM    ImportDOJDetails (NOLOCK)  
        --WHERE   AdjustedTo IN ( SELECT  AdjustedTo  
        --                        FROM    ImportDOJDetails (NOLOCK)  
        --                        GROUP BY AdjustedTo  
        --                        HAVING  COUNT(*) > 2 );    


        SELECT @ErrorMsg
            = STUFF(
              (
                  SELECT ' ' + 'Adjusted To ID ' + AdjustedTo + ' of Member ID ' + MemNo
                         + ' is pointing to more than two Member ID' + ','
                  FROM ImportDOJDetails (NOLOCK)
                  WHERE AdjustedTo IN (
                                          SELECT AdjustedTo
                                          FROM ImportDOJDetails (NOLOCK)
                                          GROUP BY AdjustedTo
                                          HAVING COUNT(*) > 2
                                      )
                  FOR XML PATH('')
              ),
              1,
              1,
              ''
                   );

        SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));
        --IF (ISNULL(@ErrorMsg, '') != '')          
        --BEGIN          

        --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1) + '</Br>';          
        --END;          

        -- *** END Adj Used multiple time in excel            



        --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
        --        + 'Adjusted To ID ' + AdjustedTo + ' of Member ID '  
        --        + MemNo + ' is pointing to '  
        --        + ( CASE position  
        --              WHEN 'L' THEN 'Left'  
        --              WHEN 'l' THEN 'Left'  
        --              WHEN 'R' THEN 'Right'  
        --              WHEN 'r' THEN 'Right'  
        --              ELSE position  
        --            END ) + ' position  to more than one Member ID.'  
        --        + ','  
        --FROM    ImportDOJDetails (NOLOCK) ,  
        --        vw_Import_Val_PositionChk (NOLOCK)  
        --WHERE   AdjustedTo = TM_adjustedtomemnobdt  
        --        AND position = tm_lrbdt;    


        SELECT @ErrorMsg
            = STUFF(
              (
                  SELECT ' ' + 'Adjusted To ID ' + AdjustedTo + ' of Member ID ' + MemNo + ' is pointing to '
                         + (CASE position
                                WHEN 'L' THEN
                                    'Left'
                                WHEN 'l' THEN
                                    'Left'
                                WHEN 'R' THEN
                                    'Right'
                                WHEN 'r' THEN
                                    'Right'
                                ELSE
                                    position
                            END
                           ) + ' position  to more than one Member ID.' + ','
                  FROM ImportDOJDetails (NOLOCK),
                       vw_Import_Val_PositionChk (NOLOCK)
                  WHERE AdjustedTo = TM_adjustedtomemnobdt
                        AND position = tm_lrbdt
                  FOR XML PATH('')
              ),
              1,
              1,
              ''
                   );



        SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));
        --IF (ISNULL(@ErrorMsg, '') != '')          
        --BEGIN          

        --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1) + '</Br>';          
        --END;          


        ---- Chk For Member DOJ must be less then or equal to adjustedTo ID OR SponsorID                      

        --IF OBJECT_ID('tempdb..#TempSponAdjDOJDetails', 'U') IS NOT NULL              
        --BEGIN              
        --    DROP TABLE #TempSponAdjDOJDetails;              
        --END;              

        --CREATE TABLE #TempSponAdjDOJDetails              
        --(              
        --    [MJD_MemNo] [VARCHAR](50) NULL,              
        --    [MJD_DTOJ] [DATETIME] NULL,              
        --    [MJD_AdjustedTo] [VARCHAR](50) NULL,              
        --    [MJD_AdjDTOJ] [DATETIME] NULL,              
        --    [MJD_SponDTOJ] [DATETIME] NULL              
        --);              

        --INSERT INTO #TempSponAdjDOJDetails              
        --SELECT CE.MemNo,              
        --       CE.DTOJ,              
        --       AdjmemNo,              
        --       AdjDTOJ,              
        --       Spon.DTOJ AS SponDTOJ              
        --FROM              
        --(              
        --    SELECT Mem.MemNo,              
        --           Mem.DTOJ,              
        --           Mem.Introducer,              
        --           Adj.MemNo AS AdjmemNo,              
        --           Adj.DTOJ AS AdjDTOJ              
        --    FROM ImportDOJDetails (NOLOCK) AS Mem              
        --        INNER JOIN ImportDOJDetails (NOLOCK) AS Adj              
        --            ON Mem.AdjustedTo = Adj.MemNo              
        --) CE              
        --    INNER JOIN ImportDOJDetails (NOLOCK) AS Spon              
        --        ON CE.Introducer = Spon.MemNo              
        --WHERE (              
        --          CE.DTOJ < Spon.DTOJ              
        --          OR CE.DTOJ < AdjDTOJ              
        --      );      




        --SELECT @ErrorMsg              
        --    = COALESCE(@ErrorMsg, '') + 'DOJ of Member ID ' + MJD_MemNo + ' is before the AdjustedTo ID '              
        --      + MJD_AdjustedTo + ' , '              
        --FROM #TempSponAdjDOJDetails              
        --WHERE (              
        --          MJD_DTOJ < MJD_SponDTOJ              
        --          OR MJD_DTOJ < MJD_AdjDTOJ              
        --      );              


        --IF OBJECT_ID('tempdb..#TempSponAdjDOJDetails', 'U') IS NOT NULL              
        --BEGIN              
        --    DROP TABLE #TempSponAdjDOJDetails;              
        --END;              

        --IF (ISNULL(@ErrorMsg, '') != '')              
        --BEGIN              

        --    SET @ErrorMsg = SUBSTRING(@ErrorMsg, 0, LEN(@ErrorMsg) - 1) + '</Br>';              
        --END;              

        --Chk for all position already filled in memjoining                           

        --SELECT  @ErrorMsg = COALESCE(@ErrorMsg, '')  
        --        + ' All position are already filled in Database  for AdjustedTo ID '  
        --        + AdjustedTo + ','  
        --FROM    ImportDOJDetails (NOLOCK)  
        --WHERE   AdjustedTo IN (  
        --        SELECT  adj.MJD_MemNo  
        --        FROM    ImportMemJoiningDtls (NOLOCK) AS mem  
        --                INNER JOIN MemJoining_Dtls (NOLOCK) AS adj ON mem.MJD_AdjustedTo = adj.MJD_MemID  
        --        GROUP BY adj.MJD_MemNo  
        --        HAVING  COUNT(adj.MJD_MemNo) > 1 );    



        SELECT @ErrorMsg
            = STUFF(
              (
                  SELECT ' ' + ' All position are already filled in Database  for AdjustedTo ID ' + AdjustedTo + ','
                  FROM ImportDOJDetails (NOLOCK)
                  WHERE AdjustedTo IN (
                                          SELECT adj.MJD_MemNo
                                          FROM ImportMemJoiningDtls (NOLOCK) AS mem
                                              INNER JOIN MemJoining_Dtls (NOLOCK) AS adj
                                                  ON mem.MJD_AdjustedTo = adj.MJD_MemID
                                          GROUP BY adj.MJD_MemNo
                                          HAVING COUNT(adj.MJD_MemNo) > 1
                                      )
                  FOR XML PATH('')
              ),
              1,
              1,
              ''
                   );




        SELECT @ErrorMsg = LTRIM(RTRIM(@ErrorMsg));

        IF (ISNULL(RIGHT(@ErrorMsg, 1), '') = ',')
        BEGIN
            SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1);

        END;

    END;

END;
GO
CREATE PROCEDURE Prc_Import_Val_SponserExistInUpline -- Prc_Import_Val_SponserExistInUpline 'Binary',''                   
    @PlanType VARCHAR(50),
    @ErrorMsg NVARCHAR(MAX) OUT
AS
BEGIN

    SET NOCOUNT ON;

    SET @ErrorMsg = '';

    IF @PlanType <> 'Generation'
    BEGIN


        DECLARE @iCnt INT;
        DECLARE @noRows INT;
        DECLARE @memno VARCHAR(15);
        DECLARE @introducermemno VARCHAR(15);
        DECLARE @sponsormemno VARCHAR(15);
        DECLARE @introducerdoj DATETIME;
        DECLARE @tmpmemno VARCHAR(15);
        DECLARE @tmpuplineflg TINYINT;
        DECLARE @AdjCountFlag INT;

        IF OBJECT_ID('tempdb..#TempChkAdjustedToDOJ', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempChkAdjustedToDOJ;
        END;

        CREATE TABLE #TempChkAdjustedToDOJ
        (
            RowID INT IDENTITY(1, 1) PRIMARY KEY,
            memno VARCHAR(50),
            adjustedtomemno VARCHAR(50),
            sponNo VARCHAR(50)
        );

        IF OBJECT_ID('tempdb..#TempChkAdjustedToDOJDB', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempChkAdjustedToDOJDB;
        END;

        CREATE TABLE #TempChkAdjustedToDOJDB
        (
            RowID INT IDENTITY(1, 1) PRIMARY KEY,
            memno VARCHAR(15),
            adjustedtomemno VARCHAR(15),
            sponNo VARCHAR(15)
        );

        INSERT INTO #TempChkAdjustedToDOJ
        SELECT MemNo,
               AdjustedTo AS adjNo,
               Introducer AS sponNo
        FROM ImportDOJDetails (NOLOCK)
        ORDER BY RowID;

        ---This statement must be immediately after "INSERT INTO #Temp" statement                                
        SET @noRows = @@ROWCOUNT;
        SELECT @iCnt = 1;

        WHILE @iCnt <= @noRows
        BEGIN

            SELECT @memno = memno,
                   @sponsormemno = adjustedtomemno,
                   @introducermemno = sponNo
            FROM #TempChkAdjustedToDOJ
            WHERE RowID = @iCnt;

            SELECT @tmpmemno = @sponsormemno;
            SELECT @tmpuplineflg = 0;

            WHILE @sponsormemno <> '0'
            BEGIN
                -- PRINT ( @introducermemno + ' =  '   + @sponsormemno );                  
                IF @introducermemno = @sponsormemno
                BEGIN
                    SELECT @tmpuplineflg = 1;
                    BREAK;
                END;
                SELECT @AdjCountFlag = 0;

                SELECT @AdjCountFlag = COUNT(AdjustedTo)
                FROM ImportDOJDetails (NOLOCK)
                WHERE MemNo = @tmpmemno;

                IF (@AdjCountFlag > 1)
                BEGIN
                    SELECT @sponsormemno = AdjustedTo
                    FROM ImportDOJDetails (NOLOCK)
                    WHERE MemNo = @tmpmemno;

                END;
                ELSE
                BEGIN
                    SELECT @sponsormemno = '0';
                    SELECT @tmpuplineflg = 1;
                    BREAK;
                END;


                SELECT @tmpmemno = @sponsormemno;
            END;
            IF @tmpuplineflg = 0
               AND @introducermemno <> '0'
            BEGIN

                INSERT INTO #TempChkAdjustedToDOJDB
                (
                    memno,
                    adjustedtomemno,
                    sponNo
                )
                VALUES
                (   @memno,          -- memno - varchar(15)            
                    @sponsormemno,   -- adjustedtomemno - varchar(15)            
                    @introducermemno -- sponNo - varchar(15)            

                    );


            --SELECT @ErrorMsg          
            --    = COALESCE(@ErrorMsg, '') + 'Sponsor ID ' + @introducermemno + ' of Member ID ' + @memno          
            --      + ' is not exists in upline.' + ',';          
            END;

            SELECT @iCnt = @iCnt + 1;
        END;


        -- For DB validation            
        SET @noRows = 0;
        SELECT @noRows = COUNT(memno)
        FROM #TempChkAdjustedToDOJDB (NOLOCK);

        SELECT @iCnt = 1;

        WHILE @iCnt <= @noRows
        BEGIN

            SELECT @memno = memno,
                   @sponsormemno = adjustedtomemno,
                   @introducermemno = sponNo
            FROM #TempChkAdjustedToDOJDB
            WHERE RowID = @iCnt;

            SELECT @tmpmemno = @sponsormemno;
            SELECT @tmpuplineflg = 0;

            WHILE @sponsormemno <> '0'
            BEGIN
                -- PRINT ( @introducermemno + ' =  '   + @sponsormemno );                  
                IF @introducermemno = @sponsormemno
                BEGIN
                    SELECT @tmpuplineflg = 1;
                    BREAK;
                END;
                SELECT @AdjCountFlag = 0;

                SELECT @AdjCountFlag = COUNT(MJD_AdjustedTo)
                FROM ImportMemJoiningDtls (NOLOCK)
                WHERE MJD_MemNo = @tmpmemno;

                IF (@AdjCountFlag > 1)
                BEGIN
                    SELECT @sponsormemno = MJD_AdjustedTo
                    FROM ImportMemJoiningDtls (NOLOCK)
                    WHERE MJD_MemNo = @tmpmemno;

                END;
                ELSE
                BEGIN
                    SELECT @sponsormemno = '0';
                    SELECT @tmpuplineflg = 1;
                    BREAK;
                END;


                SELECT @tmpmemno = @sponsormemno;
            END;
            IF @tmpuplineflg = 0
               AND @introducermemno <> '0'
            BEGIN


                SELECT @ErrorMsg
                    = COALESCE(@ErrorMsg, '') + 'Sponsor ID ' + @introducermemno + ' of Member ID ' + @memno
                      + ' is not exists in upline.' + ',';
            END;

            SELECT @iCnt = @iCnt + 1;
        END;



        --************* Check Sponsor exist in AdjustedTo Upline    

        DECLARE @SponsorNotFlg TINYINT,
                @SpRowCnt NUMERIC(18, 0),
                @MainSponsor NUMERIC(18, 0),
                @AdjID NUMERIC(18, 0),
                @CompSponsor NUMERIC(18, 0),
                @AdjCntFlg NUMERIC(18, 0);

        SET @SponsorNotFlg = 1;
        IF OBJECT_ID('tempdb..#TempChkSponsorInAdjUpline', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempChkSponsorInAdjUpline;
        END;

        CREATE TABLE #TempChkSponsorInAdjUpline
        (
            RowID INT IDENTITY(1, 1) PRIMARY KEY,
            memno VARCHAR(50),
            downmemno VARCHAR(50),
            lev NUMERIC(18, 0)
        );


        INSERT INTO #TempChkSponsorInAdjUpline
        (
            memno,
            downmemno,
            lev
        )
        SELECT AdjustedTo,
               MemNo,
               1
        FROM ImportDOJDetails (NOLOCK);



        DECLARE @LevelCount NUMERIC(18, 0),
                @RowCount NUMERIC(18, 0);

        SET @LevelCount = 1;
        SET @RowCount = 0;

        WHILE (@LevelCount < 999999)
        BEGIN
            SET @RowCount = 0;

            INSERT INTO #TempChkSponsorInAdjUpline
            (
                memno,
                downmemno,
                lev
            )
            SELECT #TempChkSponsorInAdjUpline.memno,
                   ImportDOJDetails.MemNo,
                   lev + 1
            FROM #TempChkSponsorInAdjUpline WITH (NOLOCK)
                INNER JOIN ImportDOJDetails (NOLOCK)
                    ON AdjustedTo = downmemno
            WHERE lev = @LevelCount;


            SET @RowCount = @@ROWCOUNT;

            IF @RowCount <= 0
            BEGIN

                BREAK;
            END;

            SET @LevelCount = @LevelCount + 1;

        END;


        IF OBJECT_ID('tempdb..#TempChkSponsorInExcel', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempChkSponsorInExcel;
        END;

        CREATE TABLE #TempChkSponsorInExcel
        (
            RowID INT IDENTITY(1, 1) PRIMARY KEY,
            memno VARCHAR(50),
            Introducer VARCHAR(50),
            AdjustedTo VARCHAR(50)
        );

        INSERT INTO #TempChkSponsorInExcel
        SELECT DISTINCT
               MemNo,
               Introducer,
               AdjustedTo
        FROM ImportDOJDetails (NOLOCK)
        WHERE 0 =
        (
            SELECT COUNT(memno)
            FROM #TempChkSponsorInAdjUpline
            WHERE memno = ImportDOJDetails.Introducer
                  AND downmemno = ImportDOJDetails.AdjustedTo
        )
              AND ImportDOJDetails.AdjustedTo != ImportDOJDetails.Introducer;

        --SELECT DISTINCT  
        --        ImportDOJDetails.MemNo ,  
        --        ImportDOJDetails.Introducer ,  
        --        AdjustedTo  
        --FROM    ImportDOJDetails  (NOLOCK)  
        --        LEFT OUTER   JOIN #TempChkSponsorInAdjUpline (NOLOCK) ON ImportDOJDetails.AdjustedTo = #TempChkSponsorInAdjUpline.downmemno  
        --                                      AND #TempChkSponsorInAdjUpline.memno != ImportDOJDetails.Introducer  
        --WHERE   ImportDOJDetails.AdjustedTo != ImportDOJDetails.Introducer    



        IF OBJECT_ID('tempdb..#TempChkSponsorInDB', 'U') IS NOT NULL
        BEGIN
            DROP TABLE #TempChkSponsorInDB;
        END;

        CREATE TABLE #TempChkSponsorInDB
        (
            RowID INT IDENTITY(1, 1) PRIMARY KEY,
            IntroducerID VARCHAR(50),
            DownID VARCHAR(50)
        );

        INSERT INTO #TempChkSponsorInDB
        SELECT spon.MJD_MemNo,
               down.MJD_MemNo
        FROM IDWiseDownline_Dtls (NOLOCK)
            INNER JOIN MemJoining_Dtls AS spon (NOLOCK)
                ON spon.MJD_MemID = IDD_MEMID
            INNER JOIN MemJoining_Dtls AS down (NOLOCK)
                ON down.MJD_MemID = IDD_DOWNID;

        DECLARE @ErrorInnerMsg NVARCHAR(MAX);

        SELECT @ErrorInnerMsg
            = STUFF(
              (
                  SELECT ' ' + 'Sponsor ID' + Introducer + ' not exist in AdjustedTo ID' + AdjustedTo
                         + ' upline for Member ID ' + memno + ','
                  FROM #TempChkSponsorInExcel (NOLOCK)
                      LEFT OUTER JOIN #TempChkSponsorInDB
                          ON #TempChkSponsorInExcel.AdjustedTo = #TempChkSponsorInDB.DownID
                  --LEFT OUTER JOIN IDWiseDownline_Dtls  (NOLOCK) ON #TempChkSponsorInExcel.AdjustedTo = IDWiseDownline_Dtls.IDD_DOWNID    
                  --      AND IDWiseDownline_Dtls.IDD_MEMID != #TempChkSponsorInExcel.Introducer    
                  FOR XML PATH('')
              ),
              1,
              1,
              ''
                   );



        SELECT @ErrorMsg = @ErrorMsg + LTRIM(RTRIM(ISNULL(@ErrorInnerMsg, '')));

    --********************************     

    -- *** END Check Sponsor exist in AdjustedTo Upline    

    END;


    IF OBJECT_ID('tempdb..#TempChkAdjustedToDOJ', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempChkAdjustedToDOJ;
    END;


    IF OBJECT_ID('tempdb..#TempChkAdjustedToDOJDB', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempChkAdjustedToDOJDB;
    END;


    IF OBJECT_ID('tempdb..#TempChkSponsorInExcel', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempChkSponsorInExcel;
    END;

    IF OBJECT_ID('tempdb..#TempChkSponsorInAdjUpline', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempChkSponsorInAdjUpline;
    END;

    IF OBJECT_ID('tempdb..#TempChkSponsorInDB', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempChkSponsorInDB;
    END;



    IF (ISNULL(RIGHT(@ErrorMsg, 1), '') = ',')
    BEGIN
        SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1);

    END;

    PRINT (@ErrorMsg);

END;
GO
CREATE PROCEDURE Prc_Import_Val_Package -- Prc_Import_Val_Package 'Binary',''            
    @PlanType VARCHAR(50) ,  
    @ErrorMsg NVARCHAR(MAX) OUT  
AS   
    BEGIN        
        SET NOCOUNT ON;        
        SET @ErrorMsg = '';        
        DECLARE @ErrorInnerMsg NVARCHAR(MAX)     
    --SELECT @ErrorMsg = COALESCE(@ErrorMsg, '') + 'Invalid Package For Member ID ' + MemNo + ','        
    --FROM ImportDOJDetails (NOLOCK)        
    --    LEFT OUTER JOIN PV_MST (NOLOCK)        
    --        ON Package = PV_PDCode        
    --WHERE PV_Code IS NULL;        
          
          
        SELECT  @ErrorInnerMsg = STUFF(( SELECT ' '  
                                                + 'Invalid Package For Member ID '  
                                                + MemNo + ','  
                                         FROM   ImportDOJDetails (NOLOCK)  
                                                LEFT OUTER JOIN PV_MST (NOLOCK) ON Package = PV_PDCode  
                                         WHERE  PV_Code IS NULL  
                                       FOR  
                                         XML PATH('')  
                                       ), 1, 1, '')      
    
    
    
       
        SELECT  @ErrorMsg = @ErrorMsg + LTRIM(RTRIM(ISNULL(@ErrorInnerMsg, '')))     
    
    
    
          
        IF ( ISNULL(RIGHT(@ErrorMsg, 1), '') = ',' )   
            BEGIN        
                SET @ErrorMsg = LEFT(@ErrorMsg, LEN(@ErrorMsg) - 1)         
        
            END;        
          
    END;
GO
CREATE TABLE ImportRegistrationBulkDatatable
(
    MJD_MemNo NVARCHAR(MAX) NULL,
    MJD_Introducer NVARCHAR(MAX) NULL,
    MJD_AdjustedTo NVARCHAR(MAX) NULL,
    MJD_LR NVARCHAR(MAX) NULL,
    MJD_PvCode NUMERIC(18, 0) NULL,
    MJD_PvType NUMERIC(18, 0) NULL,
    MJD_bv NUMERIC(18, 2) NULL,
    MJD_Pv NUMERIC(18, 2) NULL,
    MJD_pkgbv NUMERIC(18, 2) NULL,
    MJD_pkgPv NUMERIC(18, 2) NULL,
    MJD_Paymode NUMERIC(18, 0) NULL,
    MJD_PayType VARCHAR(50) NULL,
    MJD_Multiplecnt NUMERIC(18, 0) NULL,
    MJD_MasterID NUMERIC(18, 0) NULL,
    MJD_DTOJ VARCHAR(50) NULL,
    MJD_DOJ VARCHAR(50) NULL,
    MJD_RoyIntroducer NUMERIC(18, 0) NULL,
    MJD_RoyAdjustedTo NUMERIC(18, 0) NULL,
    MJD_CCTrnAmt NUMERIC(18, 2) NULL,
    MJD_TrnTotalAmt NUMERIC(18, 2) NULL,
    MPD_DDBank VARCHAR(100) NULL,
    MPD_DDBranch VARCHAR(100) NULL,
    MPD_DDNo NUMERIC(18, 0) NULL,
    MPD_DDDate VARCHAR(50) NULL,
    MJD_Amount NUMERIC(18, 2) NULL,
    MPD_Title VARCHAR(10) NULL,
    MPD_Name VARCHAR(200) NULL,
    MPD_Mobile VARCHAR(20) NULL,
    MPD_Email VARCHAR(50) NULL,
    MPD_PanCardNo VARCHAR(50) NULL,
    MPD_ApplyPan VARCHAR(50) NULL,
    MPD_ChqPayTo VARCHAR(100) NULL,
    MPD_Bank VARCHAR(30) NULL,
    MPD_Branch VARCHAR(50) NULL,
    MPD_BankAddress VARCHAR(400) NULL,
    MPD_AccType VARCHAR(16) NULL,
    MPD_AccNo VARCHAR(25) NULL,
    MPD_AccPwd VARCHAR(16) NULL,
    MPD_IFSDCode VARCHAR(20) NULL,
    MPD_FirmType VARCHAR(100) NULL,
    MPD_FirmDetail VARCHAR(100) NULL,
    MPD_NoDepend NUMERIC(18, 0) NULL,
    MPD_ShpPinCode VARCHAR(100) NULL,
    MPD_PinSrNo VARCHAR(50) NULL,
    MPD_ReceiptNo VARCHAR(50) NULL,
    MLD_Login VARCHAR(50) NULL,
    MLD_pwd VARCHAR(50) NULL,
    MPD_Gender VARCHAR(50) NULL,
    MPD_IPAddress VARCHAR(50) NULL,
    MPD_Browser VARCHAR(50) NULL,
    MPD_BrowserVersion VARCHAR(50) NULL,
    OH_FRID NUMERIC(18, 0) NULL,
    OH_DispatchFrID NUMERIC(18, 0) NULL,
    OH_DispatchBy NUMERIC(18, 0) NULL,
    MPD_Extra17 VARCHAR(100) NULL,
    MPD_Extra18 VARCHAR(100) NULL,
    MPD_PayReceiptMode NUMERIC(18, 0) NULL,
    MPD_EWTransfer DECIMAL(18, 0) NULL,
    CurrDate DATETIME NULL,
    CurrDateTime DATETIME NULL,
    MPD_FirstName VARCHAR(100) NULL,
    MPD_MiddleName VARCHAR(100) NULL,
    MPD_LastName VARCHAR(100) NULL,
    MPD_nomName VARCHAR(100) NULL,
    MPD_nomRel VARCHAR(100) NULL,
    MPD_nomBdate DATETIME NULL,
    MPD_DOB VARCHAR(50) NULL,
    MPD_Address VARCHAR(100) NULL,
    MPD_City VARCHAR(100) NULL,
    MPD_Country VARCHAR(100) NULL,
    MPD_State VARCHAR(100) NULL,
    MPD_District VARCHAR(100) NULL,
    MPD_PinCode VARCHAR(100) NULL,
    MLD_HQuestion VARCHAR(100) NULL,
    MLD_HAnswer VARCHAR(100) NULL,
    MPD_resphone VARCHAR(100) NULL,
    MPD_EWAccNo VARCHAR(100) NULL,
    MPD_Extra21 VARCHAR(50) NULL,
    ImportPacName VARCHAR(50) NULL,
    ImportPaymode VARCHAR(50) NULL,
    CustomerPassword VARCHAR(500) NULL,
    PasswordSalt VARCHAR(50) NULL,
    OrgPassword VARCHAR(500) NULL
);
GO
CREATE PROCEDURE prc_ImportprocessJoiningTables  
AS   
    BEGIN        
         
           
  -- Insert in MemJoining, MemProfile, memLogin        
           
        DECLARE @PlanTypeID NUMERIC(18, 0)    
           
        SELECT  @PlanTypeID = SM_Value  
        FROM    Settings_Mst (NOLOCK)  
        WHERE   SM_ID = 73    
           
        INSERT  INTO MemJoining_Dtls  
                ( MJD_MemNo ,  
                  MJD_DOJ ,  
                  MJD_DTOJ ,  
                  MJD_JoinFr ,  
                  MJD_PvCode ,  
                  MJD_PvType ,  
                  MJD_Paymode ,  
                  MJD_PayType ,  
                  MJD_Amount ,  
                  MJD_Multiplecnt ,  
                  MJD_MasterID ,  
                  MJD_Introducer ,  
                  MJD_AdjustedTo ,  
                  MJD_LR ,  
                  MJD_RoyIntroducer ,  
                  MJD_RoyAdjustedTo ,  
                  MJD_RoyLR ,  
                  MJD_CCTrnAmt ,  
                  MJD_TrnTotalAmt ,  
                  MJD_PGCode ,  
                  MJD_BV ,  
                  MJD_PV ,  
                  MJD_pkgBV ,  
                  MJD_pkgPV ,  
                  CustomerId ,  
                  TraderId        
                )  
                SELECT  MJD_MemNo ,  
                        MJD_DOJ ,  
                        MJD_DTOJ ,  
                        NULL ,  
                        MJD_PvCode ,  
                        MJD_PvType ,  
                        MJD_Paymode ,  
                        MJD_PayType ,  
                        MJD_Amount ,  
                        MJD_Multiplecnt ,  
                        MJD_MasterID ,  
                        0 ,  
                        0 ,  
                        ( CASE WHEN @PlanTypeID = 2 THEN 'L'  
                               ELSE MJD_LR  
                          END ) ,  
                        MJD_RoyIntroducer ,  
                        MJD_RoyAdjustedTo ,  
                        MJD_Introducer , -- Need to Clear after Process        
                        MJD_CCTrnAmt ,  
                        MJD_TrnTotalAmt ,  
                        MJD_AdjustedTo , -- Need to Clear after Process        
                        MJD_BV ,  
                        MJD_PV ,  
                        MJD_pkgBV ,  
                        MJD_pkgPV ,  
                        NULL , --CustomerId ,        
                        NULL  
                FROM    ImportRegistrationBulkDatatable (NOLOCK)        
                  
                  
 -- Set Introducer ID and AdjustedToID        
        IF ( @PlanTypeID = 2 )   
            BEGIN  
                 -- For Generation  
                UPDATE  MemJoining_Dtls  
                SET     MJD_Introducer = SponsID ,  
                        MJD_AdjustedTo = SponsID  
                FROM    ( SELECT    mem.MJD_MemNo AS MemNo ,  
                                    intr.MJD_MemID AS SponsID  
                          FROM      MemJoining_Dtls AS mem ( NOLOCK )  
                                    INNER   JOIN MemJoining_Dtls AS intr ( NOLOCK ) ON intr.MJD_MemNo = mem.MJD_RoyLR  
                          WHERE     mem.MJD_Introducer = 0  
                                    AND mem.MJD_MemID != 1  
                        ) MUpdate  
                WHERE   MemJoining_Dtls.MJD_MemNo = MUpdate.MemNo    
          
            END  
        ELSE   
            BEGIN  
               -- For Others plan   
                UPDATE  MemJoining_Dtls  
                SET     MJD_Introducer = SponsID ,  
                        MJD_AdjustedTo = AdjID  
                FROM    ( SELECT    mem.MJD_MemNo AS MemNo ,  
                                    intr.MJD_MemID AS SponsID ,  
                                    Adj.MJD_MemID AS AdjID  
                          FROM      MemJoining_Dtls AS mem ( NOLOCK )  
                                    INNER   JOIN MemJoining_Dtls AS intr ( NOLOCK ) ON intr.MJD_MemNo = mem.MJD_RoyLR  
                                    INNER   JOIN MemJoining_Dtls AS Adj ( NOLOCK ) ON Adj.MJD_MemNo = mem.MJD_PGCode  
                          WHERE     mem.MJD_Introducer = 0  
                                    AND mem.MJD_MemID != 1  
                        ) MUpdate  
                WHERE   MemJoining_Dtls.MJD_MemNo = MUpdate.MemNo        
            END        
        
-- Clear MJD_RoyLR & MJD_PGCode values from memjoining table        
        UPDATE  MemJoining_Dtls  
        SET     MJD_RoyLR = NULL  
        WHERE   MJD_RoyLR IS NOT NULL        
        UPDATE  MemJoining_Dtls  
        SET     MJD_PGCode = NULL  
        WHERE   MJD_PGCode IS NOT NULL        
         
        
        
 -- Insert in MemProfile        
         
        INSERT  INTO MemProfile_Dtls  
                ( MPD_MemId ,  
                  MPD_Name ,  
                  MPD_DOB ,  
                  MPD_Title ,  
                  MPD_Gender ,  
                  MPD_MaritalStatus ,  
                  MPD_Mother ,  
                  MPD_Father ,  
                  MPD_HoWo ,  
                  MPD_PassportNo ,  
                  MPD_IFSDCode ,  
                  MPD_Qualification ,  
                  MPD_Profession ,  
                  MPD_Designation ,  
                  MPD_Income ,  
                  MPD_SalStatus ,  
                  MPD_NoDepend ,  
                  MPD_TwoWheeler ,  
                  MPD_FourWheeler ,  
                  MPD_Age ,  
                  MPD_Nationality ,  
                  MPD_BloodGroup ,  
                  MPD_ResStatus ,  
                  MPD_PanCardNo ,  
                  MPD_ApplyPan ,  
                  MPD_ChqPayTo ,  
                  MPD_Address ,  
                  MPD_LandMark ,  
                  MPD_City ,  
                  MPD_District ,  
                  MPD_State ,  
                  MPD_Country ,  
                  MPD_PinCode ,  
                  MPD_Mobile ,  
                  MPD_ResPhone ,  
                  MPD_OffPhone ,  
                  MPD_Email ,  
                  MPD_Fax ,  
                  MPD_ShpAddress ,  
                  MPD_ShpLandMark ,  
                  MPD_ShpCity ,  
                  MPD_ShpDistrict ,  
                  MPD_ShpState ,  
                  MPD_ShpCountry ,  
                  MPD_ShpPinCode ,  
                  MPD_ShpContPerson ,  
                  MPD_ShpContNo ,  
                  MPD_Bank ,  
                  MPD_Branch ,  
                  MPD_BankAddress ,  
                  MPD_AccType ,  
                  MPD_AccNo ,  
                  MPD_AccPwd ,  
                  MPD_nomTitle ,  
                  MPD_nomName ,  
                  MPD_nomGender ,  
                  MPD_nomRel ,  
                  MPD_nomBdate ,  
                  MPD_NomAddr ,  
                  MPD_PinSrNo ,  
                  MPD_DDNo ,  
                  MPD_DDDate ,  
                  MPD_DDBank ,  
                  MPD_DDBranch ,  
                  MPD_DDBankAddr ,  
                  MPD_EpinPwd ,  
                  MPD_EWSpon ,  
                  MPD_EWAccNo ,  
                  MPD_EWTransfer ,  
                  MPD_FirmType ,  
                  MPD_FirmDetail ,  
                  MPD_ReceiptNo ,  
                  MPD_EpinCurrentLogin ,  
                  MPD_EpinLastLogin ,  
                  MPD_Extra1 ,  
                  MPD_Extra2 ,  
                  MPD_Extra3 ,  
                  MPD_Extra4 ,  
                  MPD_Extra5 ,  
                  MPD_Extra6 ,  
                  MPD_Extra7 ,  
                  MPD_Extra8 ,  
                  MPD_Extra9 ,  
                  MPD_Extra10 ,  
                  MPD_Extra11 ,  
                  MPD_Extra12 ,  
                  MPD_Extra13 ,  
                  MPD_Extra14 ,  
                  MPD_Extra15 ,  
                  MPD_Extra16 ,  
                  MPD_Extra17 ,  
                  MPD_Extra18 ,  
                  MPD_Extra19 ,  
                  MPD_Extra20 ,  
                  MPD_Extra21 ,  
                  MPD_Extra22 ,  
                  MPD_Extra23 ,  
MPD_Extra24 ,  
                  MPD_Extra25 ,  
                  MPD_Extra26 ,  
                  MPD_Extra27 ,  
                  MPD_Extra28 ,  
                  MPD_Extra29 ,  
                  MPD_Extra30 ,  
                  MPD_ImagePath ,  
                  MPD_PayReceiptMode ,  
                  ExternalAuthFlag        
                  
                )  
                SELECT  MemJoining_Dtls.MJD_MemID , -- MPD_MemId - numeric        
                        MPD_Name , -- MPD_Name - varchar(100)        
                        MPD_DOB , -- MPD_DOB - datetime        
                        MPD_Title , -- MPD_Title - varchar(10)        
                        MPD_Gender , -- MPD_Gender - varchar(50)        
                        NULL , -- MPD_MaritalStatus - varchar(10)        
                        NULL , -- MPD_Mother - varchar(100)        
                        NULL , -- MPD_Father - varchar(100)        
                        NULL , -- MPD_HoWo - varchar(100)        
                        NULL , -- MPD_PassportNo - varchar(50)        
                        MPD_IFSDCode , -- MPD_IFSDCode - varchar(20)        
                        NULL , -- MPD_Qualification - varchar(50)        
                        NULL , -- MPD_Profession - varchar(100)        
                        NULL , -- MPD_Designation - varchar(50)        
                        NULL , -- MPD_Income - varchar(50)        
                        NULL , -- MPD_SalStatus - varchar(50)        
                        NULL , -- MPD_NoDepend - numeric        
                        NULL , -- MPD_TwoWheeler - varchar(10)        
                        NULL , -- MPD_FourWheeler - varchar(10)        
                        NULL , -- MPD_Age - varchar(100)        
                        NULL , -- MPD_Nationality - varchar(50)        
                        NULL , -- MPD_BloodGroup - varchar(10)        
                        NULL , -- MPD_ResStatus - varchar(50)        
                        MPD_PanCardNo , -- MPD_PanCardNo - varchar(50)        
                        MPD_ApplyPan , -- MPD_ApplyPan - varchar(50)        
                        MPD_ChqPayTo , -- MPD_ChqPayTo - varchar(100)        
                        MPD_Address , -- MPD_Address - varchar(400)        
                        NULL , -- MPD_LandMark - varchar(400)        
                        MPD_City , -- MPD_City - varchar(50)        
                        MPD_District , -- MPD_District - varchar(50)        
                        MPD_State , -- MPD_State - varchar(50)        
                        MPD_Country , -- MPD_Country - varchar(50)        
                        MPD_PinCode , -- MPD_PinCode - varchar(10)        
                        MPD_Mobile , -- MPD_Mobile - varchar(20)        
                        NULL , -- MPD_ResPhone - varchar(20)        
                        NULL , -- MPD_OffPhone - varchar(20)        
                        MPD_Email , -- MPD_Email - varchar(50)        
                        NULL , -- MPD_Fax - varchar(50)        
                        NULL , -- MPD_ShpAddress - varchar(400)        
                        NULL , -- MPD_ShpLandMark - varchar(400)        
                        NULL , -- MPD_ShpCity - varchar(50)        
                        NULL , -- MPD_ShpDistrict - varchar(50)        
                        NULL , -- MPD_ShpState - varchar(50)        
                        NULL , -- MPD_ShpCountry - varchar(50)        
                        MPD_ShpPinCode , -- MPD_ShpPinCode - varchar(10)        
                        NULL , -- MPD_ShpContPerson - varchar(20)        
                        NULL , -- MPD_ShpContNo - varchar(20)        
                        MPD_Bank , -- MPD_Bank - varchar(100)        
                        MPD_Branch , -- MPD_Branch - varchar(50)        
                        MPD_BankAddress , -- MPD_BankAddress - varchar(400)        
                        MPD_AccType , -- MPD_AccType - varchar(30)        
           MPD_AccNo , -- MPD_AccNo - varchar(16)        
                        MPD_AccPwd , -- MPD_AccPwd - varchar(16)        
                        NULL , -- MPD_nomTitle - varchar(50)        
                        MPD_nomName , -- MPD_nomName - varchar(100)        
                        NULL , -- MPD_nomGender - varchar(10)        
                        MPD_nomRel , -- MPD_nomRel - varchar(50)        
                        MPD_nomBdate , -- MPD_nomBdate - datetime        
                        NULL , -- MPD_NomAddr - varchar(50)        
                        MemJoining_Dtls.MJD_MemID , -- MPD_PinSrNo - numeric        
                        NULL , -- MPD_DDNo - numeric        
                        MPD_DDDate , -- MPD_DDDate - datetime        
                        MPD_DDBank , -- MPD_DDBank - varchar(100)        
                        MPD_DDBranch , -- MPD_DDBranch - varchar(100)        
                        NULL , -- MPD_DDBankAddr - varchar(100)        
                        NULL , -- MPD_EpinPwd - varchar(100)        
                        NULL , -- MPD_EWSpon - varchar(50)        
                        NULL , -- MPD_EWAccNo - varchar(100)        
                        NULL , -- MPD_EWTransfer - decimal        
                        MPD_FirmType , -- MPD_FirmType - varchar(50)        
                        MPD_FirmDetail , -- MPD_FirmDetail - varchar(500)        
                        MPD_ReceiptNo , -- MPD_ReceiptNo - varchar(50)        
                        NULL , -- MPD_EpinCurrentLogin - datetime        
                        NULL , -- MPD_EpinLastLogin - datetime        
                        NULL , -- MPD_Extra1 - varchar(100)        
                        NULL , -- MPD_Extra2 - varchar(100)        
                        NULL , -- MPD_Extra3 - varchar(100)        
                        NULL , -- MPD_Extra4 - varchar(100)        
                        NULL , -- MPD_Extra5 - varchar(100)        
                        NULL , -- MPD_Extra6 - varchar(100)        
                        MPD_FirstName , -- MPD_Extra7 - varchar(100)        
                        MPD_MiddleName , -- MPD_Extra8 - varchar(100)        
                        MPD_LastName , -- MPD_Extra9 - varchar(100)        
                        NULL , -- MPD_Extra10 - varchar(100)        
                        NULL , -- MPD_Extra11 - varchar(100)        
                        NULL , -- MPD_Extra12 - varchar(100)        
                        NULL , -- MPD_Extra13 - varchar(100)        
                        NULL , -- MPD_Extra14 - varchar(100)        
                        NULL , -- MPD_Extra15 - varchar(100)        
                        NULL , -- MPD_Extra16 - varchar(100)        
                        MPD_Extra17 , -- MPD_Extra17 - varchar(100)        
                        MPD_Extra18 , -- MPD_Extra18 - varchar(100)        
                        NULL , -- MPD_Extra19 - varchar(100)        
                        NULL , -- MPD_Extra20 - varchar(100)        
                        NULL , -- MPD_Extra21 - varchar(100)        
                        NULL , -- MPD_Extra22 - varchar(100)        
                        NULL , -- MPD_Extra23 - varchar(100)        
                        NULL , -- MPD_Extra24 - varchar(100)        
                        NULL , -- MPD_Extra25 - varchar(100)        
                        NULL , -- MPD_Extra26 - varchar(100)        
                        NULL , -- MPD_Extra27 - varchar(100)        
                        NULL , -- MPD_Extra28 - varchar(100)        
                        NULL , -- MPD_Extra29 - varchar(100)        
                        ImportRegistrationBulkDatatable.MJD_MemNo , -- MPD_Extra30 - varchar(100)        
                        NULL , -- MPD_ImagePath - varchar(100)        
                        3 , -- MPD_PayReceiptMode - numeric        
                        NULL -- ExternalAuthFlag - varchar(4)        
                FROM    ImportRegistrationBulkDatatable (NOLOCK)  
                        INNER JOIN MemJoining_Dtls (NOLOCK) ON MemJoining_Dtls.MJD_MemNo = ImportRegistrationBulkDatatable.MJD_MemNo        
          
        
-- Insert in MemLogin_Dtls          
        INSERT  INTO MemLogin_Dtls  
                ( MLD_MemID ,  
                  MLD_Login ,  
                  MLD_pwd ,  
                  MLD_Theme ,  
                  MLD_CurrentLogin ,  
                  MLD_LastLogin ,  
                  MLD_HQuestion ,  
                  MLD_HAnswer ,  
                  MLD_decEXTRA1 ,  
                  MLD_decEXTRA2 ,  
                  MLD_chvEXTRA3 ,  
                  MLD_chvEXTRA4 ,  
                  MLD_CurrentLoginIP ,  
                  MLD_LastLoginIP        
                  
                )  
                SELECT  MemJoining_Dtls.MJD_MemID , -- MLD_MemID - numeric        
                        MLD_Login , -- MLD_Login - varchar(50)        
                        MLD_pwd , -- MLD_pwd - varchar(50)        
                        NULL , -- MLD_Theme - varchar(max)        
                        NULL , -- MLD_CurrentLogin - datetime        
                        NULL , -- MLD_LastLogin - datetime        
                        NULL , -- MLD_HQuestion - varchar(100)        
                        NULL , -- MLD_HAnswer - varchar(100)        
                        1 , -- MLD_decEXTRA1 - numeric        
                        NULL , -- MLD_decEXTRA2 - numeric        
                        NULL , -- MLD_chvEXTRA3 - varchar(50)        
                        MemJoining_Dtls.MJD_MemNo , -- MLD_chvEXTRA4 - varchar(50)        
                        NULL , -- MLD_CurrentLoginIP - varchar(50)        
                        NULL  -- MLD_LastLoginIP - varchar(50)        
                FROM    ImportRegistrationBulkDatatable  
                        INNER JOIN MemJoining_Dtls (NOLOCK) ON MemJoining_Dtls.MJD_MemNo = ImportRegistrationBulkDatatable.MJD_MemNo        
            
        
    END 
GO
CREATE PROCEDURE prc_ImportprocessCustomerTables
AS
BEGIN

    INSERT INTO Customer
    (
        MemId,
        Memno,
        CustomerGuid,
        Username,
        Email,
        Password,
        PasswordFormatId,
        PasswordSalt,
        EmailToRevalidate,
        AdminComment,
        IsTaxExempt,
        AffiliateId,
        VendorId,
        HasShoppingCartItems,
        RequireReLogin,
        FailedLoginAttempts,
        CannotLoginUntilDateUtc,
        Active,
        Deleted,
        IsSystemAccount,
        SystemName,
        LastIpAddress,
        CreatedOnUtc,
        LastLoginDateUtc,
        LastActivityDateUtc,
        RegisteredInStoreId,
        PictureId,
        BillingAddress_Id,
        ShippingAddress_Id,
        ExternalAuthFlag
    )
    SELECT MemLogin_Dtls.MLD_MemID,
           MLD_chvEXTRA4,
           NEWID(),
           MemLogin_Dtls.MLD_Login,
           MPD_Email,
           ImportRegistrationBulkDatatable.OrgPassword,
           0,
           PasswordSalt,
           NULL,
           NULL,
           0,
           0,
           0,
           0,
           0,
           0,
           NULL,
           1,
           0,
           0,
           NULL,
           NULL,
           MJD_DOJ,
           NULL,
           MJD_DOJ,
           0,
           NULL,
           NULL,
           NULL,
           0
    FROM ImportRegistrationBulkDatatable (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON dbo.MemLogin_Dtls.MLD_chvEXTRA4 = ImportRegistrationBulkDatatable.MJD_MemNo;


    -- Update memjoining    

    UPDATE MemJoining_Dtls
    SET CustomerId = Customer.Id
    FROM Customer (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON Memno = MLD_chvEXTRA4
    WHERE MLD_MemID = MJD_MemID
          AND MLD_chvEXTRA4 IS NOT NULL;


    INSERT INTO CustomerPassword
    (
        CustomerId,
        Password,
        PasswordFormatId,
        PasswordSalt,
        CreatedOnUtc
    )
    SELECT Id,
           Password,
           PasswordFormatId,
           PasswordSalt,
           CreatedOnUtc
    FROM Customer (NOLOCK)
    WHERE Memno IN (
                       SELECT MLD_chvEXTRA4
                       FROM MemLogin_Dtls (NOLOCK)
                       WHERE MLD_chvEXTRA4 IS NOT NULL
                   );




    INSERT INTO Customer_CustomerRole_Mapping
    (
        Customer_Id,
        CustomerRole_Id
    )
    SELECT Id, -- Customer_Id - int              
           3   -- CustomerRole_Id - int              
    FROM Customer
    WHERE Memno IN (
                       SELECT MLD_chvEXTRA4
                       FROM MemLogin_Dtls (NOLOCK)
                       WHERE MLD_chvEXTRA4 IS NOT NULL
                   );


END;
GO
CREATE PROCEDURE prc_ImportprocessGenericAttribute  
AS   
    BEGIN        
        
        
        IF OBJECT_ID('tempdb..#TempGenericAttribute', 'U') IS NOT NULL   
            BEGIN                                                                                             
                DROP TABLE #TempGenericAttribute                                                                                          
            END                                              
        CREATE TABLE #TempGenericAttribute  
            (  
              CustomerID INT ,  
              Gender VARCHAR(50) ,  
              FirstName VARCHAR(100) ,  
              LastName VARCHAR(100) ,  
              ZipPostalCode VARCHAR(10) ,  
              CityName VARCHAR(10) ,  
              City [numeric](18, 0) ,  
              StateProvinceId [numeric](18, 0) ,  
              CountryId [numeric](18, 0) ,  
              Phone VARCHAR(10) ,  
              Email VARCHAR(200)  
            )                                            
                     
        INSERT  INTO #TempGenericAttribute  
                ( CustomerID ,  
                  Gender ,  
                  FirstName ,  
                  LastName ,  
                  ZipPostalCode ,  
                  CityName ,  
                  City ,  
                  StateProvinceId ,  
                  CountryId ,  
                  Phone ,  
                  Email       
                            
                )  
                SELECT  customer.Id , -- CustomerID - int        
                        MPD_Gender , -- Gender - varchar(50)        
                        MPD_Extra7 , -- FirstName - varchar(100)        
                        MPD_Extra9 , -- LastName - varchar(100)        
                        MPD_PinCode , -- ZipPostalCode - varchar(10)        
                        MPD_City , -- CityName - varchar(10)        
                        city.Id , -- City - numeric        
                        StateProvince.Id , -- StateProvinceId - numeric         
                        country.Id , -- CountryId - numeric        
                        MPD_Mobile ,  -- Phone - varchar(10)        
                        MPD_Email  
                FROM    MemProfile_Dtls (NOLOCK)  
                        INNER JOIN Customer  (NOLOCK) ON MPD_MemId = MemId  
                        INNER JOIN MemLogin_Dtls (NOLOCK) ON Memno = MLD_chvEXTRA4  
                        LEFT OUTER JOIN Country (NOLOCK) ON MPD_Country = Country.Name  
                        LEFT OUTER JOIN StateProvince (NOLOCK) ON Country.Id = StateProvince.CountryId  
                                                              AND dbo.StateProvince.Name = MPD_State  
                        LEFT OUTER JOIN City (NOLOCK) ON city.CountryId = Country.Id  
                                                         AND city.StateId = StateProvince.Id  
                                                         AND City.Name = MPD_City  
                WHERE   MLD_chvEXTRA4 IS NOT NULL        
                     
          
        
         
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'Gender' ,  
                        Gender ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(Gender, '') != ''     
        
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'FirstName' ,  
                        FirstName ,  
          0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(FirstName, '') != ''     
           
           
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'LastName' ,  
                        LastName ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(LastName, '') != ''     
           
              
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'ZipPostalCode' ,  
                        ZipPostalCode ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(ZipPostalCode, '') != ''                      
                                      
                
           
              
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'CityName' ,  
                        CityName ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(CityName, '') != ''                                              
                   
              
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'City' ,  
                        City ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(City, 0) > 0                                     
                    
                                             
        INSERT  INTO dbo.GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                    
                                                
                )  
                SELECT  CustomerID ,  
                        'Customer' , -- KeyGroup - nvarchar(400)                    
                        'StateProvinceId' , -- Key - nvarchar(400)                    
                        StateProvinceId , -- Value - nvarchar(max)                    
                        0  -- StoreId - int                      
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(StateProvinceId, 0) > 0                                    
                                                         
        INSERT  INTO dbo.GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                    
                                                
                )  
                SELECT  CustomerID ,  
                        'Customer' , -- KeyGroup - nvarchar(400)                    
                        'CountryId' , -- Key - nvarchar(400)                    
                        CountryId , -- Value - nvarchar(max)                    
                        0  -- StoreId - int                    
    FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(CountryId, 0) > 0                                      
             
            
        INSERT  INTO GenericAttribute  
                ( EntityId ,  
                  KeyGroup ,  
                  [Key] ,  
                  Value ,  
                  StoreId                     
                    
                )  
                SELECT  CustomerID ,  
                        'Customer' ,  
                        'Phone' ,  
                        Phone ,  
                        0  
                FROM    #TempGenericAttribute (NOLOCK)  
                WHERE   ISNULL(Phone, '') != ''                                                         
                                       
        
  -- Add in Address Table      
        
        INSERT  INTO Address  
                ( FirstName ,  
                  LastName ,  
                  Email ,  
                  Company ,  
                  CountryId ,  
                  StateProvinceId ,  
                  City ,  
                  Address1 ,  
                  Address2 ,  
                  ZipPostalCode ,  
                  PhoneNumber ,  
                  FaxNumber ,  
                  CustomAttributes ,  
                  CreatedOnUtc ,  
                  CityName      
                )  
                SELECT  FirstName , -- FirstName - nvarchar(max)      
                        LastName , -- LastName - nvarchar(max)      
                        Email , -- Email - nvarchar(max)      
                        N'' , -- Company - nvarchar(max)      
                        CountryId , -- CountryId - int      
                        StateProvinceId , -- StateProvinceId - int      
                        City , -- City - nvarchar(max)      
                        N'' , -- Address1 - nvarchar(max)      
                        N'' , -- Address2 - nvarchar(max)      
                        ZipPostalCode , -- ZipPostalCode - nvarchar(max)      
                        Phone , -- PhoneNumber - nvarchar(max)      
                        N'' , -- FaxNumber - nvarchar(max)      
                        CustomerID , -- CustomAttributes - nvarchar(max)  -- Need To clear after update      
                        GETDATE() , -- CreatedOnUtc - datetime      
                        CityName -- CityName - nvarchar(max)      
                FROM    #TempGenericAttribute      
        
        
        UPDATE  Customer  
        SET     ShippingAddress_Id = Address.id ,  
                BillingAddress_Id = Address.id  
        FROM    Address (NOLOCK)  
        WHERE   Address.CustomAttributes = Customer.Id  
                AND CustomAttributes IS NOT NULL      
        
        
        INSERT  INTO dbo.CustomerAddresses  
                ( Customer_Id ,  
                  Address_Id   
                )  
                SELECT  CustomAttributes ,  
                        Address.id  
                FROM    Address (NOLOCK)  
                WHERE   CustomAttributes IS NOT NULL AND CustomAttributes<>0    
        
        UPDATE  dbo.Address  
        SET     CustomAttributes = NULL  
        WHERE   CustomAttributes IS NOT NULL      
        
        
        IF OBJECT_ID('tempdb..#TempGenericAttribute', 'U') IS NOT NULL   
            BEGIN                                          
                DROP TABLE #TempGenericAttribute                                                                                          
            END      
        
    END 
GO
CREATE PROCEDURE prc_importMemDirectandStatus
AS
BEGIN

    SET NOCOUNT ON;

    INSERT INTO MemDirects_Dtls
    (
        MDD_DateCode,
        MDD_Date,
        MDD_MemID,
        MDD_DirectID,
        MDD_Amt,
        MDD_PvCode,
        MDD_DSrNo,
        MDD_Flag,
        MDD_decEXTRA1,
        MDD_decEXTRA2,
        MDD_chvEXTRA3,
        MDD_chvEXTRA4,
        MDD_Cycle,
        MDD_JoiningCycle
    )
    SELECT NULL,
           MJD_DTOJ,
           MJD_Introducer,
           MJD_MemID,
           MJD_Amount,
           MJD_PvCode,
           ROW_NUMBER() OVER (PARTITION BY MJD_Introducer ORDER BY MJD_Introducer),
           (CASE
                WHEN PM_MSMID = 5 THEN
                    1
                ELSE
                    0
            END
           ),
           (CASE
                WHEN MJD_LR = 'L' THEN
                    1
                ELSE
                    NULL
            END
           ),
           (CASE
                WHEN MJD_LR = 'R' THEN
                    1
                ELSE
                    NULL
            END
           ),
           NULL,
           MJD_LR,
           0,
           0
    FROM MemJoining_Dtls (NOLOCK)
        INNER JOIN Paymode_mst (NOLOCK)
            ON MJD_Paymode = PM_ID
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
    WHERE MJD_Introducer > 0;


    DECLARE @PlanTypeID NUMERIC(18, 0);

    SELECT @PlanTypeID = SM_Value
    FROM Settings_Mst (NOLOCK)
    WHERE SM_ID = 73;
    INSERT INTO MemStatus_Dtls
    (
        MSD_Memid,
        MSD_ConfirmDate,
        MSD_DirectChCnt,
        MSD_ConfDirectChCnt,
        MSD_MSMID,
        MSD_ConfFlg,
        MSD_Cycle,
        MSD_FirstDtCode,
        MSD_IsSpillFlg,
        MSD_CurrStatusID,
        MSD_PvCode,
        MSD_PvType,
        MSD_IncFlg,
        MSD_FConfDate,
        MSD_decExtra1,
        MSD_decExtra2,
        MSD_chvExtra3,
        MSD_chvExtra4,
        MSD_dtExtra5,
        MSD_dtExtra6
    )
    SELECT MJD_MemID,                        -- MSD_Memid - numeric    
           (CASE
                WHEN PM_MSMID = 5 THEN
                    MJD_DTOJ
                ELSE
                    NULL
            END
           ),                                -- MSD_ConfirmDate - datetime    
           ISNULL(DirectCount, 0),           -- MSD_DirectChCnt - smallint    
           ISNULL(DirectConfCount, 0),       -- MSD_ConfDirectChCnt - smallint    
           PM_MSMID,                         -- MSD_MSMID - numeric    
           (CASE
                WHEN PM_MSMID = 5 THEN
                    1
                ELSE
                    0
            END
           ),                                -- MSD_ConfFlg - tinyint    
           NULL,                             -- MSD_Cycle - numeric    
           0,                                -- MSD_FirstDtCode - numeric    
           (CASE
                WHEN @PlanTypeID = 2 THEN
                    0
                ELSE
           (CASE
                WHEN ROW_NUMBER() OVER (PARTITION BY MJD_Introducer ORDER BY MJD_Introducer) > 2 THEN
                    1
                ELSE
                    0
            END
           )
            END
           ),                                -- MSD_IsSpillFlg - tinyint    
           1,                                -- MSD_CurrStatusID - tinyint    
           MJD_PvCode,                       -- MSD_PvCode - numeric    
           MJD_PvType,                       -- MSD_PvType - numeric    
           0,                                -- MSD_IncFlg - numeric    
           NULL,                             -- MSD_FConfDate - datetime    
           NULL,                             -- MSD_decExtra1 - numeric    
           NULL,                             -- MSD_decExtra2 - numeric    
           NULL,                             -- MSD_chvExtra3 - varchar(100)    
           NULL,                             -- MSD_chvExtra4 - varchar(100)    
           CONVERT(DATETIME, MJD_DTOJ, 103), -- MSD_dtExtra5 - datetime    
           NULL                              -- MSD_dtExtra6 - datetime    
    FROM MemJoining_Dtls (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
        INNER JOIN Paymode_mst (NOLOCK)
            ON MJD_Paymode = PM_ID
        LEFT OUTER JOIN
        (
            SELECT MJD_Introducer AS MemID,
                   COUNT(MJD_MemID) AS DirectCount
            FROM MemJoining_Dtls (NOLOCK)
            GROUP BY MJD_Introducer
        ) DirectCountT
            ON DirectCountT.MemID = MJD_MemID
        LEFT OUTER JOIN
        (
            SELECT MJD_Introducer AS MemID,
                   COUNT(MJD_MemID) AS DirectConfCount
            FROM MemJoining_Dtls (NOLOCK)
                INNER JOIN Paymode_mst (NOLOCK)
                    ON MJD_Paymode = PM_ID
            WHERE PM_MSMID = 5
            GROUP BY MJD_Introducer
        ) DirectConfCountT
            ON DirectConfCountT.MemID = MJD_MemID
    WHERE MJD_Introducer > 0;



END;
GO
CREATE PROCEDURE prc_importIDwiseandDownlineBiz
AS
BEGIN

    SET NOCOUNT ON;

    INSERT INTO IDWiseDownline_Dtls
    (
        IDD_MEMID,
        IDD_DOWNID,
        IDD_LEVEL,
        IDD_LR
    )
    SELECT MJD_AdjustedTo,
           MJD_MemID,
           1,
           (CASE
                WHEN MJD_LR = 'L' THEN
                    1
                ELSE
                    2
            END
           )
    FROM MemJoining_Dtls (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo;



    DECLARE @LevelCount NUMERIC(18, 0),
            @RowCount NUMERIC(18, 0);

    SET @LevelCount = 1;
    SET @RowCount = 0;

    WHILE (@LevelCount < 999999)
    BEGIN
        SET @RowCount = 0;

        INSERT INTO IDWiseDownline_Dtls
        (
            IDD_MEMID,
            IDD_DOWNID,
            IDD_LEVEL,
            IDD_LR
        )
        SELECT IDD_MEMID,
               MJD_MemID AS [IDD_DOWNID],
               IDD_LEVEL + 1 AS [IDD_Level],
               [IDD_LR]
        FROM IDWiseDownline_Dtls WITH (NOLOCK)
            INNER JOIN MemJoining_Dtls (NOLOCK)
                ON MJD_AdjustedTo = IDD_DOWNID
            INNER JOIN MemLogin_Dtls (NOLOCK)
                ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
        WHERE IDD_Level = @LevelCount;


        SET @RowCount = @@ROWCOUNT;

        IF @RowCount <= 0
        BEGIN

            BREAK;
        END;

        SET @LevelCount = @LevelCount + 1;

    END;



    --  **** Memdown_dtls            
    --   ** OWN BIZ            
    INSERT INTO MemDownline_Dtls
    SELECT MJD_MemID,
           MJD_PVCode,
           0,
           MJD_PVType,
           CONVERT(DATETIME, MJD_DOJ, 103),
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           MJD_BV,
           (CASE
                WHEN PM_MSMID = 5 THEN
                    MJD_BV
                ELSE
                    0
            END
           ),
           MJD_PV,
           (CASE
                WHEN PM_MSMID = 5 THEN
                    MJD_PV
                ELSE
                    0
            END
           ),
           NULL,
           NULL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           NULL,
           NULL,
           MJD_Amount,
           (CASE
                WHEN PM_MSMID = 5 THEN
                    MJD_Amount
                ELSE
                    0
            END
           )
    FROM MemJoining_Dtls (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
        INNER JOIN Paymode_mst (NOLOCK)
            ON MJD_Paymode = PM_ID
    WHERE MJD_Introducer > 0
          AND
          (
              MJD_BV > 0
              OR MJD_PV > 0
              OR MJD_Amount > 0
          );

    --** Upline BIZ with Left & Right Biz            

    INSERT INTO MemDownline_Dtls
    SELECT IDD_MEMID,
           MJD_PvCode,
           IDD_LEVEL,
           MJD_PvType,
           CONVERT(DATETIME, MJD_DTOJ, 103),
           (CASE
                WHEN IDD_LR = 1 THEN
                    1
                ELSE
                    0
            END
           ), -- Left Count            
           (CASE
                WHEN IDD_LR = 2 THEN
                    1
                ELSE
                    0
            END
           ), -- Right Count            
           (CASE
                WHEN IDD_LR = 1
                     AND PM_MSMID = 5 THEN
                    1
                ELSE
                    0
            END
           ), -- Conf Left Count            
           (CASE
                WHEN IDD_LR = 2
                     AND PM_MSMID = 5 THEN
                    1
                ELSE
                    0
            END
           ), -- Conf Right Count            
           (CASE
                WHEN IDD_LR = 1 THEN
                    MJD_BV
                ELSE
                    0
            END
           ), -- Left BV            
           (CASE
                WHEN IDD_LR = 2 THEN
                    MJD_BV
                ELSE
                    0
            END
           ), -- Right BV            
           (CASE
                WHEN IDD_LR = 1
                     AND PM_MSMID = 5 THEN
                    MJD_BV
                ELSE
                    0
            END
           ), -- Conf Left BV            
           (CASE
                WHEN IDD_LR = 2
                     AND PM_MSMID = 5 THEN
                    MJD_BV
                ELSE
                    0
            END
           ), -- Conf Right BV            
           (CASE
                WHEN IDD_LR = 1 THEN
                    MJD_PV
                ELSE
                    0
            END
           ), -- Left PV            
           (CASE
                WHEN IDD_LR = 2 THEN
                    MJD_PV
                ELSE
                    0
            END
           ), -- Right PV            
           (CASE
                WHEN IDD_LR = 1
                     AND PM_MSMID = 5 THEN
                    MJD_PV
                ELSE
                    0
            END
           ), -- Conf Left PV            
           (CASE
                WHEN IDD_LR = 2
                     AND PM_MSMID = 5 THEN
                    MJD_PV
                ELSE
                    0
            END
           ), -- Conf Right PV            
           (CASE
                WHEN IDD_LR = 1 THEN
                    MJD_Amount
                ELSE
                    0
            END
           ), -- Left Sale            
           (CASE
                WHEN IDD_LR = 2 THEN
                    MJD_Amount
                ELSE
                    0
            END
           ), -- Right Sale            
           (CASE
                WHEN IDD_LR = 1
                     AND PM_MSMID = 5 THEN
                    MJD_Amount
                ELSE
                    0
            END
           ), -- Conf Left Sale            
           (CASE
                WHEN IDD_LR = 2
                     AND PM_MSMID = 5 THEN
                    MJD_Amount
                ELSE
                    0
            END
           ), -- Conf Right Sale            
           0,
           0,
           0,
           0,
           NULL,
           NULL,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           0,
           NULL,
           NULL,
           0,
           0
    FROM IDWiseDownline_Dtls WITH (NOLOCK)
        INNER JOIN MemJoining_Dtls (NOLOCK)
            ON IDD_DOWNID = MJD_MemID
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
        INNER JOIN Paymode_mst (NOLOCK)
            ON MJD_Paymode = PM_ID;





END;
GO
CREATE PROCEDURE prc_importGenDownlineBiz
AS
BEGIN

    SET NOCOUNT ON;

    IF EXISTS
    (
        SELECT SM_ID
        FROM Settings_Mst WITH (NOLOCK)
        WHERE SM_ID = 73
              AND SM_Value = 3
    )
    BEGIN

        INSERT INTO IDWiseGenDownline_Dtls
        (
            IDD_MEMID,
            IDD_DOWNID,
            IDD_LEVEL
        )
        SELECT MJD_Introducer,
               MJD_MemID,
               1
        FROM MemJoining_Dtls (NOLOCK)
            INNER JOIN MemLogin_Dtls (NOLOCK)
                ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo;




        DECLARE @LevelCount NUMERIC(18, 0),
                @RowCount NUMERIC(18, 0);

        SET @LevelCount = 1;
        SET @RowCount = 0;

        WHILE (@LevelCount < 999999)
        BEGIN
            SET @RowCount = 0;
            PRINT (@LevelCount);
            INSERT INTO IDWiseGenDownline_Dtls
            (
                IDD_MEMID,
                IDD_DOWNID,
                IDD_LEVEL
            )
            SELECT IDD_MEMID,
                   MJD_MemID AS [IDD_DOWNID],
                   IDD_LEVEL + 1 AS [IDD_Level]
            FROM IDWiseGenDownline_Dtls WITH (NOLOCK)
                INNER JOIN MemJoining_Dtls (NOLOCK)
                    ON MJD_Introducer = IDD_DOWNID
                INNER JOIN MemLogin_Dtls (NOLOCK)
                    ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
            WHERE IDD_Level = @LevelCount;

            --SET @RowCount = @@IDENTITY    
            SET @RowCount = @@ROWCOUNT;

            IF @RowCount <= 0
            BEGIN

                BREAK;
            END;

            SET @LevelCount = @LevelCount + 1;

        END;



        --     **** MemActualdown_dtls      
        --   ** OWN BIZ      
        INSERT INTO MemActDownline_Dtls
        SELECT MJD_MemID,
               MJD_PvCode,
               0,
               MJD_PvType,
               CONVERT(DATETIME, MJD_DOJ, 103),
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               MJD_BV,
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_BV
                    ELSE
                        0
                END
               ),
               MJD_PV,
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_PV
                    ELSE
                        0
                END
               ),
               NULL,
               NULL,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               NULL,
               NULL,
               MJD_Amount,
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_Amount
                    ELSE
                        0
                END
               )
        FROM MemJoining_Dtls (NOLOCK)
            INNER JOIN MemLogin_Dtls (NOLOCK)
                ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
            INNER JOIN Paymode_mst (NOLOCK)
                ON MJD_Paymode = PM_ID
        WHERE MJD_Introducer > 0
              AND
              (
                  MJD_BV > 0
                  OR MJD_PV > 0
                  OR MJD_Amount > 0
              );



        ---** Upline BIZ with Left &  Biz      

        INSERT INTO MemActDownline_Dtls
        SELECT IDD_MEMID,
               MJD_PvCode,
               IDD_LEVEL,
               MJD_PvType,
               CONVERT(DATETIME, MJD_DTOJ, 103),
               1,          -- Left Count      
               0,          -- Right Count      
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        1
                    ELSE
                        0
                END
               ),          -- Conf Left Count      
               0,          -- Conf Right Count      
               MJD_BV,     -- Left BV      
               0,          -- Right BV      
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_BV
                    ELSE
                        0
                END
               ),          -- Conf Left BV      
               0,          -- Conf Right BV      
               MJD_PV,     -- Left PV      
               0,          -- Right PV      
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_PV
                    ELSE
                        0
                END
               ),          -- Conf Left PV      
               0,          -- Conf Right PV      
               MJD_Amount, -- Left Sale      
               0,          -- Right Sale      
               (CASE
                    WHEN PM_MSMID = 5 THEN
                        MJD_Amount
                    ELSE
                        0
                END
               ),          -- Conf Left Sale      
               0,          -- Conf Right Sale      
               0,
               0,
               0,
               0,
               NULL,
               NULL,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               0,
               NULL,
               NULL,
               0,
               0
        FROM IDWiseGenDownline_Dtls WITH (NOLOCK)
            INNER JOIN MemJoining_Dtls (NOLOCK)
                ON IDD_DOWNID = MJD_MemID
            INNER JOIN MemLogin_Dtls (NOLOCK)
                ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
            INNER JOIN Paymode_mst (NOLOCK)
                ON MJD_Paymode = PM_ID;



    END;




END;
GO
CREATE PROCEDURE prc_BulkImportprocessMemberRegistration --  500004, '1500001',7,'2011-12-01 00:00:00'                                                                                  
    @dblMJD_MemID NUMERIC(18, 0),
    @chvMJD_MemNo VARCHAR(15),
    @intPM_ID INT,
    @dtmMJD_DOJ DATETIME
AS
BEGIN
    DECLARE @inyConfFlg TINYINT,
            @intPV_Code INT,
            @MJD_PvCode INT,
            @intMJD_Introducer INT,
            @intMJD_AdjustedTo INT,
            @intDirectCount INT,
            @intMSM_ID INT;


    -- VARIABLE INITIALIZATION                         
    SELECT @inyConfFlg = 0,
           @intMSM_ID = 1;

    SELECT @intMJD_Introducer = MJD_Introducer,
           @MJD_PvCode = MJD_PvCode,
           @intMJD_AdjustedTo = MJD_AdjustedTo
    FROM MemJoining_Dtls WITH (NOLOCK)
    WHERE MJD_MemID = @dblMJD_MemID;

    SELECT @intPV_Code = @MJD_PvCode;

    SELECT @intMSM_ID = PM_MSMID
    FROM Paymode_mst WITH (NOLOCK)
    WHERE PM_ID = @intPM_ID;


    IF (@intMSM_ID = 5 OR @intMSM_ID = 3 OR @dblMJD_MemID = 1)
    BEGIN
        SELECT @inyConfFlg = 1;
    END;
    ELSE
    BEGIN
        SELECT @inyConfFlg = 0;
    END;

    SELECT @intDirectCount = ISNULL(MSD_DirectChCnt, 0)
    FROM MemStatus_Dtls WITH (NOLOCK)
    WHERE MSD_Memid = @intMJD_Introducer;

    IF @intDirectCount < 2
    BEGIN
        --   MAINTAIN IT AS SPILL NETWORK.                                                                                                          
        EXEC prc_MaintainSpillNetwork @dblMJD_MemID,
                                      @intMJD_AdjustedTo,
                                      @dtmMJD_DOJ,
                                      @intPV_Code;

    END;



--      IF @inyConfFlg = 1   
--          BEGIN                       
-- --/* Award Qualification fr UPLINE procedure call */                        
--              IF EXISTS ( SELECT  SM_ID  
--                          FROM    Settings_Mst WITH ( NOLOCK )  
--                          WHERE   SM_ID = 26  
--                                  AND SM_Flag = 1 )   
--                  BEGIN                      
--                      DECLARE @strUpline VARCHAR(MAX)                                                     

--                      SELECT  @strUpline = COALESCE(@strUpline + ',', '')  
--                              + CAST(IDD_MEMID AS VARCHAR)  
--                      FROM    IDWiseDownline_Dtls WITH ( NOLOCK )  
--                      WHERE   IDD_DOWNID = @dblMJD_MemID                                                                                

--                      IF @strUpline <> ''   
--                          EXEC prc_AwardQualifierBulk @strUpline,  
--                              @dtmMJD_DOJ, @dblMJD_MemID, 1                       


--                  END                         
--          END                          
----    /* Royalty Qualification procedure call */                      
--      IF EXISTS ( SELECT  SM_ID  
--                  FROM    Settings_Mst WITH ( NOLOCK )  
--                  WHERE   SM_ID = 23  
--                          AND SM_Flag = 1 /*AND sm_value = 1*/ )   
--          BEGIN                        
--          --    /*PRINT 'BEFORE UPLINE ROYALTY QUALIFICATION => '   + CAST(GETDATE() AS VARCHAR(20))*/                           
--              EXEC RoyChkStructure @dtmMJD_DOJ, 0, @dblMJD_MemID, 1                       
--           --   /*PRINT 'AFTER UPLINE ROYALTY QUALIFICATION => ' + CAST(GETDATE() AS VARCHAR(20))*/                      
--          END                             
END;
GO
CREATE PROCEDURE prc_ProcessImportBusiness
AS
BEGIN
    DECLARE @Sm_BinaryFlag INT;
    SET @Sm_BinaryFlag = 0;

    SELECT @Sm_BinaryFlag = ISNULL(SM_decExtra1, 0)
    FROM Settings_Mst (NOLOCK)
    WHERE SM_ID = 19;


    DECLARE @dblMJD_MemID NUMERIC(18, 0),
            @chvMJD_MemNo VARCHAR(50),
            @intPM_ID NUMERIC(18, 0),
            @dtmMJD_DOJ DATETIME,
            @dtmMJD_DTOJ DATETIME,
            @SCnt NUMERIC(18, 0),
            @MaxCnt NUMERIC(18, 0);


    IF OBJECT_ID('tempdb..#TempImportMemJoining_Dtls', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempImportMemJoining_Dtls;
    END;
    CREATE TABLE #TempImportMemJoining_Dtls
    (
        RowID INT IDENTITY(1, 1) PRIMARY KEY,
        MJD_MemID INT,
        MJD_MemNo VARCHAR(50),
        [MJD_Paymode] [NUMERIC](18, 0),
        MJD_DOJ DATETIME,
        MJD_DTOJ DATETIME
    );
    INSERT INTO #TempImportMemJoining_Dtls
    (
        MJD_MemID,
        MJD_MemNo,
        MJD_Paymode,
        MJD_DOJ,
        MJD_DTOJ
    )
    SELECT MJD_MemID,
           MJD_MemNo,
           MJD_Paymode,
           MJD_DOJ,
           MJD_DTOJ
    FROM MemJoining_Dtls (NOLOCK)
        INNER JOIN MemLogin_Dtls (NOLOCK)
            ON MemJoining_Dtls.MJD_MemNo = MLD_chvEXTRA4
    WHERE MLD_chvEXTRA4 IS NOT NULL
    ORDER BY MJD_MemID;

    SET @SCnt = 1;
    SELECT @MaxCnt = COUNT(RowID)
    FROM #TempImportMemJoining_Dtls;

    WHILE @SCnt <= @MaxCnt
    BEGIN

        SELECT @dblMJD_MemID = MJD_MemID,
               @chvMJD_MemNo = MJD_MemNo,
               @intPM_ID = MJD_Paymode,
               @dtmMJD_DOJ = MJD_DOJ,
               @dtmMJD_DTOJ = MJD_DTOJ
        FROM #TempImportMemJoining_Dtls (NOLOCK)
        WHERE RowID = @SCnt;

        IF @Sm_BinaryFlag = 2
        BEGIN
            PRINT (@dblMJD_MemID);
            --EXEC prc_MemberDailyRegistration @dblMJD_MemID,    
            --    @chvMJD_MemNo, @intPM_ID, @dtmMJD_DOJ       
            EXEC prc_BulkImportprocessMemberRegistration @dblMJD_MemID,
                                                         @chvMJD_MemNo,
                                                         @intPM_ID,
                                                         @dtmMJD_DOJ;
        END;
        ELSE
        BEGIN
            EXEC prc_BulkImportprocessMemberRegistration @dblMJD_MemID,
                                                         @chvMJD_MemNo,
                                                         @intPM_ID,
                                                         @dtmMJD_DOJ;
        --EXEC prc_MemberRegistration @dblMJD_MemID,    
        --    @chvMJD_MemNo, @intPM_ID, @dtmMJD_DOJ       
        END;


        SET @SCnt = @SCnt + 1;

    END;


    IF @Sm_BinaryFlag = 1
    BEGIN
        ----  FOR SPILL Optimization for Introducer direct count > 2  

        INSERT INTO MemSpill_Dtls
        (
            MSD_Memid,
            MSD_SpillID,
            MSD_Date,
            MSD_Flag,
            MSD_Type,
            MSD_PvCode,
            MSD_Amt,
            MSD_Datecode,
            MSD_SpillNAmt,
            MSD_SpillNDatecode,
            MSD_CycleSpill,
            MSD_CycleSpillNetwork,
            MSD_JoiningCycle
        )
        SELECT MJD_Introducer,                  -- MSD_Memid - numeric  
               MJD_MemID,                       -- MSD_SpillID - numeric  
               CONVERT(DATETIME, MJD_DOJ, 103), -- MSD_Date - datetime  
               1,                               -- MSD_Flag - tinyint  
               1,                               -- MSD_Type - numeric  
               MJD_PvCode,                      -- MSD_PvCode - numeric  
               0,                               -- MSD_Amt - numeric  
               NULL,                            -- MSD_Datecode - numeric  
               0,                               -- MSD_SpillNAmt - numeric  
               NULL,                            -- MSD_SpillNDatecode - numeric  
               0,                               -- MSD_CycleSpill - numeric  
               0,                               -- MSD_CycleSpillNetwork - numeric  
               0                                -- MSD_JoiningCycle - numeric  
        FROM MemJoining_Dtls (NOLOCK)
            INNER JOIN MemLogin_Dtls (NOLOCK)
                ON MemLogin_Dtls.MLD_chvEXTRA4 = MemJoining_Dtls.MJD_MemNo
            INNER JOIN MemDirects_Dtls (NOLOCK)
                ON MJD_MemID = MDD_DirectID
                   AND MDD_DSrNo > 2;


    END;


END;
GO
CREATE PROCEDURE prc_BulkImportProcessMAIN
AS
BEGIN

    SET NOCOUNT ON;
    --****  DROP MEMJOINING TRIGGER                  
    IF EXISTS
    (
        SELECT *
        FROM sys.triggers
        WHERE object_id = OBJECT_ID(N'[trg_MemberRegistration]')
    )
        DROP TRIGGER [trg_MemberRegistration];


    IF OBJECT_ID('ImportRegistrationBulkDatatable', 'U') IS NOT NULL
    BEGIN
        DROP TABLE ImportRegistrationBulkDatatable;
    END;

    CREATE TABLE ImportRegistrationBulkDatatable
    (
        MJD_MemNo NVARCHAR(50) NULL,
        MJD_Introducer NVARCHAR(MAX) NULL,
        MJD_AdjustedTo NVARCHAR(MAX) NULL,
        MJD_LR NVARCHAR(MAX) NULL,
        MJD_PvCode NUMERIC(18, 0) NULL,
        MJD_PvType NUMERIC(18, 0) NULL,
        MJD_bv NUMERIC(18, 2) NULL,
        MJD_Pv NUMERIC(18, 2) NULL,
        MJD_pkgbv NUMERIC(18, 2) NULL,
        MJD_pkgPv NUMERIC(18, 2) NULL,
        MJD_Paymode NUMERIC(18, 0) NULL,
        MJD_PayType VARCHAR(50) NULL,
        MJD_Multiplecnt NUMERIC(18, 0) NULL,
        MJD_MasterID NUMERIC(18, 0) NULL,
        MJD_DTOJ VARCHAR(50) NULL,
        MJD_DOJ VARCHAR(50) NULL,
        MJD_RoyIntroducer NUMERIC(18, 0) NULL,
        MJD_RoyAdjustedTo NUMERIC(18, 0) NULL,
        MJD_CCTrnAmt NUMERIC(18, 2) NULL,
        MJD_TrnTotalAmt NUMERIC(18, 2) NULL,
        MPD_DDBank VARCHAR(100) NULL,
        MPD_DDBranch VARCHAR(100) NULL,
        MPD_DDNo NUMERIC(18, 0) NULL,
        MPD_DDDate VARCHAR(50) NULL,
        MJD_Amount NUMERIC(18, 2) NULL,
        MPD_Title VARCHAR(10) NULL,
        MPD_Name VARCHAR(200) NULL,
        MPD_Mobile VARCHAR(20) NULL,
        MPD_Email VARCHAR(50) NULL,
        MPD_PanCardNo VARCHAR(50) NULL,
        MPD_ApplyPan VARCHAR(50) NULL,
        MPD_ChqPayTo VARCHAR(100) NULL,
        MPD_Bank VARCHAR(30) NULL,
        MPD_Branch VARCHAR(50) NULL,
        MPD_BankAddress VARCHAR(400) NULL,
        MPD_AccType VARCHAR(16) NULL,
        MPD_AccNo VARCHAR(25) NULL,
        MPD_AccPwd VARCHAR(16) NULL,
        MPD_IFSDCode VARCHAR(20) NULL,
        MPD_FirmType VARCHAR(100) NULL,
        MPD_FirmDetail VARCHAR(100) NULL,
        MPD_NoDepend NUMERIC(18, 0) NULL,
        MPD_ShpPinCode VARCHAR(100) NULL,
        MPD_PinSrNo VARCHAR(50) NULL,
        MPD_ReceiptNo VARCHAR(50) NULL,
        MLD_Login VARCHAR(50) NULL,
        MLD_pwd VARCHAR(50) NULL,
        MPD_Gender VARCHAR(50) NULL,
        MPD_IPAddress VARCHAR(50) NULL,
        MPD_Browser VARCHAR(50) NULL,
        MPD_BrowserVersion VARCHAR(50) NULL,
        OH_FRID NUMERIC(18, 0) NULL,
        OH_DispatchFrID NUMERIC(18, 0) NULL,
        OH_DispatchBy NUMERIC(18, 0) NULL,
        MPD_Extra17 VARCHAR(100) NULL,
        MPD_Extra18 VARCHAR(100) NULL,
        MPD_PayReceiptMode NUMERIC(18, 0) NULL,
        MPD_EWTransfer DECIMAL(18, 0) NULL,
        CurrDate DATETIME NULL,
        CurrDateTime DATETIME NULL,
        MPD_FirstName VARCHAR(100) NULL,
        MPD_MiddleName VARCHAR(100) NULL,
        MPD_LastName VARCHAR(100) NULL,
        MPD_nomName VARCHAR(100) NULL,
        MPD_nomRel VARCHAR(100) NULL,
        MPD_nomBdate DATETIME NULL,
        MPD_DOB VARCHAR(50) NULL,
        MPD_Address VARCHAR(100) NULL,
        MPD_City VARCHAR(100) NULL,
        MPD_Country VARCHAR(100) NULL,
        MPD_State VARCHAR(100) NULL,
        MPD_District VARCHAR(100) NULL,
        MPD_PinCode VARCHAR(100) NULL,
        MLD_HQuestion VARCHAR(100) NULL,
        MLD_HAnswer VARCHAR(100) NULL,
        MPD_resphone VARCHAR(100) NULL,
        MPD_EWAccNo VARCHAR(100) NULL,
        MPD_Extra21 VARCHAR(50) NULL,
        ImportPacName VARCHAR(50) NULL,
        ImportPaymode VARCHAR(50) NULL,
        CustomerPassword VARCHAR(500) NULL,
        PasswordSalt VARCHAR(50) NULL,
        OrgPassword VARCHAR(500) NULL
    );

    CREATE INDEX IX_ImportRegistrationBulkDatatableMJD_MemNo
    ON ImportRegistrationBulkDatatable (MJD_MemNo);

    INSERT INTO ImportRegistrationBulkDatatable
    (
        MJD_MemNo,
        MJD_Introducer,
        MJD_AdjustedTo,
        MJD_LR,
        MJD_PvCode,
        MJD_PvType,
        MJD_bv,
        MJD_Pv,
        MJD_pkgbv,
        MJD_pkgPv,
        MJD_Paymode,
        MJD_PayType,
        MJD_Multiplecnt,
        MJD_MasterID,
        MJD_DTOJ,
        MJD_DOJ,
        MJD_RoyIntroducer,
        MJD_RoyAdjustedTo,
        MJD_CCTrnAmt,
        MJD_TrnTotalAmt,
        MPD_DDBank,
        MPD_DDBranch,
        MPD_DDNo,
        MPD_DDDate,
        MJD_Amount,
        MPD_Title,
        MPD_Name,
        MPD_Mobile,
        MPD_Email,
        MPD_PanCardNo,
        MPD_ApplyPan,
        MPD_ChqPayTo,
        MPD_Bank,
        MPD_Branch,
        MPD_BankAddress,
        MPD_AccType,
        MPD_AccNo,
        MPD_AccPwd,
        MPD_IFSDCode,
        MPD_FirmType,
        MPD_FirmDetail,
        MPD_NoDepend,
        MPD_ShpPinCode,
        MPD_PinSrNo,
        MPD_ReceiptNo,
        MLD_Login,
        MLD_pwd,
        MPD_Gender,
        MPD_IPAddress,
        MPD_Browser,
        MPD_BrowserVersion,
        OH_FRID,
        OH_DispatchFrID,
        OH_DispatchBy,
        MPD_Extra17,
        MPD_Extra18,
        MPD_PayReceiptMode,
        MPD_EWTransfer,
        CurrDate,
        CurrDateTime,
        MPD_FirstName,
        MPD_MiddleName,
        MPD_LastName,
        MPD_nomName,
        MPD_nomRel,
        MPD_nomBdate,
        MPD_DOB,
        MPD_Address,
        MPD_City,
        MPD_Country,
        MPD_State,
        MPD_District,
        MPD_PinCode,
        MLD_HQuestion,
        MLD_HAnswer,
        MPD_resphone,
        MPD_EWAccNo,
        MPD_Extra21,
        ImportPacName,
        ImportPaymode,
        CustomerPassword,
        PasswordSalt,
        OrgPassword
    )
    SELECT MemberNo,                   -- MJD_MemNo - varchar(50)                  
           SponsorID,                  -- MJD_Introducer - varchar(50)                  
           AdjustedToID,               -- MJD_AdjustedTo - varchar(50)                  
           position,                   -- MJD_LR - varchar(50)          
           PV_Code,                    -- MJD_PvCode - numeric(18, 0)                  
           PV_Type,                    -- MJD_PvType - numeric(18, 0)                  
           PV_BVPts,                   -- MJD_bv - numeric(18, 2)                  
           PV_PVPts,                   -- MJD_Pv - numeric(18, 2)                  
           PV_BVPts,                   -- MJD_pkgbv - numeric(18, 2)                  
           PV_PVPts,                   -- MJD_pkgPv - numeric(18, 2)                  
           Paymode_mst.PM_ID,          -- MJD_Paymode - numeric(18, 0)                  
           Paymode_mst.PM_Desc,        -- MJD_PayType - varchar(50)                  
           1,                          -- MJD_Multiplecnt - numeric(18, 0)                  
           1,                          -- MJD_MasterID - numeric(18, 0)                  
           JoiningDateTime,            -- MJD_DTOJ - varchar(50)                  
           JoiningDateTime,            -- MJD_DOJ - varchar(50)                  
           NULL,                       -- MJD_RoyIntroducer - numeric(18, 0)                  
           NULL,                       -- MJD_RoyAdjustedTo - numeric(18, 0)                  
           NULL,                       -- MJD_CCTrnAmt - numeric(18, 2)                  
           NULL,                       -- MJD_TrnTotalAmt - numeric(18, 2)                  
           '',                         -- MPD_DDBank - varchar(100)                  
           '',                         -- MPD_DDBranch - varchar(100)                  
           NULL,                       -- MPD_DDNo - numeric(18, 0)                  
           '',                         -- MPD_DDDate - varchar(50)                  
           PV_decExtra1,               -- MJD_Amount - numeric(18, 2)                  
           '',                         -- MPD_Title - varchar(10)                  
           FirstName + ' ' + LastName, -- MPD_Name - varchar(200)                  
           Phone,                      -- MPD_Mobile - varchar(20)                  
           Email,                      -- MPD_Email - varchar(50)                  
           PanCardNo,                  -- MPD_PanCardNo - varchar(50)                  
           (CASE
                WHEN ISNULL(PanCardNo, '') = '' THEN
                    'True'
                ELSE
                    'False'
            END
           ),                          -- MPD_ApplyPan - varchar(50)                  
           '',                         -- MPD_ChqPayTo - varchar(100)                  
           '',                         -- MPD_Bank - varchar(30)                  
           '',                         -- MPD_Branch - varchar(50)                  
           '',                         -- MPD_BankAddress - varchar(400)                  
           '',                         -- MPD_AccType - varchar(16)                  
           '',                         -- MPD_AccNo - varchar(25)                  
           '',                         -- MPD_AccPwd - varchar(16)                  
           '',                         -- MPD_IFSDCode - varchar(20)                  
           '',                         -- MPD_FirmType - varchar(100)                  
           '',                         -- MPD_FirmDetail - varchar(100)                  
           NULL,                       -- MPD_NoDepend - numeric(18, 0)                  
           '',                         -- MPD_ShpPinCode - varchar(100)              
           '',                         -- MPD_PinSrNo - varchar(50)                  
           '',                         -- MPD_ReceiptNo - varchar(50)                  
           UserName,                   -- MLD_Login - varchar(50)                  
           [Password],                 -- MLD_pwd - varchar(50)                  
           Gender,                     -- MPD_Gender - varchar(50)                  
           '',                         -- MPD_IPAddress - varchar(50)                  
           '',                         -- MPD_Browser - varchar(50)                  
           '',                         -- MPD_BrowserVersion - varchar(50)                  
           NULL,                       -- OH_FRID - numeric(18, 0)                  
           NULL,                       -- OH_DispatchFrID - numeric(18, 0)                  
           NULL,                       -- OH_DispatchBy - numeric(18, 0)                  
           '',                         -- MPD_Extra17 - varchar(100)                  
           '',                         -- MPD_Extra18 - varchar(100)                  
           NULL,                       -- MPD_PayReceiptMode - numeric(18, 0)                  
           NULL,                       -- MPD_EWTransfer - decimal(18, 0)                  
           GETDATE(),                  -- CurrDate - datetime                  
           GETDATE(),                  -- CurrDateTime - datetime                  
           FirstName,                  -- MPD_FirstName - varchar(100)                  
           '',                         -- MPD_MiddleName - varchar(100)                  
           LastName,                   -- MPD_LastName - varchar(100)                  
           '',                         -- MPD_nomName - varchar(100)                  
           '',                         -- MPD_nomRel - varchar(100)                  
           NULL,                       -- MPD_nomBdate - datetime                  
           DateOfBirth,                -- MPD_DOB - varchar(50)                  
           '',                         -- MPD_Address - varchar(100)                  
           City,                       -- MPD_City - varchar(100)                  
           Country,                    -- MPD_Country - varchar(100)                  
           [state],                    -- MPD_State - varchar(100)                  
           '',                         -- MPD_District - varchar(100)                  
           ZIP,                        -- MPD_PinCode - varchar(100)                  
           '',                         -- MLD_HQuestion - varchar(100)                  
           '',                         -- MLD_HAnswer - varchar(100)                  
           '',                         -- MPD_resphone - varchar(100)                  
           '',                         -- MPD_EWAccNo - varchar(100)                  
           '',                         -- MPD_Extra21 - varchar(50)                  
           Package,
           Paymode,
           CustomerPassword,
           CustomerPasswordSalt,
           OrgPassword
    FROM BulkImportProcess (NOLOCK)
        INNER JOIN PV_MST (NOLOCK)
            ON Package = PV_PDCode
        INNER JOIN Paymode_mst (NOLOCK)
            ON Paymode = PM_SystemName
    WHERE MemberNo NOT IN (
                              SELECT MJD_MemNo FROM MemJoining_Dtls (NOLOCK)
                          );

    PRINT ('  ImportRegistrationBulkDatatable  ');

    --UPDATE  ImportRegistrationBulkDatatable              
    --SET     MJD_PvCode = PV_Code ,              
    --  MJD_PvType = PV_Type ,              
    --        MJD_Paymode = Paymode_mst.PM_ID ,              
    --        MJD_PayType = Paymode_mst.PM_Desc ,              
    --        MJD_Amount = PV_decExtra1 ,              
    --        MJD_Pv = PV_PVPts ,              
    --        MJD_bv = PV_BVPts              
    --FROM    PV_MST (NOLOCK) ,         
    --        Paymode_mst (NOLOCK)              
    --WHERE   ImportPacName = PV_PDCode              
    --        AND ImportPaymode = PM_SystemName;                


    PRINT ('  prc_ImportprocessJoiningTables  ');
    -- Insert into joining Tables                    
    EXEC prc_ImportprocessJoiningTables;


    PRINT ('  prc_ImportprocessCustomerTables  ');
    -- INSERT IN CUSTOMER & Related Tables                      
    EXEC prc_ImportprocessCustomerTables;


    PRINT ('  prc_ImportprocessGenericAttribute  ');

    -- Generic Attribute & Address                   
    EXEC prc_ImportprocessGenericAttribute;



    EXEC prc_importMemDirectandStatus;
    PRINT ('  prc_importMemDirectandStatus  ');


    EXEC prc_importIDwiseandDownlineBiz;
    PRINT ('  prc_importIDwiseandDownlineBiz  ');

    EXEC prc_importGenDownlineBiz;
    PRINT ('  prc_importGenDownlineBiz  ');


    DECLARE @PlanTypeID NUMERIC(18, 0);

    SELECT @PlanTypeID = SM_Value
    FROM Settings_Mst (NOLOCK)
    WHERE SM_ID = 73;

    IF (@PlanTypeID != 2)
    BEGIN
        -- Only For Binary & Binary + Generation    
        PRINT ('  prc_ProcessImportBusiness  ');
        -- Process Business & Generic Attribute Here                          
        EXEC prc_ProcessImportBusiness;
    END;

    --****  CLEAR MLD_chvEXTRA4 from memlogin                  
    UPDATE MemLogin_Dtls
    SET MLD_chvEXTRA4 = NULL;



    --********** Add MEMJOINING TRIGGER                  

    DECLARE @JoinTrigger NVARCHAR(MAX);

    SET @JoinTrigger = '';

    SET @JoinTrigger
        = @JoinTrigger
          + '                   
CREATE TRIGGER [trg_MemberRegistration] ON [MemJoining_Dtls]                  
    FOR INSERT                  
AS                   
BEGIN                  
 DECLARE @dblMJD_MemID NUMERIC(18, 0),                  
        @chvMJD_MemNo VARCHAR(50),                  
        @intPM_ID NUMERIC(18, 0),                  
        @dtmMJD_DOJ DATETIME                  
    BEGIN                  
        DECLARE @Sm_BinaryFlag INT                   
        SET @Sm_BinaryFlag = 0           
        SELECT  @dblMJD_MemID = MJD_MemID, @chvMJD_MemNo = MJD_MemNo,                  
                @dtmMJD_DOJ = MJD_DOJ, @intPM_ID = MJD_Paymode                  
        FROM    Inserted                      
        SELECT  @Sm_BinaryFlag = ISNULL(SM_decExtra1, 0)                  
        FROM    Settings_Mst (NOLOCK)                  
        WHERE   SM_ID = 19                      
        IF @Sm_BinaryFlag = 2                   
            BEGIN                  
                EXEC prc_MemberDailyRegistration @dblMJD_MemID, @chvMJD_MemNo,                  
                    @intPM_ID, @dtmMJD_DOJ                   
            END                  
        ELSE                   
            BEGIN                  
              EXEC prc_MemberRegistration @dblMJD_MemID, @chvMJD_MemNo,                  
                    @intPM_ID, @dtmMJD_DOJ                   
            END                   
    END                  
END                   
SET ANSI_NULLS ON ';

    EXEC sp_executesql @JoinTrigger;
END;
GO
CREATE PROCEDURE BulkImportDataProcessing -- BulkImportDataProcessing ''                                  
    @ErrorMesage NVARCHAR(MAX) OUTPUT
AS
BEGIN
    -- SET NOCOUNT ON;                                  

	IF OBJECT_ID('PK_MemDownline_Dtls') IS NOT NULL
    BEGIN
        ALTER TABLE MemDownline_Dtls DROP CONSTRAINT PK_MemDownline_Dtls;
    END;
    IF OBJECT_ID('PK_MemActDownline_Dtls') IS NOT NULL
    BEGIN
        ALTER TABLE MemActDownline_Dtls DROP CONSTRAINT PK_MemActDownline_Dtls;
    END;
    DECLARE @Joining_Plan NVARCHAR(100),
            @DuplicateMemNoList NVARCHAR(MAX),
            @DuplicateUserList NVARCHAR(MAX),
            @AdjustIDList NVARCHAR(MAX),
            @SponserIDList NVARCHAR(MAX),
            @PV_PDCodeList NVARCHAR(MAX),
            @AdjustIDCount NVARCHAR(MAX),
            @SingleErrorMsg NVARCHAR(MAX);
    SET @ErrorMesage = '';

    SET @SingleErrorMsg = '';
    IF OBJECT_ID('ImportMemJoiningDtls', 'U') IS NOT NULL
    BEGIN
        DROP TABLE ImportMemJoiningDtls;
    END;

    CREATE TABLE ImportMemJoiningDtls
    (
        [MJD_MemID] [NUMERIC](18, 0) NULL,
        [MJD_MemNo] [VARCHAR](50) NULL,
        [MJD_DOJ] [SMALLDATETIME] NULL,
        [MJD_DTOJ] [DATETIME] NULL,
        [MJD_Introducer] [NUMERIC](18, 0) NULL,
        [MJD_AdjustedTo] [NUMERIC](18, 0) NULL,
        [MJD_LR] [VARCHAR](50) NULL
    );
    INSERT INTO ImportMemJoiningDtls
    (
        MJD_MemID,
        MJD_MemNo,
        MJD_DOJ,
        MJD_DTOJ,
        MJD_Introducer,
        MJD_AdjustedTo,
        MJD_LR
    )
    SELECT MJD_MemID,
           MJD_MemNo,
           MJD_DOJ,
           MJD_DTOJ,
           MJD_Introducer,
           MJD_AdjustedTo,
           MJD_LR
    FROM MemJoining_Dtls (NOLOCK);

    PRINT ('  ImportMemJoiningDtls  ');

    -- Below Table used for multiple condition verification                                  
    IF OBJECT_ID('ImportDOJDetails', 'U') IS NOT NULL
    BEGIN
        DROP TABLE ImportDOJDetails;
    END;

    CREATE TABLE ImportDOJDetails
    (
        RowID INT IDENTITY(1, 1) PRIMARY KEY,
        [MemNo] [VARCHAR](20) NULL,
        [DTOJ] [DATETIME] NULL,
        [Introducer] [VARCHAR](50) NULL,
        [AdjustedTo] [VARCHAR](50) NULL,
        [position] [VARCHAR](50) NULL,
        [Package] [VARCHAR](50) NULL
    );

    CREATE INDEX IX_MemNo ON ImportDOJDetails (MemNo);
    CREATE INDEX IX_AdjustedToposition
    ON ImportDOJDetails
    (
        AdjustedTo,
        position
    );
    CREATE INDEX IX_AdjustedTo ON ImportDOJDetails (AdjustedTo);
    CREATE INDEX IX_Package ON ImportDOJDetails (Package);




    INSERT INTO ImportDOJDetails
    SELECT MemberNo,
           JoiningDateTime,
           SponsorID,
           AdjustedToID,
           position,
           Package
    FROM BulkImportProcess (NOLOCK)
    ORDER BY Id;

    SELECT @Joining_Plan = JM_Name
    FROM JoiningPlan_Mst (NOLOCK);

    PRINT ('  ImportDOJDetails  ');

    -- AJS********** Check duplicate member Number present in excel           

    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_Duplicate_Member @Joining_Plan, @SingleErrorMsg OUT;


    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;

    PRINT ('  Check duplicate member  ');
    ----- AJS********** Check duplicate member Number present in memjoining table                                           
    --    SET @SingleErrorMsg = ''                                  
    --    EXEC Prc_Import_Val_MemberExistInDB @Joining_Plan, @SingleErrorMsg OUT                                   

    --    IF ( ISNULL(@SingleErrorMsg, '') != '' )                                   
    --        BEGIN                                                                                      
    --            SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg                                  
    --        END                                            
    --    PRINT ( '  Check duplicate member memjoining ' )                                   

    ----- AJS********** Check duplicate username present in customer table                                         

    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_Duplicate_UserName @Joining_Plan, @SingleErrorMsg OUT;

    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;
    PRINT ('  Check duplicate username ');
    ---- AJS********** Check duplicate username present in customer table                                       

    --SET @SingleErrorMsg = ''                                  
    --EXEC Prc_Import_Val_Duplicate_UserNameInDB @Joining_Plan, @SingleErrorMsg OUT                                   

    --IF ( ISNULL(@SingleErrorMsg, '') != '' )                                   
    --    BEGIN                                                                                      
    --        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg                                  
    --    END                                     
    --PRINT ( '  Check duplicate username  customer' )             

    --- AJS********** Check SponsorID is not present memjoining table                                       


    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_SponsorID_Exist @Joining_Plan, @SingleErrorMsg OUT;

    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;
    PRINT ('  Check SponsorID');
    --- AJS********** Check  Sponser Not After Member in Excel                                         

    --SET @SingleErrorMsg = ''                                  
    --EXEC Prc_Import_Val_SponserNotBeforeMemberinExcel @Joining_Plan,                          
    --    @SingleErrorMsg OUT                                   

    --IF ( ISNULL(@SingleErrorMsg, '') != '' )                           
    --    BEGIN                                                                                      
    --        SET @ErrorMesage = @ErrorMesage + '<br/><br/>'                          
    --   + @SingleErrorMsg                                  
    --    END                                  
    --PRINT ( '  Check Sponser Not After Member' )                                   
    --- AJS********** Check  Validation wrt AdjustedTo ID                                         

    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_wrtAdjustedToID @Joining_Plan, @SingleErrorMsg OUT;

    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;
    PRINT ('  Check Validation wrt AdjustedTo ID');
    --- AJS********** Check  Validation Sponser Exist In Upline                                  

    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_SponserExistInUpline @Joining_Plan,
                                             @SingleErrorMsg OUT;

    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;
    PRINT ('  Check Validation Sponser Exist In Upline');
    --- AJS********** Check  Validation Package                                      

    SET @SingleErrorMsg = '';
    EXEC Prc_Import_Val_Package @Joining_Plan, @SingleErrorMsg OUT;

    IF (ISNULL(@SingleErrorMsg, '') != '')
    BEGIN
        SET @ErrorMesage = @ErrorMesage + '<br/><br/>' + @SingleErrorMsg;
    END;
    PRINT ('  Check Validation Package');
    -- *******************************************************************************************************                                  

    -- Drop Temp Tables If any                                   

    IF OBJECT_ID('tempdb..#TempDOJDetails', 'U') IS NOT NULL
    BEGIN
        DROP TABLE #TempDOJDetails;
    END;

    IF OBJECT_ID('ImportMemJoiningDtls', 'U') IS NOT NULL
    BEGIN
        DROP TABLE ImportMemJoiningDtls;
    END;

    --IF @ErrorMesage = ''       
    --    BEGIN             

    --        PRINT ( '  prc_BulkImportProcessMAIN  ' )                         
    --        EXEC prc_BulkImportProcessMAIN                      
    --    END             

    IF @ErrorMesage = ''
    BEGIN
        BEGIN TRY
            BEGIN TRANSACTION;

            -- Process Bulk Data                            
            -- Process Bulk Data                            
            --ALTER TABLE [MemDownline_Dtls] DROP CONSTRAINT PK_MemDownline_Dtls                

            --ALTER TABLE MemActDownline_Dtls DROP CONSTRAINT PK_MemActDownline_Dtls                


            PRINT ('  prc_BulkImportProcessMAIN  ');
            EXEC prc_BulkImportProcessMAIN;

            --ALTER TABLE [MemDownline_Dtls]                
            --ADD CONSTRAINT PK_MemDownline_Dtls PRIMARY KEY CLUSTERED (MDD_MemID, MDD_PVCode, MDD_Level, MDD_PvType, MDD_Date);                

            --ALTER TABLE MemActDownline_Dtls                
            --ADD CONSTRAINT PK_MemActDownline_Dtls PRIMARY KEY CLUSTERED (MDD_MemID, MDD_PVCode, MDD_Level, MDD_PvType, MDD_Date, MDD_decExtra1);                     



            IF @@TRANCOUNT > 0
                COMMIT TRANSACTION;
            SET @ErrorMesage = '';

        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            TRUNCATE TABLE BulkImportProcess;
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT @ErrorMessage = ERROR_MESSAGE(),
                   @ErrorSeverity = ERROR_SEVERITY(),
                   @ErrorState = ERROR_STATE();




            SET @ErrorMesage = @ErrorMessage + '<br/>' + @ErrorSeverity + '<br/>' + @ErrorState;
        --RAISERROR ( @ErrorMessage, -- Message text.                                                                                                                                                           
        --    @ErrorSeverity, -- Severity.                                         
        --    @ErrorState -- State.                                                      
        --   );                                                                             
        END CATCH;

    END;
    ELSE
    BEGIN
        -- On Error Show Error details  & clear Temp Tables                          
        TRUNCATE TABLE BulkImportProcess;
        PRINT (@ErrorMesage);
    END;


END;
GO