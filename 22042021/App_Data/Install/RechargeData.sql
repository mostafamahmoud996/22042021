CREATE TABLE [dbo].[ServiceProvider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ServiceProvider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ServiceProvider] ON
INSERT [dbo].[ServiceProvider] ([Id], [Name], [Status]) VALUES (1, N'SMS Archarya', 0)
INSERT [dbo].[ServiceProvider] ([Id], [Name], [Status]) VALUES (2, N'HSTM/Quick Recharge', 0)
INSERT [dbo].[ServiceProvider] ([Id], [Name], [Status]) VALUES (3, N'RechargeDaddy', 1)
SET IDENTITY_INSERT [dbo].[ServiceProvider] OFF
GO
CREATE TABLE [dbo].[RechargeChargesMaster]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsAmount] [bit] NOT NULL,
[Status] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeChargesMaster] ADD CONSTRAINT [PK_RechargeChargesMaster] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
INSERT INTO RechargeChargesMaster VALUES('Surcharge',1,1)
INSERT INTO RechargeChargesMaster VALUES('Service-charge',1,1)
GO
/****** Object:  Table [dbo].[RechargeTypeMaster]    Script Date: 05/31/2018 12:40:53 ******/
CREATE TABLE [dbo].[RechargeTypeMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Status] [bit] NOT NULL,
	[ProviderId] [int] NOT NULL,
 CONSTRAINT [PK__RechargeTypeMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RechargeTypeMaster] ON
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (1, N'Prepaid Mobile Recharge', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (2, N'Postpaid Mobile/Landline/ISP Payment', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (3, N'DTH Recharge', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (4, N'Data Card Operators', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (5, N'Electricity Bill Payment', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (6, N'Gas Bill Payment', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (7, N'Insurance Premium Payment', 1, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (8, N'Water Bill Payment', 0, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (9, N'Money Transfer / DMT', 0, 3)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (10, N'Flight Booking', 0, 1)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (11, N'Bus Booking', 0, 1)
INSERT [dbo].[RechargeTypeMaster] ([Id], [Name], [Status], [ProviderId]) VALUES (12, N'Hotel Booking', 0, 1)
SET IDENTITY_INSERT [dbo].[RechargeTypeMaster] OFF
ALTER TABLE [dbo].[RechargeTypeMaster] ADD  CONSTRAINT [DF__RechargeT__Provi__42197EA5]  DEFAULT ((2)) FOR [ProviderId]
GO
ALTER TABLE [dbo].[RechargeTypeMaster]  WITH CHECK ADD  CONSTRAINT [FK_RechargeTypeMaster_ServiceProvider_ProviderId] FOREIGN KEY([ProviderId])
REFERENCES [dbo].[ServiceProvider] ([Id])
GO
ALTER TABLE [dbo].[RechargeTypeMaster] CHECK CONSTRAINT [FK_RechargeTypeMaster_ServiceProvider_ProviderId]
GO
CREATE TABLE [dbo].[RechargeWallet_trn]
(
[ET_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[ET_Memid] [numeric] (18, 0) NULL,
[ET_Date] [datetime] NOT NULL,
[ET_Amt] [numeric] (18, 2) NOT NULL,
[ET_Flag] [tinyint] NOT NULL,
[ET_Remark] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Webflag] [tinyint] NOT NULL,
[ET_chvExtra1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra4] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[et_RemarkA] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Amt1] [numeric] (18, 2) NOT NULL,
[ET_Webstatusid] [numeric] (18, 2) NOT NULL,
[ET_Flg] [numeric] (18, 2) NOT NULL,
[ET_Datecode] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_RechargeWallet_trn_ET_datecode] DEFAULT ((0)),
[ET_Type] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_RechargeWallet_trn_ET_Type] DEFAULT ((0)),
[ET_Trnflag] [tinyint] NOT NULL CONSTRAINT [DF_RechargeWallet_trn_ET_Trnflag] DEFAULT ((0)),
[ET_Datetime] [datetime] NOT NULL,
[ET_UniqueId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Balance] [numeric] (18, 2) NOT NULL,
[ET_CustomerId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeWallet_trn] ADD CONSTRAINT [PK_RechargeWallet_trn] PRIMARY KEY CLUSTERED  ([ET_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_RechargeWallet_trn_7_2082874537__K22_K5_4] ON [dbo].[RechargeWallet_trn] ([ET_CustomerId], [ET_Flag]) INCLUDE ([ET_Amt]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeWallet_trn] ADD CONSTRAINT [FK_RechargeWallet_trn_Customer] FOREIGN KEY ([ET_CustomerId]) REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[RechargeWallet_trn] ADD CONSTRAINT [FK_RechargeWallet_trn_MemJoining_Dtls] FOREIGN KEY ([ET_Memid]) REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
CREATE TRIGGER [dbo].[trg_RechargeWalletBalance] ON [dbo].[RechargeWallet_trn]
    FOR INSERT
AS
    DECLARE @dblMJD_MemID NUMERIC(18, 0),
        @ET_ID NUMERIC(18, 0),
        @ET_Type NUMERIC(18, 0),
        @Balance NUMERIC(18, 2),
        @TotCRamt NUMERIC(18, 2),
        @TotDRamt NUMERIC(18, 2)  
    BEGIN  
        SELECT  @ET_ID = ET_ID, @dblMJD_MemID = ET_CustomerId, @ET_Type = ET_Type
        FROM    Inserted  
  
        SELECT  @Balance = 0  
  
        SELECT  @TotCRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    RechargeWallet_trn
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 0  
  
        SELECT  @TotDRamt = ISNULL(SUM(ET_Amt), 0)
        FROM    RechargeWallet_trn
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_ID <= @ET_ID
                AND ET_Flag = 1  
  
        SELECT  @Balance = @TotCRamt - @TotDRamt  
  
        UPDATE  RechargeWallet_trn
        SET     ET_Balance = @Balance
        WHERE   ET_CustomerId = @dblMJD_MemID
                AND ET_Type = @ET_Type
                AND ET_ID = @ET_ID   
    END
GO
CREATE TABLE [dbo].[CreditReversal]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[TxnTypeId] [int] NOT NULL,
[ApplicationDtm] [datetime] NOT NULL,
[CustomerId] [int] NOT NULL,
[BookingId] [int] NOT NULL,
[UniqueId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EwAmount] [numeric] (18, 2) NOT NULL,
[PgAmount] [numeric] (18, 2) NOT NULL,
[Reason] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdminId] [numeric] (18, 0) NOT NULL,
[IpAddress] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Browser] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrowserVersion] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserAgent] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditReversal] ADD CONSTRAINT [PK_CreditReversal] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditReversal] ADD CONSTRAINT [FK_CreditReversal_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CreditReversal] ADD CONSTRAINT [FK_CreditReversal_User_Hdr] FOREIGN KEY ([AdminId]) REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
CREATE PROCEDURE [dbo].[prc_RechargeWalletBalance] @m INT  
AS   
    BEGIN  
        SELECT  COALESCE((SELECT    SUM(ET_Amt)  
                          FROM      RechargeWallet_trn  
                          WHERE     ET_CustomerId = @m  
                                    AND ET_Flag = 0), 0)  
                - COALESCE((SELECT  SUM(ET_Amt)  
                            FROM    RechargeWallet_trn  
                            WHERE   ET_CustomerId = @m  
                                    AND ET_Flag = 1), 0)  
    END
GO
CREATE TABLE [dbo].[GrplCashWallet_trn]
(
[ET_ID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[ET_Memid] [numeric] (18, 0) NULL,
[ET_Date] [datetime] NOT NULL,
[ET_Amt] [numeric] (18, 2) NOT NULL,
[ET_Flag] [tinyint] NOT NULL,
[ET_Remark] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Webflag] [tinyint] NOT NULL,
[ET_chvExtra1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_chvExtra4] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[et_RemarkA] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Amt1] [numeric] (18, 2) NOT NULL,
[ET_Webstatusid] [numeric] (18, 2) NOT NULL,
[ET_Flg] [numeric] (18, 2) NOT NULL,
[ET_Datecode] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_GrplCashWallet_trn_ET_datecode] DEFAULT ((0)),
[ET_Type] [numeric] (18, 0) NOT NULL CONSTRAINT [DF_GrplCashWallet_trn_ET_Type] DEFAULT ((0)),
[ET_Trnflag] [tinyint] NOT NULL CONSTRAINT [DF_GrplCashWallet_trn_ET_Trnflag] DEFAULT ((0)),
[ET_Datetime] [datetime] NOT NULL,
[ET_UniqueId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ET_Balance] [numeric] (18, 2) NOT NULL,
[ET_CustomerId] [int] NOT NULL,
[ET_ExpiryDate] [datetime] NULL,
[ET_ReferralCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GrplCashWallet_trn] ADD CONSTRAINT [PK_GrplCashWallet_trn] PRIMARY KEY CLUSTERED  ([ET_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_GrplCashWallet_trn_7_651917444__K22_K5_K23_4] ON [dbo].[GrplCashWallet_trn] ([ET_CustomerId], [ET_Flag], [ET_ExpiryDate]) INCLUDE ([ET_Amt]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GrplCashWallet_trn] ADD CONSTRAINT [FK_GrplCashWallet_trn_Customer] FOREIGN KEY ([ET_CustomerId]) REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[GrplCashWallet_trn] ADD CONSTRAINT [FK_GrplCashWallet_trn_MemJoining_Dtls] FOREIGN KEY ([ET_Memid]) REFERENCES [dbo].[MemJoining_Dtls] ([MJD_MemID])
GO
 CREATE PROCEDURE [dbo].[Prc_Ins_ServiceProvider]          
    @Id AS INT ,          
    @Name AS VARCHAR(100)      
    AS           
    BEGIN       
         IF  EXISTS ( SELECT  *          
                        FROM    ServiceProvider         
                        WHERE   Name=@Name )           
         BEGIN                                              
          UPDATE  ServiceProvider SET Status=0            
          UPDATE  ServiceProvider SET Status=1 WHERE Id=@Id                            
                                                      
         END      
         ELSE    
          BEGIN                                              
          UPDATE  ServiceProvider SET Status=0         
          SET IDENTITY_INSERT [dbo].[ServiceProvider] ON        
          INSERT INTO dbo.ServiceProvider    
                  (Id, Name, Status )    
          VALUES  ( @Id,    
                    @Name,    
                    1  -- Status - bit    
                    )                          
         SET IDENTITY_INSERT [dbo].[ServiceProvider] Off                                              
 END     
END 
GO
CREATE PROCEDURE [dbo].[prc_GrplCashWalletBalance] @m INT  
AS   
    BEGIN  
        SELECT  COALESCE((SELECT    SUM(ET_Amt)  
                          FROM      GrplCashWallet_trn  
                          WHERE     ET_CustomerId = @m  
                                    AND ET_Flag = 0  
                                    AND (ET_ExpiryDate IS NULL  
                                         OR ET_ExpiryDate >= CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 103), 103))),  
                         0) - COALESCE((SELECT  SUM(ET_Amt)  
                                        FROM    GrplCashWallet_trn  
                                        WHERE   ET_CustomerId = @m  
                                                AND ET_Flag = 1), 0)   
    END  
GO
CREATE PROCEDURE [dbo].[prc_sel_RechargeTypes]  
AS   
    BEGIN  
        SELECT  RechargeTypeMaster.Id, RechargeTypeMaster.Name,  
                RechargeTypeMaster.[Status],  
                (CASE WHEN RechargeTypeMaster.[Status] = 1 THEN 'Active'  
                      ELSE 'In-Active'  
                 END) AS [StatusName], ProviderId,  
                ServiceProvider.Name AS [ServiceProvider]  
        FROM    RechargeTypeMaster  
        INNER JOIN ServiceProvider ON RechargeTypeMaster.ProviderId = ServiceProvider.Id  
        WHERE   RechargeTypeMaster.[Status] = 1  
        ORDER BY RechargeTypeMaster.Id  
    END  
GO
CREATE TABLE [dbo].[RechargeOperatorMaster](
	[Id] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Status] [bit] NOT NULL,
	[ProviderId] [int] NOT NULL,
	[Code] [varchar](10) NULL,
 CONSTRAINT [PK_RechargeOperatorMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[TypeId] ASC,
	[ProviderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (1, 1, N'Airtel', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2, 1, N'Vodafone', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (3, 1, N'BSNL TopUp', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (4, 1, N'Reliance CDMA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (4, 4, N'Reliance NetConnect+ / 1X', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (5, 1, N'Reliance GSM', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (5, 4, N'Reliance NetConnect 3G', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (6, 1, N'Aircel', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (7, 1, N'MTNL', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (8, 1, N'Idea', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (9, 1, N'Tata Indicom', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (9, 4, N'Tata Photon+ / Whiz', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (10, 1, N'Loop Mobile', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (11, 1, N'Tata Docomo', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (11, 1, N'AIRCEL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (12, 1, N'Virgin CDMA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (12, 1, N'Airtel', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (13, 1, N'MTS', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (13, 1, N'BPL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (13, 4, N'MTS Mblaze / Mbrowse', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (14, 1, N'Virgin GSM', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (14, 1, N'BSNL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (15, 1, N'S Tel', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (15, 1, N'BSNL Validity/Special', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (16, 1, N'Uninor', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (16, 1, N'IDEA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (17, 1, N'Videocon', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (17, 1, N'LoopMobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (18, 1, N'MTNL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (18, 3, N'Dish TV DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (19, 1, N'MTNL Special', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (19, 3, N'Tata Sky DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (20, 1, N'MTS', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (20, 3, N'Big TV DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (21, 1, N'RELIANCE CDMA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (21, 3, N'Videocon DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (22, 1, N'RELIANCE GSM', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (22, 3, N'Sun DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (23, 1, N'S-TEL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (23, 3, N'Airtel DTH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (24, 1, N'Tata Indicom CDMA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (25, 1, N'MTNL TopUp', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (25, 1, N'Docomo GSM', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (26, 1, N'Reliance JIO', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (26, 1, N'Docomo GSM Special', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (27, 1, N'UNINOR', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (28, 1, N'UNINOR SPECIAL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (29, 1, N'VIDEOCON SPECIAL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (30, 1, N'VIDEOCON', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (31, 1, N'VIRGIN GSM', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (31, 2, N'Airtel', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (32, 1, N'VIRGIN CDMA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (32, 2, N'Idea', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (33, 1, N'VODAFONE', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (33, 2, N'Vodafone', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (34, 2, N'Reliance GSM', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (34, 3, N'Airtel Digital TV', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (35, 2, N'Reliance CDMA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (35, 3, N'DISH TV', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (36, 2, N'BSNL Mobile', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (36, 3, N'Reliance BIGTV', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (37, 2, N'BSNL Land line', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (37, 3, N'SUNDIRECT', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (38, 2, N'Tata Docomo GSM', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (38, 3, N'TATASKY', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (39, 2, N'Tata Indicom', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (39, 3, N'VIDEOCON DTH', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (40, 2, N'LOOP Mobile', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (40, 4, N'MTS MBlaze', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (41, 2, N'MTNL Landline', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (41, 4, N'Vodafone 3G', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (42, 2, N'Airtel Land-line', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (42, 4, N'Tata Photon Whiz', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (43, 5, N'BSES Rajdhani Power Limited - Delhi', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (44, 2, N'BSNL Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (44, 5, N'BSES Yamuna Power Limited - Delhi', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (45, 2, N'Airtel Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (45, 5, N'Tata Power Delhi Limited - Delhi', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (46, 2, N'Idea Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (46, 5, N'Reliance Energy Limited', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (47, 2, N'Vodafone Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (47, 6, N'Mahanagar Gas', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (48, 2, N'Relience CDMA Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (48, 7, N'ICICI Prudential Life Insurance', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (49, 2, N'Relience GSM Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (49, 7, N'Tata AIA Life Insurance', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (50, 2, N'TATA Indicom Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (51, 2, N'MTNL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (52, 2, N'MTNL Validity', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (52, 5, N'North Bihar Electricity', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (53, 2, N'LOOP Postpaid Mobile', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (53, 5, N'South Bihar Electricity', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (54, 2, N'BSNL Landline', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (54, 5, N'Best Electricity - Mumbai', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (55, 2, N'Airtel Landline & Broadband', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (56, 2, N'MTNL Landline', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (57, 2, N'Aircel', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (57, 2, N'MTNL Validity Landline', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (58, 2, N'MTS', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (58, 5, N'BSES Rajdhani Power Limited - DELHI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (59, 2, N'Reliance', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (59, 5, N'Reliance Energy Limited - MUMBAI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (60, 2, N'Tata Docomo', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (60, 5, N'Tata Power Delhi Distribution Limited - DELHI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (61, 2, N'Tikona', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (61, 5, N'CSEB - Chhattisgarh', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (62, 6, N'Adani Gas', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (63, 6, N'Gujarat Gas', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (64, 6, N'Indraprastha Gas', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (65, 5, N'Ajmer Vidyut Vitran Nigam - RAJASTHAN', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (65, 6, N'Mahanagar Gas Limited', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (66, 5, N'BESCOM - BENGALURU', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (66, 9, N'Money Transfer', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (67, 1, N'MTNL Mumbai', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (67, 5, N'CESC - WEST BENGAL', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (68, 1, N'MTNL Mumbai Special', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (68, 5, N'CSEB - CHHATTISGARH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (69, 1, N'Tata Walky', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (69, 5, N'Jaipur Vidyut Vitran Nigam - RAJASTHAN', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (70, 1, N'Docomo Cdma', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (70, 5, N'Jodhpur Vidyut Vitran Nigam - RAJASTHAN', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (71, 4, N'MTS MBrowse', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (71, 5, N'Madhya Kshetra Vitaran - MADHYA PRADESH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (72, 4, N'Aircel Pocket Internet', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (72, 5, N'MSEDC - MAHARASHTRA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (73, 4, N'BSNL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (73, 5, N'Noida Power - NOIDA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (74, 4, N'MTNL Delhi', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (74, 5, N'Paschim Kshetra Vitaran - MADHYA PRADESH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (75, 4, N'MTNL Mumbai', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (75, 5, N'Southern Power - ANDHRA PRADESH', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (76, 4, N'Reliance NetConnect', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (76, 5, N'Southern Power - TELANGANA', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (77, 4, N'Reliance NetConnect 3G', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (77, 5, N'Torrent Power', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (78, 4, N'Reliance NetConnect+', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (79, 4, N'Tata Photon+', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (80, 2, N'MTNL Delhi', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (81, 2, N'Tata Docomo CDMA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (82, 5, N'BSES Yamuna Power Limited - DELHI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (83, 5, N'MSEDC LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (84, 5, N'RAJASTHAN VIDYUT VITRAN NIGAM LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (85, 5, N'SOUTHERN POWER DISTRIBUTION COMPANY OF TELANGANA LIMITE', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (86, 5, N'BEST MUMBAI ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (87, 5, N'SOUTHERN POWER DISTRIBUTION COMPANY LTD OF ANDHRA PRADE', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (88, 5, N'TORRENT POWER ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (89, 5, N'BESCOM BANGALORE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (90, 5, N'MADHYA PRADESH MADHYA KSHETRA VIDYUT VITARAN COMPANY LI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (91, 5, N'NOIDA POWER COMPANY LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (92, 5, N'MADHYA PRADESH PASCHIM KSHETRA VIDYUT VITARAN INDORE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (93, 5, N'CALCUTTA ELECTRICITY SUPPLY LTD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (94, 5, N'INDIA POWER CORPORATION LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (95, 5, N'JAMSHEDPUR UTILITIES AND SERVICES COMPANY LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (96, 6, N'MAHANAGAR GAS LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (97, 6, N'IGL (INDRAPRAST GAS LIMITED) ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (98, 6, N'GUJARAT GAS COMPANY LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (99, 6, N'ADANI GAS ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (100, 7, N'TATA AIG LIFE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (101, 7, N'ICICI PRU LIFE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (102, 2, N'TIKONA INTERNET ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (103, 5, N'MSEB MUMBAI ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (104, 2, N'TATA LANDLINE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (105, 7, N'BIRLA SUN LIFE INSURANCE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (106, 2, N'RELIANCE LANDLINE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (107, 5, N'MGVCL GUJARAT ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (108, 5, N'DGVCL GUJARAT ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (109, 5, N'PGVCL GUJARAT ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (110, 5, N'UGVCL GUJARAT ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (111, 7, N'INDIA FIRST LIFE INSURANCE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (112, 5, N'JAIPUR VIDYUT VITRAN NIGAM LTD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (113, 5, N'JODHPUR VIDYUT VITRAN NIGAM LTD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (114, 6, N'GSPC GAS ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (115, 2, N'LOOP MOBILE POSTPAID ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (116, 2, N'TATA DOCOMO CDMA POSTPAID ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (117, 7, N'BHARTI AXA LIFE INSURANCE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (118, 5, N'BHAGALPUR ELECTRICITY PRIVATE LIMITED ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (119, 5, N'DNHPD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (120, 5, N'TRIPURA STATE ELECTRICITY BOARD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (121, 5, N'MP MADHYA KSHETRA BHOPAL ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (122, 5, N'MP PRADESH INDORE RURAL ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (123, 5, N'MADHYA PRADESH POORV KSHETRA JABALPUR ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (124, 5, N'DAMAN AND DIU ELECTRICITY ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (125, 5, N'MEGHALAYA ELECTRICITY ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (126, 5, N'ODISHA DISCOM ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (127, 9, N'MONEY TRANSFER ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (128, 1, N'MATRIX PRECARD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (129, 2, N'MATRIX POSTCARD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (130, 5, N'AJMER VIDYUT ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (131, 7, N'SBI LIFE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (132, 7, N'PNB METLIFE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (133, 7, N'EDELWEISS TOKIO LIFE INSURANCE ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (134, 2, N'ACT BROADBAND ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (135, 8, N'BANGALORE WATER SUPPLY ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (136, 8, N'DELHI JALBOARD ', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (137, 1, N'Reliance JIO', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (144, 5, N'UPPCL - UTTAR PRADESH', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (145, 5, N'KEDL - KOTA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (146, 5, N'UPCL - UTTARAKHAND', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (147, 5, N'SOUTHCO - ODISHA', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (148, 5, N'SNDL Power - NAGPUR', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (149, 5, N'SBPDCL - BIHAR(South Bihar Power Distribution Company Ltd)', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (150, 5, N'NBPDCL - BIHAR', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (151, 5, N'Muzaffarpur Vidyut Vitran', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (152, 5, N'India Power - WEST BENGAL', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (153, 5, N'BKESL - BIKANER', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (154, 5, N'BESL - BHARATPUR', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (155, 5, N'APEPDCL - ANDHRA PRADESH', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (156, 5, N'APDCL - ASSAM', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (157, 8, N'Municipal Corporation of Gurugram', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (158, 8, N'Urban Improvement Trust (UIT) - BHIWADI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (159, 8, N'Uttarakhand Jal Sansthan', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (160, 5, N'Tata Power - MUMBAI', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (161, 5, N'Dakshin Haryana Bijli Vitran Nigam (DHBVN)', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (162, 5, N'Uttar Haryana Bijli Vitran Nigam (UHBVN)', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (163, 5, N'Punjab State Power Corporation Ltd. (PSPCL)', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (164, 5, N'WEST BENGAL STATE ELECTRICITY DISTRIBUTION', 1, 2, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (301, 1, N'BSNL Recharge/Validity (RCV)', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (302, 1, N'BSNL 3G', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (303, 1, N'BSNL Special (STV)', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (1101, 1, N'Tata Docomo Special', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (1601, 1, N'Uninor Special', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (1701, 1, N'Videocon Special', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2501, 1, N'MTNL Recharge/Special', 1, 1, NULL)
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2503, 1, N'AIRTEL', 1, 3, N'AR')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2504, 1, N'BSNL', 1, 3, N'BS')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2505, 1, N'IDEA', 1, 3, N'ID')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2506, 1, N'VODAFONE', 1, 3, N'VF')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2507, 1, N'RELIANCE JIO', 1, 3, N'RJ')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2508, 1, N'TATA INDICOM', 1, 3, N'TI')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2509, 1, N'TATA DOCOMO', 1, 3, N'TD')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2510, 1, N'MTNL - TALKTIME', 1, 3, N'MM')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2511, 1, N'MTNL - SPECIAL TARIFF', 1, 3, N'MD')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2512, 1, N'BSNL VALIDITY/SPECIAL', 1, 3, N'BR')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2513, 1, N'DOCOMO GSM SPECIAL', 1, 3, N'TB')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2514, 1, N'UNINOR', 1, 3, N'UN')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2515, 1, N'UNINOR SPECIAL', 1, 3, N'UNS')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2516, 1, N'BSNL TOPUP (J&K)', 1, 3, N'BSK')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2517, 1, N'BSNL SPECIAL ( J&K )', 1, 3, N'BSJ')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2518, 1, N'J&K ( IDEA EXPRESS )', 1, 3, N'JKI')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2519, 1, N'JIO-JK', 1, 3, N'JKJ')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2520, 2, N'AIRTEL POSTPAID', 1, 3, N'AP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2521, 2, N'BSNL POSTPAID', 1, 3, N'BP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2522, 2, N'IDEA POSTPAID', 1, 3, N'IP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2523, 2, N'VODAFONE POSTPAID', 1, 3, N'VP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2524, 2, N'RELIANCE JIO POSTPAID', 1, 3, N'RP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2525, 2, N'TATA POSTPAID', 1, 3, N'TP')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2526, 2, N'AIRTEL LANDLINE', 1, 3, N'AL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2527, 2, N'BSNL - CORPORATE', 1, 3, N'BL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2528, 2, N'MTNL - DELHI', 1, 3, N'IL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2529, 2, N'TATA DOCOMO CDMA LANDLINE', 1, 3, N'DL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2530, 2, N'MTNL - MUMBAI', 1, 3, N'ML')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2531, 2, N'BSNL - INDIVIDUAL', 1, 3, N'BIL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2532, 3, N'AIRTEL DTH', 1, 3, N'AD')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2533, 3, N'BIG TV DTH', 1, 3, N'BT')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2534, 3, N'DISH TV DTH', 1, 3, N'DT')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2535, 3, N'TATA SKY DTH', 1, 3, N'TS')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2536, 3, N'VIDEOCON DTH', 1, 3, N'VD')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2537, 3, N'SUN TV DTH', 1, 3, N'ST')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2538, 5, N'PASCHIM GUJARAT VIJ COMPANY LIMITED PGVCL', 1, 3, N'PGE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2539, 5, N'MADHYA GUJARAT VIJ COMPANY LIMITED (MGVCL)', 1, 3, N'MGE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2540, 5, N'UTTAR GUJARAT VIJ COMPANY LIMITED (UGVCL)', 1, 3, N'UGE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2541, 5, N'DAKSHIN GUJARAT VIJ COMPANY LIMITED (DGVCL)', 1, 3, N'DGE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2542, 5, N'TORRENT POWER - AGRA', 1, 3, N'TAE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2543, 5, N'MSEDC LIMITED', 1, 3, N'MSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2544, 5, N'ADANI ELECTRICITY MUMBAI LTD', 1, 3, N'REE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2545, 5, N'BSES RAJDHANI POWER LIMITED', 1, 3, N'BRE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2546, 5, N'BSES YAMUNA POWER LIMITED', 1, 3, N'BYE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2547, 5, N'TATA POWER-DELHI', 1, 3, N'NDE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2548, 5, N'BEST UNDERTAKING - MUMBAI', 1, 3, N'BME')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2549, 5, N'NOIDA POWER COMPANY LIMITED', 1, 3, N'NNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2550, 5, N'TRIPURA STATE ELECTRICITY CORPORATION LTD', 1, 3, N'TTE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2551, 5, N'MP PASCHIM KSHETRA VIDYUT VITARAN - INDORE', 1, 3, N'MPE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2552, 5, N'JAMSHEDPUR UTILITIES AND SERVICES COMPANY LIMITED', 1, 3, N'JUE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2553, 5, N'INDIA POWER CORPORATION LIMITED - BIHAR', 1, 3, N'IBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2554, 5, N'CHHATTISGARH STATE ELECTRICITY BOARD', 1, 3, N'CCE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2555, 5, N'CALCUTTA ELECTRICITY SUPPLY LTD (CESC)', 1, 3, N'CWE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2556, 5, N'BANGALORE ELECTRICITY SUPPLY COMPANY', 1, 3, N'BBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2557, 5, N'ASSAM POWER DISTRIBUTION COMPANY LTD RAPDR', 1, 3, N'AAE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2558, 5, N'AJMER VIDYUT VITRAN NIGAM LIMITED (AVVNL)', 1, 3, N'AVE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2559, 5, N'BHARATPUR ELECTRICITY SERVICES LTD. (BESL)', 1, 3, N'BEE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2560, 5, N'BIKANER ELECTRICITY SUPPLY LIMITED (BKESL)', 1, 3, N'BKE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2561, 5, N'DAMAN AND DIU ELECTRICITY', 1, 3, N'DDE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2562, 5, N'DNH POWER DISTRIBUTION COMPANY LIMITED', 1, 3, N'DNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2563, 5, N'APEPDCL-EASTERN POWER DISTRIBUTION CO AP LTD', 1, 3, N'APE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2564, 5, N'GULBARGA ELECTRICITY SUPPLY COMPANY LIMITED GESCOM', 1, 3, N'GEE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2565, 5, N'INDIA POWER CORPORATION - WEST BENGAL', 1, 3, N'IWE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2566, 5, N'JODHPUR VIDYUT VITRAN NIGAM LIMITED (JDVVNL)', 1, 3, N'JDE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2567, 5, N'JAIPUR VIDYUT VITRAN NIGAM (JVVNL)', 1, 3, N'JIE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2568, 5, N'KOTA ELECTRICITY DISTRIBUTION LIMITED (KEDL)', 1, 3, N'KTE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2569, 5, N'MEGHALAYA POWER DIST CORP LTD', 1, 3, N'MHE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2570, 5, N'MUZAFFARPUR VIDYUT VITRAN LIMITED', 1, 3, N'MZE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2571, 5, N'NORTH BIHAR POWER DISTRIBUTION COMPANY LTD.', 1, 3, N'NBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2572, 5, N'NESCO, ODISHA', 1, 3, N'NSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2573, 5, N'SOUTH BIHAR POWER DISTRIBUTION COMPANY LTD.', 1, 3, N'SBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2574, 5, N'SNDL NAGPUR', 1, 3, N'SDE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2575, 5, N'SOUTHCO, ODISHA', 1, 3, N'STE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2576, 5, N'APSPDCL-SOUTHERN POWER DISTRIBUTION CO AP LTD', 1, 3, N'ASE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2577, 5, N'TATA POWER - MUMBAI', 1, 3, N'TME')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2578, 5, N'TAMIL NADU ELECTRICITY BOARD (TNEB)', 1, 3, N'TNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2579, 5, N'TP AJMER DISTRIBUTION LTD (TPADL)', 1, 3, N'AJE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2580, 5, N'UTTARAKHAND POWER CORPORATION LIMITED', 1, 3, N'UKE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2581, 5, N'UTTAR PRADESH POWER CORP LTD (UPPCL) - URBAN', 1, 3, N'UBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2582, 5, N'UTTAR PRADESH POWER CORP LTD (UPPCL) - RURAL', 1, 3, N'URE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2583, 5, N'WESCO UTILITY', 1, 3, N'WSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2584, 5, N'DAKSHIN HARYANA BIJLI VITRAN NIGAM (DHBVN)', 1, 3, N'DHE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2585, 5, N'PUNJAB STATE POWER CORPORATION LTD (PSPCL)', 1, 3, N'PSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2586, 5, N'HUBLI ELECTRICITY SUPPLY COMPANY LTD (HESCOM)', 1, 3, N'HNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2587, 5, N'UTTAR HARYANA BIJLI VITRAN NIGAM (UHBVN)', 1, 3, N'UHE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2588, 5, N'CHAMUNDESHWARI ELECTRICITY SUPPLY CORP LTD (CESCOM', 1, 3, N'CRE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2589, 5, N'HIMACHAL PRADESH STATE ELECTRICITY BOARD (HPSEB)', 1, 3, N'HPE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2590, 5, N'JHARKHAND BIJLI VITRAN NIGAM LTD (JBVNL)', 1, 3, N'JBL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2591, 5, N'WEST BENGAL STATE ELECTRICITY DISTRIBUTION CO. LTD', 1, 3, N'WBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2592, 5, N'TORRENT POWER - AHMEDABAD', 1, 3, N'THE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2593, 5, N'TORRENT POWER - BHIWANDI', 1, 3, N'TBE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2594, 5, N'TORRENT POWER - SURAT', 1, 3, N'TSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2595, 5, N'MP POORV KSHETRA VIDYUT VITARAN - RURAL', 1, 3, N'MRE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2596, 5, N'MP MADHYA KSHETRA VIDYUT VITARAN CO. LTD.-RURAL', 1, 3, N'MME')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2597, 5, N'MP MADHYA KSHETRA VIDYUT VITARAN CO. LTD.-URBAN', 1, 3, N'MUE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2598, 5, N'TELANGANA SOUTHERN POWER DISTRIBUTION CO LTD', 1, 3, N'TLE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2599, 5, N'SIKKIM POWER - RURAL (SKMPWR)', 1, 3, N'SPE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2600, 5, N'KANPUR ELECTRICITY SUPPLY COMPANY', 1, 3, N'KNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2601, 5, N'NDMC ELECTRICITY', 1, 3, N'NME')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2602, 5, N'GOA ELECTRICITY DEPARTMENT', 1, 3, N'GOE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2603, 5, N'ASSAM POWER DISTRIBUTION COMPANY LTD (NON-RAPDR)', 1, 3, N'ANE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2604, 5, N'M.P. POORV KSHETRA VIDYUT VITARAN - URBAN', 1, 3, N'MKE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2605, 5, N'DEPARTMENT OF POWER - NAGALAND', 1, 3, N'NNE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2606, 5, N'MESCOM - MANGALORE', 1, 3, N'MSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2607, 5, N'SIKKIM POWER (URBAN)', 1, 3, N'SUE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2608, 5, N'CESU - ODISHA', 1, 3, N'COE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2609, 5, N'KSEBL - KERALA', 1, 3, N'KSE')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2610, 5, N'POWER & ELECTRICITY DEPARTMENT - MIZORAM', 1, 3, N'PME')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2611, 6, N'ADANI GAS', 1, 3, N'AG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2612, 6, N'GUJARAT GAS COMPANY LTD', 1, 3, N'GGCL')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2613, 6, N'AAVANTIKA GAS', 1, 3, N'AVG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2614, 6, N'CENTRAL UP GAS LIMITED', 1, 3, N'CUG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2615, 6, N'CHAROTAR GAS SAHAKARI MANDALI', 1, 3, N'CGG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2616, 6, N'HARYANA CITY GAS', 1, 3, N'HCG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2617, 6, N'INDIANOIL - ADANI GAS', 1, 3, N'IAG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2618, 6, N'INDRAPRASTHA GAS', 1, 3, N'IPG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2619, 6, N'MAHANAGAR GAS', 1, 3, N'MMG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2620, 6, N'MAHARASHTRA NATURAL GAS', 1, 3, N'MNG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2621, 6, N'SABARMATI GAS', 1, 3, N'SGG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2622, 6, N'SITI ENERGY', 1, 3, N'SUG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2623, 6, N'TRIPURA NATURAL GAS', 1, 3, N'TNG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2624, 6, N'UNIQUE CENTRAL PIPED GASES', 1, 3, N'UCG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2625, 6, N'VADODARA GAS', 1, 3, N'VGG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2626, 6, N'IRM ENERGY', 1, 3, N'IRG')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2627, 8, N'BANGALORE WATER SUPPLY AND SEWERAGE BOARD', 1, 3, N'BKW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2628, 8, N'BHOPAL MUNICIPAL CORPORATION', 1, 3, N'BMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2629, 8, N'DELHI JAL BOARD', 1, 3, N'DDW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2630, 8, N'GREATER WARANGAL MUNICIPAL CORPORATION', 1, 3, N'GWW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2631, 8, N'GWALIOR MUNICIPAL CORPORATION', 1, 3, N'GMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2632, 8, N'HYDERABAD METROPOLITAN WATER SUPPLY AND SEWERAGE B', 1, 3, N'HTW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2633, 8, N'INDORE MUNICIPAL CORPORATION', 1, 3, N'IMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2634, 8, N'JABALPUR MUNICIPAL CORPORATION', 1, 3, N'JMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2635, 8, N'MUNICIPAL CORPORATION JALANDHAR', 1, 3, N'JPW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2636, 8, N'MUNICIPAL CORPORATION LUDHIANA - WATER', 1, 3, N'JWW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2637, 8, N'MUNICIPAL CORPORATION OF GURUGRAM', 1, 3, N'HGW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2638, 8, N'NEW DELHI MUNICIPAL COUNCIL (NDMC)', 1, 3, N'NDW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2639, 8, N'PUNE MUNICIPAL CORPORATION', 1, 3, N'PMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2640, 8, N'SURAT MUNICIPAL CORPORATION', 1, 3, N'SGW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2641, 8, N'UJJAIN NAGAR NIGAM - PHED', 1, 3, N'UMW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2642, 8, N'URBAN IMPROVEMENT TRUST (UIT) - BHIWADI', 1, 3, N'RBW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2643, 8, N'UTTARAKHAND JAL SANSTHAN', 1, 3, N'UUW')
INSERT [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [Name], [Status], [ProviderId], [Code]) VALUES (2644, 8, N'SILVASSA MUNICIPAL COUNCIL', 1, 3, N'SMW')
/****** Object:  Default [DF__RechargeO__Provi__4E7F558A]    Script Date: 06/18/2018 18:03:51 ******/
ALTER TABLE [dbo].[RechargeOperatorMaster] ADD  DEFAULT ((1)) FOR [ProviderId]
GO
/****** Object:  ForeignKey [FK_RechargeOperatorMaster_RechargeTypeMaster]    Script Date: 06/18/2018 18:03:51 ******/
ALTER TABLE [dbo].[RechargeOperatorMaster]  WITH CHECK ADD  CONSTRAINT [FK_RechargeOperatorMaster_RechargeTypeMaster] FOREIGN KEY([TypeId])
REFERENCES [dbo].[RechargeTypeMaster] ([Id])
GO
ALTER TABLE [dbo].[RechargeOperatorMaster] CHECK CONSTRAINT [FK_RechargeOperatorMaster_RechargeTypeMaster]
GO
/****** Object:  ForeignKey [FK_RechargeOperatorMaster_ServiceProvider_ProviderId]    Script Date: 06/18/2018 18:03:51 ******/
ALTER TABLE [dbo].[RechargeOperatorMaster]  WITH CHECK ADD  CONSTRAINT [FK_RechargeOperatorMaster_ServiceProvider_ProviderId] FOREIGN KEY([ProviderId])
REFERENCES [dbo].[ServiceProvider] ([Id])
GO
ALTER TABLE [dbo].[RechargeOperatorMaster] CHECK CONSTRAINT [FK_RechargeOperatorMaster_ServiceProvider_ProviderId]
GO
/****** Object:  Table [dbo].[RechargeTypeMaster]    Script Date: 05/31/2018 12:40:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RechargeChargesDetails]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ChargeId] [int] NOT NULL,
[TypeId] [int] NOT NULL,
[OperatorId] [int] NOT NULL,
[Value] [numeric] (18, 2) NOT NULL,
[ProviderId] [int] NOT NULL CONSTRAINT [DF__RechargeC__Provi__45EA0F89] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeChargesDetails] ADD CONSTRAINT [PK_RechargeChargesDetails] PRIMARY KEY CLUSTERED  ([ProviderId], [ChargeId], [TypeId], [OperatorId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeChargesDetails] ADD CONSTRAINT [FK_RechargeChargesDetails_RechargeChargesMaster] FOREIGN KEY ([ChargeId]) REFERENCES [dbo].[RechargeChargesMaster] ([Id])
GO
ALTER TABLE [dbo].[RechargeChargesDetails] ADD CONSTRAINT [FK_RechargeChargesDetails_RechargeOperatorMaster] FOREIGN KEY ([OperatorId], [TypeId], [ProviderId]) REFERENCES [dbo].[RechargeOperatorMaster] ([Id], [TypeId], [ProviderId])
GO
ALTER TABLE [dbo].[RechargeChargesDetails] ADD CONSTRAINT [FK_RechargeChargesDetails_RechargeTypeMaster] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[RechargeTypeMaster] ([Id])
GO
ALTER TABLE [dbo].[RechargeChargesDetails] ADD CONSTRAINT [FK_RechargeChargesDetails_ServiceProvider_ProviderId] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[ServiceProvider] ([Id])
GO
CREATE TABLE [dbo].[RechargeWalletDeposit_Trn]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[TxnCode] [numeric] (18, 0) NOT NULL,
[CustomerId] [int] NOT NULL,
[ApplicationDtm] [datetime] NOT NULL,
[RequestedAmount] [numeric] (18, 2) NOT NULL,
[ApprovedAmount] [numeric] (18, 2) NOT NULL,
[AccountNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BranchName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IfscCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttachmentPath] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [int] NOT NULL,
[StatusDtm] [datetime] NULL,
[AdminId] [numeric] (18, 0) NULL,
[IPAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Browser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrowserVersion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeWalletDeposit_Trn] ADD CONSTRAINT [PK_RechargeWalletDeposit_Trn] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeWalletDeposit_Trn] ADD CONSTRAINT [IX_RechargeWalletDeposit_Trn_TxnCode] UNIQUE NONCLUSTERED  ([TxnCode]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeWalletDeposit_Trn] ADD CONSTRAINT [FK_RechargeWalletDeposit_Trn_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[RechargeWalletDeposit_Trn] ADD CONSTRAINT [FK_RechargeWalletDeposit_Trn_User_Hdr] FOREIGN KEY ([AdminId]) REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
CREATE TABLE [dbo].[RechargeDtls]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UniqueId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TxnTypeId] [int] NOT NULL,
[TxnName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApplicationDtm] [datetime] NOT NULL,
[CustomerId] [int] NOT NULL,
[PaymodeId] [numeric] (18, 0) NULL,
[Paymode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumberToRecharge] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorId] [int] NOT NULL,
[Operator] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TxnAmount] [numeric] (18, 2) NOT NULL,
[TaxAmount] [numeric] (18, 2) NOT NULL,
[TotalAmount] [numeric] (18, 2) NOT NULL,
[EwAmount] [numeric] (18, 2) NOT NULL,
[PgAmount] [numeric] (18, 2) NOT NULL,
[CircleId] [int] NULL,
[Circle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpecialOperatorId] [int] NULL,
[SpecialOperator] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cycle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubDivision] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOfBirth] [datetime] NULL,
[Unit] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdminId] [numeric] (18, 0) NULL,
[GatewayTxnId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GatewayTxnDtm] [datetime] NULL,
[GatewayResponse] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RechargeTxnId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RechargeTxnDtm] [datetime] NULL,
[RechargeResponse] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [int] NOT NULL,
[Remarks] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WalletStatus] [int] NOT NULL,
[GatewayStatus] [int] NOT NULL,
[RechargeStatus] [int] NOT NULL,
[MemId] [numeric] (18, 0) NULL,
[TaxDetails] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationDate] [datetime] NOT NULL,
[ExceptionMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GatewayHash] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GatewayRequest] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RechargeRequest] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashbackInfo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashbackAmount] [numeric] (18, 2) NOT NULL CONSTRAINT [DF_RechargeDtls_CashbackAmount] DEFAULT ((0)),
[ProviderId] [int] NOT NULL CONSTRAINT [DF__RechargeD__Provi__4401C717] DEFAULT ((1)),
[TransactionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Month] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegisteredMobileNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegisteredEmailId] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeDtls] ADD CONSTRAINT [PK_RechargeDtls] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_RechargeDtls_7_1874873796__K6_K2_1_3_4_5_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31_32_] ON [dbo].[RechargeDtls] ([CustomerId], [UniqueId]) INCLUDE ([Id], [TxnTypeId], [TxnName], [ApplicationDtm], [PaymodeId], [Paymode], [NumberToRecharge], [OperatorId], [Operator], [TxnAmount], [TaxAmount], [TotalAmount], [EwAmount], [PgAmount], [CircleId], [Circle], [AccountNumber], [SpecialOperatorId], [SpecialOperator], [Cycle], [SubDivision], [DateOfBirth], [Unit], [City], [AdminId], [GatewayTxnId], [GatewayTxnDtm], [GatewayResponse], [RechargeTxnId], [RechargeTxnDtm], [RechargeResponse], [Status], [Remarks], [WalletStatus], [GatewayStatus], [RechargeStatus], [MemId], [TaxDetails], [ApplicationDate], [ExceptionMessage], [GatewayHash], [GatewayRequest], [RechargeRequest]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_RechargeDtls_7_1874873796__K6_K2_1_9] ON [dbo].[RechargeDtls] ([CustomerId], [UniqueId]) INCLUDE ([Id], [NumberToRecharge]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RechargeDtls] ADD CONSTRAINT [FK_RechargeDtls_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[RechargeDtls] ADD CONSTRAINT [FK_RechargeDtls_RechargeTypeMaster] FOREIGN KEY ([TxnTypeId]) REFERENCES [dbo].[RechargeTypeMaster] ([Id])
GO
ALTER TABLE [dbo].[RechargeDtls] ADD CONSTRAINT [FK_RechargeDtls_ServiceProvider_ProviderId] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[ServiceProvider] ([Id])
GO
ALTER TABLE [dbo].[RechargeDtls] ADD CONSTRAINT [FK_RechargeDtls_User_Hdr] FOREIGN KEY ([AdminId]) REFERENCES [dbo].[User_Hdr] ([UH_ID])
GO
--prc_sel_RechargeDtls 1,1,1,0,10  
CREATE PROCEDURE [dbo].[prc_sel_RechargeDtls]      
    @custId INT ,      
    @txnType INT ,      
    @status INT ,      
    @startRowIndex INT ,      
    @numRows INT      
AS       
    BEGIN      
        DECLARE @numresults INT ,      
            @ColumnsForQuery NVARCHAR(MAX) ,      
            @GridColumns NVARCHAR(MAX) ,      
            @QueryFromClause NVARCHAR(MAX) ,      
            @WhereConditions AS NVARCHAR(MAX) ,      
            @GroupByConditions AS NVARCHAR(MAX) ,      
            @sortExpression NVARCHAR(50) ,      
            @sortExpressionCTE NVARCHAR(50)         
      
        SELECT  @sortExpressionCTE = ' ApplicationDtm DESC ',      
                @ColumnsForQuery = ' RechargeDtls.Id, UniqueId, TxnTypeId, TxnName, ApplicationDtm, CustomerId, PaymodeId, NumberToRecharge, OperatorId,RechargeOperatorMaster.Code AS [OperatorCode], RechargeOperatorMaster.Name AS [Operator], TxnAmount,   
    
          TaxAmount, TotalAmount, EwAmount, PgAmount, CircleId, Circle, AccountNumber, SpecialOperatorId, SpecialOperator, Cycle, SubDivision,       
          DateOfBirth, Unit, City, AdminId, GatewayTxnId, GatewayTxnDtm, GatewayResponse, RechargeTxnId, RechargeTxnDtm, RechargeResponse, RechargeDtls.Status,       
          Remarks, WalletStatus, GatewayStatus, RechargeStatus, MemId, TaxDetails, ApplicationDate, CashbackInfo, CashbackAmount ',      
                @QueryFromClause = ' FROM RechargeDtls LEFT OUTER JOIN Paymode_Mst ON RechargeDtls.PaymodeId = Paymode_Mst.PM_ID LEFT OUTER JOIN RechargeOperatorMaster ON RechargeDtls.OperatorId = RechargeOperatorMaster.Id AND     
                RechargeDtls.TxnTypeId = RechargeOperatorMaster.TypeId and RechargeDtls.ProviderId=RechargeOperatorMaster.ProviderId',      
                @WhereConditions = ' WHERE CustomerId = '      
                + CAST(@custId AS VARCHAR(10)), @GroupByConditions = ''        
        SELECT  @sortExpression = @sortExpressionCTE,      
                @GridColumns = @ColumnsForQuery        
        SET @GridColumns = REPLACE(@GridColumns, 'RechargeDtls.', '')        
        SET @GridColumns = REPLACE(@GridColumns,      
                                   'RechargeOperatorMaster.Name AS [', '')  
                                    SET @GridColumns = REPLACE(@GridColumns,      
                                   'RechargeOperatorMaster.Code AS [', '')  
        SET @GridColumns = REPLACE(@GridColumns, ']', '')        
        SELECT  @ColumnsForQuery = @ColumnsForQuery      
                + ', (CASE WHEN Paymode_Mst.PM_Name IS NULL THEN Paymode ELSE Paymode_Mst.PM_Name COLLATE DATABASE_DEFAULT END) AS [PaymodeName], Paymode ',      
                @GridColumns = @GridColumns + ', PaymodeName, Paymode '        
        IF @txnType > 0       
            BEGIN      
                IF @txnType = 123456       
                    SET @WhereConditions = @WhereConditions      
                        + ' AND TxnTypeId IN (1, 2, 3, 4, 5, 6, 7) '      
                ELSE       
                    BEGIN      
                        IF @txnType = 789000       
                            SET @WhereConditions = @WhereConditions      
                                + ' AND TxnTypeId IN (8, 9, 10) '      
                        ELSE       
                            BEGIN      
                                IF @txnType = 1      
                                    OR @txnType = 2       
                                    SET @WhereConditions = @WhereConditions      
                                        + ' AND TxnTypeId IN (1, 2) '      
      
                                ELSE       
                                    SET @WhereConditions = @WhereConditions      
                                        + ' AND TxnTypeId = '      
                                        + CAST(@txnType AS VARCHAR(10))      
                            END      
                    END      
            END      
      
        IF @status > 0       
 BEGIN      
                SET @WhereConditions = @WhereConditions      
                    + ' AND RechargeDtls.Status = '      
                    + CAST(@status AS VARCHAR(10))      
            END      
    PRINT @GridColumns  
    PRINT @QueryFromClause  
    PRINT @WhereConditions  
        EXEC prc_GridResultBuilderResultsOnly @numresults OUTPUT,      
            @startRowIndex, @numRows, @sortExpression, @ColumnsForQuery,      
   @GridColumns, @QueryFromClause, @WhereConditions,      
            @GroupByConditions, @sortExpressionCTE      
    END 
GO
CREATE PROCEDURE [dbo].[prc_sel_RechargeDeductionHeads]  
AS   
    BEGIN  
        SELECT  Id, Name, IsAmount, (CASE WHEN IsAmount = 1 THEN ' (In Rs.)'  
                                          ELSE ' (In %)'  
                                     END) AS [IsAmountName], [Status],  
                (CASE WHEN [Status] = 1 THEN 'Active'  
                      ELSE 'In-Active'  
                 END) AS [StatusName]  
        FROM    RechargeChargesMaster  
        WHERE   [Status] = 1  
        ORDER BY Id  
    END  
GO
CREATE PROCEDURE [dbo].[prc_sel_RechargeDeductions]  
AS   
    BEGIN  
        SELECT  RechargeChargesDetails.Id, ChargeId,  
                RechargeChargesMaster.Name AS [Charge],  
                RechargeChargesMaster.IsAmount,  
                (CASE WHEN RechargeChargesMaster.IsAmount = 1 THEN ' (In Rs.)'  
                      ELSE ' (In %)'  
                 END) [IsAmountName], RechargeChargesDetails.TypeId,  
                RechargeTypeMaster.Name AS [OperatorType],  
                RechargeChargesDetails.OperatorId,  
                RechargeOperatorMaster.Name AS [Operator],  
                RechargeChargesDetails.Value,  
                RechargeChargesDetails.ProviderId,  
                ServiceProvider.Name AS [ServiceProvider]  
        FROM    RechargeChargesDetails  
        INNER JOIN ServiceProvider ON RechargeChargesDetails.ProviderId = ServiceProvider.Id  
        INNER JOIN RechargeChargesMaster ON RechargeChargesDetails.ChargeId = RechargeChargesMaster.Id  
        INNER JOIN RechargeTypeMaster ON RechargeChargesDetails.TypeId = RechargeTypeMaster.Id  
        INNER JOIN RechargeOperatorMaster ON RechargeChargesDetails.OperatorId = RechargeOperatorMaster.Id  
                                             AND RechargeChargesDetails.TypeId = RechargeOperatorMaster.TypeId  
                                             AND RechargeChargesDetails.ProviderId = RechargeOperatorMaster.ProviderId  
        WHERE   Value > 0  
        ORDER BY RechargeChargesDetails.ProviderId,  
                RechargeChargesDetails.ChargeId, RechargeChargesDetails.TypeId,  
                RechargeChargesDetails.OperatorId  
    END
GO
CREATE PROCEDURE [dbo].[prc_ins_RechargeDtls]  
    @Id INT OUTPUT ,  
    @UniqueId VARCHAR(100) ,  
    @actionId INT ,  
    @TxnName VARCHAR(100) ,  
    @CustomerId INT ,  
    @Paymode VARCHAR(50) ,  
    @NumberToRecharge VARCHAR(50) ,  
    @OperatorId INT ,  
    @Operator VARCHAR(100) ,  
    @RechargeAmount NUMERIC(18, 2) ,  
    @TaxAmount NUMERIC(18, 2) ,  
    @TotalAmount NUMERIC(18, 2) ,  
    @EwAmount NUMERIC(18, 2) ,  
    @PgAmount NUMERIC(18, 2) ,  
    @CircleId INT = NULL ,  
    @Circle VARCHAR(100) = NULL ,  
    @AccountNumber VARCHAR(50) = NULL ,  
    @SpecialOperatorId INT = NULL ,  
    @SpecialOperator VARCHAR(100) = NULL ,  
    @Cycle VARCHAR(100) = NULL ,  
    @SubDivision VARCHAR(100) = NULL ,  
    @DateOfBirth DATETIME = NULL ,  
    @Unit VARCHAR(100) = NULL ,  
    @City VARCHAR(100) = NULL ,  
    @AdminId NUMERIC(18, 0) = NULL ,  
    @TaxDetails VARCHAR(500) = NULL ,  
    @CashbackInfo VARCHAR(500) = NULL ,  
    @CashbackAmount NUMERIC(18, 0) = 0 ,  
    @ProviderId INT = NULL ,  
    @TransactionId VARCHAR(50) = NULL ,  
    @Address VARCHAR(500) = NULL ,  
    @Month VARCHAR(50) = NULL ,  
    @Year VARCHAR(50) = NULL ,  
    @RegisteredMobileNo VARCHAR(50) = NULL  
AS   
    BEGIN  
        DECLARE @PaymodeId INT ,  
            @MemId NUMERIC(18, 0)  
  
        IF (@Paymode = 'PG'  
            OR @Paymode = 'EWPG'  
            OR @Paymode = 'CC'  
            OR @Paymode = 'EWCC')   
            SET @Paymode = 'CC'  
  
        SELECT  @PaymodeId = PM_ID  
        FROM    Paymode_Mst  
        WHERE   Paymode_Mst.PM_Desc = @Paymode  
  
        SELECT  @MemId = MemId  
        FROM    Customer  
        WHERE   Id = @CustomerId  
  
        INSERT  INTO RechargeDtls (UniqueId, TxnTypeId, TxnName,  
                                   ApplicationDtm, CustomerId, PaymodeId,  
                                   Paymode, NumberToRecharge, OperatorId,  
                                   Operator, TxnAmount, TaxAmount, TotalAmount,  
                                   EwAmount, PgAmount, CircleId, Circle,  
                                   AccountNumber, SpecialOperatorId,  
                                   SpecialOperator, Cycle, SubDivision,  
                                   DateOfBirth, Unit, City, AdminId,  
                                   GatewayTxnId, GatewayTxnDtm,  
                                   GatewayResponse, RechargeTxnId,  
                                   RechargeTxnDtm, RechargeResponse, Status,  
                                   Remarks, WalletStatus, GatewayStatus,  
                                   RechargeStatus, MemId, TaxDetails,  
                                   ApplicationDate, CashbackInfo,  
                                   CashbackAmount, ProviderId, TransactionId,  
                                   [Address], [Month], [Year],  
                                   RegisteredMobileNo)  
        VALUES  (@UniqueId, @actionId, @TxnName, GETDATE(), @CustomerId,  
                 @PaymodeId, @Paymode, @NumberToRecharge, @OperatorId,  
                 @Operator, @RechargeAmount, @TaxAmount, @TotalAmount,  
                 @EwAmount, @PgAmount, @CircleId, @Circle, @AccountNumber,  
                 @SpecialOperatorId, @SpecialOperator, @Cycle, @SubDivision,  
                 @DateOfBirth, @Unit, @City, @AdminId, NULL, NULL, NULL, NULL,  
                 NULL, NULL, 0, NULL, 0, 0, 0, @MemId, @TaxDetails,  
                 CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 103), 103),  
                 @CashbackInfo, @CashbackAmount, @ProviderId, @TransactionId,  
                 @Address, @Month, @Year, @RegisteredMobileNo)    
        SET @Id = @@IDENTITY    
        RETURN @@ERROR  
    END
GO
CREATE PROCEDURE [dbo].[prc_ins_RechargeWallet_trn]  
    @custId INT ,  
    @uniqueId VARCHAR(100) ,  
    @flag INT ,  
    @amount NUMERIC(18, 2) ,  
    @remark VARCHAR(200) ,  
    @webFlag INT  
AS   
    BEGIN  
        DECLARE @memid NUMERIC(18, 0)  
  
        SELECT  @memid = Memid  
        FROM    Customer  
        WHERE   Id = @custId    
        INSERT  INTO RechargeWallet_trn (ET_Memid, ET_Date, ET_Amt, ET_Flag,  
                                         ET_Remark, ET_Webflag, ET_chvExtra1,  
                                         ET_chvExtra2, ET_chvExtra3,  
                                         ET_chvExtra4, et_RemarkA, ET_Amt1,  
                                         ET_Webstatusid, ET_Flg, ET_Datecode,  
                                         ET_Type, ET_Trnflag, ET_Datetime,  
                                         ET_UniqueId, ET_Balance,  
                                         ET_CustomerId)  
        VALUES  (@memid,  
                 CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 103), 103),  
                 @amount, @flag, @remark, @webFlag, 'EWallet', NULL, NULL,  
                 NULL, NULL, 0, 0, 0, 0, 0, 1, GETDATE(), @uniqueId, 0,  
                 @custId)  
  
        RETURN @@ERROR  
    END  
GO
CREATE PROCEDURE [dbo].[prc_GenerateRWRequestCode]  
    @Code VARCHAR(15) OUTPUT  
AS   
    BEGIN  
        IF EXISTS ( SELECT TOP 1  
                            Id  
                    FROM    RechargeWalletDeposit_Trn WITH (NOLOCK)  
                    ORDER BY Id )   
            BEGIN  
                SELECT  @Code = MAX(TxnCode)  
                FROM    RechargeWalletDeposit_Trn WITH (NOLOCK)  
            END  
        ELSE   
            BEGIN  
                SET @Code = 1000000000  
            END  
  
        SET @Code = @Code + 1  
        RETURN @@ERROR  
    END  
GO
CREATE PROCEDURE [dbo].[prc_ins_RechargeWalletDeposit_Trn]  
    @txnCode NUMERIC(18, 0) OUTPUT ,  
    @customerId INT ,  
    @requestedAmount NUMERIC(18, 2) ,  
    @accountNo VARCHAR(50) = NULL ,  
    @bankName VARCHAR(100) = NULL ,  
    @branchName VARCHAR(100) = NULL ,  
    @ifscCode VARCHAR(100) = NULL ,  
    @attachmentPath VARCHAR(MAX) = NULL ,  
    @adminID NUMERIC(18, 0) = NULL ,  
    @iPAddress VARCHAR(50) = NULL ,  
    @browser VARCHAR(50) = NULL ,  
    @browserVersion VARCHAR(50) = NULL  
AS   
    BEGIN  
        EXEC prc_GenerateRWRequestCode @txnCode OUTPUT      
        INSERT  INTO RechargeWalletDeposit_Trn (TxnCode, CustomerId,  
                                                ApplicationDtm,  
                                                RequestedAmount,  
                                                ApprovedAmount, AccountNo,  
                                                BankName, BranchName, IfscCode,  
                                                AttachmentPath, Status,  
                                                StatusDtm, AdminId, IPAddress,  
                                                Browser, BrowserVersion)  
        VALUES  (@txnCode, @customerId, GETDATE(), @requestedAmount, 0,  
                 @accountNo, @bankName, @branchName, @ifscCode,  
                 @attachmentPath, 0, NULL, @adminId, @iPAddress, @browser,  
                 @browserVersion)  
  
        RETURN @@ERROR                             
  
    END
GO
CREATE PROCEDURE [dbo].[prc_sel_RechargeWalletDeposit_Trn]  
    @custId INT ,  
    @status INT ,  
    @startRowIndex INT ,  
    @numRows INT  
AS   
    BEGIN  
        DECLARE @numresults INT ,  
            @ColumnsForQuery NVARCHAR(MAX) ,  
            @GridColumns NVARCHAR(MAX) ,  
            @QueryFromClause NVARCHAR(MAX) ,  
            @WhereConditions AS NVARCHAR(MAX) ,  
            @GroupByConditions AS NVARCHAR(MAX) ,  
            @sortExpression NVARCHAR(50) ,  
            @sortExpressionCTE NVARCHAR(50)  
  
        SELECT  @sortExpressionCTE = ' ApplicationDtm DESC ',  
                @ColumnsForQuery = ' Id, TxnCode, CustomerId, ApplicationDtm, RequestedAmount, ApprovedAmount, AccountNo, BankName, BranchName, IfscCode, AttachmentPath, Status, StatusDtm, AdminId, IPAddress, Browser, BrowserVersion ',  
                @QueryFromClause = ' FROM RechargeWalletDeposit_Trn ',  
                @WhereConditions = ' WHERE CustomerId = '  
                + CAST(@custId AS VARCHAR(10)) + ' AND Status = '  
                + CAST(@status AS VARCHAR(10)), @GroupByConditions = ''  
  
        SELECT  @sortExpression = @sortExpressionCTE,  
                @GridColumns = @ColumnsForQuery  
  
        IF @status > 0   
            BEGIN  
                SELECT  @sortExpressionCTE = ' StatusDtm DESC '  
            END  
  
        EXEC prc_GridResultBuilderResultsOnly @numresults OUTPUT,  
            @startRowIndex, @numRows, @sortExpression, @ColumnsForQuery,  
            @GridColumns, @QueryFromClause, @WhereConditions,  
            @GroupByConditions, @sortExpressionCTE  
  
    END
GO
CREATE PROCEDURE [dbo].[prc_sel_RechargeWallet_Trn]    
    @custId INT ,    
    @status INT ,    
    @startRowIndex INT ,    
    @numRows INT    
AS     
    BEGIN    
        DECLARE @numresults INT ,    
            @ColumnsForQuery NVARCHAR(MAX) ,    
            @GridColumns NVARCHAR(MAX) ,    
            @QueryFromClause NVARCHAR(MAX) ,    
            @WhereConditions AS NVARCHAR(MAX) ,    
            @GroupByConditions AS NVARCHAR(MAX) ,    
            @sortExpression NVARCHAR(50) ,    
            @sortExpressionCTE NVARCHAR(50)    
    
        SELECT  @sortExpressionCTE = ' ET_Datetime DESC ',    
                @ColumnsForQuery = ' ET_ID, ET_Memid, ET_Date, ET_Amt, ET_Flag, ET_Remark, ET_Webflag, ET_chvExtra1, ET_chvExtra2, ET_chvExtra3, ET_chvExtra4, et_RemarkA, ET_Amt1, ET_Webstatusid, ET_Flg, ET_Datecode, ET_Type, 
                ET_Trnflag, ET_Datetime, ET_UniqueId, ET_Balance, ET_CustomerId ',    
                @QueryFromClause = ' FROM RechargeWallet_Trn ',    
                @WhereConditions = ' WHERE ET_CustomerId = '    
                + CAST(@custId AS VARCHAR(10)), @GroupByConditions = ''    
    
        SELECT  @sortExpression = @sortExpressionCTE,    
                @GridColumns = @ColumnsForQuery    
    
        IF @status <> 2     
            BEGIN    
                SET @WhereConditions = @WhereConditions + ' AND ET_Flag = '    
                    + CAST(@status AS VARCHAR(10))    
            END    
    
        EXEC prc_GridResultBuilderResultsOnly @numresults OUTPUT,    
            @startRowIndex, @numRows, @sortExpression, @ColumnsForQuery,    
            @GridColumns, @QueryFromClause, @WhereConditions,    
            @GroupByConditions, @sortExpressionCTE    
    
    END  
GO