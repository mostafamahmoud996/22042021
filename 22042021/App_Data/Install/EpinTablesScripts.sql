/****** Object:  Table [dbo].[PinTypes]    Script Date: 07/16/2019 14:14:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinTypes')
BEGIN
CREATE TABLE [dbo].[PinTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[WebStatus] [bit] NOT NULL,
	[RedirectToRegUpgTrn] [int] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PinVouchers]    Script Date: 07/16/2019 14:31:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinVouchers')
BEGIN
CREATE TABLE [dbo].[PinVouchers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[ActiveStatus] [bit] NOT NULL,
	[DecExtra1] [decimal](18, 2) NULL,
	[DecExtra2] [decimal](18, 2) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PinVoucherDetails]    Script Date: 07/16/2019 14:31:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinVoucherDetails')
BEGIN
CREATE TABLE [dbo].[PinVoucherDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PinVoucherId] [int] NOT NULL,
	[PinTypeId] [int] NOT NULL,
	[WebStatus] [bit] NOT NULL,
	[Validity] [int] NOT NULL,
	[DecExtra1] [decimal](18, 2) NULL,
	[DecExtra2] [decimal](18, 2) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[Pinactive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Default [DF__PinVouche__Pinac__448BD6FC]    Script Date: 07/16/2019 14:31:38 ******/
ALTER TABLE [dbo].[PinVoucherDetails] ADD  DEFAULT ((0)) FOR [Pinactive]
/****** Object:  ForeignKey [PinType_PinVoucherDetail]    Script Date: 07/16/2019 14:31:38 ******/
ALTER TABLE [dbo].[PinVoucherDetails]  WITH CHECK ADD  CONSTRAINT [PinType_PinVoucherDetail] FOREIGN KEY([PinTypeId])
REFERENCES [dbo].[PinTypes] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[PinVoucherDetails] CHECK CONSTRAINT [PinType_PinVoucherDetail]
/****** Object:  ForeignKey [PinVoucher_PinVoucherDetail]    Script Date: 07/16/2019 14:31:38 ******/
ALTER TABLE [dbo].[PinVoucherDetails]  WITH CHECK ADD  CONSTRAINT [PinVoucher_PinVoucherDetail] FOREIGN KEY([PinVoucherId])
REFERENCES [dbo].[PinVouchers] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[PinVoucherDetails] CHECK CONSTRAINT [PinVoucher_PinVoucherDetail]
END
GO
/****** Object:  Table [dbo].[PinDiscounts]    Script Date: 07/16/2019 14:38:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinDiscounts')
BEGIN
CREATE TABLE [dbo].[PinDiscounts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PinVoucherDetailId] [int] NULL,
	[DiscountId] [int] NOT NULL,
	[CustomerRoleId] [int] NOT NULL,
	[DecExtra1] [decimal](18, 2) NOT NULL,
	[DecExtra2] [decimal](18, 2) NOT NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  ForeignKey [FK__PinDiscou__Custo__4D211CFD]    Script Date: 07/16/2019 14:38:22 ******/
ALTER TABLE [dbo].[PinDiscounts]  WITH CHECK ADD FOREIGN KEY([CustomerRoleId])
REFERENCES [dbo].[CustomerRole] ([Id])
/****** Object:  ForeignKey [FK__PinDiscou__Disco__4F09656F]    Script Date: 07/16/2019 14:38:22 ******/
ALTER TABLE [dbo].[PinDiscounts]  WITH CHECK ADD FOREIGN KEY([DiscountId])
REFERENCES [dbo].[Discount] ([Id])
/****** Object:  ForeignKey [FK__PinDiscou__PinVo__51E5D21A]    Script Date: 07/16/2019 14:38:22 ******/
ALTER TABLE [dbo].[PinDiscounts]  WITH CHECK ADD FOREIGN KEY([PinVoucherDetailId])
REFERENCES [dbo].[PinVoucherDetails] ([Id])
END
GO
/****** Object:  Table [dbo].[PinVoucherInPlaceOrder]    Script Date: 07/16/2019 14:31:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinVoucherInPlaceOrder')
BEGIN
CREATE TABLE [dbo].[PinVoucherInPlaceOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PinVoucherId] [int] NOT NULL,
	[VoucherDetailId] [int] NOT NULL,
	[PinVoucherName] [nvarchar](100) NOT NULL,
	[PinTypeId] [int] NOT NULL,
	[PinType] [nvarchar](100) NULL,
	[NetAmount] [decimal](18, 2) NOT NULL,
	[DiscountPercentage] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[TotalDiscount] [decimal](18, 2) NULL,
	[MemberId] [int] NULL,
	[TraderId] [int] NULL,
	[ActualDiscountPercentage] [decimal](18, 2) NULL,
	[ActualDiscountAmount] [decimal](18, 2) NULL,
	[ActualMaximumDiscountAmount] [decimal](18, 2) NULL,
	[ActualDiscountStartDate] [datetime] NULL,
	[ActualDiscountEndDate] [datetime] NULL,
	[DecExtra1] [decimal](18, 2) NULL,
	[DecExtra2] [decimal](18, 2) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PinRequests]    Script Date: 07/16/2019 14:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinRequests')
BEGIN
CREATE TABLE [dbo].[PinRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RequestNo] [nvarchar](max) NULL,
	[MemberId] [int] NULL,
	[TraderId] [int] NULL,
	[TotalQuantity] [int] NOT NULL,
	[TotalAmount] [decimal](18, 2) NOT NULL,
	[DiscountVoucherWise] [decimal](18, 2) NOT NULL,
	[DiscountOnOrderTotal] [decimal](18, 2) NOT NULL,
	[UsePercentageOrderTotal] [bit] NULL,
	[NetAmount] [decimal](18, 2) NOT NULL,
	[PaymodeId] [int] NULL,
	[ChequeNo] [nvarchar](max) NULL,
	[ChequeBank] [nvarchar](max) NULL,
	[ChequeBranch] [nvarchar](max) NULL,
	[ChequeDate] [datetime] NULL,
	[PGCharges] [decimal](18, 2) NULL,
	[PGTransactionAmount] [decimal](18, 2) NULL,
	[PGCode] [nvarchar](max) NULL,
	[RequestGeneratedBy] [int] NOT NULL,
	[TotalAssignQty] [decimal](18, 2) NULL,
	[RequestStatus] [int] NOT NULL,
	[RequestStatusDateTime] [datetime] NOT NULL,
	[FilePath] [nvarchar](max) NULL,
	[RequestFromCustomerId] [int] NOT NULL,
	[RequestToCustomerId] [int] NOT NULL,
	[ExpireUTCDateTime] [datetime] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[DispatchBy] [int] NULL,
	[DispatchByCustomerId] [int] NULL,
	[DispatchQuantity] [int] NULL,
	[DispatchRemark] [nvarchar](max) NULL,
	[DispatchFromIPAddress] [nvarchar](50) NULL,
	[DispatchDateTime] [datetime] NULL,
	[IPAddress] [nvarchar](max) NULL,
	[Browser] [nvarchar](max) NULL,
	[BrowserVersion] [nvarchar](max) NULL,
	[DecExtra1] [decimal](18, 2) NULL,
	[DecExtra2] [decimal](18, 2) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__PinReque__3214EC070F23EA841] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End
GO
/****** Object:  Table [dbo].[PinRequestDetails]    Script Date: 07/16/2019 14:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinRequestDetails')
BEGIN
CREATE TABLE [dbo].[PinRequestDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PinRequestId] [int] NOT NULL,
	[PinVoucherId] [int] NOT NULL,
	[PinTypeId] [int] NOT NULL,
	[PinQuantity] [int] NOT NULL,
	[PinPrice] [decimal](18, 2) NOT NULL,
	[DiscountPerPin] [decimal](18, 2) NOT NULL,
	[NetAmount] [decimal](18, 2) NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[PayableAmout] [decimal](18, 2) NOT NULL,
	[DispatchQuantity] [int] NULL,
	[UsePercentage] [bit] NULL,
	[DecExtra1] [decimal](18, 2) NULL,
	[DecExtra2] [decimal](18, 2) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__PinReque__3214EC071D72091DB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  ForeignKey [PinRequestDetail_PinType]    Script Date: 07/16/2019 14:20:17 ******/
ALTER TABLE [dbo].[PinRequestDetails]  WITH CHECK ADD  CONSTRAINT [PinRequestDetail_PinType] FOREIGN KEY([PinTypeId])
REFERENCES [dbo].[PinTypes] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[PinRequestDetails] CHECK CONSTRAINT [PinRequestDetail_PinType]
End
GO
/****** Object:  Table [dbo].[PinDetails]    Script Date: 07/16/2019 14:50:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinDetails')
BEGIN
CREATE TABLE [dbo].[PinDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SerialNo] [numeric](18, 0) NULL,
	[UniqueCode] [nvarchar](100) NULL,
	[PinVoucherId] [int] NULL,
	[PinTypeId] [int] NULL,
	[CustomerRequestDetailsId] [int] NULL,
	[TraderRequestDetailsId] [int] NULL,
	[CustomerId] [int] NULL,
	[TraderId] [int] NULL,
	[PinDetailsStatus] [int] NULL,
	[SealedStatus] [int] NULL,
	[PinScratchDate] [datetime] NULL,
	[PinExpireDateTime] [datetime] NULL,
	[Remark] [nvarchar](max) NULL,
	[PinUsedCustomerId] [int] NULL,
	[PinUsedTraderId] [int] NULL,
	[PinUsedDate] [datetime] NULL,
	[PinUsedTransactionFlag] [int] NULL,
	[PinUsedRemark] [nvarchar](max) NULL,
	[DecExtra1] [numeric](18, 0) NULL,
	[DecExtra2] [numeric](18, 0) NULL,
	[ChvExtra3] [nvarchar](max) NULL,
	[ChvExtra4] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__PinDetai__3214EC0767D51339] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  ForeignKey [FK_PinDetails_PinTypes]    Script Date: 07/16/2019 14:50:01 ******/
ALTER TABLE [dbo].[PinDetails]  WITH CHECK ADD  CONSTRAINT [FK_PinDetails_PinTypes] FOREIGN KEY([PinTypeId])
REFERENCES [dbo].[PinTypes] ([Id])
ALTER TABLE [dbo].[PinDetails] CHECK CONSTRAINT [FK_PinDetails_PinTypes]
/****** Object:  ForeignKey [FK_PinDetails_PinVouchers]    Script Date: 07/16/2019 14:50:01 ******/
ALTER TABLE [dbo].[PinDetails]  WITH CHECK ADD  CONSTRAINT [FK_PinDetails_PinVouchers] FOREIGN KEY([PinVoucherId])
REFERENCES [dbo].[PinVouchers] ([Id])
ALTER TABLE [dbo].[PinDetails] CHECK CONSTRAINT [FK_PinDetails_PinVouchers]
END
GO
/****** Object:  Table [dbo].[PinTransferDetails]    Script Date: 07/16/2019 14:55:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PinTransferDetails')
BEGIN
CREATE TABLE [dbo].[PinTransferDetails](
    [Id] [int] IDENTITY(1,1) NOT NULL,
	[pinDetailId] [int] NULL,
	[TransferFrom] [int] NULL,
	[TransferTo] [int] NULL,
	[IsTrader] [bit] NULL,
	[TransferType] [int] NULL,
	[DecExtra1] [numeric](18, 0) NULL,
	[DecExtra2] [numeric](18, 0) NULL,
	[ChvExtra3] [nvarchar](10) NULL,
	[ChvExtra4] [nvarchar](10) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Remark] [varchar](50) NULL,
	
 CONSTRAINT [PK__PinTrans__3214EC070A2A2B3D] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
SET ANSI_PADDING OFF
/****** Object:  ForeignKey [FK_PinTransferDetails_PinDetails]    Script Date: 07/16/2019 14:55:32 ******/
ALTER TABLE [dbo].[PinTransferDetails]  WITH CHECK ADD  CONSTRAINT [FK_PinTransferDetails_PinDetails] FOREIGN KEY([pinDetailId])
REFERENCES [dbo].[PinDetails] ([Id])
ALTER TABLE [dbo].[PinTransferDetails] CHECK CONSTRAINT [FK_PinTransferDetails_PinDetails]
END
GO