CREATE PROCEDURE [dbo].[prc_Downlinewithposition]   --prc_Downlinewithposition 0,1001, '', 0, 0                                             
    @numresults INT OUTPUT ,
    @fromdate AS VARCHAR(100) = NULL ,
    @todate AS VARCHAR(100) = NULL ,
    @decmjd_MemID AS VARCHAR(100) = NULL ,
    @fromlevel AS VARCHAR(100) = NULL ,
    @Tolevel AS VARCHAR(100) = NULL ,
    @ddlproduct AS VARCHAR(100) = NULL ,
    @Position AS VARCHAR(100) = NULL ,
    @sortExpression NVARCHAR(50) = NULL ,
    @NumRows INT = NULL ,
    @StartRowIndex INT = NULL
AS 
    BEGIN      
           
        DECLARE @ColumnsForQuery NVARCHAR(MAX) ,
            @GridColumns NVARCHAR(MAX) ,
            @QueryFromClause NVARCHAR(MAX) ,
            @WhereConditions AS NVARCHAR(MAX) ,
            @PlanTypeCondition AS NVARCHAR(MAX)                            
   -- select plan type                            
                               
        SET @PlanTypeCondition = ( SELECT   JM_Name
                                   FROM     JoiningPlan_Mst WITH (NOLOCK)
                                 );                            
        IF @sortExpression IS NULL
            OR LTRIM(RTRIM(@sortExpression)) = '' 
            BEGIN                                 
                SET @sortExpression = 'mjd_memid'                                
            END        
        IF @PlanTypeCondition = 'Binary' 
            BEGIN          
                        
                SET @ColumnsForQuery = 'MJD_Memid, mjd_memno as [ID], mpd_name as [Name], 
               (p.pv_name + ''( '' + CONVERT(VARCHAR(20), p.pv_saleprice, 102) + '' )'') as 
              [Joining Package] , (p1.pv_name + ''( '' + CONVERT(VARCHAR(20), p1.pv_saleprice, 102) + '' )'') As [Current Package] ,
               MSM_status  as [Status]  ,MJD_DOJ as [DOJ], IDD_LEVEL AS [Level], IntroducerNo as [Sponsor ID], AdjustedToNo As [Adjusted To ID] ,
               [Position W.R.T. Me]=[dbo].[fnGetPositionWRTSponsor] (mjd_memid,1) '             
                             
                SET @GridColumns = 'RowNumber,MJD_Memid, [ID],[Name], 
              [Joining Package] , [Current Package] ,[Status]  ,[DOJ],[Level],[Sponsor ID], [Adjusted To ID] ,
               [Position W.R.T. Me]'       
            END
        IF @PlanTypeCondition = 'Generation' 
            BEGIN  
     
                SET @ColumnsForQuery = 'MJD_Memid, mjd_memno as [ID], mpd_name as [Name], 
               (p.pv_name + ''( '' + CONVERT(VARCHAR(20), p.pv_saleprice, 102) + '' )'') as  [Joining Package] , 
               (p1.pv_name + ''( '' + CONVERT(VARCHAR(20), p1.pv_saleprice, 102) + '' )'') As [Current Package] ,
               MSM_status  as [Status]  ,MJD_DOJ as [DOJ], IDD_LEVEL AS [Level], IntroducerNo as [Sponsor ID]'             
                             
                SET @GridColumns = 'RowNumber,MJD_Memid, [ID],[Name],[Joining Package] , [Current Package] ,[Status]  ,[DOJ],[Level],[Sponsor ID]'       
                 
            
                                          
  
            END    
                                              
        SET @QueryFromClause = ' FROM [vw_MemberStatusDetails]   WITH ( NOLOCK ) INNER JOIN memstatus_dtls WITH ( NOLOCK ) ON memstatus_dtls.msd_memid = [vw_MemberStatusDetails].mjd_memid                                        
           INNER JOIN memstatus_mst WITH ( NOLOCK ) ON MemStatus_Dtls.MSD_MSMID = MemStatus_Mst.MSM_ID  
           INNER JOIN pv_mst p  WITH ( NOLOCK ) ON [vw_MemberStatusDetails].MJD_PvCode = p.pv_code                         
          INNER JOIN pv_mst p1 WITH ( NOLOCK ) ON [vw_MemberStatusDetails].Up_PvCode = p1.pv_code 
          INNER JOIN IDWiseDownline_Dtls WITH ( NOLOCK ) ON IDWiseDownline_Dtls.IDD_DOWNID = [vw_MemberStatusDetails].mjd_memid
             '              
                
        IF @decmjd_MemID IS NOT NULL 
            BEGIN                                                                                                      
                SET @WhereConditions = ' WHERE   IDWiseDownline_Dtls.IDD_MEMID = '
                    + @decmjd_MemID                                                                                                   
            END                                                                                                        
                                  
        IF @fromdate IS NOT NULL
            AND @fromdate <> ''
            AND @todate IS NOT NULL
            AND @todate <> '' 
            BEGIN                                                   
                SET @WhereConditions = @WhereConditions
                    + ' AND  ( CASE WHEN MSD_ConfirmDate IS NULL THEN mjd_doj                               
                            
                             ELSE mjd_doj                                  
                                  
                          END )  >= convert(datetime,''' + @fromdate
                    + ''',103) AND ( CASE WHEN MSD_ConfirmDate IS NULL THEN mjd_doj                                  
                            
                               ELSE mjd_doj                                
                            
                          END )  <= convert(datetime,''' + @todate + ''',103)'                              
            END                                                           
                            
        IF @fromlevel IS NOT NULL
            AND @fromlevel <> ''
            AND @Tolevel IS NOT NULL
            AND @Tolevel <> '' 
            BEGIN                                     
                SET @WhereConditions = @WhereConditions
                    + ' AND  IDD_LEVEL  >= ' + @fromlevel
                    + ' AND  IDD_LEVEL  <= ' + @Tolevel                                                                                                                                    
                            
            END                              
                            
        IF @ddlproduct IS NOT NULL
            AND @ddlproduct <> ''
            AND @ddlproduct <> 'Select Package' 
            BEGIN              
                               
                SET @WhereConditions = @WhereConditions
                    + ' AND  mjd_pvcode  = ' + @ddlproduct                                                                  
                            
            END                    
                                  
        IF @Position IS NOT NULL
            AND @Position <> ''
            AND @Position <> 'All' 
            BEGIN                                                                                                      
                           
                SET @WhereConditions = @WhereConditions
                    + ' AND  [vw_MemberStatusDetails].MJD_LR  = '''
                    + @Position + ''' '                                                                 
                            
            END                                              
            
        DECLARE @numresults1 INT                                                                      
        EXEC prc_GridResultBuilderResultsOnly @numresults1 OUTPUT,
            @StartRowIndex, @NumRows, @sortExpression, @ColumnsForQuery,
            @GridColumns, @QueryFromClause, @WhereConditions, NULL,
            ' MJD_Memid desc '                                                                                     
          
        SET @numresults = @numresults1                  
        SELECT  @numresults AS [Total ROW COUNT]              
    END
GO
CREATE PROCEDURE prc_app_PluginStatus -- prc_app_PluginStatus 'Plugins.Widgets.Blog'  
-- @keyName VARCHAR(50)  
AS   
BEGIN  
    SELECT  *  
    FROM    (SELECT 1 AS [Id],  
                    'Plugins.Widgets.ImageGallery' AS [ResourceName],  
                    'imagegallery/folderlist' AS [Url],  
                    ISNULL((SELECT  1  
                            FROM    Setting WITH (NOLOCK)  
                            WHERE   Id = 619 AND Value LIKE '%Widgets.ImageGallery%'),  
                           0) AS [Enabled]  
             UNION  
             SELECT 2 AS [Id],  
                    'Plugins.Widgets.VideoGallery' AS [ResourceName],  
                    'videogallery/folderlist' AS [Url],  
                    ISNULL((SELECT  1  
                            FROM    Setting WITH (NOLOCK)  
                            WHERE   Id = 619 AND Value LIKE '%Widgets.VideoGallery%'),  
                           0) AS [Enabled]  
             UNION  
             SELECT 3 AS [Id], 'Plugins.Widgets.EventModule' AS [ResourceName],  
                    'eventmodule/folderlist' AS [Url],  
                    ISNULL((SELECT  1  
                            FROM    Setting WITH (NOLOCK)  
                            WHERE   Id = 619 AND Value LIKE '%Widgets.EventModule%'),  
                           0) AS [Enabled]  
             UNION  
             SELECT 4 AS [Id], 'Plugins.Widgets.Blog' AS [ResourceName], 'blog' AS [Url],  
                    ISNULL((SELECT  [Enabled]  
                            FROM    SS_MM_MenuItem WITH (NOLOCK)  
                            INNER JOIN SS_MM_Menu WITH (NOLOCK) ON SS_MM_MenuItem.MenuId = SS_MM_Menu.Id  
                            WHERE   Type IN (320) AND Enabled = 1), 0) AS [Enabled]  
             UNION  
             SELECT 5 AS [Id], 'Plugins.Widgets.News' AS [ResourceName], 'news' AS [Url],  
                    ISNULL((SELECT  [Enabled]  
                            FROM    SS_MM_MenuItem WITH (NOLOCK)  
                            INNER JOIN SS_MM_Menu WITH (NOLOCK) ON SS_MM_MenuItem.MenuId = SS_MM_Menu.Id  
                            WHERE   Type IN (325) AND Enabled = 1), 0) AS [Enabled]  
             UNION  
             SELECT 6 AS [Id], 'Plugins.Widgets.Forums' AS [ResourceName], 'boards' AS [Url],  
                    ISNULL((SELECT  [Enabled]  
                            FROM    SS_MM_MenuItem WITH (NOLOCK)  
                            INNER JOIN SS_MM_Menu WITH (NOLOCK) ON SS_MM_MenuItem.MenuId = SS_MM_Menu.Id  
                            WHERE   Type IN (330) AND Enabled = 1), 0) AS [Enabled]) CTE  
    --WHERE   ResourceName = @keyName  
END  
GO
CREATE VIEW vw_TreeViewInfo    
AS    
    SELECT  MemJoining_Dtls.MJD_LR AS 'MemPosition' ,    
            NULL AS 'MemLevel' ,    
            MemJoining_Dtls.MJD_MemID AS 'MemId' ,    
            Customer.Id AS 'CustomerId' ,    
            MemJoining_Dtls.MJD_MemNo AS 'MemNo' ,    
            MemProfile_Dtls.MPD_Name AS 'MemName' ,    
            MemJoining_Dtls.MJD_Amount AS 'MemJoiningAmount' ,    
            MemStatus_Dtls.MSD_ConfFlg AS 'MemConfFlag' ,    
            MemStatus_Dtls.MSD_ConfirmDate AS 'MemConfDate' ,    
            --GenoDesc_Mst.GM_ID AS 'MemImgTypeId' ,    
            CASE WHEN ( MemStatus_Dtls.MSD_CurrStatusID ) = 2 THEN ( SELECT    
                                                              GM_ID    
                                                              FROM    
                                                              GenoDesc_Mst    
                                                              WHERE    
                                                              GM_ID = 6    
                                                              )    
                 ELSE  GenoDesc_Mst.GM_ID    
            END AS 'MemImgTypeId' ,    
            --GenoDesc_Mst.GM_Name AS 'MemStatus' ,    
            CASE WHEN ( MemStatus_Dtls.MSD_CurrStatusID ) = 2 THEN ( SELECT    
                                                              GM_Name    
                                                              FROM    
                                                              GenoDesc_Mst    
                                                              WHERE    
                                                              GM_ID = 6    
                                                              )    
                 ELSE GenoDesc_Mst.GM_Name    
            END AS 'MemStatus' ,            
                   
        --GenoDesc_Mst.GM_Image AS 'MemStatusImg',        
            MemJoining_Dtls.MJD_DOJ AS 'DOJ' ,    
            CONVERT(VARCHAR(10), MemJoining_Dtls.MJD_DOJ, 103) AS 'DOJ_DDMMYYYY' ,    
            MemProfile_Dtls.MPD_DOB AS 'DOB' ,    
            CONVERT(VARCHAR(10), MemProfile_Dtls.MPD_DOB, 103) AS 'DOB_DDMMYYYY' ,    
            MemJoining_Dtls.MJD_Introducer AS 'SponsorId' ,    
            sponMJD.MJD_MemNo AS 'SponsorMemNo' ,    
            sponMPD.MPD_Name AS 'SponsorName' ,    
            MemJoining_Dtls.MJD_AdjustedTo AS 'AdjustedToId' ,    
            adjtoMJD.MJD_MemNo AS 'AdjustedToMemNo' ,    
            adjtoMPD.MPD_Name AS 'AdjustedToName' ,    
            MemJoining_Dtls.MJD_PvCode AS 'PackageId' ,    
            PV_MST.PV_PDCode AS 'PackageCode' ,    
            PV_MST.PV_Name AS 'PackageName' ,    
            UpdPkg.Pv_Code AS 'UpgradePackageId' ,    
            UpdPkg.PV_PDCode AS 'UpgradePackageCode' ,    
            UpdPkg.PV_Name AS 'UpgradePackageName' ,    
            vwCummulativeJoiningCounts.LeftCount AS 'LeftCount' ,    
            vwCummulativeJoiningCounts.RightCount AS 'RightCount' ,    
            0 AS 'OwnCount' ,    
            vwCummulativeJoiningCounts.ConfLeftCount AS 'ConfirmLeftCount' ,    
            vwCummulativeJoiningCounts.ConfRightCount AS 'ConfirmRightCount' ,    
            0 AS 'ConfirmOwnCount' ,    
            vwCummulativeJoiningCounts.LeftPV AS 'LeftPV' ,    
            vwCummulativeJoiningCounts.RightPV AS 'RightPV' ,    
            vwCummulativeJoiningCounts.OwnPV AS 'OwnPV' ,    
            vwCummulativeJoiningCounts.ConfLeftPV AS 'ConfirmLeftPV' ,    
            vwCummulativeJoiningCounts.ConfRightPV AS 'ConfirmRightPV' ,    
            vwCummulativeJoiningCounts.ConfOwnPV AS 'ConfirmOwnPV' ,    
            vwCummulativeJoiningCounts.LeftbV AS 'LeftBV' ,    
            vwCummulativeJoiningCounts.RightbV AS 'RightBV' ,    
            vwCummulativeJoiningCounts.OwnBV AS 'OwnBV' ,    
            vwCummulativeJoiningCounts.ConfLeftbV AS 'ConfirmLeftBV' ,    
            vwCummulativeJoiningCounts.ConfRightbV AS 'ConfirmRightBV' ,    
            vwCummulativeJoiningCounts.ConfOwnBV AS 'ConfirmOwnBV' ,    
            vwCummulativeJoiningCounts.LeftSale AS 'LeftSale' ,    
vwCummulativeJoiningCounts.RightSale AS 'RightSale' ,    
           vwCummulativeJoiningCounts.OwnSale AS 'OwnSale' ,    
            vwCummulativeJoiningCounts.ConfLeftSale AS 'ConfirmLeftSale' ,    
            vwCummulativeJoiningCounts.ConfRightSale AS 'ConfirmRightSale' ,    
            vwCummulativeJoiningCounts.ConfOwnSale AS 'ConfirmOwnSale' ,    
            Offer_Mst.OM_ID AS 'AwardId' ,    
            Offer_Mst.OM_Name AS 'AwardName' ,    
            OfferAchiever_Trn.OAT_AchvDt AS 'AwardQualDate'    
    FROM    MemJoining_Dtls WITH ( NOLOCK )    
            INNER JOIN Customer WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = Customer.MemId    
            INNER JOIN MemProfile_Dtls WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = MemProfile_Dtls.MPD_MemId    
            INNER JOIN MemStatus_Dtls WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = MemStatus_Dtls.MSD_Memid    
            LEFT OUTER JOIN PV_MST WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_PvCode = PV_MST.PV_Code    
            LEFT OUTER JOIN MemStatus_Mst WITH ( NOLOCK ) ON MemStatus_Dtls.MSD_MSMID = MemStatus_Mst.MSM_ID    
            LEFT OUTER JOIN GenoDesc_Mst WITH ( NOLOCK ) ON ( CASE    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 1    
                                                              THEN 3    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 2    
                                                              THEN 2    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 5    
                                                              THEN 1    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 3    
                                                              THEN 5    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 4    
                                                              THEN 4    
                                                              WHEN ( MemStatus_Dtls.MSD_MSMID ) = 6    
                                                              THEN 6    
                                                              END ) = GenoDesc_Mst.GM_ID    
            LEFT OUTER JOIN MemJoining_Dtls sponMJD WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_Introducer = sponMJD.MJD_MemID    
            LEFT OUTER JOIN MemProfile_Dtls sponMPD WITH ( NOLOCK ) ON sponMJD.MJD_MemID = sponMPD.MPD_MemId    
            LEFT OUTER JOIN MemJoining_Dtls adjtoMJD WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_AdjustedTo = adjtoMJD.MJD_MemID    
            LEFT OUTER JOIN MemProfile_Dtls adjtoMPD WITH ( NOLOCK ) ON adjtoMJD.MJD_MemID = adjtoMPD.MPD_MemId    
            LEFT OUTER JOIN vwCummulativeJoiningCounts WITH ( NOLOCK ) ON MemJoining_Dtls.MJD_MemID = vwCummulativeJoiningCounts.MemID    
            LEFT OUTER JOIN Upgrade_Trn WITH ( NOLOCK ) ON Upgrade_Trn.UT_MemID = MemJoining_Dtls.MJD_MemID    
                                                           AND Upgrade_Trn.UT_ID = ( SELECT    
                                                              MAX(u.UT_ID)    
                                                              FROM    
                                                              Upgrade_Trn u    
                                                              WITH ( NOLOCK )    
                                                              WHERE    
                                                              u.UT_MemID = Upgrade_Trn.UT_MemID    
                                                              )    
            LEFT OUTER JOIN PV_MST UpdPkg WITH ( NOLOCK ) ON Upgrade_Trn.UT_NewPvCode = UpdPkg.PV_Code    
            LEFT OUTER JOIN Offer_Mst WITH ( NOLOCK ) ON OM_Type = 1    
                                      AND OM_Serialno = ( SELECT    
                                                              ISNULL(MAX(OfferAchiever_Trn.OAT_serialno),    
                                                              0)    
                    FROM    
                                                              OfferAchiever_Trn    
                                                              WITH ( NOLOCK )    
                                                              WHERE    
                                                              OfferAchiever_Trn.OAT_MemID = MemJoining_Dtls.MJD_MemID    
                                                              AND OfferAchiever_Trn.OAT_Status = 1    
                                                              )    
            LEFT OUTER JOIN OfferAchiever_Trn WITH ( NOLOCK ) ON OfferAchiever_Trn.OAT_MemID = MemJoining_Dtls.MJD_MemID    
                                                              AND OfferAchiever_Trn.OAT_Status = 1    
                                                              AND OfferAchiever_Trn.OAT_serialno = Offer_Mst.OM_Serialno  
GO
CREATE PROCEDURE prc_BinaryTreeView -- prc_BinaryTreeView 1, 2  
    @sessionId NUMERIC(18, 0) ,
    @memId NUMERIC(18, 0)
AS 
    BEGIN      
        SET NOCOUNT ON      
                                  
        DECLARE @counter INT ,
            @Id INT ,
            @adjToId NUMERIC(18, 0) ,
            @level INT ,
            @cnt INT ,
            @memNo VARCHAR(10) ,
            @adjustedToId VARCHAR(10) ,
            @adjustedToMemNo VARCHAR(20) ,
            @adjustedToName VARCHAR(100) ,
            @sponsorId VARCHAR(10) ,
            @sponsorMemNo VARCHAR(20) ,
            @sponsorName VARCHAR(100)      
                      
        SELECT  @counter = 1 ,
                @Id = 1 ,
                @adjToId = ( CASE WHEN @memId = 0 THEN @sessionId
                                  ELSE @memId
                             END ) ,
                @level = 0  
                          
        SELECT  @sponsorId = MJD_MemID ,
                @sponsorMemNo = MJD_MemNo ,
                @sponsorName = MPD_Name
        FROM    vw_MemJoiningDtls
        WHERE   MJD_MemID = @sessionId  
      
        IF OBJECT_ID('tempdb..#BinaryTreeView', 'U') IS NOT NULL 
            BEGIN      
            -- PRINT 'DROP TABLE #BinaryTreeView'      
                DROP TABLE #BinaryTreeView      
            END      
              
        CREATE TABLE #BinaryTreeView
            (
              [Id] INT IDENTITY(1, 1)
                       PRIMARY KEY
                       NOT NULL ,
              [MemPositionWrtSponsor] VARCHAR(10) NULL ,
              [MemPosition] VARCHAR(10) NULL ,
              [MemLevel] INT NULL ,
              [MemId] NUMERIC(18, 0) NULL ,
              [CustomerId] INT NULL ,
              [MemNo] VARCHAR(50) NULL ,
              [MemName] VARCHAR(100) NULL ,
              [MemJoiningAmount] NUMERIC(18, 2) NULL ,
              [MemConfFlag] INT NULL ,
              [MemConfDate] DATETIME NULL ,
              [MemImgTypeId] INT NULL ,
              [MemStatus] VARCHAR(50) NULL ,      
                               --[MemCurrStatusId] INT NULL,      
                               --[MemCurrStatus] VARCHAR(50) NULL,      
                               --[MemStatusImg] IMAGE NULL,      
                               --[MemGenealogyIconPath] VARCHAR(200) NULL,      
              [DOJ] DATETIME NULL ,
              [DOJ_DDMMYYYY] VARCHAR(10) NULL ,
              [DOB] DATETIME NULL ,
              [DOB_DDMMYYYY] VARCHAR(10) NULL ,
              [SponsorId] NUMERIC(18, 0) NULL ,
              [SponsorMemNo] VARCHAR(50) NULL ,
              [SponsorName] VARCHAR(100) NULL ,
              [AdjustedToId] NUMERIC(18, 0) NULL ,
              [AdjustedToMemNo] VARCHAR(50) NULL ,
              [AdjustedToName] VARCHAR(100) NULL ,
              [PackageId] NUMERIC(18, 0) NULL ,
              [PackageCode] VARCHAR(50) NULL ,
              [PackageName] VARCHAR(100) NULL ,
              [UpgradePackageId] NUMERIC(18, 0) NULL ,
              [UpgradePackageCode] VARCHAR(50) NULL ,
              [UpgradePackageName] VARCHAR(100) NULL ,
              [LeftCount] NUMERIC(18, 0) NOT NULL ,
              [RightCount] NUMERIC(18, 0) NOT NULL ,
              [OwnCount] NUMERIC(18, 0) NOT NULL ,
              [ConfirmLeftCount] NUMERIC(18, 0) NOT NULL ,
              [ConfirmRightCount] NUMERIC(18, 0) NOT NULL ,
              [ConfirmOwnCount] NUMERIC(18, 0) NOT NULL ,
              [LeftPV] NUMERIC(18, 0) NOT NULL ,
              [RightPV] NUMERIC(18, 0) NOT NULL ,
              [OwnPV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmLeftPV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmRightPV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmOwnPV] NUMERIC(18, 0) NOT NULL ,
              [LeftBV] NUMERIC(18, 0) NOT NULL ,
              [RightBV] NUMERIC(18, 0) NOT NULL ,
              [OwnBV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmLeftBV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmRightBV] NUMERIC(18, 0) NOT NULL ,
              [ConfirmOwnBV] NUMERIC(18, 0) NOT NULL ,
              [LeftSale] NUMERIC(18, 2) NOT NULL ,
              [RightSale] NUMERIC(18, 2) NOT NULL ,
              [OwnSale] NUMERIC(18, 2) NOT NULL ,
              [ConfirmLeftSale] NUMERIC(18, 2) NOT NULL ,
              [ConfirmRightSale] NUMERIC(18, 2) NOT NULL ,
              [ConfirmOwnSale] NUMERIC(18, 2) NOT NULL ,
              [AwardId] NUMERIC(18, 0) NULL ,
              [AwardName] VARCHAR(100) NULL ,
              [AwardQualDate] DATETIME NULL ,
              [IsAvailable] BIT NOT NULL ,
              [IsDirect] BIT NOT NULL
            )      
         
        INSERT  INTO #BinaryTreeView
                ( MemPositionWrtSponsor ,
                  MemPosition ,
                  MemLevel ,
                  MemId ,
                  CustomerId ,
                  MemNo ,
                  MemName ,
                  MemJoiningAmount ,
                  MemConfFlag ,
                  MemConfDate ,
                  MemImgTypeId ,
                  MemStatus , /*MemCurrStatusId,      
                               MemCurrStatus, MemStatusImg,      
                                  MemGenealogyIconPath, */
                  DOJ ,
                  DOJ_DDMMYYYY ,
                  DOB ,
                  DOB_DDMMYYYY ,
                  SponsorId ,
                  SponsorMemNo ,
                  SponsorName ,
                  AdjustedToId ,
                  AdjustedToMemNo ,
                  AdjustedToName ,
                  PackageId ,
                  PackageCode ,
                  PackageName ,
                  UpgradePackageId ,
                  UpgradePackageCode ,
                  UpgradePackageName ,
                  LeftCount ,
                  RightCount ,
                  OwnCount ,
                  ConfirmLeftCount ,
                  ConfirmRightCount ,
                  ConfirmOwnCount ,
                  LeftPV ,
                  RightPV ,
                  OwnPV ,
                  ConfirmLeftPV ,
                  ConfirmRightPV ,
                  ConfirmOwnPV ,
                  LeftBV ,
                  RightBV ,
                  OwnBV ,
                  ConfirmLeftBV ,
                  ConfirmRightBV ,
                  ConfirmOwnBV ,
                  LeftSale ,
                  RightSale ,
                  OwnSale ,
                  ConfirmLeftSale ,
                  ConfirmRightSale ,
                  ConfirmOwnSale ,
                  AwardId ,
                  AwardName ,
                  AwardQualDate ,
                  IsAvailable ,
                  IsDirect
                )
                SELECT  --@counter AS 'Id',      
                        'L' AS 'MemPositionWrtSponsor' ,
                        MemPosition ,
                        0 AS 'MemLevel' ,
                        MemId ,
                        CustomerId ,
                        MemNo ,
                        MemName ,
                        MemJoiningAmount ,
                        MemConfFlag ,
                        MemConfDate ,
                        MemImgTypeId ,
                        MemStatus , /*MemCurrStatusId, MemCurrStatus,      
                    MemStatusImg, NULL AS 'MemGenealogyIconPath',*/
                        DOJ ,
                        DOJ_DDMMYYYY ,
                        DOB ,
                        DOB_DDMMYYYY ,
                        SponsorId ,
                        SponsorMemNo ,
                        SponsorName ,
                        AdjustedToId ,
                        AdjustedToMemNo ,
                        AdjustedToName ,
                        PackageId ,
                        PackageCode ,
                        PackageName ,
                        UpgradePackageId ,
                        UpgradePackageCode ,
                        UpgradePackageName ,
                        LeftCount ,
                        RightCount ,
                        OwnCount ,
                        ConfirmLeftCount ,
                        ConfirmRightCount ,
                        ConfirmOwnCount ,
                        LeftPV ,
                        RightPV ,
                        OwnPV ,
                        ConfirmLeftPV ,
                        ConfirmRightPV ,
                        ConfirmOwnPV ,
                        LeftBV ,
                        RightBV ,
                        OwnBV ,
                        ConfirmLeftBV ,
                        ConfirmRightBV ,
                        ConfirmOwnBV ,
                        LeftSale ,
                        RightSale ,
                        OwnSale ,
                        ConfirmLeftSale ,
                        ConfirmRightSale ,
                        ConfirmOwnSale ,
                        AwardId ,
                        AwardName ,
                        AwardQualDate ,
                        0 ,
                        0
                FROM    vw_TreeViewInfo WITH ( NOLOCK )
                WHERE   vw_TreeViewInfo.MemId = @adjToId      
                  
              
 ------------------------------------------------------------------------------------------------------------------------------------------------      
      
    --PRINT 'BINARY TREE' --==================================================================================================================      
          
          
          
          
        WHILE @counter < 8 
            BEGIN      
                    --PRINT @counter      
                    --PRINT @adjToId      
                    --PRINT 'L'      
              
                SELECT  @adjustedToMemNo = NULL ,
                        @adjustedToName = NULL  
              
                SELECT  @adjustedToMemNo = MJD_MemNo ,
                        @adjustedToName = MPD_Name
                FROM    vw_MemJoiningDtls
                WHERE   MJD_MemID = @adjToId
                        AND @adjToId > 0  
              
                IF EXISTS ( SELECT  MemId
                            FROM    vw_TreeViewInfo WITH ( NOLOCK )
                            WHERE   vw_TreeViewInfo.AdjustedToId = @adjToId
                                    AND vw_TreeViewInfo.MemPosition = 'L' ) 
                    BEGIN      
                        INSERT  INTO #BinaryTreeView
                                ( MemPositionWrtSponsor ,
                                  MemPosition ,
                                  MemLevel ,
                                  MemId ,
                                  CustomerId ,
                                  MemNo ,
                                  MemName ,
                                  MemJoiningAmount ,
                                  MemConfFlag ,
                                  MemConfDate ,
                                  MemImgTypeId ,
                                  MemStatus , /*MemCurrStatusId,      
                                               MemCurrStatus,  MemStatusImg,      
                                                  MemGenealogyIconPath,*/
                                  DOJ ,
                                  DOJ_DDMMYYYY ,
                                  DOB ,
                                  DOB_DDMMYYYY ,
                                  SponsorId ,
                                  SponsorMemNo ,
                                  SponsorName ,
                                  AdjustedToId ,
                                  AdjustedToMemNo ,
                                  AdjustedToName ,
                                  PackageId ,
                                  PackageCode ,
                                  PackageName ,
                                  UpgradePackageId ,
                                  UpgradePackageCode ,
                                  UpgradePackageName ,
                                  LeftCount ,
                                  RightCount ,
                                  OwnCount ,
                                  ConfirmLeftCount ,
                                  ConfirmRightCount ,
                                  ConfirmOwnCount ,
                                  LeftPV ,
                                  RightPV ,
                                  OwnPV ,
                                  ConfirmLeftPV ,
                                  ConfirmRightPV ,
                                  ConfirmOwnPV ,
                                  LeftBV ,
                                  RightBV ,
                                  OwnBV ,
                                  ConfirmLeftBV ,
                                  ConfirmRightBV ,
                                  ConfirmOwnBV ,
                                  LeftSale ,
                                  RightSale ,
                                  OwnSale ,
                                  ConfirmLeftSale ,
                                  ConfirmRightSale ,
                                  ConfirmOwnSale ,
                                  AwardId ,
                                  AwardName ,
                                  AwardQualDate ,
                                  IsAvailable ,
                                  IsDirect
                                )
                                SELECT  --@counter AS 'Id',      
                                        NULL AS 'MemPositionWrtSponsor' ,
                                        NULL AS 'MemPosition' ,
                                        NULL AS 'MemLevel' ,
                                        MemId ,
                                        CustomerId ,
                                        MemNo ,
                                        MemName ,
                                        MemJoiningAmount ,
                                        MemConfFlag ,
                                        MemConfDate ,
                                        MemImgTypeId ,
                                        MemStatus , /*MemCurrStatusId,      
                                    MemCurrStatus, MemStatusImg,      
                                    NULL AS 'MemGenealogyIconPath',*/
                                        DOJ ,
                                        DOJ_DDMMYYYY ,
                                        DOB ,
                                        DOB_DDMMYYYY ,
                                        SponsorId ,
                                        SponsorMemNo ,
                                        SponsorName ,
                                        AdjustedToId ,
                                        AdjustedToMemNo ,
                                        AdjustedToName ,
                                        PackageId ,
                                        PackageCode ,
                                        PackageName ,
                                        UpgradePackageId ,
                                        UpgradePackageCode ,
                                        UpgradePackageName ,
                                        LeftCount ,
                                        RightCount ,
                                        OwnCount ,
                                        ConfirmLeftCount ,
                                        ConfirmRightCount ,
                                        ConfirmOwnCount ,
                                        LeftPV ,
                                        RightPV ,
                                        OwnPV ,
                                        ConfirmLeftPV ,
                                        ConfirmRightPV ,
                                        ConfirmOwnPV ,
                                        LeftBV ,
                                        RightBV ,
                                        OwnBV ,
                                        ConfirmLeftBV ,
                                        ConfirmRightBV ,
                                        ConfirmOwnBV ,
                                        LeftSale ,
                                        RightSale ,
                                        OwnSale ,
                                        ConfirmLeftSale ,
                                        ConfirmRightSale ,
                                        ConfirmOwnSale ,
                                        AwardId ,
                                        AwardName ,
                                        AwardQualDate ,
                                        0 AS 'IsAvailable' ,
                                        0 AS 'IsDirect'
                                FROM    vw_TreeViewInfo WITH ( NOLOCK )
                                WHERE   vw_TreeViewInfo.AdjustedToId = @adjToId
                                        AND vw_TreeViewInfo.MemPosition = 'L'      
                    END      
                ELSE 
                    BEGIN      
                        INSERT  INTO #BinaryTreeView
                                ( MemPositionWrtSponsor ,
                                  MemPosition ,
                                  MemLevel ,
                                  MemId ,
                                  CustomerId ,
                                  MemNo ,
                                  MemName ,
                                  MemJoiningAmount ,
                                  MemConfFlag ,
                                  MemConfDate ,
                                  MemImgTypeId ,
                                  MemStatus , /*MemCurrStatusId,      
                                               MemCurrStatus, MemStatusImg,      
                                                  MemGenealogyIconPath,*/
                                  DOJ ,
                                  DOJ_DDMMYYYY ,
                                  DOB ,
                                  DOB_DDMMYYYY ,
                                  SponsorId ,
                                  SponsorMemNo ,
                                  SponsorName ,
                                  AdjustedToId ,
                                  AdjustedToMemNo ,
                                  AdjustedToName ,
                                  PackageId ,
                                  PackageCode ,
                                  PackageName ,
                                  UpgradePackageId ,
                                  UpgradePackageCode ,
                                  UpgradePackageName ,
                                  LeftCount ,
                                  RightCount ,
                                  OwnCount ,
                                  ConfirmLeftCount ,
                                  ConfirmRightCount ,
                                  ConfirmOwnCount ,
                                  LeftPV ,
                                  RightPV ,
                                  OwnPV ,
                                  ConfirmLeftPV ,
                                  ConfirmRightPV ,
                                  ConfirmOwnPV ,
                                  LeftBV ,
                                  RightBV ,
                                  OwnBV ,
                                  ConfirmLeftBV ,
                                  ConfirmRightBV ,
                                  ConfirmOwnBV ,
                                  LeftSale ,
                                  RightSale ,
                                  OwnSale ,
                                  ConfirmLeftSale ,
                                  ConfirmRightSale ,
                                  ConfirmOwnSale ,
                                  AwardId ,
                                  AwardName ,
                                  AwardQualDate ,
                                  IsAvailable ,
                                  IsDirect
                                )
                        VALUES  ( NULL , -- MemPositionWrtSponsor - VARCHAR(10)      
                                  NULL , -- MemPosition - VARCHAR(10)      
                                  NULL , -- MemLevel - int      
                                  NULL , -- MemId - numeric      
                                  NULL , -- CustomerId - int      
                                  NULL , -- MemNo - varchar(50)      
                                  NULL , -- MemName - varchar(100)      
                                  NULL , -- MemJoiningAmount - numeric      
                                  NULL , -- MemConfFlag - int      
                                  NULL , -- MemConfDate - int      
                                  NULL , -- MemImgTypeId - int      
                                  NULL , -- MemStatus - varchar(50)      
                             --NULL, -- MemCurrStatusId - int      
                             --NULL, -- MemCurrStatus - varchar(50)      
                             --NULL, -- MemStatusImg - image      
                             --NULL, -- MemGenealogyIconPath - varchar(200)      
                                  NULL , -- DOJ - datetime      
                                  NULL , -- DOJ_DDMMYYYY - varchar(10)      
                                  NULL , -- DOB - datetime      
                                  NULL , -- DOB_DDMMYYYY - varchar(10)      
                                  @sponsorId , -- SponsorId - numeric      
                                  @sponsorMemNo , -- SponsorMemNo - varchar(50)      
                                  @sponsorName , -- SponsorName - varchar(100)      
                                  @adjToId , -- AdjustedToId - numeric      
                                  @adjustedToMemNo , -- AdjustedToMemNo - varchar(50)      
                                  @adjustedToName , -- AdjustedToName - varchar(100)      
                                  NULL , -- PackageId - numeric      
                                  NULL , -- PackageCode - varchar(100)      
                                  NULL , -- PackageName - varchar(100)      
                                  NULL , -- UpgradePackageId - numeric      
                                  NULL , -- UpgradePackageCode - varchar(100)      
                                  NULL , -- UpgradePackageName - varchar(100)      
                                  0 , -- LeftCount - numeric      
                                  0 , -- RightCount - numeric      
                                  0 , -- OwnCount - numeric      
                                  0 , -- ConfirmLeftCount - numeric      
                                  0 , -- ConfirmRightCount - numeric      
                                  0 , -- ConfirmOwnCount - numeric      
                                  0 , -- LeftPV - numeric      
                                  0 , -- RightPV - numeric      
                                  0 , -- OwnPV - numeric      
                                  0 , -- ConfirmLeftPV - numeric      
                                  0 , -- ConfirmRightPV - numeric      
                                  0 , -- ConfirmOwnPV - numeric      
                                  0 , -- LeftBV - numeric      
                                  0 , -- RightBV - numeric      
                                  0 , -- OwnBV - numeric      
                                  0 , -- ConfirmLeftBV - numeric      
                                  0 , -- ConfirmRightBV - numeric      
                                  0 , -- ConfirmOwnBV - numeric      
                                  0 , -- LeftSale - numeric     
                                  0 , -- RightSale - numeric      
                                  0 , -- OwnSale - numeric      
                                  0 , -- ConfirmLeftSale - numeric      
                                  0 , -- ConfirmRightSale - numeric      
                                  0 , -- ConfirmOwnSale - numeric      
                                  NULL , -- AwardId - numeric      
                                  NULL , -- AwardName - varchar(100)      
                                  NULL ,  -- AwardQualDate - datetime      
                                  ( CASE WHEN @adjToId > 0 THEN 1
                                         ELSE 0
                                    END ) , -- IsAvailable - bit      
                                  0 -- IsDirect - bit      
                                )      
                    END      
                              
                              
                        --------------------------------------------------      
                    --PRINT 'R'      
                              
                IF EXISTS ( SELECT  MemId
                            FROM    vw_TreeViewInfo WITH ( NOLOCK )
                            WHERE   vw_TreeViewInfo.AdjustedToId = @adjToId
                                    AND vw_TreeViewInfo.MemPosition = 'R' ) 
                    BEGIN      
                        INSERT  INTO #BinaryTreeView
                                ( MemPositionWrtSponsor ,
                                  MemPosition ,
                                  MemLevel ,
                                  MemId ,
                                  CustomerId ,
                                  MemNo ,
                                  MemName ,
                                  MemJoiningAmount ,
                                  MemConfFlag ,
                                  MemConfDate ,
                                  MemImgTypeId ,
                                  MemStatus , /*MemCurrStatusId,      
                                               MemCurrStatus,  MemStatusImg,      
                                                  MemGenealogyIconPath,*/
                                  DOJ ,
                                  DOJ_DDMMYYYY ,
                                  DOB ,
                                  DOB_DDMMYYYY ,
                                  SponsorId ,
                                  SponsorMemNo ,
                                  SponsorName ,
                                  AdjustedToId ,
                                  AdjustedToMemNo ,
                                  AdjustedToName ,
                                  PackageId ,
                                  PackageCode ,
                                  PackageName ,
                                  UpgradePackageId ,
                                  UpgradePackageCode ,
                                  UpgradePackageName ,
                                  LeftCount ,
                                  RightCount ,
                                  OwnCount ,
                                  ConfirmLeftCount ,
                                  ConfirmRightCount ,
                                  ConfirmOwnCount ,
                                  LeftPV ,
                                  RightPV ,
                                  OwnPV ,
                                  ConfirmLeftPV ,
                                  ConfirmRightPV ,
                                  ConfirmOwnPV ,
                                  LeftBV ,
                                  RightBV ,
                                  OwnBV ,
                                  ConfirmLeftBV ,
                                  ConfirmRightBV ,
                                  ConfirmOwnBV ,
                                  LeftSale ,
                                  RightSale ,
                                  OwnSale ,
                                  ConfirmLeftSale ,
                                  ConfirmRightSale ,
                                  ConfirmOwnSale ,
                                  AwardId ,
                                  AwardName ,
                                  AwardQualDate ,
                                  IsAvailable ,
                                  IsDirect
                                )
                                SELECT  --@counter AS 'Id',      
                                        NULL AS 'MemPositionWrtSponsor' ,
                                        NULL AS 'MemPosition' ,
                                        NULL AS 'MemLevel' ,
                                        MemId ,
                                        CustomerId ,
                                        MemNo ,
                                        MemName ,
                                        MemJoiningAmount ,
                                        MemConfFlag ,
                                        MemConfDate ,
                                        MemImgTypeId ,
                                        MemStatus , /*MemCurrStatusId,      
                                    MemCurrStatus, MemStatusImg,      
                                    NULL AS 'MemGenealogyIconPath',*/
                                        DOJ ,
                                        DOJ_DDMMYYYY ,
                                        DOB ,
                                        DOB_DDMMYYYY ,
                                        SponsorId ,
                                        SponsorMemNo ,
                                        SponsorName ,
                                        AdjustedToId ,
                                        AdjustedToMemNo ,
                                        AdjustedToName ,
                                        PackageId ,
                                        PackageCode ,
                                        PackageName ,
                                        UpgradePackageId ,
                                        UpgradePackageCode ,
                                        UpgradePackageName ,
                                        LeftCount ,
                                        RightCount ,
                                        OwnCount ,
                                        ConfirmLeftCount ,
                                        ConfirmRightCount ,
                                        ConfirmOwnCount ,
                                        LeftPV ,
                                        RightPV ,
                                        OwnPV ,
                                        ConfirmLeftPV ,
                                        ConfirmRightPV ,
                                        ConfirmOwnPV ,
                                        LeftBV ,
                                        RightBV ,
                                        OwnBV ,
                                        ConfirmLeftBV ,
                                        ConfirmRightBV ,
                                        ConfirmOwnBV ,
                                        LeftSale ,
                                        RightSale ,
                                        OwnSale ,
                                        ConfirmLeftSale ,
                                        ConfirmRightSale ,
                                        ConfirmOwnSale ,
                                        AwardId ,
                                        AwardName ,
                                        AwardQualDate ,
                                        0 AS 'IsAvailable' ,
                                        0 AS 'IsDirect'
                                FROM    vw_TreeViewInfo WITH ( NOLOCK )
                                WHERE   vw_TreeViewInfo.AdjustedToId = @adjToId
                                        AND vw_TreeViewInfo.MemPosition = 'R'      
                    END      
                ELSE 
                    BEGIN      
                        INSERT  INTO #BinaryTreeView
                                ( MemPositionWrtSponsor ,
                                  MemPosition ,
                                  MemLevel ,
                                  MemId ,
                                  CustomerId ,
                                  MemNo ,
                                  MemName ,
                                  MemJoiningAmount ,
                                  MemConfFlag ,
                                  MemConfDate ,
                                  MemImgTypeId ,
                                  MemStatus , /*MemCurrStatusId,      
                                               MemCurrStatus, MemStatusImg,      
                                                  MemGenealogyIconPath,*/
                                  DOJ ,
                                  DOJ_DDMMYYYY ,
                                  DOB ,
                                  DOB_DDMMYYYY ,
                                  SponsorId ,
                                  SponsorMemNo ,
                                  SponsorName ,
                                  AdjustedToId ,
                                  AdjustedToMemNo ,
                                  AdjustedToName ,
                                  PackageId ,
                                  PackageCode ,
                                  PackageName ,
                                  UpgradePackageId ,
                                  UpgradePackageCode ,
                                  UpgradePackageName ,
                                  LeftCount ,
                                  RightCount ,
                                  OwnCount ,
                                  ConfirmLeftCount ,
                                  ConfirmRightCount ,
                                  ConfirmOwnCount ,
                                  LeftPV ,
                                  RightPV ,
                                  OwnPV ,
                                  ConfirmLeftPV ,
                                  ConfirmRightPV ,
                                  ConfirmOwnPV ,
                                  LeftBV ,
                                  RightBV ,
                                  OwnBV ,
                                  ConfirmLeftBV ,
                                  ConfirmRightBV ,
                                  ConfirmOwnBV ,
                                  LeftSale ,
                                  RightSale ,
                                  OwnSale ,
                                  ConfirmLeftSale ,
                                  ConfirmRightSale ,
                                  ConfirmOwnSale ,
                                  AwardId ,
                                  AwardName ,
                                  AwardQualDate ,
                                  IsAvailable ,
                                  IsDirect
                                )
                        VALUES  ( NULL , -- MemPositionWrtSponsor - VARCHAR(10)      
                                  NULL , -- MemPosition - VARCHAR(10)      
                                  NULL , -- MemLevel - int      
                                  NULL , -- MemId - numeric      
                                  NULL , -- CustomerId - int      
                                  NULL , -- MemNo - varchar(50)      
                                  NULL , -- MemName - varchar(100)      
                                  NULL , -- MemJoiningAmount - numeric      
                                  NULL , -- MemConfFlag - int      
                                  NULL , -- MemConfDate - int      
                                  NULL , -- MemImgTypeId - int      
                                  NULL , -- MemStatus - varchar(50)      
                             --NULL, -- MemCurrStatusId - int      
                             --NULL, -- MemCurrStatus - varchar(50)      
                             --NULL, -- MemStatusImg - image      
                             --NULL, -- MemGenealogyIconPath - varchar(200)      
                                  NULL , -- DOJ - datetime      
                                  NULL , -- DOJ_DDMMYYYY - varchar(10)      
                                  NULL , -- DOB - datetime      
                                  NULL , -- DOB_DDMMYYYY - varchar(10)      
                                  @sponsorId , -- SponsorId - numeric      
                                  @sponsorMemNo , -- SponsorMemNo - varchar(50)      
                                  @sponsorName , -- SponsorName - varchar(100)      
                                  @adjToId , -- AdjustedToId - numeric      
                                  @adjustedToMemNo , -- AdjustedToMemNo - varchar(50)      
                                  @adjustedToName , -- AdjustedToName - varchar(100)  
                                  NULL , -- PackageId - numeric      
                                  NULL , -- PackageCode - varchar(100)      
                                  NULL , -- PackageName - varchar(100)      
                                  NULL , -- UpgradePackageId - numeric      
                                  NULL , -- UpgradePackageCode - varchar(100)      
                                  NULL , -- UpgradePackageName - varchar(100)      
                                  0 , -- LeftCount - numeric      
                                  0 , -- RightCount - numeric      
                                  0 , -- OwnCount - numeric      
                                  0 , -- ConfirmLeftCount - numeric      
                                  0 , -- ConfirmRightCount - numeric      
                                  0 , -- ConfirmOwnCount - numeric      
                                  0 , -- LeftPV - numeric      
                                  0 , -- RightPV - numeric      
                                  0 , -- OwnPV - numeric      
                                  0 , -- ConfirmLeftPV - numeric      
                                  0 , -- ConfirmRightPV - numeric      
                                  0 , -- ConfirmOwnPV - numeric      
                                  0 , -- LeftBV - numeric      
                                  0 , -- RightBV - numeric      
                                  0 , -- OwnBV - numeric      
                                  0 , -- ConfirmLeftBV - numeric      
                                  0 , -- ConfirmRightBV - numeric      
                                  0 , -- ConfirmOwnBV - numeric      
                                  0 , -- LeftSale - numeric      
                                  0 , -- RightSale - numeric      
                                  0 , -- OwnSale - numeric      
                                  0 , -- ConfirmLeftSale - numeric      
                                  0 , -- ConfirmRightSale - numeric      
                                  0 , -- ConfirmOwnSale - numeric      
                                  NULL , -- AwardId - numeric      
                                  NULL , -- AwardName - varchar(100)      
                                  NULL ,  -- AwardQualDate - datetime      
                                  ( CASE WHEN @adjToId > 0 THEN 1
                                         ELSE 0
                                    END ) , -- IsAvailable - bit      
                                  0 -- IsDirect - bit      
                                )      
                    END      
                          
                    --PRINT '@adjToId = ' + CAST(@adjToId AS VARCHAR(10))      
                    --PRINT '@Id = ' + CAST(@Id AS VARCHAR(10))      
                  --PRINT '------------------------------'      
                          
                SELECT TOP 1
                        @adjToId = ISNULL(MemId, -1) ,
                        @Id = Id
                FROM    #BinaryTreeView WITH ( NOLOCK )
                WHERE   Id > @Id
                ORDER BY Id                          
                           
                SET @counter = @counter + 1      
            END      
              
      
         
        UPDATE  #BinaryTreeView
        SET     MemPositionWrtSponsor = 'L'
        WHERE   Id IN ( 1, 2, 4, 5, 8, 9, 10, 11 )      
                                                                    
        UPDATE  #BinaryTreeView
        SET     MemPositionWrtSponsor = 'R'
        WHERE   Id IN ( 3, 6, 7, 12, 13, 14, 15 )      
                  
        UPDATE  #BinaryTreeView
        SET     MemPosition = 'L'
        WHERE   Id IN ( 1, 2, 4, 6, 8, 10, 12, 14 )      
                                                                    
        UPDATE  #BinaryTreeView
        SET     MemPosition = 'R'
        WHERE   Id IN ( 3, 5, 7, 9, 11, 13, 15 )      
           
        UPDATE  #BinaryTreeView
        SET     MemLevel = 0
        WHERE   Id IN ( 1 )      
                                                                    
        UPDATE  #BinaryTreeView
        SET     MemLevel = 1
        WHERE   Id IN ( 2, 3 )      
                                                                    
        UPDATE  #BinaryTreeView
        SET     MemLevel = 2
        WHERE   Id >= 4
                AND Id <= 7      
          
        UPDATE  #BinaryTreeView
        SET     MemLevel = 3
        WHERE   Id >= 8
                AND Id <= 15      
          
        UPDATE  #BinaryTreeView
        SET     MemLevel = 4
        WHERE   Id >= 16
                AND Id <= 31      
          
    /*  
    SELECT  @counter = 1, @level = 1      
          
    WHILE @counter < 15   
        BEGIN      
            IF EXISTS ( SELECT  MemId  
                        FROM    #BinaryTreeView WITH (NOLOCK)  
                        WHERE   Id = @counter AND MemId IS NOT NULL )   
                BEGIN      
                    SELECT  @adjustedToId = MemId, @adjustedToMemNo = MemNo,  
                            @adjustedToName = MemName  
                    FROM    #BinaryTreeView WITH (NOLOCK)  
                    WHERE   Id = @counter      
          
                      
                    UPDATE  #BinaryTreeView  
                    SET     IsAvailable = 1,   
                    --SET     IsAvailable = 1, MemStatusImg = (SELECT      
                    --                                          GM_decExtra1      
                    --                                         FROM      
                    --                                          GenoDesc_Mst      
                    --                                         WHERE      
                    --                                          GM_ID = 0),  
                            AdjustedToId = @adjustedToId,  
                            AdjustedToMemNo = @adjustedToMemNo,  
                            AdjustedToName = @adjustedToName  
                    WHERE   Id IN (((@level * @counter) + 1),  
                                   ((@level * @counter) + 2)) AND MemId IS NULL AND AdjustedToId > 0  
                END      
                      
            SET @counter = @counter + 1      
                  
            IF @counter = 2 OR @counter = 4 OR @counter = 8   
                SET @level = @level + 1      
        END      
    */  
          
        UPDATE  #BinaryTreeView
        SET     IsDirect = 1
        WHERE   MemId IN ( SELECT   MDD_DirectID
                           FROM     MemDirects_Dtls WITH ( NOLOCK )
                           WHERE    MDD_MemID = @sessionId )
                AND IsDirect = 0      
              
              
        SELECT  *
        FROM    #BinaryTreeView      
          
    END  
GO
CREATE VIEW vw_MultiBinaryTreeViewInfo  
AS  
SELECT  MemJoining_Dtls.MJD_LR AS 'MemPosition', NULL AS 'MemLevel',  
        MemJoining_Dtls.MJD_MemID AS 'MemId', Customer.Id AS 'CustomerId',  
        MemJoining_Dtls.MJD_MemNo AS 'MemNo',  
        MemProfile_Dtls.MPD_Name AS 'MemName',  
        MemJoining_Dtls.MJD_Amount AS 'MemJoiningAmount',  
        MemStatus_Dtls.MSD_ConfFlg AS 'MemConfFlag',  
        MemStatus_Dtls.MSD_ConfirmDate AS 'MemConfDate',  
        MultipleBinaryIncome_Mst.Id AS 'MemImgTypeId',  
        MultipleBinaryIncome_Mst.BinaryIncomeName AS 'MemStatus',  
        --MultipleBinaryIncome_Mst.TreeViewIconImage AS 'MemStatusIconImg',  
        /*('/content/images/icons/Genealogy/Set-1/' + MultipleBinaryIncome_Mst.TreeViewIconImagePath)  
        AS 'MemStatusImg', */ MemJoining_Dtls.MJD_DOJ AS 'DOJ',  
        CONVERT(VARCHAR(10), MemJoining_Dtls.MJD_DOJ, 103) AS 'DOJ_DDMMYYYY',  
        MemProfile_Dtls.MPD_DOB AS 'DOB',  
        CONVERT(VARCHAR(10), MemProfile_Dtls.MPD_DOB, 103) AS 'DOB_DDMMYYYY',  
        MemJoining_Dtls.MJD_Introducer AS 'SponsorId',  
        sponMJD.MJD_MemNo AS 'SponsorMemNo', sponMPD.MPD_Name AS 'SponsorName',  
        MemJoining_Dtls.MJD_AdjustedTo AS 'AdjustedToId',  
        adjtoMJD.MJD_MemNo AS 'AdjustedToMemNo',  
        adjtoMPD.MPD_Name AS 'AdjustedToName',  
        MemJoining_Dtls.MJD_PvCode AS 'PackageId',  
        PV_MST.PV_PDCode AS 'PackageCode', PV_MST.PV_Name AS 'PackageName',  
        UpdPkg.Pv_Code AS 'UpgradePackageId',  
        UpdPkg.PV_PDCode AS 'UpgradePackageCode',  
        UpdPkg.PV_Name AS 'UpgradePackageName',  
        vwMultpleBinaryTempDownMember.mdd_leftCnt AS 'LeftCount',  
        vwMultpleBinaryTempDownMember.mdd_RightCnt AS 'RightCount',  
        0 AS 'OwnCount',  
        vwMultpleBinaryTempDownMember.mdd_ConfleftCnt AS 'ConfirmLeftCount',  
        vwMultpleBinaryTempDownMember.mdd_ConfRightCnt AS 'ConfirmRightCount',  
        0 AS 'ConfirmOwnCount',  
        vwMultpleBinaryTempDownMember.mdd_leftPV AS 'LeftPV',  
        vwMultpleBinaryTempDownMember.mdd_RightPV AS 'RightPV',  
        vwMultpleBinaryTempDownMember.mdd_OwnPV AS 'OwnPV',  
        vwMultpleBinaryTempDownMember.mdd_ConfleftPV AS 'ConfirmLeftPV',  
        vwMultpleBinaryTempDownMember.mdd_ConfRightPV AS 'ConfirmRightPV',  
        vwMultpleBinaryTempDownMember.mdd_ConfOwnPV AS 'ConfirmOwnPV',  
        vwMultpleBinaryTempDownMember.mdd_leftBV AS 'LeftBV',  
        vwMultpleBinaryTempDownMember.mdd_RightBV AS 'RightBV',  
        vwMultpleBinaryTempDownMember.MDD_OwnBV AS 'OwnBV',  
        vwMultpleBinaryTempDownMember.mdd_ConfleftBV AS 'ConfirmLeftBV',  
        vwMultpleBinaryTempDownMember.mdd_ConfRightBV AS 'ConfirmRightBV',  
        vwMultpleBinaryTempDownMember.mdd_ConfOwnBV AS 'ConfirmOwnBV',  
        vwMultpleBinaryTempDownMember.MDD_LeftSale AS 'LeftSale',  
        vwMultpleBinaryTempDownMember.mdd_RightSale AS 'RightSale',  
        0 AS 'OwnSale',  
        vwMultpleBinaryTempDownMember.mdd_ConfleftSale AS 'ConfirmLeftSale',  
        vwMultpleBinaryTempDownMember.mdd_ConfRightSale AS 'ConfirmRightSale',  
        0 AS 'ConfirmOwnSale', Offer_Mst.OM_ID AS 'AwardId',  
        Offer_Mst.OM_Name AS 'AwardName',  
        OfferAchiever_Trn.OAT_AchvDt AS 'AwardQualDate'  
FROM    MemJoining_Dtls WITH (NOLOCK)  
INNER JOIN Customer WITH (NOLOCK) ON MemJoining_Dtls.MJD_MemID = Customer.MemId  
INNER JOIN MemProfile_Dtls WITH (NOLOCK) ON MemJoining_Dtls.MJD_MemID = MemProfile_Dtls.MPD_MemId  
INNER JOIN MemStatus_Dtls WITH (NOLOCK) ON MemJoining_Dtls.MJD_MemID = MemStatus_Dtls.MSD_Memid  
LEFT OUTER JOIN PV_MST WITH (NOLOCK) ON MemJoining_Dtls.MJD_PvCode = PV_MST.PV_Code  
LEFT OUTER JOIN MemStatus_Mst WITH (NOLOCK) ON MemStatus_Dtls.MSD_MSMID = MemStatus_Mst.MSM_ID  
LEFT OUTER JOIN MultipleBinaryIncome_Mst WITH (NOLOCK) ON (CASE  
                                                              WHEN (MemStatus_Dtls.MSD_MSMID) = 1  
                                                              THEN 3  
                                            WHEN (MemStatus_Dtls.MSD_MSMID) = 2  
                                                              THEN 2  
                                                              WHEN (MemStatus_Dtls.MSD_MSMID) = 5  
                                                              THEN 1  
                                                              WHEN (MemStatus_Dtls.MSD_MSMID) = 3  
                                                              THEN 5  
                                                              WHEN (MemStatus_Dtls.MSD_MSMID) = 4  
                                                              THEN 4  
                                                              WHEN (MemStatus_Dtls.MSD_MSMID) = 6  
                                                              THEN 6  
                                                           END) = MultipleBinaryIncome_Mst.Id  
LEFT OUTER JOIN MemJoining_Dtls sponMJD WITH (NOLOCK) ON MemJoining_Dtls.MJD_Introducer = sponMJD.MJD_MemID  
LEFT OUTER JOIN MemProfile_Dtls sponMPD WITH (NOLOCK) ON sponMJD.MJD_MemID = sponMPD.MPD_MemId  
LEFT OUTER JOIN MemJoining_Dtls adjtoMJD WITH (NOLOCK) ON MemJoining_Dtls.MJD_AdjustedTo = adjtoMJD.MJD_MemID  
LEFT OUTER JOIN MemProfile_Dtls adjtoMPD WITH (NOLOCK) ON adjtoMJD.MJD_MemID = adjtoMPD.MPD_MemId  
LEFT OUTER JOIN vwMultpleBinaryTempDownMember WITH (NOLOCK) ON MemJoining_Dtls.MJD_MemID = vwMultpleBinaryTempDownMember.mdd_memid  
LEFT OUTER JOIN Upgrade_Trn WITH (NOLOCK) ON Upgrade_Trn.UT_MemID = MemJoining_Dtls.MJD_MemID AND Upgrade_Trn.UT_ID = (SELECT  
                                                              MAX(u.UT_ID)  
                                                              FROM  
                                                              Upgrade_Trn u  WITH (NOLOCK)
                                                              WHERE  
                                                              u.UT_MemID = Upgrade_Trn.UT_MemID)  
LEFT OUTER JOIN PV_MST UpdPkg WITH (NOLOCK) ON Upgrade_Trn.UT_NewPvCode = UpdPkg.PV_Code  
LEFT OUTER JOIN Offer_Mst WITH (NOLOCK) ON OM_Type = 1 AND OM_Serialno = (SELECT  
                                                              ISNULL(MAX(OfferAchiever_Trn.OAT_serialno),  
                                                              0)  
                                                              FROM  
                                                              OfferAchiever_Trn  WITH (NOLOCK)
                                                              WHERE  
                                                              OfferAchiever_Trn.OAT_MemID = MemJoining_Dtls.MJD_MemID AND OfferAchiever_Trn.OAT_Status = 1)  
LEFT OUTER JOIN OfferAchiever_Trn WITH (NOLOCK) ON OfferAchiever_Trn.OAT_MemID = MemJoining_Dtls.MJD_MemID AND OfferAchiever_Trn.OAT_Status = 1 AND OfferAchiever_Trn.OAT_serialno = Offer_Mst.OM_Serialno  
GO
CREATE PROCEDURE prc_MultiBinaryTreeView  
    @sessionId NUMERIC(18, 0),  
    @memId NUMERIC(18, 0)--,  
    --@treeId INT  
AS   
BEGIN  
    SET NOCOUNT ON  
                              
    DECLARE @counter INT,  
        @Id INT,  
        @adjToId NUMERIC(18, 0),  
        @level INT,  
        @cnt INT,  
        @memNo VARCHAR(10),  
        @adjustedToId VARCHAR(10),  
        @adjustedToMemNo VARCHAR(10),  
        @adjustedToName VARCHAR(10)  
                  
    SELECT  @counter = 1, @Id = 1,  
            @adjToId = (CASE WHEN @memId = 0 THEN @sessionId  
                             ELSE @memId  
                        END), @level = 0  
  
    IF OBJECT_ID('tempdb..#MultiBinaryTreeView', 'U') IS NOT NULL   
        BEGIN  
            -- PRINT 'DROP TABLE #MultiBinaryTreeView'  
            DROP TABLE #MultiBinaryTreeView  
        END  
          
    CREATE TABLE #MultiBinaryTreeView ([Id] INT IDENTITY(1, 1)  
                                                PRIMARY KEY  
                                                NOT NULL,  
                                       [MemPositionWrtSponsor] VARCHAR(10)  
                                        NULL,  
                                       [MemPosition] VARCHAR(10) NULL,  
                                       [MemLevel] INT NULL,  
                                       [MemId] NUMERIC(18, 0) NULL,  
                                       [CustomerId] INT NULL,  
                                       [MemNo] VARCHAR(50) NULL,  
                                       [MemName] VARCHAR(100) NULL,  
                                       [MemJoiningAmount] NUMERIC(18, 2) NULL,  
                                       [MemConfFlag] INT NULL,  
                                       [MemConfDate] DATETIME NULL,  
                                       [MemImgTypeId] INT NULL,  
                                       [MemStatus] VARCHAR(50) NULL,  
          --[MemCurrStatusId] INT NULL,  
          --[MemCurrStatus] VARCHAR(50) NULL,  
                                     --[MemStatusImg] IMAGE NULL,  
                                     --[MemGenealogyIconPath] VARCHAR(200)  
                                     --   NULL,  
                                       [DOJ] DATETIME NULL,  
                                       [DOJ_DDMMYYYY] VARCHAR(10) NULL,  
                                       [DOB] DATETIME NULL,  
                                       [DOB_DDMMYYYY] VARCHAR(10) NULL,  
                                       [SponsorId] NUMERIC(18, 0) NULL,  
                                       [SponsorMemNo] VARCHAR(50) NULL,  
                                       [SponsorName] VARCHAR(100) NULL,  
                                       [AdjustedToId] NUMERIC(18, 0) NULL,  
                                       [AdjustedToMemNo] VARCHAR(50) NULL,  
                                       [AdjustedToName] VARCHAR(100) NULL,  
                                       [PackageId] NUMERIC(18, 0) NULL,  
                                       [PackageCode] VARCHAR(50) NULL,  
                                       [PackageName] VARCHAR(100) NULL,  
                                       [UpgradePackageId] NUMERIC(18, 0) NULL,  
                                       [UpgradePackageCode] VARCHAR(50) NULL,  
                                       [UpgradePackageName] VARCHAR(100) NULL,  
                                       [LeftCount] NUMERIC(18, 0) NOT NULL,  
                                       [RightCount] NUMERIC(18, 0) NOT NULL,  
                                       [OwnCount] NUMERIC(18, 0) NOT NULL,  
                                       [ConfirmLeftCount] NUMERIC(18, 0)  
                                        NOT NULL,  
                                       [ConfirmRightCount] NUMERIC(18, 0)  
                                        NOT NULL,  
                                       [ConfirmOwnCount] NUMERIC(18, 0)  
                                        NOT NULL,  
      [LeftPV] NUMERIC(18, 0) NOT NULL,  
                                       [RightPV] NUMERIC(18, 0) NOT NULL,  
                                       [OwnPV] NUMERIC(18, 0) NOT NULL,  
                                       [ConfirmLeftPV] NUMERIC(18, 0) NOT NULL,  
                                       [ConfirmRightPV] NUMERIC(18, 0)  
                                        NOT NULL,  
                                       [ConfirmOwnPV] NUMERIC(18, 0) NOT NULL,  
                                       [LeftBV] NUMERIC(18, 0) NOT NULL,  
                                       [RightBV] NUMERIC(18, 0) NOT NULL,  
                                       [OwnBV] NUMERIC(18, 0) NOT NULL,  
                                       [ConfirmLeftBV] NUMERIC(18, 0) NOT NULL,  
                                       [ConfirmRightBV] NUMERIC(18, 0)  
                                        NOT NULL,  
                                       [ConfirmOwnBV] NUMERIC(18, 0) NOT NULL,  
                                       [LeftSale] NUMERIC(18, 2) NOT NULL,  
                                       [RightSale] NUMERIC(18, 2) NOT NULL,  
                                       [OwnSale] NUMERIC(18, 2) NOT NULL,  
                                       [ConfirmLeftSale] NUMERIC(18, 2)  
                                        NOT NULL,  
                                       [ConfirmRightSale] NUMERIC(18, 2)  
                                        NOT NULL,  
                                       [ConfirmOwnSale] NUMERIC(18, 2)  
                                        NOT NULL,  
                                       [AwardId] NUMERIC(18, 0) NULL,  
                                       [AwardName] VARCHAR(100) NULL,  
                                       [AwardQualDate] DATETIME NULL,  
                                       [IsAvailable] BIT NOT NULL,  
                                       [IsDirect] BIT NOT NULL)  
    INSERT  INTO #MultiBinaryTreeView (MemPositionWrtSponsor, MemPosition,  
                                       MemLevel, MemId, CustomerId, MemNo,  
                                       MemName, MemJoiningAmount, MemConfFlag,  
                                       MemConfDate, MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                               MemCurrStatus,  MemStatusImg,  
                                       MemGenealogyIconPath,*/ DOJ,  
                                       DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY,  
                                       SponsorId, SponsorMemNo, SponsorName,  
                                       AdjustedToId, AdjustedToMemNo,  
                                       AdjustedToName, PackageId, PackageCode,  
                                       PackageName, UpgradePackageId,  
                                       UpgradePackageCode, UpgradePackageName,  
                                       LeftCount, RightCount, OwnCount,  
                                       ConfirmLeftCount, ConfirmRightCount,  
                                       ConfirmOwnCount, LeftPV, RightPV, OwnPV,  
                                       ConfirmLeftPV, ConfirmRightPV,  
                                       ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                                       ConfirmLeftBV, ConfirmRightBV,  
                                       ConfirmOwnBV, LeftSale, RightSale,  
                                       OwnSale, ConfirmLeftSale,  
                                       ConfirmRightSale, ConfirmOwnSale,  
                                       AwardId, AwardName, AwardQualDate,  
                                       IsAvailable, IsDirect)  
            SELECT  --@counter AS 'Id',  
                    'L' AS 'MemPositionWrtSponsor', MemPosition,  
                    0 AS 'MemLevel', MemId, CustomerId, MemNo, MemName,  
                    MemJoiningAmount, MemConfFlag, MemConfDate, MemImgTypeId,  
                    MemStatus, /*MemCurrStatusId, MemCurrStatus,  
                    MemStatusImg, NULL AS 'MemGenealogyIconPath',*/ DOJ,  
                    DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY, SponsorId, SponsorMemNo,  
                    SponsorName, AdjustedToId, AdjustedToMemNo, AdjustedToName,  
                    PackageId, PackageCode, PackageName, UpgradePackageId,  
                    UpgradePackageCode, UpgradePackageName, LeftCount,  
                    RightCount, OwnCount, ConfirmLeftCount, ConfirmRightCount,  
                    ConfirmOwnCount, LeftPV, RightPV, OwnPV, ConfirmLeftPV,  
                    ConfirmRightPV, ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                    ConfirmLeftBV, ConfirmRightBV, ConfirmOwnBV, LeftSale,  
                    RightSale, OwnSale, ConfirmLeftSale, ConfirmRightSale,  
                    ConfirmOwnSale, AwardId, AwardName, AwardQualDate, 0, 0  
            FROM    vw_MultiBinaryTreeViewInfo  WITH (NOLOCK)
            WHERE   vw_MultiBinaryTreeViewInfo.MemId = @adjToId  
                      
 ------------------------------------------------------------------------------------------------------------------------------------------------  
  
    --PRINT 'BINARY TREE' --==================================================================================================================  
      
      
      
      
    WHILE @counter < 8   
        BEGIN  
            --PRINT @counter  
            --PRINT @adjToId  
            --PRINT 'L'  
                      
            IF EXISTS ( SELECT  MemId  
                        FROM    vw_MultiBinaryTreeViewInfo  WITH (NOLOCK)
                        WHERE   vw_MultiBinaryTreeViewInfo.AdjustedToId = @adjToId AND vw_MultiBinaryTreeViewInfo.MemPosition = 'L' )   
                BEGIN  
                    INSERT  INTO #MultiBinaryTreeView (MemPositionWrtSponsor,  
                                                       MemPosition, MemLevel,  
                                                       MemId, CustomerId,  
                                                       MemNo, MemName,  
                                                       MemJoiningAmount,  
                                                       MemConfFlag,  
                                                       MemConfDate,  
                                                       MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                                               MemCurrStatus,  MemStatusImg,  
                                                       MemGenealogyIconPath,*/  
                                                       DOJ, DOJ_DDMMYYYY, DOB,  
                                                       DOB_DDMMYYYY, SponsorId,  
                                                       SponsorMemNo,  
                                                       SponsorName,  
                                                       AdjustedToId,  
                                                       AdjustedToMemNo,  
                                                       AdjustedToName,  
                                                       PackageId, PackageCode,  
                                                       PackageName,  
                                                       UpgradePackageId,  
                                                       UpgradePackageCode,  
                                                       UpgradePackageName,  
                                                       LeftCount, RightCount,  
                                                       OwnCount,  
                                                       ConfirmLeftCount,  
                                                       ConfirmRightCount,  
                                                       ConfirmOwnCount, LeftPV,  
                                                       RightPV, OwnPV,  
                                                       ConfirmLeftPV,  
                                                       ConfirmRightPV,  
                                                       ConfirmOwnPV, LeftBV,  
                                                       RightBV, OwnBV,  
                                                       ConfirmLeftBV,  
                                                       ConfirmRightBV,  
                                                       ConfirmOwnBV, LeftSale,  
                                                       RightSale, OwnSale,  
                                                       ConfirmLeftSale,  
                                                       ConfirmRightSale,  
                                                       ConfirmOwnSale, AwardId,  
                                                       AwardName,  
                                                       AwardQualDate,  
                                                       IsAvailable, IsDirect)  
                            SELECT  --@counter AS 'Id',  
                                    NULL AS 'MemPositionWrtSponsor',  
                                    NULL AS 'MemPosition', NULL AS 'MemLevel',  
                                    MemId, CustomerId, MemNo, MemName,  
                                    MemJoiningAmount, MemConfFlag, MemConfDate,  
                                    MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                                    MemCurrStatus, MemStatusImg,  
                                    NULL AS 'MemGenealogyIconPath',*/ DOJ,  
                                    DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY, SponsorId,  
                                    SponsorMemNo, SponsorName, AdjustedToId,  
                                    AdjustedToMemNo, AdjustedToName, PackageId,  
                                    PackageCode, PackageName, UpgradePackageId,  
                                    UpgradePackageCode, UpgradePackageName,  
                                    LeftCount, RightCount, OwnCount,  
                                    ConfirmLeftCount, ConfirmRightCount,  
                                    ConfirmOwnCount, LeftPV, RightPV, OwnPV,  
                                    ConfirmLeftPV, ConfirmRightPV,  
                                    ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                                    ConfirmLeftBV, ConfirmRightBV,  
                                    ConfirmOwnBV, LeftSale, RightSale, OwnSale,  
                                    ConfirmLeftSale, ConfirmRightSale,  
                                    ConfirmOwnSale, AwardId, AwardName,  
                                    AwardQualDate, 0 AS 'IsAvailable',  
                                    0 AS 'IsDirect'  
                            FROM    vw_MultiBinaryTreeViewInfo  WITH (NOLOCK)
                            WHERE   vw_MultiBinaryTreeViewInfo.AdjustedToId = @adjToId AND vw_MultiBinaryTreeViewInfo.MemPosition = 'L'  
                END  
            ELSE   
                BEGIN  
                    INSERT  INTO #MultiBinaryTreeView (MemPositionWrtSponsor,  
                                                       MemPosition, MemLevel,  
                                                       MemId, CustomerId,  
                                                       MemNo, MemName,  
                                                       MemJoiningAmount,  
                                                       MemConfFlag,  
                                                       MemConfDate,  
                                                       MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                                               MemCurrStatus, MemStatusImg,  
                                                       MemGenealogyIconPath,*/  
                                                       DOJ, DOJ_DDMMYYYY, DOB,  
                                                       DOB_DDMMYYYY, SponsorId,  
                                                       SponsorMemNo,  
                                                       SponsorName,  
                                                       AdjustedToId,  
                                            AdjustedToMemNo,  
                                                       AdjustedToName,  
                                                       PackageId, PackageCode,  
                                                       PackageName,  
                                                       UpgradePackageId,  
                                                       UpgradePackageCode,  
                                                       UpgradePackageName,  
                                                       LeftCount, RightCount,  
                                                       OwnCount,  
                                                       ConfirmLeftCount,  
                                                       ConfirmRightCount,  
                                                       ConfirmOwnCount, LeftPV,  
                                                       RightPV, OwnPV,  
                                                       ConfirmLeftPV,  
                                                       ConfirmRightPV,  
                                                       ConfirmOwnPV, LeftBV,  
                                                       RightBV, OwnBV,  
                                                       ConfirmLeftBV,  
                                                       ConfirmRightBV,  
                                                       ConfirmOwnBV, LeftSale,  
                                                       RightSale, OwnSale,  
                                                       ConfirmLeftSale,  
                                                       ConfirmRightSale,  
                                                       ConfirmOwnSale, AwardId,  
                                                       AwardName,  
                                                       AwardQualDate,  
                                                       IsAvailable, IsDirect)  
                    VALUES  (NULL, -- MemPositionWrtSponsor - VARCHAR(10)  
                             NULL, -- MemPosition - VARCHAR(10)  
                             NULL, -- MemLevel - int  
                             NULL, -- MemId - numeric  
                             NULL, -- CustomerId - int  
                             NULL, -- MemNo - varchar(50)  
                             NULL, -- MemName - varchar(100)  
                             NULL, -- MemJoiningAmount - numeric  
                             NULL, -- MemConfFlag - int  
                             NULL, -- MemConfDate - int  
                             NULL, -- MemImgTypeId - int  
                             NULL, -- MemStatus - varchar(50)  
                             --NULL, -- MemCurrStatusId - int  
                             --NULL, -- MemCurrStatus - varchar(50)  
                             --NULL, -- MemStatusImg - image  
                             --NULL, -- MemGenealogyIconPath - varchar(200)  
                             NULL, -- DOJ - datetime  
                             NULL, -- DOJ_DDMMYYYY - varchar(10)  
                             NULL, -- DOB - datetime  
                             NULL, -- DOB_DDMMYYYY - varchar(10)  
                             NULL, -- SponsorId - numeric  
                             NULL, -- SponsorMemNo - varchar(50)  
                             NULL, -- SponsorName - varchar(100)  
                             NULL, -- AdjustedToId - numeric  
                             NULL, -- AdjustedToMemNo - varchar(50)  
                             NULL, -- AdjustedToName - varchar(100)  
                             NULL, -- PackageId - numeric  
                             NULL, -- PackageCode - varchar(100)  
                             NULL, -- PackageName - varchar(100)  
                             NULL, -- UpgradePackageId - numeric  
                             NULL, -- UpgradePackageCode - varchar(100)  
                             NULL, -- UpgradePackageName - varchar(100)  
                             0, -- LeftCount - numeric  
                             0, -- RightCount - numeric  
                             0, -- OwnCount - numeric  
                             0, -- ConfirmLeftCount - numeric  
                             0, -- ConfirmRightCount - numeric  
                             0, -- ConfirmOwnCount - numeric  
                             0, -- LeftPV - numeric  
                             0, -- RightPV - numeric  
                             0, -- OwnPV - numeric  
                             0, -- ConfirmLeftPV - numeric  
                             0, -- ConfirmRightPV - numeric  
                             0, -- ConfirmOwnPV - numeric  
                             0, -- LeftBV - numeric  
                             0, -- RightBV - numeric  
                             0, -- OwnBV - numeric  
                             0, -- ConfirmLeftBV - numeric  
                             0, -- ConfirmRightBV - numeric  
                             0, -- ConfirmOwnBV - numeric  
                             0, -- LeftSale - numeric  
                             0, -- RightSale - numeric  
                             0, -- OwnSale - numeric  
                             0, -- ConfirmLeftSale - numeric  
                             0, -- ConfirmRightSale - numeric  
                             0, -- ConfirmOwnSale - numeric  
                             NULL, -- AwardId - numeric  
                             NULL, -- AwardName - varchar(100)  
                             NULL,  -- AwardQualDate - datetime  
                             0, -- IsAvailable - bit  
                             0 -- IsDirect - bit  
                             )  
                END  
                          
                          
                        --------------------------------------------------  
                    --PRINT 'R'  
                          
            IF EXISTS ( SELECT  MemId  
                        FROM    vw_MultiBinaryTreeViewInfo  WITH (NOLOCK)
                        WHERE   vw_MultiBinaryTreeViewInfo.AdjustedToId = @adjToId AND vw_MultiBinaryTreeViewInfo.MemPosition = 'R' )   
                BEGIN  
                    INSERT  INTO #MultiBinaryTreeView (MemPositionWrtSponsor,  
                                                       MemPosition, MemLevel,  
                                                       MemId, CustomerId,  
                                                       MemNo, MemName,  
                                                       MemJoiningAmount,  
                                                       MemConfFlag,  
                                                       MemConfDate,  
                                                       MemImgTypeId, MemStatus, /*MemCurrStatusId,  
              MemCurrStatus,  MemStatusImg,  
                                                       MemGenealogyIconPath,*/  
                                                       DOJ, DOJ_DDMMYYYY, DOB,  
                                                       DOB_DDMMYYYY, SponsorId,  
                                                       SponsorMemNo,  
                                                       SponsorName,  
                                                       AdjustedToId,  
                                                       AdjustedToMemNo,  
                                                       AdjustedToName,  
                                                       PackageId, PackageCode,  
                                                       PackageName,  
                                                       UpgradePackageId,  
                                                       UpgradePackageCode,  
                                                       UpgradePackageName,  
                                                       LeftCount, RightCount,  
                                                       OwnCount,  
                               ConfirmLeftCount,  
                                                       ConfirmRightCount,  
                                                       ConfirmOwnCount, LeftPV,  
                                                       RightPV, OwnPV,  
                                                       ConfirmLeftPV,  
                                                       ConfirmRightPV,  
                                                       ConfirmOwnPV, LeftBV,  
                                                       RightBV, OwnBV,  
                                                       ConfirmLeftBV,  
                                                       ConfirmRightBV,  
                                                       ConfirmOwnBV, LeftSale,  
                                                       RightSale, OwnSale,  
                                                       ConfirmLeftSale,  
                                                       ConfirmRightSale,  
                                                       ConfirmOwnSale, AwardId,  
                                                       AwardName,  
                                                       AwardQualDate,  
                                                       IsAvailable, IsDirect)  
                            SELECT  --@counter AS 'Id',  
                                    NULL AS 'MemPositionWrtSponsor',  
                                    NULL AS 'MemPosition', NULL AS 'MemLevel',  
                                    MemId, CustomerId, MemNo, MemName,  
                                    MemJoiningAmount, MemConfFlag, MemConfDate,  
                                    MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                                    MemCurrStatus, MemStatusImg,  
                                    NULL AS 'MemGenealogyIconPath',*/ DOJ,  
                                    DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY, SponsorId,  
                                    SponsorMemNo, SponsorName, AdjustedToId,  
                                    AdjustedToMemNo, AdjustedToName, PackageId,  
                                    PackageCode, PackageName, UpgradePackageId,  
                                    UpgradePackageCode, UpgradePackageName,  
                                    LeftCount, RightCount, OwnCount,  
                                    ConfirmLeftCount, ConfirmRightCount,  
                                    ConfirmOwnCount, LeftPV, RightPV, OwnPV,  
                                    ConfirmLeftPV, ConfirmRightPV,  
                                    ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                                    ConfirmLeftBV, ConfirmRightBV,  
                                    ConfirmOwnBV, LeftSale, RightSale, OwnSale,  
                                    ConfirmLeftSale, ConfirmRightSale,  
                                    ConfirmOwnSale, AwardId, AwardName,  
                                    AwardQualDate, 0 AS 'IsAvailable',  
                                    0 AS 'IsDirect'  
                            FROM    vw_MultiBinaryTreeViewInfo  WITH (NOLOCK)
                            WHERE   vw_MultiBinaryTreeViewInfo.AdjustedToId = @adjToId AND vw_MultiBinaryTreeViewInfo.MemPosition = 'R'  
                END  
            ELSE   
                BEGIN  
                    INSERT  INTO #MultiBinaryTreeView (MemPositionWrtSponsor,  
                                                       MemPosition, MemLevel,  
                                                       MemId, CustomerId,  
                                                       MemNo, MemName,  
                                                       MemJoiningAmount,  
                                                       MemConfFlag,  
                                                       MemConfDate,  
                                                       MemImgTypeId, MemStatus, /*MemCurrStatusId,  
              MemCurrStatus, MemStatusImg,  
                                                       MemGenealogyIconPath,*/  
                                                       DOJ, DOJ_DDMMYYYY, DOB,  
                                                       DOB_DDMMYYYY, SponsorId,  
                                                       SponsorMemNo,  
                                                       SponsorName,  
                                                       AdjustedToId,  
                                                       AdjustedToMemNo,  
                                                       AdjustedToName,  
                                                       PackageId, PackageCode,  
                                                       PackageName,  
                                                       UpgradePackageId,  
                                                       UpgradePackageCode,  
                                                       UpgradePackageName,  
                                                       LeftCount, RightCount,  
                                                       OwnCount,  
                                                       ConfirmLeftCount,  
                                                       ConfirmRightCount,  
                                                       ConfirmOwnCount, LeftPV,  
                                                       RightPV, OwnPV,  
                                                       ConfirmLeftPV,  
                                                       ConfirmRightPV,  
                                                       ConfirmOwnPV, LeftBV,  
                                                       RightBV, OwnBV,  
                                                       ConfirmLeftBV,  
                                                       ConfirmRightBV,  
                                                       ConfirmOwnBV, LeftSale,  
                                                       RightSale, OwnSale,  
                                                       ConfirmLeftSale,  
                                                       ConfirmRightSale,  
                                                       ConfirmOwnSale, AwardId,  
                                                       AwardName,  
                                                       AwardQualDate,  
                                                       IsAvailable, IsDirect)  
                    VALUES  (NULL, -- MemPositionWrtSponsor - VARCHAR(10)  
                             NULL, -- MemPosition - VARCHAR(10)  
                             NULL, -- MemLevel - int  
                             NULL, -- MemId - numeric  
                             NULL, -- CustomerId - int  
                             NULL, -- MemNo - varchar(50)  
                             NULL, -- MemName - varchar(100)  
                             NULL, -- MemJoiningAmount - numeric  
                             NULL, -- MemConfFlag - int  
                             NULL, -- MemConfDate - int  
                             NULL, -- MemImgTypeId - int  
                             NULL, -- MemStatus - varchar(50)  
                             --NULL, -- MemCurrStatusId - int  
                             --NULL, -- MemCurrStatus - varchar(50)  
                             --NULL, -- MemStatusImg - image  
                             --NULL, -- MemGenealogyIconPath - varchar(200)  
                             NULL, -- DOJ - datetime  
                             NULL, -- DOJ_DDMMYYYY - varchar(10)  
                             NULL, -- DOB - datetime  
                             NULL, -- DOB_DDMMYYYY - varchar(10)  
                             NULL, -- SponsorId - numeric  
                             NULL, -- SponsorMemNo - varchar(50)  
                             NULL, -- SponsorName - varchar(100)  
                             NULL, -- AdjustedToId - numeric  
                             NULL, -- AdjustedToMemNo - varchar(50)  
                             NULL, -- AdjustedToName - varchar(100)  
                             NULL, -- PackageId - numeric  
                             NULL, -- PackageCode - varchar(100)  
                             NULL, -- PackageName - varchar(100)  
                             NULL, -- UpgradePackageId - numeric  
                             NULL, -- UpgradePackageCode - varchar(100)  
                             NULL, -- UpgradePackageName - varchar(100)  
                             0, -- LeftCount - numeric  
                             0, -- RightCount - numeric  
                             0, -- OwnCount - numeric  
                             0, -- ConfirmLeftCount - numeric  
                             0, -- ConfirmRightCount - numeric  
                             0, -- ConfirmOwnCount - numeric  
                             0, -- LeftPV - numeric  
                             0, -- RightPV - numeric  
                             0, -- OwnPV - numeric  
                             0, -- ConfirmLeftPV - numeric  
                             0, -- ConfirmRightPV - numeric  
                             0, -- ConfirmOwnPV - numeric  
                             0, -- LeftBV - numeric  
                             0, -- RightBV - numeric  
                             0, -- OwnBV - numeric  
                             0, -- ConfirmLeftBV - numeric  
                             0, -- ConfirmRightBV - numeric  
                             0, -- ConfirmOwnBV - numeric  
                             0, -- LeftSale - numeric  
                             0, -- RightSale - numeric  
                             0, -- OwnSale - numeric  
                             0, -- ConfirmLeftSale - numeric  
                             0, -- ConfirmRightSale - numeric  
                             0, -- ConfirmOwnSale - numeric  
                             NULL, -- AwardId - numeric  
                             NULL, -- AwardName - varchar(100)  
                             NULL,  -- AwardQualDate - datetime  
                             0, -- IsAvailable - bit  
                             0 -- IsDirect - bit  
                             )  
                END  
                      
                    --PRINT '@adjToId = ' + CAST(@adjToId AS VARCHAR(10))  
                    --PRINT '@Id = ' + CAST(@Id AS VARCHAR(10))  
                    --PRINT '------------------------------'  
                      
            SELECT TOP 1  
                    @adjToId = ISNULL(MemId, -1), @Id = Id  
            FROM    #MultiBinaryTreeView WITH (NOLOCK)  
            WHERE   Id > @Id  
            ORDER BY Id                      
                       
            SET @counter = @counter + 1  
        END  
          
  
     
    UPDATE  #MultiBinaryTreeView  
    SET     MemPositionWrtSponsor = 'L'  
    WHERE   Id IN (1, 2, 4, 5, 8, 9, 10, 11)  
                                                                
    UPDATE  #MultiBinaryTreeView  
    SET     MemPositionWrtSponsor = 'R'  
    WHERE   Id IN (3, 6, 7, 12, 13, 14, 15)  
              
    UPDATE  #MultiBinaryTreeView  
    SET     MemPosition = 'L'  
    WHERE   Id IN (1, 2, 4, 6, 8, 10, 12, 14)  
                                                                
    UPDATE  #MultiBinaryTreeView  
    SET     MemPosition = 'R'  
    WHERE   Id IN (3, 5, 7, 9, 11, 13, 15)  
                                                                
    UPDATE  #MultiBinaryTreeView  
    SET     MemLevel = 0  
    WHERE   Id IN (1)  
                                                                
    UPDATE  #MultiBinaryTreeView  
    SET     MemLevel = 1  
    WHERE   Id IN (2, 3)  
                                                                
    UPDATE  #MultiBinaryTreeView  
    SET     MemLevel = 2  
    WHERE   Id >= 4 AND Id <= 7  
      
    UPDATE  #MultiBinaryTreeView  
    SET     MemLevel = 3  
    WHERE   Id >= 8 AND Id <= 15  
      
    UPDATE  #MultiBinaryTreeView  
    SET     MemLevel = 4      WHERE   Id >= 16 AND Id <= 31  
      
    SELECT  @counter = 1, @level = 1  
      
    WHILE @counter < 15   
        BEGIN  
            IF EXISTS ( SELECT  MemId  
                        FROM    #MultiBinaryTreeView WITH (NOLOCK)  
                        WHERE   Id = @counter AND MemId IS NOT NULL )   
                BEGIN  
                    SELECT  @adjustedToId = MemId, @adjustedToMemNo = MemNo,  
                            @adjustedToName = MemName  
                    FROM    #MultiBinaryTreeView WITH (NOLOCK)  
                    WHERE   Id = @counter  
      
                    UPDATE  #MultiBinaryTreeView  
                    SET     IsAvailable = 1, /*MemStatusImg = (SELECT  
                                                              GM_decExtra1  
                                                             FROM  
                                                              GenoDesc_Mst  
                                                             WHERE  
                                                              GM_ID = 0),*/  
                            AdjustedToId = @adjustedToId,  
                            AdjustedToMemNo = @adjustedToMemNo,  
                            AdjustedToName = @adjustedToName  
                    WHERE   Id IN (((@level * @counter) + 1),  
                                   ((@level * @counter) + 2)) AND MemId IS NULL  
                END  
                  
            SET @counter = @counter + 1  
              
            IF @counter = 2 OR @counter = 4 OR @counter = 8   
                SET @level = @level + 1  
        END  
      
      
    UPDATE  #MultiBinaryTreeView  
    SET     IsDirect = 1  
    WHERE   MemId IN (SELECT    MDD_DirectID  
                      FROM      MemDirects_Dtls  WITH (NOLOCK)
                      WHERE     MDD_MemID = @sessionId) AND IsDirect = 0  
          
          
    SELECT  *  
    FROM    #MultiBinaryTreeView  
  
END  
GO
CREATE PROCEDURE prc_GenerationTreeView  
    @sessionId NUMERIC(18, 0),  
    @memId NUMERIC(18, 0),  
    @pageNo INT  
AS   
BEGIN  
    SET NOCOUNT ON  
                              
    DECLARE @counter INT,  
        @Id INT,  
        @sponId NUMERIC(18, 0),  
        @level INT,  
        @memNo VARCHAR(10),  
        @sponsorId VARCHAR(10),  
        @sponsorMemNo VARCHAR(10),  
        @sponsorName VARCHAR(10),  
        @cnt INT,  
        @t1 INT,  
        @t2 INT,  
        @t3 INT  
  
                  
    SELECT  @counter = 1, @Id = 1,  
            @sponId = (CASE WHEN @memId = 0 THEN @sessionId  
                            ELSE @memId  
                       END), @level = 0  
  
    IF OBJECT_ID('tempdb..#GenerationTreeView', 'U') IS NOT NULL   
        BEGIN  
            -- PRINT 'DROP TABLE #GenerationTreeView'  
            DROP TABLE #GenerationTreeView  
        END  
          
    CREATE TABLE #GenerationTreeView ([Id] INT IDENTITY(1, 1)  
                                               PRIMARY KEY  
                                               NOT NULL,  
                                      [MemPosition] INT NULL,  
                                      [MemLevel] INT NULL,  
                                      [MemId] NUMERIC(18, 0) NULL,  
                                      [CustomerId] INT NULL,  
                                      [MemNo] VARCHAR(50) NULL,  
                                      [MemName] VARCHAR(100) NULL,  
                                      [MemJoiningAmount] NUMERIC(18, 2) NULL,  
                                      [MemConfFlag] INT NULL,  
                                      [MemConfDate] DATETIME NULL,  
                                      [MemImgTypeId] INT NULL,  
                                      [MemStatus] VARCHAR(50) NULL,  
           --[MemCurrStatusId] INT NULL,  
           --[MemCurrStatus] VARCHAR(50) NULL,  
                                      --[MemStatusImg] IMAGE NULL,  
                                      --[MemGenealogyIconPath] VARCHAR(200) NULL,  
                                      [DOJ] DATETIME NULL,  
                                      [DOJ_DDMMYYYY] VARCHAR(10) NULL,  
                                      [DOB] DATETIME NULL,  
                                      [DOB_DDMMYYYY] VARCHAR(10) NULL,  
                                      [SponsorId] NUMERIC(18, 0) NULL,  
                                      [SponsorMemNo] VARCHAR(50) NULL,  
                                      [SponsorName] VARCHAR(100) NULL,  
                                      [AdjustedToId] NUMERIC(18, 0) NULL,  
                                      [AdjustedToMemNo] VARCHAR(50) NULL,  
                                      [AdjustedToName] VARCHAR(100) NULL,  
                                      [PackageId] NUMERIC(18, 0) NULL,  
                                      [PackageCode] VARCHAR(50) NULL,  
                                      [PackageName] VARCHAR(100) NULL,  
                                      [UpgradePackageId] NUMERIC(18, 0) NULL,  
                                      [UpgradePackageCode] VARCHAR(50) NULL,  
                                      [UpgradePackageName] VARCHAR(100) NULL,  
                                      [LeftCount] NUMERIC(18, 0) NOT NULL,  
                                      [RightCount] NUMERIC(18, 0) NOT NULL,  
                                      [OwnCount] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmLeftCount] NUMERIC(18, 0)  
                                        NOT NULL,  
                                      [ConfirmRightCount] NUMERIC(18, 0)  
                                        NOT NULL,  
                                      [ConfirmOwnCount] NUMERIC(18, 0)  
                                        NOT NULL,  
                                      [LeftPV] NUMERIC(18, 0) NOT NULL,  
                                      [RightPV] NUMERIC(18, 0) NOT NULL,  
                                      [OwnPV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmLeftPV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmRightPV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmOwnPV] NUMERIC(18, 0) NOT NULL,  
                                      [LeftBV] NUMERIC(18, 0) NOT NULL,  
                                      [RightBV] NUMERIC(18, 0) NOT NULL,  
                                      [OwnBV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmLeftBV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmRightBV] NUMERIC(18, 0) NOT NULL,  
                                      [ConfirmOwnBV] NUMERIC(18, 0) NOT NULL,  
                                      [LeftSale] NUMERIC(18, 2) NOT NULL,  
                                      [RightSale] NUMERIC(18, 2) NOT NULL,  
                                      [OwnSale] NUMERIC(18, 2) NOT NULL,  
                                      [ConfirmLeftSale] NUMERIC(18, 2)  
                                        NOT NULL,  
                                      [ConfirmRightSale] NUMERIC(18, 2)  
                                        NOT NULL,  
                                      [ConfirmOwnSale] NUMERIC(18, 2) NOT NULL,  
                                      [AwardId] NUMERIC(18, 0) NULL,  
                                      [AwardName] VARCHAR(100) NULL,  
                                      [AwardQualDate] DATETIME NULL,  
                                      [IsAvailable] BIT NOT NULL,  
                                      [directCount] BIT NOT NULL)  
                                          
     
    INSERT  INTO #GenerationTreeView (MemPosition, MemLevel, MemId, CustomerId,  
                                      MemNo, MemName, MemJoiningAmount,  
                                      MemConfFlag, MemConfDate, MemImgTypeId,  
                                      MemStatus,  
           /*MemCurrStatusId, MemCurrStatus,  
                                      MemStatusImg, MemGenealogyIconPath,*/  
                                      DOJ, DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY,  
                                      SponsorId, SponsorMemNo, SponsorName,  
                                      AdjustedToId, AdjustedToMemNo,  
                                      AdjustedToName, PackageId, PackageCode,  
                                      PackageName, UpgradePackageId,  
                                      UpgradePackageCode, UpgradePackageName,  
                                      LeftCount, RightCount, OwnCount,  
                                      ConfirmLeftCount, ConfirmRightCount,  
                                      ConfirmOwnCount, LeftPV, RightPV, OwnPV,  
                                      ConfirmLeftPV, ConfirmRightPV,  
                                      ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                                      ConfirmLeftBV, ConfirmRightBV,  
                                      ConfirmOwnBV, LeftSale, RightSale,  
                                      OwnSale, ConfirmLeftSale,  
                                      ConfirmRightSale, ConfirmOwnSale,  
                                      AwardId, AwardName, AwardQualDate,  
                                      IsAvailable, directCount)  
            SELECT  --@counter AS 'Id',  
                    ROW_NUMBER() OVER (ORDER BY MemId) AS 'MemPosition',  
                    0 AS 'MemLevel', MemId, CustomerId, MemNo, MemName,  
                    MemJoiningAmount, MemConfFlag, MemConfDate, MemImgTypeId,  
                    MemStatus, /*MemCurrStatusId, MemCurrStatus,  
                    MemStatusImg, NULL AS 'MemGenealogyIconPath',*/ DOJ,  
                    DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY, SponsorId, SponsorMemNo,  
                    SponsorName, AdjustedToId, AdjustedToMemNo, AdjustedToName,  
                    PackageId, PackageCode, PackageName, UpgradePackageId,  
                    UpgradePackageCode, UpgradePackageName, LeftCount,  
                    RightCount, OwnCount, ConfirmLeftCount, ConfirmRightCount,  
                    ConfirmOwnCount, LeftPV, RightPV, OwnPV, ConfirmLeftPV,  
                    ConfirmRightPV, ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                    ConfirmLeftBV, ConfirmRightBV, ConfirmOwnBV, LeftSale,  
                    RightSale, OwnSale, ConfirmLeftSale, ConfirmRightSale,  
                    ConfirmOwnSale, AwardId, AwardName, AwardQualDate, 0, 0  
            FROM    vw_TreeViewInfo  WITH (NOLOCK)
            WHERE   vw_TreeViewInfo.MemId = @sponId  
                      
 ------------------------------------------------------------------------------------------------------------------------------------------------  
  
    -- PRINT 'BINARY TREE' ========================================================================================================================  
  
    WHILE @counter < 5   
        BEGIN  
            --PRINT @counter  
            --PRINT @sponId  
            SELECT  @cnt = 1  
              
            IF @counter = 1 OR @counter = 2 OR @counter = 5 OR @counter = 14   
                SET @level = @level + 1  
                      
            IF EXISTS ( SELECT  MemId  
                        FROM    vw_TreeViewInfo  WITH (NOLOCK)
                        WHERE   vw_TreeViewInfo.SponsorId = @sponId )   
                BEGIN  
                    INSERT  INTO #GenerationTreeView (MemPosition, MemLevel,  
                                                      MemId, CustomerId, MemNo,  
                                                      MemName,  
                                                      MemJoiningAmount,  
                                                      MemConfFlag, MemConfDate,  
                                                      MemImgTypeId, MemStatus, /*MemCurrStatusId,  
               MemCurrStatus, MemStatusImg,  
                                                      MemGenealogyIconPath,*/  
                                                      DOJ, DOJ_DDMMYYYY, DOB,  
                                                      DOB_DDMMYYYY, SponsorId,  
                                                      SponsorMemNo,  
                                                      SponsorName,  
                                                      AdjustedToId,  
                                                      AdjustedToMemNo,  
                                                      AdjustedToName,  
                                                      PackageId, PackageCode,  
                                                      PackageName,  
                                                      UpgradePackageId,  
                                                      UpgradePackageCode,  
                                                      UpgradePackageName,  
                                                      LeftCount, RightCount,  
                                                      OwnCount,  
                                                      ConfirmLeftCount,  
                                                      ConfirmRightCount,  
                                                      ConfirmOwnCount, LeftPV,  
                                                      RightPV, OwnPV,  
                                                      ConfirmLeftPV,  
                                                      ConfirmRightPV,  
                                                      ConfirmOwnPV, LeftBV,  
                                                      RightBV, OwnBV,  
                                                      ConfirmLeftBV,  
                                                      ConfirmRightBV,  
                                                      ConfirmOwnBV, LeftSale,  
                                                      RightSale, OwnSale,  
                                                      ConfirmLeftSale,  
                                                      ConfirmRightSale,  
                                                      ConfirmOwnSale, AwardId,  
                                                      AwardName, AwardQualDate,  
                                                      IsAvailable, directCount)  
                            SELECT TOP 3  
                                    *  
                            FROM    (SELECT ROW_NUMBER() OVER (ORDER BY MemId) AS 'MemPosition',  
                                            @level AS 'MemLevel',  
                                            MemId,  
                                            CustomerId,  
                                            MemNo,  
                                            MemName,  
                                            MemJoiningAmount,  
                                            MemConfFlag,  
                                            MemConfDate,  
                                            MemImgTypeId,  
                                            MemStatus,  
                                            /*MemCurrStatusId,  
                                            MemCurrStatus,  
                                            MemStatusImg,  
                                            NULL AS 'MemGenealogyIconPath',*/  
                                            DOJ,  
                                            DOJ_DDMMYYYY,  
                                            DOB,  
                                            DOB_DDMMYYYY,  
                                            SponsorId,  
                                            SponsorMemNo,  
                                            SponsorName,  
                                            AdjustedToId,  
                                            AdjustedToMemNo,  
                                            AdjustedToName,  
                                            PackageId,  
                                            PackageCode,  
                                            PackageName,  
                                            UpgradePackageId,  
                                            UpgradePackageCode,  
                                            UpgradePackageName,  
                                            LeftCount,  
                                            RightCount,  
                                            OwnCount,  
                                            ConfirmLeftCount,  
                                            ConfirmRightCount,  
                                            ConfirmOwnCount,  
                                            LeftPV,  
                                            RightPV,  
                                            OwnPV,  
                                            ConfirmLeftPV,  
                                            ConfirmRightPV,  
                                            ConfirmOwnPV,  
                                            LeftBV,  
                                            RightBV,  
                                            OwnBV,  
                                            ConfirmLeftBV,  
                                            ConfirmRightBV,  
                                            ConfirmOwnBV,  
                                            LeftSale,  
                                            RightSale,  
                                            OwnSale,  
                                            ConfirmLeftSale,  
                                            ConfirmRightSale,  
                                            ConfirmOwnSale,  
                                            AwardId,  
                                            AwardName,  
                                            AwardQualDate,  
                                            0 AS 'IsAvailable',  
                                            0 AS 'directCount'  
                                     FROM   vw_TreeViewInfo  WITH (NOLOCK)
                                     WHERE  vw_TreeViewInfo.SponsorId = @sponId) CTE  
                            WHERE   MemPosition > (CASE WHEN @level = 1  
                                                        THEN @pageNo  
                                                        ELSE 0  
                                                   END)  
                              
                    SELECT  @cnt = COUNT(MemId) + 1  
                    FROM    #GenerationTreeView WITH (NOLOCK)  
                    WHERE   SponsorId = @sponId  
                END  
                  
            PRINT '@cnt = ' + CAST(@cnt AS VARCHAR(10))  
   
            WHILE @cnt < 4   
                BEGIN  
                    INSERT  INTO #GenerationTreeView (MemPosition, MemLevel,  
                                                      MemId, CustomerId, MemNo,  
                                                      MemName,  
                                                      MemJoiningAmount,  
                                                      MemConfFlag, MemConfDate,  
                                                      MemImgTypeId, MemStatus, /*MemCurrStatusId,  
                                               MemCurrStatus, MemStatusImg,  
                                                      MemGenealogyIconPath,*/  
                                                      DOJ, DOJ_DDMMYYYY, DOB,  
                                                      DOB_DDMMYYYY, SponsorId,  
                                                      SponsorMemNo,  
                                                      SponsorName,  
                                                      AdjustedToId,  
                                                      AdjustedToMemNo,  
                                                      AdjustedToName,  
                                                      PackageId, PackageCode,  
                                                      PackageName,  
                                                      UpgradePackageId,  
                                                      UpgradePackageCode,  
                                                      UpgradePackageName,  
                                                      LeftCount, RightCount,  
                                                      OwnCount,  
                                                      ConfirmLeftCount,  
                                                      ConfirmRightCount,  
                                                      ConfirmOwnCount, LeftPV,  
                                                      RightPV, OwnPV,  
                                                      ConfirmLeftPV,  
                                                      ConfirmRightPV,  
                                                      ConfirmOwnPV, LeftBV,  
                                                      RightBV, OwnBV,  
                                                      ConfirmLeftBV,  
                                                      ConfirmRightBV,  
                                                      ConfirmOwnBV, LeftSale,  
                                                      RightSale, OwnSale,  
                                                      ConfirmLeftSale,  
                                                      ConfirmRightSale,  
                                                      ConfirmOwnSale, AwardId,  
                                                      AwardName, AwardQualDate,  
                                                      IsAvailable, directCount)  
                    VALUES  (@cnt, -- MemPosition - VARCHAR(10)  
                             @level, -- MemLevel - int  
                             NULL, -- MemId - numeric  
                             NULL, -- CustomerId - int  
                             NULL, -- MemNo - varchar(50)  
                             NULL, -- MemName - varchar(100)  
                             NULL, -- MemJoiningAmount - numeric  
                         NULL, -- MemConfFlag - int  
                             NULL, -- MemConfDate - int  
                             NULL, -- MemImgTypeId - int  
                             NULL, -- MemStatus - varchar(50)  
                             --NULL, -- MemCurrStatusId - int  
                             --NULL, -- MemCurrStatus - varchar(50)  
                             --NULL, -- MemStatusImg - image  
                             --NULL, -- MemGenealogyIconPath - varchar(200)  
                             NULL, -- DOJ - datetime  
                             NULL, -- DOJ_DDMMYYYY - varchar(10)  
                             NULL, -- DOB - datetime  
                             NULL, -- DOB_DDMMYYYY - varchar(10)  
                             NULL, -- SponsorId - numeric  
                             NULL, -- SponsorMemNo - varchar(50)  
                             NULL, -- SponsorName - varchar(100)  
                             NULL, -- AdjustedToId - numeric  
                             NULL, -- AdjustedToMemNo - varchar(50)  
                             NULL, -- AdjustedToName - varchar(100)  
                             NULL, -- PackageId - numeric  
                             NULL, -- PackageCode - varchar(100)  
                             NULL, -- PackageName - varchar(100)  
                             NULL, -- UpgradePackageId - numeric  
                             NULL, -- UpgradePackageCode - varchar(100)  
                             NULL, -- UpgradePackageName - varchar(100)  
                             0, -- LeftCount - numeric  
                             0, -- RightCount - numeric  
                             0, -- OwnCount - numeric  
                             0, -- ConfirmLeftCount - numeric  
                             0, -- ConfirmRightCount - numeric  
                             0, -- ConfirmOwnCount - numeric  
                             0, -- LeftPV - numeric  
                             0, -- RightPV - numeric  
                             0, -- OwnPV - numeric  
                             0, -- ConfirmLeftPV - numeric  
                             0, -- ConfirmRightPV - numeric  
                             0, -- ConfirmOwnPV - numeric  
                             0, -- LeftBV - numeric  
                             0, -- RightBV - numeric  
                             0, -- OwnBV - numeric  
                             0, -- ConfirmLeftBV - numeric  
                             0, -- ConfirmRightBV - numeric  
                             0, -- ConfirmOwnBV - numeric  
                             0, -- LeftSale - numeric  
                             0, -- RightSale - numeric  
                             0, -- OwnSale - numeric  
                             0, -- ConfirmLeftSale - numeric  
                             0, -- ConfirmRightSale - numeric  
                             0, -- ConfirmOwnSale - numeric  
                             NULL, -- AwardId - numeric  
                             NULL, -- AwardName - varchar(100)  
                             NULL,  -- AwardQualDate - datetime  
                             0, -- IsAvailable - bit  
                             0 -- directCount - bit  
                             )  
                                       
                    SET @cnt = @cnt + 1  
                END  
                  
                      
            --PRINT '@sponId = ' + CAST(@sponId AS VARCHAR(10))  
            --PRINT '@Id = ' + CAST(@Id AS VARCHAR(10))  
            --PRINT '@counter = ' + CAST(@counter AS VARCHAR(10))  
            --PRINT '@level = ' + CAST(@level AS VARCHAR(10))  
            --PRINT '------------------------------'  
                      
            SELECT TOP 1  
                    @sponId = ISNULL(MemId, -1), @Id = Id  
            FROM    #GenerationTreeView WITH (NOLOCK)  
            WHERE   Id > @Id  
            ORDER BY Id                      
                       
            SET @counter = @counter + 1  
   END  
      
    SELECT  @counter = 1, @level = 1  
      
    WHILE @counter < 15   
        BEGIN  
            IF EXISTS ( SELECT  MemId  
                        FROM    #GenerationTreeView WITH (NOLOCK)  
                        WHERE   Id = @counter AND MemId IS NOT NULL )   
                BEGIN  
                    SELECT  @sponsorId = MemId, @sponsorMemNo = MemNo,  
                            @sponsorName = MemName  
                    FROM    #GenerationTreeView WITH (NOLOCK)  
                    WHERE   Id = @counter  
                      
                    SELECT  @t1 = (CASE WHEN @counter = 1 THEN 2  
                                        WHEN @counter = 2 THEN 5  
                                        WHEN @counter = 3 THEN 8  
                                        WHEN @counter = 4 THEN 11  
                                   END), @t2 = (CASE WHEN @counter = 1 THEN 3  
                                                     WHEN @counter = 2 THEN 6  
                                                     WHEN @counter = 3 THEN 9  
                                                     WHEN @counter = 4 THEN 12  
                                                END),  
                            @t3 = (CASE WHEN @counter = 1 THEN 4  
                                        WHEN @counter = 2 THEN 7  
                                        WHEN @counter = 3 THEN 10  
                                        WHEN @counter = 4 THEN 13  
                                   END)  
      
                    UPDATE  #GenerationTreeView  
                    SET     IsAvailable = 1, /*MemStatusImg = (SELECT  
                                                              GM_decExtra1  
                                                             FROM  
                                                              GenoDesc_Mst  
                                                             WHERE  
                                                              GM_ID = 0),*/  
                            SponsorId = @sponsorId,  
                            SponsorMemNo = @sponsorMemNo,  
                            SponsorName = @sponsorName  
                    WHERE   Id IN (@t1, @t2, @t3) AND MemId IS NULL  
                END  
                  
            SET @counter = @counter + 1  
              
            IF @counter = 2 OR @counter = 5 OR @counter = 14   
                SET @level = @level + 1  
        END  
          
    SELECT  Id, MemPosition, MemLevel, MemId, CustomerId, MemNo, MemName,  
            MemJoiningAmount, MemConfFlag, MemConfDate, MemImgTypeId,  
            MemStatus,  
            /*MemCurrStatusId, MemCurrStatus, MemStatusImg,  
            MemGenealogyIconPath,*/ DOJ, DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY,  
            SponsorId, SponsorMemNo, SponsorName, AdjustedToId,  
            AdjustedToMemNo, AdjustedToName, PackageId, PackageCode,  
            PackageName, UpgradePackageId, UpgradePackageCode,  
            UpgradePackageName, LeftCount, RightCount, OwnCount,  
            ConfirmLeftCount, ConfirmRightCount, ConfirmOwnCount, LeftPV,  
            RightPV, OwnPV, ConfirmLeftPV, ConfirmRightPV, ConfirmOwnPV,  
            LeftBV, RightBV, OwnBV, ConfirmLeftBV, ConfirmRightBV,  
            ConfirmOwnBV, LeftSale, RightSale, OwnSale, ConfirmLeftSale,  
            ConfirmRightSale, ConfirmOwnSale, AwardId, AwardName,  
            AwardQualDate, IsAvailable,  
            ISNULL((SELECT  COUNT(MJD_Introducer)  
                    FROM    MemJoining_Dtls WITH (NOLOCK)  
                    WHERE   MJD_Introducer = #GenerationTreeView.MemId), 0) AS 'directCount'  
    FROM    #GenerationTreeView WITH (NOLOCK)  
END  
GO
CREATE PROCEDURE prc_GenerationAdjustedToTreeView  
    @sessionId NUMERIC(18, 0),  
    @memId NUMERIC(18, 0),  
    @pageNo INT  
AS   
BEGIN  
    SET NOCOUNT ON  
                              
    DECLARE @counter INT,  
        @Id INT,  
        @adjToId NUMERIC(18, 0),  
        @level INT,  
        @memNo VARCHAR(10),  
        @adjustedToId VARCHAR(10),  
        @adjustedToMemNo VARCHAR(10),  
        @adjustedToName VARCHAR(10),  
        @cnt INT,  
        @t1 INT,  
        @t2 INT,  
        @t3 INT  
                  
    SELECT  @counter = 1, @Id = 1,  
            @adjToId = (CASE WHEN @memId = 0 THEN @sessionId  
                             ELSE @memId  
                        END), @level = 0  
  
    IF OBJECT_ID('tempdb..#GenerationAdjustedToTreeView', 'U') IS NOT NULL   
        BEGIN  
            -- PRINT 'DROP TABLE #GenerationAdjustedToTreeView'  
            DROP TABLE #GenerationAdjustedToTreeView  
        END  
          
    CREATE TABLE #GenerationAdjustedToTreeView ([Id] INT IDENTITY(1, 1)  
                                                         PRIMARY KEY  
                                                         NOT NULL,  
                                                [MemPosition] INT NULL,  
                                                [MemLevel] INT NULL,  
                                                [MemId] NUMERIC(18, 0) NULL,  
                                                [CustomerId] INT NULL,  
                                                [MemNo] VARCHAR(50) NULL,  
                                                [MemName] VARCHAR(100) NULL,  
                                                [MemJoiningAmount]  
                                                    NUMERIC(18, 2) NULL,  
                                                [MemConfFlag] INT NULL,  
                                                [MemConfDate] DATETIME NULL,  
                                                [MemImgTypeId] INT NULL,  
                                                [MemStatus] VARCHAR(50) NULL,  
												--[MemCurrStatusId] INT NULL,  
												--[MemCurrStatus] VARCHAR(50) NULL,  
                                                --[MemStatusImg] IMAGE NULL,  
                                                --[MemGenealogyIconPath]  
                                                --    VARCHAR(200) NULL,  
                                                [DOJ] DATETIME NULL,  
                                                [DOJ_DDMMYYYY] VARCHAR(10)  
                                                    NULL,  
                                                [DOB] DATETIME NULL,  
                                                [DOB_DDMMYYYY] VARCHAR(10)  
                                                    NULL,  
                                                [SponsorId] NUMERIC(18, 0)  
                                                    NULL,  
                                                [SponsorMemNo] VARCHAR(50)  
                                                    NULL,  
                                                [SponsorName] VARCHAR(100)  
                                                    NULL,  
                                                [AdjustedToId] NUMERIC(18, 0)  
                                                    NULL,  
                                                [AdjustedToMemNo] VARCHAR(50)  
                                                    NULL,  
                                                [AdjustedToName] VARCHAR(100)  
                                                    NULL,  
                                                [PackageId] NUMERIC(18, 0)  
                                                    NULL,  
                                                [PackageCode] VARCHAR(50) NULL,  
                                                [PackageName] VARCHAR(100)  
                                                    NULL,  
                                                [UpgradePackageId]  
                                                    NUMERIC(18, 0) NULL,  
                                                [UpgradePackageCode]  
                                                    VARCHAR(50) NULL,  
                                                [UpgradePackageName]  
                                                    VARCHAR(100) NULL,  
                                                [LeftCount] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [RightCount] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [OwnCount] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [ConfirmLeftCount]  
                                                    NUMERIC(18, 0) NOT NULL,  
                                                [ConfirmRightCount]  
                                                    NUMERIC(18, 0) NOT NULL,  
                                                [ConfirmOwnCount]  
                                                    NUMERIC(18, 0) NOT NULL,  
                                                [LeftPV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [RightPV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [OwnPV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [ConfirmLeftPV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [ConfirmRightPV]  
                                                    NUMERIC(18, 0) NOT NULL,  
                                                [ConfirmOwnPV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [LeftBV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [RightBV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [OwnBV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [ConfirmLeftBV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [ConfirmRightBV]  
                                                    NUMERIC(18, 0) NOT NULL,  
                                                [ConfirmOwnBV] NUMERIC(18, 0)  
                                                    NOT NULL,  
                                                [LeftSale] NUMERIC(18, 2)  
                                                    NOT NULL,  
                                                [RightSale] NUMERIC(18, 2)  
                                                    NOT NULL,  
                                                [OwnSale] NUMERIC(18, 2)  
                                                    NOT NULL,  
                                                [ConfirmLeftSale]  
                                                    NUMERIC(18, 2) NOT NULL,  
                                                [ConfirmRightSale]  
                                                    NUMERIC(18, 2) NOT NULL,  
                                                [ConfirmOwnSale]  
                                                    NUMERIC(18, 2) NOT NULL,  
                                                [AwardId] NUMERIC(18, 0) NULL,  
                                                [AwardName] VARCHAR(100) NULL,  
                                                [AwardQualDate] DATETIME NULL,  
                                      [IsAvailable] BIT NOT NULL,  
                                                [directCount] BIT NOT NULL)  
                                          
     
    INSERT  INTO #GenerationAdjustedToTreeView (MemPosition, MemLevel, MemId,  
                                                CustomerId, MemNo, MemName,  
                                                MemJoiningAmount, MemConfFlag,  
                                                MemConfDate, MemImgTypeId,  
                                                MemStatus,  
                               /*MemCurrStatusId, MemCurrStatus,  
                                                MemStatusImg,  
                                                MemGenealogyIconPath,*/ DOJ,  
                                                DOJ_DDMMYYYY, DOB,  
                                                DOB_DDMMYYYY, SponsorId,  
                                                SponsorMemNo, SponsorName,  
                                                AdjustedToId, AdjustedToMemNo,  
                                                AdjustedToName, PackageId,  
                                                PackageCode, PackageName,  
                                                UpgradePackageId,  
                                                UpgradePackageCode,  
                                                UpgradePackageName, LeftCount,  
                                                RightCount, OwnCount,  
                                                ConfirmLeftCount,  
                                                ConfirmRightCount,  
                                                ConfirmOwnCount, LeftPV,  
                                                RightPV, OwnPV, ConfirmLeftPV,  
                                                ConfirmRightPV, ConfirmOwnPV,  
                                                LeftBV, RightBV, OwnBV,  
                                                ConfirmLeftBV, ConfirmRightBV,  
                                                ConfirmOwnBV, LeftSale,  
                                                RightSale, OwnSale,  
                                                ConfirmLeftSale,  
                                                ConfirmRightSale,  
                                                ConfirmOwnSale, AwardId,  
                                                AwardName, AwardQualDate,  
                                                IsAvailable, directCount)  
            SELECT  --@counter AS 'Id',  
                    ROW_NUMBER() OVER (ORDER BY MemId) AS 'MemPosition',  
                    0 AS 'MemLevel', MemId, CustomerId, MemNo, MemName,  
                    MemJoiningAmount, MemConfFlag, MemConfDate, MemImgTypeId,  
                    MemStatus, /*MemCurrStatusId, MemCurrStatus,  
                    MemStatusImg, NULL AS 'MemGenealogyIconPath',*/ DOJ,  
                    DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY, SponsorId, SponsorMemNo,  
                    SponsorName, AdjustedToId, AdjustedToMemNo, AdjustedToName,  
                    PackageId, PackageCode, PackageName, UpgradePackageId,  
                    UpgradePackageCode, UpgradePackageName, LeftCount,  
                    RightCount, OwnCount, ConfirmLeftCount, ConfirmRightCount,  
                    ConfirmOwnCount, LeftPV, RightPV, OwnPV, ConfirmLeftPV,  
                    ConfirmRightPV, ConfirmOwnPV, LeftBV, RightBV, OwnBV,  
                    ConfirmLeftBV, ConfirmRightBV, ConfirmOwnBV, LeftSale,  
                    RightSale, OwnSale, ConfirmLeftSale, ConfirmRightSale,  
                    ConfirmOwnSale, AwardId, AwardName, AwardQualDate, 0, 0  
            FROM    vw_TreeViewInfo  WITH (NOLOCK)
            WHERE   vw_TreeViewInfo.MemId = @adjToId  
                      
 ------------------------------------------------------------------------------------------------------------------------------------------------  
  
    -- PRINT 'BINARY TREE' ========================================================================================================================  
  
    WHILE @counter < 5   
        BEGIN  
            --PRINT @counter  
            --PRINT @adjToId  
            SELECT  @cnt = 1  
              
            IF @counter = 1 OR @counter = 2 OR @counter = 5 OR @counter = 14   
                SET @level = @level + 1  
                      
            IF EXISTS ( SELECT  MemId  
                        FROM    vw_TreeViewInfo  WITH (NOLOCK)
                        WHERE   vw_TreeViewInfo.AdjustedToId = @adjToId )   
                BEGIN  
                    INSERT  INTO #GenerationAdjustedToTreeView (MemPosition,  
                                                              MemLevel, MemId,  
                                                              CustomerId,  
                                                              MemNo, MemName,  
                                                              MemJoiningAmount,  
                                                              MemConfFlag,  
                                                              MemConfDate,  
                                                              MemImgTypeId,  
                                                              MemStatus, /*MemCurrStatusId,  
                 MemCurrStatus, MemStatusImg,  
                                                              MemGenealogyIconPath,*/  
                                                              DOJ,  
                                                              DOJ_DDMMYYYY,  
                                                              DOB,  
                                                              DOB_DDMMYYYY,  
                                                              SponsorId,  
                                                              SponsorMemNo,  
                                                              SponsorName,  
                                                              AdjustedToId,  
                                                              AdjustedToMemNo,  
                                                              AdjustedToName,  
                                                              PackageId,  
                                                              PackageCode,  
                                                              PackageName,  
                                                              UpgradePackageId,  
                                                              UpgradePackageCode,  
                                                              UpgradePackageName,  
                                                              LeftCount,  
                                                              RightCount,  
                                                              OwnCount,  
                                                              ConfirmLeftCount,  
                                                              ConfirmRightCount,  
                                                              ConfirmOwnCount,  
                                                              LeftPV, RightPV,  
                                                              OwnPV,  
                                                              ConfirmLeftPV,  
                                                              ConfirmRightPV,  
                                                              ConfirmOwnPV,  
                                                              LeftBV, RightBV,  
                                                              OwnBV,  
                                                              ConfirmLeftBV,  
                                                              ConfirmRightBV,  
                                                              ConfirmOwnBV,  
                                                              LeftSale,  
                               RightSale,  
                                                              OwnSale,  
                                                              ConfirmLeftSale,  
                                                              ConfirmRightSale,  
                                                              ConfirmOwnSale,  
                                                              AwardId,  
                                                              AwardName,  
                                                              AwardQualDate,  
                                                              IsAvailable,  
                                                              directCount)  
                            SELECT TOP 3  
                                    *  
                            FROM    (SELECT ROW_NUMBER() OVER (ORDER BY MemId) AS 'MemPosition',  
                                            @level AS 'MemLevel',  
                                            MemId,  
                                            CustomerId,  
                                            MemNo,  
                                            MemName,  
                                            MemJoiningAmount,  
                                            MemConfFlag,  
                                            MemConfDate,  
                                            MemImgTypeId,  
                                            MemStatus,  
                                            /*MemCurrStatusId,  
                                            MemCurrStatus,  
                                            MemStatusImg,  
                                            NULL AS 'MemGenealogyIconPath',*/  
                                            DOJ,  
                                            DOJ_DDMMYYYY,  
                                            DOB,  
                                            DOB_DDMMYYYY,  
                                            SponsorId,  
                                            SponsorMemNo,  
                                            SponsorName,  
                                            AdjustedToId,  
                                            AdjustedToMemNo,  
                                            AdjustedToName,  
                                            PackageId,  
                                            PackageCode,  
                                            PackageName,  
                                            UpgradePackageId,  
                                            UpgradePackageCode,  
                                            UpgradePackageName,  
                                            LeftCount,  
                                            RightCount,  
                                            OwnCount,  
                                            ConfirmLeftCount,  
                                            ConfirmRightCount,  
                                            ConfirmOwnCount,  
                                            LeftPV,  
                                            RightPV,  
                                            OwnPV,  
                                            ConfirmLeftPV,  
                                            ConfirmRightPV,  
                                            ConfirmOwnPV,  
                                            LeftBV,  
                                            RightBV,  
                                            OwnBV,  
                                            ConfirmLeftBV,  
                                            ConfirmRightBV,  
                                            ConfirmOwnBV,  
                                            LeftSale,  
                                            RightSale,  
                                            OwnSale,  
                                            ConfirmLeftSale,  
                                            ConfirmRightSale,  
                                            ConfirmOwnSale,  
                                            AwardId,  
                                            AwardName,  
                                            AwardQualDate,  
                                            0 AS 'IsAvailable',  
                                            0 AS 'directCount'  
                                     FROM   vw_TreeViewInfo  WITH (NOLOCK)
                                     WHERE  vw_TreeViewInfo.AdjustedToId = @adjToId) CTE  
                            WHERE   MemPosition > (CASE WHEN @level = 1  
                                                        THEN @pageNo  
                                                        ELSE 0  
                                                   END)  
                              
                    SELECT  @cnt = COUNT(MemId) + 1  
                    FROM    #GenerationAdjustedToTreeView WITH (NOLOCK)  
                    WHERE   AdjustedToId = @adjToId  
                END  
                  
            PRINT '@cnt = ' + CAST(@cnt AS VARCHAR(10))  
   
            WHILE @cnt < 4   
                BEGIN  
                    INSERT  INTO #GenerationAdjustedToTreeView (MemPosition,  
                                                              MemLevel, MemId,  
                                                              CustomerId,  
                                                              MemNo, MemName,  
                                                              MemJoiningAmount,  
                                                              MemConfFlag,  
                                                              MemConfDate,  
                                                              MemImgTypeId,  
                                                              MemStatus, /*MemCurrStatusId,  
                 MemCurrStatus, MemStatusImg,  
                                                              MemGenealogyIconPath,*/  
                                                              DOJ,  
                                                              DOJ_DDMMYYYY,  
                                                              DOB,  
                                                              DOB_DDMMYYYY,  
                                                              SponsorId,  
                                                              SponsorMemNo,  
                                                              SponsorName,  
                                                              AdjustedToId,  
                                                              AdjustedToMemNo,  
                                                              AdjustedToName,  
                                                              PackageId,  
                                                              PackageCode,  
                                                              PackageName,  
                                                              UpgradePackageId,  
                                                              UpgradePackageCode,  
                                                              UpgradePackageName,  
                                                              LeftCount,  
                                                              RightCount,  
                                                              OwnCount,  
                                                              ConfirmLeftCount,  
                                                              ConfirmRightCount,  
                                                              ConfirmOwnCount,  
                                                              LeftPV, RightPV,  
                                                              OwnPV,  
                                                              ConfirmLeftPV,  
                                                              ConfirmRightPV,  
                                                       ConfirmOwnPV,  
                                                              LeftBV, RightBV,  
                                                              OwnBV,  
                                                              ConfirmLeftBV,  
                                                              ConfirmRightBV,  
                                                              ConfirmOwnBV,  
                                                              LeftSale,  
                                                              RightSale,  
                                                              OwnSale,  
                                                              ConfirmLeftSale,  
                                                              ConfirmRightSale,  
                                                              ConfirmOwnSale,  
                                                              AwardId,  
                                                              AwardName,  
                                                              AwardQualDate,  
                                                              IsAvailable,  
                                                              directCount)  
                    VALUES  (@cnt, -- MemPosition - VARCHAR(10)  
                             @level, -- MemLevel - int  
                             NULL, -- MemId - numeric  
                             NULL, -- CustomerId - int  
                             NULL, -- MemNo - varchar(50)  
                             NULL, -- MemName - varchar(100)  
                             NULL, -- MemJoiningAmount - numeric  
                             NULL, -- MemConfFlag - int  
                             NULL, -- MemConfDate - int  
                             NULL, -- MemImgTypeId - int  
                             NULL, -- MemStatus - varchar(50)  
                             --NULL, -- MemCurrStatusId - int  
                             --NULL, -- MemCurrStatus - varchar(50)  
                             --NULL, -- MemStatusImg - image  
                             --NULL, -- MemGenealogyIconPath - varchar(200)  
                             NULL, -- DOJ - datetime  
                             NULL, -- DOJ_DDMMYYYY - varchar(10)  
                             NULL, -- DOB - datetime  
                             NULL, -- DOB_DDMMYYYY - varchar(10)  
                             NULL, -- SponsorId - numeric  
                             NULL, -- SponsorMemNo - varchar(50)  
                             NULL, -- SponsorName - varchar(100)  
                             NULL, -- AdjustedToId - numeric  
                             NULL, -- AdjustedToMemNo - varchar(50)  
                             NULL, -- AdjustedToName - varchar(100)  
                             NULL, -- PackageId - numeric  
                             NULL, -- PackageCode - varchar(100)  
                             NULL, -- PackageName - varchar(100)  
                             NULL, -- UpgradePackageId - numeric  
                             NULL, -- UpgradePackageCode - varchar(100)  
                             NULL, -- UpgradePackageName - varchar(100)  
                             0, -- LeftCount - numeric  
                             0, -- RightCount - numeric  
                             0, -- OwnCount - numeric  
                             0, -- ConfirmLeftCount - numeric  
                             0, -- ConfirmRightCount - numeric  
                             0, -- ConfirmOwnCount - numeric  
                             0, -- LeftPV - numeric  
                             0, -- RightPV - numeric  
                             0, -- OwnPV - numeric  
                             0, -- ConfirmLeftPV - numeric  
                             0, -- ConfirmRightPV - numeric  
                             0, -- ConfirmOwnPV - numeric  
                             0, -- LeftBV - numeric  
                          0, -- RightBV - numeric  
                             0, -- OwnBV - numeric  
                             0, -- ConfirmLeftBV - numeric  
                             0, -- ConfirmRightBV - numeric  
                             0, -- ConfirmOwnBV - numeric  
                             0, -- LeftSale - numeric  
                             0, -- RightSale - numeric  
                             0, -- OwnSale - numeric  
                             0, -- ConfirmLeftSale - numeric  
                             0, -- ConfirmRightSale - numeric  
                             0, -- ConfirmOwnSale - numeric  
                             NULL, -- AwardId - numeric  
                             NULL, -- AwardName - varchar(100)  
                             NULL,  -- AwardQualDate - datetime  
                             0, -- IsAvailable - bit  
                             0 -- directCount - bit  
                             )  
                                       
                    SET @cnt = @cnt + 1  
                END  
            SELECT TOP 1  
                    @adjToId = ISNULL(MemId, -1), @Id = Id  
            FROM    #GenerationAdjustedToTreeView WITH (NOLOCK)  
            WHERE   Id > @Id  
            ORDER BY Id                      
                       
            SET @counter = @counter + 1  
        END  
      
    SELECT  @counter = 1, @level = 1  
          
    WHILE @counter < 15   
        BEGIN  
            IF EXISTS ( SELECT  MemId  
                        FROM    #GenerationAdjustedToTreeView WITH (NOLOCK)  
                        WHERE   Id = @counter AND MemId IS NOT NULL )   
                BEGIN  
                    SELECT  @adjustedToId = MemId, @adjustedToMemNo = MemNo,  
                            @adjustedToName = MemName  
                    FROM    #GenerationAdjustedToTreeView WITH (NOLOCK)  
                    WHERE   Id = @counter  
                      
                    SELECT  @t1 = (CASE WHEN @counter = 1 THEN 2  
                                        WHEN @counter = 2 THEN 5  
                                        WHEN @counter = 3 THEN 8  
                                        WHEN @counter = 4 THEN 11  
                                   END), @t2 = (CASE WHEN @counter = 1 THEN 3  
                                                     WHEN @counter = 2 THEN 6  
                                                     WHEN @counter = 3 THEN 9  
                                                     WHEN @counter = 4 THEN 12  
                                                END),  
                            @t3 = (CASE WHEN @counter = 1 THEN 4  
                                        WHEN @counter = 2 THEN 7  
                                        WHEN @counter = 3 THEN 10  
                                        WHEN @counter = 4 THEN 13  
                                   END)  
                    UPDATE  #GenerationAdjustedToTreeView  
                    SET     IsAvailable = 1, /*MemStatusImg = (SELECT  
                                                              GM_decExtra1  
                                                             FROM  
                                                              GenoDesc_Mst  
                                                             WHERE  
                                         GM_ID = 0),*/  
                            AdjustedToId = @adjustedToId,  
                            AdjustedToMemNo = @adjustedToMemNo,  
                            AdjustedToName = @adjustedToName  
                    WHERE   Id IN (@t1, @t2, @t3) AND MemId IS NULL  
                END  
                  
            SET @counter = @counter + 1  
              
            IF @counter = 2 OR @counter = 5 OR @counter = 14   
                SET @level = @level + 1  
        END  
          
    SELECT  Id, MemPosition, MemLevel, MemId, CustomerId, MemNo, MemName,  
            MemJoiningAmount, MemConfFlag, MemConfDate, MemImgTypeId,  
            MemStatus,  
            /*MemCurrStatusId, MemCurrStatus, MemStatusImg,  
            MemGenealogyIconPath,*/ DOJ, DOJ_DDMMYYYY, DOB, DOB_DDMMYYYY,  
            SponsorId, SponsorMemNo, SponsorName, AdjustedToId,  
            AdjustedToMemNo, AdjustedToName, PackageId, PackageCode,  
            PackageName, UpgradePackageId, UpgradePackageCode,  
            UpgradePackageName, LeftCount, RightCount, OwnCount,  
            ConfirmLeftCount, ConfirmRightCount, ConfirmOwnCount, LeftPV,  
            RightPV, OwnPV, ConfirmLeftPV, ConfirmRightPV, ConfirmOwnPV,  
            LeftBV, RightBV, OwnBV, ConfirmLeftBV, ConfirmRightBV,  
            ConfirmOwnBV, LeftSale, RightSale, OwnSale, ConfirmLeftSale,  
            ConfirmRightSale, ConfirmOwnSale, AwardId, AwardName,  
            AwardQualDate, IsAvailable,  
            ISNULL((SELECT  COUNT(MJD_Introducer)  
                    FROM    MemJoining_Dtls WITH (NOLOCK)  
                    WHERE   MJD_Introducer = #GenerationAdjustedToTreeView.MemId),  
                   0) AS 'directCount'  
    FROM    #GenerationAdjustedToTreeView WITH (NOLOCK)  
END  
GO
CREATE VIEW vw_OrderList          
AS          
    SELECT  [Order].Id ,          
            StoreID ,          
            --'Herbayu Wellness India Pvt. Ltd.' AS StoreName ,          
            orderOH_code ,          
            OrderTotal ,          
            OrderShippingExclTax ,          
            OrderTax ,          
            OrderStatusID ,          
            ( CASE WHEN OrderStatusID = 10 THEN 'Pending'          
                   WHEN OrderStatusID = 20 THEN 'Processing'          
                   WHEN OrderStatusID = 30 THEN 'Complete'          
                   WHEN OrderStatusID = 40 THEN 'Cancelled'          
                   ELSE ''          
              END ) AS OrderStatus ,          
            PaymentStatusId ,          
            ( CASE WHEN PaymentStatusId = 10 THEN 'Pending'          
                   WHEN PaymentStatusId = 20 THEN 'Authorized'          
                   WHEN PaymentStatusId = 30 THEN 'Paid'          
                   WHEN PaymentStatusId = 35 THEN 'Partially Refunded'          
                   WHEN PaymentStatusId = 40 THEN 'Refunded'          
                   WHEN PaymentStatusId = 50 THEN 'Voided'          
                   ELSE ''          
              END ) AS PaymentStatus ,          
            ShippingStatusId ,          
            ( CASE WHEN ShippingStatusId = 10 THEN 'Shipping Not Required'          
                   WHEN ShippingStatusId = 20 THEN 'Not Yet Shipped'          
                   WHEN ShippingStatusId = 25 THEN 'Partially Shipped'          
                   WHEN ShippingStatusId = 30 THEN 'Shipped'          
                   WHEN ShippingStatusId = 40 THEN 'Delivered'          
                   ELSE ''          
              END ) AS ShippingStatus ,          
            BillingAddressId ,          
            ShippingAddressId ,          
            PM_Name AS PaymentMethodSystemName ,          
            BillingAddress.Email ,          
            BillingAddress.FirstName ,          
            BillingAddress.LastName ,          
            BillingAddress.FirstName + ' ' + BillingAddress.LastName AS fullName ,          
            CustomerId ,          
            Memno , --Customer.Memno,            
            [Order].CreatedOnUtc ,          
            PaidDateUtc ,          
            OrderAgains ,          
            OrderTo ,          
            OrderType ,          
            InvoiceNo ,          
            OrderStateID ,          
            [Order].AffiliateId ,          
            OrderGuid,        
            --BillingAddress.Email,            
            -- BillingAddress.FirstName,            
            -- BillingAddress.LastName,            
            BillingCountry.Name AS BillingCountry ,          
            BillingStateProvince.Name AS BillingState ,          
           BillingAddress.City AS BillingCity ,           
            BillingAddress.Address1 AS BillingAddress ,          
            BillingAddress.ZipPostalCode AS BillingZipPostalCode ,          
            BillingAddress.PhoneNumber AS BillingPhoneNumber ,          
            ShippingAddress.Email AS ShippingEmail ,          
            ShippingAddress.FirstName AS ShippingFirstName ,          
            ShippingAddress.LastName AS ShippingLastName ,          
            ShippingCountry.Name AS  ShippingCountry ,          
            ShippingStateProvince.Name AS ShippingState ,          
            ShippingAddress.City AS ShippingCity ,          
            ShippingAddress.Address1 AS ShippingAddress ,          
            ShippingAddress.ZipPostalCode AS ShippingZipPostalCode ,          
            ShippingAddress.PhoneNumber AS ShippingPhoneNumber          
    FROM    [Order] (NOLOCK)          
            INNER JOIN Customer (NOLOCK) ON [Order].CustomerId = Customer.Id          
            LEFT OUTER  JOIN [Address] AS BillingAddress ( NOLOCK ) ON [Order].BillingAddressId = BillingAddress.Id          
   LEFT OUTER  JOIN [Address] AS ShippingAddress ( NOLOCK ) ON [Order].ShippingAddressId = ShippingAddress.Id          
            LEFT OUTER JOIN Paymode_mst (NOLOCK) ON PM_SystemName = PaymentMethodSystemName AND PM_ShoppeFlg=1          
            LEFT OUTER JOIN StateProvince AS BillingStateProvince ( NOLOCK ) ON BillingStateProvince.Id = BillingAddress.StateProvinceId          
            LEFT OUTER JOIN StateProvince AS ShippingStateProvince ( NOLOCK ) ON ShippingStateProvince.Id = ShippingAddress.StateProvinceId          
             LEFT OUTER JOIN country AS BillingCountry ( NOLOCK ) ON BillingCountry.Id = BillingAddress.CountryId          
            LEFT OUTER JOIN country AS ShippingCountry ( NOLOCK ) ON ShippingCountry.Id = ShippingAddress.CountryId          
            --LEFT OUTER JOIN City AS BillingCity ( NOLOCK ) ON BillingCity.Id = BillingAddress.City          
            --LEFT OUTER JOIN City AS ShippingCity ( NOLOCK ) ON ShippingCity.Id = ShippingAddress.City 
Go
CREATE PROCEDURE prc_getOrderList  -- prc_getOrderList '30','30','40,25',0,0,0,1,0,0,'','05/28/18 6:30:00 PM','11/30/19 6:30:00 PM','','','',null,0,0,0,1,0,1,0,0,0,1000        
    @orderStatusId VARCHAR(20) ,      
    @paymentStatusId VARCHAR(20)  ,     
    @shippingStatusId VARCHAR(20)  ,       
    @storeId INT ,      
    @vendorId INT ,      
    @customerId INT ,      
    @orderAgainst INT ,      
    @productId INT ,      
    @warehouseId INT ,      
    @paymentMethodSystemName VARCHAR(500) ,        
   -- @affiliateId INT ,  -- Not Required      
    @createdFromUtc VARCHAR(200) ,      
    @createdToUtc VARCHAR(200) ,      
    @Email VARCHAR(1000) ,      
    @LastName VARCHAR(200) ,      
    @OrderNotes VARCHAR(1000) ,      
    @OrderType INT ,      
    @InvoiceNo VARCHAR(200) ,      
    @trader INT ,      
    @IsTrader BIT ,      
    @IsAdmin BIT ,      
    @IsSuperAdmin BIT ,      
    @LoginCustomerId INT ,      
    @BillingCountryId INT ,       
    @orderGuid VARCHAR(200) ,      
    @PageIndex INT = 0 ,      
    @PageSize INT = 2147483644      
AS       
    BEGIN            
             
        DECLARE @SearchKeywords BIT ,      
            @sql NVARCHAR(MAX) ,      
            @sqlCnt NVARCHAR(MAX) ,      
            @sql_orderby NVARCHAR(MAX)              
        SET NOCOUNT ON              
                   
           --paging          
        DECLARE @PageLowerBound INT          
        DECLARE @PageUpperBound INT          
        DECLARE @RowsToReturn INT          
        SET @RowsToReturn = @PageSize * ( @PageIndex + 1 )           
        SET @PageLowerBound = @PageSize * @PageIndex          
        SET @PageUpperBound = @PageLowerBound + @PageSize + 1          
                
                 
        SET @sql = ''            
        SET @sqlCnt = ''            
        --SET @sql = @sql + 'SELECT  TOP ' + CONVERT(VARCHAR(10), ( @RowsToReturn )) + ' * FROM ( SELECT  ROW_NUMBER()  OVER (ORDER BY CreatedOnUtc desc) AS SrNo ,* FROM  vw_OrderList (nolock)  where 1=1 '        
        SET @sql = @sql      
            + ' SELECT  ROW_NUMBER()  OVER (ORDER BY CreatedOnUtc desc) AS SrNo ,* FROM  vw_OrderList (nolock)  where 1=1 '        
        IF LTRIM(RTRIM(@orderStatusId)) <> ''      
            BEGIN            
                SET @sql = @sql + ' and orderStatusId IN(SELECT CAST(data as int) FROM [nop_splitstring_to_table]( CONVERT(VARCHAR(20), ''' + @orderStatusId + ''') , '','' )) '             
                       
            END            
          IF LTRIM(RTRIM(@paymentStatusId)) <> ''            
            BEGIN         
    SET @sql = @sql + ' and PaymentStatusId IN(SELECT CAST(data as int) FROM [nop_splitstring_to_table]( CONVERT(VARCHAR(20), ''' + @paymentStatusId + ''') , '','' )) '      
          
            END            
          IF LTRIM(RTRIM(@shippingStatusId)) <> ''     
            BEGIN    
    SET @sql = @sql + ' and ShippingStatusId IN(SELECT CAST(data as int) FROM [nop_splitstring_to_table]( CONVERT(VARCHAR(20), '''+ @shippingStatusId + ''') , '','' )) '           
            END            
            
        IF @storeId > 0       
            BEGIN            
                SET @sql = @sql + ' and StoreID = '      
                    + CONVERT(VARCHAR(10), @storeId)            
            END            
            
        IF @customerId > 0       
            BEGIN            
                SET @sql = @sql + ' and CustomerId = '      
                    + CONVERT(VARCHAR(10), @customerId)            
            END            
            
            
        IF @orderAgainst > 0 AND @IsTrader =1 AND @IsAdmin =0      
            BEGIN            
                SET @sql = @sql + ' and OrderAgains = '      
                    + CONVERT(VARCHAR(10), @orderAgainst)            
            END            
            
        IF @productId > 0       
            BEGIN            
                SET @sql = @sql      
                    + ' and ID  in (SELECT OrderId FROM  OrderItem WHERE ProductId IN (   SELECT OrderId FROM  OrderItem (nolock) WHERE ProductId IN (     '      
                    + CONVERT(VARCHAR(10), @productId) + '  )) '            
            END            
    
                      
        IF LTRIM(RTRIM(@paymentMethodSystemName)) <> ''      
            AND @paymentMethodSystemName != NULL       
            BEGIN            
                SET @sql = @sql + ' and PaymentMethodSystemName = '      
                    + CONVERT(VARCHAR(500), @paymentMethodSystemName)            
            END            
             
       IF LTRIM(RTRIM(@OrderType)) <> ''     
            BEGIN            
                SET @sql = @sql + ' and OrderType = '      
                    + CONVERT(VARCHAR(10), @OrderType)            
            END         
              
        
        
             IF @InvoiceNo  > 0       
            BEGIN            
                SET @sql = @sql + ' and InvoiceNo = '      
                    + CONVERT(VARCHAR(10), @InvoiceNo)            
            END         
              
              
       IF @trader  > 0       
            BEGIN            
                SET @sql = @sql + ' and OrderTo = '      
                    + CONVERT(VARCHAR(10), @trader)            
            END         
              
              
            -- IF @IsTrader  > 0       
            --BEGIN            
            --    SET @sql = @sql + ' and AffiliateId = '      
            --        + CONVERT(VARCHAR(10), @affiliateId)            
            --END         
              
              
            -- IF @IsAdmin  > 0       
            --BEGIN            
            --    SET @sql = @sql + ' and AffiliateId = '      
            --        + CONVERT(VARCHAR(10), @affiliateId)            
            --END         
              
              
            -- IF @IsSuperAdmin  > 0       
            --BEGIN            
            --    SET @sql = @sql + ' and AffiliateId = '      
            --        + CONVERT(VARCHAR(10), @affiliateId)            
            --END         
              
              
            -- IF @LoginCustomerId  > 0       
            --BEGIN            
            --    SET @sql = @sql + ' and AffiliateId = '      
            --        + CONVERT(VARCHAR(10), @affiliateId)            
            --END         
              
              
             IF @orderGuid  > 0       
            BEGIN            
                SET @sql = @sql + ' and OrderGuid = '      
                    + CONVERT(VARCHAR(10), @orderGuid)            
            END         
              
              
                  
             
        --IF @affiliateId > 0       
        --    BEGIN            
        --        SET @sql = @sql + ' and AffiliateId = '      
        --            + CONVERT(VARCHAR(10), @affiliateId)            
        --    END            
            
        --IF @affiliateId > 0         
        --    BEGIN            
        --        SET @sql = @sql + ' and AffiliateId = '        
        --            + CONVERT(VARCHAR(10), @affiliateId)            
        --    END            
         
        IF LTRIM(RTRIM(ISNULL(@createdFromUtc, ''))) <> ''      
            AND LTRIM(RTRIM(ISNULL(@createdToUtc, ''))) <> ''       
            BEGIN            
                --SET @sql = @sql + ' and CreatedOnUtc >='''        
                --    + CONVERT(VARCHAR(100), @createdFromUtc)        
                --    + '''  and CreatedOnUtc <='''        
                --    + CONVERT(VARCHAR(100), @createdToUtc) + ''''            
                   
                SET @sql = @sql + ' and CreatedOnUtc >='''      
                    + CONVERT(VARCHAR(50), @createdFromUtc, 106)      
                    + '''  and CreatedOnUtc <='''      
                    + CONVERT(VARCHAR(50), @createdToUtc, 106) + ''''            
                             
            END            
             
            
        IF LTRIM(RTRIM(@Email)) <> ''      
            AND @Email != NULL       
            BEGIN            
                SET @sql = @sql + ' and Email = '      
                    + CONVERT(VARCHAR(500), @Email)            
            END            
             
            
        IF LTRIM(RTRIM(@LastName)) <> ''      
            AND @LastName != NULL       
            BEGIN            
                SET @sql = @sql + ' and fullName like ( ''%'      
                    + CONVERT(VARCHAR(500), @LastName) + '%'')  '            
            END             
                        
      --  SET @sql = @sql + ' order by  CreatedOnUtc desc '            
              
        SET @sqlCnt = @sql        
              
        SET @sql = 'SELECT  TOP ' + CONVERT(VARCHAR(10), ( @RowsToReturn ))      
            + ' * FROM (' + @sql + ' )SResult ' + ' where SrNo >'      
            + CONVERT(VARCHAR(10), @PageLowerBound) + ' AND SrNo <'      
            + CONVERT(VARCHAR(10), @PageUpperBound) + ' Order by SrNo ;'             
               
        SET @sql = @sql + ' SELECT  COUNT(SrNo)  AS TotCnt  FROM  (' + @sqlCnt      
            + ' )SRCnt'        
                  
        PRINT ( @sql )          
        EXEC sp_executesql @sql   
    END   
Go