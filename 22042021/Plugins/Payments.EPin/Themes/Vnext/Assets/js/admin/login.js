﻿$(document).ready(function () {
    var sblbtn = $("#btnlogin");
    var sbrbtn = $("#btnrecover");
    $("#loginform input.username").focus();

    jQuery(function ($) {
        $.supersized({

            // Functionality
            slide_interval: 4000,    // Length between transitions
            transition: 1,    // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
            transition_speed: 1000,    // Speed of transition
            performance: 1,    // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)

            // Size & Position
            min_width: 0,    // Min width allowed (in pixels)
            min_height: 0,    // Min height allowed (in pixels)
            vertical_center: 1,    // Vertically center background
            horizontal_center: 1,    // Horizontally center background
            fit_always: 0,    // Image will never exceed browser width or height (Ignores min. dimensions)
            fit_portrait: 1,    // Portrait images will not exceed browser height
            fit_landscape: 0,    // Landscape images will not exceed browser width

            // Components
            slide_links: 'blank',    // Individual links for each slide (Options: false, 'num', 'name', 'blank')
            slides: [    // Slideshow Images
                        { image: virtualPath + 'img/bg/adminlogin.jpg' },
            ]
        });

    });

    $("#loginform").validate({
        //debug: true,
        rules: {
            AdminUsername: {
                required: {
                    depends: function () {
                        $(this).val($(this).val().trim().toLowerCase());
                        return true;
                    }
                },
                regex: /^[a-zA-Z0-9]+$/,
                rangelength: [4, 20]
            },
            AdminPassword: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                rangelength: [4, 20]
            }
        },
        messages: {
            AdminUsername: {
                required: "Enter a Username (4-20 characters).",
                regex: "Only alphabets and numbers are allowed.",
                rangelength: "Username should be 4-20 characters."
            },
            AdminPassword: {
                required: "Enter a Password (4-20 characters).",
                rangelength: "Password should be 4-20 characters."
            }
        }
    });

    $("#loginform").on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var paramList = {};
                paramList.s = $('#AdminUsername').val().trim() + '|' + $('#AdminPassword').val().trim();
                paramList.returnJSON = true;
                //console.log(JSON.stringify(paramList));
                paramList.s = StringEncryption(paramList.s);

                //console.log(JSON.stringify(paramList));
                //return false;

                var $btn = sblbtn.button('loading');
                sblbtn.prop("disabled", true);

                $.ajax({
                    url: apiPath + "/AuthenticateAdmin",
                    type: "POST",
                    data: paramList,
                    success: function (data, textStatus, xhr) {
                        window.location.href = virtualPath + 'Admin/Dashboard';
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (errorThrown == '')
                            errorThrown = 'Connection is not available.';
                        //$(".flipLink").animate({ opacity: "1" }, 500);
                        bootalert(errorThrown, 'error');
                        $btn.button('reset')
                        sblbtn.prop("disabled", false);
                    },
                    complete: function (a) {
                    }
                });                
            }
            e.preventDefault();
        } catch (e) {
        }
    });

    $("#recoverform").validate({
        rules: {
            EmailId: {
                required: {
                    depends: function () {
                        $(this).val($(this).val().trim().toLowerCase());
                        return true;
                    }
                },
                regex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$/,
                /*regex: /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,*/
                //email: true,
                rangelength: [4, 100]
            }
        },
        messages: {
            EmailId: {
                required: "Enter an Email ID (4-100 characters).",
                regex: "Enter a valid Email ID.",
                //email: "Enter a valid Email ID.",
                rangelength: "Email ID should be 4-100 characters."
            }
        }
    });

    $("#recoverform").on('submit', function (e) {
        e.preventDefault();
        var $btn = sbrbtn.button('loading');
        sbrbtn.prop("disabled", true);

        var Am = new Object();
        Am.BusinessEmailID = $("#EmailId").val().trim();
        var isvalidate = $(this).valid();

        if (isvalidate) {
            $.ajax({
                url: apiPath + "/PasswordResetRequest",
                type: "POST",
                data: Am,
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    if (data == '') {
                        bootalert('Password Reset email sent on your Email ID.', 'success');
                        //$("#EmailId").val('');
                    }
                    else {
                        bootalert(data, 'error');
                    }

                    $btn.button('reset')
                    sbrbtn.prop("disabled", false);
                    $(".flipLink").animate({ opacity: "1" }, 500);
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (errorThrown == '')
                        errorThrown = 'Connection not available.';
                    $btn.button('reset')
                    sbrbtn.prop("disabled", false);
                    $(".flipLink").animate({ opacity: "1" }, 500);
                    bootalert(errorThrown, 'error');
                }
            });
        }
        else {
            $btn.button('reset')
            sbrbtn.prop("disabled", false);
            e.preventDefault();
        }
    });

    $("a#aforgot").on('click', function (e) {
        $('div#loginformcontainer').fadeOut(400, "linear");
        setTimeout(function () {
            $('div#recoveryformcontainer').fadeIn(400, "linear");
        }, 400);
    });

    $("a#asignin").on('click', function (e) {
        $('div#recoveryformcontainer').fadeOut(400, "linear");
        setTimeout(function () {
            $('div#loginformcontainer').fadeIn(400, "linear");
        }, 400);
    });
});
