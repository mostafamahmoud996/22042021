﻿current_page_id = "#myprofile";
var cities = null, cityname = '';

$(document).ready(function (e) {
    try {
        var cp = {};
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/Profile",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(cp),
            dataType: 'json',
            jsonp: null,
            success: function (data, textStatus, xhr) {
                try {
                    //console.log(StringDecryption(data));
                    var sa = [];
                    sa = StringDecryption(data).split("|");
                    switch (sa[0]) {
                        case 'Mr':
                            $("#malePUF").prop('checked', true);
                            break;
                        case 'Ms':
                        case 'Mrs':
                            $("#femalePUF").prop('checked', true);
                            break;
                        case 'M/S':
                            $("#companyPUF").prop('checked', true);
                            break;
                        default:
                    }
                    if (sa[1] != '') {
                        $("#firstnamePUF").val(sa[1]);
                        $("#firstnamePUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    if (sa[2] != '') {
                        $("#lastnamePUF").val(sa[2]);
                        $("#lastnamePUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    if (sa[3] != '') {
                        $("#mobilenoPUF").val(sa[3]);
                        $("#mobilenoPUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    if (sa[4] != '') {
                        $("#emailidPUF").val(sa[4]);
                        $("#emailidPUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    if (sa[5] != '') {
                        $("#addressPUF").val(sa[5]);
                        $("#addressPUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    if (sa[6] != '') {
                        $("#pincodePUF").val(sa[6]);
                        $("#pincodePUF").closest(".form-group,.input-group").removeClass('is-empty');
                    }
                    var states = JSON.parse(sa[9]);
                    cities = JSON.parse(sa[10]);
                    if (sa[8] != '')
                        cityname = sa[8];
                    if (states.length > 0) {
                        $.each(states, function (i, v) {
                            $("#statePUF").append($("<option>", { value: v.Id, text: v.Name }));
                        });
                        if (sa[7] != '' && sa[7] != '0')
                            $("#statePUF").val(sa[7]).trigger('change');
                        $('.selectpicker').selectpicker('refresh');
                    }

                    $("#formLoadingProfile").fadeOut('fast', function (e) {
                        $("#frmmyprofile").fadeIn()
                    });
                } catch (e) {
                    alertException(e, current_page_id, 'UpdateProfile.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
        });
        $('form#frmmyprofile').validate({
            rules: {
                genderPUF: { required: true },
                firstnamePUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_]+$/,
                },
                lastnamePUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_]+$/,
                },
                mobilenoPUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9]+$/,
                },
                emailidPUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    //regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    regex: /^[A-Za-z0-9\-\_\.\@]+$/,
                },
                addressPUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\-\,\.\/\(\)\s]+$/,
                },
                pincodePUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9]+$/,
                    rangelength: [6, 6],
                },
                statePUF: {
                    required: true,
                },
                cityPUF: {
                    required: true,
                },
            },
            messages: {
                genderPUF: { required: "Select Title" },
                firstnamePUF: {
                    required: "Please enter First name",
                    regex: "Only alphabets, numbers and underscore is allowed",
                },
                lastnamePUF: {
                    required: "Please enter Last name",
                    regex: "Only alphabets, numbers and underscore is allowed",
                },
                mobilenoPUF: {
                    required: "Please enter Mobile number",
                    regex: "Only numbers are allowed",
                },
                emailidPUF: {
                    required: "Please enter Email Id",
                    regex: "Enter a valid Email Id",
                },
                addressPUF: {
                    required: "Please enter Address",
                    regex: "Enter a valid Address",
                },
                pincodePUF: {
                    required: "Please enter Pin/Zip code",
                    regex: "Enter a valid Pin/Zip code",
                },
                statePUF: {
                    required: "Please select a State",
                },
                cityPUF: {
                    required: "Please select a City",
                },
            }
        });
    } catch (e) {
        alertException(e, current_page_id, 'UpdateProfile.validate');
    }
    $("#statePUF").on('change', function (e) {
        try {
            var val = $(this).val();
            var cityId = '';
            if (cities.length > 0) {
                $("#cityPUF").empty();
                var filled = false;
                $.each(cities, function (i, v) {
                    if (parseInt(val) == parseInt(v.StateId)) {
                        $("#cityPUF").append($("<option>", { value: v.Id, text: v.Name }));
                        if (trimString(cityname) == trimString(v.Name)) {
                            cityId = v.Id.toString();
                        }
                        filled = true;
                    }
                });
                if (cityId != '') {
                    //console.log(cityId);
                    $("#cityPUF").val('19').trigger('change');
                    $("#cityPUF option").each(function () {
                        if ($(this).val() == cityId)
                            $(this).attr("selected", "selected");
                    });
                }
                $('.selectpicker').selectpicker('refresh');
            }
        } catch (e) {
            alertException(e, current_page_id, 'statePUF.change');
        }
    });
    $('form#frmmyprofile').on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var paramList = {};
                paramList.s = $('input[name=genderPUF]:checked').val() + "|" + $('#firstnamePUF').val().trim() + "|" + $('#lastnamePUF').val().trim() + "|" + $('#mobilenoPUF').val().trim() + "|" + $('#emailidPUF').val().trim() + "|" + $('#addressPUF').val().trim() + "|" + $('#pincodePUF').val().trim() + "|" + $('#statePUF :selected').val().trim() + "|" + $('#cityPUF :selected').val().trim() + "|" + $('#cityPUF :selected').text().trim();
                paramList.s = StringEncryption(paramList.s);
                $.ajax({
                    type: 'POST',
                    url: apiPath + "/Customer/UpdateProfile",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(paramList),
                    dataType: 'json',
                    jsonp: null,
                    beforeSend: function (xhr, opts) {
                        $("button.submit-button").prop('disabled', true);
                        $("button.submit-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;updating...');
                    },
                    success: function (data, textStatus, xhr) {
                        try {
                            alertme('Profile updated successfully.', 'success');
                        } catch (e) {
                            alertException(e, current_page_id, 'UpdateProfile.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                    complete: function (a) {
                        $("button.submit-button").prop('disabled', false);
                        $("button.submit-button span").html('Update Profile');
                    }
                });
            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, 'form#frmmyprofile.submit');
        }

    });
});

