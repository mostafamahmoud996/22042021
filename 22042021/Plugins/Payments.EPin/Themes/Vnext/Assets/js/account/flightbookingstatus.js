﻿current_page_id = "#flightbookingstatus";
var actionId = 0, txnid = '', status = '';

$(document).ready(function (e) {
    try {
        if (localStorage.getItem("flighttransactionid") != null)
            txnid = localStorage.getItem("flighttransactionid");
        else
            window.location = virtualPath + "Account/FlightBooking";

        if (localStorage.getItem("flightbookingstatus") != null)
            status = localStorage.getItem("flightbookingstatus");

        if (status == 'success') {
            $(".div-icon").html(tickanimation);
            $("#transaction_info").html('<p>Your ' + localStorage.getItem("FromCode") + ' - ' + localStorage.getItem("ToCode") + ' ' + (localStorage.getItem("roundtrip") == 1 ? 'round-trip' : 'one-way') + ' flight is confirmed.</p><p> Your Trip Id is:<br /> <b>' + txnid + '</b>.<br /><br />Please use this Trip Id for any communication with us.');
        }
        else {
            $(".div-icon").html('<i class="fa fa-exclamation-circle fa-4x text-warning"></i>');
            $("#transaction_info").html(localStorage.getItem("errormessage"));
        }

        localStorage.removeItem("domestic");
        localStorage.removeItem("FromCode");
        localStorage.removeItem("FromAirport");
        localStorage.removeItem("ToCode");
        localStorage.removeItem("ToAirport");
        localStorage.removeItem("roundtrip");
        localStorage.removeItem("DepartureDate");
        localStorage.removeItem("DepartureDateLabel");
        localStorage.removeItem("ArrivalDate");
        localStorage.removeItem("ArrivalDateLabel");
        localStorage.removeItem("travellers");
        localStorage.removeItem("totaltravellers");
        localStorage.removeItem("cabinclass");

        localStorage.removeItem("onwardOptions");
        localStorage.removeItem("returnOptions");
        localStorage.removeItem("onwardflight");
        localStorage.removeItem("returnflight");
        localStorage.removeItem("flighttotalfare");

        localStorage.removeItem("flighttransactionid");
        localStorage.removeItem("flightbookingstatus");
        localStorage.removeItem("errormessage");
    } catch (e) {
        alertException(e, current_page_id, 'pageLoad');
    }

    $(document).on('click', 'button.submit-button', function (e) {
        try {
            e.preventDefault();
            window.location = virtualPath + "Account/FlightBooking";
        } catch (e) {
            alertException(e, current_page_id, 'button.submit-button.click');
        }
    });

    $("a.goto-flight-booking").on('click', function (e) {
        window.location = virtualPath + "Account/FlightBooking";
    });
});

