﻿current_page_id = "#referandearn";
var sharetext = "";

try {
    alertme("Something went wrong! Report if it happens again.");
    var ewp = {};
    $.ajax({
        type: 'POST',
        timeout: 6000,   //timeout to 6s
        url: apiPath + "/AppReferralOffer",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ewp),
        dataType: 'json',
        jsonp: null,
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                //console.log(r);
                var arr = [];
                arr = r.split("|");
                var flag = parseInt(arr[0]);
                var noOfTimes = parseInt(arr[1]);
                var usedCount = parseInt(arr[2]);
                var yourAmount = arr[3];
                var friendAmount = arr[4];
                var referralCode = arr[5];
                var description = arr[6];
                var expirydate = "";
                var now = new Date();
                var currDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                //console.log('currDate - ' + currDate);

                if (arr[7] != "")
                    expirydate = new Date(arr[7].split('-')[2], arr[7].split('-')[1] - 1, arr[7].split('-')[0]);
                else
                    expirydate = currDate;

                var expirydays = 0;
                if (arr.length >= 9)
                    expirydays = parseInt(arr[8] == null ? 0 : arr[8]);

                //alert('expirydate - ' + expirydate + '  , currDate - ' + currDate + '  , flag - ' + flag);

                if (flag == 1 && expirydate >= currDate) {
                    $("#formErrorRAE").show();
                    $("#frmreferandearn").hide();
                    if (referralCode != '') {
                        $("#formErrorRAE").hide();
                        $("#frmreferandearn").show();
                        $("#referral_code_anchor").html(referralCode);
                        var YOUR_AMOUNT = yourAmount;
                        var FRIEND_AMOUNT = friendAmount;
                        $(".your-amount").html(YOUR_AMOUNT);
                        $(".friend-amount").html(FRIEND_AMOUNT);
                        sharetext = description;
                        if (expirydays != null)
                            $(".expiry-days").html(expirydays);
                    }
                }
            } catch (e) {
                alertException(e, current_page_id, 'AppReferralOffer.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
    });
} catch (e) {
    alertException(e, current_page_id, 'Page.load');
}