﻿current_page_id = "#makepayment";
var providerId = 0, actionId = 0, operatorId = 0, actionName = '', walletBalance = 0, prefix = '', txnname = '', oprname = '', circname = '', taxes = '', rechargeAmount = 0, taxAmount = 0, totalAmount = 0;
var profileflagMP = false, refW = null;
cashback_list_json = [];

function RechargePrerequisities() {
    try {
        var cmp = {};
        cmp.s = "";
        $.ajax({
            type: 'POST',
            url: "api/RechargePrerequisities",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(cmp),
            dataType: 'json',
            jsonp: null,
            success: function (data, textStatus, xhr) {
                try {
                    var r = StringDecryption(data);
                    //console.log(r);
                    arr = [];
                    arr = r.split("|");

                    if (arr[0] != '') {
                        dedhead_list_json = JSON.parse(arr[0]);
                    }

                    if (arr[1] != '') {
                        deduction_list_json = JSON.parse(arr[1]);
                    }

                    //console.log('dedhead_list_json---------------------------------------------------------');
                    //console.log(JSON.stringify(dedhead_list_json));
                    //console.log('deduction_list_json---------------------------------------------------------');
                    //console.log(JSON.stringify(deduction_list_json));

                    var taxList = $.grep(deduction_list_json, function (e) { return e.TypeId == actionId && e.OperatorId == operatorId && e.Value > 0; });
                    if (taxList.length > 0) {
                        var taxdet = '', taxamt = 0, taxtooltip = '';
                        $.each(taxList, function (i, v) {
                            taxdet = '', tax = 0, taxamt = 0;
                            //console.log(v.Value);
                            if (v.IsAmount) {
                                taxamt = parseFloat(v.Value);
                            }
                            else {
                                taxamt = parseFloat(rechargeAmount) * parseFloat(v.Value) / 100;
                            }

                            taxamt = parseFloat(taxamt.toFixed(2));
                            taxAmount += taxamt;
                            taxdet = v.Id + "~" + v.ChargeId + "~" + v.Charge + "~" + v.IsAmount + "~" + v.IsAmountName + "~" + v.TypeId + "~" + v.OperatorType + "~" + v.OperatorId + "~" + v.Operator + "~" + v.Value;

                            if (taxes == '') {
                                taxes = taxdet;
                            }
                            else {
                                taxes = taxes + "#" + taxdet;
                            }
                            taxtooltip += (taxtooltip == '' ? "" : "<br />") + v.Charge + (v.IsAmount ? "" : " (" + v.Value + "%)") + ": Rs." + taxamt + '/-';
                        });

                        //$("a.tax-tooltip").prop('title', taxtooltip);
                        //$("a.tax-tooltip").removeProp('data-original-title');
                        //$('[data-toggle="tooltip"]').tooltip({
                        //    title: function () {
                        //        return $(this).prop('title');
                        //    }
                        //});

                        $("a.tax-tooltip").html(taxtooltip);

                        $("#divrechargeamountMP").show();
                        $("#hdnrechargeamountMP").val(rechargeAmount);
                        $("#rechargeamountMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + rechargeAmount.toString());

                        $("#divtaxamountMP").show();
                        $("#hdntaxamountMP").val(taxAmount);
                        $("#taxamountMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + taxAmount.toString());

                        totalAmount = rechargeAmount + taxAmount;
                        totalAmount = parseFloat(totalAmount.toFixed(2));

                        $("#hdnamountMP").val(totalAmount);
                        $("#amountMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + totalAmount.toString());
                    }
                    GetWalletBalance();
                } catch (e) {
                    alertException(e, 'app.init', 'RechargePrerequisities.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            }
        });
    } catch (e) {
        //alertException(e, 'app.init', 'RechargePrerequisities');
        LogException(init_current_page_id + ' => ' + e.message);
    }
}

function BalanceAmountToPay() {
    var amt = parseFloat($("#hdnamountMP").val());
    var ewbal = parseFloat($("#hdnwalletbalanceMP").val());
    var bal = 0;

    if ($("#usewalletMP").prop('checked')) {
        bal = (ewbal >= amt) ? 0 : (amt - ewbal);
    }
    else {
        bal = amt;
    }
    $("#balanceamtMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + bal);
    $("#hdnbalanceamtMP").val(bal);
    $("button.make-payment span").html('Recharge/Pay Now');
    if (bal < 0) {
        alertme('Amount to Pay cannot be less than 0 (zero)');
        $("button.make-payment").prop('disabled', true);
    }
    if (bal > 0) {
        $("#divbalamtMP").slideDown('fast');
        $("button.make-payment").prop('disabled', false);
        $("button.make-payment span").html('Proceed for Payment');
    }
    else {
        $("#divbalamtMP").slideUp('fast');
    }
}

function ParameterString() {
    var p = "";

    p = prefix;

    p += "|" + $("#hdnactionMP").val();

    p += "|" + $("#hdnmobilenumberMP").val();

    p += "|" + $("#hdnoperatorMP").val();

    if (parseFloat($("#hdnbalanceamtMP").val()) == 0)
        p += "|" + $("#hdnamountMP").val();
    else
        p += "|" + $("#hdnbalanceamtMP").val();

    p += "|" + $("#hdncircleMP").val();

    p += "|" + $("#hdnunitMP").val();

    p += "|" + $("#hdndobMP").val();

    p += "|" + $("#hdncycleMP").val();

    if (localStorage.getItem("sdn") != null) {
        p += "|" + localStorage.getItem("sdn").toString();
    }
    else {
        p += "|";
    }

    if (localStorage.getItem("sds") != null) {
        p += "|" + localStorage.getItem("sds").toString();
    }
    else {
        p += "|";
    }

    p += "|" + $("#hdncityMP").val();

    p += "|" + $("#hdnaccnoMP").val();

    p += "|" + txnname;
    p += "|" + oprname;
    p += "|" + circname;

    p += "|" + rechargeAmount.toString();
    p += "|" + taxes;
    p += "|" + taxAmount.toString();
    p += "|" + $("#hdnamountMP").val();

    p += "|" + ($("#usewalletMP").prop('checked') == true ? 1 : 0);

    p += "|" + $("#cbCodesMP").val();
    p += "|" + $("#cbFaresMP").val();
    p += "|" + $("#cbAmountsMP").val();

    p += "|" + $("#hdnproviderIdMP").val();
    p += "|" + $("#hdnTransactionIdMP").val();
    p += "|" + $("#hdnAddrMP").val();
    p += "|" + $("#hdnMonthMP").val();
    p += "|" + $("#hdnYearMP").val();
    p += "|" + $("#hdnMobNoMP").val();

    return p;
}

function RechargeCompletion(status, txnid) {
    localStorage.setItem("rechargestatus", status);
    localStorage.setItem("transactionid", txnid);
    window.location = virtualPath + "Account/RechargeStatus";
}


function PostWalletBalance(r) {
    //notification(r);
    var ewbal = parseFloat(r);
    $("#hdnwalletbalanceMP").val(ewbal);
    $("#walletbalanceMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + $("#hdnwalletbalanceMP").val());

    if (ewbal > 0) {
        $("#usewalletMP").prop('checked', true);
        $("#usewalletMP").prop("disabled", false);
    }
    else {
        $("#usewalletMP").prop('checked', false);
        $("#usewalletMP").prop("disabled", true);
    }

    BalanceAmountToPay();

    if (pgresponse == '') {
        $("#formLoadingMP").fadeOut('fast', function (e) {
            $("#formMP").fadeIn();
        });
    }
}

function RevertWalletDeduction(uid) {
    var btntext = $("button.make-payment span").html();
    var xyz = {};
    xyz.s = StringEncryption(uid);
    $.ajax({
        type: 'POST',
        url: apiPath + "/Customer/WalletDeductionReversal",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(xyz),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                PostWalletBalance(r);
            } catch (e) {
                alertException(e, current_page_id, 'RevertWalletDeduction.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

function RechargeMakePayment() {
    $("#formErrorMP").fadeOut('fast', function (e) {
        $("#formLoadingMP").fadeIn();
    });
    var btntext = $("button.make-payment span").html();
    var ewp = {};
    ewp.s = actionId;
    //console.log(ewp.s);
    ewp.s = StringEncryption(ewp.s);
    $.ajax({
        type: 'POST',
        url: "api/Customer/RechargeMakePayment",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ewp),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data).split('|');
                //console.log(r);
                PostWalletBalance(r[0]);
                cashback_list_json = JSON.parse(r[1]);
                //console.log(cashback_list_json);
                var flg = false;
                var cashback = $.grep(cashback_list_json, function (e) { return e.OperatorId == operatorId; });
                if (cashback.length > 0) {
                    flg = true;
                    //console.log('operator-wise---------------------');
                }
                else {
                    cashback = null;
                    cashback = $.grep(cashback_list_json, function (e) { return e.OperatorId == null; });
                    if (cashback.length > 0) {
                        flg = true;
                        //console.log('transaction-wise--------------------');
                    }
                }
                if (flg) {
                    var cashbackAmt = parseFloat(localStorage.getItem("a")) * parseFloat(cashback[0].Percentage) / 100;
                    if (parseFloat(cashback[0].MaxAmount) > 0 && cashbackAmt > parseFloat(cashback[0].MaxAmount))
                        cashbackAmt = parseFloat(cashback[0].MaxAmount);
                    cashbackAmt = Math.floor(cashbackAmt);
                    if (cashbackAmt > 0) {
                        $("#cashbackCodeMP").html(cashback[0].Code);
                        $("#cbCodesMP").val(cashback[0].Code);
                        $("#cbFaresMP").val(localStorage.getItem("a"));
                        $("#cbAmountsMP").val(cashbackAmt);
                        $("#cbTotalAmtMP").val(cashbackAmt);
                        $("#cashbackMP span").html('<i class="fa fa-rupee"></i> ' + cashbackAmt.toLocaleString('en-IN'));
                        $("#divcashbackMP").show();
                    }
                }
            } catch (e) {
                alertException(e, current_page_id, 'RechargeMakePayment.success');
                $("#errorMessageMP").html(e.message);
                $("#formLoadingMP").fadeOut('fast', function (e) {
                    $("#formErrorMP").fadeIn();
                });
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}


$(document).ready(function (e) {
    try {
        if (pgresponse != "") {
            var req = [], req_status = '', req_txnid = '', error_message = '';
            //console.log(pgresponse);
            //pgresponse = StringDecryption(pgresponse);
            if (pgresponse.indexOf('|') != -1)
                req = pgresponse.split('|');
            else {
                bootalert('Oops! Unexpected error occurred. Please report it to us.', 'error');
            }

            req_status = req[0];
            if (req.length > 1)
                req_txnid = req[1];
            if (req.length > 2)
                error_message = req[2];

            pgresponse = null;

            if (req_status != '' && req_txnid != '') {
                if (req_status == 'success') {
                    var btntextR = $("button.make-payment span").html();
                    var abc = {};
                    abc.s = req_txnid;
                    //console.log(ewp.s);
                    abc.s = StringEncryption(abc.s);
                    $.ajax({
                        type: 'POST',
                        url: "api/Customer/GatewayRecharge",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(abc),
                        dataType: 'json',
                        jsonp: null,
                        beforeSend: function (xhr, opts) {
                            $("button.make-payment").prop('disabled', true);
                            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                        },
                        success: function (data, textStatus, xhr) {
                            try {
                                var r = StringDecryption(data);
                                var arr = [];
                                arr = r.split("|");
                                if (arr[0] == 'success' || arr[0] == 'pending') {
                                    RechargeCompletion(arr[0], arr[1]);
                                }
                            } catch (e) {
                                alertException(e, current_page_id, 'GatewayRecharge.success');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            //OnAjaxError(xhr, textStatus, errorThrown);
                            //alertException(e, current_page_id, 'Customer/GatewayRecharge.error');
                            $("#errorMessageMP").html(errorThrown);
                            $("#formLoadingMP").fadeOut('fast', function (e) {
                                $("#formErrorMP").fadeIn();
                            });
                        },
                        complete: function (a) {
                            $("button.make-payment").prop('disabled', false);
                            $("button.make-payment span").html(btntextR);
                        }
                    });
                }
                else {
                    RevertWalletDeduction(req_txnid);
                    $("#errorMessageMP").html(error_message);
                    $("#formLoadingMP").fadeOut('fast', function (e) {
                        $("#formErrorMP").fadeIn();
                    });
                }
            }
        }
        else {
            RechargePrerequisities();
            if (localStorage.getItem("i") != null) {
                actionId = parseInt(localStorage.getItem("i").toString());
                providerId = parseInt(localStorage.getItem("pid").toString());
                $("#hdnproviderIdMP").val(providerId);

                if (localStorage.getItem("pp") != null) {
                    if (localStorage.getItem("pp").toString() == 'pr') {
                        actionId = 1;
                    }
                    if (localStorage.getItem("pp").toString() == 'po') {
                        actionId = 2;
                    }
                }
                var txnNameList = $.grep(type_list_json, function (e) { return e.Id == actionId; });
                if (txnNameList.length > 0) {
                    txnname = txnNameList[0].Name;
                }

                switch (actionId) {
                    case 1:
                        prefix = "MPR";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Mobile number");
                        break;

                    case 2:
                        prefix = "MPO";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Mobile number");
                        break;

                    case 3:
                        prefix = "DTH";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Subscriber ID");
                        break;

                    case 4:
                        prefix = "DCR";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Datacard Number");
                        break;

                    case 5:
                        prefix = "ELE";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("CA Number");
                        break;

                    case 6:
                        prefix = "GAS";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Account Number");
                        break;

                    case 7:
                        prefix = "INS";
                        $("h5.payment-card-title").html(txnname);
                        $("label#mobilenumberMPLabel").html("Policy Number");
                        break;

                    default:
                        break;
                }
                $("#hdnactionMP").val(actionId);

                if (localStorage.getItem("m") != null) {
                    $("#hdnmobilenumberMP").val(localStorage.getItem("m").toString());
                    $("#mobilenumberMP span").html($("#hdnmobilenumberMP").val());
                }

                if (localStorage.getItem("o") != null) {
                    $("#hdnoperatorMP").val(localStorage.getItem("o").toString());
                    operatorId = parseInt($("#hdnoperatorMP").val());
                }

                if (localStorage.getItem("a") != null) {
                    $("#hdnamountMP").val(localStorage.getItem("a").toString());
                    rechargeAmount = parseFloat($("#hdnamountMP").val());
                    totalAmount = rechargeAmount;
                    $("#amountMP span").html('<i class="fa fa-rupee"></i>&nbsp;' + $("#hdnamountMP").val());
                }

                if (localStorage.getItem("c") != null) {
                    $("#hdncircleMP").val(localStorage.getItem("c").toString());
                }

                if (localStorage.getItem("onm") != null && localStorage.getItem("cnm") != null) {
                    oprname = localStorage.getItem("onm").toString();
                    circname = localStorage.getItem("cnm").toString();
                    $("#operatorMP span").html(localStorage.getItem("onm").toString() + ' - ' + localStorage.getItem("cnm").toString());
                }
                else if (localStorage.getItem("onm") != null && localStorage.getItem("cnm") == null) {
                    $("#operatorMP span").html(localStorage.getItem("onm").toString());
                }

                if (localStorage.getItem("u") != null) {
                    $("#hdnunitMP").val(localStorage.getItem("u").toString());
                    $("#unitMP span").html($("#hdnunitMP").val());
                    $("#divunitMP").show();
                }

                if (localStorage.getItem("d") != null) {
                    $("#hdndobMP").val(localStorage.getItem("d").toString());
                    $("#dobMP span").html($("#hdndobMP").val());
                    $("#divdobMP").show();
                }

                if (localStorage.getItem("cy") != null) {
                    $("#hdncycleMP").val(localStorage.getItem("cy").toString());
                    $("#cycleMP span").html($("#hdncycleMP").val());
                    $("#divcycleMP").show();
                }

                if (localStorage.getItem("sdn") != null) {
                    $("#hdnsubdivMP").val(localStorage.getItem("sdn").toString());
                    $("#subdivMP span").html($("#hdnsubdivMP").val());
                    $("#divsubdivMP").show();
                }

                if (localStorage.getItem("sds") != null) {
                    $("#hdnsubdivMP").val(localStorage.getItem("sds").toString());
                    $("#subdivMP span").html($("#hdnsubdivMP").val());
                    $("#divsubdivMP").show();
                }

                if (localStorage.getItem("ci") != null) {
                    $("#hdncityMP").val(localStorage.getItem("ci").toString());
                    $("#cityMP span").html($("#hdncityMP").val());
                    $("#divcityMP").show();
                }

                if (localStorage.getItem("acc") != null) {
                    $("#hdnaccnoMP").val(localStorage.getItem("acc").toString());
                    $("#accnoMP span").html($("#hdnaccnoMP").val());
                    $("#divaccnoMP").show();
                }

                if (localStorage.getItem("tid") != null) {
                    $("#hdnTransactionIdMP").val(localStorage.getItem("tid").toString());
                    $("#TransactionIdMP span").html($("#hdnTransactionIdMP").val());
                    $("#divTransactionIdMP").show();
                }

                if (localStorage.getItem("addr") != null) {
                    $("#hdnAddrMP").val(localStorage.getItem("addr").toString());
                    $("#AddrMP span").html($("#hdnAddrMP").val());
                    $("#divAddrMP").show();
                }

                if (localStorage.getItem("month") != null) {
                    $("#hdnMonthMP").val(localStorage.getItem("month").toString());
                    $("#MonthMP span").html($("#hdnMonthMP").val());
                    $("#divMonthMP").show();
                }

                if (localStorage.getItem("year") != null) {
                    $("#hdnYearMP").val(localStorage.getItem("year").toString());
                    $("#YearMP span").html($("#hdnYearMP").val());
                    $("#divYearMP").show();
                }

                if (localStorage.getItem("mobno") != null) {
                    $("#hdnMobNoMP").val(localStorage.getItem("mobno").toString());
                    $("#MobNoMP span").html($("#hdnMobNoMP").val());
                    $("#divMobNoMP").show();
                }



                $('form#formMP').validate({
                    rules: {
                        hdnactionMP: { required: true },
                        hdnmobilenumberMP: { required: true },
                        hdnoperatorMP: { required: true },
                        hdnamountMP: { required: true },
                        hdncircleMP: {
                            required: {
                                depends: function (e) {
                                    return (providerId == 1 && (actionId == 1 || actionId == 2 || actionId == 4));
                                }
                            }
                        },
                        hdnunitMP: {
                            required: {
                                depends: function (e) {
                                    return actionId == 5 && (parseInt($("#hdnoperatorMP").val()) == 72 || parseInt($("#hdnoperatorMP").val()) == 83);
                                }
                            }
                        },
                        hdncityMP: {
                            required: {
                                depends: function (e) {
                                    return actionId == 5 && (parseInt($("#hdnoperatorMP").val()) == 77 || parseInt($("#hdnoperatorMP").val()) == 88);
                                }
                            }
                        },
                        hdnsubdivMP: {
                            required: {
                                depends: function (e) {
                                    return (providerId == 1 && actionId == 5 && (parseInt($("#hdnoperatorMP").val()) == 52 || parseInt($("#hdnoperatorMP").val()) == 53));
                                }
                            }
                        },
                        hdncycleMP: {
                            required: {
                                depends: function (e) {
                                    return (actionId == 5 && (parseInt($("#hdnoperatorMP").val()) == 46 || parseInt($("#hdnoperatorMP").val()) == 59 || parseInt($("#hdnoperatorMP").val()) == 72));
                                }
                            }
                        },
                        hdnwalletbalanceMP: { required: true },
                        hdndobMP: {
                            required: {
                                depends: function (e) {
                                    return (actionId == 7);
                                }
                            }
                        },
                    },
                    messages: {
                        hdnactionMP: { required: "Action not received" },
                        hdnmobilenumberMP: { required: "ID not received" },
                        hdnoperatorMP: { required: "Provider not received" },
                        hdnamountMP: { required: "Amount not received" },
                        hdncircleMP: { required: "Circle not received" },
                        hdnunitMP: { required: "Unit not received" },
                        hdncityMP: { required: "City not received" },
                        hdnsubdivMP: { required: "Sub-division not received" },
                        hdncycleMP: { required: "Cycle not received" },
                        hdnwalletbalanceMP: { required: "Wallet balance not received" },
                        hdndobMP: { required: "Date of birth not received" },
                    }
                });

                if (LOGGED_IN_ID != null) {
                    if (LOGGED_IN_NAME == null) {
                        profileflagMP = true;
                    }
                    else {
                        if (LOGGED_IN_NAME == '') {
                            profileflagMP = true;
                        }
                        else {
                            if (LOGGED_IN_EMAIL == null) {
                                profileflagMP = true;
                            }
                            else {
                                if (LOGGED_IN_EMAIL == '') {
                                    profileflagMP = true;
                                }
                                else {
                                    if (LOGGED_IN_MOBILENO == null) {
                                        profileflagMP = true;
                                    }
                                    else {
                                        if (LOGGED_IN_MOBILENO == '') {
                                            profileflagMP = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (profileflagMP) {
                        bootalert(profile_update_message);
                    }
                }
            }
            else {
                $("#formLoadingMP").html().replace('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>', '<i class="fa fa-exclamation-circle fa-2x"></i>');
                $("#formLoadingMP span").html('Oops! Something went wrong. Go back and try again.');
                throw $("#formLoadingMP span").html();
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'PageLoad');
    }

    $("#usewalletMP").on('click', function (e) {
        try {
            BalanceAmountToPay();
        } catch (e) {
            alertException(e, current_page_id, 'usewalletMP.click');
        }
    });

    $("a.goto-recharge").on('click', function (e) {
        try {
            window.location = virtualPath + "Account/MobileRecharge?i=" + localStorage.getItem("enci");
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-recharge.click');
        }
    });

    $("a.retry-make-payment").on('click', function (e) {
        try {
            RechargeMakePayment();
        } catch (e) {
            alertException(e, current_page_id, 'a.retry-make-payment.click');
        }
    });

    $('form#formMP').on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var pmd = "";
                if ($("#usewalletMP").prop("checked")) {
                    pmd += "EW";
                }

                //if ($("#usegrplcashMP").prop("checked")) {
                //    pmd += (pmd == '' ? '' : ',') + "GC";
                //}

                if (parseFloat($("#hdnbalanceamtMP").val()) > 0) {
                    pmd += (pmd == '' ? '' : ',') + "CC";
                }

                if (profileflagMP) {
                    bootalert(profile_update_message);
                    return false;
                }

                var btntext = $("button.make-payment span").html();
                var paramList = {};
                paramList.s = pmd + "|" + ParameterString();
                var s = paramList.s;
                paramList.s = StringEncryption(paramList.s);
                $.ajax({
                    type: 'POST',
                    url: "api/Customer/InterimRecharge",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(paramList),
                    dataType: 'json',
                    jsonp: null,
                    beforeSend: function (xhr, opts) {
                        $("button.make-payment").prop('disabled', true);
                        $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                    },
                    success: function (data, textStatus, xhr) {
                        try {
                            var r = StringDecryption(data);
                            var arrR = [];
                            arrR = r.split("|");
                            var tempStatus = arrR[0];
                            var tempReqId = arrR[1];
                            if (tempStatus == 'success' || tempStatus == 'pending') {
                                RechargeCompletion(tempStatus, tempReqId);
                            }
                            else if (tempStatus == 'inprocess') {
                                $("button.make-payment span").html('Redirecting...');
                                s = 'RW' + "|" + tempReqId + "|" + LOGGED_IN_NAME + "|" + LOGGED_IN_EMAIL + "|" + LOGGED_IN_MOBILENO + "|" + LOGGED_IN_ID + "|" + s;
                                s = StringEncryption(s);
                                //webPath = "http://localhost:15536";
                                //var uW = webPath + '/Customer/PaymentInitiate?s=' + s + '&p=' + StringEncryption(virtualPath + 'Account/MakePayment');
                                var uW = virtualPath + 'Account/PaymentInitiate?s=' + s + '&p=' + StringEncryption(virtualPath + 'Account/MakePayment');
                                window.location = uW;
                            }
                            else if (tempStatus == 'failed') {
                                alertme(tempReqId);
                                $("button.make-payment").prop('disabled', false);
                                $("button.make-payment span").html(btntext);
                            }
                        } catch (e) {
                            //console.log(e);
                            alertException(e, current_page_id, 'InterimRecharge.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $("button.make-payment").prop('disabled', false);
                        $("button.make-payment span").html(btntext);
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                    complete: function (a) {
                    }
                });
                e.preventDefault();
            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, 'form#formMP.submit');
        }

    });

    $(document).on('click', '#formMP a.close-popup-over-list', function (e) {
        try {
            e.preventDefault();
            $('#formMP .popup-over-list').removeClass('slideInUp');
            $('#formMP .popup-over-list').addClass('slideOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                $('#formMP .popup-over-list').hide();
            });
        } catch (e) {
            alertException(e, current_page_id, '#formMP a.close-popup-over-list.click');
        }
    });
});





//function openInAppBrowser(url, txnid) {
//    ClearAllIntervals();
//    var reqstatus = null;
//    var reqstatusInterval = null;
//    var itvrl = 10000;
//    notification('Please DO NOT press "BACK BUTTON" during Payment Process.', function () {
//        var win = cordova.InAppBrowser.open(url, "_blank", "location=no,hardwareback=no,clearsessioncache=yes,clearcache=yes");
//        win.addEventListener("loadstop", function () {
//            win.executeScript({ code: "if(localStorage.getItem('prereqstatus') != null){localStorage.removeItem('reqstatus');localStorage.removeItem('prereqstatus')}" });
//            reqstatusInterval = setInterval(function () {
//                win.executeScript({ code: "localStorage.getItem('reqstatus')" }, function (values) {
//                    reqstatus = values[0];
//                    var rrr = StringDecryption(reqstatus);
//                    //notification('txnid - ' + txnid + '  ,  rrr - ' + rrr + '  ,  itvrl - ' + itvrl);
//                    if (reqstatus != null && reqstatus != '' && rrr.indexOf(txnid) != -1) {
//                        clearInterval(reqstatusInterval);
//                        reqstatusInterval = null;
//                        win.close();
//                    }
//                });
//                itvrl = 5000;
//            }, itvrl);
//        });

//        win.addEventListener('exit', function () {
//            var req = [], req_status = '', req_txnid = '', error_message = '';
//            //console.log(reqstatus);
//            if (reqstatus != null && reqstatus != '') {
//                reqstatus = StringDecryption(reqstatus);
//                if (reqstatus.indexOf('|') != -1)
//                    req = reqstatus.split('|');
//                else {
//                    bootalert('Oops! Unexpected error occurred. Please report it to us.', 'error');
//                }

//                req_status = req[0];
//                if (req.length > 1)
//                    req_txnid = req[1];
//                if (req.length > 2)
//                    error_message = req[2];

//                reqstatus = null;

//                if (req_status != '' && req_txnid != '') {
//                    if (req_status == 'success') {
//                        var btntextR = $("button.make-payment span").html();
//                        var abc = {};
//                        abc.s = req_txnid;
//                        //console.log(ewp.s);
//                        abc.s = StringEncryption(abc.s);
//                        $.ajax({
//                            type: 'POST',
//                            url: apiPath + "/Customer/GatewayRecharge",
//                            contentType: "application/json; charset=utf-8",
//                            data: JSON.stringify(abc),
//                            dataType: 'json',
//                            jsonp: null,
//                            beforeSend: function (xhr, opts) {
//                                $("button.make-payment").prop('disabled', true);
//                                $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
//                            },
//                            success: function (data, textStatus, xhr) {
//                                try {
//                                        var r = StringDecryption(data);
//                                        var arr = [];
//                                        arr = r.split("|");
//                                        if (arr[0] == 'success' || arr[0] == 'pending') {
//                                            RechargeCompletion(arr[0], arr[1]);
//                                        }
//                                } catch (e) {
//                                    alertException(e, current_page_id, 'GatewayRecharge.success');
//                                }
//                            },
//                            error: function (xhr, textStatus, errorThrown) {
//                                OnAjaxError(xhr, textStatus, errorThrown);
//                            },
//                            complete: function (a) {
//                                $("button.make-payment").prop('disabled', false);
//                                $("button.make-payment span").html(btntextR);
//                            }
//                        });
//                    }
//                    else {
//                        bootalert(error_message, 'error', function () {
//                            //GetWalletBalance();
//                            RevertWalletDeduction(req_txnid);
//                        });
//                    }
//                }
//            }


//        });
//    });
//}





function GetWalletBalance() {
    var btntext = $("button.make-payment span").html();
    var ewp = {};
    ewp.s = LOGGED_IN_ID;
    ewp.s = StringEncryption(ewp.s);
    $.ajax({
        type: 'POST',
        url: "api/Customer/GetWalletBalance",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ewp),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data).split('|');
                PostWalletBalance(r[0]);
            } catch (e) {
                alertException(e, current_page_id, 'GetWalletBalance.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

