﻿current_page_id = "#notifications";
var numrows = 10;

function GetNotificationList(i, s, e, o, b) {
    try {
        var btntext = $(b).html();
        var ewp = {};
        ewp.s = i + "|" + s + "|" + e;
        //console.log(ewp.s);
        ewp.s = StringEncryption(ewp.s);
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/NotificationList",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $(b).prop('disabled', true);
                $(b).html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;loading...');
            },
            success: function (data, textStatus, xhr) {
                try {
                    data = JSON.parse(StringDecryption(data));
                    if (data.length > 0) {
                        //console.log(o);
                        var dt = '', dto = '', dtc = '';
                        if (data.length < e)
                            $(b).hide();
                        $.each(data, function (i, v) {
                            //var $li = $("<li>", { 'class': 'notification-record row padding-5 no-margin display-block', html: '<div class="text-left text-md ' + (v.IsViewed == 0 ? 'bold' : '') + '">' + v.Subject + '</div><div class="text-left text-sm">' + v.Body + '</div><div class="text-left text-sm">' + ConvertDateFromJson(v.CreatedDate, 'date') + '</div>' });
                            var $li = $("<li>", { 'class': 'notification-record row padding-5 no-margin display-block', html: '<div class="text-left text-md ' + (v.IsViewed == 0 ? 'bold' : '') + '">' + v.Body + '</div><div class="text-left text-sm">' + ConvertDateFromJson(v.CreatedDate, 'date') + '</div>' });
                            $(o).append($li);
                        });
                    }
                    else {
                        var $li = $("<li>", { 'class': 'no-margin no-bg no-shadow' });
                        var $dv = $("<div>", { html: '<p>&nbsp;</p><img src="' + virtualPath + 'Plugins/Nop.Plugin.Api/img/icons/empty-state.jpg" class="max-width-25p center-block" /><h6 class="center-block text-none">No notifications available</h6>' });
                        $li.append($dv);
                        $(o).append($li);
                        $(b).hide();
                    }
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $(b).prop('disabled', false);
                $(b).html(btntext);
            }
        });
    } catch (e) {
        alertException(e, current_page_id, '');
    }
}

$(document).ready(function (e) {
    GetNotificationList('0', 0, numrows, 'ul.all-notifications', 'button.load-more-all');
    $('button.load-more-all').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.all-notifications li.notification-record").length;
            GetNotificationList('0', s, numrows, 'ul.all-notifications', 'button.load-more-all');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

});