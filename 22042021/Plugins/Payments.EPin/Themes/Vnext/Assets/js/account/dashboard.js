﻿current_page_id = "#dashboard";
var page_show = false, data_loaded = false;

function InitializeSwiper() {
    try {
        //console.log('page_show - ' + page_show + '  ,  data_loaded - ' + data_loaded);
        if (page_show && data_loaded) {
            var dashboardSwiper = new Swiper('#dashboard_swiper', swiper_settings);
            $(".swiper-navigation-button").fadeIn();
        }
    } catch (e) {
        alertException(e, current_page_id, 'InitializeSwiper');
    }
}

$(document).ready(function (e) {
    try {

        page_show = true;
        InitializeSwiper();

        var dbb = {};
        dbb.s = "";
        //console.log(dbb.s);
        dbb.s = StringEncryption(dbb.s);
        //$.ajax({
        //    type: 'POST',
        //    url: apiPath + "/DashboardBanners",
        //    contentType: "application/json; charset=utf-8",
        //    data: JSON.stringify(dbb),
        //    dataType: 'json',
        //    jsonp: null,
        //    success: function (data, textStatus, xhr) {
        //        try {
        //            var r = StringDecryption(data);
        //            //console.log(r);
        //            if (r != 'null') {
        //                var data = StringDecryption(data);
        //                //console.log(data);
        //                if (data.length > 0) {
        //                    var html = '';
        //                    for (var i = 0; i < data.length; i++) {
        //                        var imgpath = rechargePath + data[i].FilePath;
        //                        html += '<div class="swiper-slide"><a href="#" class="display-block slide-anchor" data-isurl="' + data[i].IsUrl + '" data-url="' + data[i].RedirectTo + '"><img src="' + imgpath + '" class="center-block width-100p" style="max-height:160px" /></a></div>';
        //                    }
        //                    $("#dashboard_swiper").find(".swiper-wrapper").empty().append(html);
        //                    $("#dashboard_swiper").find('.swiper-wrapper').attr('style', '');
        //                    //console.log(html);
        //                    data_loaded = true;
        //                    InitializeSwiper();
        //                }
        //            }
        //        } catch (e) {
        //            alertException(e, current_page_id, '');
        //        }
        //    },
        //    error: function (xhr, textStatus, errorThrown) {
        //        OnAjaxError(xhr, textStatus, errorThrown);
        //    },
        //});

        
    } catch (e) {
        alertException(e, current_page_id, 'Page.Load');
    }

    $(document).on("click", 'a.slide-anchor', function (e) {
        try {
            var isurl = $(this).data('isurl');
            var url = $(this).data('url');
            if (url.indexOf('?') != -1) {
                var arr = url.split('?');
                var qs = StringEncryption(arr[1].replace('i=', ''));
                url = arr[0] + '?i=' + qs;
            }
            if (isurl == 1)
                window.open(url, '_blank');
            else if (isurl == 2)
                window.location = url;
        } catch (e) {
            alertException(e, current_page_id, 'a.slide-anchor');
        }
    });
    
        
        
    
});