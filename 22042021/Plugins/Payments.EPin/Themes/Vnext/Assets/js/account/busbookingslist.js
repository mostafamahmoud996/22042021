﻿current_page_id = "#busbookingslist";
var numrows = 10;

function GetMFBTransactionList(i, s, e, o, b) {
    try {
        var btntext = $(b).html();
        var ewp = {};
        ewp.s = i + "|" + s + "|" + e;
        //console.log(ewp.s);
        ewp.s = StringEncryption(ewp.s);
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/BusBookingList",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $(b).prop('disabled', true);
                $(b).html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;loading...');
            },
            success: function (data, textStatus, xhr) {
                try {
                    data = JSON.parse(StringDecryption(data));
                    if (data.length > 0) {
                        //console.log(data);
                        var dt = '', dto = '', dtc = '';
                        if (data.length < e)
                            $(b).hide();
                        $.each(data, function (i, v) {
                            var status = (v.BookingStatus == 1 ? '<span class="text-success">&nbsp;Booked</span>' : (v.BookingStatus == 2 ? '<span class="text-warning">&nbsp;Pending</span>' : (v.BookingStatus == 3 ? '<span class="text-danger">&nbsp;Failed</span>' : (v.BookingStatus == 4 ? '<span class="text-danger">&nbsp;Cancelled</span>' : '&nbsp;Abandoned'))));
                            var onwardbus, returnbus, onwardbuscount, returnbuscount, onwarddepbus, onwardarrbus, returndepbus, returnarrbus, onwardtotalduration, returntotalduration
                            //console.log(v);
                            var originCity = v.Origin;
                            var destinationCity = v.Destination;
                            var totaltravellers = v.TotalTravellers;
                            var tripinfo = JSON.parse(v.OnwardBus);
                            var departureTime = ConvertStringToDateTime(tripinfo.departure_time, 'hhmmtt');
                            var arrivalTime = ConvertStringToDateTime(tripinfo.arrival_time, 'hhmmtt');
                            var duration = BusDuration(ConvertStringToDateTime(tripinfo.departure_time, ''), ConvertStringToDateTime(tripinfo.arrival_time, ''));
                            var totalFare = v.TxnAmount;

                            var $li = $("<li>", { 'class': 'bg-white card_content_shadow margin-bottom-5 no-border no-padding' });
                            var html = '';
                            html += '<div>';
                            html += '<div class="pull-left padding-5" style="width:75%;padding-bottom:0!important">';
                            html += '<div class="display-inline-block"><b class="text-lg">' + departureTime + '</b>';
                            html += '<small class="center-block text-gray">' + originCity + '</small>';
                            html += '</div>';
                            html += '<div class="display-inline-block"><img src="' + virtualPath + 'img/icons/bus-gray.png" style="max-width:21px;margin:0 10px" />';
                            html += '<small class="center-block text-gray">&nbsp;</small>';
                            html += '</div>';
                            html += '<div class="display-inline-block"><b class="text-lg">' + arrivalTime + '</b>';
                            html += '<small class="center-block text-gray">' + destinationCity + '</small>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="pull-right amount padding-5 bg-danger text-center text-bold text-lg" style="width:25%;background: #FFE95F;"><i class="fa fa-rupee"></i> ' + parseFloat(totalFare).toLocaleString('en-IN') + '</div>';
                            html += '<small class="center-block">' + duration + '</small>';
                            html += '<div class="clearfix"></div>';
                            html += '</div>';
                            html += '<div>';
                            html += '<div class="pull-left padding-left-5" style="width:75%">';
                            html += '<small class="bold">' + tripinfo.travels_name + '</small>';
                            html += '<small class="center-block text-left text-light-gray">' + tripinfo.bus_type + '</small>';
                            html += '</div>';
                            html += '<div class="pull-right center-block text-center" style="width:25%">';
                            html += '<b>' + totaltravellers + '</b><br />';
                            html += '<small class="text-light-gray">seats</small>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '</div>';
                            html += '<div>';
                            html += '<div class="pull-left padding-left-5" style="width:50%">';
                            html += status;
                            html += '</div>';
                            html += '<div class="pull-right padding-right-5 text-right" style="width:50%">';
                            if (v.BookingStatus == 1)
                                html += '<a href="#" class="text-sm view-ticket" data-bookingid="' + v.BookingTxnId + '" data-partnerreferenceid="' + v.UniqueId + '" data-origin="' + v.Origin + '" data-destination="' + v.Destination + '" data-boardingpoint="' + v.BusBoardingPoint + '">View Ticket</a>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '</div>';
                            html += '</div>';
                            var $dvd = $("<div>", { 'class': 'details width-100p no-padding', html: html });
                            $li.append($dvd);
                            $(o).append($li);
                        });
                    }
                    else {
                        var $li = $("<li>", { 'class': 'no-margin no-bg no-shadow' });
                        var $dv = $("<div>", { html: '<p>&nbsp;</p><img src="' + virtualPath + 'img/icons/empty_state.png" class="max-width-25p center-block" /><h6 class="center-block text-none">Book a bus to generate history</h6>' });
                        $li.append($dv);
                        $(o).append($li);
                        $(b).hide();
                    }
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $(b).prop('disabled', false);
                $(b).html(btntext);
            }
        });
    } catch (e) {
        alertException(e, current_page_id, 'GetMFBTransactionList');
    }
}

$(document).ready(function (e) {
    try {
        GetMFBTransactionList('0', 0, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        GetMFBTransactionList('1', 0, numrows, 'ul.approved-transactions', 'button.load-more-approved');
        GetMFBTransactionList('3', 0, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
        GetMFBTransactionList('4', 0, numrows, 'ul.cancelled-transactions', 'button.load-more-cancelled');
    } catch (e) {
        alertException(e, current_page_id, 'Page.load');
    }

    $('button.load-more-pending').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.pending-transactions li.transaction-record").length;
            GetMFBTransactionList('0', s, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-approved').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.approved-transactions li.transaction-record").length;
            GetMFBTransactionList('1', s, numrows, 'ul.approved-transactions', 'button.load-more-approved')
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-rejected').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.rejected-transactions li.transaction-record").length;
            GetMFBTransactionList('3', s, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-cancelled').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.cancelled-transactions li.transaction-record").length;
            GetMFBTransactionList('4', s, numrows, 'ul.cancelled-transactions', 'button.load-more-cancelled');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $(document).on('click', 'a.view-ticket', function (e) {
        try {
            var q = 'i=' + StringEncryption($(this).data('bookingid') + "|" + $(this).data('partnerreferenceid') + "|" + $(this).data('origin') + "|" + $(this).data('destination') + "|" + $(this).data('boardingpoint'));
            window.location = virtualPath + "Account/BusTicketView?" + q;
        } catch (e) {
            alertException(e, current_page_id, 'a.view-ticket.click');
        }
    });
    $('#close_bus_ticket_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_bus_ticket").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#close_bus_ticket_modal.click');
        }
    });
    $(document).on('click', 'a.cancel-ticket', function (e) {
        try {
            var _this = this;
            var confirmCallback = function (e) {
                if (e == 1) {
                    var v = $(_this).data('v').toString().replaceAll('\'', '"');
                    //console.log(v);
                    var onwardbus = $(_this).data('onwardbus').toString().replaceAll('\'', '"');
                    onwardbus = onwardbus.substring(1, onwardbus.length - 1);
                    onwardbus = JSON.parse(onwardbus.toString());
                    //console.log(onwardbus);
                    var bus = null;
                    if (parseInt($(_this).data('bustypeid')) == 1)
                        bus = onwardbus.bus[0];
                    else
                        bus = onwardbus.onward[0];
                    //console.log(bus.departureDateTime);
                    //console.log(onwardbus.fareId);
                    if (new Date() < ConvertStringToDateTime(bus.departureDateTime, '')) {
                        $("#modal_bus_ticket").modal('show');
                        $(".modal-backdrop").hide();

                        setTimeout(function () {
                            $("#bus_ticket_loading").fadeOut('fast', function (e) {
                                notification('Ticket against ' + onwardbus.fareId + ' is cancelled by you', null);
                                $("#bus_ticket_cancel_container").fadeIn();
                            });
                        }, 3000);
                        //var paramList = {};
                        //paramList.s = $(_this).data('id') + "|" + onwardbus.fareId + "|" + destination + "|" + departDate + "|" + returnDate + "|" + adult.toString() + "|" + child.toString() + "|" + infant.toString() + "|" + cabinClass + "|" + mode + "|" + airline;
                        ////console.log(paramList.s);
                        //paramList.s = StringEncryption(paramList.s);
                        //$.ajax({
                        //    type: 'POST',
                        //    url: apiPath + "/Customer/CancelBus",
                        //    contentType: "application/json; charset=utf-8",
                        //    data: JSON.stringify(paramList),
                        //    dataType: 'json',
                        //    jsonp: null,
                        //    success: function (data, textStatus, xhr) {
                        //        try {
                        //                var json = JSON.parse(StringDecryption(data)).response;
                        //                console.log(json);
                        //        } catch (e) {
                        //            //console.log(e);
                        //            alertException(e, current_page_id, 'SearchBuss.success');
                        //        }
                        //    },
                        //    error: function (xhr, textStatus, errorThrown) {
                        //        OnAjaxError(xhr, textStatus, errorThrown);
                        //    },
                        //});
                    }
                    else {
                        notification('Cannot cancel Ticket after bus departure time.', null);
                    }
                }
                else {
                    //DO NOTHING
                }
            }
            navigator.notification.confirm("Do you want to Cancel this ticket?", confirmCallback, alertTitle, ['Yes', 'No']);
        } catch (e) {
            alertException(e, current_page_id, 'a.cancel-ticket.click');
        }
    });
    $('#close_bus_ticket_cancel_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_bus_ticket_cancel").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#close_bus_ticket_cancel_modal.click');
        }
    });
});
