﻿current_page_id = "#forgotpassword";

$("body").one('pageshow', current_page_id, function (event, ui) {
    try {
        //$('#loginidFP').focus();
        if (localStorage.getItem("VF_" + appId + "_LOGGED_IN_LOGINID") != null) {
            $('#loginidFP').val(localStorage.getItem("VF_" + appId + "_LOGGED_IN_LOGINID"));
        }
    } catch (e) {
        alertException(e, current_page_id, '');
    }

});

try {
    $('form#frmforgotpassword').validate({
        rules: {
            loginidFP: {
                required: {
                    depends: function (e) {
                        $(this).val($(this).val().trim());
                        return true;
                    }
                },
                regex: /^[a-zA-Z0-9_@.]+$/,
            },
        },
        messages: {
            loginidFP: {
                required: "Enter an Username",
                regex: "Only alphabets, numbers and underscore is allowed",
            },
        }
    });    
} catch (e) {
    alertException(e, current_page_id, '');
}


$('form#frmforgotpassword').on('submit', function (e) {
    try {
        var isvalidate = $(this).valid();
        if (isvalidate) {
            var btntext = $("button.submit-button span").html();
            var paramList = {};
            paramList.s = StringEncryption($('#loginidFP').val().trim());
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/PasswordRecovery",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                beforeSend: function (xhr, opts) {
                    $("button.submit-button").prop('disabled', true);
                    $("button.submit-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;sending email...');
                },
                success: function (data, textStatus, xhr) {
                    try {
                        //console.log(data);
                        //console.log('--------------------');
                        if (data.e != '') {
                            alertme(data.e);
                        }
                        else {
                            var m = StringDecryption(data.r);
                            alertme(m, 'success');
                        }
                    } catch (e) {
                        alertException(e, current_page_id, '');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
                complete: function (a) {
                    $("button.submit-button").prop('disabled', false);
                    $("button.submit-button span").html(btntext);
                }
            });
        }
        e.preventDefault();
    } catch (e) {
        alertException(e, current_page_id, '');
    }

});

$("#signmeinFP").on("click", function (e) {
    $(":mobile-pagecontainer").pagecontainer("change", "signin.html", {role: "page"});
});

$("#signmeupFP").on("click", function (e) { 
    $(":mobile-pagecontainer").pagecontainer("change", "signup.html", {role: "page" });
});

$("#gobacktosteponeFP").on("click", function (e) {
    $("#frmforgotpassword").fadeOut('fast', function (e) {
        $("#frmforgotpasswordotp").fadeIn();
        $('#loginidFP').focus();
    });
});

$("#gplusloginFP").on("click", function (e) {
    try {
        window.plugins.googleplus.login(
            {},
            function (obj) {
                var gender, firstname, lastname, mobileno, emailid;
                gender = "";
                firstname = obj.givenName;
                lastname = obj.familyName;
                mobileno = "";
                emailid = obj.email;
                SocialSign(gender, firstname, lastname, mobileno, emailid, true);
            },
            function (msg) {
                alertme('Having trouble signin in using your Google Account.');
            }
        );
    } catch (e) {
        alertException(e, current_page_id, '');
    }
});

$("#facebookloginFP").on("click", function (e) {
    try {
        //alertme('Coming Soon...', 'info');
        var fbAPIResponse = function (response) {
            //$("#ss_response").html("fbAPI - \n\n" + JSON.stringify(response));
            var gender, firstname, lastname, mobileno, emailid;
            if (response.gender != null && response.gender != '')
                gender = response.gender == 'male' ? 'Mr' : 'Ms';
            firstname = response.first_name;
            lastname = response.last_name;
            mobileno = "";
            emailid = response.email;
            //notification('gender - ' + gender + '\n' + 'firstname - ' + firstname + '\n' + 'lastname - ' + lastname + '\n' + 'mobileno - ' + mobileno + '\n' + 'emailid - ' + emailid)
            SocialSign(gender, firstname, lastname, mobileno, emailid, false);
        }
        var fbAPIUserInfoCall = function () {
            facebookConnectPlugin.api("me/?fields=id,email,gender,first_name,last_name", ["public_profile"], fbAPIResponse);
        }
        //alertme('Coming Soon...', 'info');
        if (localStorage.getItem("VF_" + appId + "_LOGGED_IN_FB_UID") == null) {
            facebookConnectPlugin.login(
            ['email', 'public_profile'],
            function (response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    localStorage.setItem("VF_" + appId + "_LOGGED_IN_FB_UID", uid);
                    fbAPIUserInfoCall();
                } else if (response.status === 'not_authorized') {
                    alertme('You are logged in to your Facebook Account but have not authenticated our app.');
                } else {
                    // the user isn't logged in to Facebook.
                }
            },
            function (response) {
                if (!response.errorCode == 4201)
                    alertme('Having trouble sign in using your Facebook Account. Try again later.');
            }
        );
        }
        else {
            fbAPIUserInfoCall();
        }
    } catch (e) {
        alertException(e, current_page_id, '');
    }
});