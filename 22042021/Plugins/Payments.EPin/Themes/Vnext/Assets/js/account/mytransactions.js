﻿current_page_id = "#mytransactions";
var numrows = 10;

$(document).ready(function (e) {
    try {
        $('form#formMyTsearch').validate({
            rules: {
                searchMyT: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    rangelength: [2, 20]
                },
            },
            messages: {
                searchMyT: {
                    required: "Please enter seach text",
                    rangelength: "Search text should be of {0} to {1} characters"
                },
            }
        });

        GetTransactionList('0', 0, 0, numrows, null, 'ul.all-transactions', 'button.load-more-all');
        GetTransactionList('0', 1, 0, numrows, null, 'ul.recharge-transactions', 'button.load-more-success');
        GetTransactionList('0', 3, 0, numrows, null, 'ul.ticket-transactions', 'button.load-more-failure');

    } catch (e) {
        alertException(e, current_page_id, '');
    }
    $(document).on('click', 'a.redo-transaction', function (e) {
        try {
            localStorage.setItem("i", $(this).data('actionid'));
            localStorage.setItem("m", $(this).data('number'));
            localStorage.setItem("o", $(this).data('operatorid'));
            localStorage.setItem("a", $(this).data('amount'));
            if ($(this).data('txnname').indexOf('Prepaid') != -1)
                localStorage.setItem("pp", 'pr');
            if ($(this).data('txnname').indexOf('Postpaid') != -1)
                localStorage.setItem("pp", 'po');
            if ($(this).data('circleid') != '')
                localStorage.setItem("c", $(this).data('circleid'));
            if ($(this).data('unit') != '')
                localStorage.setItem("u", $(this).data('unit'));
            if ($(this).data('dob') != '')
                localStorage.setItem("d", $(this).data('dob'));
            if ($(this).data('cycle') != '')
                localStorage.setItem("cy", $(this).data('cycle'));
            if ($(this).data('actionid') == '4' && $(this).data('operatorid') == '52')
                localStorage.setItem("sdn", $(this).data('subdiv'));
            if ($(this).data('actionid') == '4' && $(this).data('operatorid') == '53')
                localStorage.setItem("sds", $(this).data('subdiv'));
            if ($(this).data('city') != '')
                localStorage.setItem("ci", $(this).data('city'));
            if ($(this).data('accno') != '')
                localStorage.setItem("acc", $(this).data('accno'));
            window.location = virtualPath + "Account/MobileRecharge?i=" + $(this).data('actionid');
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $(document).on('click', 'a.view-transaction-details', function (e) {
        try {
            var content = "<h4>" + $(this).data('txnname') + "</h4>";
            content += '<b>Number:</b> ' + $(this).data('number') + '<br />';
            content += '<b>Service Provider:</b> ' + $(this).data('operator');
            if ($(this).data('circle') != null && $(this).data('circle') != '' && $(this).data('circle') != '0')
                content += " - " + $(this).data('circle') + '<br />';
            else
                content += '<br />';
            if ($(this).data('accno') != null && $(this).data('accno') != '' && $(this).data('accno') != '0')
                content += '<b>Account Number:</b> ' + $(this).data('accno') + '<br />';
            if ($(this).data('cycle') != null && $(this).data('cycle') != '' && $(this).data('cycle') != '0')
                content += '<b>Cycle:</b> ' + $(this).data('cycle') + '<br />';
            if ($(this).data('subdiv') != null && $(this).data('subdiv') != '' && $(this).data('subdiv') != '0')
                content += '<b>Sub-division:</b> ' + $(this).data('subdiv') + '<br />';
            if ($(this).data('dob') != null && $(this).data('dob') != '' && $(this).data('dob') != '0')
                content += '<b>Date of Birth:</b> ' + $(this).data('dob') + '<br />';
            if ($(this).data('unit') != null && $(this).data('unit') != '' && $(this).data('unit') != '0')
                content += '<b>Unit:</b> ' + $(this).data('unit') + '<br />';
            content += '<b>Recharge/Payment Amount:</b> Rs.' + $(this).data('txnamount') + '/-<br />';
            if ($(this).data('taxamount') != null && $(this).data('taxamount') != '' && $(this).data('taxamount') != '0') {
                content += "<hr class='margin-5' />";
                var tax = [];
                if ($(this).data('taxdetails') != null && $(this).data('taxdetails') != '') {
                    if ($(this).data('taxdetails').indexOf("#") != -1)
                        tax = $(this).data('taxdetails').split("#");
                    else
                        tax[0] = $(this).data('taxdetails');
                    //console.log(tax);
                    var taxdtls = '', txxx = '';
                    for (var i = 0; i < tax.length; i++) {
                        var tx = tax[i].split("~");
                        taxdtls = "<b>" + tx[2] + (tx[3] == 'true' ? "" : tx[4]) + ":</b> Rs." + (tx[3] == true ? tx[9] : (parseFloat($(this).data('txnamount')) * parseFloat(tx[9]) / 100).toString()) + "/-";
                        if (txxx == '') {
                            txxx = taxdtls;
                        }
                        else {
                            txxx = txxx + "<br />" + taxdtls;
                        }
                    }
                }
                content += txxx + "<br />";
                //content += '<b>Tax Amount:</b> ' + $(this).data('taxamount') + '<br />';
                content += "<hr class='margin-5' />";
                content += '<b>Total Amount: Rs.' + $(this).data('totalamount') + '/-</b><br />';
                content += "<hr class='margin-5' />";
                if (parseFloat($(this).data('ewamount')) > 0) {
                    var ewstatus = '-';
                    switch (parseInt($(this).data('walletstatus'))) {
                        case 1:
                            ewstatus = '<span class="text-success">Paid</span>';
                            break;

                        case 2:
                            ewstatus = '<span class="text-warning">Pending</span>';
                            break;

                        case 3:
                            ewstatus = '<span class="text-danger">Failed</span>';
                            break;

                        default:
                            break;
                    }
                    if (ewstatus != '')
                        content += '<b>Wallet status:</b> ' + ewstatus + '<br />';
                }
                if (parseFloat($(this).data('pgamount')) > 0) {
                    var pgstatus = '-';
                    switch (parseInt($(this).data('gatewaystatus'))) {
                        case 1:
                            pgstatus = '<span class="text-success">Paid</span>';
                            break;

                        case 2:
                            pgstatus = '<span class="text-warning">Pending</span>';
                            break;

                        case 3:
                            pgstatus = '<span class="text-danger">Failed</span>';
                            break;

                        case 4:
                            pgstatus = '<span class="text-danger">Cancelled</span>';
                            break;

                        case 5:
                            pgstatus = '<span class="text-danger">Failed</span>';
                            break;

                        default:
                            break;
                    }
                    if (pgstatus != '')
                        content += '<b>Payment Gateway status:</b> ' + pgstatus + '<br />';
                }
                var rechargestatus = '-';
                switch (parseInt($(this).data('rechargestatus'))) {
                    case 1:
                        rechargestatus = '<span class="text-success">Successful</span>';
                        break;

                    case 2:
                        rechargestatus = '<span class="text-warning">Pending</span>';
                        break;

                    case 3:
                        rechargestatus = '<span class="text-danger">Failed</span>';
                        break;

                    case 4:
                        rechargestatus = '<span class="text-danger">Cancelled</span>';
                        break;

                    case 5:
                        rechargestatus = '<span class="text-danger">Failed</span>';
                        break;

                    default:
                        break;
                }
                if (rechargestatus != '')
                    content += '<b>Recharge status:</b> ' + rechargestatus + '<br />';
            }
            bootbox.alert(content);
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });
    $('form#formMyTsearch').on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {

            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, '');
        }

    });

    $('button.load-more-all').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.all-transactions li.transaction-record").length;
            GetTransactionList('0', 0, s, numrows, null, 'ul.all-transactions', 'button.load-more-all');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-success').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.recharge-transactions li.transaction-record").length;
            GetTransactionList('0', 1, s, numrows, null, 'ul.recharge-transactions', 'button.load-more-success')
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-failure').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.ticket-transactions li.transaction-record").length;
            GetTransactionList('0', 3, s, numrows, null, 'ul.ticket-transactions', 'button.load-more-failure');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });
});
