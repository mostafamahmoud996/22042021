﻿current_page_id = "#busreview";
var originId = '', originCity = '', destinationId = '', destinationCity = '', departDate = '', returnDate = '', departDateLabel = '', returnDateLabel = '', mode = '', airline = '', no_of_seats = 0, totaltravellers = 0, cbCode = '', cbDescription = '', cbAmount = 0, totalFare = 0, totalTax = 0, totalBaseFare = 0, selected_seats = [], seatscountmsg = '';

function CalculateFare() {
    try {
        var bf = 0, tt = 0, tf = 0, ss = '';
        if (selected_seats.length > 0) {
            for (var i = 0; i < selected_seats.length; i++) {
                //console.log(selected_seats[i]);
                bf += parseFloat(selected_seats[i].base_fare);
                tt += parseFloat(selected_seats[i].other_tax);
                tf += parseFloat(selected_seats[i].fare);
                ss += (ss == '' ? '' : ', ') + selected_seats[i].seat_name;
            }
        }

        var cashbackAmt = 0;
        if (cashback_list_json.length > 0) {
            cbCode = cashback_list_json[0].Code;
            cashbackAmt = parseFloat(tf) * parseFloat(cashback_list_json[0].Percentage) / 100;
            if (parseFloat(cashback_list_json[0].MaxAmount) > 0 && cashbackAmt > parseFloat(cashback_list_json[0].MaxAmount))
                cashbackAmt = parseFloat(cashback_list_json[0].MaxAmount);
            cashbackAmt = Math.floor(cashbackAmt);

            $("#total_cashback_amount").html(cashbackAmt);
            $("#divtotalcashback").show();
        }

        //bf = Math.round(totaltravellers * bf);
        //tt = Math.round(totaltravellers * tt);
        //tf = Math.round(totaltravellers * tf);

        $("#traveller_count").html(selected_seats.length);
        $("#traveller_base_fare").html(bf);
        $("#fees_surcharges").html(tt);
        $("#total_charges").html(tf);
        $("#selected_seats").html(ss == '' ? 'not selected' : ss);
        $("#seatscount").val(selected_seats.length);
        $(".payment-amount span").html(tf);
    } catch (e) {
        alertException(e, current_page_id, 'CalculateFare');
    }
}
function AddSeat(seat_count, obj, row) {
    try {
        var html = '', bseats = '';
        var seat = 'icon_bus_empty';
        if (row.ladies_seat == 'true')
            seat = 'bus_lady_first';
        if (row.available == 'false')
            seat = 'icon_bus_selected';
        var checked = '';
        if (localStorage.getItem("bus_seats") != null) {
            bseats = ',' + localStorage.getItem("bus_seats") + ',';
            if (bseats.indexOf(',' + row.seat_name + ',') > -1) {
                checked = 'checked';
                selected_seats.push(row);
            }
        }

        html += '<div class="padding-5">';
        html += '<input id="bus_seat_uid_' + seat_count + '" type="checkbox" class="bus_seat_checkbox" value="' + row.seat_name + '" ' + (row.available == 'false' ? 'disabled' : '') + ' data-seatinfo="' + StringEncryption(JSON.stringify(row)) + '" data-seat-name="' + row.seat_name + '" ' + checked + '>';
        html += '<label for="bus_seat_uid_' + seat_count + '" class="bus_seat_label ' + seat + '">';
        html += '<i class="b_one"></i>';
        html += '<i class="b_two"></i>';
        html += '<i class="b_three"></i>';
        html += '</label>';
        html += '</div>';
        $(obj).append($.parseHTML(html));
    } catch (e) {
        alertException(e, current_page_id, 'AddSeat');
    }
}

$(document).ready(function (e) {
    try {
        if (localStorage.getItem("bus_FromCityId") != null) {
            originId = localStorage.getItem("bus_FromCityId");
            destinationId = localStorage.getItem("bus_ToCityId");
            originCity = localStorage.getItem("bus_FromCity");
            destinationCity = localStorage.getItem("bus_ToCity");
            departDate = localStorage.getItem("bus_DepartureDate");
            returnDate = localStorage.getItem("bus_ArrivalDate");
            departDateLabel = localStorage.getItem("bus_DepartureDateLabel");
            returnDateLabel = localStorage.getItem("bus_ArrivalDateLabel");
            mode = localStorage.getItem("bus_roundtrip") == 0 ? 'ONE' : 'ROUND';
            totaltravellers = localStorage.getItem("bus_NoOfSeats");
            $("#total_travellers").html(totaltravellers);
            seatscountmsg = 'Select ' + totaltravellers + ' seat(s)';
            if (localStorage.getItem("bus_onwardOptions") != null) {
                var tripinfo = JSON.parse(StringDecryption(localStorage.getItem("bus_onwardOptions")));
                //console.log(tripinfo);
                var departureTime = ConvertStringToDateTime(tripinfo.departure_time, 'hhmmtt');
                var arrivalTime = ConvertStringToDateTime(tripinfo.arrival_time, 'hhmmtt');

                //$(".page-title").html('<div style="line-height:25px">' + localStorage.getItem("bus_FromCity") + '&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;' + localStorage.getItem("bus_ToCity") + '</div><div style="line-height:18px;font-size:12px">' + localStorage.getItem("bus_DepartureDateLabel") + (localStorage.getItem("bus_roundtrip") == 0 ? '' : (' - ' + localStorage.getItem("bus_ArrivalDateLabel"))) + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + '</div>');
                $("span.onward-title").html("<span class='bold'>DEPARTURE</span> (" + originCity + " - " + destinationCity + ' | ' + departDateLabel + ')');
                $("#onward_bus_title .from").html(originCity + '<small class="center-block">' + departureTime + '</small>');
                $("#onward_bus_title .to").html(destinationCity + '<small class="center-block">' + arrivalTime + '</small>');
                $("#onward_bus_more_info").html('<b class="text-dark-grey">' + tripinfo.travels_name + '</b>' + '<small class="center-block">(' + tripinfo.bus_type + ')</small>');
                if (tripinfo.boarding_points.length > 0) {
                    for (var i = 0; i < tripinfo.boarding_points.length; i++) {
                        $("#selBoardingpoint").append($("<option>", { 'value': tripinfo.boarding_points[i].bp_id, 'text': tripinfo.boarding_points[i].bp_name + ' - ' + tripinfo.boarding_points[i].bp_time.split(' ')[1] + ' ' + tripinfo.boarding_points[i].bp_time.split(' ')[2] }));
                    }
                    if (localStorage.getItem("bus_boarding_point_id") != null)
                        $("#selBoardingpoint").val(localStorage.getItem("bus_boarding_point_id")).trigger('change');
                }

                if (tripinfo.cancellation_policy.length > 0) {
                    var html = '';
                    html += '<ol class="no-margin" style="padding-left:15px">';
                    for (var j = 0; j < tripinfo.cancellation_policy.length; j++) {
                        html += '<li class="text-sm">';
                        html += (tripinfo.cancellation_policy[j].from_time == '0' ? 'Upto ' : (tripinfo.cancellation_policy[j].from_time + ' to ')) + tripinfo.cancellation_policy[j].to_time + ' - <b><i class="fa fa-rupee"></i> ' + tripinfo.cancellation_policy[j].service_charge + '</b>';
                        html += '</li>';
                    }
                    html += '</ol>';
                    $("#divPolicy").html(html);
                }

                var paramList = {};
                paramList.s = tripinfo.trip_id;
                //console.log(paramList.s);
                paramList.s = StringEncryption(paramList.s);
                $.ajax({
                    type: 'POST',
                    url: apiPath + "/Customer/BusSeatLayout",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(paramList),
                    dataType: 'json',
                    jsonp: null,
                    success: function (data, textStatus, xhr) {
                        try {
                            var r = StringDecryption(data);
                            var seats = JSON.parse(r).seats;
                            //console.log(seats);
                            var seat_count = 0;
                            for (var i = 0; i < 2; i++) {
                                var level = $.grep(seats, function (e) { return e.level == i; });
                                if (level.length > 0) {
                                    var percent = 0;
                                    var l0_row1 = $.grep(seats, function (e) { return e.level == i && e.row == 0; });
                                    var l0_row2 = $.grep(seats, function (e) { return e.level == i && e.row == 1; });
                                    var l0_row3 = $.grep(seats, function (e) { return e.level == i && e.row == 2; });
                                    var l0_row4 = $.grep(seats, function (e) { return e.level == i && e.row == 3; });
                                    var l0_row5 = $.grep(seats, function (e) { return e.level == i && e.row == 4; });
                                    if (l0_row1.length > 0)
                                        percent++;
                                    if (l0_row2.length > 0)
                                        percent++;
                                    if (l0_row3.length > 0)
                                        percent++;
                                    if (l0_row4.length > 0)
                                        percent++;
                                    if (l0_row5.length > 0)
                                        percent++;
                                    percent = (100 / percent).toFixed(2);

                                    for (var j = 0; j < 5; j++) {
                                        var id = '#lower_row' + (j + 1);
                                        if (i == 1)
                                            id = '#upper_row' + (j + 1);
                                        var rows = $.grep(seats, function (e) { return e.level == i && e.row == j; });
                                        //console.log(rows);
                                        if (rows.length > 0) {
                                            for (var k = 0; k < rows.length; k++) {
                                                seat_count++;
                                                //console.log('----------------------------------------------------------------------------------');
                                                //console.log('seat_count - ' + seat_count + '  ,  id - ' + id + '  ,  i - ' + i + '  ,  j - ' + j + '  ,  k - ' + k);
                                                AddSeat(seat_count, id, rows[k]);
                                                $(id).show();
                                                $(id).css('width', percent.toString() + '%');
                                            }
                                        }
                                    }
                                }
                                else {
                                    if (i == 1) {
                                        $("#level_selection").hide();
                                        $("#UpperLevel").hide();
                                    }
                                }
                            }
                            CalculateFare();
                            $("li.lower a").trigger('click');
                            $("#formLoadingBSRW").fadeOut('fast', function (e) {
                                $("#frmbusreview").fadeIn();
                            });
                        } catch (e) {
                            //console.log(e);
                            alertException(e, current_page_id, 'SearchBuses.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $("#bus_review_error_message").html(errorThrown);
                        $("#formLoadingBSRW").fadeOut('fast', function (e) {
                            $("#reviewemptyBSRW").fadeIn();
                        });
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                });
            }
            else {
                $("#formLoadingBSRW").fadeOut('fast', function (e) {
                    $("#reviewemptyBSRW").fadeIn();
                });
            }
        }
        else
            window.location = virtualPath + "Account/BusBooking";

    } catch (e) {
        alertException(e, current_page_id, 'Page.Load');
    }

    $("a.goto-bus-search").on('click', function (e) {
        window.location = virtualPath + "Account/BusSearchResults";
    });


    $(document).on('click', 'input.bus_seat_checkbox', function (e) {
        //console.log('chk = ' + $('input:checkbox.bus_seat_checkbox:checked').length + '  ,  max - ' + totaltravellers + '  ,  checked - ' + $(this).prop('checked') + '  ,  value - ' + $(this).val());
        if ($('input:checkbox.bus_seat_checkbox:checked').length > totaltravellers)
            return false;
        else {
            if ($(this).prop('checked') == true) {
                selected_seats.push(JSON.parse(StringDecryption($(this).data('seatinfo'))));
            }
            else
                removeByAttr(selected_seats, 'seat_name', $(this).val());
        }
        CalculateFare();
        //console.log(selected_seats);
    });

    $('form#frmbusreview').on('submit', function (e) {
        try {
            e.preventDefault();
            var isvalidate = $(this).valid();
            if ($("#selBoardingpoint :selected").val() == '') {
                notification("Select a Boarding point", null);
                isvalidate = false;
                return false;
            }
            if (isvalidate && parseInt($("#seatscount").val()) != totaltravellers) {
                notification(seatscountmsg, null);
                isvalidate = false;
                return false;
            }
            if (isvalidate) {
                localStorage.setItem("bus_totalfare", $("#total_charges").html().replaceAll(',', ''));
                localStorage.setItem("bus_seats", $("#selected_seats").html().replaceAll(', ', ','));
                localStorage.setItem("bus_seats_info", JSON.stringify(selected_seats));
                localStorage.setItem("bus_boarding_point", $("#selBoardingpoint :selected").text());
                localStorage.setItem("bus_boarding_point_id", $("#selBoardingpoint :selected").val());
                localStorage.setItem("bus_cashbackcode", cbCode);
                localStorage.setItem("bus_cashbackamount", $("#total_cashback_amount").html().replaceAll(',', ''));

                window.location = virtualPath + "Account/BusMakePayment";
            }
        } catch (e) {
            alertException(e, current_page_id, 'frmflightsearchresults.submit');
        }
    });
});