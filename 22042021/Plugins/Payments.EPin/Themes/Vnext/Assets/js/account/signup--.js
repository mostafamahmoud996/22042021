﻿current_page_id = "#signup";

$(document).ready(function (e) {
    try {
        $("#mobilenoSUF").focus();

        $('form#frmsignupsendotp').validate({
            rules: {
                mobilenoSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    rangelength: [10, 10],
                    regex: /^[0-9]+$/,
                },
            },
            messages: {
                mobilenoSUF: {
                    required: "Please enter Mobile number",
                    rangelength: "Mobile number should be {0}-digit",
                    regex: "Only numbers are allowed",
                },
            }
        });

        $('form#frmsignupverifyotp').validate({
            rules: {
                otpSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9]+$/,
                },
            },
            messages: {
                otpSUF: {
                    required: "Please enter OTP",
                    regex: "Only numbers are allowed",
                },
            }
        });

        $('form#frmsignup').validate({
            rules: {
                genderSUF: { required: true },
                firstnameSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_]+$/,
                },
                lastnameSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_]+$/,
                },
                mobilenoSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9]+$/,
                },
                emailidSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    //email: true,
                    regex: /^[A-Za-z0-9\-\_\.\@]+$/,
                },
                passwordSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    rangelength: [6, 15]
                },
                repasswordSUF: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    equalTo: "#passwordSUF"
                },
            },
            messages: {
                genderSUF: { required: "Select Title" },
                firstnameSUF: {
                    required: "Please enter First name",
                    regex: "Only alphabets, numbers and underscore is allowed",
                },
                lastnameSUF: {
                    required: "Please enter Last name",
                    regex: "Only alphabets, numbers and underscore is allowed",
                },
                mobilenoSUF: {
                    required: "Please enter Mobile number",
                    regex: "Only numbers are allowed",
                },
                emailidSUF: {
                    required: "Please enter Email Id",
                    regex: "Enter a valid Email Id",
                },
                passwordSUF: {
                    required: "Please enter a Password",
                    rangelength: "Password should be of {0} to {1} characters"
                },
                repasswordSUF: {
                    required: "Re-enter Password to confirm",
                    equalTo: "Re-entered password does not match"
                },
            }
        });

        $('form#frmsignupsendotp').on('submit', function (e) {
            try {
                var isvalidate = $(this).valid();
                if (isvalidate) {
                    var btntext = $("button.otp-button span").html();
                    var paramList = {};
                    paramList.s = StringEncryption($('#mobilenoSUF').val().trim());
                    paramList.returnJSON = true;
                    $.ajax({
                        type: 'POST',
                        url: apiPath + "/Customer/SignUpOTP",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(paramList),
                        dataType: 'json',
                        jsonp: null,
                        beforeSend: function (xhr, opts) {
                            $("button.otp-button").prop('disabled', true);
                            $("button.otp-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;sending otp...');
                        },
                        success: function (data, textStatus, xhr) {
                            try {
                                //console.log(data);
                                //console.log('--------------------');
                                var r = StringDecryption(data);
                                //$(":mobile-pagecontainer").pagecontainer("change", "signin.html", { role: "page" });

                                var fn1 = function () {
                                    $('#signup_step_01').addClass('bounceOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                                    $('#signup_step_02').removeClass("bounceOutRight");
                                }
                                $.when(
                                    fn1
                                )
                                .done(function () {
                                    $('#signup_step_01').removeClass("bounceInLeft");
                                    $('#mobilenumberSUF').html('+91-' + $('#mobilenoSUF').val());
                                    localStorage.setItem("VF_" + appId + "_LOGGED_IN_OTP", r);
                                    $('#signup_step_01').hide();
                                    $('#signup_step_02').addClass('bounceInRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                                    $('#signup_step_02').css('display', '');
                                    $("#otpSUF").focus();
                                });
                            } catch (e) {
                                alertException(e, current_page_id, '');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            OnAjaxError(xhr, textStatus, errorThrown);
                        },
                        complete: function (a) {
                            $("button.otp-button").prop('disabled', false);
                            $("button.otp-button span").html(btntext);
                        }
                    });
                }
                e.preventDefault();
            } catch (e) {
                alertException(e, current_page_id, '');
            }

        });

        $('form#frmsignupverifyotp').on('submit', function (e) {
            try {
                var isvalidate = $(this).valid();
                if (isvalidate) {
                    if (localStorage.getItem("VF_" + appId + "_LOGGED_IN_OTP") == $("#otpSUF").val().trim()) {
                        var fn1 = function () {
                            $('#signup_step_02').addClass('bounceOutLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                            $('#signup_step_03').removeClass("bounceOutRight");
                        }
                        $.when(
                            fn1
                        )
                        .done(function () {
                            $('#signup_step_02').removeClass("bounceInRight");
                            $('#signup_step_02').hide();
                            $('#signup_step_03').addClass('bounceInRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                            $('#signup_step_03').css('display', '');
                            $("#firstnameSUF").focus();
                        });
                    }
                    else
                        alertme('Entered OTP does not match.');
                }
                e.preventDefault();
            } catch (e) {
                alertException(e, current_page_id, '');
            }

        });

        $('form#frmsignup').on('submit', function (e) {
            try {
                var isvalidate = $(this).valid();
                if (isvalidate) {
                    var btntext = $("button.submit-button span").html();
                    var mp = window.sessionStorage.getItem("i") == null ? 0 : 1;
                    var paramList = {};
                    paramList.s = $('input[name=genderSUF]:checked').val() + "|" + $('#firstnameSUF').val().trim() + "|" + $('#lastnameSUF').val().trim() + "|" + $('#mobilenoSUF').val().trim() + "|" + $('#emailidSUF').val().trim() + "|" + $('#passwordSUF').val().trim() + "|" + "";
                    paramList.s = StringEncryption(paramList.s);
                    paramList.returnJSON = true;
                    $.ajax({
                        type: 'POST',
                        url: apiPath + "/Customer/SignUp",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(paramList),
                        dataType: 'json',
                        jsonp: null,
                        beforeSend: function (xhr, opts) {
                            $("button.submit-button").prop('disabled', true);
                            $("button.submit-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;signing up...');
                        },
                        success: function (data, textStatus, xhr) {
                            try {
                                var arr = [];
                                arr = StringDecryption(data).split("|");
                                localStorage.removeItem("VF_" + appId + "_LOGGED_IN_OTP");
                                bootalert('Congratulations! You are now a registered Customer.', 'success', function () {
                                    if (window.sessionStorage.getItem("i") != null) {
                                        window.location = virtualPath + "Account/MakePayment";
                                    }
                                    else {
                                        window.location = virtualPath + "Account/Dashboard";
                                    }
                                });
                            } catch (e) {
                                alertException(e, current_page_id, 'SignUp.success');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            OnAjaxError(xhr, textStatus, errorThrown);
                        },
                        complete: function (a) {
                            $("button.submit-button").prop('disabled', false);
                            $("button.submit-button span").html(btntext);
                        }
                    });
                }
                e.preventDefault();
            } catch (e) {
                alertException(e, current_page_id, '');
            }

        });

        $("#changemobileSUF").on("click", function (e) {
            var fn1 = function () {
                $('#signup_step_02').addClass('bounceOutRight animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                $('#signup_step_01').removeClass("bounceOutLeft");
            }
            $.when(
                fn1
            )
            .done(function () {
                $('#signup_step_02').removeClass("bounceInRight");
                $('#signup_step_02').hide();
                $('#signup_step_01').addClass('bounceInLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');
                $('#signup_step_01').css('display', '');
            });
        });

        $("#resendotpSUF").on("click", function (e) {
            var btntext = $("button.verify-otp-button span").html();
            var paramList = {};
            paramList.s = StringEncryption($('#mobilenoSUF').val().trim());
            paramList.returnJSON = true;
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/SignUpOTP",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                beforeSend: function (xhr, opts) {
                    $("button.verify-otp-button").prop('disabled', true);
                    $("button.verify-otp-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;sending otp...');
                    $("#changemobileSUF").fadeOut();
                },
                success: function (data, textStatus, xhr) {
                    try {
                        //console.log(data);
                        //console.log('--------------------');
                        if (data.e != '') {
                            alertme(data.e);
                        }
                        else {
                            var r = StringDecryption(data.r);
                            localStorage.setItem("VF_" + appId + "_LOGGED_IN_OTP", r);
                            $("#otpSUF").closest(".form-group,.input-group").removeClass('is-empty');
                        }
                    } catch (e) {
                        alertException(e, current_page_id, '');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
                complete: function (a) {
                    $("button.verify-otp-button").prop('disabled', false);
                    $("button.verify-otp-button span").html(btntext);
                    $("#changemobileSUF").fadeIn();
                }
            });
        });
        $("#signinnowSUF").on("click", function (e) {
            $(":mobile-pagecontainer").pagecontainer("change", "signin.html", { role: "page" });
        });
        $("#gplusloginSUF").on("click", function (e) {
            try {
                window.plugins.googleplus.login(
                    {},
                    function (obj) {
                        var gender, firstname, lastname, mobileno, emailid;
                        gender = "";
                        firstname = obj.givenName;
                        lastname = obj.familyName;
                        mobileno = "";
                        emailid = obj.email;
                        SocialSign(gender, firstname, lastname, mobileno, emailid, true);
                    },
                    function (msg) {
                        alertme('Having trouble signin in using your Google Account.');
                    }
                );
            } catch (e) {
                alertException(e, current_page_id, '');
            }
        });
        $("#facebookloginSUF").on("click", function (e) {
            try {
                alertme('Coming Soon...', 'info');
            } catch (e) {
                alertException(e, current_page_id, '');
            }
        });

    } catch (e) {
        alertException(e, current_page_id, '');
    }
});