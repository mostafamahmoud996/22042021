﻿current_page_id = "#flightsearchresults";
var type, origin, destination, departDate, departDateLabel, returnDate, returnDateLabel, travellers, totaltravellers, cabinClass, cabinClassName, mode, airline;
var adult = 0, child = 0, infant = 0;
var customerId = LOGGED_IN_ID == null ? '0' : LOGGED_IN_ID.toString();
cashback_list_json = [];

$(document).ready(function (e) {
    try {
        if (localStorage.getItem("domestic") != null) {
            localStorage.removeItem("onwardOptions");
            localStorage.removeItem("returnOptions");
            localStorage.removeItem("onwardflight");
            localStorage.removeItem("returnflight");
            localStorage.removeItem("flighttotalfare");

            localStorage.removeItem("onward-cb-code");
            localStorage.removeItem("onward-cb-description");
            localStorage.removeItem("onward-cb-fare");
            localStorage.removeItem("onward-cb-amount");

            localStorage.removeItem("return-cb-code");
            localStorage.removeItem("return-cb-description");
            localStorage.removeItem("return-cb-fare");
            localStorage.removeItem("return-cb-amount");

            type = localStorage.getItem("domestic");
            origin = localStorage.getItem("FromCode");
            destination = localStorage.getItem("ToCode");
            departDate = localStorage.getItem("DepartureDate");
            departDateLabel = localStorage.getItem("DepartureDateLabel");
            if (localStorage.getItem("ArrivalDate") != null) {
                returnDate = localStorage.getItem("ArrivalDate");
                returnDateLabel = localStorage.getItem("ArrivalDateLabel");
            }
            travellers = localStorage.getItem("travellers");
            totaltravellers = parseInt(localStorage.getItem("totaltravellers"));
            $("#total_travellers").html(totaltravellers == 1 ? '1 Traveller' : totaltravellers.toString() + ' Travellers');
            if (travellers.indexOf('~') != -1) {
                var arr = travellers.split('~');
                adult = parseInt(arr[0]);
                child = parseInt(arr[1]);
                if (arr.length > 2) {
                    infant = parseInt(arr[2]);
                }
            }
            else {
                adult = parseInt(travellers);
            }
            cabinClass = localStorage.getItem("cabinclass");
            cabinClassName = cabinClass == 'E' ? 'Economy' : 'Business';
            mode = localStorage.getItem("roundtrip") == 0 ? 'ONE' : 'ROUND';
            airline = '';

            if (child > 0)
                $("#child_fare").show();

            if (infant > 0)
                $("#infant_fare").show();

            var page_title = '<div style="line-height:25px">' + origin + '&nbsp;<i class="fa ' + (mode == 'ONE' ? 'fa-arrow-right' : 'fa-exchange') + '"></i>&nbsp;' + destination + '</div><div style="line-height:18px;font-size:12px">' + departDateLabel + (mode == 'ONE' ? '' : (' - ' + returnDateLabel)) + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + '</div>';
            $(".flight-page-title").html(page_title);
            $("span.onward-title").html("<span class='bold'>DEPARTURE</span> (" + origin + " - " + destination + ' | ' + departDateLabel + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + ')');
            $("#onward_flight_title span.from").html(origin);
            $("#onward_flight_title span.to").html(destination);
            if (mode == 'ROUND') {
                $("span.return-title").html("<span class='bold'>RETURN</span> (" + destination + " - " + origin + ' | ' + returnDateLabel + ')');
                $(".return-information").show();
                $(".return-height-5").show();
                $("#return_flight_title span.from").html(destination);
                $("#return_flight_title span.to").html(origin);
            }

            $("ul#ul_onward_flight_list").empty();
            $("#onward_flight_list_header").hide();
            $("#ul_onward_flight_list").hide();
            $("#onwardlistloadingFSR").show();

            $("#return_flight_list_header").hide();
            $("#ul_return_flight_list").hide();
            $("ul#ul_return_flight_list").show();

            var paramList = {};
            paramList.s = type + "|" + origin + "|" + destination + "|" + departDate + "|" + returnDate + "|" + adult.toString() + "|" + child.toString() + "|" + infant.toString() + "|" + cabinClass + "|" + mode + "|" + airline;
            //console.log(paramList.s);
            paramList.s = StringEncryption(paramList.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/SearchFlights",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    try {
                        var r = StringDecryption(data);
                        //console.log(r);
                        var json = JSON.parse(r.split('|')[0]).response;
                        cashback_list_json = JSON.parse(r.split('|')[1]);
                        //console.log(json);
                        //console.log(cashback_list_json);
                        if (json.onwardOptions == null) {
                            $("#onwardlistloadingFSR").fadeOut('fast', function (e) {
                                $("#onwardlistemptyFSR").fadeIn();
                            });
                        }
                        else {
                            localStorage.setItem("onwardOptions", JSON.stringify(json.onwardOptions));
                            for (var i = 0; i < json.onwardOptions.length ; i++) {
                                var flight = json.onwardOptions[i].flight[0];
                                var flightcount = json.onwardOptions[i].flight.length, currDtm = new Date(), availability = true;
                                var totalduration = TotalFlightDuration(1, json.onwardOptions[i].flight);
                                currDtm = currDtm.addHours(1);
                                if (availability && ConvertStringToDateTime(flight.departureDateTime, '') >= currDtm) {
                                    var rule = json.onwardOptions[i].rule;
                                    var departureTime = ConvertStringToDateTime(flight.departureDateTime, 'hhmmtt');
                                    var arrivalTime = ConvertStringToDateTime(json.onwardOptions[i].flight[flightcount - 1].arrivalDateTime, 'hhmmtt');
                                    var cashback = JSON.parse(CalculateCashbackAmount(type, 1, flight.airlineCode, json.onwardOptions[i].totalFare))[0];
                                    //console.log(cashbackAmt);
                                    var $li = $("<li>", { 'class': 'three', 'data-airlinecode': flight.airlineCode, 'data-departuredatetime': flight.departureDateTime, 'data-arrivaldatetime': flight.arrivalDateTime, 'data-duration': totalduration, 'data-totalfare': json.onwardOptions[i].totalFare });
                                    var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + webPath + '/Files/AirlineLogos/' + flight.airlineCode + '.gif" class="center-block" style="max-width:30px" /><small class="center-block text-light-gray">' + flight.airlineCode + '-' + flight.flightNumber + '</small>' });
                                    var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-onward center-block text-center" data-fareid="' + json.onwardOptions[i].fareId + '" data-airlinecode="' + flight.airlineCode + '-' + flight.flightNumber + '" data-departuretime="' + departureTime + '" data-totalfare="' + json.onwardOptions[i].totalFare + '" data-cb-code="' + cashback.code + '" data-cb-description="' + cashback.description + '" data-cb-amount="' + cashback.amount + '"><span class="center-block text-center text-dark-grey bold">' + departureTime + '  -  ' + arrivalTime + '</span><small class="center-block text-center text-light-gray">' + StopsInfoFromCount(flightcount) + ' | ' + totalduration + '</small></a>' });
                                    var $dva = $("<div>", { 'class': 'amount text-center text-bold', 'style': 'color:#2BA39F', html: '<i class="fa fa-rupee"></i> ' + parseFloat(json.onwardOptions[i].totalFare).toLocaleString('en-IN') + (rule.refundable == "yes" ? '<small class="center-block text-black text-normal">Refundable</small>' : '') + (cashback.amount == 0 ? '' : '<small class="center-block text-black text-normal"><i class="fa fa-rupee"></i> ' + cashback.amount.toString() + ' eCash</small>') });
                                    $li.append($dvl);
                                    $li.append($dvd);
                                    $li.append($dva);
                                    $("ul#ul_onward_flight_list").append($li);
                                }
                            }
                            if ($("ul#ul_onward_flight_list li").length > 0) {
                                SortList("#ul_onward_flight_list", 'totalfare', 'asc', 'number');
                                $("#onwardlistloadingFSR").fadeOut('fast', function (e) {
                                    $("#onward_flight_list_header").fadeIn();
                                    $("#ul_onward_flight_list").fadeIn();
                                });
                            }
                            else {
                                $("#onwardlistloadingFSR").fadeOut('fast', function (e) {
                                    $("#onwardlistemptyFSR").fadeIn();
                                });
                            }
                        }
                        if (mode == 'ROUND') {
                            if (json.returnOptions == null) {
                                $("#return_flight_list_header").fadeOut();
                                $("#ul_return_flight_list").fadeOut('fast', function (e) {
                                    $("#returnlistemptyFSR").fadeIn();
                                });
                            }
                            else {
                                localStorage.setItem("returnOptions", JSON.stringify(json.returnOptions));
                                //$("span.return-title").html(destination + " to " + origin + " - " + json.returnOptions.length + "flight(s) found")
                                for (var i = 0; i < json.returnOptions.length ; i++) {
                                    var flight = json.returnOptions[i].flight[0];
                                    var flightcount = json.returnOptions[i].flight.length, currDtm = new Date();
                                    var totalduration = TotalFlightDuration(1, json.returnOptions[i].flight);
                                    currDtm = currDtm.addHours(1);
                                    if (ConvertStringToDateTime(flight.departureDateTime, '') >= currDtm) {
                                        var rule = json.returnOptions[i].rule;
                                        var departureTime = ConvertStringToDateTime(flight.departureDateTime, 'hhmmtt');
                                        var arrivalTime = ConvertStringToDateTime(json.returnOptions[i].flight[flightcount - 1].arrivalDateTime, 'hhmmtt');
                                        var cashback = JSON.parse(CalculateCashbackAmount(type, 2, flight.airlineCode, json.returnOptions[i].totalFare))[0];
                                        var $li = $("<li>", { 'class': 'three', 'data-airlinecode': flight.airlineCode, 'data-departuredatetime': flight.departureDateTime, 'data-arrivaldatetime': flight.arrivalDateTime, 'data-duration': totalduration, 'data-totalfare': json.returnOptions[i].totalFare });
                                        var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + webPath + '/Files/AirlineLogos/' + flight.airlineCode + '.gif" class="center-block" style="max-width:30px" /><small class="center-block text-light-gray">' + flight.airlineCode + '-' + flight.flightNumber + '</small>' });
                                        var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-return center-block text-center" data-fareid="' + json.returnOptions[i].fareId + '" data-airlinecode="' + flight.airlineCode + '-' + flight.flightNumber + '" data-departuretime="' + departureTime + '" data-totalfare="' + json.returnOptions[i].totalFare + '" data-cb-code="' + cashback.code + '" data-cb-description="' + cashback.description + '" data-cb-amount="' + cashback.amount + '"><span class="center-block text-center text-dark-grey bold">' + departureTime + '  -  ' + arrivalTime + '</span><small class="center-block text-center text-light-gray">' + StopsInfoFromCount(flightcount) + ' | ' + totalduration + '<small></a>' });
                                        var $dva = $("<div>", { 'class': 'amount text-center text-bold', 'style': 'color:#2BA39F', html: '<i class="fa fa-rupee"></i> ' + parseFloat(json.returnOptions[i].totalFare).toLocaleString('en-IN') + (rule.refundable == "yes" ? '<small class="center-block text-success text-normal">Refundable</small>' : '') + (cashback.amount == 0 ? '' : '<small class="center-block text-black text-normal"><i class="fa fa-rupee"></i> ' + cashback.amount.toString() + ' eCash</small>') });
                                        $li.append($dvl);
                                        $li.append($dvd);
                                        $li.append($dva);
                                        $("ul#ul_return_flight_list").append($li);
                                    }
                                }
                                if ($("ul#ul_return_flight_list li").length > 0) {
                                    SortList("#ul_return_flight_list", 'totalfare', 'asc', 'number');
                                    $("#returnlistemptyFSR").fadeOut('fast', function (e) {
                                        $("#return_flight_list_header").fadeIn();
                                        $("#ul_return_flight_list").fadeIn();
                                    });
                                }
                                else {
                                    $("#return_flight_list_header").fadeOut();
                                    $("#ul_return_flight_list").fadeOut('fast', function (e) {
                                        $("#returnlistemptyFSR").fadeIn();
                                    });
                                }
                            }
                        }
                    } catch (e) {
                        //console.log(e);
                        $("#onwardlistloadingFSR").fadeOut('fast', function (e) {
                            if (e.message.indexOf('Service not available') != -1)
                                $("h5.flight-search-error-message").html('No flights available for selected Date')
                            else
                                $("h5.flight-search-error-message").html('Flight information not available at this moment.');
                            $("#onwardlistemptyFSR").fadeIn();
                        });
                        alertException(e, current_page_id, 'SearchFlights.success');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    //OnAjaxError(xhr, textStatus, errorThrown);
                    $("#onwardlistloadingFSR").fadeOut('fast', function (e) {
                        if (errorThrown.indexOf('Service not available') != -1)
                            $("h5.flight-search-error-message").html('No flights available for selected Date')
                        else
                            $("h5.flight-search-error-message").html('Flight information not available at this moment.');
                        $("#onwardlistemptyFSR").fadeIn();
                    });
                },
            });
        }
        else
            window.location = virtualPath + "Account/FlightBooking";
    } catch (e) {
        alertException(e, current_page_id, 'Page.Load');
    }

    $("a.goto-flight-booking").on('click', function (e) {
        window.location = virtualPath + "Account/FlightBooking";
    });

    $(document).on('click', "a.select-onward", function (e) {
        try {
            var _this = this;
            if (localStorage.getItem("onwardOptions") == null) {
                bootalert("Oops! Departure flight information not found.", 'error', function (e) {
                    window.location = virtualPath + "Account/FlightBooking";
                });
            }
            else {
                var json = JSON.parse(localStorage.getItem("onwardOptions"));
                var onwardflight = $.grep(json, function (e) { return e.fareId == $(_this).data('fareid'); });
                if (onwardflight.length > 0) {
                    $(_this).addClass('selected');
                    $("span.onward-details").html('(' + $(_this).data('airlinecode') + ' | ' + $(_this).data('departuretime') + ' | <i class="fa fa-rupee"></i> ' + $(_this).data('totalfare') + ')');
                    var newobj = '#flight_booking';
                    if (mode == 'ROUND') {
                        newobj = '#return_flight_list';
                    }
                    $('#onward_flight_list').fadeOut('fast', function (e) {
                        $(newobj).fadeIn();
                    });

                    var flight = onwardflight[0].flight[0];
                    var flightcount = onwardflight[0].flight.length;
                    var totalduration = TotalFlightDuration(1, onwardflight[0].flight);

                    $("#onward_flight_more_info").html(cabinClassName + ' | ' + StopsInfoFromCount(flightcount));
                    $("img#onward_airlinecode").prop('src', webPath + '/Files/AirlineLogos/' + onwardflight[0].flight[0].airlineCode + '.gif');
                    $("#onward_flight_code").html(onwardflight[0].flight[0].airlineCode + ' - ' + onwardflight[0].flight[0].flightNumber);

                    //$("#onward_departure_time").html(ConvertStringToDateTime(onwardflight[0].flight[0].departureDateTime, 'hhmmtt'));
                    //$("#onward_departure_date").html(ConvertStringToDateTime(onwardflight[0].flight[0].departureDateTime, 'ddd, DD MMM'));
                    //$("#onward_duration").html(totalduration);
                    //$("#onward_arrival_time").html(ConvertStringToDateTime(onwardflight[0].flight[flightcount - 1].arrivalDateTime, 'hhmmtt'));
                    //$("#onward_arrival_date").html(ConvertStringToDateTime(onwardflight[0].flight[flightcount - 1].arrivalDateTime, 'ddd, DD MMM'));

                    $("#onward_stop_details").html(StopOverDetails(onwardflight[0].flight));
                    $("#onward_baggage").html(onwardflight[0].rule.baggage == "" ? "N.a." : onwardflight[0].rule.baggage + ' kg');
                    $("#onward_total_fare span").html(parseFloat(onwardflight[0].totalFare).toLocaleString('en-IN'));
                    var cashbackAmt = parseFloat($(_this).data('cb-amount'));
                    if (cashbackAmt > 0) {
                        $("#onward_cashback_code").html('Cashback applied (Code - ' + $(_this).data('cb-code') + ')');
                        $("#onward_cashback span").html(cashbackAmt.toLocaleString('en-IN'));
                        $("#onward_cashback_description").html($(_this).data('cb-description'));
                        $("#divonwardcashback").show();
                        $("#total_cashback_amount").html(cashbackAmt.toLocaleString('en-IN'));
                        $("#divtotalcashback").show();
                    }
                    $("#adult_count").html(adult);
                    $("#child_count").html(child);
                    $("#infant_count").html(infant);

                    var adultBaseFare = parseFloat(onwardflight[0].adultBaseFare) * adult;
                    var childBaseFare = parseFloat(onwardflight[0].childBaseFare) * child;
                    var infantBaseFare = parseFloat(onwardflight[0].infantBaseFare) * infant;
                    var feesSurcharges = parseFloat(onwardflight[0].otherTax) + parseFloat(onwardflight[0].serviceTax) + parseFloat(onwardflight[0].tds) + parseFloat(onwardflight[0].yq);
                    var total_charges = parseFloat(onwardflight[0].totalFare) + parseFloat(onwardflight[0].serviceTax) + parseFloat(onwardflight[0].tds);
                    var bal = 0;
                    //console.log('adultBaseFare - ' + adultBaseFare);
                    //console.log('childBaseFare - ' + childBaseFare);
                    //console.log('infantBaseFare - ' + infantBaseFare);
                    //console.log('feesSurcharges - ' + feesSurcharges);
                    //console.log('total_charges - ' + total_charges);
                    //console.log('(adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges) - ' + (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges));
                    if (total_charges > (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges)) {
                        bal = total_charges - adultBaseFare - childBaseFare - infantBaseFare - feesSurcharges;
                        adultBaseFare = adultBaseFare + bal;
                    }
                    else {
                        bal = (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges) - total_charges;
                        adultBaseFare = adultBaseFare - bal;
                    }

                    $("#adult_base_fare").html(adultBaseFare.toLocaleString('en-IN'));
                    $("#child_base_fare").html(childBaseFare.toLocaleString('en-IN'));
                    $("#infant_base_fare").html(infantBaseFare.toLocaleString('en-IN'));
                    $("#fees_surcharges").html(feesSurcharges.toLocaleString('en-IN'));

                    $("#onward_fare_type").html('<span class="text-' + (onwardflight[0].rule.refundable == 'yes' ? 'success">Refundable' : 'danger">Non-Refundable') + '</span>');
                    var reschedule_charge = 'Not Available';
                    if (onwardflight[0].rule.rescheduleCharge != null) {
                        reschedule_charge = onwardflight[0].rule.rescheduleCharge;
                        if (reschedule_charge != 'Not Available') {
                            reschedule_charge = '<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(reschedule_charge).toLocaleString('en-IN');
                        }
                    }
                    $("#onward_reschedule_charges").html(reschedule_charge);
                    var cancFee = onwardflight[0].rule.cancellationFee == null ? '0' : onwardflight[0].rule.cancellationFee;
                    var cancNote = '';
                    if (isNaN(cancFee)) {
                        cancNote = cancFee;
                        $("#onward_cancellation_charges").html('N.a.');
                    }
                    else {
                        $("#onward_cancellation_charges").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(cancFee).toLocaleString('en-IN'));
                    }
                    var cancellation_slab = '-';
                    if (onwardflight[0].rule.cancellationSlab != null) {
                        cancellation_slab = '<ul style="margin-left:15px"><li>' + onwardflight[0].rule.cancellationSlab.replaceAll('#', '</li><li>');
                        cancellation_slab = cancellation_slab.substring(0, cancellation_slab.length - 5) + '</ul>';
                    }
                    if (cancNote != '') {
                        if (cancellation_slab == '-')
                            cancellation_slab = cancNote;
                        else
                            cancellation_slab = cancNote + '<br /><br />' + cancellation_slab;
                    }
                    $("#onward_cancellation_slab").html(cancellation_slab);
                    var onward_flight_remarks = '-';
                    if (onwardflight[0].rule.remarks != null)
                        onward_flight_remarks = onwardflight[0].rule.remarks;
                    $("#onward_remarks").html(onward_flight_remarks);
                    $("h3.payment-amount span").html(total_charges.toLocaleString('en-IN'));
                    $("#total_charges").html($("h3.payment-amount span").html());

                    localStorage.setItem("onwardflight", JSON.stringify(onwardflight));

                    if ($(_this).data('cb-code') != '') {
                        localStorage.setItem("onward-cb-code", $(_this).data('cb-code'));
                        localStorage.setItem("onward-cb-description", $(_this).data('cb-description'));
                        localStorage.setItem("onward-cb-fare", onwardflight[0].totalFare);
                        localStorage.setItem("onward-cb-amount", $(_this).data('cb-amount'));
                    }
                    //console.log(onwardflight);
                }
                else {
                    bootalert("Oops! Departure flight information not found.", 'error', function (e) {
                        window.location = virtualPath + "Account/FlightBooking";
                    });
                }
            }
        } catch (e) {
            alertException(e, current_page_id, 'a.select-onward.click');
        }
    });

    $(document).on('click', "a.select-return", function (e) {
        try {
            var _this = this;
            if (localStorage.getItem("returnOptions") == null) {
                bootalert("Oops! Return flight information not found.", 'error', function (e) {
                    window.location = virtualPath + "Account/FlightBooking";
                });
            }
            else {
                var json = JSON.parse(localStorage.getItem("returnOptions"));
                var returnflight = $.grep(json, function (e) { return e.fareId == $(_this).data('fareid'); });
                if (returnflight.length > 0) {
                    var onwardflight = JSON.parse(localStorage.getItem("onwardflight"));
                    var onwardflight_arrivalDateTime = ConvertStringToDateTime(onwardflight[0].flight[0].arrivalDateTime, '');
                    var onwardflight_arrivalTime = ConvertStringToDateTime(onwardflight[0].flight[0].arrivalDateTime, 'hhmmtt');
                    var returnflight_departureDateTime = ConvertStringToDateTime(returnflight[0].flight[0].departureDateTime, '');
                    if (onwardflight_arrivalDateTime >= returnflight_departureDateTime) {
                        notification(('Your ' + origin + ' to ' + destination + ' flight reaches at ' + onwardflight_arrivalTime + '.\n\nPlease select a Return flight after ' + onwardflight_arrivalTime + '.'), null);
                    }
                    else {
                        $(_this).addClass('selected');

                        var flight = returnflight[0].flight[0];
                        var flightcount = returnflight[0].flight.length;
                        var totalduration = TotalFlightDuration(1, returnflight[0].flight);

                        $("#return_flight_more_info").html(cabinClassName + ' | ' + StopsInfoFromCount(flightcount));
                        $("img#return_airlinecode").prop('src', webPath + '/Files/AirlineLogos/' + returnflight[0].flight[0].airlineCode + '.gif');
                        $("#return_flight_code").html(returnflight[0].flight[0].airlineCode + ' - ' + returnflight[0].flight[0].flightNumber);

                        //$("#return_departure_time").html(ConvertStringToDateTime(returnflight[0].flight[0].departureDateTime, 'hhmmtt'));
                        //$("#return_departure_date").html(ConvertStringToDateTime(returnflight[0].flight[0].departureDateTime, 'ddd, DD MMM'));
                        //$("#return_duration").html(totalduration);
                        //$("#return_arrival_time").html(ConvertStringToDateTime(returnflight[0].flight[flightcount - 1].arrivalDateTime, 'hhmmtt'));
                        //$("#return_arrival_date").html(ConvertStringToDateTime(returnflight[0].flight[flightcount - 1].arrivalDateTime, 'ddd, DD MMM'));

                        $("#return_stop_details").html(StopOverDetails(returnflight[0].flight));
                        $("#return_baggage").html(returnflight[0].rule.baggage == "" ? "N.a." : returnflight[0].rule.baggage + ' kg');
                        $("#return_total_fare span").html(parseFloat(returnflight[0].totalFare).toLocaleString('en-IN'));
                        var cashbackAmt = parseFloat($(_this).data('cb-amount'));
                        if (cashbackAmt > 0) {
                            $("#return_cashback_code").html('Cashback applied (Code - ' + $(_this).data('cb-code') + ')');
                            $("#return_cashback span").html(cashbackAmt.toLocaleString('en-IN'));
                            $("#return_cashback_description").html($(_this).data('cb-description'));
                            $("#divreturncashback").show();
                            $("#total_cashback_amount").html((parseFloat($("#total_cashback_amount").html()) + cashbackAmt).toLocaleString('en-IN'));
                            $("#divtotalcashback").show();
                        }
                        var adultBaseFare = (parseFloat(onwardflight[0].adultBaseFare) * adult) + (parseFloat(returnflight[0].adultBaseFare) * adult);
                        var childBaseFare = (parseFloat(onwardflight[0].childBaseFare) * child) + (parseFloat(returnflight[0].childBaseFare) * child);
                        var infantBaseFare = (parseFloat(onwardflight[0].infantBaseFare) * infant) + (parseFloat(returnflight[0].infantBaseFare) * infant);
                        var feesSurcharges = parseFloat(onwardflight[0].otherTax) + parseFloat(onwardflight[0].serviceTax) + parseFloat(onwardflight[0].tds) + parseFloat(onwardflight[0].yq) + parseFloat(returnflight[0].otherTax) + parseFloat(returnflight[0].serviceTax) + parseFloat(returnflight[0].tds) + parseFloat(returnflight[0].yq);
                        var total_charges = parseFloat(onwardflight[0].totalFare) + parseFloat(onwardflight[0].serviceTax) + parseFloat(onwardflight[0].tds) + parseFloat(returnflight[0].totalFare) + parseFloat(returnflight[0].serviceTax) + parseFloat(returnflight[0].tds);
                        var bal = 0;
                        //console.log('adultBaseFare - ' + adultBaseFare);
                        //console.log('childBaseFare - ' + childBaseFare);
                        //console.log('infantBaseFare - ' + infantBaseFare);
                        //console.log('feesSurcharges - ' + feesSurcharges);
                        //console.log('total_charges - ' + total_charges);
                        //console.log('(adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges) - ' + (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges));
                        if (total_charges > (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges)) {
                            bal = total_charges - adultBaseFare - childBaseFare - infantBaseFare - feesSurcharges;
                            adultBaseFare = adultBaseFare + bal;
                        }
                        else {
                            bal = (adultBaseFare + childBaseFare + infantBaseFare + feesSurcharges) - total_charges;
                            adultBaseFare = adultBaseFare - bal;
                        }

                        $("#adult_base_fare").html(adultBaseFare.toLocaleString('en-IN'));
                        $("#child_base_fare").html(childBaseFare.toLocaleString('en-IN'));
                        $("#infant_base_fare").html(infantBaseFare.toLocaleString('en-IN'));
                        $("#fees_surcharges").html(feesSurcharges.toLocaleString('en-IN'));

                        $("#return_fare_type").html('<span class="text-' + (returnflight[0].rule.refundable == 'yes' ? 'success">Refundable' : 'danger">Non-Refundable') + '</span>');
                        var reschedule_charge = 'Not Available';
                        if (returnflight[0].rule.rescheduleCharge != null) {
                            reschedule_charge = returnflight[0].rule.rescheduleCharge;
                            if (reschedule_charge != 'Not Available') {
                                reschedule_charge = '<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(reschedule_charge).toLocaleString('en-IN');
                            }
                        }
                        $("#return_reschedule_charges").html(reschedule_charge);
                        var cancFee = returnflight[0].rule.cancellationFee == null ? '0' : returnflight[0].rule.cancellationFee;
                        var cancNote = '';
                        if (isNaN(cancFee)) {
                            cancNote = cancFee;
                            $("#return_cancellation_charges").html('N.a.');
                        }
                        else {
                            $("#return_cancellation_charges").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(cancFee).toLocaleString('en-IN'));
                        }
                        var cancellation_slab = '-';
                        if (returnflight[0].rule.cancellationSlab != null) {
                            cancellation_slab = '<ul style="margin-left:15px"><li>' + returnflight[0].rule.cancellationSlab.replaceAll('#', '</li><li>');
                            cancellation_slab = cancellation_slab.substring(0, cancellation_slab.length - 5) + '</ul>';
                        }
                        if (cancNote != '') {
                            if (cancellation_slab == '-')
                                cancellation_slab = cancNote;
                            else
                                cancellation_slab = cancNote + '<br /><br />' + cancellation_slab;
                        }
                        $("#return_cancellation_slab").html(cancellation_slab);
                        var return_flight_remarks = '-';
                        if (returnflight[0].rule.remarks != null)
                            return_flight_remarks = returnflight[0].rule.remarks;
                        $("#return_remarks").html(return_flight_remarks);

                        $("h3.payment-amount span").html(total_charges.toLocaleString('en-IN'));
                        $("#total_charges").html($("h3.payment-amount span").html());

                        $('#return_flight_list').fadeOut('fast', function (e) {
                            $('#flight_booking').fadeIn();
                        });

                        localStorage.setItem("returnflight", JSON.stringify(returnflight));

                        if ($(_this).data('cb-code') != '') {
                            localStorage.setItem("return-cb-code", $(_this).data('cb-code'));
                            localStorage.setItem("return-cb-description", $(_this).data('cb-description'));
                            localStorage.setItem("return-cb-fare", returnflight[0].totalFare);
                            localStorage.setItem("return-cb-amount", $(_this).data('cb-amount'));
                        }

                        //console.log(returnflight);
                    }
                }
                else {
                    bootalert("Oops! Return flight information not found.", 'error', function (e) {
                        window.location = virtualPath + "Account/FlightBooking";
                    });
                }
            }
        } catch (e) {
            alertException(e, current_page_id, 'a.select-return.click');
        }
    });

    $("a.goto-onward").on('click', function (e) {
        try {
            $('#return_flight_list').fadeOut('fast', function (e) {
                $('#onward_flight_list').fadeIn();
            });
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-onward.click');
        }
    });

    $("a.edit-onward").on('click', function (e) {
        try {
            $('#flight_booking').fadeOut('fast', function (e) {
                $('#onward_flight_list').fadeIn();
            });
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-onward.click');
        }
    });

    $("a.edit-return").on('click', function (e) {
        try {
            $('#flight_booking').fadeOut('fast', function (e) {
                $('#return_flight_list').fadeIn();
            });
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-onward.click');
        }
    });

    $("#onward_flight_list_header a.airlinecode").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_flight_list_header a.departuretime").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.duration").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_flight_list", 'airlinecode', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_flight_list", 'airlinecode', 'asc', 'text');
                else
                    SortList("#ul_onward_flight_list", 'airlinecode', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_flight_list_header a.airlinecode.click');
        }
    });

    $("#onward_flight_list_header a.departuretime").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.duration").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_flight_list", 'departuredatetime', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_flight_list", 'departuredatetime', 'asc', 'text');
                else
                    SortList("#ul_onward_flight_list", 'departuredatetime', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_flight_list_header a.departuretime.click');
        }
    });

    $("#onward_flight_list_header a.duration").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.departuretime").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_flight_list", 'duration', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_flight_list", 'duration', 'asc', 'text');
                else
                    SortList("#ul_onward_flight_list", 'duration', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_flight_list_header a.duration.click');
        }
    });

    $("#onward_flight_list_header a.totalfare").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.departuretime").find('i').css('opacity', 0);
                $("#onward_flight_list_header a.duration").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_flight_list", 'totalfare', 'asc', 'number');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_flight_list", 'totalfare', 'asc', 'number');
                else
                    SortList("#ul_onward_flight_list", 'totalfare', 'desc', 'number');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_flight_list_header a.totalfare.click');
        }
    });

    $("#return_flight_list_header a.airlinecode").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#return_flight_list_header a.arrivaltime").find('i').css('opacity', 0);
                $("#return_flight_list_header a.duration").find('i').css('opacity', 0);
                $("#return_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_return_flight_list", 'airlinecode', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_return_flight_list", 'airlinecode', 'asc', 'text');
                else
                    SortList("#ul_return_flight_list", 'airlinecode', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#return_flight_list_header a.airlinecode.click');
        }
    });

    $("#return_flight_list_header a.arrivaltime").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#return_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#return_flight_list_header a.duration").find('i').css('opacity', 0);
                $("#return_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_return_flight_list", 'arrivaldatetime', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_return_flight_list", 'arrivaldatetime', 'asc', 'text');
                else
                    SortList("#ul_return_flight_list", 'arrivaldatetime', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#return_flight_list_header a.arrivaltime.click');
        }
    });

    $("#return_flight_list_header a.duration").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#return_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#return_flight_list_header a.arrivaltime").find('i').css('opacity', 0);
                $("#return_flight_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_return_flight_list", 'duration', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_return_flight_list", 'duration', 'asc', 'text');
                else
                    SortList("#ul_return_flight_list", 'duration', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#return_flight_list_header a.duration.click');
        }
    });

    $("#return_flight_list_header a.totalfare").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#return_flight_list_header a.airlinecode").find('i').css('opacity', 0);
                $("#return_flight_list_header a.arrivaltime").find('i').css('opacity', 0);
                $("#return_flight_list_header a.duration").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_return_flight_list", 'totalfare', 'asc', 'number');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_return_flight_list", 'totalfare', 'asc', 'number');
                else
                    SortList("#ul_return_flight_list", 'totalfare', 'desc', 'number');
            }
        } catch (e) {
            alertException(e, current_page_id, '#return_flight_list_header a.totalfare.click');
        }
    });

    $('form#frmflightsearchresults').on('submit', function (e) {
        try {
            e.preventDefault();
            var isvalidate = $(this).valid();
            if (isvalidate) {
                localStorage.setItem("flighttotalfare", $("#total_charges").html().replaceAll(',', ''));

                window.location = virtualPath + "Account/FlightMakePayment";
            }
        } catch (e) {
            alertException(e, current_page_id, 'frmflightsearchresults.submit');
        }

    });
});

