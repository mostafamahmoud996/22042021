﻿current_page_id = "#busmakepayment";
var originId = '', originCity = '', destinationId = '', destinationCity = '', departDate = '', departDateLabel = '', mode = '', airline = '', totalFare = 0, taxAmount = 0, totalAmount = 0, no_of_seats = 0, totaltravellers = 0, bus_totalfare = 0, bus_seats = '', bus_seats_info = '', bus_cashbackcode = '', taxes = '', bus_cashbackamount = 0, profileflagMPB = false, bus_boarding_point_id = 0, bus_boarding_point = '', bus_type = '', bus_typeId = 0, trip_id = 0;

function BusPrerequisities() {
    try {
        var cmp = {};
        cmp.s = "";
        $.ajax({
            type: 'POST',
            url: apiPath + "/BusDeductionPrerequisities",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(cmp),
            dataType: 'json',
            jsonp: null,
            success: function (data, textStatus, xhr) {
                try {
                    var r = StringDecryption(data);
                    //console.log(r);
                    arr = [];
                    arr = r.split("|");

                    if (arr[0] != '') {
                        dedhead_list_json = JSON.parse(arr[0]);
                    }

                    if (arr[1] != '') {
                        deduction_list_json = JSON.parse(arr[1]);
                    }

                    //console.log('dedhead_list_json---------------------------------------------------------');
                    //console.log(JSON.stringify(dedhead_list_json));
                    //console.log('deduction_list_json---------------------------------------------------------');
                    //console.log(deduction_list_json);

                    var taxList = $.grep(deduction_list_json, function (e) { return e.Value > 0; });
                    //console.log(taxList);
                    if (taxList.length > 0) {
                        var taxdet = '', taxamt = 0, taxtooltip = '';
                        $.each(taxList, function (i, v) {
                            taxdet = '', tax = 0, taxamt = 0;
                            console.log(v);
                            if (v.IsAmount) {
                                taxamt = parseFloat(v.Value);
                            }
                            else {
                                taxamt = parseFloat(bus_totalfare) * parseFloat(v.Value) / 100;
                            }

                            taxamt = parseFloat(taxamt.toFixed(2));
                            taxAmount += taxamt;
                            taxdet = v.Id + "~" + v.TypeId + "~" + v.OperatorType + "~" + v.OperatorId + "~" + v.Operator + "~" + v.OperatorCode + "~" + v.Value;

                            if (taxes == '') {
                                taxes = taxdet;
                            }
                            else {
                                taxes = taxes + "#" + taxdet;
                            }
                            taxtooltip += (taxtooltip == '' ? "" : "<br />") + v.Charge + (v.IsAmount ? "" : " (" + v.Value + "%)") + ": Rs." + taxamt + '/-';
                        });

                        //$("span.tax-tooltip").prop('title', taxtooltip);
                        //$("span.tax-tooltip").removeProp('data-original-title');
                        //$('[data-toggle="tooltip"]').tooltip({
                        //    title: function () {
                        //        return $(this).prop('title');
                        //    }
                        //});

                        //console.log(taxAmount);
                        //console.log(taxtooltip);

                        $("span.tax-tooltip").html(taxtooltip);

                        $("#divtaxamountMPB").show();
                        $("#hdntaxamountMPB").val(taxAmount);
                        $("#taxamountMPB span").html('<i class="fa fa-rupee"></i>&nbsp;' + taxAmount.toString());

                        totalAmount = parseFloat(bus_totalfare) + parseFloat(taxAmount);
                        totalAmount = parseFloat(totalAmount).toFixed(2);

                        $("#hdnamountMPB").val(totalAmount);
                        $("#amountMPB span").html('<i class="fa fa-rupee"></i>&nbsp;' + totalAmount.toString());
                    }
                    GetWalletBalance();
                } catch (e) {
                    alertException(e, 'app.init', 'FlightPrerequisities.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            }
        });
    } catch (e) {
        //alertException(e, 'app.init', 'RechargePrerequisities');
        LogException(init_current_page_id + ' => ' + e.message);
    }
}

function BalanceAmountToPay() {
    var amt = parseFloat($("#hdnamountMPB").val());
    var ewbal = parseFloat($("#hdnwalletbalanceMPB").val());
    var grplbal = parseFloat($("#hdngrplmaxcashamountMPB").val());
    var bal = 0;

    if ($("#usewalletMPB").prop('checked')) {
        bal = (ewbal >= amt) ? 0 : (amt - ewbal);
    }
    else {
        bal = amt;
    }

    if ($("#usegrplcashMPB").prop('checked')) {
        if (bal == 0) {
            notification('Total Fare is payable using Wallet.\nUncheck Wallet to use GRPL Cash as payment mode.', function () {
                $("#usegrplcashMPB").prop('checked', false);
            });
        }
        bal = (grplbal >= bal) ? 0 : (bal - grplbal);
    }
    else {
        bal = bal;
    }
    //console.log('bal - ' + bal);
    $("#balanceamtMPB span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(bal).toLocaleString('en-IN'));
    $("#hdnbalanceamtMPB").val(bal);
    $("button.make-payment span").html('Book Now');

    if (bal < 0) {
        alertme('Total Fare cannot be less than 0 (zero)');
        $("button.make-payment").prop('disabled', true);
    }
    if (bal > 0) {
        $("#divbalamtMPB").slideDown('fast');
        $("button.make-payment").prop('disabled', false);
        $("button.make-payment span").html('Proceed for Payment');
    }
    else {
        $("#divbalamtMPB").slideUp('fast');
    }
}

function ParameterString() {
    var p = "";
    p = originCity;
    p += "|" + destinationCity;
    p += "|" + departDate;
    p += "|" + totaltravellers;
    p += "|" + bus_seats;
    p += "|" + bus_boarding_point_id;
    p += "|" + bus_boarding_point;
    p += "|" + bus_typeId;
    p += "|" + bus_type;
    p += "|" + $("#totalfareMPB").val();
    p += "|" + taxes;
    p += "|" + taxAmount.toString();
    p += "|" + $("#hdnamountMPB").val();
    p += "|" + ($("#usewalletMPB").prop('checked') == true ? 1 : 0);
    p += "|" + LOGGED_IN_MOBILENO;
    p += "|" + LOGGED_IN_EMAIL;
    p += "|" + $("#cbCodeMPB").val();
    p += "|" + $("#cbAmountMPB").val();
    p += "|" + ($("#usegrplcashMPB").prop('checked') == true ? 1 : 0);
    p += "|" + trip_id;
    p += "|" + originId;
    p += "|" + destinationId;

    return p;
}

function busBookingCompletion(status, txnid) {
    localStorage.setItem("busbookingstatus", status);
    localStorage.setItem("bustransactionid", txnid);
    window.location = virtualPath + "Account/BusBookingStatus";
}

function PostWalletBalance(r) {
    var arr = r.split('|');
    var amt = parseFloat($("#hdnamountMPB").val());
    //notification(r);
    var ewbal = parseFloat(arr[0]);
    $("#hdnwalletbalanceMPB").val(ewbal);
    $("#walletbalanceMPB span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat($("#hdnwalletbalanceMPB").val()).toLocaleString('en-IN'));

    if (ewbal > 0) {
        $("#usewalletMPB").prop('checked', true);
        $("#usewalletMPB").prop("disabled", false);
    }
    else {
        $("#usewalletMPB").prop('checked', false);
        $("#usewalletMPB").prop("disabled", true);
    }

    var grplbal = parseFloat(arr[1]);
    $("#hdngrplcashbalanceMPB").val(grplbal);
    $("#grplcashbalanceMPB span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat($("#hdngrplcashbalanceMPB").val()).toLocaleString('en-IN'));

    var grplpercent = parseFloat(arr[2]);
    var grplmaxamt = 0;
    if (grplpercent > 0) {
        grplmaxamt = Math.floor(amt * grplpercent / 100);
        grplmaxamt = grplmaxamt > grplbal ? grplbal : grplmaxamt;
        $("#hdngrplmaxcashamountMPB").val(grplmaxamt);
        $("#spangrplmaxcashamountMPB").html(grplmaxamt);
    }

    if (grplmaxamt > 0) {
        if ((ewbal >= amt ? 0 : amt - ewbal) > 0)
            $("#usegrplcashMPB").prop('checked', true);
        $("#usegrplcashMPB").prop("disabled", false);
    }
    else {
        $("#usegrplcashMPB").prop('checked', false);
        $("#usegrplcashMPB").prop("disabled", true);
    }

    BalanceAmountToPay();

    if (pgresponse == '') {
        $("#formLoadingMPB").fadeOut('fast', function (e) {
            $("#formMPB").fadeIn();
        });
    }
}
function GetWalletBalance() {
    var btntext = $("button.make-payment span").html();
    var ewp = {};
    $.ajax({
        type: 'POST',
        url: apiPath + "/Customer/GetWalletBalance",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ewp),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                //console.log(r);
                PostWalletBalance(r);
            } catch (e) {
                alertException(e, current_page_id, 'GetWalletBalance.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

function RevertWalletDeduction(uid) {
    var btntext = $("button.make-payment span").html();
    var xyz = {};
    xyz.s = StringEncryption(uid);
    $.ajax({
        type: 'POST',
        url: apiPath + "/Customer/WalletDeductionReversal",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(xyz),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                PostWalletBalance(r);
            } catch (e) {
                alertException(e, current_page_id, 'RevertWalletDeduction.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

$(document).ready(function (e) {
    try {
        if (localStorage.getItem("bus_FromCityId") != null) {
            originId = localStorage.getItem("bus_FromCityId");
            destinationId = localStorage.getItem("bus_ToCityId");
            originCity = localStorage.getItem("bus_FromCity");
            destinationCity = localStorage.getItem("bus_ToCity");
            departDate = localStorage.getItem("bus_DepartureDate");
            departDateLabel = localStorage.getItem("bus_DepartureDateLabel");
            totaltravellers = localStorage.getItem("bus_NoOfSeats");
            bus_boarding_point = localStorage.getItem("bus_boarding_point");
            bus_boarding_point_id = localStorage.getItem("bus_boarding_point_id");
            bus_totalfare = localStorage.getItem("bus_totalfare");
            bus_seats = localStorage.getItem("bus_seats");
            bus_seats_info = localStorage.getItem("bus_seats_info");
            bus_cashbackcode = localStorage.getItem("bus_cashbackcode");
            bus_cashbackamount = localStorage.getItem("bus_cashbackamount");

            var page_title = originCity + '&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;' + destinationCity + ' (' + departDateLabel + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + ')';
            $(".bus-page-title").html(page_title);
        }


        if (pgresponse != "") {
            var req = [], req_status = '', req_txnid = '', error_message = '';
            //console.log(pgresponse);
            //pgresponse = StringDecryption(pgresponse);
            if (pgresponse.indexOf('|') != -1)
                req = pgresponse.split('|');
            else {
                bootalert('Oops! Unexpected error occurred. Please report it to us.', 'error');
            }

            req_status = req[0];
            if (req.length > 1)
                req_txnid = req[1];
            if (req.length > 2)
                error_message = req[2];

            pgresponse = null;

            if (req_status != '' && req_txnid != '') {
                if (req_status == 'success') {
                    var btntextR = $("button.make-payment span").html();
                    var abc = {};
                    abc.s = req_txnid;
                    //console.log(ewp.s);
                    abc.s = StringEncryption(abc.s);
                    $.ajax({
                        type: 'POST',
                        url: apiPath + "/Customer/GatewayBusBooking",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(abc),
                        dataType: 'json',
                        jsonp: null,
                        beforeSend: function (xhr, opts) {
                            $("button.make-payment").prop('disabled', true);
                            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                        },
                        success: function (data, textStatus, xhr) {
                            try {
                                var r = StringDecryption(data);
                                var arr = [];
                                arr = r.split("|");
                                if (arr[0] == 'success') {
                                    busBookingCompletion(arr[0], arr[1]);
                                }
                            } catch (e) {
                                alertException(e, current_page_id, 'GatewaybusBooking.success');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            RevertWalletDeduction(req_txnid);
                            //OnAjaxError(xhr, textStatus, errorThrown);
                            $("#errorMessageMPB").html(errorThrown);
                            $("#formLoadingMPB").fadeOut('fast', function (e) {
                                $("#formErrorMPB").fadeIn();
                            });
                        },
                        complete: function (a) {
                            $("button.make-payment").prop('disabled', false);
                            $("button.make-payment span").html(btntextR);
                        }
                    });
                }
                else {
                    RevertWalletDeduction(req_txnid);
                    $("#errorMessageMPB").html(error_message);
                    $("#formLoadingMPB").fadeOut('fast', function (e) {
                        $("#formErrorMPB").fadeIn();
                    });
                }
            }
        }
        else {
            BusPrerequisities();
            if (localStorage.getItem("bus_FromCityId") != null) {
                //originId = localStorage.getItem("bus_FromCityId");
                //destinationId = localStorage.getItem("bus_ToCityId");
                //originCity = localStorage.getItem("bus_FromCity");
                //destinationCity = localStorage.getItem("bus_ToCity");
                //departDate = localStorage.getItem("bus_DepartureDate");
                //departDateLabel = localStorage.getItem("bus_DepartureDateLabel");
                //totaltravellers = localStorage.getItem("bus_NoOfSeats");
                //bus_boarding_point = localStorage.getItem("bus_boarding_point");
                //bus_boarding_point_id = localStorage.getItem("bus_boarding_point_id");
                //bus_totalfare = localStorage.getItem("bus_totalfare");
                //bus_seats = localStorage.getItem("bus_seats");
                //bus_seats_info = localStorage.getItem("bus_seats_info");
                //bus_cashbackcode = localStorage.getItem("bus_cashbackcode");
                //bus_cashbackamount = localStorage.getItem("bus_cashbackamount");

                var tripinfo = JSON.parse(StringDecryption(localStorage.getItem("bus_onwardOptions")));
                //console.log(tripinfo);
                trip_id = tripinfo.trip_id;
                bus_typeId = tripinfo.seat_type;
                bus_type = tripinfo.bus_type;
                var departureTime = ConvertStringToDateTime(tripinfo.departure_time, 'hhmmtt');
                var arrivalTime = ConvertStringToDateTime(tripinfo.arrival_time, 'hhmmtt');
                $("#travelname span").html(tripinfo.travels_name);
                $("#bustypename span").html(tripinfo.bus_type);
                $("#seatsbooked span").html(bus_seats.replaceAll(',', ', '));
                $("#boardingpoint span").html(bus_boarding_point);

                if (localStorage.getItem("bus_cashbackcode") != null) {
                    bus_cashbackcode = localStorage.getItem("bus_cashbackcode");
                    bus_cashbackamount = localStorage.getItem("bus_cashbackamount");

                    $("#cbCodeMPB").val(bus_cashbackcode);
                    $("#cbAmountMPB").val(bus_cashbackamount);
                    $("#cashbackMPB span").html('<i class="fa fa-rupee"></i> ' + bus_cashbackamount.toLocaleString('en-IN'));
                    if (bus_cashbackamount > 0)
                        $("#divcashbackMPB").show();
                }

                //var page_title = '<div style="line-height:25px">' + originCity + '&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;' + destinationCity + '</div><div style="line-height:18px;font-size:12px">' + departDateLabel + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + '</div>';
                //$(".bus-page-title").html(page_title);

                $("#bookingemailid").val(LOGGED_IN_EMAIL);
                $("#bookingemailidlabel span").html($("#bookingemailid").val());

                $("#bookingmobileno").val(LOGGED_IN_MOBILENO);
                $("#bookingmobilenolabel span").html($("#bookingmobileno").val());
            }
            if (LOGGED_IN_ID != null) {
                if (LOGGED_IN_NAME == null) {
                    profileflagMPB = true;
                }
                else {
                    if (LOGGED_IN_NAME == '') {
                        profileflagMPB = true;
                    }
                    else {
                        if (LOGGED_IN_EMAIL == null) {
                            profileflagMPB = true;
                        }
                        else {
                            if (LOGGED_IN_EMAIL == '') {
                                profileflagMPB = true;
                            }
                            else {
                                if (LOGGED_IN_MOBILENO == null) {
                                    profileflagMPB = true;
                                }
                                else {
                                    if (LOGGED_IN_MOBILENO == '') {
                                        profileflagMPB = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (profileflagMPB) {
                    //bootalert(profile_update_message);
                    $("#errorMessageMPB").html(profile_update_message);
                    $("#formLoadingMPB").fadeOut('fast', function (e) {
                        $("#formErrorMPB").fadeIn();
                    });
                }
                else {
                    var travellerhtml = '';
                    if (totaltravellers > 0) {
                        for (var i = 0; i < totaltravellers; i++) {
                            travellerhtml += '<div class="panel panel-default">';
                            travellerhtml += '<div class="panel-heading no-padding-top" role="tab" id="traveller_' + (i + 1) + '_heading">';
                            travellerhtml += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#traveller_info" href="#traveller_' + (i + 1) + '_body" aria-expanded="true" aria-controls="traveller_' + (i + 1) + '_body">';
                            travellerhtml += '<h4 class="panel-title text-info">';
                            travellerhtml += 'Traveller ' + (i + 1);
                            travellerhtml += '<i class="fa fa-angle-down"></i>';
                            travellerhtml += '</h4>';
                            travellerhtml += '</a>';
                            travellerhtml += '</div>';
                            travellerhtml += '<div id="traveller_' + (i + 1) + '_body" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="traveller_' + (i + 1) + '_heading" aria-expanded="true">';
                            travellerhtml += '<div class="panel-body">';
                            travellerhtml += '<div class="form-group no-padding no-margin toggle-switch">';
                            travellerhtml += '<label>';
                            travellerhtml += '<input id="traveller_mr_' + (i + 1) + '" type="radio" value="Mr." name="traveller_gender_' + (i + 1) + '" class="traveller_gender" />';
                            travellerhtml += '<span class="toggle__text">Mr</span>';
                            travellerhtml += '</label>';
                            travellerhtml += '<label>';
                            travellerhtml += '<input id="traveller_ms_' + (i + 1) + '" type="radio" value="Ms." name="traveller_gender_' + (i + 1) + '" class="traveller_gender" />';
                            travellerhtml += '<span class="toggle__text">Ms</span>';
                            travellerhtml += '</label>';
                            travellerhtml += '<label>';
                            travellerhtml += '<input id="traveller_mrs_' + (i + 1) + '" type="radio" value="Mrs." name="traveller_gender_' + (i + 1) + '" class="traveller_gender" />';
                            travellerhtml += '<span class="toggle__text">Mrs</span>';
                            travellerhtml += '</label>';
                            travellerhtml += '</div>';
                            travellerhtml += '<div style="height:12px"></div>';
                            travellerhtml += '<div class="form-group label-floating">';
                            travellerhtml += '<label for="traveller_firstname_' + (i + 1) + '" class="control-label" style="left:0">First Name</label>';
                            travellerhtml += '<input type="text" id="traveller_firstname_' + (i + 1) + '" name="traveller_firstname_' + (i + 1) + '" class="form-control traveller_firstname" maxlength="30">';
                            travellerhtml += '</div>';
                            travellerhtml += '<div class="form-group label-floating">';
                            travellerhtml += '<label for="traveller_lastname_' + (i + 1) + '" class="control-label" style="left:0">Last Name</label>';
                            travellerhtml += '<input type="text" id="traveller_lastname_' + (i + 1) + '" name="traveller_lastname_' + (i + 1) + '" class="form-control traveller_lastname" maxlength="30">';
                            travellerhtml += '</div>';
                            travellerhtml += '<div class="form-group label-floating">';
                            travellerhtml += '<label for="traveller_age_' + (i + 1) + '" class="control-label" style="left:0">Age</label>';
                            travellerhtml += '<input type="text" id="traveller_age_' + (i + 1) + '" name="traveller_age_' + (i + 1) + '" class="form-control traveller_age" maxlength="30">';
                            travellerhtml += '</div>';
                            travellerhtml += '</div>';
                            travellerhtml += '</div>';
                            travellerhtml += '</div>';
                        }
                    }
                    $("#traveller_info").append($.parseHTML(travellerhtml));

                    if (LOGGED_IN_GENDER != null) {
                        if (LOGGED_IN_GENDER == 'Mr') {
                            $("#traveller_mr_1").prop('checked', true);
                        }
                        if (LOGGED_IN_GENDER == 'Ms') {
                            $("#traveller_ms_1").prop('checked', true);
                        }
                    }
                    if (LOGGED_IN_FIRSTNAME != null)
                        $("#traveller_firstname_1").val(LOGGED_IN_FIRSTNAME);

                    if (LOGGED_IN_LASTNAME != null)
                        $("#traveller_lastname_1").val(LOGGED_IN_LASTNAME);

                    if (localStorage.getItem("bus_totalfare") != null) {
                        $("#FromCityIdMPB").val(originId);
                        $("#ToCityIdMPB").val(destinationId);
                        $("#DepartureDateMPB").val(departDate);
                        $("#totaltravellersMPB").val(totaltravellers);
                        $("#onwardbusMPB").val(localStorage.getItem("bus_onwardOptions"));
                        $("#totalfareMPB").val(bus_totalfare);
                        $("#labelTotalfareMPB span").html('<i class="fa fa-rupee"></i> ' + parseFloat($("#totalfareMPB").val()).toLocaleString('en-IN'));
                        GetWalletBalance();
                        jQuery.validator.addClassRules({
                            traveller_gender: { required: true },
                            traveller_firstname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return true;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            traveller_lastname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return true;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            traveller_age: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return true;
                                    }
                                },
                                regex: /^[0-9]+$/,
                                min: 1,
                                max: 120,
                                //rangelength: [10, 10]
                            },
                        });

                        $('form#formMPB').validate({
                            rules: {
                                FromCityIdMPB: { required: true },
                                ToCityIdMPB: { required: true },
                                DepartureDateMPB: { required: true },
                                totaltravellersMPB: { required: true },
                                onwardbusMPB: { required: true },
                                totalfareMPB: { required: true },
                            },
                            messages: {
                                FromCityIdMPB: { required: "Departure city not selected" },
                                ToCityIdMPB: { required: "Arrival city not selected" },
                                DepartureDateMPB: { required: "Departure date not selected" },
                                totaltravellersMPB: { required: "Traveller not selected" },
                                onwardbusMPB: { required: "Departure bus not selected" },
                                totalfareMPB: { required: "Total fare not available" },
                            }
                        });
                    }
                    else {
                        $("#errorMessageMPB").html('Oops! Something went wrong. Go back and try again.');
                        $("#formLoadingMPB").fadeOut('fast', function (e) {
                            $("#formErrorMPB").fadeIn();
                        });
                    }
                }
            }
            else {
                $("#errorMessageMPB").html('Sign In and try again.');
                $("#formLoadingMPB").fadeOut('fast', function (e) {
                    $("#formErrorMPB").fadeIn();
                });
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'PageLoad');
    }

    $("#usewalletMPB").on('click', function (e) {
        try {
            var amt = parseFloat($("#hdnamountMPB").val());
            var ewbal = parseFloat($("#hdnwalletbalanceMPB").val());
            var bal = 0;

            if ($(this).prop('checked')) {
                bal = (ewbal >= amt) ? 0 : (amt - ewbal);
                if (bal == 0)
                    $("#usegrplcashMPB").prop('checked', false);
            }
            else {
                bal = amt;
            }
            BalanceAmountToPay();
        } catch (e) {
            alertException(e, current_page_id, 'usewalletMPB.click');
        }
    });

    $("#usegrplcashMPB").on('click', function (e) {
        try {
            BalanceAmountToPay();
        } catch (e) {
            alertException(e, current_page_id, 'usegrplcashMPB.click');
        }
    });

    $("a.goto-busbooking").on('click', function (e) {
        try {
            window.location = virtualPath + "Account/BusBooking";
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-busbooking.click');
        }
    });


    $("a.retry-make-payment").on('click', function (e) {
        try {
            $("form#formMPB").submit();
        } catch (e) {
            alertException(e, current_page_id, 'a.retry-make-payment.click');
        }
    });

    $('form#formMPB').on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var pmd = "";
                if ($("#usewalletMPB").prop("checked")) {
                    pmd += "EW";
                }

                if ($("#usegrplcashMPB").prop("checked")) {
                    pmd += (pmd == '' ? '' : ',') + "GC";
                }

                if (parseFloat($("#hdnbalanceamtMPB").val()) > 0) {
                    pmd += (pmd == '' ? '' : ',') + "CC";
                }

                if (profileflagMPB) {
                    bootalert(profile_update_message);
                    return false;
                }

                var travellerinfo = '';
                if (totaltravellers > 0) {
                    for (var i = 0; i < totaltravellers; i++) {
                        var gender = $("#traveller_mr_" + (i + 1)).prop('checked') ? $("#traveller_mr_" + (i + 1)).val() : ($("#traveller_ms_" + (i + 1)).prop('checked') ? $("#traveller_ms_" + (i + 1)).val() : $("#traveller_mrs_" + (i + 1)).val());
                        var firstname = $("#traveller_firstname_" + (i + 1)).val().trim();
                        var lastname = $("#traveller_lastname_" + (i + 1)).val().trim();
                        var age = $("#traveller_age_" + (i + 1)).val().trim();
                        travellerinfo = travellerinfo + (travellerinfo == '' ? '' : '~') + gender + '#' + firstname + '#' + lastname + '#' + age;
                    }
                    //console.log(travellerinfo);
                }

                var btntext = $("button.make-payment span").html();
                var paramList = {};
                paramList.s = pmd + "|" + ParameterString();
                paramList.o = StringDecryption($("#onwardbusMPB").val().trim());
                paramList.r = bus_seats_info;
                paramList.a = travellerinfo;
                var s = paramList.s;
                //console.log(JSON.stringify(paramList));
                //return false;
                paramList.s = StringEncryption(paramList.s);
                $.ajax({
                    type: 'POST',
                    url: apiPath + "/Customer/InterimBusBooking",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(paramList),
                    dataType: 'json',
                    jsonp: null,
                    beforeSend: function (xhr, opts) {
                        $("button.make-payment").prop('disabled', true);
                        $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                    },
                    success: function (data, textStatus, xhr) {
                        try {
                            var r = StringDecryption(data);
                            var arrR = r.split("|");
                            var tempStatus = arrR[0];
                            var tempReqId = arrR[1];
                            if (tempStatus == 'success') {
                                busBookingCompletion(tempStatus, tempReqId);
                            }
                            else if (tempStatus == 'inprocess') {
                                $("button.make-payment span").html('Redirecting...');
                                s = 'B' + "|" + tempReqId + "|" + LOGGED_IN_NAME + "|" + LOGGED_IN_EMAIL + "|" + LOGGED_IN_MOBILENO + "|" + LOGGED_IN_ID + "|" + s;
                                s = StringEncryption(s);
                                var uW = webPath + '/Customer/PaymentInitiate?s=' + s + '&p=' + StringEncryption(virtualPath + 'Account/BusMakePayment');
                                window.location = uW;
                            }
                        } catch (e) {
                            //console.log(e);
                            alertException(e, current_page_id, 'InterimbusBooking.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $("button.make-payment").prop('disabled', false);
                        $("button.make-payment span").html(btntext);
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                    complete: function (a) {
                    }
                });
                e.preventDefault();
            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, 'form#formMPB.submit');
        }

    });

    $(document).on('click', '#formMPB a.close-popup-over-list', function (e) {
        try {
            e.preventDefault();
            $('#formMPB .popup-over-list').removeClass('slideInUp');
            $('#formMPB .popup-over-list').addClass('slideOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                $('#formMPB .popup-over-list').hide();
            });
        } catch (e) {
            alertException(e, current_page_id, '#formMPB a.close-popup-over-list.click');
        }
    });


    $("a.goto-bus-search").on('click', function (e) {
        window.location = virtualPath + "Account/BusReview";
    });
    $("a.goto-refer-and-earn").on('click', function (e) {
        window.location = virtualPath + "Account/ReferAndEarn";
    });

});