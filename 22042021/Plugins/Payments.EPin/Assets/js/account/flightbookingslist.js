﻿current_page_id = "#flightbookingslist";
var numrows = 10;

function GetMFBTransactionList(i, s, e, o, b) {
    try {
        var btntext = $(b).html();
        var ewp = {};
        ewp.s = i + "|" + s + "|" + e;
        //console.log(ewp.s);
        ewp.s = StringEncryption(ewp.s);
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/FlightBookingList",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $(b).prop('disabled', true);
                $(b).html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;loading...');
            },
            success: function (data, textStatus, xhr) {
                try {
                    data = JSON.parse(StringDecryption(data));
                    if (data.length > 0) {
                        //console.log(data);
                        var dt = '', dto = '', dtc = '';
                        if (data.length < e)
                            $(b).hide();
                        $.each(data, function (i, v) {
                            var status = (v.BookingStatus == 1 ? '<span class="text-success">&nbsp;Booked</span>' : (v.BookingStatus == 2 ? '<span class="text-warning">&nbsp;Pending</span>' : (v.BookingStatus == 3 ? '<span class="text-danger">&nbsp;Failed</span>' : (v.BookingStatus == 4 ? '<span class="text-danger">&nbsp;Cancelled</span>' : '&nbsp;Abandoned'))));
                            var onwardflight, returnflight, onwardflightcount, returnflightcount, onwarddepflight, onwardarrflight, returndepflight, returnarrflight, onwardtotalduration, returntotalduration
                            if (v.FlightTypeId == 1) {
                                onwardflight = JSON.parse(v.OnwardFlight.substring(1, v.OnwardFlight.length - 1)).flight;
                                returnflight = (v.ReturnFlight != null && v.ReturnFlight != '') ? JSON.parse(v.ReturnFlight.substring(1, v.ReturnFlight.length - 1)).flight : null;
                                onwardflightcount = onwardflight.length;
                                returnflightcount = returnflight != null ? returnflight.length : 0;
                                onwarddepflight = onwardflight[0];
                                onwardarrflight = onwardflight[onwardflightcount - 1];
                                returndepflight = returnflight != null ? returnflight[0] : null;
                                returnarrflight = returnflight != null ? returnflight[returnflightcount - 1] : null;
                                onwardtotalduration = TotalFlightDuration(v.FlightTypeId, onwardflight);
                                returntotalduration = returnflight == null ? '-' : TotalFlightDuration(v.FlightTypeId, returnflight);
                            }
                            else {
                                onwardflight = JSON.parse(v.OnwardFlight.substring(1, v.OnwardFlight.length - 1)).onward;
                                returnflight = JSON.parse(v.OnwardFlight.substring(1, v.OnwardFlight.length - 1)).return;
                                onwardflightcount = onwardflight.length;
                                returnflightcount = returnflight.length;
                                onwarddepflight = onwardflight[0];
                                onwardarrflight = onwardflight[onwardflightcount - 1];
                                returndepflight = returnflight.length > 0 ? returnflight[0] : null;
                                returnarrflight = returnflight.length > 0 ? returnflight[returnflightcount - 1] : null;
                                onwardtotalduration = TotalFlightDuration(v.FlightTypeId, onwardflight);
                                returntotalduration = returnflight.length > 0 ? TotalFlightDuration(v.FlightTypeId, returnflight) : '-';
                            }
                            var dvhtml = '';
                            dvhtml += '<div class="col-sm-4 text-sm">' + status + '</div>';
                            dvhtml += '<div class="col-sm-4 text-center text-sm text-danger">' + v.UniqueId + '</div>';
                            dvhtml += '<div class="col-sm-4 text-right text-sm">' + ConvertDateFromJson(v.ApplicationDtm, '') + '</div>';
                            dvhtml += '<div class="clearfix"></div>';
                            dvhtml += '<div class="position-relative">';
                            dvhtml += '<small class="center-block text-center text-light-gray">' + StopsInfoFromCount(onwardflightcount) + '</small>';
                            dvhtml += '<small class="center-block text-center text-light-gray position-absolute" style="left:0;right:0;margin:auto;top:45px">' + onwardtotalduration + '</small>';
                            dvhtml += '<div class="pull-left" style="width:22.5%">';
                            dvhtml += '<h4 class="no-margin center-block bold text-left">' + v.Origin + '</h4>';
                            dvhtml += '<small class="center-block text-light-gray text-left">' + ConvertStringToDateTime(onwarddepflight.departureDateTime, 'ddd, DD MMM') + '</small>';
                            dvhtml += '<small class="center-block text-light-gray text-left">' + ConvertStringToDateTime(onwarddepflight.departureDateTime, 'hhmmtt') + '</small>';
                            dvhtml += '</div>';
                            dvhtml += '<div class="pull-left" style="width:55%">';
                            dvhtml += '<div class="pull-left" style="width:20px"><i class="fa fa-plane" style="transform:rotate(45deg);line-height:28px;font-size:1.5em"></i></div>';
                            dvhtml += '<div class="pull-left" style="width:calc(100% - 20px);height:28px">';
                            dvhtml += '<hr style="vertical-align:middle;background:#aaa;margin:0;margin-top:11px;width:95%;margin-left:2.5%;height:1px" />';
                            dvhtml += '</div>';
                            dvhtml += '<div class="clearfix"></div>';
                            dvhtml += '</div>';
                            dvhtml += '<div class="pull-right" style="width:22.5%">';
                            dvhtml += '<h4 class="no-margin center-block bold text-right">' + v.Destination + '</h4>';
                            dvhtml += '<small class="center-block text-light-gray text-right">' + ConvertStringToDateTime(onwardarrflight.arrivalDateTime, 'ddd, DD MMM') + '</small>';
                            dvhtml += '<small class="center-block text-light-gray text-right">' + ConvertStringToDateTime(onwardarrflight.arrivalDateTime, 'hhmmtt') + '</small>';
                            dvhtml += '</div>';
                            dvhtml += '<div class="clearfix"></div>';
                            dvhtml += '</div>';

                            if (returnflightcount > 0) {
                                dvhtml += '<div class="position-relative">';
                                dvhtml += '<small class="center-block text-center text-light-gray">' + StopsInfoFromCount(returnflightcount) + '</small>';
                                dvhtml += '<small class="center-block text-center text-light-gray position-absolute" style="left:0;right:0;margin:auto;top:45px">' + returntotalduration + '</small>';
                                dvhtml += '<div class="pull-left" style="width:22.5%">';
                                dvhtml += '<h4 class="no-margin center-block bold text-left">' + v.Destination + '</h4>';
                                if (returnarrflight != null) {
                                    dvhtml += '<small class="center-block text-light-gray text-left">' + ConvertStringToDateTime(returnarrflight.arrivalDateTime, 'ddd, DD MMM') + '</small>';
                                    dvhtml += '<small class="center-block text-light-gray text-left">' + ConvertStringToDateTime(returnarrflight.arrivalDateTime, 'hhmmtt') + '</small>';
                                }
                                dvhtml += '</div>';
                                dvhtml += '<div class="pull-left" style="width:55%">';
                                dvhtml += '<div class="pull-left" style="width:20px"><i class="fa fa-plane" style="transform:rotate(45deg);line-height:28px;font-size:1.5em"></i></div>';
                                dvhtml += '<div class="pull-left" style="width:calc(100% - 20px);height:28px">';
                                dvhtml += '<hr style="vertical-align:middle;background:#aaa;margin:0;margin-top:11px;width:95%;margin-left:2.5%;height:1px" />';
                                dvhtml += '</div>';
                                dvhtml += '<div class="clearfix"></div>';
                                dvhtml += '</div>';
                                dvhtml += '<div class="pull-right" style="width:22.5%">';
                                dvhtml += '<h4 class="no-margin center-block bold text-right">' + v.Origin + '</h4>';
                                if (returndepflight != null) {
                                    dvhtml += '<small class="center-block text-light-gray text-right">' + ConvertStringToDateTime(returndepflight.departureDateTime, 'ddd, DD MMM') + '</small>';
                                    dvhtml += '<small class="center-block text-light-gray text-right">' + ConvertStringToDateTime(returndepflight.departureDateTime, 'hhmmtt') + '</small>';
                                }
                                dvhtml += '</div>';
                                dvhtml += '<div class="clearfix"></div>';
                                dvhtml += '</div>';
                            }

                            dvhtml += '<div>';
                            dvhtml += '<div class="text-center text-sm" style="line-height:25px">' + v.FlightType + ' | ' + v.CabinClass + ' | ' + (v.TotalTravellers == 1 ? '1 Traveller' : v.TotalTravellers + ' Travellers') + ' | ' + (v.TripType == 'ONE' ? 'One-way' : 'Round-trip') + '</div>';
                            dvhtml += '</div>';
                            if (v.BookingStatus == 1) {
                                dvhtml += '<div>';
                                dvhtml += '<div class="pull-left" style="width:100%">';
                                var originAirport = '', destinationAirport = '';
                                var oac = $.grep(airport_domestic_list_json, function (e) { return e.Code == v.Origin });
                                if (oac.length == 0) {
                                    oac = $.grep(airport_international_list_json, function (e) { return e.Code == v.Origin; });
                                }
                                if (oac.length > 0) {
                                    //console.log(oac);
                                    originAirport = oac[0].Name;
                                }
                                var dac = $.grep(airport_domestic_list_json, function (e) { return e.Code == v.Destination; });
                                if (dac.length == 0) {
                                    dac = $.grep(airport_international_list_json, function (e) { return e.Code == v.Destination; });
                                }
                                if (dac.length > 0) {
                                    //console.log(dac);
                                    destinationAirport = dac[0].Name;
                                }
                                dvhtml += '<a href="#" class="center-block text-center text-sm view-ticket" data-type="' + v.FlightTypeId + '" data-partnerreferenceid="' + v.UniqueId + '" data-transid="' + v.BookingTxnId + '" data-origin="' + v.Origin + '" data-destination="' + v.Destination + '" data-originairport="' + originAirport + '" data-destinationairport="' + destinationAirport + '" data-status="' + v.BookingStatus + '">View Ticket</a>';
                                dvhtml += '</div>';
                                dvhtml += '<div class="pull-right" style="width:50%">';
                                //dvhtml += (new Date() < ConvertStringToDateTime(onwarddepflight.departureDateTime, '') ? '<a href="#" class="center-block text-right text-sm text-danger text-capitalize cancel-ticket" data-id="' + v.Id + '" data-flighttypeid="' + v.FlightTypeId + '" data-onwardflight="' + v.OnwardFlight.replaceAll('"', '\'') + '" data-returnflight="' + v.ReturnFlight.replaceAll('"', '\'') + '" data-v="' + JSON.stringify(v).replaceAll('"', '\'') + '">Cancel Ticket</a>' : '');
                                dvhtml += '</div>';
                                dvhtml += '<div class="clearfix"></div>';
                                dvhtml += '</div>';
                            }
                            //dvhtml += (v.BookingStatus == 1 ? '<a href="#" class="center-block view-ticket" data-id="' + v.Id + '">View Ticket</a>' : '');
                            //dvhtml += (v.BookingStatus == 1 && (new Date() < ConvertStringToDateTime(onwarddepflight.departureDateTime, '')) ? '<div style="margin-top:5px"><a href="#" class="btn btn-xs btn-danger cancel-ticket no-margin width-100p text-capitalize" data-onwardflight="' + v.OnwardFlight.replaceAll('"', '\'') + '" data-returnflight="' + v.ReturnFlight.replaceAll('"', '\'') + '">Cancel Ticket</a></div>' : '');
                            var $li = $("<li>", { 'class': 'flight-record transaction-record', html: dvhtml });
                            $(o).append($li);
                        });
                    }
                    else {
                        var $li = $("<li>", { 'class': 'no-margin no-bg no-shadow' });
                        var $dv = $("<div>", { html: '<p>&nbsp;</p><img src="' + virtualPath + 'img/icons/empty_state.png" class="max-width-25p center-block" /><h6 class="center-block text-none">Book a flight to generate history</h6>' });
                        $li.append($dv);
                        $(o).append($li);
                        $(b).hide();
                    }
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $(b).prop('disabled', false);
                $(b).html(btntext);
            }
        });
    } catch (e) {
        alertException(e, current_page_id, 'GetMFBTransactionList');
    }
}


$(document).ready(function (e) {
    try {
        GetMFBTransactionList('0', 0, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        GetMFBTransactionList('1', 0, numrows, 'ul.approved-transactions', 'button.load-more-approved');
        GetMFBTransactionList('3', 0, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
        GetMFBTransactionList('4', 0, numrows, 'ul.cancelled-transactions', 'button.load-more-cancelled');
    } catch (e) {
        alertException(e, current_page_id, 'Page.load');
    }

    $('button.load-more-pending').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.pending-transactions li.transaction-record").length;
            GetMFBTransactionList('0', s, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-approved').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.approved-transactions li.transaction-record").length;
            GetMFBTransactionList('1', s, numrows, 'ul.approved-transactions', 'button.load-more-approved')
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-rejected').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.rejected-transactions li.transaction-record").length;
            GetMFBTransactionList('3', s, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-cancelled').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.cancelled-transactions li.transaction-record").length;
            GetMFBTransactionList('4', s, numrows, 'ul.cancelled-transactions', 'button.load-more-cancelled');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $(document).on('click', 'a.view-ticket', function (e) {
        try {
            var q = 'i=' + StringEncryption($(this).data('type') + "|" + $(this).data('transid') + "|" + $(this).data('partnerreferenceid') + "|" + $(this).data('origin') + "|" + $(this).data('destination') + "|" + $(this).data('originairport') + "|" + $(this).data('destinationairport') + "|" + $(this).data('status'));
            window.location = virtualPath + "Account/FlightTicketView?" + q;
        } catch (e) {
            alertException(e, current_page_id, 'a.view-ticket.click');
        }
    });

    $('#close_flight_ticket_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_flight_ticket").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#close_flight_ticket_modal.click');
        }
    });

    $(document).on('click', 'a.cancel-ticket', function (e) {
        try {
            var _this = this;
            var confirmCallback = function (e) {
                if (e == 1) {
                    var v = $(_this).data('v').toString().replaceAll('\'', '"');
                    //console.log(v);
                    var onwardflight = $(_this).data('onwardflight').toString().replaceAll('\'', '"');
                    onwardflight = onwardflight.substring(1, onwardflight.length - 1);
                    onwardflight = JSON.parse(onwardflight.toString());
                    //console.log(onwardflight);
                    var flight = null;
                    if (parseInt($(_this).data('flighttypeid')) == 1)
                        flight = onwardflight.flight[0];
                    else
                        flight = onwardflight.onward[0];
                    //console.log(flight.departureDateTime);
                    //console.log(onwardflight.fareId);
                    if (new Date() < ConvertStringToDateTime(flight.departureDateTime, '')) {
                        $("#modal_flight_ticket").modal('show');
                        $(".modal-backdrop").hide();

                        setTimeout(function () {
                            $("#flight_ticket_loading").fadeOut('fast', function (e) {
                                notification('Ticket against ' + onwardflight.fareId + ' is cancelled by you', null);
                                $("#flight_ticket_cancel_container").fadeIn();
                            });
                        }, 3000);
                        //var paramList = {};
                        //paramList.s = $(_this).data('id') + "|" + onwardflight.fareId + "|" + destination + "|" + departDate + "|" + returnDate + "|" + adult.toString() + "|" + child.toString() + "|" + infant.toString() + "|" + cabinClass + "|" + mode + "|" + airline;
                        ////console.log(paramList.s);
                        //paramList.s = StringEncryption(paramList.s);
                        //$.ajax({
                        //    type: 'POST',
                        //    url: apiPath + "/Customer/CancelFlight",
                        //    contentType: "application/json; charset=utf-8",
                        //    data: JSON.stringify(paramList),
                        //    dataType: 'json',
                        //    jsonp: null,
                        //    success: function (data, textStatus, xhr) {
                        //        try {
                        //            var json = JSON.parse(StringDecryption(data)).response;
                        //            console.log(json);
                        //        } catch (e) {
                        //            //console.log(e);
                        //            alertException(e, current_page_id, 'SearchFlights.success');
                        //        }
                        //    },
                        //    error: function (xhr, textStatus, errorThrown) {
                        //        notification('Something din\'t work as expected. Try again later.', null);
                        //        OnAjaxError(xhr, textStatus, errorThrown);
                        //    },
                        //});
                    }
                    else {
                        notification('Cannot cancel Ticket after flight departure time.', null);
                    }
                }
                else {
                    //DO NOTHING
                }
            }
            navigator.notification.confirm("Do you want to Cancel this ticket?", confirmCallback, alertTitle, ['Yes', 'No']);
        } catch (e) {
            alertException(e, current_page_id, 'a.cancel-ticket.click');
        }
    });

    $('#close_flight_ticket_cancel_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_flight_ticket_cancel").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#close_flight_ticket_cancel_modal.click');
        }
    });
});