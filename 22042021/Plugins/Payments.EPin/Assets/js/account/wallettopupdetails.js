﻿current_page_id = "#wallettopupdetails";
var numrows = 10;

$(document).ready(function (e) {
    try {
        GetRWDTransactionList('0', 0, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        GetRWDTransactionList('1', 0, numrows, 'ul.approved-transactions', 'button.load-more-approved');
        GetRWDTransactionList('2', 0, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
    } catch (e) {
        alertException(e, current_page_id, '');
    }

    $('button.load-more-pending').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.pending-transactions li.transaction-record").length;
            GetRWDTransactionList('0', s, numrows, 'ul.pending-transactions', 'button.load-more-pending');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-approved').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.approved-transactions li.transaction-record").length;
            GetRWDTransactionList('1', s, numrows, 'ul.approved-transactions', 'button.load-more-approved')
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });

    $('button.load-more-rejected').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.rejected-transactions li.transaction-record").length;
            GetRWDTransactionList('2', s, numrows, 'ul.rejected-transactions', 'button.load-more-rejected');
        } catch (e) {
            alertException(e, current_page_id, '');
        }
    });
});
//function formatDate(inputDate) {
//    var value = new Date(parseInt(inputDate.replace(/(^.*\()|([+-].*$)/g, '')));
//    var month = value.getMonth() + 1;
//    var formattedDate = value.getDate() + "/" + month + "/" + value.getFullYear();
//    return formattedDate;
//}
function GetRWDTransactionList(i, s, e, o, b) {
    try {
        var btntext = $(b).html();
        var ewp = {};
        ewp.s = i + "|" + s + "|" + e;
        //console.log(ewp.s);
        ewp.s = StringEncryption(ewp.s);
        $.ajax({
            type: 'POST',
            url: "api/Customer/WalletDepositList",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $(b).prop('disabled', true);
                $(b).html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;loading...');
            },
            success: function (data, textStatus, xhr) {
                try {
                    data = JSON.parse(StringDecryption(data));
                    if (data.length > 0) {
                        //console.log(o);
                        var dt = '', dto = '', dtc = '';
                        if (data.length < e)
                            $(b).hide();
                        $.each(data, function (i, v) {
                            var $li = $("<li>", { 'class': 'transaction-record box-shadow' });
                            var $dv = $("<div>", { html: '<div class="pull-left text-md bold">' + v.BankName + '</div><div class="pull-right text-md bold"><i class="fa fa-rupee"></i>&nbsp;' + (v.Status == 1 ? v.ApprovedAmount : v.RequestedAmount) + '</div><div class="clearfix"></div><div class="text-left text-sm">Branch:&nbsp;' + v.BranchName + '. IFSC Code:&nbsp;' + v.IfscCode + '</div><div class="text-left text-sm">' + (v.Status == 0 ? formatDate(v.ApplicationDtm) : formatDate(v.StatusDtm)) + '</div>' });
                            $li.append($dv);
                            $(o).append($li);
                        });
                    }
                    else {
                        var $li = $("<li>", { 'class': 'no-margin no-bg no-shadow' });
                        var $dv = $("<div>", { html: '<p>&nbsp;</p><img src="' + virtualPath + 'Plugins/Nop.Plugin.Api/img/icons/empty-state.jpg" class="max-width-25p center-block" /><h6 class="center-block text-none">No history available</h6>' });
                        $li.append($dv);
                        $(o).append($li);
                        $(b).hide();
                    }
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $(b).prop('disabled', false);
                $(b).html(btntext);
            }
        });
    } catch (e) {
        alertException(e, current_page_id, '');
    }
}