﻿current_page_id = "#changepassword";

$("body").one('pageshow', current_page_id, function (event, ui) {
    if (localStorage.getItem("VF_" + appId + "_CHANGE_PASSWORD") != undefined) {
        if (localStorage.getItem("VF_" + appId + "_CHANGE_PASSWORD") != null) {
            $("#divmenuicon").hide();
            notification('Change your password to continue');
        }
    }
    $("#oldpassword").focus();
});

$('a.reload-page').on("click", function (e) {
    try {
        ResetForm('#frmchgpwd');
    } catch (e) {
        alertException(e, current_page_id, 'a.reload-page.click');
    }
});

$('form#frmchgpwd').validate({
    rules: {
        oldpassword: {
            required: {
                depends: function (e) {
                    $(this).val($(this).val().trim());
                    return true;
                }
            },
        },
        newpassword: {
            required: {
                depends: function (e) {
                    $(this).val($(this).val().trim());
                    return true;
                }
            },
            notEqualTo: "#oldpassword"
        },
        confirmpassword: {
            required: {
                depends: function (e) {
                    $(this).val($(this).val().trim());
                    return true;
                }
            },
            equalTo: "#newpassword"
        },
    },
    messages: {
        oldpassword: { required: "Enter old password" },
        newpassword: {
            required: "Enter a new password",
            notEqualTo: "Old & New password should be different"
        },
        confirmpassword: {
            required: "Re-enter new password to confirm",
            equalTo: "Password's do not match"
        },
    }
});

$('form#frmchgpwd').on('submit', function (e) {
    try {
        var isvalidate = $(this).valid();
        if (isvalidate) {
            var paramList = {};
            var em = localStorage.getItem("VF_" + appId + "_LOGGED_IN_EMAIL") == null ? "" : localStorage.getItem("VF_" + appId + "_LOGGED_IN_EMAIL");
            paramList.s = localStorage.getItem("VF_" + appId + "_LOGGED_IN_ID") + "|" + localStorage.getItem("VF_" + appId + "_LOGGED_IN_LOGINID") + "|" + em + "|" + $('#oldpassword').val().trim() + "|" + $('#newpassword').val().trim();
            paramList.s = StringEncryption(paramList.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/ChangePassword",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                beforeSend: function (xhr, opts) {
                    $("button.submit-button").prop('disabled', true);
                    $("button.submit-button span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;updating...');
                },
                success: function (data, textStatus, xhr) {
                    try {
                        if (data.e != '') {
                            alertme(data.e);
                        }
                        else {
                            alertme('Password changed successfully.', 'success');
                            ResetForm('#frmchgpwd');
                            if (localStorage.getItem("VF_" + appId + "_CHANGE_PASSWORD") != undefined) {
                                localStorage.removeItem("VF_" + appId + "_CHANGE_PASSWORD");
                            }
                        }
                    } catch (e) {
                        alertException(e, current_page_id, 'ChangePassword.success');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
                complete: function (a) {
                    $("button.submit-button").prop('disabled', false);
                    $("button.submit-button span").html('Change Password');
                }
            });
        }
        e.preventDefault();
    } catch (e) {
        alertException(e, current_page_id, 'frmchgpwd submit');
    }
});