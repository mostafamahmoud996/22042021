﻿current_page_id = "#flightticketview";
var type = 0, transid = '', partnerreferenceid = '', origin = '', destination = '', originairport = '', destinationairport = '', bookingstatus = 0, isrefundable = 0;

function CancelTicket(f) {
    if (f == 2) {
        var tickets = '';
        for (var i = 0; i < $(".passenger").length; i++) {
            if ($(".passenger").eq(i).prop('checked')) {
                tickets += (tickets == '' ? '' : ',') + $(".passenger").eq(i).data('passengerid') + '~' + $(".passenger").eq(i).data('segmenttype') + '~' + $(".passenger").eq(i).data('passengername') + '~' + $(".passenger").eq(i).data('ticketnumber');
            }
        }

        var ewpc = {};
        ewpc.s = transid + "|" + partnerreferenceid + "|" + $("#cancellationReason").val().trim() + '|' + tickets + '|' + $(".passenger").length + '|' + $("#hdn_flight_cancellation_charges").val() + '|' + $("#total_flight_cancellation_charges").val();
        //console.log(ewpc.s);
        ewpc.s = StringEncryption(ewpc.s);
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/CancelFlightTicket",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewpc),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $("button.submit-button").prop('disabled', true);
                $("button.submit-button span").css('opacity', 0);
                $("button.submit-button").addClass('spinner_01');
            },
            success: function (data, textStatus, xhr) {
                try {
                    notification('Ticket has been cancelled successfully!\n\nRefund will take place in 7-10 days', function () {
                    });
                } catch (e) {
                    alertException(e, current_page_id, 'CancelFlightTicket.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $("button.submit-button").prop('disabled', false);
                $("button.submit-button span").css('opacity', '');
                $("button.submit-button").removeClass('spinner_01');
            }
        });
    }
}

function TotalCancellationCharges() {
    try {
        var chk = 0;
        for (var i = 0; i < $(".passenger").length; i++) {
            if ($(".passenger").eq(i).prop('checked')) {
                chk++;
            }
        }
        var total_charges = chk * parseInt($("#hdn_flight_cancellation_charges").val());
        $("#total_flight_cancellation_charges").val(total_charges);
        $("h3.cancellation-amount span").html(total_charges.toLocaleString('en-IN'));
    } catch (e) {
        alertException(e, current_page_id, 'TotalCancellationCharges');
    }
}

function TravellerInformation(r, travellers) {
    try {
        var f = r.flight;
        var o = r.rule.onward;
        //console.log('-------------------------------------------');
        //console.log(f);
        //f.sort(function (a, b) {
        //    var dateA = new Date(a.departureDateTime), dateB = new Date(b.departureDateTime)
        //    return dateA - dateB //sort by date ascending
        //})
        //console.log(f);
        //console.log('-------------------------------------------');
        var html = '';
        if (f != undefined) {
            if (f.length > 0) {
                for (var i = 0; i < f.length; i++) {
                    var originairport = '', destinationairport = '', departureAirportCode = '', arrivalAirportCode = '';
                    departureAirportCode = f[i].origin;
                    arrivalAirportCode = f[i].destination;
                    if (i > 0) {
                        var diffMs = (ConvertStringToDateTime(f[i].departureDateTime, '') - ConvertStringToDateTime(f[i - 1].arrivalDateTime, '')); // milliseconds between now & Christmas
                        //console.log(diffMs);
                        var totalhours = Math.floor((diffMs % 86400000) / 3600000); // hours
                        //console.log(totalhours);
                        var totalminutes = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                        //console.log(totalminutes);
                        if (totalminutes >= 60) {
                            var mintohrs = Math.floor(totalminutes / 60);
                            totalminutes = totalminutes - (mintohrs * 60);
                            if (totalminutes == 60) {
                                totalminutes = 0;
                                mintohrs = mintohrs + 1;
                            }
                            totalhours = totalhours + mintohrs;
                        }
                        var totalduration = totalhours.toString() + ' hrs ' + totalminutes.toString() + ' min';
                        html += '<div class="bg-black-fade-01 padding-5 text-center"><i class="fa fa-history text-center"></i>&nbsp;&nbsp;' + totalduration + ' layover in ' + (f[i - 1].arrivalAirportCode == undefined ? f[i].destination : f[i].arrivalAirportCode) + '</div>';
                        html += '<div class="clearfix"></div>';
                        html += '<br />';
                    }
                    var oac = $.grep(airport_domestic_list_json, function (e) { return e.Code == departureAirportCode });
                    if (oac.length == 0) {
                        oac = $.grep(airport_international_list_json, function (e) { return e.Code == departureAirportCode; });
                    }
                    if (oac.length > 0) {
                        //console.log(oac);
                        originairport = oac[0].Name;
                    }
                    var dac = $.grep(airport_domestic_list_json, function (e) { return e.Code == arrivalAirportCode; });
                    if (dac.length == 0) {
                        dac = $.grep(airport_international_list_json, function (e) { return e.Code == arrivalAirportCode; });
                    }
                    if (dac.length > 0) {
                        //console.log(dac);
                        destinationairport = dac[0].Name;
                    }
                    html += '<div class="bg-warning padding-5" style="border: 1px dashed #aaa">';
                    html += '<h4 class="padding-5 text-center text-bold no-margin">PNR - ' + f[i].airlinePnr + '</h4>';
                    html += '<div class="clearfix"></div>';
                    html += '<div class="pull-left" style="width:35%">';
                    html += '<h5 class="no-margin" style="white-space:nowrap">' + departureAirportCode + '&nbsp;' + ConvertStringToDateTime(f[i].departureDateTime, 'hhmmtt').replace(' ', '') + '</h5>';
                    html += '<small class="center-block text-light-gray text-left">' + ConvertStringToDateTime(f[i].departureDateTime, 'ddd, DD MMM') + '</small>';
                    html += '<small class="center-block text-light-gray text-left">' + originairport + '</small>';
                    html += '</div>';
                    html += '<div class="pull-left text-light-gray text-center" style="width:30%">';
                    html += '<i class="fa fa-history text-center"></i>';
                    html += '<div class="text-light-gray">' + FlightDuration(f[i]) + '</div>';
                    html += '</div>';
                    html += '<div class="pull-right text-right" style="width:35%">';
                    html += '<h5 class="no-margin" style="white-space:nowrap">' + arrivalAirportCode + '&nbsp;' + ConvertStringToDateTime(f[i].arrivalDateTime, 'hhmmtt').replace(' ', '') + '</h5>';
                    html += '<small class="center-block text-light-gray text-right">' + ConvertStringToDateTime(f[i].arrivalDateTime, 'ddd, DD MMM') + '</small>';
                    html += '<small class="center-block text-light-gray text-right">' + destinationairport + '</small>';
                    html += '</div>';
                    html += '<div class="clearfix"></div>';
                    html += '<div class="height-5"></div>';
                    html += '<div>';
                    html += '<div class="pull-left text-bold text-sm" style="width:65%"><span style="width:28px;display:inline-block">&nbsp;&nbsp;&nbsp;</span>PASSENGER</div>';
                    html += '<div class="pull-right text-bold text-sm" style="width:35%">TICKET NUMBER</div>';
                    for (var j = 0; j < f[i].ticket.length; j++) {
                        html += '<div class="pull-left text-sm text-capitalize" style="width:65%;line-height:27px">';
                        html += '<span class="sr-no text-center" style="height:25px;width:25px;display:inline-block">' + (j + 1) + '.</span>';
                        //html += '<span class="checkbox no-margin" style="display:none"><label class="display-inline-block"><input id="chk_' + (i + 1) + '_' + (j + 1) + '" name="chk_' + (i + 1) + '_' + (j + 1) + '" data-passengerid="' + f[i].ticket[j].passengerId + '" data-ticketnumber="' + f[i].ticket[j].ticketNumber + '" data-passengername="' + (f[i].ticket[j].firstName + ' ' + f[i].ticket[j].lastName) + '" data-segmenttype="' + (f[i].origin == origin ? 1 : 2) + '" class="passenger" type="checkbox" /></label></span>';

                        var td = $.grep(travellers, function (e) { return e.CustomerId == LOGGED_IN_ID && e.UniqueId == partnerreferenceid && e.PassengerId == f[i].ticket[j].passengerId });
                        if (td.length > 0) {
                            //console.log(td);
                            if (td.Status != 3)
                                html += '<span class="checkbox no-margin display-none" style="display:none"><label class="display-inline-block"><input id="chk_' + (i + 1) + '_' + (j + 1) + '" name="chk_' + (i + 1) + '_' + (j + 1) + '" data-passengerid="' + f[i].ticket[j].passengerId + '" data-ticketnumber="' + f[i].ticket[j].ticketNumber + '" data-passengername="' + (f[i].ticket[j].firstName + ' ' + f[i].ticket[j].lastName) + '" data-segmenttype="' + (f[i].origin == origin ? 1 : 2) + '" class="passenger" type="checkbox" checked /></label></span> ';
                        }

                        html += ' ' + f[i].ticket[j].firstName + ' ' + f[i].ticket[j].lastName + '</div>';
                        html += '<div class="pull-right text-sm" style="width:35%">' + f[i].ticket[j].ticketNumber + '</div>';
                    }
                    html += '<div class="clearfix"></div>';
                    html += '</div>';
                    html += '</div>';
                    if ((i + 1) < f.length)
                        html += '<br />';
                }
            }
        }
        return html;
    } catch (e) {
        alertException(e, current_page_id, 'TravellerInformation');
    }
}


function TravellerUpdate(r, status) {
    try {
        var f = r.flight;
        var o = r.rule.onward;
        //console.log('-------------------------------------------');
        //console.log(f);
        //f.sort(function (a, b) {
        //    var dateA = new Date(a.departureDateTime), dateB = new Date(b.departureDateTime)
        //    return dateA - dateB //sort by date ascending
        //})
        //console.log(f);
        //console.log('-------------------------------------------');
        var html = '';
        if (f != undefined) {
            if (f.length > 0) {
                var passengers = '';
                for (var i = 0; i < f.length; i++) {
                    for (var j = 0; j < f[i].ticket.length; j++) {
                        if (i == 0) {
                            passengers += (passengers == '' ? '' : ',') + f[i].ticket[j].firstName + '~' + f[i].ticket[j].lastName + '~' + f[i].ticket[j].passengerId;
                        }
                    }
                }
                if (passengers != '') {
                    var ewpl = {};
                    ewpl.s = transid + "|" + partnerreferenceid + "|" + passengers;
                    //console.log(ewp.s);
                    ewpl.s = StringEncryption(ewpl.s);
                    $.ajax({
                        type: 'POST',
                        url: apiPath + "/Customer/FlightTicketTravellers",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(ewpl),
                        dataType: 'json',
                        jsonp: null,
                        success: function (data, textStatus, xhr) {
                            try {
                                travellers = JSON.parse(StringDecryption(data));
                                //console.log(travellers);
                                $("#flight_stop_details").html(TravellerInformation(r, travellers));
                                TotalCancellationCharges();
                                if (status != 3 && isrefundable == 1)
                                    $("#cancel_ticket").trigger('click');
                                $("#flight_ticket_loading").fadeOut('fast', function (e) {
                                    $("#flight_ticket_container").fadeIn();
                                });
                            } catch (e) {
                                alertException(e, current_page_id, 'Page.load');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            OnAjaxError(xhr, textStatus, errorThrown);
                        },
                    });
                }
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'TravellerUpdate');
    }
}

$(document).ready(function (e) {
    try {
        if (page_query_string != '') {
            //console.log(page_query_string);
            //console.log(page_query_string_object);
            var q = page_query_string_object;
            //console.log(q);
            type = q[0];
            transid = q[1];
            partnerreferenceid = q[2];
            origin = q[3];
            destination = q[4];
            originairport = q[5];
            destinationairport = q[6];
            $("#transid").val(transid);
            $("#partnerreferenceid").val(partnerreferenceid);
            $("#div_page_title").html('Ticket for Trip Id - ' + transid);
            var ewp = {};
            if (a == true && q.length >= 9)
                ewp.s = q[8] + "|" + type + "|" + transid + "|" + partnerreferenceid;
            else
                ewp.s = type + "|" + transid + "|" + partnerreferenceid;
            //console.log(ewp.s);
            ewp.s = StringEncryption(ewp.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/FlightTicket",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(ewp),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    try {
                        bookingstatus = parseInt(StringDecryption(data).split('|')[0]);
                        data = JSON.parse(StringDecryption(data).split('|')[1]);
                        //console.log(data);
                        if (data.status == 'SUCCESS') {
                            TravellerUpdate(data.response, bookingstatus);
                            var flightcount = data.response.flight.length;
                            $("#flight_title .from").html(origin);
                            $("#flight_title .to").html(destination);
                            $(".from-city").html(originairport);
                            $(".to-city").html(destinationairport);
                            $("#flight_baggage").html(data.response.rule.onward.baggage == "" ? "N.a." : data.response.rule.onward.baggage + ' kg');
                            $("#flight_fare_type").html('<span class="text-' + (data.response.rule.onward.refundable == 'yes' ? 'success">Refundable' : 'danger">Non-Refundable') + '</span>');
                            isrefundable = parseInt(data.response.rule.onward.refundable == 'yes' ? 1 : 0);
                            var reschedule_charge = 'Not Available';
                            if (data.response.rule.onward.rescheduleCharge != null) {
                                reschedule_charge = data.response.rule.onward.rescheduleCharge;
                                if (reschedule_charge != 'Not Available') {
                                    reschedule_charge = '<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(reschedule_charge).toLocaleString('en-IN');
                                }
                            }
                            $("#flight_reschedule_charges").html(reschedule_charge);
                            $("#hdn_flight_cancellation_charges").val(data.response.rule.onward.cancellationFee);
                            $("#flight_cancellation_charges").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(data.response.rule.onward.cancellationFee).toLocaleString('en-IN'));
                            var cancellation_slab = '-';
                            if (data.response.rule.onward.cancellationSlab != null) {
                                cancellation_slab = '<ul style="margin-left:15px"><li>' + data.response.rule.onward.cancellationSlab.replaceAll('#', '</li><li>');
                                cancellation_slab = cancellation_slab.substring(0, cancellation_slab.length - 5) + '</ul>';
                            }
                            $("#flight_cancellation_slab").html(cancellation_slab);
                            var flight_flight_remarks = '-';
                            if (data.response.rule.onward.remarks != null)
                                flight_flight_remarks = data.response.rule.onward.remarks;
                            $("#flight_remarks").html(flight_flight_remarks);
                        }
                        else {
                            $("#flight_ticket_loading").fadeOut('fast', function (e) {
                                $("#flight_ticket_not_available").fadeIn();
                            });
                        }
                    } catch (e) {
                        alertException(e, current_page_id, 'Page.load');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#flight_ticket_loading").fadeOut('fast', function (e) {
                        $("#flight_ticket_not_available").fadeIn();
                        $("#flight_ticket_not_available h5").html(errorThrown);
                    });
                    //OnAjaxError(xhr, textStatus, errorThrown);
                },
            });

            $('form#frmflightticketview').validate({
                rules: {
                    transid: { required: true },
                    partnerreferenceid: { required: true },
                    cancellationReason: {
                        required: {
                            depends: function (e) {
                                $(this).val($(this).val().trim());
                                return false;
                            }
                        },
                        regex: /^[a-zA-Z0-9\-\,\.\/\(\)\s]+$/,
                    },
                },
                messages: {
                    transid: { required: "Trip Id not available" },
                    partnerreferenceid: { required: "Transaction Id not available" },
                    cancellationReason: {
                        required: "Please enter Cancellation Reason",
                        regex: "Enter only alphabets, number, space and special characters such as -,./()",
                    },
                }
            });

            $('form#frmflightticketview').on('submit', function (e) {
                try {
                    var isvalidate = $(this).valid();
                    var chk = 0;
                    for (var i = 0; i < $(".passenger").length; i++) {
                        if ($(".passenger").eq(i).prop('checked'))
                            chk++;
                    }
                    if (chk == 0) {
                        notification('Please select one or all Passengers to cancel.', null);
                        isvalidate = false;
                    }
                    if (isvalidate) {
                        navigator.notification.confirm(
                            'Are you sure you want to cancel the Ticket?', // message
                            CancelTicket, // callback to invoke with index of button pressed
                            alertTitle, // title
                            ['NO', 'YES'] // buttonLabels
                        );
                        e.preventDefault();
                    }
                    e.preventDefault();
                } catch (e) {
                    alertException(e, current_page_id, 'form#frmflightticketview.submit');
                }
            });
        }
        else {
            $("#flight_ticket_loading").fadeOut('fast', function (e) {
                $("#flight_ticket_not_available").fadeIn();
            });
        }
    } catch (e) {
        alertException(e, current_page_id, 'Page.load');
    }

    $("#chkAllPassengers").on('click', function (e) {
        try {
            if ($(this).prop('checked'))
                $(".passenger").prop('checked', true);
            else
                $(".passenger").prop('checked', false);
            TotalCancellationCharges();
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });

    $(document).on('click', ".passenger", function (e) {
        try {
            if ($(this).prop('checked'))
                $("#chkAllPassengers").prop('checked', ($(".passenger").length == $(".passenger").filter(':checked').length));
            else
                $("#chkAllPassengers").prop('checked', ($(".passenger").length == $(".passenger").filter(':checked').length));
            TotalCancellationCharges();
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });

    $("#cancel_ticket").on('click', function (e) {
        try {
            $("#cancel_ticket").fadeOut('fast', function (e) {
                $("#undo_cancel_ticket").fadeIn();
                $("#submit_button").slideDown();
                $("#cancel_reason").fadeIn();
                $("#div_chkAllPassengers").fadeIn();
                TotalCancellationCharges();
            });
        } catch (e) {
            alertException(e, current_page_id, 'cancel_ticket.click');
        }
    });

    $("#undo_cancel_ticket").on('click', function (e) {
        try {
            $("#submit_button").slideUp();
            $("#cancel_reason").fadeOut();
            $("#cancel_reason").val('');
            $("#div_chkAllPassengers").fadeOut();
            $("#undo_cancel_ticket").fadeOut('fast', function (e) {
                $("#cancel_ticket").fadeIn();
                TotalCancellationCharges();
            });
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });
});