﻿current_page_id = "#rechargestatus";
var actionId = 0, txnid = '', status = '', enci = localStorage.getItem("enci");


$(document).ready(function (e) {
    try {
        if (localStorage.getItem("i") != null) {
            actionId = parseInt(localStorage.getItem("i").toString());
            if (localStorage.getItem("pp") != null) {
                if (localStorage.getItem("pp").toString() == 'pr') {
                    actionId = 1;
                }
                if (localStorage.getItem("pp").toString() == 'po') {
                    actionId = 2;
                }
            }
            var txnNameList = $.grep(type_list_json, function (e) { return e.Id == actionId; });
            if (txnNameList.length > 0) {
                txnname = txnNameList[0].Name;
            }
            if (localStorage.getItem("transactionid") != null)
                txnid = localStorage.getItem("transactionid");
            if (localStorage.getItem("rechargestatus") != null)
                status = localStorage.getItem("rechargestatus");

            if (status == 'success')
                $(".div-icon").html(tickanimation);
            else
                $(".div-icon").html('<img src="' + virtualPath + 'Plugins/Nop.Plugin.Api/img/tick-orange.png" style="max-width:150px">');

            $("h4.recharge-type").html(txnname);
            $("span.transaction-id").html('Recharge/Payment against Id <b>' + txnid + '</b>' + (status == 'success' ? ' is successful.' : ' is pending confirmation from Operator.'));
            localStorage.removeItem("i");
            localStorage.removeItem("enci");
            localStorage.removeItem("pp");
            localStorage.removeItem("m");
            localStorage.removeItem("o");
            localStorage.removeItem("onm");
            localStorage.removeItem("a");
            localStorage.removeItem("c");
            localStorage.removeItem("cnm");
            localStorage.removeItem("u");
            localStorage.removeItem("d");
            localStorage.removeItem("cy");
            localStorage.removeItem("sdn");
            localStorage.removeItem("sds");
            localStorage.removeItem("ci");
            localStorage.removeItem("acc");
            localStorage.removeItem("transactionid");
            localStorage.removeItem("rechargestatus");
            localStorage.removeItem("errormessage");
        }
    } catch (e) {
        alertException(e, current_page_id, 'pageLoad');
    }

    $(document).on('click', 'button.submit-button', function (e) {
        try {
            e.preventDefault();
            window.location = "MobileRecharge?i=" + enci;
        } catch (e) {
            alertException(e, current_page_id, '#formMP a.close-popup-over-list.click');
        }
    });
});

