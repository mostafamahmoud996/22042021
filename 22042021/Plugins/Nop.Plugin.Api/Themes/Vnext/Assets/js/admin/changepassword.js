﻿$(document).ready(function () {
    var sblbtn = $("#btnSubmit");

    $("input#OldPassword").focus();

    $("#frmChangePassword").validate({
        rules: {
            OldPassword: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                //regex: /(?=^.{6,20}$)(?=.*[A-Z])(?=.*[!@@#$%^&*]+)(?![.\n])(?=.*[a-z]).*$/,
                rangelength: [4, 20]
            },
            Password: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                //regex: /(?=^.{6,20}$)(?=.*[A-Z])(?=.*[\@@\#\$\%\&\*]+)(?![.\n])(?=.*[a-z]).*$/,
                rangelength: [6, 20],
                notEqualTo: "#OldPassword"
            },
            PasswordConfirm: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                equalTo: "#Password"
            }
        },
        messages: {
            OldPassword: {
                required: "Enter your old Password.",
                rangelength: "Old Password should be {0}-{1} characters."
            },
            Password: {
                required: "Enter a new Password.",
                //regex: "Password should contain at-least 1 capital letter, 1 small letter, 1 special character (<span class='text-red'>@@#$%&*</span>) & should be minimum 6 characters long.",
                rangelength: "Password should be {0}-{1} characters.",
                notEqualTo: "New Password should not match with Old password.",
            },
            PasswordConfirm: {
                required: "Re-enter password  to confirm.",
                equalTo: "Re-entered password does not match."
            }
        }
    });

    $("#frmChangePassword").on('submit', function (e) {
        e.preventDefault();
        var isvalidate = $(this).valid();
        if (isvalidate) {
            var obj = new Object();
            obj.s = $("label.username").text().trim() + "|" + $("#OldPassword").val().trim() + "|" + $("#Password").val().trim();
            obj.s = StringEncryption(obj.s);
            var $btn = sblbtn.button('loading');
            sblbtn.prop("disabled", true);
            $.ajax({
                url: apiPath + "/ChangePassword",
                type: "POST",
                data: obj,
                success: function (data, textStatus, xhr) {
                    bootalert('Password updated successfully.', 'success');
                    setTimeout(function () {
                        window.location = virtualPath + 'Admin/ChangePassword';
                    }, 2000);
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (errorThrown == '')
                        errorThrown = 'Connection is not available.';
                    $btn.button('reset')
                    sblbtn.prop("disabled", false);
                    bootalert(errorThrown, 'error');
                }
            });
        }
    });
});