﻿current_page_id = "#busticketview";
var type = 0, bookingid = '', partnerreferenceid = '', origin = '', destination = '', boardingpoint = '', destinationairport = '', bookingstatus = 0, isrefundable = 0;

function CancelTicket(f) {
    if (f == 2) {
        var tickets = '';
        for (var i = 0; i < $(".passenger").length; i++) {
            if ($(".passenger").eq(i).prop('checked')) {
                tickets += (tickets == '' ? '' : ',') + $(".passenger").eq(i).data('passengerid') + '~' + $(".passenger").eq(i).data('segmenttype') + '~' + $(".passenger").eq(i).data('passengername') + '~' + $(".passenger").eq(i).data('ticketnumber');
            }
        }

        var ewpc = {};
        ewpc.s = bookingid + "|" + partnerreferenceid + "|" + $("#cancellationReason").val().trim() + '|' + tickets + '|' + $(".passenger").length + '|' + $("#hdn_bus_cancellation_charges").val() + '|' + $("#total_bus_cancellation_charges").val();
        //console.log(ewpc.s);
        ewpc.s = StringEncryption(ewpc.s);
        $.ajax({
            type: 'POST',
            url: apiPath + "/Customer/CancelBusTicket",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewpc),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $("button.submit-button").prop('disabled', true);
                $("button.submit-button span").css('opacity', 0);
                $("button.submit-button").addClass('spinner_01');
            },
            success: function (data, textStatus, xhr) {
                try {
                    notification('Ticket has been cancelled successfully!\n\nRefund will take place in 7-10 days', function () {

                    });
                } catch (e) {
                    alertException(e, current_page_id, 'CancelBusTicket.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $("button.submit-button").prop('disabled', false);
                $("button.submit-button span").css('opacity', '');
                $("button.submit-button").removeClass('spinner_01');
            }
        });
    }
}

function TotalCancellationCharges() {
    try {
        var chk = 0;
        for (var i = 0; i < $(".passenger").length; i++) {
            if ($(".passenger").eq(i).prop('checked')) {
                chk++;
            }
        }
        var total_charges = chk * parseInt($("#hdn_bus_cancellation_charges").val());
        $("#total_bus_cancellation_charges").val(total_charges);
        $("h3.cancellation-amount span").html(total_charges.toLocaleString('en-IN'));
    } catch (e) {
        alertException(e, current_page_id, 'TotalCancellationCharges');
    }
}

$(document).ready(function (e) {
    try {
        if (page_query_string != '') {
            var q = page_query_string_object;
            //console.log(q);
            bookingid = q[0];
            partnerreferenceid = q[1];
            origin = q[2];
            destination = q[3];
            boardingpoint = q[4];
            $("#bookingid").val(bookingid);
            $("#partnerreferenceid").val(partnerreferenceid);
            $("#div_page_title").html('Ticket for Trip Id - ' + bookingid);
            var ewp = {};
            ewp.s = bookingid + "|" + partnerreferenceid;
            //console.log(ewp.s);
            ewp.s = StringEncryption(ewp.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/BusTicket",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(ewp),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    try {
                        bookingstatus = parseInt(StringDecryption(data).split('|')[0]);
                        var tripinfo = JSON.parse(StringDecryption(data).split('|')[1]);
                        //console.log(tripinfo);
                        var departureTime = ConvertStringToDateTime(tripinfo.departure_time, 'hhmmtt');
                        var arrivalTime = ConvertStringToDateTime(tripinfo.arrival_time, 'hhmmtt');
                        $("#onward_bus_title .from").html(tripinfo.departure_city + '<small class="center-block">' + departureTime + '</small>');
                        $("#onward_bus_title .to").html(tripinfo.arrival_city + '<small class="center-block">' + arrivalTime + '</small>');
                        $("#onward_bus_more_info").html('<b class="text-dark-grey">' + tripinfo.travels_name + '</b>' + '<small class="center-block">(' + tripinfo.bus_type + ')</small>');

                        if (tripinfo.cancellation_policy.length > 0) {
                            var html = '';
                            html += '<ol class="no-margin" style="padding-left:15px">';
                            for (var j = 0; j < tripinfo.cancellation_policy.length; j++) {
                                html += '<li class="text-sm">';
                                html += (tripinfo.cancellation_policy[j].from_time == '0' ? 'Upto ' : (tripinfo.cancellation_policy[j].from_time + ' to ')) + tripinfo.cancellation_policy[j].to_time + ' - <b><i class="fa fa-rupee"></i> ' + tripinfo.cancellation_policy[j].service_charge + '</b>';
                                html += '</li>';
                            }
                            html += '</ol>';
                            $("#divPolicy").html(html);
                        }

                        if (tripinfo.passenger_details.length > 0) {
                            var html = '';
                            html += '<div>';
                            html += '<div class="pull-left text-bold text-sm" style="width:65%"><span style="width:28px;display:inline-block">&nbsp;&nbsp;&nbsp;</span>PASSENGER</div>';
                            html += '<div class="pull-right text-center text-bold text-sm" style="width:35%">SEAT NUMBER</div>';
                            for (var j = 0; j < tripinfo.passenger_details.length; j++) {
                                html += '<div class="pull-left text-sm text-capitalize" style="width:65%;line-height:27px">';
                                html += '<span class="sr-no text-center" style="height:25px;width:25px;display:inline-block">' + (j + 1) + '.</span>';
                                html += '<span class="checkbox no-margin display-none" style="display:none"><label class="display-inline-block"><input id="chk_' + (j + 1) + '" name="chk_' + (j + 1) + '" data-booking_id="' + tripinfo.booking_id + '" data-pnr="' + tripinfo.pnr + '" data-seat_name="' + tripinfo.passenger_details[j].seat_name + '" data-passengername="' + tripinfo.passenger_details[j].name + '" class="passenger" type="checkbox" checked /></label></span> ';
                                html += ' ' + tripinfo.passenger_details[j].name + '</div>';
                                html += '<div class="pull-right text-center text-sm" style="width:35%">' + tripinfo.passenger_details[j].seat_name + '</div>';
                            }
                            html += '<div class="clearfix"></div>';
                            html += '</div>';
                            $("#passenger_details").html(html);
                        }
                        $("#total_charges").html(tripinfo.total_fare)
                        $("#bus_ticket_loading").fadeOut('fast', function (e) {
                            $("#bus_ticket_container").fadeIn();
                        });
                    } catch (e) {
                        alertException(e, current_page_id, 'Page.load');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#bus_ticket_loading").fadeOut('fast', function (e) {
                        $("#bus_ticket_not_available").fadeIn();
                        $("#bus_ticket_not_available h5").html(errorThrown);
                    });
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
            });

            $('form#frmbusticketview').validate({
                rules: {
                    bookingid: { required: true },
                    partnerreferenceid: { required: true },
                    cancellationReason: {
                        required: {
                            depends: function (e) {
                                $(this).val($(this).val().trim());
                                return false;
                            }
                        },
                        regex: /^[a-zA-Z0-9\-\,\.\/\(\)\s]+$/,
                    },
                },
                messages: {
                    bookingid: { required: "Trip Id not available" },
                    partnerreferenceid: { required: "Transaction Id not available" },
                    cancellationReason: {
                        required: "Please enter Cancellation Reason",
                        regex: "Enter only alphabets, number, space and special characters such as -,./()",
                    },
                }
            });
            $('form#frmbusticketview').on('submit', function (e) {
                try {
                    var isvalidate = $(this).valid();
                    var chk = 0;
                    for (var i = 0; i < $(".passenger").length; i++) {
                        if ($(".passenger").eq(i).prop('checked'))
                            chk++;
                    }
                    if (chk == 0) {
                        notification('Please select one or all Passengers to cancel.', null);
                        isvalidate = false;
                    }
                    if (isvalidate) {
                        navigator.notification.confirm(
                            'Are you sure you want to cancel the Ticket?', // message
                            CancelTicket, // callback to invoke with index of button pressed
                            alertTitle, // title
                            ['NO', 'YES'] // buttonLabels
                        );
                        e.preventDefault();
                    }
                    e.preventDefault();
                } catch (e) {
                    alertException(e, current_page_id, 'form#frmbusticketview.submit');
                }
            });
        }
        else {
            $("#bus_ticket_loading").fadeOut('fast', function (e) {
                $("#bus_ticket_not_available").fadeIn();
            });
        }
    } catch (e) {
        alertException(e, current_page_id, 'Page.load');
    }
    $("#chkAllPassengers").on('click', function (e) {
        try {
            if ($(this).prop('checked'))
                $(".passenger").prop('checked', true);
            else
                $(".passenger").prop('checked', false);
            TotalCancellationCharges();
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });
    $(document).on('click', ".passenger", function (e) {
        try {
            if ($(this).prop('checked'))
                $("#chkAllPassengers").prop('checked', ($(".passenger").length == $(".passenger").filter(':checked').length));
            else
                $("#chkAllPassengers").prop('checked', ($(".passenger").length == $(".passenger").filter(':checked').length));
            TotalCancellationCharges();
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });
    $("#cancel_ticket").on('click', function (e) {
        try {
            $("#cancel_ticket").fadeOut('fast', function (e) {
                $("#undo_cancel_ticket").fadeIn();
                $("#extra_space").fadeIn();
                $("#submit_button").slideDown();
                $("#cancel_reason").fadeIn();
                $("#div_chkAllPassengers").fadeIn();
                TotalCancellationCharges();
            });
        } catch (e) {
            alertException(e, current_page_id, 'cancel_ticket.click');
        }
    });
    $("#undo_cancel_ticket").on('click', function (e) {
        try {
            $("#submit_button").slideUp();
            $("#extra_space").fadeOut();
            $("#cancel_reason").fadeOut();
            $("#cancel_reason").val('');
            $("#div_chkAllPassengers").fadeOut();
            $("#undo_cancel_ticket").fadeOut('fast', function (e) {
                $("#cancel_ticket").fadeIn();
                TotalCancellationCharges();
            });
        } catch (e) {
            alertException(e, current_page_id, 'undo_cancel_ticket.click');
        }
    });
});

