﻿current_page_id = "#flightbooking";
var minDt = new Date(), step = 250, deparr = 1, overwritelist = false;
var airportlistloaded = false, profileflagFB = false;

function LoadAirports(obj) {
    try {
        if (obj)
            $("#searchairport").val('Enter ' + (deparr == 1 ? 'Departure' : 'Arrival') + ' City');

        $("#airportlistloadingMR").show();
        $("#airportlistemptyMR").hide();
        $("#airportlistMR").hide();

        if (deparr == 1) {
            if (airport_domestic_list_json.length > 0) {
                $("ul#airport_code_list").empty();
                for (var i = 0; i < airport_domestic_list_json.length ; i++) {
                    var $li = $("<li>");
                    var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + virtualPath + 'img/icons/control-tower.png" class="max-width-50p" />' });
                    var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-airport" data-code="' + airport_domestic_list_json[i].Code + '" data-name="' + airport_domestic_list_json[i].Name + '" data-city="' + airport_domestic_list_json[i].City + '" data-country="' + airport_domestic_list_json[i].Country + '"><span class="text-left text-md display-block text-gray"><b class="text-highlight">' + (airport_domestic_list_json[i].City == null ? airport_domestic_list_json[i].Name : airport_domestic_list_json[i].City) + '</b>, ' + airport_domestic_list_json[i].Country + '</span><span class="display-block text-sm text-left text-light-gray">' + airport_domestic_list_json[i].Code + ' - ' + airport_domestic_list_json[i].Name + '</span></a>' });
                    $li.append($dvl);
                    $li.append($dvd);
                    $("ul#airport_code_list").append($li);
                }
                $("#airportlistloadingMR").hide();
                $("#airportlistMR").show();
            }
            else {
                $("#airportlistloadingMR").hide();
                $("#airportlistemptyMR").show();
            }
        }
        else {
            if ($("#domestic").prop('checked')) {
                if (airport_domestic_list_json.length > 0) {
                    $("ul#airport_code_list").empty();
                    for (var i = 0; i < airport_domestic_list_json.length ; i++) {
                        var $li = $("<li>");
                        var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + virtualPath + 'img/icons/control-tower.png" class="max-width-50p" />' });
                        var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-airport" data-code="' + airport_domestic_list_json[i].Code + '" data-name="' + airport_domestic_list_json[i].Name + '" data-city="' + airport_domestic_list_json[i].City + '" data-country="' + airport_domestic_list_json[i].Country + '"><span class="text-left text-md display-block text-gray"><b class="text-highlight">' + (airport_domestic_list_json[i].City == null ? airport_domestic_list_json[i].Name : airport_domestic_list_json[i].City) + '</b>, ' + airport_domestic_list_json[i].Country + '</span><span class="display-block text-sm text-left text-light-gray">' + airport_domestic_list_json[i].Code + ' - ' + airport_domestic_list_json[i].Name + '</span></a>' });
                        $li.append($dvl);
                        $li.append($dvd);
                        $("ul#airport_code_list").append($li);
                    }
                    $("#airportlistloadingMR").hide();
                    $("#airportlistMR").show();
                }
                else {
                    $("#airportlistloadingMR").hide();
                    $("#airportlistemptyMR").show();
                }
            }
            else {
                if (airport_international_list_json.length > 0) {
                    $("ul#airport_code_list").empty();
                    for (var i = 0; i < step ; i++) {
                        var $li = $("<li>");
                        var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + virtualPath + 'img/icons/control-tower.png" class="max-width-50p" />' });
                        var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-airport" data-code="' + airport_international_list_json[i].Code + '" data-name="' + airport_international_list_json[i].Name + '" data-city="' + airport_international_list_json[i].City + '" data-country="' + airport_international_list_json[i].Country + '"><span class="text-left text-md display-block text-gray"><b class="text-highlight">' + (airport_international_list_json[i].City == null ? airport_international_list_json[i].Name : airport_international_list_json[i].City) + '</b>, ' + airport_international_list_json[i].Country + '</span><span class="display-block text-sm text-left text-light-gray">' + airport_international_list_json[i].Code + ' - ' + airport_international_list_json[i].Name + '</span></a>' });
                        $li.append($dvl);
                        $li.append($dvd);
                        $("ul#airport_code_list").append($li);
                    }
                    $("#airportlistloadingMR").hide();
                    $("#airportlistMR").show();
                }
                else {
                    $("#airportlistloadingMR").hide();
                    $("#airportlistemptyMR").show();
                }
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'LoadAirports');
    }
}

function SetTravellerList() {
    if ($("#travellers").val().indexOf('~') != -1) {
        var arr = $("#travellers").val().split('~');
        $("#adult_counter").find('.input-number').val(arr[0]).trigger("change");
        $("#child_counter").find('.input-number').val(arr[1]).trigger("change");
        if (arr.length > 2) {
            $("#infant_counter").find('.input-number').val(arr[2]).trigger("change");
        }
    }
    else {
        $("#adult_counter").find('.input-number').val($("#travellers").val()).trigger("change");
        $("#child_counter").find('.input-number').val('0').trigger("change");
        $("#infant_counter").find('.input-number').val('0').trigger("change");
    }
    //TravellerList();
}

function RoundTrip(flag) {
    if (flag) {
        $("#divDepartureDate").animate({ width: '49%' }, function (e) {
            $("#divArrivalDate").fadeIn();
        });
    }
    else {
        $("#divArrivalDate").fadeOut('fast', function (e) {
            $("#divDepartureDate").animate({ width: '100%' });
        });
    }
    $("#ArrivalDate").val('').trigger('change');
    $("label[for='ArrivalDate']").text('Arrival date');
}

function CloseAirportPopup() {
    $('#airport_list_to_search').addClass('slideOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
        $('#airport_list_to_search').hide();
    });
    $('#airport_list_to_search').removeClass('slideInUp');
}

function TravellerList() {
    //console.log('TravellerList');
    var txt = '1 Adult';
    var val = '';
    var tot = 0;
    if (parseInt($("#adult_counter").find('.input-number').val()) > 0) {
        txt = $("#adult_counter").find('.input-number').val() + ($("#adult_counter").find('.input-number').val() == 1 ? ' Adult' : ' Adults');
        val = $("#adult_counter").find('.input-number').val();
        tot = parseInt($("#adult_counter").find('.input-number').val());
        if (parseInt($("#child_counter").find('.input-number').val()) > 0) {
            txt += ', ' + $("#child_counter").find('.input-number').val() + ($("#child_counter").find('.input-number').val() == 1 ? ' Child' : ' Children');
            val += '~' + $("#child_counter").find('.input-number').val();
            tot += parseInt($("#child_counter").find('.input-number').val());
        }

        if (parseInt($("#infant_counter").find('.input-number').val()) > 0) {
            txt += ', ' + $("#infant_counter").find('.input-number').val() + ($("#infant_counter").find('.input-number').val() == 1 ? ' Infant' : ' Infants');
            val += '~' + $("#infant_counter").find('.input-number').val();
            tot += parseInt($("#infant_counter").find('.input-number').val());
        }
    }
    $("a#travellerlist h4").html(txt);
    $("#travellers").val(val);
    $("#totaltravellers").val(tot);
}

$(document).ready(function (e) {
    try {
        if (LOGGED_IN_ID != null) {
            if (LOGGED_IN_NAME == null) {
                profileflagFB = true;
            }
            else {
                if (LOGGED_IN_NAME == '') {
                    profileflagFB = true;
                }
                else {
                    if (LOGGED_IN_EMAIL == null) {
                        profileflagFB = true;
                    }
                    else {
                        if (LOGGED_IN_EMAIL == '') {
                            profileflagFB = true;
                        }
                        else {
                            if (LOGGED_IN_MOBILENO == null) {
                                profileflagFB = true;
                            }
                            else {
                                if (LOGGED_IN_MOBILENO == '') {
                                    profileflagFB = true;
                                }
                            }
                        }
                    }
                }
            }
            if (profileflagFB) {
                notification(profile_update_message.replace('<br /><br />', '\n\n'), null);
            }
        }
        if (localStorage.getItem("domestic") != null) {
            $("#domestic").prop('checked', (localStorage.getItem("domestic") == 1 ? true : false));
            $("#international").prop('checked', (localStorage.getItem("domestic") == 2 ? true : false));
            localStorage.removeItem("domestic");
        }
        else {
            $("#domestic").prop('checked', true);
            $("#adult_counter").find('.input-number').val('1');
            $("#child_counter").find('.input-number').val('0');
            $("#infant_counter").find('.input-number').val('0');
        }
        if (localStorage.getItem("FromCode") != null) {
            $("#FromCode").val(localStorage.getItem("FromCode"));
            var objDep = $.grep(airport_domestic_list_json, function (e) { return e.Code == $("#FromCode").val(); });
            //console.log(objDep);
            if (objDep.length > 0) {
                $("a#fromcodeanchor h3").html(objDep[0].Code);
                var small = objDep[0].Name;
                if (small.length > 25)
                    small = objDep[0].Name.substr(0, 22) + '...';
                $("a#fromcodeanchor small").html(small);
            }
            localStorage.removeItem("FromCode");
            localStorage.removeItem("FromAirport");
        }
        if (localStorage.getItem("ToCode") != null) {
            $("#ToCode").val(localStorage.getItem("ToCode"));
            var objArr = [];
            if ($("#domestic").prop('checked'))
                objArr = $.grep(airport_domestic_list_json, function (e) { return e.Code == $("#ToCode").val(); });
            else
                objArr = $.grep(airport_international_list_json, function (e) { return e.Code == $("#ToCode").val(); });
            //console.log(objArr);
            if (objArr.length > 0) {
                $("a#tocodeanchor h3").html(objArr[0].Code);
                var small = objArr[0].Name;
                if (small.length > 25)
                    small = objArr[0].Name.substr(0, 22) + '...';
                $("a#tocodeanchor small").html(small);
            }
            localStorage.removeItem("ToCode");
            localStorage.removeItem("ToAirport");
        }
        if (localStorage.getItem("roundtrip") != null) {
            $("#roundtrip").prop('checked', (localStorage.getItem("roundtrip") == 1 ? true : false));
            RoundTrip($("#roundtrip").prop('checked'));
            localStorage.removeItem("roundtrip");
        }
        if (localStorage.getItem("DepartureDate") != null) {
            $("#DepartureDate").val(localStorage.getItem("DepartureDate"));
            $("label[for='DepartureDate']").text(localStorage.getItem("DepartureDateLabel"));
            localStorage.removeItem("DepartureDate");
            localStorage.removeItem("DepartureDateLabel");
        }
        if (localStorage.getItem("ArrivalDate") != null) {
            $("#ArrivalDate").val(localStorage.getItem("ArrivalDate"));
            $("label[for='ArrivalDate']").text(localStorage.getItem("ArrivalDateLabel"));
            localStorage.removeItem("ArrivalDate");
            localStorage.removeItem("ArrivalDateLabel");
        }
        if (localStorage.getItem("travellers") != null) {
            $("#travellers").val(localStorage.getItem("travellers"));
            $("#totaltravellers").val(localStorage.getItem("totaltravellers"));
            localStorage.removeItem("travellers");
            localStorage.removeItem("totaltravellers");
            SetTravellerList();
            TravellerList();
        }
        if (localStorage.getItem("cabinclass") != null) {
            $("#economyclass").prop('checked', (localStorage.getItem("cabinclass") == 'E' ? true : false));
            $("#businessclass").prop('checked', (localStorage.getItem("cabinclass") == 'B' ? true : false));
            localStorage.removeItem("cabinclass");
        }

        LoadAirports(true);

        if (localStorage.getItem("onwardOptions") != null)
            localStorage.removeItem("onwardOptions");

        if (localStorage.getItem("returnOptions") != null)
            localStorage.removeItem("returnOptions");

        if (localStorage.getItem("onwardflight") != null)
            localStorage.removeItem("onwardflight");

        if (localStorage.getItem("returnflight") != null)
            localStorage.removeItem("returnflight");

        if (localStorage.getItem("flighttotalfare") != null)
            localStorage.removeItem("flighttotalfare");

        $('#DepartureDateContainer').datetimepicker({ format: 'DD-MM-YYYY', minDate: new Date(minDt.getFullYear(), minDt.getMonth(), minDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
        $('#DepartureDateContainer').on('dp.change', function (e) {
            var dt1 = 'Departure date', dt2 = '';
            //console.log(e.date);
            if (e.date) {
                dt1 = e.date.format('ddd, DD MMM');
                dt2 = e.date.format('DD-MM-YYYY');
            }
            $("label[for='DepartureDate']").html(dt1);
            $("#DepartureDate").val(dt2);
            if (dt2 != '')
                $(this).closest('.form-group').removeClass('is-empty');
            $(this).datetimepicker('hide');
        });
        $('#ArrivalDateContainer').datetimepicker({ format: 'DD-MM-YYYY', minDate: new Date(minDt.getFullYear(), minDt.getMonth(), minDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'right', vertical: 'auto' } });
        $('#ArrivalDateContainer').on('dp.change', function (e) {
            var dt1 = 'Arrival date', dt2 = '';
            //console.log(e.date);
            if (e.date) {
                dt1 = e.date.format('ddd, DD MMM');
                dt2 = e.date.format('DD-MM-YYYY');
            }
            $("label[for='ArrivalDate']").html(dt1);
            $("#ArrivalDate").val(dt2);
            if (dt2 != '')
                $(this).closest('.form-group').removeClass('is-empty');
            $(this).datetimepicker('hide');
        });

        $('form#frmflightbooking').validate({
            rules: {
                FromCode: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9A-Za-z]+$/,
                    rangelength: [2, 5]
                },
                ToCode: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9A-Za-z]+$/,
                    rangelength: [2, 5]
                },
                DepartureDate: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[0-9\-]+$/,
                    rangelength: [10, 10]
                },
                ArrivalDate: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return $("#roundtrip").prop('checked');
                        }
                    },
                    regex: /^[0-9\-]+$/,
                    rangelength: [10, 10],
                },
                travellers: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    rangelength: [1, 10]
                },
            },
            messages: {
                FromCode: { required: "Select Departure city" },
                ToCode: { required: "Select Arrival city" },
                DepartureDate: { required: "Select a Departure date" },
                ArrivalDate: { required: "Select a Arrival date" },
                travellers: { required: "Please select Travellers" },
            }
        });

        $('form#frmflightbooking').on('submit', function (e) {
            try {
                e.preventDefault();
                var isvalidate = $(this).valid();
                if ($("#roundtrip").prop('checked') && $("#DepartureDate").val().trim() != '' && $("#ArrivalDate").val().trim() != '') {
                    if (ConvertStringToDateTime($("#DepartureDate").val().trim(), '') > ConvertStringToDateTime($("#ArrivalDate").val().trim(), '')) {
                        //bootalert('Arrival date should be greater than or equal to Departure date.', 'error');
                        notification('Arrival date should be greater than or equal to Departure date.', null);
                        isvalidate = false;
                    }
                }
                if (LOGGED_IN_ID != null) {
                    if (LOGGED_IN_NAME == null) {
                        profileflagFB = true;
                    }
                    else {
                        if (LOGGED_IN_NAME == '') {
                            profileflagFB = true;
                        }
                        else {
                            if (LOGGED_IN_EMAIL == null) {
                                profileflagFB = true;
                            }
                            else {
                                if (LOGGED_IN_EMAIL == '') {
                                    profileflagFB = true;
                                }
                                else {
                                    if (LOGGED_IN_MOBILENO == null) {
                                        profileflagFB = true;
                                    }
                                    else {
                                        if (LOGGED_IN_MOBILENO == '') {
                                            profileflagFB = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (profileflagFB) {
                        notification(profile_update_message.replace('<br /><br />', '\n\n'), null);
                        isvalidate = false;
                    }
                }
                if (isvalidate) {
                    localStorage.setItem("domestic", $("#domestic").prop('checked') ? 1 : 2);
                    localStorage.setItem("FromCode", $("#FromCode").val().trim());
                    localStorage.setItem("ToCode", $("#ToCode").val().trim());
                    localStorage.setItem("FromAirport", $("#fromcodeanchor small").text().trim());
                    localStorage.setItem("ToAirport", $("#tocodeanchor small").text().trim());
                    localStorage.setItem("roundtrip", $("#roundtrip").prop('checked') ? 1 : 0);
                    localStorage.setItem("DepartureDate", $("#DepartureDate").val().trim());
                    localStorage.setItem("ArrivalDate", $("#ArrivalDate").val().trim());
                    localStorage.setItem("DepartureDateLabel", $("label[for='DepartureDate']").text());
                    localStorage.setItem("ArrivalDateLabel", $("label[for='ArrivalDate']").text());
                    localStorage.setItem("travellers", $("#travellers").val().trim());
                    localStorage.setItem("totaltravellers", $("#totaltravellers").val().trim());
                    localStorage.setItem("cabinclass", $("#economyclass").prop('checked') ? 'E' : ($("#businessclass").prop('checked') ? 'B' : 'P'));
                    if ($("#domestic").prop('checked'))
                        window.location = virtualPath + "Account/FlightSearchResults";
                    else
                        window.location = virtualPath + "Account/FlightSearchResultsInternational";
                }
            } catch (e) {
                alertException(e, current_page_id, 'frmflightbooking.submit');
            }

        });
    } catch (e) {
        alertException(e, current_page_id, 'Page.Load');
    }


    $("#roundtrip").on('click', function (e) {
        try {
            //console.log($(this).prop('checked'));
            RoundTrip($(this).prop('checked'));
        } catch (e) {
            alertException(e, current_page_id, '#roundtrip.click');
        }
    });

    $("input:radio[name='domestictrip']").on('change', function (e) {
        try {

            //$("a#fromcodeanchor h3").html('N.A.');
            //$("a#fromcodeanchor small").html('Select Departure city');
            //$("#FromCode").val('');

            $("a#tocodeanchor h3").html('N.A.');
            $("a#tocodeanchor small").html('Select Arrival city');
            $("#ToCode").val('');
        } catch (e) {
            alertException(e, current_page_id, 'input:radio[name=domestictrip].click');
        }
    });

    $("#searchairport").on('focusin', function (e) {
        try {
            var val = $(this).val().trim() == 'Enter ' + (deparr == 1 ? 'Departure' : 'Arrival') + ' City' ? '' : $(this).val().trim();
            $(this).val(val);
        } catch (e) {
            alertException(e, current_page_id, 'a.swap.click');
        }
    });

    $("#searchairport").on('keyup', function (e) {
        try {
            var len = $("#searchairport").val().trim().length;
            if (len > 1) {
                overwritelist = false;
                $("#airportlistloadingMR").show();
                $("#airportlistemptyMR").hide();
                $("#airportlistMR").hide();
                var searchTerm = $("#searchairport").val().toLowerCase();
                //console.log(searchTerm);
                var filteredList = $.grep(airport_domestic_list_json, function (e) { return (e.Name.toLowerCase().substr(0, len) == searchTerm || e.Code.toLowerCase().substr(0, len) == searchTerm || (e.City == null ? '' : e.City).toLowerCase().substr(0, len) == searchTerm || e.Country.toLowerCase().substr(0, len) == searchTerm); });
                if (deparr == 2 && !$("#domestic").prop('checked'))
                    filteredList = $.grep(airport_international_list_json, function (e) { return (e.Name.toLowerCase().substr(0, len) == searchTerm || e.Code.toLowerCase().substr(0, len) == searchTerm || (e.City == null ? '' : e.City).toLowerCase().substr(0, len) == searchTerm || e.Country.toLowerCase().substr(0, len) == searchTerm); });
                //console.log(filteredList);
                if (filteredList.length > 0) {
                    $("ul#airport_code_list").empty();
                    for (var i = 0; i < filteredList.length ; i++) {
                        var $li = $("<li>");
                        var $dvl = $("<div>", { 'class': 'logo', html: '<img src="' + virtualPath + 'img/icons/control-tower.png" class="max-width-50p" />' });
                        var $dvd = $("<div>", { 'class': 'details', html: '<a href="#" class="select-airport" data-code="' + filteredList[i].Code + '" data-name="' + filteredList[i].Name + '" data-city="' + filteredList[i].City + '" data-country="' + filteredList[i].Country + '"><span class="text-left text-md display-block text-gray"><b class="text-highlight">' + (filteredList[i].City == null ? filteredList[i].Name : filteredList[i].City) + '</b>, ' + filteredList[i].Country + '</span><span class="display-block text-sm text-left text-light-gray">' + filteredList[i].Code + ' - ' + filteredList[i].Name + '</span></a>' });
                        $li.append($dvl);
                        $li.append($dvd);
                        $("ul#airport_code_list").append($li);
                    }
                    $("#airportlistloadingMR").hide();
                    $("#airportlistMR").show();
                }
                else {
                    $("#airportlistloadingMR").hide();
                    $("#airportlistemptyMR").show();
                }
            }
            else {
                if (!overwritelist) {
                    overwritelist = true;
                    LoadAirports(false);
                }
            }
        } catch (e) {
            alertException(e, current_page_id, 'a.swap.click');
        }
    });

    $("#searchairport").on('blur', function (e) {
        try {
            var val = $(this).val().trim() == '' ? 'Enter ' + (deparr == 1 ? 'Departure' : 'Arrival') + ' City' : $(this).val().trim();
            $(this).val(val);
        } catch (e) {
            alertException(e, current_page_id, '#searchairport.click');
        }
    });

    $('a.clear-textbox').on('click', function (e) {
        try {
            $(this).closest(".search-wrapper").find('input').val('');
            LoadAirports(true);
        } catch (e) {
            alertException(e, current_page_id, 'a.clear-textbox.click');
        }
    });

    $("a.swap").on('click', function (e) {
        try {
            $(this).find('i').toggleClass('flip');
            var a1 = $("a#fromcodeanchor h3").html();
            var a2 = $("a#fromcodeanchor small").html();
            var a3 = $("#FromCode").val();

            var b1 = $("a#tocodeanchor h3").html();
            var b2 = $("a#tocodeanchor small").html();
            var b3 = $("#ToCode").val();

            $("a#fromcodeanchor").fadeOut('fast');
            $("a#tocodeanchor").fadeOut('fast', function (e) {
                $("a#fromcodeanchor h3").html(b1);
                $("a#fromcodeanchor small").html(b2);
                $("#FromCode").val(b3);

                $("a#tocodeanchor h3").html(a1);
                $("a#tocodeanchor small").html(a2);
                $("#ToCode").val(a3);

                $("a#fromcodeanchor").fadeIn('fast');
                $("a#tocodeanchor").fadeIn('fast');
            });
        } catch (e) {
            alertException(e, current_page_id, 'a.swap.click');
        }
    });

    $('#fromcodeanchor').on('click', function (e) {
        try {
            e.preventDefault();
            deparr = 1;

            $("#searchairport").val('Enter ' + (deparr == 1 ? 'Departure' : 'Arrival') + ' City');
            $("#airportlistloadingMR").show();
            $("#airportlistemptyMR").hide();
            $("#airportlistMR").hide();

            $('#airport_list_to_search').show();
            $('#airport_list_to_search').addClass('slideInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                $('#airport_list_to_search').show();
                LoadAirports(true);
            });
            $('#airport_list_to_search').removeClass('slideOutDown');
        } catch (e) {
            alertException(e, current_page_id, '#fromcodeanchor.click');
        }
    });

    $('#tocodeanchor').on('click', function (e) {
        try {
            e.preventDefault();
            deparr = 2;

            $("#searchairport").val('Enter ' + (deparr == 1 ? 'Departure' : 'Arrival') + ' City');
            $("#airportlistloadingMR").show();
            $("#airportlistemptyMR").hide();
            $("#airportlistMR").hide();

            $('#airport_list_to_search').show();
            $('#airport_list_to_search').addClass('slideInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                $('#airport_list_to_search').show();
                LoadAirports(true);
            });
            $('#airport_list_to_search').removeClass('slideOutDown');
        } catch (e) {
            alertException(e, current_page_id, '#tocodeanchor.click');
        }
    });

    $('.airport-code-list.close-popup-over-list').on('click', function (e) {
        try {
            CloseAirportPopup();
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, '.airport-code-list.close-popup-over-list.click');
        }
    });

    $(document).on('click', 'a.select-airport', function (e) {
        try {
            var code = $(this).data('code');
            var name = $(this).data('name');
            var city = $(this).data('city');
            var country = $(this).data('country');
            var small = name;
            if (small.length > 25)
                small = name.substr(0, 22) + '...';
            //console.log('code - ' + code + '  ,  name - ' + name + '  ,  city - ' + city + '  ,  country - ' + country);
            if (deparr == 1) {
                if (code != $("#ToCode").val()) {
                    $("a#fromcodeanchor h3").html(code);
                    $("a#fromcodeanchor small").html(small);
                    $("#FromCode").val(code);
                }
                else {
                    alertme('Departure city and Arrival city cannot be same.');
                    return false;
                }
            }
            else {
                if (code != $("#FromCode").val()) {
                    $("a#tocodeanchor h3").html(code);
                    $("a#tocodeanchor small").html(small);
                    $("#ToCode").val(code);
                }
                else {
                    alertme('Departure city and Arrival city cannot be same.');
                    return false;
                }
            }
            CloseAirportPopup();
        } catch (e) {
            alertException(e, current_page_id, 'a.select-airport.click');
        }
    });

    $('a#travellerlist').on('click', function (e) {
        try {
            $("#modal_traveller_list").modal('show');
            $(".modal-backdrop").hide();
            SetTravellerList();
        } catch (e) {
            alertException(e, current_page_id, 'a#travellerlist.click');
        }
    });

    $('.btn-number').on('click', function (e) {
        try {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        } catch (e) {
            alertException(e, current_page_id, '.btn-number.click');
        }
    });

    $('.input-number').on('focusin', function (e) {
        try {
            $(this).data('oldValue', $(this).val());
        } catch (e) {
            alertException(e, current_page_id, '.input-number.focusin');
        }
    });

    $('.input-number').on('change', function (e) {
        try {
            //console.log('.input-number.change');
            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            if (isNaN($(this).val().trim())) {
                alert('Sorry, only numbers are allowed');
                $(this).val($(this).data('oldValue'));
                $(this).focus();
            }
            else if ($(this).val().indexOf('.') != -1) {
                alert('Sorry, decimal values not allowed');
                $(this).val($(this).data('oldValue'));
                $(this).focus();
            }
            else {
                valueCurrent = parseInt($(this).val());
                name = $(this).attr('name');
                if (valueCurrent >= minValue) {
                    $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled');

                    if (valueCurrent <= maxValue) {
                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled');
                    } else {
                        //alert('Sorry, the maximum value was reached');
                        $(this).val($(this).data('oldValue'));
                    }
                } else {
                    //alert('Sorry, the minimum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
            }

        } catch (e) {
            alertException(e, current_page_id, '.input-number.change');
        }
    });

    $(".input-number").on('keydown', function (e) {
        try {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        } catch (e) {
            alertException(e, current_page_id, '.input-number.keydown');
        }
    });

    $('#close_traveller_modal').on('click', function (e) {
        try {
            e.preventDefault();
            var continueflag = true;
            var adult = parseInt($("#adult_counter").find('.input-number').val());
            var child = parseInt($("#child_counter").find('.input-number').val());
            var infant = parseInt($("#infant_counter").find('.input-number').val());
            if ((adult + child) > 9) {
                $("#traveller_error_message").html('Maximum 9 travellers can be booked at a time');
                $("#traveller_error_message").slideDown();
                setTimeout(function () {
                    $("#traveller_error_message").slideUp('fast', function (e) {
                        $("#traveller_error_message").html('');
                    });
                }, 5000);
                continueflag = false;
            }
            if (continueflag && infant > adult) {
                $("#traveller_error_message").html('Number of Infants cannot exceed number of Adults');
                $("#traveller_error_message").slideDown();
                setTimeout(function () {
                    $("#traveller_error_message").slideUp('fast', function (e) {
                        $("#traveller_error_message").html('');
                    });
                }, 5000);
                continueflag = false;
            }
            if (continueflag) {
                TravellerList();
                $("#modal_traveller_list").modal('hide');
            }
        } catch (e) {
            alertException(e, current_page_id, '#close_traveller_modal.click');
        }
    });

    $('#cancel_traveller_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_traveller_list").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#cancel_traveller_modal.click');
        }
    });
});