﻿current_page_id = "#bussearchresults";
var originId = '', originCity = '', destinationId = '', destinationCity = '', departDate = '', returnDate = '', departDateLabel = '', returnDateLabel = '', mode = '', airline = '', no_of_seats = 0, totaltravellers = 0;
cashback_list_json = [];
function SearchBuses() {
    try {
        localStorage.removeItem("bus_onwardOptions");
        localStorage.removeItem('bcb-code');
        localStorage.removeItem('bcb-description');
        localStorage.removeItem('bcb-amount');
        localStorage.removeItem("bus_totalfare");
        localStorage.removeItem("bus_boarding_point_id");
        localStorage.removeItem("bus_boarding_point");
        localStorage.removeItem("bus_seats");
        localStorage.removeItem("bus_seats_info");
        localStorage.removeItem("bus_cashbackcode");
        localStorage.removeItem("bus_cashbackamount");

        $("#onward_bus_list_header").fadeOut();
        $("#ul_onward_bus_list").fadeOut();
        $("#onwardlistemptyBSR").fadeOut('fast', function (e) {
            $("#onwardlistloadingBSR").fadeIn();
        });
        if (localStorage.getItem("bus_FromCityId") != null) {
            originId = localStorage.getItem("bus_FromCityId");
            destinationId = localStorage.getItem("bus_ToCityId");
            originCity = localStorage.getItem("bus_FromCity");
            destinationCity = localStorage.getItem("bus_ToCity");
            departDate = localStorage.getItem("bus_DepartureDate");
            returnDate = localStorage.getItem("bus_ArrivalDate");
            departDateLabel = localStorage.getItem("bus_DepartureDateLabel");
            returnDateLabel = localStorage.getItem("bus_ArrivalDateLabel");
            mode = localStorage.getItem("bus_roundtrip") == 0 ? 'ONE' : 'ROUND';
            totaltravellers = localStorage.getItem("bus_NoOfSeats");
            $(".bus-page-title").html(localStorage.getItem("bus_FromCity") + '&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;' + localStorage.getItem("bus_ToCity") + ' (' + localStorage.getItem("bus_DepartureDateLabel") + (localStorage.getItem("bus_roundtrip") == 0 ? '' : (' - ' + localStorage.getItem("bus_ArrivalDateLabel"))) + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + ')');

            //$("span.onward-title").html("<span class='bold'>DEPARTURE</span> (" + originCity + " - " + destinationCity + ' | ' + departDateLabel + ')');
            //if (mode == 'ROUND') {
            //    $("span.return-title").html("<span class='bold'>RETURN</span> (" + destinationCity + " - " + originCity + ' | ' + returnDateLabel + ')');
            //}

            var paramList = {};
            paramList.s = originId + "|" + destinationId + "|" + departDate;
            //console.log(paramList.s);
            paramList.s = StringEncryption(paramList.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/SearchBuses",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    try {
                        //var r = JSON.parse(StringDecryption(data).split('|')[0]);
                        var r = StringDecryption(data);
                        var json = JSON.parse(r.split('|')[0]);
                        cashback_list_json = JSON.parse(r.split('|')[1]);
                        //console.log('----  ONWARD TRIPS ------------------------------------------');
                        //console.log(json.trips);
                        GenerateBusList('onward', json.trips, "ul#ul_onward_bus_list", function () {
                            //console.log('------onward result calback executed--------------');
                            $("#onwardlistloadingBSR").fadeOut('fast', function (e) {
                                $("#onward_bus_list_header").fadeIn();
                                $("#ul_onward_bus_list").fadeIn();
                            });
                        }, function () {
                            //console.log('------onward empty calback executed--------------');
                            $("#onwardlistloadingBSR").fadeOut('fast', function (e) {
                                $("#onwardlistemptyBSR").fadeIn();
                            });
                        });
                    } catch (e) {
                        //console.log(e);
                        alertException(e, current_page_id, 'SearchBuses.success');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#onward_search_error_message").html(errorThrown);
                    $("#onwardlistloadingBSR").fadeOut('fast', function (e) {
                        $("#onwardlistemptyBSR").fadeIn();
                    });
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
            });
        }
        else
            window.location = virtualPath + "Account/BusBooking";
    } catch (e) {
        alertException(e, current_page_id, 'Page.Load');
    }
}
SearchBuses();

function GenerateBusList(type, trips, obj, resultcallback, emptycallback) {
    try {
        //console.log(trips);
        if (trips.length > 0) {
            //console.log('cashback_list_json  ----------------');
            //console.log(cashback_list_json);
            var cashback = {};
            cashback.code = (cashback_list_json.length > 0 ? cashback_list_json[0].Code : '');
            cashback.description = (cashback_list_json.length > 0 ? cashback_list_json[0].Description : '');
            for (var i = 0; i < trips.length; i++) {
                if (trips[i].vehicle_type == 'BUS' && no_of_seats <= parseInt(trips[i].available_seats)) {
                    var departureTime = ConvertStringToDateTime(trips[i].departure_time, 'hhmmtt');
                    var arrivalTime = ConvertStringToDateTime(trips[i].arrival_time, 'hhmmtt');
                    var duration = BusDuration(ConvertStringToDateTime(trips[i].departure_time, ''), ConvertStringToDateTime(trips[i].arrival_time, ''));
                    var fare_details = trips[i].fare_details;
                    fare_details.sort(function (a, b) {
                        return parseFloat(b.total_fare) - parseFloat(a.total_fare);
                    });
                    var totalFare = totaltravellers * parseFloat(fare_details[0].total_fare);
                    var cashbackAmt = 0;
                    if (cashback_list_json.length > 0) {
                        cashbackAmt = parseFloat(totalFare) * parseFloat(cashback_list_json[0].Percentage) / 100;
                        if (parseFloat(cashback_list_json[0].MaxAmount) > 0 && cashbackAmt > parseFloat(cashback_list_json[0].MaxAmount))
                            cashbackAmt = parseFloat(cashback_list_json[0].MaxAmount);
                        cashbackAmt = Math.floor(cashbackAmt);
                    }
                    cashback.amount = cashbackAmt;

                    var $li = $("<li>", { 'class': 'bg-white card_content_shadow margin-bottom-5 no-border', 'data-tripid': trips[i].trip_id, 'data-operator': trips[i].travels_name, 'data-departuredatetime': ConvertStringToDateTime(trips[i].departure_time, ''), 'data-arrivaldatetime': ConvertStringToDateTime(trips[i].arrival_time, ''), 'data-departuretime': departureTime, 'data-arrivaltime': arrivalTime, 'data-duration': (ConvertStringToDateTime(trips[i].arrival_time, '') - ConvertStringToDateTime(trips[i].departure_time, '')), 'data-totalfare': totalFare, 'data-tripinfo': StringEncryption(JSON.stringify(trips[i])) });
                    var html = '';
                    html += '<div>';
                    html += '<div class="pull-left padding-5" style="width:75%;padding-bottom:0!important">';
                    html += '<div class="display-inline-block"><b class="text-lg">' + departureTime + '</b>';
                    html += '<small class="center-block text-gray">' + originCity + '</small>';
                    html += '</div>';
                    html += '<div class="display-inline-block"><img src="' + virtualPath + 'img/icons/bus-gray.png" style="max-width:21px;margin:0 10px" />';
                    html += '<small class="center-block text-gray">&nbsp;</small>';
                    html += '</div>';
                    html += '<div class="display-inline-block"><b class="text-lg">' + arrivalTime + '</b>';
                    html += '<small class="center-block text-gray">' + destinationCity + '</small>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="pull-right amount padding-5 bg-danger text-center text-bold text-lg" style="width:25%;background: #FFE95F;"><i class="fa fa-rupee"></i> ' + parseFloat(totalFare).toLocaleString('en-IN') + '</div>';
                    html += '<small class="center-block">' + duration + '</small>';
                    html += '<div class="clearfix"></div>';
                    html += '</div>';
                    html += '<div>';
                    html += '<div class="pull-left padding-left-5" style="width:75%">';
                    html += '<small class="bold">' + trips[i].travels_name + '</small>';
                    html += '<small class="center-block text-left text-light-gray">' + trips[i].bus_type + '</small>';
                    html += '</div>';
                    html += '<div class="pull-right center-block text-center" style="width:25%">';
                    html += '<b>' + trips[i].available_seats + '</b><br />';
                    html += '<small class="text-light-gray">seats</small>';
                    html += '</div>';
                    html += '<div class="clearfix"></div>';
                    html += '</div>';
                    html += '<div>';
                    html += '<div class="pull-left padding-left-5" style="width:75%">';
                    if (trips[i].boarding_points.length > 0) {
                        html += '<small class="text-left"><a href="#" class="stop-points" data-type="1" data-list="' + StringEncryption(JSON.stringify(trips[i].boarding_points)) + '"><img src="' + virtualPath + 'img/icons/bus-location.png" style="max-width:11px" /> Boarding (' + trips[i].boarding_points.length + ')</a></small>';
                        if (trips[i].dropping_points.length > 0)
                            html += '&nbsp;|&nbsp;<small class="text-left"><a href="#" class="stop-points" data-type="2" data-list="' + StringEncryption(JSON.stringify(trips[i].dropping_points)) + '"> <img src="' + virtualPath + 'img/icons/bus-location.png" style="max-width:11px" /> Drop (' + trips[i].dropping_points.length + ')</a></small>';
                    }
                    html += '</div>';
                    html += '<div class="pull-right center-block text-center" style="width:25%">';
                    //html += '<small ' + (trips[i].partial_cancellation_allowed == 'true' ? 'class="text-success">Refundable' : 'class="text-danger">Non-Refundable') + '</small><br />';
                    if (trips[i].cancellation_policy.length > 0) {
                        html += '<a href="#" class="cancellation-policy" data-i="' + (i + 1) + '"><small>Policies</small></a>';
                    }
                    html += '</div>';
                    html += '<div class="clearfix"></div>';
                    html += '</div>';
                    if (trips[i].cancellation_policy.length > 0) {
                        html += '<div id="divPolicy_' + (i + 1) + '" style="display:none">';
                        html += '<ol class="margin-5" style="padding-left:15px">';
                        html += '<b class="text-sm" style="margin-left:-15px">Cancellation Policy:</b>';
                        for (var j = 0; j < trips[i].cancellation_policy.length; j++) {
                            html += '<li class="text-sm">';
                            html += (trips[i].cancellation_policy[j].from_time == '0' ? 'Upto ' : (trips[i].cancellation_policy[j].from_time + ' to ')) + trips[i].cancellation_policy[j].to_time + ' - <b><i class="fa fa-rupee"></i> ' + trips[i].cancellation_policy[j].service_charge + '</b>';
                            html += '</li>';
                        }
                        html += '</ol>';
                        html += '</div>';
                    }
                    html += '<a href="#" class="btn btn-sm select-' + type + ' center-block text-center no-margin no-border-radius brown darken-1 text-white no-shadow position-relative" style="margin-top:10px!important" data-tripid="' + trips[i].trip_id + '" data-operatorid="' + trips[i].operator_id + '" data-totalfare="' + totalFare + '" data-tripinfo="' + StringEncryption(JSON.stringify(trips[i])) + '" data-cb-code="' + cashback.code + '" data-cb-description="' + cashback.description + '" data-cb-amount="' + cashback.amount + '">';
                    html += 'Book Now';
                    if (cashback.amount > 0) {
                        html += ' & Earn <i class="fa fa-rupee" style="font-size:100%;top:0"></i> ' + cashback.amount + ' eCash';
                    }
                    html += '</a>';
                    html += '</div>';
                    var $dvd = $("<div>", { 'class': 'details width-100p no-padding', html: html });
                    $li.append($dvd);
                    $(obj).append($li);
                }
            }

            var promise = SortList("#ul_onward_bus_list", 'totalfare', 'asc', 'number');
            $.when(promise).then(function (e) {
                //console.log('resultcallback executed-------------');
                // Make sure the callback is a function​
                if (typeof resultcallback === "function") {
                    //console.log('resultcallback executed-------------');
                    // Call it, since we have confirmed it is callable​
                    resultcallback();
                }
            });
        }
        else {
            //console.log('emptycallback executed-------------');
            // Make sure the callback is a function​
            if (typeof emptycallback === "function") {
                //console.log('emptycallback executed-------------');
                // Call it, since we have confirmed it is callable​
                emptycallback();
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'GenerateBusList');
    }
}

$(document).ready(function (e) {
    $("a.goto-bus-booking").on('click', function (e) {
        window.location = virtualPath + "Account/BusBooking";
    });

    $("a.retry-bus-search").on('click', function (e) {
        try {
            SearchBuses();
        } catch (e) {
            alertException(e, current_page_id, 'a.retry-make-payment.click');
        }
    });

    $("#onward_bus_list_header a.operator").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.duration").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_bus_list", 'operator', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_bus_list", 'operator', 'asc', 'text');
                else
                    SortList("#ul_onward_bus_list", 'operator', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_bus_list_header a.operator.click');
        }
    });

    $("#onward_bus_list_header a.departuredatetime").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_bus_list_header a.operator").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.duration").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_bus_list", 'departuredatetime', 'asc', 'text');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_bus_list", 'departuredatetime', 'asc', 'text');
                else
                    SortList("#ul_onward_bus_list", 'departuredatetime', 'desc', 'text');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_bus_list_header a.departuredatetime.click');
        }
    });

    $("#onward_bus_list_header a.duration").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_bus_list_header a.operator").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.totalfare").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_bus_list", 'duration', 'asc', 'number');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_bus_list", 'duration', 'asc', 'number');
                else
                    SortList("#ul_onward_bus_list", 'duration', 'desc', 'number');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_bus_list_header a.duration.click');
        }
    });

    $("#onward_bus_list_header a.totalfare").on('click', function (e) {
        try {
            if ($(this).find('i').css('opacity') == '0') {
                $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
                $("#onward_bus_list_header a.operator").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
                $("#onward_bus_list_header a.duration").find('i').css('opacity', 0);
                $(this).find('i').css('opacity', 1);
                SortList("#ul_onward_bus_list", 'totalfare', 'asc', 'number');
            }
            else {
                $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
                if ($(this).find('i').hasClass('fa-angle-up'))
                    SortList("#ul_onward_bus_list", 'totalfare', 'asc', 'number');
                else
                    SortList("#ul_onward_bus_list", 'totalfare', 'desc', 'number');
            }
        } catch (e) {
            alertException(e, current_page_id, '#onward_bus_list_header a.totalfare.click');
        }
    });

    //$("#return_bus_list_header a.operator").on('click', function (e) {
    //    try {
    //        if ($(this).find('i').css('opacity') == '0') {
    //            $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
    //            $("#return_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.duration").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.totalfare").find('i').css('opacity', 0);
    //            $(this).find('i').css('opacity', 1);
    //            SortList("#ul_return_bus_list", 'operator', 'asc', 'text');
    //        }
    //        else {
    //            $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
    //            if ($(this).find('i').hasClass('fa-angle-up'))
    //                SortList("#ul_return_bus_list", 'operator', 'asc', 'text');
    //            else
    //                SortList("#ul_return_bus_list", 'operator', 'desc', 'text');
    //        }
    //    } catch (e) {
    //        alertException(e, current_page_id, '#return_bus_list_header a.operator.click');
    //    }
    //});

    //$("#return_bus_list_header a.departuredatetime").on('click', function (e) {
    //    try {
    //        if ($(this).find('i').css('opacity') == '0') {
    //            $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
    //            $("#return_bus_list_header a.operator").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.duration").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.totalfare").find('i').css('opacity', 0);
    //            $(this).find('i').css('opacity', 1);
    //            SortList("#ul_return_bus_list", 'departuredatetime', 'asc', 'text');
    //        }
    //        else {
    //            $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
    //            if ($(this).find('i').hasClass('fa-angle-up'))
    //                SortList("#ul_return_bus_list", 'departuredatetime', 'asc', 'text');
    //            else
    //                SortList("#ul_return_bus_list", 'departuredatetime', 'desc', 'text');
    //        }
    //    } catch (e) {
    //        alertException(e, current_page_id, '#return_bus_list_header a.departuredatetime.click');
    //    }
    //});

    //$("#return_bus_list_header a.duration").on('click', function (e) {
    //    try {
    //        if ($(this).find('i').css('opacity') == '0') {
    //            $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
    //            $("#return_bus_list_header a.operator").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.totalfare").find('i').css('opacity', 0);
    //            $(this).find('i').css('opacity', 1);
    //            SortList("#ul_return_bus_list", 'duration', 'asc', 'number');
    //        }
    //        else {
    //            $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
    //            if ($(this).find('i').hasClass('fa-angle-up'))
    //                SortList("#ul_return_bus_list", 'duration', 'asc', 'number');
    //            else
    //                SortList("#ul_return_bus_list", 'duration', 'desc', 'number');
    //        }
    //    } catch (e) {
    //        alertException(e, current_page_id, '#return_bus_list_header a.duration.click');
    //    }
    //});

    //$("#return_bus_list_header a.totalfare").on('click', function (e) {
    //    try {
    //        if ($(this).find('i').css('opacity') == '0') {
    //            $(this).find('i').removeClass('fa-angle-down fa-angle-up').addClass('fa-angle-up');
    //            $("#return_bus_list_header a.operator").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.departuredatetime").find('i').css('opacity', 0);
    //            $("#return_bus_list_header a.duration").find('i').css('opacity', 0);
    //            $(this).find('i').css('opacity', 1);
    //            SortList("#ul_return_bus_list", 'totalfare', 'asc', 'number');
    //        }
    //        else {
    //            $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
    //            if ($(this).find('i').hasClass('fa-angle-up'))
    //                SortList("#ul_return_bus_list", 'totalfare', 'asc', 'number');
    //            else
    //                SortList("#ul_return_bus_list", 'totalfare', 'desc', 'number');
    //        }
    //    } catch (e) {
    //        alertException(e, current_page_id, '#return_bus_list_header a.totalfare.click');
    //    }
    //});

    $(document).on('click', "a.select-onward", function (e) {
        try {
            var _this = this;
            localStorage.setItem('bus_onwardOptions', $(this).data('tripinfo'));
            if ($(this).data('cb-code') != '') {
                localStorage.setItem('bcb-code', $(this).data('cb-code'));
                localStorage.setItem('bcb-description', $(this).data('cb-description'));
                localStorage.setItem('bcb-amount', $(this).data('cb-amount'));
            }
            window.location = virtualPath + "Account/BusReview";

        } catch (e) {
            alertException(e, current_page_id, 'a.select-onward.click');
        }
    });

    //$(document).on('click', "a.select-return", function (e) {
    //    try {
    //        var _this = this;
    //        localStorage.setItem('returnOptions', $(this).data('tripinfo'));
    //        window.location = virtualPath + "Account/BusReview";
    //    } catch (e) {
    //        alertException(e, current_page_id, 'a.select-onward.click');
    //    }
    //});

    $("a.goto-onward").on('click', function (e) {
        try {
            $('#return_bus_list').fadeOut('fast', function (e) {
                $('#onward_bus_list').fadeIn();
            });
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-onward.click');
        }
    });

    $(document).on('click', "a.cancellation-policy", function (e) {
        try {
            var i = $(this).data('i');
            $("#divPolicy_" + i).slideToggle("slow");
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-onward.click');
        }
    });

    $(document).on('click', 'a.stop-points', function (e) {
        try {
            var type = parseInt($(this).data('type'));
            //console.log('type - ' + type);
            $("#modal_stop_list").modal('hide');
            $("#stoplistloadingBSR").show();
            $("#stoplistemptyBSR").hide();
            $("#ulStopList").hide();
            $("#ulStopList").empty();
            var json = StringDecryption($(this).data('list'));
            //console.log(json);
            json = JSON.parse(json);
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    var html = '';
                    var name = (type == 1 ? json[i].bp_name : json[i].dp_name);
                    if (name == undefined || name == '')
                        name = '-'
                    var address = (type == 1 ? json[i].bp_address : json[i].dp_address);
                    if (address == undefined || address == '')
                        address = '-'
                    var contact_number = (type == 1 ? json[i].bp_contact_number : json[i].dp_contact_number);
                    if (contact_number == undefined || contact_number == '')
                        contact_number = '-'
                    var time = (type == 1 ? json[i].bp_time : json[i].dp_time);
                    if (time == undefined || time == '')
                        time = '-'
                    html += '<div class="padding-5 bold">&nbsp;<i class="fa fa-map-marker"></i>&nbsp;&nbsp;' + name + '</div>';
                    html += '<small class="display-block padding-lr-5">&nbsp;<i class="fa fa-map-pin"></i>&nbsp;&nbsp;' + address + '</small>';
                    html += '<small class="display-block padding-lr-5">&nbsp;<i class="fa fa-phone"></i>&nbsp;&nbsp;' + contact_number + '</small>';
                    html += '<small class="display-block padding-lr-5 text-danger">&nbsp;<i class="fa fa-clock-o"></i>&nbsp;&nbsp;' + time + '</small>';
                    var $li = $("<li>", { 'class': '', html: html });
                    $("#ulStopList").append($li);
                }
                $("#stoplistloadingBSR").fadeOut('fast', function (e) {
                    $("#ulStopList").fadeIn();
                });
            }
            else {
                $("#stoplistloadingBSR").fadeOut('fast', function (e) {
                    $("#stoplistemptyBSR").fadeIn();
                });
            }
            $("#modal_stop_list").modal('show');
            $(".modal-backdrop").hide();
        } catch (e) {
            alertException(e, current_page_id, 'a.stop-points.click');
        }
    });

    $('#close_stops_modal').on('click', function (e) {
        try {
            e.preventDefault();
            $("#modal_stop_list").modal('hide');
        } catch (e) {
            alertException(e, current_page_id, '#cancel_traveller_modal.click');
        }
    });
});