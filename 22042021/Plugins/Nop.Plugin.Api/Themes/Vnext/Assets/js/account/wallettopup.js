﻿current_page_id = "#wallettopup";

$(document).ready(function (e) {
    try {
        $('form#formWTP').validate({
            rules: {
                amountWTP: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    //regex: /^[a-zA-Z0-9\_]+$/,
                    regex: /^[0-9]+$/,
                    rangelength: [2, 10]
                },
                bankWTP: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_\ \\(\)\s]+$/,
                    rangelength: [2, 100]
                },
                branchWTP: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9\_\(\)\s]+$/,
                    rangelength: [2, 100]
                },
                ifscWTP: {
                    required: {
                        depends: function (e) {
                            $(this).val($(this).val().trim());
                            return true;
                        }
                    },
                    regex: /^[a-zA-Z0-9]+$/,
                    rangelength: [2, 15]
                },
                fileuploadWTP: { required: true },
            },
            messages: {
                amountWTP: {
                    required: "Please enter an Amount",
                    regex: "Only numbers are allowed",
                    rangelength: "Amount should be of {0} to {1} digit"
                },
                bankWTP: {
                    required: "Please enter a Bank name",
                    regex: "Only alphabets, number, space and _() are allowed",
                    rangelength: "Bank name should be of {0} to {1} characters"
                },
                branchWTP: {
                    required: "Please enter a Branch name",
                    regex: "Only alphabets, number, space and _() are allowed",
                    rangelength: "Branch name should be of {0} to {1} characters"
                },
                ifscWTP: {
                    required: "Please enter an IFSC code",
                    regex: "Only alphabets & numbers are allowed",
                    rangelength: "IFSC code should be of {0} to {1} characters"
                },
                fileuploadWTP: { required: "Select a file to upload" },
            }
        });
    } catch (e) {
        alertException(e, current_page_id, '');
    }
    $('#fileuploadWTP').on('change', function (ev) {
        try {
            if (typeof (FileReader) != "undefined") {
                var maxCnt = 1;
                //var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$/;
                var files = ev.target.files;
                $("span.span-img-base64").html('');
                $("span.span-img-filename").html('');

                if (files.length > maxCnt) {
                    ResetFileInput("#fileuploadWTP");
                    notification("Maximum " + maxCnt + " proof can be uploaded.", null);
                }
                else {
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        var fileType = ".jpg,.jpeg and .png", type1 = "image/jpeg", type2 = "image/png";

                        if (file.type == type1 || file.type == type2) {
                            var fileLimit = "3145728";
                            var fileLimitInMB = "3";

                            if (file.size <= parseInt(fileLimit)) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    try {
                                        $("span.span-img-base64").html(e.target.result);
                                        $("span.span-img-filename").html(file.name);
                                    } catch (e) {
                                        alertException(e, current_page_id, 'reader.onload');
                                    }
                                }
                                reader.readAsDataURL(file);
                            }
                            else {
                                ResetFileInput("#fileuploadWTP");
                                notification("Maximum file size allowed is " + fileLimitInMB + " MB.", null);
                            }
                        }
                        else {
                            ResetFileInput("#fileuploadWTP");
                            notification("Only " + fileType + " file is accepted.", null);
                        }
                    }
                }
            }
            else {
                alertme("Feature not supported.");
            }
        } catch (e) {
            alertException(e, current_page_id, '#fileuploadWTP.change');
            $("span.span-img-base64").html('');
            $("span.span-img-filename").html('');
        }

    });

    $('form#formWTP').on('submit', function (e) {
        try {
            var btntext = $("button.submit-button").html();
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var ewp = {};
                ewp.s = $("#amountWTP").val().trim() + "|" + $("#bankWTP").val().trim() + "|" + $("#branchWTP").val().trim() + "|" + $("#ifscWTP").val().trim() + "|" + $("span.span-img-filename").html() + "|" + $("span.span-img-base64").html();
                //console.log(ewp.s);
                ewp.s = StringEncryption(ewp.s);
                $.ajax({
                    type: 'POST',
                    url: "api/Customer/WalletDeposit",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(ewp),
                    dataType: 'json',
                    jsonp: null,
                    beforeSend: function (xhr, opts) {
                        $("button.submit-button").prop('disabled', true);
                        $("button.submit-button").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                    },
                    success: function (data, textStatus, xhr) {
                        try {
                            var r = StringDecryption(data);
                            ResetForm('#formWTP');
                            bootalert("Deposit Request No." + r + " raised successfully.", 'success');
                            //swal({
                            //    text: "Deposit Request No." + r + " raised successfully.",
                            //    type: 'success',
                            //    //confirmButtonText: 'Cool'
                            //}).catch(swal.noop);
                        } catch (e) {
                            alertException(e, current_page_id, 'Customer/WalletDeposit.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                    complete: function (a) {
                        $("button.submit-button").prop('disabled', false);
                        $("button.submit-button").html(btntext);
                    }
                });
            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, 'form#formWTP.submit');
        }

    });
});
