﻿current_page_id = "#busbookingstatus";
var actionId = 0, txnid = '', status = '';
$(document).ready(function (e) {
    try {
        if (localStorage.getItem("bustransactionid") != null)
            txnid = localStorage.getItem("bustransactionid");
        else
            window.location = virtualPath + "Account/BusBooking";

        if (localStorage.getItem("busbookingstatus") != null)
            status = localStorage.getItem("busbookingstatus");

        if (status == 'success') {
            $(".div-icon").html(tickanimation);
            $("#transaction_info").html('<p>Your booking of ' + localStorage.getItem("bus_FromCity") + ' - ' + localStorage.getItem("bus_ToCity") + ' bus is confirmed.</p><p> Your Trip Id is:<br /> <b>' + txnid + '</b>.<br /><br />Please use this Trip Id for any communication with us.');
        }
        else {
            $(".div-icon").html('<i class="fa fa-exclamation-circle fa-4x text-warning"></i>');
            $("#transaction_info").html(localStorage.getItem("errormessage"));
        }


        localStorage.removeItem("bus_FromCityId");
        localStorage.removeItem("bus_ToCityId");
        localStorage.removeItem("bus_FromCity");
        localStorage.removeItem("bus_ToCity");
        localStorage.removeItem("bus_roundtrip");
        localStorage.removeItem("bus_DepartureDate");
        localStorage.removeItem("bus_ArrivalDate");
        localStorage.removeItem("bus_DepartureDateLabel");
        localStorage.removeItem("bus_ArrivalDateLabel");
        localStorage.removeItem("bus_totalfare");
        localStorage.removeItem("bus_seats");
        localStorage.removeItem("bus_seats_info");
        localStorage.removeItem("bus_cashbackcode");
        localStorage.removeItem("bus_cashbackamount");
        localStorage.removeItem("bus_onwardOptions");
        localStorage.removeItem('bcb-code');
        localStorage.removeItem('bcb-description');
        localStorage.removeItem('bcb-amount');

        localStorage.removeItem("bustransactionid");
        localStorage.removeItem("busbookingstatus");
        localStorage.removeItem("errormessage");
    } catch (e) {
        alertException(e, current_page_id, 'pageLoad');
    }

    $(document).on('click', 'button.submit-button', function (e) {
        try {
            e.preventDefault();
            window.location = virtualPath + "Account/BusBooking";
        } catch (e) {
            alertException(e, current_page_id, 'button.submit-button.click');
        }
    });
    $("a.goto-bus-booking").on('click', function (e) {
        window.location = virtualPath + "Account/BusBooking";
    });
});
