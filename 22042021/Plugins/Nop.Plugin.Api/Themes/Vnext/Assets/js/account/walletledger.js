﻿current_page_id = "#walletledger";
var numrows = 10;

$(document).ready(function (e) {
    try {
        var ewp = {};
        $.ajax({
            type: 'POST',
            url: "api/Customer/GetWalletBalance",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            success: function (data, textStatus, xhr) {
                try {
                    var r = StringDecryption(data).split('|');
                    $("span.wallet-balance").html(r[0]);
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
        });
        GetRWDTransactionList('2', 0, numrows, 'ul.all-transactions', 'button.load-more-all');
        GetRWDTransactionList('0', 0, numrows, 'ul.credit-transactions', 'button.load-more-credit');
        GetRWDTransactionList('1', 0, numrows, 'ul.debit-transactions', 'button.load-more-debit');
    } catch (e) {
        alertException(e, current_page_id, '');
    }

    $('button.load-more-all').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.all-transactions li.transaction-record").length;
            GetRWDTransactionList('2', s, numrows, 'ul.all-transactions', 'button.load-more-all');
        } catch (e) {
            alertException(e, current_page_id, 'button.load-more-all.click');
        }
    });

    $('button.load-more-credit').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.credit-transactions li.transaction-record").length;
            GetRWDTransactionList('0', s, numrows, 'ul.credit-transactions', 'button.load-more-credit')
        } catch (e) {
            alertException(e, current_page_id, 'button.load-more-credit.click');
        }
    });

    $('button.load-more-debit').on('click', function (e) {
        try {
            e.preventDefault();
            var s = $("ul.debit-transactions li.transaction-record").length;
            GetRWDTransactionList('1', s, numrows, 'ul.debit-transactions', 'button.load-more-debit');
        } catch (e) {
            alertException(e, current_page_id, 'button.load-more-debit.click');
        }
    });
});
//function formatDate(inputDate) {
//    var value = new Date(parseInt(inputDate.replace(/(^.*\()|([+-].*$)/g, '')));
//    var month = value.getMonth() + 1;
//    var formattedDate = value.getDate() + "/" + month + "/" + value.getFullYear();
//    return formattedDate;
//}
function GetRWDTransactionList(i, s, e, o, b) {
    try {
        var btntext = $(b).html();
        var ewp = {};
        ewp.s = i + "|" + s + "|" + e;
        //console.log(ewp.s);
        ewp.s = StringEncryption(ewp.s);
        $.ajax({
            type: 'POST',
            url: "api/Customer/WalletLedger",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ewp),
            dataType: 'json',
            jsonp: null,
            beforeSend: function (xhr, opts) {
                $(b).prop('disabled', true);
                $(b).html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;loading...');
            },
            success: function (data, textStatus, xhr) {
                try {
                    data = JSON.parse(StringDecryption(data));
                    if (data.length > 0) {
                        //console.log(o);
                        var dt = '', dto = '', dtc = '';
                        if (data.length < e)
                            $(b).hide();
                        $.each(data, function (i, v) {
                            var $li = $("<li>", { 'class': 'transaction-record row padding-5 no-margin display-block box-shadow', html: '<div class="pull-left bold">' + v.ET_Remark + '</div><div class="clearfix"></div><div class="text-left text-sm">Request No:&nbsp;' + v.ET_UniqueId + '</div><div class="clearfix"></div><div class="text-left text-sm col-xs-9 col-sm-9 col-md-9 no-padding">' + formatDate(v.ET_Datetime) + '</div><div class="pull-right text-md bold text-' + (v.ET_Flag == 0 ? 'success' : 'danger') + ' col-xs-3 col-sm-3 col-md-3 no-padding text-right">' + (v.ET_Flag == 0 ? '+' : '-') + '&nbsp;<i class="fa fa-rupee"></i>&nbsp;<span class="transaction-amount">' + v.ET_Amt + '</span></div>' });
                            $(o).append($li);
                        });
                    }
                    else {
                        var $li = $("<li>", { 'class': 'no-margin no-bg no-shadow' });
                        var $dv = $("<div>", { html: '<p>&nbsp;</p><img src="' + virtualPath + 'Plugins/Nop.Plugin.Api/img/icons/empty-state.jpg" class="max-width-25p center-block" /><h6 class="center-block text-none">No history available</h6>' });
                        $li.append($dv);
                        $(o).append($li);
                        $(b).hide();
                    }
                } catch (e) {
                    alertException(e, current_page_id, '');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            },
            complete: function (a) {
                $(b).prop('disabled', false);
                $(b).html(btntext);
            }
        });
    } catch (e) {
        alertException(e, current_page_id, '');
    }
}