﻿$(document).ready(function () {
    var sblbtn = $("#btnlogin");
    var sbrbtn = $("#btnrecover");
    $("#loginform input.username").focus();

    $("#loginform").validate({
        //debug: true,
        rules: {
            MemUsername: {
                required: {
                    depends: function () {
                        $(this).val($(this).val().trim().toLowerCase());
                        return true;
                    }
                },
                regex: /^[a-zA-Z0-9\@\_\.]+$/,
                rangelength: [4, 100]
            },
            MemPassword: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                rangelength: [4, 20]
            }
        },
        messages: {
            MemUsername: {
                required: "Enter a Username (4-20 characters).",
                regex: "Only alphabets and numbers are allowed.",
                rangelength: "Username should be 4-20 characters."
            },
            MemPassword: {
                required: "Enter a Password (4-20 characters).",
                rangelength: "Password should be 4-20 characters."
            }
        }
    });

    $("#loginform").on('submit', function (e) {
        //debugger;
        var isvalidate = $(this).valid();
        if (isvalidate) {
            var paramList = {};
            paramList.s = $('#MemUsername').val().trim() + '|' + $('#MemPassword').val().trim();
            paramList.returnJSON = true;
            //console.log(JSON.stringify(paramList));
            paramList.s = StringEncryption(paramList.s);

            var $btn = sblbtn.button('loading');
            sblbtn.prop("disabled", true);

            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/SignIn",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    window.location.href = virtualPath + 'Account/Dashboard';
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (errorThrown == '')
                        errorThrown = 'Connection is not available.';
                    //$(".flipLink").animate({ opacity: "1" }, 500);
                    bootalert(errorThrown, 'error');
                    $btn.button('reset')
                    sblbtn.prop("disabled", false);
                },
                complete: function (a) {
                }
            });
        }
        e.preventDefault();
    });

    $("#recoverform").validate({
        rules: {
            Username: {
                required: {
                    depends: function () {
                        $(this).val($(this).val().trim().toLowerCase());
                        return true;
                    }
                },
                regex: /^[a-zA-Z0-9\@\_\.]+$/,
                //regex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$/,
                /*regex: /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,*/
                //email: true,
                rangelength: [4, 100]
            }
        },
        messages: {
            Username: {
                required: "Enter a Username.",
                regex: "Enter a valid Username.",
                //email: "Enter a valid Email ID.",
                rangelength: "Username should be 4-100 characters."
            }
        }
    });

    $("#recoverform").on('submit', function (e) {
        e.preventDefault();
        var isvalidate = $(this).valid();

        if (isvalidate) {
            var paramList = {};
            paramList.s = StringEncryption($('#Username').val().trim());
            var $btn = sbrbtn.button('loading');
            sbrbtn.prop("disabled", true);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/PasswordRecovery",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramList),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    bootalert(StringDecryption(data), 'success');
                    $btn.button('reset')
                    sbrbtn.prop("disabled", false);
                    $(".flipLink").animate({ opacity: "1" }, 500);
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (errorThrown == '')
                        errorThrown = 'Connection not available.';
                    $btn.button('reset')
                    sbrbtn.prop("disabled", false);
                    $(".flipLink").animate({ opacity: "1" }, 500);
                    bootalert(errorThrown, 'error');
                }
            });
        }
        else {
            $btn.button('reset')
            sbrbtn.prop("disabled", false);
            e.preventDefault();
        }
    });

    $("a#aforgot").on('click', function (e) {
        $('div#loginformcontainer').fadeOut(400, "linear");
        $('h2.sign-in-title').fadeOut(400, "linear");
        setTimeout(function () {
            $("label.error").hide();
            $('div#recoveryformcontainer').fadeIn(400, "linear");
        }, 400);
    });

    $("a#asignin").on('click', function (e) {
        $('div#recoveryformcontainer').fadeOut(400, "linear");
        setTimeout(function () {
            $('div#loginformcontainer').fadeIn(400, "linear");
            $('h2.sign-in-title').fadeIn(400, "linear");
        }, 400);
    });
});