﻿current_page_id = "#flightmakepayment";
var type, origin, destination, departDate, departDateLabel, returnDate, returnDateLabel, travellers, totaltravellers, cabinClass, cabinClassName, mode, airline, onwardflight, returnflight, onwardOperatorCode, returnOperatorId;
var adult = 0, child = 0, infant = 0, totalFare = 0, taxAmount = 0, profileflagMPF = false, maxDt = new Date(), expiryDt = new Date(), cb_codes = '', cb_fares = '', cb_amounts = '', taxes = '', cb_total_amount = 0, grplcash_maxusage = 0;

function FlightPrerequisities() {
    try {
        var cmp = {};
        cmp.s = "";
        $.ajax({
            type: 'POST',
            url: apiPath + "/FlightPrerequisities",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(cmp),
            dataType: 'json',
            jsonp: null,
            success: function (data, textStatus, xhr) {
                try {
                    var r = StringDecryption(data);
                    //console.log(r);
                    arr = [];
                    arr = r.split("|");

                    if (arr[0] != '') {
                        dedhead_list_json = JSON.parse(arr[0]);
                    }

                    if (arr[1] != '') {
                        deduction_list_json = JSON.parse(arr[1]);
                    }

                    //console.log('dedhead_list_json---------------------------------------------------------');
                    //console.log(JSON.stringify(dedhead_list_json));
                    //console.log('deduction_list_json---------------------------------------------------------');
                    //console.log(deduction_list_json);

                    var taxList = $.grep(deduction_list_json, function (e) { return e.TypeId == type && (e.OperatorCode == onwardOperatorCode || e.OperatorCode == returnOperatorId) && e.Value > 0; });
                    //console.log(taxList);
                    if (taxList.length > 0) {
                        var taxdet = '', taxamt = 0, taxtooltip = '';
                        $.each(taxList, function (i, v) {
                            taxdet = '', tax = 0, taxamt = 0;
                            //console.log(v.Value);
                            if (v.IsAmount) {
                                taxamt = parseFloat(v.Value);
                            }
                            else {
                                taxamt = parseFloat(totalFare) * parseFloat(v.Value) / 100;
                            }

                            taxamt = parseFloat(taxamt.toFixed(2));
                            taxAmount += taxamt;
                            taxdet = v.Id + "~" + v.TypeId + "~" + v.OperatorType + "~" + v.OperatorId + "~" + v.Operator + "~" + v.OperatorCode + "~" + v.Value;

                            if (taxes == '') {
                                taxes = taxdet;
                            }
                            else {
                                taxes = taxes + "#" + taxdet;
                            }
                            taxtooltip += (taxtooltip == '' ? "" : "<br />") + v.Charge + (v.IsAmount ? "" : " (" + v.Value + "%)") + ": Rs." + taxamt + '/-';
                        });

                        //$("span.tax-tooltip").prop('title', taxtooltip);
                        //$("span.tax-tooltip").removeProp('data-original-title');
                        //$('[data-toggle="tooltip"]').tooltip({
                        //    title: function () {
                        //        return $(this).prop('title');
                        //    }
                        //});

                        $("span.tax-tooltip").html(taxtooltip);

                        $("#divtaxamountMPF").show();
                        $("#hdntaxamountMPF").val(taxAmount);
                        $("#taxamountMPF span").html('<i class="fa fa-rupee"></i>&nbsp;' + taxAmount.toString());

                        totalAmount = totalFare + taxAmount;
                        totalAmount = parseFloat(totalAmount).toFixed(2);

                        $("#hdnamountMPF").val(totalAmount);
                        $("#amountMPF span").html('<i class="fa fa-rupee"></i>&nbsp;' + totalAmount.toString());
                    }
                    GetWalletBalance();
                } catch (e) {
                    alertException(e, 'app.init', 'FlightPrerequisities.success');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                OnAjaxError(xhr, textStatus, errorThrown);
            }
        });
    } catch (e) {
        //alertException(e, 'app.init', 'RechargePrerequisities');
        LogException(init_current_page_id + ' => ' + e.message);
    }
}

function BalanceAmountToPay() {
    var amt = parseFloat($("#hdnamountMP").val());
    var ewbal = parseFloat($("#hdnwalletbalanceMPF").val());
    var grplbal = parseFloat($("#hdngrplmaxcashamountMPF").val());
    var bal = 0;

    if ($("#usewalletMPF").prop('checked')) {
        bal = (ewbal >= amt) ? 0 : (amt - ewbal);
    }
    else {
        bal = amt;
    }

    if ($("#usegrplcashMPF").prop('checked')) {
        if (bal == 0) {
            notification('Total Fare is payable using Wallet.\nUncheck Wallet to use GRPL Cash as payment mode.', function () {
                $("#usegrplcashMPF").prop('checked', false);
            });
        }
        bal = (grplbal >= bal) ? 0 : (bal - grplbal);
    }
    else {
        bal = bal;
    }
    //console.log('bal - ' + bal);
    $("#balanceamtMPF span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat(bal).toLocaleString('en-IN'));
    $("#hdnbalanceamtMPF").val(bal);
    $("button.make-payment span").html('Book Now');

    if (bal < 0) {
        alertme('Total Fare cannot be less than 0 (zero)');
        $("button.make-payment").prop('disabled', true);
    }
    if (bal > 0) {
        $("#divbalamtMPF").slideDown('fast');
        $("button.make-payment").prop('disabled', false);
        $("button.make-payment span").html('Proceed for Payment');
    }
    else {
        $("#divbalamtMPF").slideUp('fast');
    }
}

function ParameterString() {
    var p = "";
    p = type;
    p += "|" + origin;
    p += "|" + destination;
    p += "|" + departDate;
    p += "|" + returnDate;
    p += "|" + totaltravellers;
    p += "|" + travellers;
    p += "|" + cabinClass;
    p += "|" + mode;
    p += "|" + airline;
    p += "|" + $("#totalfareMPF").val();
    p += "|" + taxes;
    p += "|" + taxAmount.toString();
    p += "|" + $("#hdnamountMPF").val();

    p += "|" + ($("#usewalletMPF").prop('checked') == true ? 1 : 0);
    p += "|" + LOGGED_IN_MOBILENO;
    p += "|" + LOGGED_IN_EMAIL;

    p += "|" + $("#cbCodesMPF").val();
    p += "|" + $("#cbFaresMPF").val();
    p += "|" + $("#cbAmountsMPF").val();
    p += "|" + ($("#usegrplcashMPF").prop('checked') == true ? 1 : 0);

    return p;
}

function FlightBookingCompletion(status, txnid) {
    localStorage.setItem("flightbookingstatus", status);
    localStorage.setItem("flighttransactionid", txnid);
    window.location = virtualPath + "Account/FlightBookingStatus";
}

function PostWalletBalance(r) {
    var arr = r.split('|');
    var amt = parseFloat($("#hdnamountMPF").val());
    //notification(r);
    var ewbal = parseFloat(arr[0]);
    $("#hdnwalletbalanceMPF").val(ewbal);
    $("#walletbalanceMPF span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat($("#hdnwalletbalanceMPF").val()).toLocaleString('en-IN'));

    if (ewbal > 0) {
        $("#usewalletMPF").prop('checked', true);
        $("#usewalletMPF").prop("disabled", false);
    }
    else {
        $("#usewalletMPF").prop('checked', false);
        $("#usewalletMPF").prop("disabled", true);
    }

    var grplbal = parseFloat(arr[1]);
    $("#hdngrplcashbalanceMPF").val(grplbal);
    $("#grplcashbalanceMPF span").html('<i class="fa fa-rupee"></i>&nbsp;' + parseFloat($("#hdngrplcashbalanceMPF").val()).toLocaleString('en-IN'));

    var grplpercent = parseFloat(arr[2]);
    var grplmaxamt = 0;
    if (grplpercent > 0) {
        grplmaxamt = Math.floor(amt * grplpercent / 100);
        grplmaxamt = grplmaxamt > grplbal ? grplbal : grplmaxamt;
        $("#hdngrplmaxcashamountMPF").val(grplmaxamt);
        $("#spangrplmaxcashamountMPF").html(grplmaxamt);
    }

    if (grplmaxamt > 0) {
        if ((ewbal >= amt ? 0 : amt - ewbal) > 0)
            $("#usegrplcashMPF").prop('checked', true);
        $("#usegrplcashMPF").prop("disabled", false);
    }
    else {
        $("#usegrplcashMPF").prop('checked', false);
        $("#usegrplcashMPF").prop("disabled", true);
    }

    BalanceAmountToPay();
    if (pgresponse == '') {
        $("#formLoadingMPF").fadeOut('fast', function (e) {
            $("#formMPF").fadeIn();
        });
    }
}
function GetWalletBalance() {
    var btntext = $("button.make-payment span").html();
    var ewp = {};
    $.ajax({
        type: 'POST',
        url: apiPath + "/Customer/GetWalletBalance",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ewp),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                //console.log(r);
                PostWalletBalance(r);
            } catch (e) {
                alertException(e, current_page_id, 'GetWalletBalance.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

function RevertWalletDeduction(uid) {
    var btntext = $("button.make-payment span").html();
    var xyz = {};
    xyz.s = StringEncryption(uid);
    $.ajax({
        type: 'POST',
        url: apiPath + "/Customer/WalletDeductionReversal",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(xyz),
        dataType: 'json',
        jsonp: null,
        beforeSend: function (xhr, opts) {
            $("button.make-payment").prop('disabled', true);
            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
        },
        success: function (data, textStatus, xhr) {
            try {
                var r = StringDecryption(data);
                PostWalletBalance(r);
            } catch (e) {
                alertException(e, current_page_id, 'RevertWalletDeduction.success');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            OnAjaxError(xhr, textStatus, errorThrown);
        },
        complete: function (a) {
            $("button.make-payment").prop('disabled', false);
            $("button.make-payment span").html(btntext);
        }
    });
}

$(document).ready(function (e) {
    try {
        if (localStorage.getItem("domestic") != null) {
            type = localStorage.getItem("domestic");
            origin = localStorage.getItem("FromCode");
            destination = localStorage.getItem("ToCode");
            departDate = localStorage.getItem("DepartureDate");
            departDateLabel = localStorage.getItem("DepartureDateLabel");
            returnDate = localStorage.getItem("ArrivalDate");
            returnDateLabel = localStorage.getItem("ArrivalDateLabel");
            travellers = localStorage.getItem("travellers");
            totaltravellers = parseInt(localStorage.getItem("totaltravellers"));
            onwardflight = JSON.parse(localStorage.getItem("onwardflight"));
            onwardOperatorCode = onwardflight[0].flight[0].airlineCode;
            totalFare = parseFloat(localStorage.getItem("flighttotalfare"));

            if (mode == 'ROUND') {
                returnflight = JSON.parse(localStorage.getItem("returnflight"));
                returnOperatorCode = returnflight[0].flight[0].airlineCode;
            }
            else {
                returnflight = null;
                returnOperatorCode = null;
            }

            var arrD = departDate.split('-');
            expiryDt = new Date(arrD[2], arrD[1], arrD[0]);
            expiryDt.setMonth(expiryDt.getMonth() + 5);
            if (returnDate != '') {
                arrD = returnDate.split('-');
                expiryDt = new Date(arrD[2], arrD[1], arrD[0]);
                expiryDt.setMonth(expiryDt.getMonth() + 5);
            }

            cabinClass = localStorage.getItem("cabinclass");
            cabinClassName = cabinClass == 'E' ? 'Economy' : 'Business';
            mode = localStorage.getItem("roundtrip") == 0 ? 'ONE' : 'ROUND';
            airline = '';
            var page_title = '' + origin + '&nbsp;<i class="fa ' + (mode == 'ONE' ? 'fa-arrow-right' : 'fa-exchange') + '"></i>&nbsp;' + destination + ' (' + departDateLabel + (mode == 'ONE' ? '' : (' - ' + returnDateLabel)) + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + '';
            $(".flight-page-title").html(page_title);
        }

        if (pgresponse != "") {
            var req = [], req_status = '', req_txnid = '', error_message = '';
            //console.log(pgresponse);
            //pgresponse = StringDecryption(pgresponse);
            if (pgresponse.indexOf('|') != -1)
                req = pgresponse.split('|');
            else {
                bootalert('Oops! Unexpected error occurred. Please report it to us.', 'error');
            }

            req_status = req[0];
            if (req.length > 1)
                req_txnid = req[1];
            if (req.length > 2)
                error_message = req[2];

            pgresponse = null;

            if (req_status != '' && req_txnid != '') {
                if (req_status == 'success') {
                    var btntextR = $("button.make-payment span").html();
                    var abc = {};
                    abc.s = req_txnid;
                    //console.log(ewp.s);
                    abc.s = StringEncryption(abc.s);
                    $.ajax({
                        type: 'POST',
                        url: apiPath + "/Customer/GatewayFlightBooking",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(abc),
                        dataType: 'json',
                        jsonp: null,
                        beforeSend: function (xhr, opts) {
                            $("button.make-payment").prop('disabled', true);
                            $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                        },
                        success: function (data, textStatus, xhr) {
                            try {
                                var r = StringDecryption(data);
                                var arr = [];
                                arr = r.split("|");
                                if (arr[0] == 'success') {
                                    FlightBookingCompletion(arr[0], arr[1]);
                                }
                            } catch (e) {
                                alertException(e, current_page_id, 'GatewayFlightBooking.success');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            RevertWalletDeduction(req_txnid);
                            //OnAjaxError(xhr, textStatus, errorThrown);
                            $("#errorMessageMPF").html(errorThrown);
                            $("#formLoadingMPF").fadeOut('fast', function (e) {
                                $("#formErrorMPF").fadeIn();
                            });
                        },
                        complete: function (a) {
                            $("button.make-payment").prop('disabled', false);
                            $("button.make-payment span").html(btntextR);
                        }
                    });
                }
                else {
                    RevertWalletDeduction(req_txnid);
                    $("#errorMessageMPF").html(error_message);
                    $("#formLoadingMPF").fadeOut('fast', function (e) {
                        $("#formErrorMPF").fadeIn();
                    });
                }
            }
        }
        else {
            FlightPrerequisities();
            localStorage.removeItem("flighttransactionid");
            localStorage.removeItem("onwardOptions");
            if (localStorage.getItem("domestic") != null) {
                //type = localStorage.getItem("domestic");
                //origin = localStorage.getItem("FromCode");
                //destination = localStorage.getItem("ToCode");
                //departDate = localStorage.getItem("DepartureDate");
                //departDateLabel = localStorage.getItem("DepartureDateLabel");
                //returnDate = localStorage.getItem("ArrivalDate");
                //returnDateLabel = localStorage.getItem("ArrivalDateLabel");
                //travellers = localStorage.getItem("travellers");
                //totaltravellers = parseInt(localStorage.getItem("totaltravellers"));

                //var arrD = departDate.split('-');
                //expiryDt = new Date(arrD[2], arrD[1], arrD[0]);
                //expiryDt.setMonth(expiryDt.getMonth() + 5);
                //if (returnDate != '') {
                //    arrD = returnDate.split('-');
                //    expiryDt = new Date(arrD[2], arrD[1], arrD[0]);
                //    expiryDt.setMonth(expiryDt.getMonth() + 5);
                //}

                //cabinClass = localStorage.getItem("cabinclass");
                //cabinClassName = cabinClass == 'E' ? 'Economy' : 'Business';
                //mode = localStorage.getItem("roundtrip") == 0 ? 'ONE' : 'ROUND';
                //airline = '';
                //var page_title = '<div style="line-height:25px">' + origin + '&nbsp;<i class="fa ' + (mode == 'ONE' ? 'fa-arrow-right' : 'fa-exchange') + '"></i>&nbsp;' + destination + '</div><div style="line-height:18px;font-size:12px">' + departDateLabel + (mode == 'ONE' ? '' : (' - ' + returnDateLabel)) + ' | ' + (totaltravellers == 1 ? ' 1 Traveller' : totaltravellers.toString() + ' Travellers') + '</div>';
                //$(".flight-page-title").html(page_title);

                //console.log('departDate - ' + departDate);
                //console.log('returnDate - ' + returnDate);
                //console.log('expiryDt - ' + expiryDt);

                //console.log('onward-cb-code = ' + localStorage.getItem("onward-cb-code"));
                //console.log('onward-cb-description = ' + localStorage.getItem("onward-cb-description"));
                //console.log('onward-cb-fare = ' + localStorage.getItem("onward-cb-fare"));
                //console.log('onward-cb-amount = ' + localStorage.getItem("onward-cb-amount"));

                //console.log('return-cb-code = ' + localStorage.getItem("return-cb-code"));
                //console.log('return-cb-description = ' + localStorage.getItem("return-cb-description"));
                //console.log('return-cb-fare = ' + localStorage.getItem("return-cb-fare"));
                //console.log('return-cb-amount = ' + localStorage.getItem("return-cb-amount"));

                if (localStorage.getItem("onward-cb-code") != null) {
                    cb_codes = localStorage.getItem("onward-cb-code");
                    cb_fares = localStorage.getItem("onward-cb-fare");
                    cb_amounts = localStorage.getItem("onward-cb-amount");
                    cb_total_amount = parseFloat(localStorage.getItem("onward-cb-amount"));
                }

                if (localStorage.getItem("return-cb-code") != null) {
                    cb_codes += (cb_codes == '' ? '' : ',') + localStorage.getItem("return-cb-code");
                    cb_fares += (cb_fares == '' ? '' : ',') + localStorage.getItem("return-cb-fare");
                    cb_amounts += (cb_amounts == '' ? '' : ',') + localStorage.getItem("return-cb-amount");
                    cb_total_amount += parseFloat(localStorage.getItem("return-cb-amount"));
                }

                //console.log('cb_codes = ' + cb_codes);
                //console.log('cb_fares = ' + cb_fares);
                //console.log('cb_amounts = ' + cb_amounts);
                //console.log('cb_total_amount = ' + cb_total_amount);

                if (cb_codes != '') {
                    $("#cbCodesMPF").val(cb_codes);
                    $("#cbFaresMPF").val(cb_fares);
                    $("#cbAmountsMPF").val(cb_amounts);
                    $("#cbTotalAmtMPF").val(cb_total_amount);
                    $("#cashbackMPF span").html('<i class="fa fa-rupee"></i> ' + cb_total_amount.toLocaleString('en-IN'));
                    if (cb_total_amount > 0)
                        $("#divcashbackMPF").show();
                }

                if (travellers.indexOf('~') != -1) {
                    var arr = travellers.split('~');
                    adult = parseInt(arr[0]);
                    child = parseInt(arr[1]);
                    if (arr.length > 2) {
                        infant = parseInt(arr[2]);
                    }
                }
                else {
                    adult = travellers;
                }

                $("#bookingemailid").val(LOGGED_IN_EMAIL);
                $("#bookingemailidlabel span").html($("#bookingemailid").val());

                $("#bookingmobileno").val(LOGGED_IN_MOBILENO);
                $("#bookingmobilenolabel span").html($("#bookingmobileno").val());


            }
            if (LOGGED_IN_ID != null) {
                if (LOGGED_IN_NAME == null) {
                    profileflagMPF = true;
                }
                else {
                    if (LOGGED_IN_NAME == '') {
                        profileflagMPF = true;
                    }
                    else {
                        if (LOGGED_IN_EMAIL == null) {
                            profileflagMPF = true;
                        }
                        else {
                            if (LOGGED_IN_EMAIL == '') {
                                profileflagMPF = true;
                            }
                            else {
                                if (LOGGED_IN_MOBILENO == null) {
                                    profileflagMPF = true;
                                }
                                else {
                                    if (LOGGED_IN_MOBILENO == '') {
                                        profileflagMPF = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (profileflagMPF) {
                    //bootalert(profile_update_message);
                    $("#errorMessageMPF").html(profile_update_message);
                    $("#formLoadingMPF").fadeOut('fast', function (e) {
                        $("#formErrorMPF").fadeIn();
                    });
                }
                else {
                    var adulthtml = '';
                    if (adult > 0) {
                        for (var i = 0; i < adult; i++) {
                            adulthtml += '<div class="panel panel-default' + (i == 0 ? ' no-margin' : '') + '">';
                            adulthtml += '<div class="panel-heading no-padding-top" role="tab" id="adult_' + (i + 1) + '_heading">';
                            adulthtml += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#traveller_info" href="#adult_' + (i + 1) + '_body" aria-expanded="true" aria-controls="adult_' + (i + 1) + '_body">';
                            adulthtml += '<h4 class="panel-title text-info">';
                            adulthtml += 'Adult ' + (i + 1);
                            adulthtml += '<i class="fa fa-angle-down"></i>';
                            adulthtml += '</h4>';
                            adulthtml += '</a>';
                            adulthtml += '</div>';
                            adulthtml += '<div id="adult_' + (i + 1) + '_body" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="adult_' + (i + 1) + '_heading" aria-expanded="true">';
                            adulthtml += '<div class="panel-body">';
                            adulthtml += '<div class="form-group no-padding no-margin toggle-switch">';
                            adulthtml += '<label>';
                            adulthtml += '<input id="adult_mr_' + (i + 1) + '" type="radio" value="Mr." name="adult_gender_' + (i + 1) + '" class="adult_gender" />';
                            adulthtml += '<span class="toggle__text">Mr</span>';
                            adulthtml += '</label>';
                            adulthtml += '<label>';
                            adulthtml += '<input id="adult_ms_' + (i + 1) + '" type="radio" value="Ms." name="adult_gender_' + (i + 1) + '" class="adult_gender" />';
                            adulthtml += '<span class="toggle__text">Ms</span>';
                            adulthtml += '</label>';
                            adulthtml += '<label>';
                            adulthtml += '<input id="adult_mrs_' + (i + 1) + '" type="radio" value="Mrs." name="adult_gender_' + (i + 1) + '" class="adult_gender" />';
                            adulthtml += '<span class="toggle__text">Mrs</span>';
                            adulthtml += '</label>';
                            adulthtml += '</div>';
                            adulthtml += '<div style="height:12px"></div>';
                            adulthtml += '<div class="form-group label-floating">';
                            adulthtml += '<label for="adult_firstname_' + (i + 1) + '" class="control-label" style="left:0">First Name</label>';
                            adulthtml += '<input type="text" id="adult_firstname_' + (i + 1) + '" name="adult_firstname_' + (i + 1) + '" class="form-control adult_firstname" maxlength="30">';
                            adulthtml += '</div>';
                            adulthtml += '<div class="form-group label-floating">';
                            adulthtml += '<label for="adult_lastname_' + (i + 1) + '" class="control-label" style="left:0">Last Name</label>';
                            adulthtml += '<input type="text" id="adult_lastname_' + (i + 1) + '" name="adult_lastname_' + (i + 1) + '" class="form-control adult_lastname" maxlength="30">';
                            adulthtml += '</div>';
                            if (parseInt(type) == 2) {
                                adulthtml += '<div class="form-group label-floating">';
                                adulthtml += '<label for="adult_passportno_' + (i + 1) + '" class="control-label" style="left:0">Passport Number</label>';
                                adulthtml += '<input type="text" id="adult_passportno_' + (i + 1) + '" name="adult_passportno_' + (i + 1) + '" class="form-control adult_passportno" maxlength="20">';
                                adulthtml += '</div>';
                                adulthtml += '<div class="form-group label-floating">';
                                adulthtml += '<label for="adult_passportissueplace_' + (i + 1) + '" class="control-label" style="left:0">Passport Issuing Place</label>';
                                adulthtml += '<input type="text" id="adult_passportissueplace_' + (i + 1) + '" name="adult_passportissueplace_' + (i + 1) + '" class="form-control adult_passportissueplace" maxlength="20">';
                                adulthtml += '</div>';
                                adulthtml += '<div class="form-group label-floating">';
                                adulthtml += '<label for="adult_passportissuedate_' + (i + 1) + '" class="control-label" style="left:0">Passport Issue date</label>';
                                adulthtml += '<input type="text" id="adult_passportissuedate_' + (i + 1) + '" name="adult_passportissuedate_' + (i + 1) + '" class="form-control adult_passportissuedate" maxlength="10">';
                                adulthtml += '</div>';
                                adulthtml += '<div class="form-group label-floating">';
                                adulthtml += '<label for="adult_passportexpirydate_' + (i + 1) + '" class="control-label" style="left:0">Passport Expiry date</label>';
                                adulthtml += '<input type="text" id="adult_passportexpirydate_' + (i + 1) + '" name="adult_passportexpirydate_' + (i + 1) + '" class="form-control adult_passportexpirydate" maxlength="10">';
                                adulthtml += '</div>';
                                adulthtml += '<div class="form-group select">';
                                adulthtml += '<label for="adult_visatype_' + (i + 1) + '" class="control-label text-left" style="margin-top:5px;left:0">Visa Type</label>';
                                adulthtml += '<select id="adult_visatype_' + (i + 1) + '" name="adult_visatype_' + (i + 1) + '" class="adult_visatype">';
                                adulthtml += '<option value="">Select Visa Type</option>';
                                adulthtml += '<option>Business visa</option>';
                                adulthtml += '<option>Dependent</option>';
                                adulthtml += '<option>Employment</option>';
                                adulthtml += '<option>Hajj</option>';
                                adulthtml += '<option>Migrant</option>';
                                adulthtml += '<option>Residence</option>';
                                adulthtml += '<option>Seamen</option>';
                                adulthtml += '<option>Student</option>';
                                adulthtml += '<option>Tourist</option>';
                                adulthtml += '<option>Umrah</option>';
                                adulthtml += '</select>';
                                adulthtml += '</div>';
                            }
                            adulthtml += '</div>';
                            adulthtml += '</div>';
                            adulthtml += '</div>';
                        }
                    }

                    var childhtml = '';
                    if (child > 0) {
                        for (var i = 0; i < child; i++) {
                            childhtml += '<div class="panel panel-default">';
                            childhtml += '<div class="panel-heading no-padding-top" role="tab" id="child_' + (i + 1) + '_heading">';
                            childhtml += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#traveller_info" href="#child_' + (i + 1) + '_body" aria-expanded="true" aria-controls="child_' + (i + 1) + '_body">';
                            childhtml += '<h4 class="panel-title text-info">';
                            childhtml += 'Child ' + (i + 1);
                            childhtml += '<i class="fa fa-angle-down"></i>';
                            childhtml += '</h4>';
                            childhtml += '</a>';
                            childhtml += '</div>';
                            childhtml += '<div id="child_' + (i + 1) + '_body" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="child_' + (i + 1) + '_heading" aria-expanded="true">';
                            childhtml += '<div class="panel-body">';
                            childhtml += '<div class="form-group no-padding no-margin toggle-switch">';
                            childhtml += '<label>';
                            childhtml += '<input id="child_master_' + (i + 1) + '" type="radio" value="Mstr." name="child_gender_' + (i + 1) + '" class="child_gender" />';
                            childhtml += '<span class="toggle__text">Master</span>';
                            childhtml += '</label>';
                            childhtml += '<label>';
                            childhtml += '<input id="child_miss_' + (i + 1) + '" type="radio" value="Miss." name="child_gender_' + (i + 1) + '" class="child_gender" />';
                            childhtml += '<span class="toggle__text">Miss</span>';
                            childhtml += '</label>';
                            childhtml += '</div>';
                            childhtml += '<div class="form-group label-floating">';
                            childhtml += '<label for="child_firstname_' + (i + 1) + '" class="control-label" style="left:0">First Name</label>';
                            childhtml += '<input type="text" id="child_firstname_' + (i + 1) + '" name="child_firstname_' + (i + 1) + '" class="form-control child_firstname" maxlength="30">';
                            childhtml += '</div>';
                            childhtml += '<div class="form-group label-floating">';
                            childhtml += '<label for="child_lastname_' + (i + 1) + '" class="control-label" style="left:0">Last Name</label>';
                            childhtml += '<input type="text" id="child_lastname_' + (i + 1) + '" name="child_lastname_' + (i + 1) + '" class="form-control child_lastname" maxlength="30">';
                            childhtml += '</div>';
                            if (parseInt(type) == 2) {
                                childhtml += '<div class="form-group label-floating">';
                                childhtml += '<label for="child_passportno_' + (i + 1) + '" class="control-label" style="left:0">Passport Number</label>';
                                childhtml += '<input type="text" id="child_passportno_' + (i + 1) + '" name="child_passportno_' + (i + 1) + '" class="form-control child_passportno" maxlength="20">';
                                childhtml += '</div>';
                                childhtml += '<div class="form-group label-floating">';
                                childhtml += '<label for="child_passportissueplace_' + (i + 1) + '" class="control-label" style="left:0">Passport Issuing Place</label>';
                                childhtml += '<input type="text" id="child_passportissueplace_' + (i + 1) + '" name="child_passportissueplace_' + (i + 1) + '" class="form-control child_passportissueplace" maxlength="20">';
                                childhtml += '</div>';
                                childhtml += '<div class="form-group label-floating">';
                                childhtml += '<label for="child_passportissuedate_' + (i + 1) + '" class="control-label" style="left:0">Passport Issue date</label>';
                                childhtml += '<input type="text" id="child_passportissuedate_' + (i + 1) + '" name="child_passportissuedate_' + (i + 1) + '" class="form-control child_passportissuedate" maxlength="10">';
                                childhtml += '</div>';
                                childhtml += '<div class="form-group label-floating">';
                                childhtml += '<label for="child_passportexpirydate_' + (i + 1) + '" class="control-label" style="left:0">Passport Expiry date</label>';
                                childhtml += '<input type="text" id="child_passportexpirydate_' + (i + 1) + '" name="child_passportexpirydate_' + (i + 1) + '" class="form-control child_passportexpirydate" maxlength="10">';
                                childhtml += '</div>';
                                childhtml += '<div class="form-group select">';
                                childhtml += '<label for="child_visatype_' + (i + 1) + '" class="control-label text-left" style="margin-top:5px;left:0">Visa Type</label>';
                                childhtml += '<select id="child_visatype_' + (i + 1) + '" name="child_visatype_' + (i + 1) + '" class="child_visatype">';
                                childhtml += '<option value="">Select Visa Type</option>';
                                childhtml += '<option>Business visa</option>';
                                childhtml += '<option>Dependent</option>';
                                childhtml += '<option>Employment</option>';
                                childhtml += '<option>Hajj</option>';
                                childhtml += '<option>Migrant</option>';
                                childhtml += '<option>Residence</option>';
                                childhtml += '<option>Seamen</option>';
                                childhtml += '<option>Student</option>';
                                childhtml += '<option>Tourist</option>';
                                childhtml += '<option>Umrah</option>';
                                childhtml += '</select>';
                                childhtml += '</div>';
                            }
                            childhtml += '</div>';
                            childhtml += '</div>';
                            childhtml += '</div>';
                        }
                    }
                    var infanthtml = '';
                    if (infant > 0) {
                        for (var i = 0; i < infant; i++) {
                            infanthtml += '<div class="panel panel-default">';
                            infanthtml += '<div class="panel-heading no-padding-top" role="tab" id="infant_' + (i + 1) + '_heading">';
                            infanthtml += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#traveller_info" href="#infant_' + (i + 1) + '_body" aria-expanded="true" aria-controls="infant_' + (i + 1) + '_body">';
                            infanthtml += '<h4 class="panel-title text-info">';
                            infanthtml += 'Infant ' + (i + 1);
                            infanthtml += '<i class="fa fa-angle-down"></i>';
                            infanthtml += '</h4>';
                            infanthtml += '</a>';
                            infanthtml += '</div>';
                            infanthtml += '<div id="infant_' + (i + 1) + '_body" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="infant_' + (i + 1) + '_heading" aria-expanded="true">';
                            infanthtml += '<div class="panel-body">';
                            infanthtml += '<div class="form-group no-padding no-margin toggle-switch">';
                            infanthtml += '<label>';
                            infanthtml += '<input id="infant_master_' + (i + 1) + '" type="radio" value="Mstr." name="infant_gender_' + (i + 1) + '" class="infant_gender" />';
                            infanthtml += '<span class="toggle__text">Master</span>';
                            infanthtml += '</label>';
                            infanthtml += '<label>';
                            infanthtml += '<input id="infant_miss_' + (i + 1) + '" type="radio" value="Miss." name="infant_gender_' + (i + 1) + '" class="infant_gender" />';
                            infanthtml += '<span class="toggle__text">Miss</span>';
                            infanthtml += '</label>';
                            infanthtml += '</div>';
                            infanthtml += '<div class="form-group label-floating">';
                            infanthtml += '<label for="infant_firstname_' + (i + 1) + '" class="control-label" style="left:0">First Name</label>';
                            infanthtml += '<input type="text" id="infant_firstname_' + (i + 1) + '" name="infant_firstname_' + (i + 1) + '" class="form-control infant_firstname" maxlength="30">';
                            infanthtml += '</div>';
                            infanthtml += '<div class="form-group label-floating">';
                            infanthtml += '<label for="infant_lastname_' + (i + 1) + '" class="control-label" style="left:0">Last Name</label>';
                            infanthtml += '<input type="text" id="infant_lastname_' + (i + 1) + '" name="infant_lastname_' + (i + 1) + '" class="form-control infant_lastname" maxlength="30">';
                            infanthtml += '</div>';
                            infanthtml += '<div class="form-group label-floating">';
                            infanthtml += '<label for="infant_dob_' + (i + 1) + '" class="control-label" style="left:0">Date of Birth</label>';
                            infanthtml += '<input type="text" id="infant_dob_' + (i + 1) + '" name="infant_dob_' + (i + 1) + '" class="form-control infant_dob" maxlength="10">';
                            infanthtml += '</div>';
                            if (parseInt(type) == 2) {
                                infanthtml += '<div class="form-group label-floating">';
                                infanthtml += '<label for="infant_passportno_' + (i + 1) + '" class="control-label" style="left:0">Passport Number</label>';
                                infanthtml += '<input type="text" id="infant_passportno_' + (i + 1) + '" name="infant_passportno_' + (i + 1) + '" class="form-control infant_passportno" maxlength="20">';
                                infanthtml += '</div>';
                                infanthtml += '<div class="form-group label-floating">';
                                infanthtml += '<label for="infant_passportissueplace_' + (i + 1) + '" class="control-label" style="left:0">Passport Issuing Place</label>';
                                infanthtml += '<input type="text" id="infant_passportissueplace_' + (i + 1) + '" name="infant_passportissueplace_' + (i + 1) + '" class="form-control infant_passportissueplace" maxlength="20">';
                                infanthtml += '</div>';
                                infanthtml += '<div class="form-group label-floating">';
                                infanthtml += '<label for="infant_passportissuedate_' + (i + 1) + '" class="control-label" style="left:0">Passport Issue date</label>';
                                infanthtml += '<input type="text" id="infant_passportissuedate_' + (i + 1) + '" name="infant_passportissuedate_' + (i + 1) + '" class="form-control infant_passportissuedate" maxlength="10">';
                                infanthtml += '</div>';
                                infanthtml += '<div class="form-group label-floating">';
                                infanthtml += '<label for="infant_passportexpirydate_' + (i + 1) + '" class="control-label" style="left:0">Passport Expiry date</label>';
                                infanthtml += '<input type="text" id="infant_passportexpirydate_' + (i + 1) + '" name="infant_passportexpirydate_' + (i + 1) + '" class="form-control infant_passportexpirydate" maxlength="10">';
                                infanthtml += '</div>';
                                infanthtml += '<div class="form-group select">';
                                infanthtml += '<label for="infant_visatype_' + (i + 1) + '" class="control-label text-left" style="margin-top:5px;left:0">Visa Type</label>';
                                infanthtml += '<select id="infant_visatype_' + (i + 1) + '" name="infant_visatype_' + (i + 1) + '" class="infant_visatype">';
                                infanthtml += '<option value="">Select Visa Type</option>';
                                infanthtml += '<option>Business visa</option>';
                                infanthtml += '<option>Dependent</option>';
                                infanthtml += '<option>Employment</option>';
                                infanthtml += '<option>Hajj</option>';
                                infanthtml += '<option>Migrant</option>';
                                infanthtml += '<option>Residence</option>';
                                infanthtml += '<option>Seamen</option>';
                                infanthtml += '<option>Student</option>';
                                infanthtml += '<option>Tourist</option>';
                                infanthtml += '<option>Umrah</option>';
                                infanthtml += '</select>';
                                infanthtml += '</div>';
                            }
                            infanthtml += '</div>';
                            infanthtml += '</div>';
                            infanthtml += '</div>';
                        }
                    }
                    $("#traveller_info").append($.parseHTML(adulthtml + childhtml + infanthtml));

                    if (LOGGED_IN_GENDER != null) {
                        if (LOGGED_IN_GENDER == 'Mr') {
                            $("#adult_mr_1").prop('checked', true);
                        }
                        if (LOGGED_IN_GENDER == 'Ms') {
                            $("#adult_ms_1").prop('checked', true);
                        }
                    }
                    if (LOGGED_IN_FIRSTNAME != null)
                        $("#adult_firstname_1").val(LOGGED_IN_FIRSTNAME);

                    if (LOGGED_IN_LASTNAME != null)
                        $("#adult_lastname_1").val(LOGGED_IN_LASTNAME);

                    if (localStorage.getItem("flighttotalfare") != null) {
                        $("#domesticMPF").val(type);
                        $("#FromCodeMPF").val(origin);
                        $("#ToCodeMPF").val(destination);
                        $("#DepartureDateMPF").val(departDate);
                        $("#ArrivalDateMPF").val(returnDate);
                        $("#travellersMPF").val(travellers);
                        $("#totaltravellersMPF").val(totaltravellers);
                        $("#cabinclassMPF").val(cabinClass);
                        $("#roundtripMPF").val(mode);
                        $("#onwardflightMPF").val(localStorage.getItem("onwardflight"));
                        if (mode == 'ROUND')
                            $("#returnflightMPF").val(localStorage.getItem("returnflight"));
                        else
                            $("#returnflightMPF").val('');
                        $("#totalfareMPF").val(localStorage.getItem("flighttotalfare"));
                        $("#labelTotalfareMPF span").html('<i class="fa fa-rupee"></i> ' + parseFloat($("#totalfareMPF").val()).toLocaleString('en-IN'));
                        //GetWalletBalance();
                        if (adult > 0 && parseInt(type) == 2) {
                            for (var i = 0; i < adult; i++) {
                                $('#adult_passportissuedate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', maxDate: new Date(maxDt.getFullYear(), maxDt.getMonth(), maxDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                                $('#adult_passportexpirydate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', minDate: expiryDt, showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                            }
                        }
                        if (child > 0 && parseInt(type) == 2) {
                            for (var i = 0; i < child; i++) {
                                $('#child_passportissuedate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', maxDate: new Date(maxDt.getFullYear(), maxDt.getMonth(), maxDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                                $('#child_passportexpirydate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', minDate: expiryDt, showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                            }
                        }
                        if (infant > 0) {
                            for (var i = 0; i < infant; i++) {
                                $('#infant_dob_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', maxDate: new Date(maxDt.getFullYear(), maxDt.getMonth(), maxDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                                if (parseInt(type) == 2) {
                                    $('#infant_passportissuedate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', maxDate: new Date(maxDt.getFullYear(), maxDt.getMonth(), maxDt.getDate()), showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                                    $('#infant_passportexpirydate_' + (i + 1)).datetimepicker({ format: 'DD-MM-YYYY', minDate: expiryDt, showClear: true, showClose: true, widgetPositioning: { horizontal: 'left', vertical: 'auto' } });
                                }
                            }
                        }

                        jQuery.validator.addClassRules({
                            adult_gender: { required: true },
                            adult_firstname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return true;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            adult_lastname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return true;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            adult_passportno: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9]+$/,
                            },
                            adult_passportissueplace: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s]+$/,
                            },
                            adult_passportissuedate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            adult_passportexpirydate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            adult_visatype: {
                                required: {
                                    depends: function (e) {
                                        return parseInt(type) == 2;
                                    }
                                },
                            },
                            child_gender: {
                                required: {
                                    depends: function (e) {
                                        return (child > 0);
                                    }
                                },
                            },
                            child_firstname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return (child > 0);
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            child_lastname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return (child > 0);
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            child_passportno: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9]+$/,
                            },
                            child_passportissueplace: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s]+$/,
                            },
                            child_passportissuedate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            child_passportexpirydate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            child_visatype: {
                                required: {
                                    depends: function (e) {
                                        return parseInt(type) == 2;
                                    }
                                },
                            },
                            infant_gender: {
                                required: {
                                    depends: function (e) {
                                        return (infant > 0);
                                    }
                                },
                            },
                            infant_firstname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return (infant > 0);
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            infant_lastname: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return (infant > 0);
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s\']+$/,
                            },
                            infant_dob: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return (infant > 0);
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                            },
                            infant_passportno: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9]+$/,
                            },
                            infant_passportissueplace: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[a-zA-Z0-9\s]+$/,
                            },
                            infant_passportissuedate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            infant_passportexpirydate: {
                                required: {
                                    depends: function (e) {
                                        $(this).val($(this).val().trim());
                                        return parseInt(type) == 2;
                                    }
                                },
                                regex: /^[0-9\-]+$/,
                                rangelength: [10, 10]
                            },
                            infant_visatype: {
                                required: {
                                    depends: function (e) {
                                        return parseInt(type) == 2;
                                    }
                                },
                            },
                        });

                        $('form#formMPF').validate({
                            rules: {
                                domesticMPF: { required: true },
                                FromCodeMPF: { required: true },
                                ToCodeMPF: { required: true },
                                DepartureDateMPF: { required: true },
                                ArrivalDateMPF: {
                                    required: {
                                        depends: function (e) {
                                            return $("#roundtripMPF").val() == 'ROUND';
                                        }
                                    }
                                },
                                travellersMPF: { required: true },
                                totaltravellersMPF: { required: true },
                                cabinclassMPF: { required: true },
                                roundtripMPF: { required: true },
                                onwardflightMPF: { required: true },
                                returnflightMPF: {
                                    required: {
                                        depends: function (e) {
                                            return $("#roundtripMPF").val() == 'ROUND' && parseInt(type) == 1;
                                        }
                                    }
                                },
                                totalfareMPF: { required: true },
                            },
                            messages: {
                                domesticMPF: { required: "Domestic/International not selected" },
                                FromCodeMPF: { required: "Departure city not selected" },
                                ToCodeMPF: { required: "Arrival city not selected" },
                                DepartureDateMPF: { required: "Departure date not selected" },
                                ArrivalDateMPF: { required: "Arrival date not selected" },
                                travellersMPF: { required: "Traveller not selected" },
                                totaltravellersMPF: { required: "Traveller not selected" },
                                cabinclassMPF: { required: "Cabin Class not selected" },
                                roundtripMPF: { required: "Trip type not selected" },
                                onwardflightMPF: { required: "Departure flight not selected" },
                                returnflightMPF: { required: "Return flight not selected" },
                                totalfareMPF: { required: "Total fare not available" },
                            }
                        });
                    }
                    else {
                        $("#errorMessageMPF").html('Oops! Something went wrong. Go back and try again.');
                        $("#formLoadingMPF").fadeOut('fast', function (e) {
                            $("#formErrorMPF").fadeIn();
                        });
                    }
                }
            }
            else {
                $("#errorMessageMPF").html('Sign In and try again.');
                $("#formLoadingMPF").fadeOut('fast', function (e) {
                    $("#formErrorMPF").fadeIn();
                });
            }
        }
    } catch (e) {
        alertException(e, current_page_id, 'PageLoad');
    }

    $("#usewalletMPF").on('click', function (e) {
        try {
            var amt = parseFloat($("#hdnamountMPF").val());
            var ewbal = parseFloat($("#hdnwalletbalanceMPF").val());
            var bal = 0;

            if ($(this).prop('checked')) {
                bal = (ewbal >= amt) ? 0 : (amt - ewbal);
                if (bal == 0)
                    $("#usegrplcashMPF").prop('checked', false);
            }
            else {
                bal = amt;
            }
            BalanceAmountToPay();
        } catch (e) {
            alertException(e, current_page_id, 'usewalletMPF.click');
        }
    });

    $("#usegrplcashMPF").on('click', function (e) {
        try {
            BalanceAmountToPay();
        } catch (e) {
            alertException(e, current_page_id, 'usegrplcashMPF.click');
        }
    });


    $("a.goto-flightbooking").on('click', function (e) {
        try {
            window.location = virtualPath + "Account/FlightBooking";
        } catch (e) {
            alertException(e, current_page_id, 'a.goto-flightbooking.click');
        }
    });


    $("a.retry-make-payment").on('click', function (e) {
        try {
            $("form#formMPF").submit();
        } catch (e) {
            alertException(e, current_page_id, 'a.retry-make-payment.click');
        }
    });

    $('form#formMPF').on('submit', function (e) {
        try {
            var isvalidate = $(this).valid();
            if (isvalidate) {
                var pmd = "";
                if ($("#usewalletMPF").prop("checked")) {
                    pmd += "EW";
                }

                if ($("#usegrplcashMPF").prop("checked")) {
                    pmd += (pmd == '' ? '' : ',') + "GC";
                }

                if (parseFloat($("#hdnbalanceamtMPF").val()) > 0) {
                    pmd += (pmd == '' ? '' : ',') + "CC";
                }

                if (profileflagMPF) {
                    bootalert(profile_update_message);
                    return false;
                }

                var adultinfo = '';
                if (adult > 0) {
                    for (var i = 0; i < adult; i++) {
                        var gender = $("#adult_mr_" + (i + 1)).prop('checked') ? $("#adult_mr_" + (i + 1)).val() : ($("#adult_ms_" + (i + 1)).prop('checked') ? $("#adult_ms_" + (i + 1)).val() : $("#adult_mrs_" + (i + 1)).val());
                        var firstname = $("#adult_firstname_" + (i + 1)).val().trim();
                        var lastname = $("#adult_lastname_" + (i + 1)).val().trim();
                        var passportno = "";
                        var passportissueplace = "";
                        var passportissuedate = "";
                        var passportexpirydate = "";
                        var visatype = "";
                        if (parseInt(type) == 2) {
                            passportno = $("#adult_passportno_" + (i + 1)).val().trim();
                            passportissueplace = $("#adult_passportissueplace_" + (i + 1)).val().trim();
                            passportissuedate = $("#adult_passportissuedate_" + (i + 1)).val().trim();
                            passportexpirydate = $("#adult_passportexpirydate_" + (i + 1)).val().trim();
                            visatype = $("#adult_visatype_" + (i + 1) + " :selected").val().trim();
                        }
                        adultinfo = adultinfo + (adultinfo == '' ? '' : '~') + gender + '#' + firstname + '#' + lastname + '#' + passportno + '#' + passportissueplace + '#' + passportissuedate + '#' + passportexpirydate + '#' + visatype;
                    }
                    //console.log(adultinfo);
                }
                var childinfo = '';
                if (child > 0) {
                    for (var i = 0; i < child; i++) {
                        var gender = $("#child_master_" + (i + 1)).prop('checked') ? $("#child_master_" + (i + 1)).val() : $("#child_miss_" + (i + 1)).val();
                        var firstname = $("#child_firstname_" + (i + 1)).val().trim();
                        var lastname = $("#child_lastname_" + (i + 1)).val().trim();
                        var passportno = "";
                        var passportissueplace = "";
                        var passportissuedate = "";
                        var passportexpirydate = "";
                        var visatype = "";
                        if (parseInt(type) == 2) {
                            passportno = $("#child_passportno_" + (i + 1)).val().trim();
                            passportissueplace = $("#child_passportissueplace_" + (i + 1)).val().trim();
                            passportissuedate = $("#child_passportissuedate_" + (i + 1)).val().trim();
                            passportexpirydate = $("#child_passportexpirydate_" + (i + 1)).val().trim();
                            visatype = $("#child_visatype_" + (i + 1) + " :selected").val().trim();
                        }
                        childinfo = childinfo + (childinfo == '' ? '' : '~') + gender + '#' + firstname + '#' + lastname + '#' + passportno + '#' + passportissueplace + '#' + passportissuedate + '#' + passportexpirydate + '#' + visatype;
                    }
                    //console.log(childinfo);
                }
                var infantinfo = '';
                if (infant > 0) {
                    for (var i = 0; i < infant; i++) {
                        var gender = $("#infant_master_" + (i + 1)).prop('checked') ? $("#infant_master_" + (i + 1)).val() : $("#infant_miss_" + (i + 1)).val();
                        var firstname = $("#infant_firstname_" + (i + 1)).val().trim();
                        var lastname = $("#infant_lastname_" + (i + 1)).val().trim();
                        var dob = $("#infant_dob_" + (i + 1)).val().trim();
                        //dob = dob.split("/")[0] + "-" + dob.split("/")[1] + "-" + dob.split("/")[2];
                        var passportno = "";
                        var passportissueplace = "";
                        var passportissuedate = "";
                        var passportexpirydate = "";
                        var visatype = "";
                        if (parseInt(type) == 2) {
                            passportno = $("#infant_passportno_" + (i + 1)).val().trim();
                            passportissueplace = $("#infant_passportissueplace_" + (i + 1)).val().trim();
                            passportissuedate = $("#infant_passportissuedate_" + (i + 1)).val().trim();
                            passportexpirydate = $("#infant_passportexpirydate_" + (i + 1)).val().trim();
                            visatype = $("#infant_visatype_" + (i + 1) + " :selected").val().trim();
                        }
                        infantinfo = infantinfo + (infantinfo == '' ? '' : '~') + gender + '#' + firstname + '#' + lastname + '#' + dob + '#' + passportno + '#' + passportissueplace + '#' + passportissuedate + '#' + passportexpirydate + '#' + visatype;
                    }
                    //console.log(infantinfo);
                }

                var btntext = $("button.make-payment span").html();
                var paramList = {};
                paramList.s = pmd + "|" + ParameterString();
                paramList.o = $("#onwardflightMPF").val().trim();
                paramList.r = $("#returnflightMPF").val().trim();
                paramList.a = adultinfo;
                paramList.c = childinfo;
                paramList.i = infantinfo;
                var s = paramList.s;
                //console.log(JSON.stringify(paramList));
                paramList.s = StringEncryption(paramList.s);
                $.ajax({
                    type: 'POST',
                    url: apiPath + "/Customer/InterimFlightBooking",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(paramList),
                    dataType: 'json',
                    jsonp: null,
                    beforeSend: function (xhr, opts) {
                        $("button.make-payment").prop('disabled', true);
                        $("button.make-payment span").html('<i class="fa fa-cog fa-spin fa-fw"></i>&nbsp;submitting...');
                    },
                    success: function (data, textStatus, xhr) {
                        try {
                            var r = StringDecryption(data);
                            var arrR = r.split("|");
                            var tempStatus = arrR[0];
                            var tempReqId = arrR[1];
                            if (tempStatus == 'success') {
                                FlightBookingCompletion(tempStatus, tempReqId);
                            }
                            else if (tempStatus == 'inprocess') {
                                $("button.make-payment span").html('Redirecting...');
                                s = 'F' + "|" + tempReqId + "|" + LOGGED_IN_NAME + "|" + LOGGED_IN_EMAIL + "|" + LOGGED_IN_MOBILENO + "|" + LOGGED_IN_ID + "|" + s;
                                s = StringEncryption(s);
                                var uW = webPath + '/Customer/PaymentInitiate?s=' + s + '&p=' + StringEncryption(virtualPath + 'Account/FlightMakePayment');
                                window.location = uW;
                            }
                        } catch (e) {
                            //console.log(e);
                            alertException(e, current_page_id, 'InterimFlightBooking.success');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $("button.make-payment").prop('disabled', false);
                        $("button.make-payment span").html(btntext);
                        OnAjaxError(xhr, textStatus, errorThrown);
                    },
                    complete: function (a) {
                    }
                });
                e.preventDefault();
            }
            e.preventDefault();
        } catch (e) {
            alertException(e, current_page_id, 'form#formMPF.submit');
        }

    });


    $(document).on('click', '#formMPF a.close-popup-over-list', function (e) {
        try {
            e.preventDefault();
            $('#formMPF .popup-over-list').removeClass('slideInUp');
            $('#formMPF .popup-over-list').addClass('slideOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                $('#formMPF .popup-over-list').hide();
            });
        } catch (e) {
            alertException(e, current_page_id, '#formMPF a.close-popup-over-list.click');
        }
    });
    $("a.goto-flight-search").on('click', function (e) {
        if (parseInt(type) == 1)
            window.location = virtualPath + "Account/FlightSearchResults";
        else
            window.location = virtualPath + "Account/FlightSearchResultsInternational";
    });
    $("a.goto-refer-and-earn").on('click', function (e) {
        window.location = virtualPath + "Account/ReferAndEarn";
    });
});
