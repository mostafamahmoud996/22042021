﻿current_page_id = "#walletsignin";
var ew_page_name = '';
$("body").one('pageshow', current_page_id, function (event, ui) {
    try {
        $('#ewpassword').focus();
        $("#ewpassword").closest(".form-group,.input-group").removeClass('is-empty');
    } catch (e) {
        alertException(e, current_page_id, '');
    }

});

try {
    if (page_query_string != '') {
        actionId = page_query_string_object[0].split('=')[1];
        switch (parseInt(actionId)) {
            case 1:
                ew_page_name = 'wallettopup.html';
                break;

            case 2:
                ew_page_name = 'wallettopupdetails.html';
                break;

            case 3:
                ew_page_name = 'walletledger.html';
                break;

            default:
                break;
        }
    }
    $('form#frmwalletsignin').validate({
        rules: {
            ewpassword: {
                required: {
                    depends: function (e) {
                        $(this).val($(this).val().trim());
                        return true;
                    }
                },
                rangelength: [2, 20]
            },
        },
        messages: {
            ewpassword: {
                required: "Enter a Password",
                rangelength: "Password should be of {0} to {1} characters"
            },
        }
    });
} catch (e) {
    alertException(e, current_page_id, '');
}


$('form#frmwalletsignin').on('submit', function (e) {
    try {
        var isvalidate = $(this).valid();
        if (isvalidate) {
            var ewp = {};
            ewp.s = localStorage.getItem("VF_" + appId + "_LOGGED_IN_ID").toString() + "|" + $("#ewpassword").val().trim();
            //console.log(ewp.s);
            ewp.s = StringEncryption(ewp.s);
            $.ajax({
                type: 'POST',
                url: apiPath + "/Customer/WalletSignIn",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(ewp),
                dataType: 'json',
                jsonp: null,
                success: function (data, textStatus, xhr) {
                    try {
                        if (data.e != '') {
                            alertme(data.e);
                        }
                        else {
                            window.sessionStorage.setItem("sew", localStorage.getItem("VF_" + appId + "_LOGGED_IN_LOGINID"));
            window.location = ew_page_name;
                        }
                    } catch (e) {
                        alertException(e, current_page_id, '');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    OnAjaxError(xhr, textStatus, errorThrown);
                },
            });
        }
        e.preventDefault();
    } catch (e) {
        alertException(e, current_page_id, '');
    }

});

$("#ewforgotpwd").on("click", function (e) {
    alertme('Wallet password has been sent on your Email Id.', 'success');
});