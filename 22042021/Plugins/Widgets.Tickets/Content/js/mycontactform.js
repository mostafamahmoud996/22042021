
var ContactForm = {
    form: false,
    url: false,

    init: function (form, url) {
        this.form = form;
        this.url = url;
    },

    save: function () {
        if (($(this.form).valid()))
        {
            $.ajax({
                cache: false,
                url: this.url,
                data: $(this.form).serialize(),
                type: 'post',
                success: this.successStep,
                error: this.errorStep,
            });
        }
    },

    successStep: function (response) {
        if (response.success) {
            $('#result-error-' + response.formId).html('');
            $('#contact-page-' + response.formId).html(response.message)
        }
        else {
            $('#result-error-' + response.formId).html(response.message);
        }
    },

    errorStep: function (response) {
        alert('Error: ' + response);
    }
};

