﻿After installation go to: 
Content Management -> Widgets -> Tickets System - Set Active TRUE


Change log:
Version 2.56
Added support department - message template

After update plugin please run this script:
alter table [dbo].[Ticket_Department]
add [MessageTemplateId] int
GO
update [dbo].[Ticket_Department] set [MessageTemplateId] = 0
GO

Run method - install string resource in the configuration plugin


Version 2.47
Added support attributes for product ticket, ticket, order ticket
New settings:
Allow customer to close the ticket
Time interval between tickets (seconds)
New tab on the Customer => Tickets - you can display all tickets for the customer

After update run this script:

alter table [dbo].[Ticket_ContactAttribute]
add [LocationControlTypeId] int 
GO
update [Ticket_ContactAttribute] set [LocationControlTypeId] = 0
GO

Add new resource strings:
Admin.Tickets.Fields.Configuration.AllowCustomerCloseTicket => Allow customer to close the ticket
Admin.Tickets.Fields.Configuration.AllowCustomerCloseTicket.hint => Allow customer to close the ticket
Admin.Tickets.Fields.Configuration.TimeIntervalBetweenTickets => Time interval between tickets (seconds)
Admin.Tickets.Fields.Configuration.TimeIntervalBetweenTickets.hint => Please wait several seconds before placing the new ticket
Admin.Tickets.ContactAttributes.Fields.LocationControlType => Location
Admin.Tickets.ContactAttributes.Fields.LocationControlType.hint => Location
Ticket.Fields.TimeIntervalBetweenTickets => Please wait several seconds before placing the new ticket.
