function init(data, flag) {
    var labelType, useGradients, nativeTextSupport, animate;

    (function () {
        var ua = navigator.userAgent,
            iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
            typeOfCanvas = typeof HTMLCanvasElement,
            nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
            textSupport = nativeCanvasSupport
              && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
        //I'm setting this based on the fact that ExCanvas provides text support for IE
        //and that as of today iPhone/iPad current text support is lame
        labelType = (!nativeCanvasSupport || (textSupport && !iStuff)) ? 'Native' : 'HTML';
        nativeTextSupport = labelType == 'Native';
        useGradients = nativeCanvasSupport;
        animate = !(iStuff || !nativeCanvasSupport);
    })();

    var Log = {
        elem: false,
        write: function (text) {
            if (!this.elem)
                this.elem = document.getElementById('log');
            else {
                this.elem.innerHTML = text;
                this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
            }
        }
    };
    var imCount = 0;
    var nodes = [];
    var raw_template = $('#tooltip-template').html();
    var tooltiptemplate = Handlebars.compile(raw_template);



    //init data
    var infovis = document.getElementById('infovis');
    var w = infovis.offsetWidth - 50, h = infovis.offsetHeight - 50;
    //end
    //init Spacetree
    //Create a new ST instance
    var ht = new $jit.Hypertree({
        //id of viz container element
        injectInto: 'infovis',
        //set duration for the animation
        //duration: 800,
        width: w,
        height: h,
        //set animation transition type
        transition: $jit.Trans.Quad.easeInOut,
        //set distance between node and its children
        //levelDistance: 50,
        //orientation: 'top',
        //align: 'center',
        //levelsToShow: 4,
        offsetX: 0, offsetY: 210,
        useCanvas: false,
        //enable panning
        Navigation: {
            enable: true,
            panning: true
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            overridable: false,
            type: 'circle',
            color: 'gray',
            alpha: 1,
            dim: 60,
            width: 100,
            autoHeight: false,
            autoWidth: false,
            align: "left",
            CanvasStyles: {}
        },


        Canvas: {
            injectInto: 'infovis',
            type: '3D', //'3D'  
            width: 400,
            height: 400,
            left: 40,
            useCanvas: false,
            withLabels: true,
            background: true
        },
        Tips: {
            enable: true,
            type: 'HTML',
            offsetX: 10,
            offsetY: 10,
            onShow: function (tip, node) {
                //tip.innerHTML = node.name,
                //tip.innerHTML = node.data.adjustedid,
                //tip.innerHTML = node.data.sponserid
                if (node.name != "") {
                    var html = tooltiptemplate(node);
                    tip.innerHTML = html;
                }
            }
        },
        Edge: {
            overridable: true,
            type: 'line',//'bezier'
            color: '#000',
            CanvasStyles: {
                shadowColor: '#000',
                shadowBlur: 10
            }
        },
        Events: {
            enable: true,
            enableForEdges: true,
            type: 'HTML',
            onClick: function (node, eventInfo, e) {

                if (/[A-Z]/.test(node.id)) {
                    var obj = {
                        SponserId: node.data.SponserId,
                        AdjustedId: node.data.AdjustedId,
                        Position: node.data.Position
                    };
                    $.ajax({
                        url: '/Plugins/PlanBinary/RedirectToRegister',
                        dataType: 'json',
                        type: 'POST',
                        data: obj,
                        success: function (json) {
                            window.location.href = json;
                        },

                    });
                }
            }
        },
        request: function (nodeId, level, onComplete) {
            //alert(nodeId + 'p   ' + level + ' p  ' + onComplete);
            if (/[A-Z]/.test(nodeId)) {
                onComplete.onComplete();
            }
            else {
                //var i = 0;
                $.ajax({
                    url: '/Plugins/PlanBinary/GetGenealogy',
                    dataType: 'json',
                    type: 'POST',
                    data: { id: nodeId },
                    success: function (json) {
                        var tree = json;
                        var subtree = $jit.json.getSubtree(tree, nodeId);
                        $jit.json.prune(subtree, level);
                        //i++;
                        ans = {
                            'id': nodeId,
                            'children': subtree.children
                        };
                        onComplete.onComplete(nodeId, ans);
                    },

                });
            }

        },
        onBeforeCompute: function (node) {

            Log.write("loading " + node.name);
            st.switchPosition("top", "replot")
        },

        onAfterCompute: function (node) {
            Log.write("done");
            if (imCount == 0)
                $('.node').prepend("<img src='' style='background: #fff;margin-left: -50%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            imCount++;

        },

        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function (label, node) {
            $jit.util.addEvent(domElement, 'click', function () {
                ht.onClick(node.id, {
                    onComplete: function () {
                        ht.controller.onComplete();
                    }
                });
            });
            var style = label.style;
            style.color = 'black';
            style.textAlign = 'center';
            style.marginLeft = '25px';
            style.marginTop = '5px';
            if (nodes.indexOf(node.id) !== 1) {
                nodes.push(node.id);
            }
        },

        //This method is called right before plotting
        //a node. It's useful for changing an individual node
        //style properties before plotting it.
        //The data properties prefixed with a dollar
        //sign will override the global node style properties.
        onBeforePlotNode: function (node) {
            //add some color to the nodes in the path between the
            //root node and the selected node.
            if (node.selected) {
                node.data.$color = "#ff7";
            }
            else {
                delete node.data.$color;
                //if the node belongs to the last plotted level
                if (!node.anySubnode("exist")) {
                    //count children number
                    var count = 0;
                    node.eachSubnode(function (n) { count++; });
                    //assign a node color based on
                    //how many children it has
                    node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];
                }
            }


        },
        onPlaceLabel: function (node) {
            var style = domElement.style;
            style.display = '';
            style.cursor = 'pointer';
            if (node._depth <= 1) {
                style.fontSize = "0.8em";
                style.color = "#ddd";

            } else if (node._depth == 2) {
                style.fontSize = "0.7em";
                style.color = "#555";

            } else {
                style.display = 'none';
            }

            var left = parseInt(style.left);
            var w = domElement.offsetWidth;
            style.left = (left - w / 2) + 'px';
        },
        onComplete: function (node) {
            var node = ht.graph.getClosestNodeToOrigin("current");
            var html = "<h4>" + node.name + "</h4><b>Connections:</b>";
            html += "<ul>";
            node.eachAdjacency(function (adj) {
                var child = adj.nodeTo;
                if (child.data) {
                    var rel = (child.data.band == node.name) ? child.data.relation : node.data.relation;
                    html += "<li>" + child.name + " " + "<div class=\"relation\">(relation: " + rel + ")</div></li>";
                }
            });
            html += "</ul>";
            $jit.id('inner-details').innerHTML = html;
            $.ajax({
                url: '/Plugins/PlanBinary/GetAvatarPaths',
                dataType: 'json',
                type: 'POST',
                data: { ids: nodes },
                success: function (obj) {
                    var regex = /[a-zA-Z]/;
                    $.each(obj, function (Key, d) {
                        if (d.path != "") {
                            $('#' + d.id).find('img').attr('src', d.path);
                        }
                        else {
                            if (d.gender == "F") {
                                $('#' + d.id).find('img').attr('src', '/Plugins/Plan.Binary/Content/Images/male.png');
                            }
                            if (d.gender == "M") {
                                $('#' + d.id).find('img').attr('src', '/Plugins/Plan.Binary/Content/Images/female.png');
                            }
                            if (d.gender == "O" && !regex.test(d.id)) {
                                $('#' + d.id).find('img').attr('src', '/Plugins/Plan.Binary/Content/Images/female.png');
                            }
                            else {

                                if (regex.test(d.id)) {
                                    $('#' + d.id).detach('img');
                                    $('#' + d.id).html('Sign Up');
                                    $('#' + d.id).css('margin-top', '19px');
                                    $('#' + d.id).css('color', 'white');
                                }
                            }
                        }
                    });
                }

            });

        }
    });


    //load JSON data.
    ht.loadJSON(json);
    //compute positions and plot.
    ht.refresh();
    //end
    ht.controller.onComplete();

    $('#search').click(function () {
        var searchid = $('#searchId').val();

        $.ajax({
            cache: false,
            type: "POST",
            data: { id: searchid },
            dataType: "json",
            async: false,
            url: "/Plugins/PlanBinary/GetGenealogy",
            success: function (d) {
                if (d == "Invalid") {
                    $("#infovis").html("<div style='padding-top: 130px;padding-left: 300px;'>Invalid Member ID</div>");
                }
                else {
                    var data = JSON.stringify(d);
                    $("#infovis").html("");
                    var i = init(d);
                    $('.node').css('left', '290px');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    });
}


