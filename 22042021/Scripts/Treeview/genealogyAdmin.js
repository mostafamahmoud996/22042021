var labelType, useGradients, nativeTextSupport, animate;

(function () {
    var ua = navigator.userAgent,
        iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
        typeOfCanvas = typeof HTMLCanvasElement,
        nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
        textSupport = nativeCanvasSupport
          && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
    //I'm setting this based on the fact that ExCanvas provides text support for IE
    //and that as of today iPhone/iPad current text support is lame
    labelType = (!nativeCanvasSupport || (textSupport && !iStuff)) ? 'Native' : 'HTML';
    nativeTextSupport = labelType == 'Native';
    useGradients = nativeCanvasSupport;
    animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
    elem: false,
    write: function (text) {
        if (!this.elem)
            this.elem = document.getElementById('log');
        else {
            this.elem.innerHTML = text;
            this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
        }
    }
};
var imCount = 0;
var nodes = [];
var raw_template = $('#tooltip-template').html();
var tooltiptemplate = Handlebars.compile(raw_template);
function init(data) {

    //init data

    //end
    //init Spacetree
    //Create a new ST instance
    var st = new $jit.ST({
        //id of viz container element
        injectInto: 'infovis',
        //set duration for the animation
        duration: 800,
        //set animation transition type
        transition: $jit.Trans.Quad.easeInOut,
        //set distance between node and its children
        levelDistance: 50,
        orientation: 'top',
        align: 'center',
        levelsToShow: 4,
        offsetX: 0, offsetY: 210,
        useCanvas: false,
        //enable panning
        Navigation: {
            enable: true,
            panning: true
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            overridable: false,
            type: 'circle',
            color: 'gray',
            alpha: 1,
            dim: 60,
            width: 100,
            autoHeight: false,
            autoWidth: false,
            align: "left",
            CanvasStyles: {}
        },


        Canvas: {
            injectInto: 'infovis',
            type: '3D', //'3D'  
            width: 400,
            height: 400,
            useCanvas: false,
            withLabels: true,
            background: true
        },
        Tips: {
            enable: true,
            type: 'HTML',
            offsetX: 10,
            offsetY: 10,
            onShow: function (tip, node) {
                //tip.innerHTML = node.name,
                //tip.innerHTML = node.data.adjustedid,
                //tip.innerHTML = node.data.sponserid
                if (node.name != "") {
                    var html = tooltiptemplate(node);
                    tip.innerHTML = html;
                }
            }
        },
        Edge: {
            overridable: true,
            type: 'line',//'bezier'
            color: '#000',
            CanvasStyles: {
                shadowColor: '#000',
                shadowBlur: 10
            }
        },
        Events: {
            enable: true,
            enableForEdges: true,
            type: 'HTML',
            //onClick: function (nodeOrEdge, eventInfo, e) {

            //    if (eventInfo.getEdge()) {
            //        alert(" Edge clicked");
            //    }
            //}
        },
        request: function (nodeId, level, onComplete) {
            //alert(nodeId + 'p   ' + level + ' p  ' + onComplete);
            if (/[A-Z]/.test(nodeId)) {
                onComplete.onComplete();
            }
            else {
                //var i = 0;
                $.ajax({
                    url: '/Plugins/PlanBinary/GetGenealogy',
                    dataType: 'json',
                    type: 'POST',
                    data: { id: nodeId },
                    success: function (json) {
                        var tree = json;
                        var subtree = $jit.json.getSubtree(tree, nodeId);
                        $jit.json.prune(subtree, level);
                        //i++;
                        ans = {
                            'id': nodeId,
                            'children': subtree.children
                        };
                        onComplete.onComplete(nodeId, ans);
                    },

                });
            }

        },
        onBeforeCompute: function (node) {

            Log.write("loading " + node.name);
            st.switchPosition("top", "replot")
        },

        onAfterCompute: function (node) {
            Log.write("done");
            //if (node.data.Avatar == "") {
            //    if (node.data.Gender == "1")
            //$('.node').prepend("<img src='/Plugins/Plan.Binary/Content/Images/female.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);padding: 4px;'>");
            //    if (node.data.Gender == "0")
            //        $('.node').prepend("<img src='/Plugins/Plan.Binary/Content/Images/male.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);padding: 4px;'>");
            //}
            //else
            //    $('.node').prepend("<img src=" & node.Avatar & "style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);padding: 4px;'>");
            if (imCount == 0)
                $('.node').prepend("<img src='' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            imCount++;

        },

        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function (label, node) {
            label.id = node.id;
            label.innerHTML = node.name;
            label.onclick = function () {
                //if (normal.checked) {
                //    
                //} else {
                st.onClick(node.id);
                //}
            };
            //set label styles
            var style = label.style;
            //style.width = 81 + 'px';
            // style.height = 81 + 'px';
            //style.cursor = 'pointer';
            style.color = 'black';
            //style.fontSize = '0.8em';
            style.textAlign = 'center';
            style.marginLeft = '24px';
            style.marginTop = '4px';
            //style.top = '260px';            
            //style.left= '290';
            //style.borderRadius = '50%';
            //$('#' + node.id).prepend("<img src='/Plugins/Plan.Binary/Content/Images/female.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            //if (node.data.Avatar == "" || node.data.Avatar == null) {
            //    if (node.data.Gender == "1") {
            //        if ($('#' + node.id).find('img')) {
            //            //$('#' + node.id).detach('img');
            //            //$('#' + node.id).append("<img src='/Plugins/Plan.Binary/Content/Images/female.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            //            $('#' + node.id).attr('src', '/Plugins/Plan.Binary/Content/Images/female.png');
            //        }
            //    }
            //    if (node.data.Gender == "0") {
            //        //$('#' + node.id).detach('img');
            //        //$('#' + node.id).append("<img src='/Plugins/Plan.Binary/Content/Images/male.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            //        $('#' + node.id).attr('src', '/Plugins/Plan.Binary/Content/Images/male.png');
            //    }
            //}
            //else {
            //    //$('#' + node.id).detach('img');
            //    //$('#' + node.id).append("<img src=" + node.data.Avatar + "style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);'>");
            //    $('#' + node.id).attr('src', node.data.Avatar);
            //}
            if (nodes.indexOf(node.id) !== 1) {
                nodes.push(node.id);
            }
        },

        //This method is called right before plotting
        //a node. It's useful for changing an individual node
        //style properties before plotting it.
        //The data properties prefixed with a dollar
        //sign will override the global node style properties.
        onBeforePlotNode: function (node) {
            //add some color to the nodes in the path between the
            //root node and the selected node.
            if (node.selected) {
                node.data.$color = "#ff7";
            }
            else {
                delete node.data.$color;
                //if the node belongs to the last plotted level
                if (!node.anySubnode("exist")) {
                    //count children number
                    var count = 0;
                    node.eachSubnode(function (n) { count++; });
                    //assign a node color based on
                    //how many children it has
                    node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];
                }
            }


        },
        onPlaceLabel: function (node) {

        },
        onComplete: function (node) {
            $.ajax({
                url: '/Plugins/PlanBinary/GetAvatarPaths',
                dataType: 'json',
                type: 'POST',
                data: { ids: nodes },
                success: function (obj) {
                    $.each(obj, function (Key, d) {
                        if (d.path != "")
                            $('#' + d.id).find('img').attr('src', d.path);
                        else {
                            if (d.gender == "0") {

                                $('#' + d.id).find('img').attr('src', '/Plugins/Plan.Binary/Content/Images/male.png');
                            }
                            if (d.gender == "1") {

                                $('#' + d.id).find('img').attr('src', '/Plugins/Plan.Binary/Content/Images/female.png');
                            }
                            else {
                                var regex = /[a-zA-Z]/;
                                if (regex.test(d.id)) {
                                    $('#' + d.id).detach('img');
                                    $('#' + d.id).html('Sign Up');
                                    $('#' + d.id).css('margin-top', '19px');
                                    $('#' + d.id).css('color', 'white');
                                }
                            }
                        }
                    });
                }

            });

        },
        onAfterPlotNode: function (node) {


            //$('#'+node.id).append("<img src='/Plugins/Plan.Binary/Content/Images/female.png' style='background: #fff;margin-left: 15%;z-index: 1000;position: inherit;width: 51px;border-radius: 50%;border: 1px solid rgba(52, 73, 94, 0.44);padding: 4px;'>");
        },
        //This method is called right before plotting
        //an edge. It's useful for changing an individual edge
        //style properties before plotting it.
        //Edge data proprties prefixed with a dollar sign will
        //override the Edge global style properties.
        onBeforePlotLine: function (adj) {
            if (adj.nodeFrom.selected && adj.nodeTo.selected) {
                adj.data.$color = "#eed";
                adj.data.$lineWidth = 3;
            }
            else {
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }

        }
    });
    //load json data
    st.loadJSON(data);
    //compute node positions and layout
    st.compute();
    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
    st.onClick(st.root);
    //end
    //Add event handlers to switch spacetree orientation.
    //var top = $jit.id('r-top'),
    //    left = $jit.id('r-left'),
    //    bottom = $jit.id('r-bottom'),
    //    right = $jit.id('r-right'),
    //    normal = $jit.id('s-normal');


    //function changeHandler() {
    //    if (this.checked) {
    //        top.disabled = bottom.disabled = right.disabled = left.disabled = true;
    //        st.switchPosition(this.value, "animate", {
    //            onComplete: function () {
    //                top.disabled = bottom.disabled = right.disabled = left.disabled = false;
    //            }
    //        });
    //    }
    //};

    //top.onchange = left.onchange = bottom.onchange = right.onchange = changeHandler;
    //end

}

